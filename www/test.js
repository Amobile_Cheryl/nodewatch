function foo(){}              //通过function foo(){}定义一个函数对象
foo.prototype.z = 3;          //函数默认带个prototype对象属性   (typeof foo.prototype;//"object")
 
var obj =new foo();           //我们通过new foo()构造器的方式构造了一个新的对象
obj.y = 2;                    //通过赋值添加两个属性给obj
obj.x = 1;                    //通过这种方式构造对象，对象的原型会指向构造函数的prototype属性，也就是foo.prototype
                               
obj.x; // 1                 //当访问obj.x时，发现obj上有x属性，所以返回1
obj.y; // 2                 //当访问obj.y时，发现obj上有y属性，所以返回2
obj.z; // 3                 //当访问obj.z时，发现obj上没有z属性，那怎么办呢？它不会停止查找，它会查找它的原型，也就是foo.prototype,这时找到z了，所以返回3
 
//我们用字面量创建的对象或者函数的默认prototype对象，实际上它也是有原型的，它的原型指向Object.prototype,然后Object.prototype也是有原型的，它的原型指向null。
                                   //那这里的Object.prototype有什么作用呢？
typeof obj.toString; // ‘function'  
 
//我们发现typeof obj.toString是一个函数，但是不管在对象上还是对象的原型上都没有toString方法，因为在它原型链的末端null之前都有个Object.prototype方法，
//而toString正是Object.prototype上面的方法。这也解释了为什么JS基本上所有对象都有toString方法
'z' in obj; // true               //obj.z是从foo.prototype继承而来的，所以'z' in obj返回了true
obj.hasOwnProperty('z'); // false   //但是obj.hasOwnProperty('z')返回了false,表示z不是obj直接对象上的，而是对象的原型链上面的属性。(hsaOwnProperty也是Object.prototype上的方法)
