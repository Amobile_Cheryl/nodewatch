(function($) {
    /* define namespace starts */
    var AD = {};
    AD.collections = {};	
    AD.title = "ns:Menu.MonitorSetting";
    AD.page = "ajax/alertSetting_list.html";

    var url = $.config.server_rest_url+'/alertDefinitions';
	var idAttribute = AD.idAttribute = "id";

	AD.forms = [
        //forms
        /*
		{
            // 'class': 'col col-2 ',
            title: 'id',
            property: 'id',
            default_value: ""
        },
		*/
        // "row",
        //form
        {
            // 'class': 'col col-2',
            title: 'name',
            property: 'name',
            default_value: ""
        },
        // "fieldset",
        //form
        _.defaults({
            // 'class': 'col col-2',
            title: 'Email Notification',
            property: 'processingMode',
            options: [
				{name:"Email", isChecked:true},
				{name:"None", isChecked:false}
			]
        },Backbone.UIToolBox.RadioButton),
        "row",
        //form
        _.defaults({
            // 'class': 'col col-2',
            title: 'Target',
            property: 'node1',
            class: 'col col-4',
			label_value: function(argument) {
				if(!AD.collections.target) {
					var coll = Backbone.getCollectionByUrl($.config.server_rest_url+'/alertSettingOptions',"id");
					var onErrorHandler = function(collection, response, options) {
						coll.add([
							{id:"", name:""},
							{id:"AvailableStorageSize", name:"Available Storage Size"},
							{id:"BatteryLevel", name:"Battery Level"},
							{id:"BatteryTemperature", name:"Battery Temperature"},
							{id:"BluetoothEnabled", name:"Bluetooth Enabled"},
							{id:"BuiltinAvailableMemorySize", name:"Builtin Available Memory Size"},
							{id:"CPULoad", name:"CPU Load"},
							{id:"DeviceLastConnectTime", name:"Device Last Connect Time"},
							{id:"DeviceStatus", name:"Device Status"},
							{id:"DeviceType", name:"Device Type"},
							{id:"ExtensionAvailableMemorySize", name:"Extension Available Memory Size"},
							{id:"KeyboardEnabled", name:"Keyboard Enabled"},
							{id:"PrimaryCameraEnabled", name:"Primary Camera Enabled"},
							{id:"WLANEnabled", name:"WLAN Enabled"},
							{id:"WWANEnabled", name:"WWAN Enabled"}
						]);
					};
					coll.fetch({
						async: false,
						data: {
							"fields": ["id", "name"]
						},
						error: onErrorHandler
					});
					coll.on("add", function(coll) {
						console.log("Add "+coll.get("option_name")+"!");
					});
					// coll.pop();
					AD.collections.target = coll;
				}
				var coll = AD.collections.target;
				//console.log(coll);
				var jcol = coll.toJSON();
				//console.log(jcol);
				var ret = [];
				for (var i = 0; i< jcol.length-1; i++) {
					ret.push({
						label: jcol[i].name,
						value: jcol[i].id
					});
				}
				//console.log(ret);
				return ret;
			}()
        },Backbone.UIToolBox.selector),
        //form
        _.defaults({
            // 'class': 'col col-2',
            title: 'Operator',
            property: 'operator1',
			class: 'col col-2',
			//tips: 'Please select one',
			label_value: [
				{label:"", value:""},
				{label:">", value:">"},
				{label:"=", value:"="},
				{label:"<", value:"<"}
			]
        },Backbone.UIToolBox.selector),
        //form
        {
            // 'class': 'col col-2',
            title: 'Value',
            property: 'value1',
			class: 'col col-4',
            default_value: ""
        },
		//
		{
			title: 'Add Condition',
			class: 'col col-2',
			default_value: "",
			view: function() {
				return "<label class=\"btn btn-primary\">AND</label>";
			}
		},
		//////
		_.defaults({
            // 'class': 'col col-2',
            title: 'Target',
            property: 'node2',
            class: 'col col-4',
			label_value: function(argument) {
				if(!AD.collections.target) {
					var coll = Backbone.getCollectionByUrl($.config.server_rest_url+'/alertSettingOptions',"id");
					var onErrorHandler = function(collection, response, options) {
						coll.add([
							{id:"", name:""},
							{id:"AvailableStorageSize", name:"Available Storage Size"},
							{id:"BatteryLevel", name:"Battery Level"},
							{id:"BatteryTemperature", name:"Battery Temperature"},
							{id:"BluetoothEnabled", name:"Bluetooth Enabled"},
							{id:"BuiltinAvailableMemorySize", name:"Builtin Available Memory Size"},
							{id:"CPULoad", name:"CPU Load"},
							{id:"DeviceLastConnectTime", name:"Device Last Connect Time"},
							{id:"DeviceStatus", name:"Device Status"},
							{id:"DeviceType", name:"Device Type"},
							{id:"ExtensionAvailableMemorySize", name:"Extension Available Memory Size"},
							{id:"KeyboardEnabled", name:"Keyboard Enabled"},
							{id:"PrimaryCameraEnabled", name:"Primary Camera Enabled"},
							{id:"WLANEnabled", name:"WLAN Enabled"},
							{id:"WWANEnabled", name:"WWAN Enabled"}
						]);
					};
					coll.fetch({
						async: false,
						data: {
							"fields": ["id", "name"]
						},
						error: onErrorHandler
					});
					coll.on("add", function(coll) {
						console.log("Add "+coll.get("option_name")+"!");
					});
					// coll.pop();
					AD.collections.target = coll;
				}
				var coll = AD.collections.target;
				//console.log(coll);
				var jcol = coll.toJSON();
				//console.log(jcol);
				var ret = [];
				for (var i = 0; i< jcol.length-1; i++) {
					ret.push({
						label: jcol[i].name,
						value: jcol[i].id
					});
				}
				//console.log(ret);
				return ret;
			}()
        },Backbone.UIToolBox.selector),
        //form
        _.defaults({
            // 'class': 'col col-2',
            title: 'Operator',
            property: 'operator2',
			class: 'col col-2',
			//tips: 'Please select one',
			label_value: [
				{label:"", value:""},
				{label:">", value:">"},
				{label:"=", value:"="},
				{label:"<", value:"<"}
			]
        },Backbone.UIToolBox.selector),
        //form
        {
            // 'class': 'col col-2',
            title: 'Value',
            property: 'value2',
			class: 'col col-4',
            default_value: ""
        },
		//
		{
			title: 'Add Condition',
			class: 'col col-2',
			default_value: "",
			view: function() {
				return "<label class=\"btn btn-primary\">AND</label>";
			}
		},
		        _.defaults({
            // 'class': 'col col-2',
            title: 'Target',
            property: 'node3',
            class: 'col col-4',
			label_value: function(argument) {
				if(!AD.collections.target) {
					var coll = Backbone.getCollectionByUrl($.config.server_rest_url+'/alertSettingOptions',"id");
					var onErrorHandler = function(collection, response, options) {
						coll.add([
							{id:"", name:""},
							{id:"AvailableStorageSize", name:"Available Storage Size"},
							{id:"BatteryLevel", name:"Battery Level"},
							{id:"BatteryTemperature", name:"Battery Temperature"},
							{id:"BluetoothEnabled", name:"Bluetooth Enabled"},
							{id:"BuiltinAvailableMemorySize", name:"Builtin Available Memory Size"},
							{id:"CPULoad", name:"CPU Load"},
							{id:"DeviceLastConnectTime", name:"Device Last Connect Time"},
							{id:"DeviceStatus", name:"Device Status"},
							{id:"DeviceType", name:"Device Type"},
							{id:"ExtensionAvailableMemorySize", name:"Extension Available Memory Size"},
							{id:"KeyboardEnabled", name:"Keyboard Enabled"},
							{id:"PrimaryCameraEnabled", name:"Primary Camera Enabled"},
							{id:"WLANEnabled", name:"WLAN Enabled"},
							{id:"WWANEnabled", name:"WWAN Enabled"}
						]);
					};
					coll.fetch({
						async: false,
						data: {
							"fields": ["id", "name"]
						},
						error: onErrorHandler
					});
					coll.on("add", function(coll) {
						console.log("Add "+coll.get("option_name")+"!");
					});
					// coll.pop();
					AD.collections.target = coll;
				}
				var coll = AD.collections.target;
				//console.log(coll);
				var jcol = coll.toJSON();
				//console.log(jcol);
				var ret = [];
				for (var i = 0; i< jcol.length-1; i++) {
					ret.push({
						label: jcol[i].name,
						value: jcol[i].id
					});
				}
				//console.log(ret);
				return ret;
			}()
        },Backbone.UIToolBox.selector),
        //form
        _.defaults({
            // 'class': 'col col-2',
            title: 'Operator',
            property: 'operator3',
			class: 'col col-2',
			//tips: 'Please select one',
			label_value: [
				{label:"", value:""},
				{label:">", value:">"},
				{label:"=", value:"="},
				{label:"<", value:"<"}
			]
        },Backbone.UIToolBox.selector),
        //form
        {
            // 'class': 'col col-2',
            title: 'Value',
            property: 'value3',
			class: 'col col-4',
            default_value: ""
        },
		//
		{
			title: 'Add Condition',
			class: 'col col-2',
			default_value: "",
			view: function() {
				return "<label class=\"btn btn-primary\">AND</label>";
			}
		},
		        _.defaults({
            // 'class': 'col col-2',
            title: 'Target',
            property: 'node4',
            class: 'col col-4',
			label_value: function(argument) {
				if(!AD.collections.target) {
					var coll = Backbone.getCollectionByUrl($.config.server_rest_url+'/alertSettingOptions',"id");
					var onErrorHandler = function(collection, response, options) {
						coll.add([
							{id:"", name:""},
							{id:"AvailableStorageSize", name:"Available Storage Size"},
							{id:"BatteryLevel", name:"Battery Level"},
							{id:"BatteryTemperature", name:"Battery Temperature"},
							{id:"BluetoothEnabled", name:"Bluetooth Enabled"},
							{id:"BuiltinAvailableMemorySize", name:"Builtin Available Memory Size"},
							{id:"CPULoad", name:"CPU Load"},
							{id:"DeviceLastConnectTime", name:"Device Last Connect Time"},
							{id:"DeviceStatus", name:"Device Status"},
							{id:"DeviceType", name:"Device Type"},
							{id:"ExtensionAvailableMemorySize", name:"Extension Available Memory Size"},
							{id:"KeyboardEnabled", name:"Keyboard Enabled"},
							{id:"PrimaryCameraEnabled", name:"Primary Camera Enabled"},
							{id:"WLANEnabled", name:"WLAN Enabled"},
							{id:"WWANEnabled", name:"WWAN Enabled"}
						]);
					};
					coll.fetch({
						async: false,
						data: {
							"fields": ["id", "name"]
						},
						error: onErrorHandler
					});
					coll.on("add", function(coll) {
						console.log("Add "+coll.get("option_name")+"!");
					});
					// coll.pop();
					AD.collections.target = coll;
				}
				var coll = AD.collections.target;
				//console.log(coll);
				var jcol = coll.toJSON();
				//console.log(jcol);
				var ret = [];
				for (var i = 0; i< jcol.length-1; i++) {
					ret.push({
						label: jcol[i].name,
						value: jcol[i].id
					});
				}
				//console.log(ret);
				return ret;
			}()
        },Backbone.UIToolBox.selector),
        //form
        _.defaults({
            // 'class': 'col col-2',
            title: 'Operator',
            property: 'operator4',
			class: 'col col-2',
			//tips: 'Please select one',
			label_value: [
				{label:"", value:""},
				{label:">", value:">"},
				{label:"=", value:"="},
				{label:"<", value:"<"}
			]
        },Backbone.UIToolBox.selector),
        //form
        {
            // 'class': 'col col-2',
            title: 'Value',
            property: 'value4',
			class: 'col col-4',
            default_value: ""
        },
		//
		{
			title: 'Add Condition',
			class: 'col col-2',
			default_value: "",
			view: function() {
				return "<label class=\"btn btn-primary\">AND</label>";
			}
		},
		        _.defaults({
            // 'class': 'col col-2',
            title: 'Target',
            property: 'node5',
            class: 'col col-4',
			label_value: function(argument) {
				if(!AD.collections.target) {
					var coll = Backbone.getCollectionByUrl($.config.server_rest_url+'/alertSettingOptions',"id");
					var onErrorHandler = function(collection, response, options) {
						coll.add([
							{id:"", name:""},
							{id:"AvailableStorageSize", name:"Available Storage Size"},
							{id:"BatteryLevel", name:"Battery Level"},
							{id:"BatteryTemperature", name:"Battery Temperature"},
							{id:"BluetoothEnabled", name:"Bluetooth Enabled"},
							{id:"BuiltinAvailableMemorySize", name:"Builtin Available Memory Size"},
							{id:"CPULoad", name:"CPU Load"},
							{id:"DeviceLastConnectTime", name:"Device Last Connect Time"},
							{id:"DeviceStatus", name:"Device Status"},
							{id:"DeviceType", name:"Device Type"},
							{id:"ExtensionAvailableMemorySize", name:"Extension Available Memory Size"},
							{id:"KeyboardEnabled", name:"Keyboard Enabled"},
							{id:"PrimaryCameraEnabled", name:"Primary Camera Enabled"},
							{id:"WLANEnabled", name:"WLAN Enabled"},
							{id:"WWANEnabled", name:"WWAN Enabled"}
						]);
					};
					coll.fetch({
						async: false,
						data: {
							"fields": ["id", "name"]
						},
						error: onErrorHandler
					});
					coll.on("add", function(coll) {
						console.log("Add "+coll.get("option_name")+"!");
					});
					// coll.pop();
					AD.collections.target = coll;
				}
				var coll = AD.collections.target;
				//console.log(coll);
				var jcol = coll.toJSON();
				//console.log(jcol);
				var ret = [];
				for (var i = 0; i< jcol.length-1; i++) {
					ret.push({
						label: jcol[i].name,
						value: jcol[i].id
					});
				}
				//console.log(ret);
				return ret;
			}()
        },Backbone.UIToolBox.selector),
        //form
        _.defaults({
            // 'class': 'col col-2',
            title: 'Operator',
            property: 'operator5',
			class: 'col col-2',
			//tips: 'Please select one',
			label_value: [
				{label:"", value:""},
				{label:">", value:">"},
				{label:"=", value:"="},
				{label:"<", value:"<"}
			]
        },Backbone.UIToolBox.selector),
        //form
        {
            // 'class': 'col col-2',
            title: 'Value',
            property: 'value5',
			class: 'col col-4',
            default_value: ""
        },
		//
		{
			title: '',
			class: 'col col-2',
			default_value: "",
			view: function() {
				return "<div/>";
			}
		},
		{
			property: 'conditionList',
			default_value: [],
			useTemplate: false,
			view: function() {
				return "<input type=\"hidden\" name=\"conditionList\">";
			},
			value: function() {
				return "";
			}
		},
		//////
		/*
		{
			title: 'Add Condition',
			class: 'col col-2',
			default_value: "",
			view: function() {
				var t = this;
				var addButton = $('<button/>')
					.addClass('btn btn-primary')
					.prop('disabled', true)
					.text('AND')
					.on('click', function(event) {
						event.preventDefault();
						var $this = $(this),
						data = $this.data();
						$("section button:contains(AND)").parent().parent().after("<section class=\"col col-4\"><p>Test</p></section>");
					});
				return addButton;
			}
		},
		*/
		//form
        _.defaults({
            // 'class': 'col col-2',
            title: 'Remark',
            property: 'remark',
			class: 'col col-12',
            default_value: "",
        },Backbone.UIToolBox.textarea)
    ];
	
    AD.columns = [
        //column1
        /*{
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            width: '50px',
            callback: function(o) {
                return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
            }
        },
		*/
        //column2
        {
            title: i18n.t("ns:MonitorSetting.Name"),
            property: 'name',
            filterable: true,
            sortable: true,
            width: '180px'
        },
        {
            title: i18n.t("ns:MonitorSetting.Domain"),
            property: 'domainId',
            filterable: false,
            sortable: false,
            width: '100px',
			callback: function(o) {
				return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId];
            }
        },
		{
            title: i18n.t("ns:MonitorSetting.Condition"),
            filterable: false,
            sortable: false,
			callback: function(o) {
				// Combine conditions
				var condition = "";
				$.each(o.conditionList, function (index, value){
					var operator = "";
					if (!_.isUndefined(o.conditionList[index].operator)) {
	                    switch (o.conditionList[index].operator) {
	                        case "4": operator = ">";        break;
	                        case "1": operator = "=";        break;
	                        case "2": operator = "<";        break;
	                        default:  operator = o.conditionList[index].operator; break;
	                    }
	                }
					if (index == 0) {
						condition = "(" + o.conditionList[0].nodeName + " " + operator + " " + o.conditionList[0].value + ") ";
					} else if (index > 0){ 
						condition += " " + o.conditionList[index].logical + " (" + o.conditionList[index].nodeName + " " + operator + " " + o.conditionList[index].value + ") ";	
					};					
				});				
				return '<a href="#ajax/device_list.html/alertSetting_list/' + o.id +'">' + condition + '</a>';
			}
        },
		{
            title: i18n.t("ns:MonitorSetting.ProcessMode"),
            property: 'processingMode',
            filterable: false,
            sortable: false,
            width: '100px',
			callback: function(o) {
				if(o.processingMode === "Email"){
					return "<i class=\"fa fa-envelope-o\"></i> Email";
				}else{
					return "";
				}
			}
        },
		{
            title: i18n.t("ns:MonitorSetting.Remark"),
            property: 'remark',
            filterable: true,
            sortable: false,
            width: '200px'
        },
		{
            title: i18n.t("ns:MonitorSetting.Action"),
			sortable: false,
            width: '100px',
            callback: function(o) {
                var _html = '';
                _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                return _html;
            }
        }
    ];
    /* define namespace ends */

    /* model starts */
    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });
    /* model ends */

    /* collection starts */
    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });
    /* collection ends */

    var routes = {};
    routes[AD.page + '/grid'] 		 = 'show_grid';    
    routes[AD.page + '/add'] 		 = 'act_add';
    routes[AD.page + '/edit/:_id'] 	 = 'act_edit';
    routes[AD.page + '/delete/:_id'] = 'act_delete';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        // [Action] Add a setting
        act_add: function() {
        	this.navigate("#ajax/alertSetting_add.html", {
                trigger: true
            });
        },
        // [Action] Edit a setting
        act_edit: function(id) {            
            this.navigate("#ajax/alertSetting_edit.html/" + id, {
                trigger: true
            });    
        },
        // [Action] Delete a setting
		act_delete: function(id) {
			var dmURL = $.config.server_rest_url + '/alertDefinitions/';

			$.SmartMessageBox({
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; " + i18n.t("ns:Message.MonitorSetting.DeleteSetting") + " <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> "+i18n.t("ns:Message.MonitorSetting.DeleteTheMonitorContent")+"</span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + id,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.MonitorSetting.MonitorSetting")+i18n.t("ns:Message.MonitorSetting.DeleteSuccess"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.MonitorSetting.DeleteSettingID") + id + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.MonitorSetting.MonitorSetting")+i18n.t("ns:Message.MonitorSetting.DeleteFail"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.MonitorSetting.DeleteSettingID") + id + "</i>",                            
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/alertSetting_list.html";
                    });
                } else {
                    window.location.href = "#ajax/alertSetting_list.html";
                }
            });
		}
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
		i18n.init(function(t){
			$('[data-i18n]').i18n();
		});
		$("body").removeClass("loading");
	
        // alert('bootstrap');
        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/grid", {
                trigger: true
            });
        }

        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
    };
    /* define bootstrap ends */
    $[AD.page] = AD;
    $[AD.page].bootstrap();
})(jQuery);
