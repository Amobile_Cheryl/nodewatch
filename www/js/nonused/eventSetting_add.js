(function($) {        
    var AD = {};
    AD.page = "ajax/eventSetting_add.html";
    
    var reloadJs = function(){
        /*
         * Load bootstrap wizard dependency
         */
        loadScript("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js", runBootstrapWizard);
        
        //Bootstrap Wizard Validations
        var runBootstrapWizard = function() {
            
            var $validator = $("#wizard-1").validate({
            
                rules: {
                    name: {
                        required: true
                    },
                    node: { //node means target
                        required: true
                    },
                    operator: {
                        required: true
                    },
                    value: {
                        required: true
                    },
                    remark: {
                        required: false
                    }
                },
                
                messages: {
                    name:         i18n.t("ns:EventSetting.Messages.SpecifyEventName"),
                    node:         i18n.t("ns:EventSetting.Messages.SpecifyTarget"),
                    operator:     i18n.t("ns:EventSetting.Messages.SpecifyOperator"),
                    value:        i18n.t("ns:EventSetting.Messages.SpecifyValue"),
                    remark:       i18n.t("ns:EventSetting.Messages.SpecifyRemark"),
                },
                
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            
            $("#bootstrap-wizard-1").bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                    // debugger;
                    var $valid = $("#wizard-1").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    } else {
                        // Save button event                        
                        // [Slove IE issue] Kenny block at 2015/2/6
						if($.browser.msie || ($.browser.name === "Netscape")){
							event.returnValue = false;
						}else{
							event.preventDefault();
						}
                        
						$("body").addClass("loading");

                        var originalData = {            
                            name:           $('input[name="name"]').val(),
                            processingMode: $('input[name="radio-inline"]:checked').val(),   
                            node:           $('select[name="node"]').val(),
                            operator:       $('select[name="operator"]').val(),
                            value:          $('input[name="value"]').val(),
                            remark:         $('input[name="remark"]').val()
                        };                        
                        var data = JSON.stringify(originalData);
                        console.log(originalData.node);
                        console.log(data);
                        $("#wizard-1").submit(function(e)
                        {
                            $.ajax({
                                type: 'POST',                                
                                url:  $.config.server_rest_url + '/deviceEventDefinitions' ,
                                contentType: 'application/json',
                                dataType: 'json',
                                data: data,
                                timeout: 5000
                            }).done(function(data) {
                                if (_.isUndefined(data)){
                                    console.log("[Event Setting] Added done => response undefined.");
                                } else {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.EventSetting.EventSetting") + i18n.t("ns:Message.EventSetting.AddSuccess"),
                                        content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Message.EventSetting.AddSuccess") + "</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                }                                
                            }).fail(function (jqXHR, textStatus){
                                $.smallBox({
                                    title: i18n.t("ns:Message.EventSetting.EventSetting") + i18n.t("ns:Message.EventSetting.AddFail"),
                                    content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Message.EventSetting.AddFail") + "</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                console.log("[Event Setting] Added failed => " + textStatus);                                
                            }).always(function(){
                                $("body").removeClass("loading");          
                                window.location.href = "#ajax/eventSetting_list.html/grid";
                            });         
                            e.preventDefault(); //Prevent Default action. 
                        }); 
                        $("#wizard-1").submit(); //Submit the form                       
                    }
                }
            });            
            $('#btn_back').removeClass("disabled");            
            $('#btn_save').removeClass("disabled");
        }

        runBootstrapWizard();

        // Back button event
        $("#btn_back").click(function(event){        
            event.preventDefault();        
            window.location.href = "#ajax/eventSetting_list.html/grid";        
        });        
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Reload JavaScript
        reloadJs();        

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/grid", {
                trigger: true
            });
        }

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }        
    };
    /* define bootstrap ends */
    $[AD.page] = AD;
})(jQuery);
