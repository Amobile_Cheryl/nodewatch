(function($) {
    /* define namespace starts */
    var AD = {};
    AD.collections = {};
	AD.title = "ns:Menu.EventSetting";
    AD.page = "ajax/eventSetting_list.html";

    var url = $.config.server_rest_url+'/deviceEventDefinitions';
	var idAttribute = AD.idAttribute = "id";    

    AD.columns = [
        //column1
        /*
		{
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            width: '50px',
            callback: function(o) {
                return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
            }
        },*/
        //column2
        {
            title: i18n.t("ns:EventSetting.Name"),
            property: 'name',
            filterable: true,
            sortable: true
        },
        {
            title: i18n.t("ns:EventSetting.Domain"),
            property: 'domainId',
            filterable: false,
            sortable: false,
            width: '100px',
			callback: function(o) {
				return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId];
            }
        },
		{
            title: i18n.t("ns:EventSetting.Condition"),
            filterable: false,
            sortable: false,
			callback: function(o) {
                var operator = "";
                if (!_.isUndefined(o.operator)) {
                    switch (o.operator) {
                        case "4": operator = ">";        break;
                        case "1": operator = "=";        break;
                        case "2": operator = "<";        break;
                        default:  operator = o.operator; break;
                    }
                    return '<a href="#ajax/device_list.html/eventSetting_list/' + o.id + '">' + o.node + ' ' + operator + ' ' + o.value + '</a>';
                } else {
                    return "";
                };
			}
        },
		{
            title: i18n.t("ns:EventSetting.ProcessMode"),
            property: 'processingMode',
            filterable: false,
            sortable: false,
            width: '100px',
			callback: function(o) {
				if(o.processingMode === "Email"){
					return "<i class=\"fa fa-envelope-o\"></i> Email";
				}else{
					return "";
				}
			}
        },
		{
            title: i18n.t("ns:EventSetting.Remark"),
            property: 'remark',
            filterable: true,
            sortable: false
        },
		{
            title: i18n.t("ns:EventSetting.Action"),
			sortable: false,
			width: '100px',
            callback: function(o) {
                var _html = '';
                _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                return _html;
            }
        }
    ];
    /* define namespace ends */

    /* model starts */
    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });
    /* model ends */

    /* collection starts */
    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });
    /* collection ends */

    var routes = {};
    routes[AD.page + '/grid']           = 'show_grid';
    routes[AD.page + '/add']            = 'act_add';
    routes[AD.page + '/edit/:_id']      = 'act_edit';    
    routes[AD.page + '/delete/:_id']    = 'act_delete';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        // [Action] Add a setting
        act_add: function() {            
            this.navigate("#ajax/eventSetting_add.html", {
                trigger: true
            });    
        },
        // [Action] Edit a setting
        act_edit: function(id) {            
            this.navigate("#ajax/eventSetting_edit.html/" + id, {
                trigger: true
            });    
        },
        // [Action] Delete a setting
		act_delete: function(id) {
			var dmURL = $.config.server_rest_url + '/deviceEventDefinitions/';

			$.SmartMessageBox({
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; " + i18n.t("ns:Message.EventSetting.DeleteSetting") + " <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> "+i18n.t("ns:Message.EventSetting.DeleteTheEventContent")+"</span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + id,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.EventSetting.DeleteSetting") + i18n.t("ns:Message.EventSetting.DeleteSuccess"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.EventSetting.DeleteSettingID") + id + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.EventSetting.DeleteSetting") + i18n.t("ns:Message.EventSetting.DeleteFail"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.EventSetting.DeleteSettingID") + id + "</i>",                            
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/eventSetting_list.html";
                    });
                } else {
                    window.location.href = "#ajax/eventSetting_list.html";
                }
            });
		}
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
		i18n.init(function(t){
			$('[data-i18n]').i18n();
		});
	
        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/grid", {
                trigger: true
            });
        }

        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");
    };
    /* define bootstrap ends */
    $[AD.page] = AD;
    $[AD.page].bootstrap();
})(jQuery);
