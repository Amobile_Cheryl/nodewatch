(function($) {

    /* define namespace starts */

    var AD = {};
    AD.collections = {};
    // var url = AD.url = $.config.server_url + '/domain';
    // var url = 'http://220.130.176.240:8080/III_NetAdmin2/rest/event_list';
    // var url = 'http://220.130.176.238:8080/acs-core/RESTful/deviceEvents'
    var url = $.config.server_rest_url+"/deviceEvents";
	//var url = 'http://220.130.176.240:8080/III_NetAdmin2/rest/event_list';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "Event";
    AD.page = "ajax/event_list.html";
    //AD.api = 'cctech';
    AD.isAdd = false;

    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /*                  */







    AD.validate = function() {
        return true;
    };


    AD.forms = [
        //form config
        //form
        {
            title: 'Event Time',
            property: 'creationTime',
            default_value: ""
        },
        //form
        {
            title: 'Device Name',
            property: "deviceId",
            default_value: ""
        },
        //form
        {
            title: 'Error Code',
            property: "errorCode",
            default_value: ""
        },
        //form
        {
            title: 'File',
            property: "eventFileId",
            default_value: ""
        }

    ];

    AD.columns = [
        //columns1
        {
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            sortable: false,
            callback: function(o) {
                if (o)
                    return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
                else
                    return '<input type="checkbox" value="0"></input>';
            }
        },
        //columns2
        {
            title: 'Device Name',
            //property: 'deviceId',
            filterable: false,
            sortable: true,
            callback :function(o) 
            {
                return o.device && o.device.cwmpId || "";
            }
        },
        //columns3
        {
            title: 'Event Time',
            callback: function(o) {
                /*change millisecond to date format*/
                var time = o.creationTime;
                var date = time;
                var str = df.format(date);
                return str;
            },
            filterable: false,
            sortable: false
        },
        //columns4
        {
            title: 'Error Code',
            property: 'errorCode',
            filterable: false,
            sortable: false
        },
        //columns5
        {
            title: 'File',
            property: 'eventFileId',
            filterable: false,
            sortable: false,
	    callback: function(o) {
                return 'event'+o.eventFileId+'.txt';
            }
        },
        //columns6
        {
            title: 'Action',
            sortable: false,
            callback: function(o) {
                var _html = '';
                // _html +='<button class="btn btn-mini btn-success" title="Verify"><i class="icon-ok"></i> </button>';
                if (o) {
                    // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Edit</span></a>';
                    _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-times"></i></a>';
                    // _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-edit"></i></a>';
                }
                return _html;
            }
        }
    ];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */
    var routes = {};

    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/delete/:_id'] = 'delete_single';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes

        // show_edit: function(_id) {
        // 	alert('hello?');
        // }
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        // alert('bootstrap');
        // if (!_.isUndefined($[AD.page].app)) {
        //     $[AD.page].app.navigate("#" + AD.page + "/grid", {
        //         trigger: true
        //     });
        // }


        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();

})(jQuery);
