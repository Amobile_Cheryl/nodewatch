(function($) {        
    var AD = {};
    AD.page = "ajax/alertSetting_edit.html";    

    var getSettingInfo = function(id, data) {
        $("body").addClass("loading");

        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/alertDefinitions/' + id,
            contentType: 'application/json',
            dataType: 'json',
            data: data,
            timeout: 15000,
            async: false
        }).done(function(data){
            if (!_.isUndefined(data.conditionList)) {                
                for (var i = 1; i < data.conditionList.length; i++) {
                    if (i < $.config.maxAlertSettingTarget - 1) {
                        var condition = {};
                        condition.size = i;
                        
                        // Add condition html dynamically
                        var condition_html = Handlebars.compile(
                            $("#conditionHtml").html()
                        )(condition);
                        $("#condition").append(condition_html);
                    } else {
                        // Outbound
                    };    
                };                    
            }
            
            $.each($('.conditionField'), function (index, value) {
                $("#condition-" + index + " select[name='node" + index + "']").val(data.conditionList[index].nodeName);
                if (!_.isUndefined(data.conditionList[index].operator)) {
                    var operator = "";
                    switch (data.conditionList[index].operator) {
                        case ">": operator = "4";  break;
                        case "=": operator = "1";  break;
                        case "<": operator = "2";  break;
                        default:  operator = data.conditionList[index].operator; break;
                    }
                    $("#condition-" + index + " select[name='operator" + index + "']").val(operator); 
                }                
                $("#condition-" + index + " input[name='value" + index + "']").val(data.conditionList[index].value);
            });
            $("#alertSettingName").text(data.name);
            $('input[name="name"]').val(data.name); 
            $('input[name="radio-inline"][value=' + data.processingMode + ']').prop("checked", true);            
            $('input[name="remark"]').val(data.remark);
            console.log(data);
        }).fail(function(data){
            $.smallBox({
                title: i18n.t("ns:Message.MonitorSetting.MonitorSetting") + i18n.t("ns:Message.MonitorSetting.EditFail"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.MonitorSetting.EditMonitorID") + "&nbsp;" + id + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Alert Setting] Got group info failed.");
            window.location.href = "#ajax/alertSetting_list.html";            
        }).always(function(){
            $("body").removeClass("loading");
        });
    };

    var reloadJs = function(){
        var data;
        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/alertSetting_edit.html\/(\w+)$/, "$1");

        // [Slove IE issue] Kenny block at 2015/2/6
		if($.browser.msie || ($.browser.name === "Netscape")){
			event.returnValue = false;
		}else{
			event.preventDefault();
		}     
                
        if (_.isUndefined(id)){            
            $.smallBox({
                title: i18n.t("ns:Message.MonitorSetting.MonitorSetting") + i18n.t("ns:Message.MonitorSetting.GotInfoFail"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.MonitorSetting.GotInfoFail") + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Alert Setting] Got the info failed.");
            window.location.href = "#ajax/alertSetting_list.html";              
        } else {
            // Get alert setting info
            getSettingInfo(id, data);  
        }

        //Load bootstrap wizard dependency
        loadScript("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js", runBootstrapWizard);
        
        var numberOfTarget = 0;
        //Bootstrap Wizard Validations
        var runBootstrapWizard = function() {
            
            var $validator = $("#wizard-1").validate({
                rules: {
                    name:   {required: true},
                    node0:  {required: true}, operator0: {required: true}, value0: {required: true},
                    node1:  {required: true}, operator1: {required: true}, value1: {required: true},
                    node2:  {required: true}, operator2: {required: true}, value2: {required: true},
                    node3:  {required: true}, operator3: {required: true}, value3: {required: true},
                    node4:  {required: true}, operator4: {required: true}, value4: {required: true},
                    remark: {required: false}                    
                },                
                messages: {
                    name:      i18n.t("ns:MonitorSetting.Messages.SpecifyMonitorName"),
                    node0:      i18n.t("ns:MonitorSetting.Messages.SpecifyTarget"),
                    node1:     i18n.t("ns:MonitorSetting.Messages.SpecifyTarget"),
                    node2:     i18n.t("ns:MonitorSetting.Messages.SpecifyTarget"),
                    node3:     i18n.t("ns:MonitorSetting.Messages.SpecifyTarget"),
                    node4:     i18n.t("ns:MonitorSetting.Messages.SpecifyTarget"),
                    operator0:  i18n.t("ns:MonitorSetting.Messages.SpecifyOperator"),
                    operator1: i18n.t("ns:MonitorSetting.Messages.SpecifyOperator"),
                    operator2: i18n.t("ns:MonitorSetting.Messages.SpecifyOperator"),
                    operator3: i18n.t("ns:MonitorSetting.Messages.SpecifyOperator"),
                    operator4: i18n.t("ns:MonitorSetting.Messages.SpecifyOperator"),
                    value0:     i18n.t("ns:MonitorSetting.Messages.SpecifyValue"),
                    value1:    i18n.t("ns:MonitorSetting.Messages.SpecifyValue"),
                    value2:    i18n.t("ns:MonitorSetting.Messages.SpecifyValue"),
                    value3:    i18n.t("ns:MonitorSetting.Messages.SpecifyValue"),
                    value4:    i18n.t("ns:MonitorSetting.Messages.SpecifyValue"),
                    remark:    i18n.t("ns:MonitorSetting.Messages.SpecifyRemark")                    
                },
                
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            
            $("#bootstrap-wizard-1").bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                    // debugger;
                    var $valid = $("#wizard-1").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    } else {
                        // Save button event                        
                        // [Slove IE issue] Kenny block at 2015/2/6
						if($.browser.msie || ($.browser.name === "Netscape")){
							event.returnValue = false;
						}else{
							event.preventDefault();
						}                                                
                        $("body").addClass("loading");

                        var conditionList = [];
                        $.each($('.conditionField'), function (index, value) {
                            var condition = {
                                nodeName:       $("#condition-" + index + " select[name='node" + index + "']").val(),
                                operator:       $("#condition-" + index + " select[name='operator" + index + "']").val(),
                                value:          $("#condition-" + index + " input[name='value" + index + "']").val(),
                                logical:        "AND"
                            };
                            conditionList.push(condition);                            
                        });

                        var originalData = {            
                            name:           $('input[name="name"]').val(),
                            processingMode: $('input[name="radio-inline"]:checked').val(),                               
                            conditionList:  conditionList,
                            remark:         $('input[name="remark"]').val()
                        };                        
                        var data = JSON.stringify(originalData);

                        $("#wizard-1").submit(function(e)
                        {
                            $.ajax({
                                type: 'POST',                                
                                url:  $.config.server_rest_url + '/alertDefinitions/' + id ,
                                contentType: 'application/json',
                                dataType: 'json',
                                data: data,
                                timeout: 5000
                            }).done(function(data) {
                                if (_.isUndefined(data)){
                                    console.log("[Alert Setting] Added done => response undefined.");
                                } else {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.MonitorSetting.MonitorSetting") + i18n.t("ns:Message.MonitorSetting.EditSuccess"),
                                        content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Message.MonitorSetting.EditSuccess") + "</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                }                                
                            }).fail(function (jqXHR, textStatus){
                                $.smallBox({
                                    title: i18n.t("ns:Message.MonitorSetting.MonitorSetting") + i18n.t("ns:Message.MonitorSetting.EditFail"),
                                    content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Message.MonitorSetting.EditFail") + "</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                console.log("[Alert Setting] Added failed => " + textStatus);                                
                            }).always(function(){
                                $("body").removeClass("loading");          
                                window.location.href = "#ajax/alertSetting_list.html/grid";
                            });         
                            e.preventDefault(); //Prevent Default action. 
                        }); 
                        $("#wizard-1").submit(); //Submit the form                       
                    }
                }
            });            
            $('#btn_back').removeClass("disabled");            
            $('#btn_save').removeClass("disabled");
        }        
        
        // Add condition div
        $("#bootstrap-wizard-1").on("click", ".addConditionDiv", function(event) {
            if (numberOfTarget < $.config.maxAlertSettingTarget - 1) {
                var condition = {};
                condition.size = $(".conditionField").size();
                
                // Add condition html dynamically
                var condition_html = Handlebars.compile(
                    $("#conditionHtml").html()
                )(condition);
                $("#condition").append(condition_html);
                $("#condition").i18n();

                numberOfTarget ++;
            } else {
                // Outbound
            };            
        });

        // Remove condition div
        $("#bootstrap-wizard-1").on("click", ".minusConditionDiv", function(event) {
            numberOfTarget --;
            $(this).parent().remove();
            $.each($('.conditionField'), function (index, value) {
                $("#" + this.id).attr('id', "condition-" + index);
                $("#" + this.id + " .minusConditionDiv").attr('id', "actionCondition-" + index);
            });
        });

        // Back button event
        $("#btn_back").click(function(event){        
            event.preventDefault();        
            window.location.href = "#ajax/alertSetting_list.html/grid";        
        }); 

        runBootstrapWizard();       
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Reload JavaScript
        reloadJs();        

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/grid", {
                trigger: true
            });
        }

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }        
    };
    /* define bootstrap ends */
    $[AD.page] = AD;
})(jQuery);