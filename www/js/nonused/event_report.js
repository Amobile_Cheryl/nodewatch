(function($) {
    /* define namespace starts */

    var AD = {};
    AD.title = "ns:Menu.EventReport";
	AD.page = "ajax/event_report.html";
	var colorList = ["bg-color-blue", "bg-color-greenLight", "bg-color-red", "bg-color-yellow",
					 "bg-color-blueLight","bg-color-teal", "bg-color-orange", "bg-color-blueDark"];
	var reportGridView;

	function init(){
		AD.Model = new Backbone.Model();
		AD.Collection = new Backbone.Collection({
			model: AD.Model
		});
		tableRender();
		chartRender(1);
		$("#period").slider().on('slideStop', function(event){
			// API Ref:http://www.eyecon.ro/bootstrap-slider/
			var period = event.value;
			chartRender(period);
		});
	}
	function showProgressBar(dataInfo){
		var total = 0;
		$.ajax({
			type: 'GET',
			url: $.config.server_rest_url + '/devices?isCount=true',
			dataType: 'json',
			async: false,
			timeout: 5000
		}).done(function(result){
			total = result.amount;
			$("#report").empty();
			AD.Collection.reset();
			for (var i = 0; i < dataInfo.length; i++) {
				if(dataInfo[i].count!==0){
					var pg = new Backbone.ProgressBar({
						el:			$("#report"),
						id:			dataInfo[i].name,
						link:		'#ajax/device_list.html/eventSetting_list/'+dataInfo[i].name, // Todo Detail
						label:		dataInfo[i].name,
						remark:		'',
						data:		dataInfo[i].count + "/" + total,
						percentage:	(dataInfo[i].count/total*100).toFixed(2),
						color:		colorList[i%8]
					});
					var record = {};
					record.name = dataInfo[i].name;
					record.counter = dataInfo[i].count;
					AD.Collection.add(record);
				}
			}
			reportGridView.render();
		});
	}
	
	function showProgressBar_old(dataInfo){
		//AMDM API : getDeviceReportStatisticsData -> findTaskStatisticDataForAm
		var colorList = ["bg-color-blue", "bg-color-greenLight", "bg-color-red", "bg-color-yellow",
                         "bg-color-blueLight","bg-color-teal", "bg-color-orange", "bg-color-blueDark"];
		
		var total = 0;
		$.ajax({
			type: 'GET',
			url: $.config.server_rest_url + '/devices?isCount=true',
			dataType: 'json',
			async: false,
			timeout: 5000
		}).done(function(result){
			total = result.amount;
			for (var i = 0; i < dataInfo.length-1; i++) {
				var url =  $.config.server_rest_url + '/deviceEventDefinitions/' + dataInfo[i].id + '/devices?isCount=true';
				var counter = 0;
				
				$.ajax({
					type: 'GET',
					url: url,
					//data: data,
					dataType: 'json',
					async: false,
					timeout: 5000,
					error: function(data, status, error){
						console.log("[Report] Got events counter failed. => " + data.responseText);
					}
				}).done(function(data){ 
					if(_.isNumber(data)){
						counter = data;
					}else{
						// DELETE AFTER DEMO
						counter = Math.floor((Math.random() * total) + 1);
					} 
				}).fail(function(){
					// DELETE AFTER DEMO
					counter = Math.floor((Math.random() * total) + 1);
				}).always(function(){
					if(counter!==0){
						var pg = new Backbone.ProgressBar({
							el:			$("#report"),
							id:			dataInfo[i].id,
							link:		'#ajax/device_list.html/eventSetting_list/'+dataInfo[i].id,
							label:		dataInfo[i].name,
							remark:		dataInfo[i].remark,
							data:		counter + "/" + total,
							percentage:	(counter/total*100).toFixed(2),
							color:		colorList[i%8]
						});
						var record = {};
						record.name = dataInfo[i].name;
						record.counter = counter;
						AD.Collection.add(record);
					}
				});
			}
		});
	}
	
	function chartRender(period){     
        var data;
        // ���o�{��Events info.
        $.ajax({
            type: 'GET',
            //url: $.config.server_rest_url + '/deviceEventDefinitions',
			url: $.config.server_rest_url + '/eventReports/'+period,
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got events info failed. => " + data.responseText);
            }
        }).done(function(data){        
            showProgressBar(data);            
        }).fail(function(){
            //TODO          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotEventInfoFailed"),
                content: "<i class='fa fa-tasks'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotEventInfoFailed") + "</i>",
                // title: "[Dashboard] Got events info failed",
                // content: "<i class='fa fa-tasks'></i> <i>Got events info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Report] Got events info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        });        
    };
	
	function tableRender() {
		var reportGridHtml = "<table class=\"table table-striped noselect\" style=\"\">"
			+"<tbody>"
				+"<tr><th>"+i18n.t("ns:Report.EventReport.Name")+"</th><th>"+i18n.t("ns:Report.EventReport.NumberOfDevices")+"</th>"
				+"{{#each data}}"
				+"<tr><td>{{name}}</td><td>{{counter}}</td></tr>"
				+"{{/each}}"
			+"</tbody>"
		+"</table>";
		var ReportGridView = Backbone.View.extend({
			template: Handlebars.compile(reportGridHtml),
			render: function() {
				var collection = this.collection.toJSON();
				$(this.el).html(this.template({
					data: collection
				}));
				$("#reportGrid").html(this.$el);
			}
		});
		reportGridView = new ReportGridView({
			collection: AD.Collection,
			reportGridHtml: reportGridHtml
		});
		reportGridView.render();
	}
	
    /* define bootstrap starts */
    AD.bootstrap = function() {

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        Backbone.emulateHTTP = true;
        Backbone.history.start();
		$("body").removeClass("loading");
		
		init();
    };
    /* define bootstrap ends */

	$[AD.page] = AD;


})(jQuery);