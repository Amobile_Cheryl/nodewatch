(function($) {        
    var AD = {};
    AD.page = "ajax/app_upload_windows.html";
    
    var uploadIcon = function() {
        // Icon
        var result = $('#result');
        var currentFile;
        var replaceResults = function(img) {
            var content;
            if (!(img.src || img instanceof HTMLCanvasElement)) {
                content = $('<span>Loading image file failed</span>');
            } else {
                content = $('<a target="_blank">').append(img)
                    .attr('download', currentFile.name)
                    .attr('href', img.src || img.toDataURL());
            }
            result.children().replaceWith(content);
            $("#appearance").val(content[0].href);
        };
        var displayImage = function(file, options) {
            currentFile = file;
            if (!loadImage(
                file,
                replaceResults,
                options
            )) {
                result.children().replaceWith(
                    $('<span>Your browser does not support the URL or FileReader API.</span>')
                );
            }
        };
        var dropChangeHandler = function(e) {
            e.preventDefault();
            e = e.originalEvent;
            var target = e.dataTransfer || e.target,
                file = target && target.files && target.files[0],
                options = {
                    maxWidth: result.width(),
                    canvas: true
                };
            if (!file) {
                return;
            }
            loadImage.parseMetaData(file, function(data) {
                displayImage(file, options);
            });
        };
        $("#appearanceInput").on("change", dropChangeHandler);
    };

    var reloadJs = function(){        
        // Load bootstrap wizard dependency        
        loadScript("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js", runBootstrapWizard);
        // Upload an app icon
        uploadIcon();
        // Bootstrap Wizard Validations
        var runBootstrapWizard = function() {
            
            var $validator = $("#wizard-1").validate({
            
                rules: {
                    name: {
                        required: true
                    },
                    fileVersion: {
                        required: true
                    },
                    /*insatllPath: {
                        required: true
                    },*/
                    label: {
                        required: true
                    },                    
                    remark: {
                        required: false
                    },
                    appearanceFile: {
                        required: true
                    },
                    uploadApp: {
                        required: true
                    }
                },
                
                messages: {
                    name:           i18n.t("ns:File.App.Message.SpecifyFileProcessName"),
                    fileVersion:    i18n.t("ns:File.App.Message.SpecifyFileVersion"),
                    //insatllPath:    "Please specify your file insatll path",
                    label:          i18n.t("ns:File.App.Message.SpecifyFileName"),
                    remark:         "Please specify your remark",
                    // remark:         "Please specify your remark",
                    appearanceFile: "Please specify your icon to upload",
                    uploadApp:      i18n.t("ns:File.App.Message.SpecifyAppToUpload")
                },
                
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $("#bootstrap-wizard-1").bootstrapWizard({
                'tabClass': 'form-wizard',                
                'onNext': function (tab, navigation, index) {
                    // debugger;
                    var $valid = $("#wizard-1").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    } else {
                        // Save button event                        
                        // [Slove IE issue] Kenny block at 2015/2/6
						if($.browser.msie || ($.browser.name === "Netscape")){
							event.returnValue = false;
						}else{
							event.preventDefault();
						}                                               
                        // var originalData = {            
                        //     name:           $('input[name="name"]').val() || [],
                        //     fileVersion:    $('input[name="fileVersion"]').val() || [],
                        //     insatllPath:    $('input[name="insatllPath"]').val() || [],
                        //     procName:       $('input[name="procName"]').val() || [],
                        //     remark:         $('input[name="remark"]').val() || [],
                        //     appearanceFile: $('input[name="appearanceFile"]').val() || [],                    
                        //     uploadApp:      $('input[name="uploadApp"]').val() || []
                        // };
                        // var data = JSON.stringify(originalData);                    

                        $("body").addClass("loading");
                        $("#wizard-1").submit(function(e)
                        {                                                     
                            var formData = new FormData(this);
                            $.ajax({
                                url:  $.config.server_host + '/upload' ,
                                type: 'POST',
                                data:  formData,
                                mimeType:"multipart/form-data",
                                dataType: 'json',
                                contentType: false,
                                cache: false,
                                processData:false,
                                //timeout: 25000,
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.log("[Windows App] Uploaded failed");
                                }                             
                            }).done(function(data) {
                                if (typeof(data) === 'undefined'){
                                    console.log("[Windows App] Uploaded done => response undefined.");
                                } else {
                                    if (data.result) {
                                        $.smallBox({
                                            title: i18n.t("ns:Message.App.WindowsApp") + i18n.t("ns:Message.App.UploadSuccess"),
                                            content: "<i class='fa fa-cloud-upload'></i> <i>"+ i18n.t("ns:Message.App.UploadSuccess") + "</i>",
                                            // title: "[Windows App] Uploaded successfully",
                                            // content: "<i class='fa fa-cloud-upload'></i> <i>Uploaded successfully</i>",
                                            color: "#648BB2",
                                            iconSmall: "fa fa-cloud-upload fa-2x fadeInRight animated",
                                            timeout: 5000
                                        });
                                    } else {                                            
                                        $.smallBox({
                                            title: i18n.t("ns:Message.App.WindowsApp") + i18n.t("ns:Message.App.UploadFailed"),
                                            content: "<i class='fa fa-cloud-upload'></i> <i>"+ i18n.t("ns:Message.App.UploadFailed") + "</i>",
                                            // title: "[Windows App] uploaded failed",
                                            // content: "<i class='fa fa-cloud-upload'></i> <i>Uploaded failed</i>",
                                            color: "#B36464",
                                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                            timeout: 5000
                                        });
                                        console.log("[Windows App] Uploaded failed => " + JSON.stringify(data));
                                    };
                                }                                                                    
                            }).fail(function (jqXHR, textStatus){
                                $.smallBox({
                                    title: i18n.t("ns:Message.App.WindowsApp") + i18n.t("ns:Message.App.UploadFailed"),
                                    content: "<i class='fa fa-cloud-upload'></i> <i>"+ i18n.t("ns:Message.App.UploadFailed") + "</i>",
                                    // title: "[Windows App] uploaded failed",
                                    // content: "<i class='fa fa-cloud-upload'></i> <i>Uploaded failed</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                console.log("[Windows App] Uploaded failed => " + textStatus);
                            }).always(function(){
                                // debugger;
                                $(".loadingModal").empty();
                                $("body").removeClass("loading");     
                                window.location.href = "#ajax/app_list.html/grid/u1";
                            });         
                            e.preventDefault(); //Prevent Default action. 
                            // e.unbind();
                        }); 
                        $("#wizard-1").submit(); //Submit the form

                        $(".loadingModal").empty();
                        $("body").addClass("loading");
                        $(".loadingModal").append('<div id="progressDiv" class="progress progress-striped active" style="width: 250px"><div id="progressBar" class="progress-bar bg-color-greenLight" role="progressbar" style="width: 0%"></div></div>');
                        $("#progressDiv").css("top","58%").css("position","relative").css("margin-left","auto").css("margin-right","auto");                
                        getUploadProgress();        
                    }
                }
            });
            $('#btn_back').removeClass("disabled");            
            $('#btn_save').removeClass("disabled");            
        }

		runBootstrapWizard(); 

        // Back button event
        $("#btn_back").click(function(event){        
            event.preventDefault();        
            window.location.href = "#ajax/app_list.html/grid/u1";        
        });      
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
		
        // Translate Input of the placeholder
        translatePlaceholder();
        
        // Reload JavaScript
        reloadJs();
		
        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/app_upload_windows.html\/(\w+)$/, "$1");

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/" + id, {
                trigger: true
            });
        }

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");
		
		$("#uploadApp").change(function(event){
			$("#customFileInput span").text(this.files[0].name);
		});

    };
    /* define bootstrap ends */
    $[AD.page] = AD;

    // Add progress bar
    function uploadProgressHandle(uploadProgressResult) {
        try
        {                        
            var $xml = $(uploadProgressResult);
            var isNotFinished       = $xml.find("finished");
            var uploadBytesRead     = $xml.find("bytes_read").text();
            var uploadContentLength = $xml.find("content_length").text();
            var uploadPercent       = $xml.find("percent_complete").text();
            // console.log("===[1]===");
            if ((isNotFinished.text() == "") && (uploadPercent == "") && (isNotFinished.length == 0)) {                            
                // console.log("===[2]===");
                // console.log(uploadProgressResult);                
                setTimeout(getUploadProgress, 100);
            } else {
                if (uploadPercent != "") {
                    // console.log("===[3]===");
                    // console.log(uploadProgressResult);
                    console.log("Upload", uploadPercent + "% " + uploadBytesRead + " of " + uploadContentLength + " bytes read");
                    $("#progressBar").attr("style","width: " + uploadPercent + "%");
                    // document.getElementById('progressBar').innerHTML = "50%";
                    // $("#progressBar").attr("style","width: 50%");
                    setTimeout(getUploadProgress, 100);
                } else {
                    console.log(uploadProgressResult);            
                    // console.log("===[4]===");
                    if ($(".loading").length != 0) {
                        $("#progressBar").attr("style","width: 100%");
                        setTimeout(function() {
                            // $("#progressBar").text("Processing...");
                            $("#progressBar").text(i18n.t("ns:Message.Processing"));
                        }, 2000);
                    };
                }
            }
        } catch (e) {
            console.log(e.message);
        }
    };

    // Get upload progress
    function getUploadProgress() {
        var url = $.config.server_host + "/upload";
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'xml',
                crossDomain: true,
            success: function(response){
                uploadProgressHandle(response);
            },
            error: function(jqXHR, textStatus){
                $.smallBox({
                    title: i18n.t("ns:Message.App.WindowsApp") + i18n.t("ns:Message.App.UploadFailed"),
                    content: "<i class='fa fa-cloud-upload'></i> <i>"+ i18n.t("ns:Message.App.UploadFailed") + "</i>",
                    // title: "[Windows App] Uploaded failed",
                    // content: "<i class='fa fa-cloud-upload'></i> <i>Uploaded failed.</i>",
                    color: "#B36464",
                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                    timeout: 5000
                });
                console.log("[Windows App] getUploadProgress failed => " + textStatus);
                $(".loadingModal").empty();
                $("body").removeClass("loading");                              
                window.location.href = "#ajax/app_list.html/grid/u1";
            }
        });
    }

    // Translate Input of the placeholder
    function translatePlaceholder () {
        $("#label").attr("placeholder", i18n.t("ns:File.App.Message.Name"));
        $("#fileVersion").attr("placeholder", i18n.t("ns:File.App.Message.Version"));
        $("#installPath").attr("placeholder", i18n.t("ns:File.App.Message.InstallPath"));
        $("#name").attr("placeholder", i18n.t("ns:File.App.Message.ProcessName"));
        $("#remark").attr("placeholder", i18n.t("ns:File.App.Message.WinRemark"));
    }
})(jQuery);
