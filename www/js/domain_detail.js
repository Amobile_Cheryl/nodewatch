(function($) {
	/* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');

    var AD = {};
    AD.page = "ajax/domain_detail.html";

    function s1(id) 
    {
		/* define date format*/
        df = new DateFormat();
        df.setFormat('yyyy-MMM-dd HH:mm:ss');
        /*                  */
        
        /* define namespace starts */
        var hash = window.location.hash;
        
        AD.collections = {};    
        var url = $.config.server_rest_url + '/domains/' + id + '/devices';
        var idAttribute = AD.idAttribute = "id";
        AD.title = "ns:Menu.DeviceList";
        AD.isAdd = false;

        AD.validate = function() {
            return true;
        };

        AD.columns = [
            {
                title: '',
                sortable: false,
                width: '60px',
                callback: function(o) {
                    return '<img src="'+o.deviceTypeIconUrl+'"" style="height:30px; margin-top:-6px; margin-bottom:-6px;" />';
                }
            },
            {
                title: 'ns:Device.Title.Status',
                filterable: false,
                sortable: false,
                width: '80px',
                callback: function(o) {
                    var isLock;
                    //isLock = (_.isUndefined(o.isLock)) ? "" : "<i class=\"fa fa-lock\" title=\""+i18n.t("ns:Device.Message.Lock")+"\"></i>";
                    if (o.hasOwnProperty('isLock') == true)
                        isLock = o.isLock ? "<i class=\"fa fa-lock txt-color-red\" title=\""+i18n.t("ns:Device.Message.Lock")+"\"></i>" : "<i class=\"fa fa-unlock txt-color-blueLight\" title=\""+i18n.t("ns:Device.Message.Unlock")+"\"></i>";
                    else
                        isLock = "<i class=\"fa fa-unlock txt-color-blueLight\" title=\""+i18n.t("ns:Device.Message.Unlock")+"\"></i>";

                    var isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:gray;\" title=\""+i18n.t("ns:Message.Device.Offline")+"\"></i>";
                    if (!_.isUndefined(o.isOnline)) {
                        if(o.isOnline){
                            isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:lawngreen;\" title=\""+i18n.t("ns:Message.Device.Online")+"\"></i>";
                        }
                    }
                    //var isJailbreak = (_.isUndefined(o.isJailbreak)) ? "" : "<i class=\"fa fa-bug\" style=\"color:crimson;\" title=\""+i18n.t("ns:Message.Device.Jailbreak")+"\"></i>";
                    //return isLock+isJailbreak;
                    //return isJailbreak;
                    return isLock + "&nbsp;" + isOnlineStatus;
                }
            },
            {
                title: 'ns:Device.Title.Label',
                property: 'label',
                filterable: true,
                searchName: 'label',
                sortable: true,
                width: '200px',
                callback: function(o) {
                    if (o.provisionStatus === "Provisioned") {
                        return '<a href="#ajax/device_detail.html/' + o.id + '" title="Check the device detail">' + o.label + '</a>';
                    } else {
                        return o.label;
                    }
                }
            },           
            //column2
            {
                title: 'IMEI & MAC',
                property: 'name',
                filterable: true,
                searchName: 'name',
                sortable: true,
                width: '200px'
            },
            {
                title: 'ns:Device.Title.DeviceType',
                cellClassName: 'deviceTypeName',
                filterable: true,
                searchName: 'deviceTypeName',
                width: '150px',
                searchDom : function(){
                    var options = '<option value selected="selected">'+i18n.t("ns:Device.Title.DeviceType")+'</option>';
                    $.ajax({
                        url: $.config.server_rest_url+'/deviceTypes',
                        dataType: 'JSON',
                        async: false,
                        success: function(deviceTypeCollection){
                            for(var i=0;i<deviceTypeCollection.length-1;i++){
                                options+='<option>'+deviceTypeCollection[i].name+'</option>';
                            }
                        }
                    });
                    return '<select class="form-control" name="deviceType" placeholder="'+i18n.t("ns:Device.Title.DeviceType")+'">'+options+'</select>';
                },
                sortable: false,
                callback: function(o) {
                    return o.deviceTypeName;
                }
            },
            //column6
            {
                title: 'ns:Device.Title.LastReport',
                property: 'latestConnectTime',
                width: '150px',
                cellClassName: 'latestConnectTime',
                filterable: false,
                sortable: true,
            },
        ];

        AD.buttons = [
			{
				"sExtends": "text",
				"sButtonText" : "<i class='fa fa-edit'></i>&nbsp; " + i18n.translate("ns:Domain.Edit"),
				"sButtonClass": "txt-color-white bg-color-blue",
				"fnClick": function(nButton, oConfig, oFlash) {
					var hash = window.location.hash;
					var id = hash.replace(/^#ajax\/domain_detail.html\/(\w+)$/, "$1");
					window.location.href = "#ajax/domain_member_list.html/" + id;
				}
			},
        ];

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });

        /* collection ends */
		var gv = $("#domain_member .cctech-gv");
		
		if(!MemberGrid){
			var MemberGrid = new Backbone.GridView({
				el: gv,
				collection: new AD.Collection(),
				columns: AD.columns,
				title: AD.title,
				AD: AD,
				sort: AD.sort,
				buttons: AD.buttons
			});
		}
		MemberGrid.refresh();
    };
	
	var routes = {};
	routes[AD.page + '/:_id'] = 'render';
	var Router = Backbone.Router.extend({
		routes: routes,
		render: function(id){
		}
	});
	
    AD.bootstrap = function() 
    {
		devicesLocation = undefined;
		i18n.init(function(t){
			$('[data-i18n]').i18n();
		});
		$("body").removeClass("loading");
	
        var router = new Router;
		if (!Backbone.History.started) {
			Backbone.emulateHTTP = true;
			Backbone.history.start();
		}
		
		var hash = window.location.hash;
        var id = hash.split('/')[2];
        s1(id);
        if (!_.isUndefined(id)) {
        	$.ajax({
				url: $.config.server_rest_url+'/domains/'+id,
				type: 'GET',
				dataType: 'json',
				timeout: 10000,
				error: function(data, status, error){
				}
			})
			.done(function(domain) 
			{
				$("#domainName font").text(domain.name);
				$("#domainName").attr("href","#"+AD.page+"/"+domain.id);
			});	
        };
    };
    $[AD.page] = AD;
})(jQuery);
