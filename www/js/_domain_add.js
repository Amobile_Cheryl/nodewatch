angular.module('app', [ 'multi-select' ])

.controller('appCtrl',  function($scope){
    $scope.cars = [{id:1, name: 'Audi',   ticked: false }, {id:2, name: 'BMW',   ticked: false }, {id:1, name: 'Honda',   ticked: true}];
    $scope.selectedCar = [];

    $scope.fruits = [{id: 1, name: 'Apple'}, {id: 2, name: 'Orange'},{id: 3, name: 'Banana'}];
    $scope.selectedFruit = null;
})