(function($) {
    var AD = {};
    AD.page = "ajax/group_member_list.html";

    var reloadJs = function(){
        /* define date format*/
        df = new DateFormat();
        df.setFormat('yyyy-MMM-dd HH:mm:ss');
        /*                  */
        
        /* define namespace starts */
        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/group_member_list.html\/(\w+)$/, "$1");
        
        AD.collections = {};    
        var url = $.config.server_rest_url + '/deviceGroups/' + id + '/devices';
        var idAttribute = AD.idAttribute = "id";
        AD.title = "ns:Group.Members";
        AD.isAdd = false;

        AD.validate = function() {
            return true;
        };

        AD.columns = [
            //column1
            {
                title: 'ns:Device.Title.ID',
                property: 'name',
                filterable: false,
                sortable: false,
                callback: function(o) {
                    if (o.provisionStatus === "Provisioned")
                        return '<a href="#ajax/device_detail.html/' + o.id + '" title="Check the device detail">' + o.name + '</a>';
                    else
                        return o.name;
                    // return '<a href="#ajax/group_edit.html/' + o.cwmpId + '">' + o.cwmpId + '</a>';
                }
            },
            //column2
            /*{
                title: 'Device Type',
                cellClassName: 'deviceTypeName',
                filterable: false,
                callback: function(o) {
                    // if (o.deviceTypeId == 3)
                    //     return "<img src='img/devicetype/phone.png'>" + $.config.DeviceType[o.deviceTypeId];
                    // else
                        return "<img src='img/devicetype/phone.png'>" + o.deviceTypeName;
                }
            },*/
            //Column3
            {
                title: 'Firmware Version',
                property: 'firmwareId',
                filterable: false,
                sortable: true,
            },
            //column4
            {
                title: 'ns:Device.Title.ConnectionStatus',
                property: 'latestConnectTime',
                cellClassName: 'latestConnectTime',
                filterable: false,
                sortable: true,
                callback: function(o) {
                    /*change millisecond to date format*/
                    var isOnlineStatus = "--";
                    if (!_.isEmpty(o.latestConnectTime)) {
                        isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:gray;\" title=\""+i18n.t("ns:Message.Device.Offline")+"\"></i>";
                        if (!_.isUndefined(o.isOnline)) {
                            if(o.isOnline){
                                isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:lawngreen;\" title=\""+i18n.t("ns:Message.Device.Online")+"\"></i>";
                            }
                        }
                    }
                    return isOnlineStatus+o.latestConnectTime;
                },
            },
            //column5
            {
                title: 'ns:Device.Title.Domain',
                property: 'domainId',
                filterable: false,
                sortable: false,
                visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
                callback: function(o) {
                    //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId] ;
                    return (typeof(o.domainName) === "undefined") ? '' : o.domainName;
                }
            }
        ];
        /* define namespace ends */

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
    };

    /* collection ends */
    var routes = {};
    routes[AD.page + '/:_id'] = 'show_grid';
    routes[AD.page + '/edit/:_id'] = 'show_edit';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        initialize: function(opt) {
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            // console.error();
        },                    
        show_grid: function() {
            var t = this;
            // init GridView
            if (!t.gv) {
                t.gv = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: !_.isUndefined(t.AD.isAdd) ? t.AD.isAdd : true,
                    sort: t.AD.sort,
                    filter: t.AD.filter,
                    buttons: t.AD.buttons,
                    AD: t.AD
                });                
            } else
                t.gv.refresh();


            t.$fv.hide();
            t.$gv.show();

            pageSetUp();
        }

    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
		$("body").removeClass("loading");
        // Reload JavaScript
        reloadJs();

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/group_member_list.html\/(\w+)$/, "$1");

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/" + id, {
                trigger: true
            });
        }

        if (!_.isUndefined(id)) {
            $.ajax({
                url: $.config.server_rest_url+'/deviceGroups/'+id,
                type: 'GET',
                dataType: 'json',
                timeout: 10000,
                error: function(data, status, error){
                }
            }).done(function(data) {
                $("#groupName font").text(data.name);
                $("#groupName").attr("href","#"+AD.page+"/"+id);
            }).fail(function(){
            }); 
        };

        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
    };
    /* define bootstrap ends */
    $[AD.page] = AD;
    $[AD.page].bootstrap();

})(jQuery);
