(function($) {        
    var AD = {};
    AD.page = "ajax/group_edit.html";
    /*
    var multiselectInit = function() {
        $('.multiselect').multiselect({
            enableFiltering : true,
            includeSelectAllOption: true,
            maxHeight:$('body').height()-545,
            buttonWidth:'auto',
            numberDisplayed: 0
        });
        $('#get-selected').on('click', function() {
            var values = [];

            $('option:selected', $('.multiselect')).each(function() {
                values.push($(this).val());
            });
        });
    };
    */
    var getGroupInfo = function(id, data) {
        $("body").addClass("loading");

        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/deviceGroups/' + id,
            data: data,
            dataType: 'json',
            timeout: 15000,
            async: false,
            error: function(data, status, error){
                console.log("[Group edit] Got group info failed. => " + data.responseText);                
            }
        }).done(function(data){
			$("#deviceGroupName").text(data.name);
            $('input[name="groupname"]').val(data.name);
            $('input[name="operatingSystem"]').val(data.operatingSystem);
            $('input[name="remark"]').val(data.remark); 
            // deviceIdSet = data.deviceIdSet;
            console.log(data);       
        }).fail(function(){
            $.smallBox({
                title: i18n.t("ns:Message.Group.GroupEdit") + i18n.t("ns:Message.Group.GotGroupInfoFailed"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.EditGroupID") + "&nbsp;" + id + "</i>",
                // title: "[Group edit] Got group info failed",
                // content: "<i class='fa fa-pencil-square-o'></i> <i>Edited Group ID: &nbsp;" + id + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Group edit] Got group info failed.");
            window.location.href = "#ajax/group_list.html";            
        }).always(function(){
            $("body").removeClass("loading");
        });
    };

  //   var getDevices = function(id, data) {
  //       var operatingSystem = $("#operatingSystem").val();
  //       if (typeof(id) === 'undefined') {
  //           var url = $.config.server_rest_url + "/devices";
  //       } else {
  //           var url = $.config.server_rest_url + "/devices?operatingSystemName=" + operatingSystem;
  //       }
		// // ?page=1&size=20&

  //       $.ajax({
  //           type: 'GET',
  //           url: url,
  //           data: data,
  //           timeout: 20000,
  //           async: false,
  //           error: function(data, status, error){
  //               console.log("[Group edit] Got device list failed. => " + data.responseText);                
  //           }
  //       }).done(function(data){
  //           //動態產生Device的現有清單        
  //           var devices = new Array();
  //           var label;
  //           for (var i = 0; i < data.length -1; i ++){
  //               (typeof(data[i].deviceTypeName) === "undefined" || data[i].deviceTypeName === null || data[i].deviceTypeName === "")
  //               ? (label = data[i].name)
  //               : (label = data[i].name + " (" + data[i].deviceTypeName + ")");

  //               var obj = {
  //                   value: data[i].id,
  //                   label: label
  //               };
  //               devices.push(obj);
  //           }            
  //           $("#deviceSelect").multiselect('dataprovider', devices);                
  //           $("#deviceSelect").multiselect('select', deviceIdSet);
  //       }).fail(function(){
  //           $.smallBox({
  //               title: i18n.t("ns:Message.Group.GroupEdit") + i18n.t("ns:Message.Group.GotDeviceListFailed"),
  //               content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.EditGroupID") + "&nbsp;" + id + "</i>",
  //               // title: "[Group edit] Got device list failed",
  //               // content: "<i class='fa fa-pencil-square-o'></i> <i>Edited Group ID: &nbsp;" + id + "</i>",
  //               color: "#B36464",
  //               iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
  //               timeout: 5000
  //           });
  //           console.log("[Group edit] Got device list failed.");
  //           window.location.href = "#ajax/group_list.html";            
  //       }).always(function(){
  //           $("body").removeClass("loading");
  //       });
  //   };

    var putGroup = function(id, data) {
        if (id != null) {
            $.ajax({
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-HTTP-Method-Override', 'PUT');
                },
                type: 'POST',
                crossDomain: true,
                url: $.config.server_rest_url + '/deviceGroups/' + id,            
                contentType: 'application/json',
                data: data,
                timeout: 10000,
                error: function(data, status, error){
                    console.log("[Group edit] POST failed. => " + data.responseText);
                }
            }).done(function(data){
                $.smallBox({
                    title: i18n.t("ns:Message.Group.GroupEdit") + i18n.t("ns:Message.Group.EditedSuccess"),
                    content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.EditGroupID") + "&nbsp;" + id + "</i>",
                    // title: "[Group edit] Edited successfully",
                    // content: "<i class='fa fa-pencil-square-o'></i> <i>Edited Group ID:"+"&nbsp;" + id + "</i>",
                    color: "#648BB2",
                    iconSmall: "fa fa-pencil-square-o fa-2x fadeInRight animated",
                    timeout: 5000
                });
            }).fail(function(){
                $.smallBox({
                    title: i18n.t("ns:Message.Group.GroupEdit") + i18n.t("ns:Message.Group.EditedFailed"),
                    content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.EditGroupID")+"&nbsp;" + id + "</i>",
                    // title: "[Group edit] Edited failed",
                    // content: "<i class='fa fa-pencil-square-o'></i> <i>Edited Group ID: &nbsp;" + id + "</i>",
                    color: "#B36464",
                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                    timeout: 5000
                });                   
            }).always(function(){
                window.location.href = "#ajax/group_list.html";
				//window.history.back();
            });
        }
    };

    var reloadJs = function(){
        var data;
        var deviceIdSet = [];
        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/group_edit.html\/(\w+)$/, "$1");

        //event.preventDefault();        
                
        if (typeof(id) === 'undefined'){
            console.log("[Group edit] Got group info failed.");
            $.smallBox({
                title: i18n.t("ns:Message.Group.GotGroupInfoFailed"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.EditGroupID") + "&nbsp;" + data.name + "</i>",
                // title: "Got group info failed",
                // content: "<i class='fa fa-pencil-square-o'></i> <i>Edited Group ID: &nbsp;" + data.name + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            window.location.href = "#ajax/group_list.html";              
        } else {
            // 取得Group name, operatingSystem, remark and deviceIdSet
            getGroupInfo(id, data);    
            // HTTP GET 取得Device的現有清單                        
            //getDevices(id, data);
        }  

        // Back button event
        $("#btn_back").click(function(event){        
            event.preventDefault();        
            window.location.href = "#ajax/group_list.html";
			// window.history.back();
        });

        // Save button event
        $("#btn_save").click(function(event){                
            event.preventDefault();

            var originalData = {            
                remark: $('input[name="remark"]').val() || ""                    
                // ,deviceIdSet: $('#deviceSelect').val() || []
            };
            var data = JSON.stringify(originalData);

            // HTTP PUT 修改Group
            putGroup(id, data);
        });
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
		$("body").removeClass("loading");

        // Translate Input of the placeholder
        translatePlaceholder();

        // multiselect initial
        // multiselectInit();

        // Reload JavaScript
        reloadJs();        

        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/group_edit.html\/(\w+)$/, "$1");

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/" + id, {
                trigger: true
            });
        }

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
    };
    /* define bootstrap ends */
    $[AD.page] = AD;

    function translatePlaceholder () {
        $("#remark").attr("placeholder", i18n.t("ns:Group.Message.Remark"));
    }

})(jQuery);
