(function($) {
    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /*                  */

    var AD = {};
    AD.title = "Task Detail";
    AD.page = "ajax/subtask_list.html";
    
	var adReload = function(id) {
		showProgress(id);
		AD.collections = {};
		AD.buttons = {};
		AD.columns = [
			{
				title: i18n.t("ns:Group.ID"),
				property: 'id',
				width: '80px',
				filterable: false,
				sortable: true
			},
			{
				title: i18n.t("ns:Group.DeviceName"),
				//property: 'deviceId',
				filterable: false,
				sortable: false,
				callback: function(o) {
					if (_.isNull(o.deviceName)) {
						return i18n.t("ns:Group.Message.RecordDelete");
					} else {
						if (o.provisionStatus == "Provisioned") {
							return '<a href="#ajax/device_detail.html/' + o.deviceId + '">' + o.deviceName + '</a>';
						} else {
							return o.deviceName;
						}
					}
				}
			},
			{
				title: i18n.t("ns:Group.Status"),
				property: 'state',
				filterable: false,
				sortable: false,
				callback: function(o) {
					var color = "#919191";
					color = eval('$.config.ui.subtaskExecutionStatusColor.'+o.state.toLowerCase());
					return "<i class=\"fa fa-circle\" style=\"color:"+color+";\"></i> "+o.state;
				}
			},
			{
				title: i18n.t("ns:Group.LastUpdateTime"),
				property: 'finishTime',
				callback: function(o) {
					return o.finishTime;
				},
				filterable: false,
				sortable: true
			},
			{
				title: i18n.t("ns:Group.Remark"),
				property: 'remark',
				filterable: false,
				sortable: false
			}
		];
		/* define namespace ends */
		var url = $.config.server_rest_url+'/tasks/'+id+'/subtasks';
		var idAttribute = AD.idAttribute = "id";
		AD.isAdd = false;

		/* model starts */

		AD.Model = Backbone.Model.extend({
			urlRoot: url,
			idAttribute: idAttribute
		});

		/* model ends */

		/* collection starts */

		AD.Collection = Backbone.Collection.extend({
			url: url,
			model: AD.Model
		});
		
		var gvs = ".cctech-gv";
		var gv = $(gvs).first();
		
		if(!subtaskGrid){
			var subtaskGrid = new Backbone.GridView({
				el: gv,
				collection: new AD.Collection(),
				columns: AD.columns,
				title: 'Subtasks',
				AD: AD
			});
		}else{
			subtaskGrid.refresh();
		}
		pageSetUp();
	};

	/* collection ends */

	function showProgress(id){
		var tpl_taskInfo_html = "<table class=\"table table-striped\" style=\"clear: both\">"+
            "<thead><tr role=\"row\">" +
                "<th style=\"cursor:pointer; width: 120px;font-size: 13px;background-color: #A6CD95;\"><i class=\"fa fa-lg fa-chevron-circle-left\"></i>&nbsp;<font data-i18n=\"ns:Group.BackToList\">Back to list</font></th>" +
                "<th></th></tr></thead>" + 
            "<tbody>" +
    		"<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.ID\"></font></td><td>{{id}}</td></tr>"+
    		"<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.Name\"></font></td><td>{{job}}</td></tr>"+
    		"<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.SubmitTime\"></font></td><td>{{creationTime}}</td></tr>"+
    		"<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.Status\"></font></td><td>{{displayExecuteStatus executeStatus}}</td></tr>"+
    		"<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.Remark\"></font></td><td>{{remark}}</td></tr>"+
    		"<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.Creator\"></font></td><td>{{creatorName}}</td></tr>"+
    		//"<tr><td>Action:<i class=\"fa fa-refresh\"></i>Redo</td></tr>"+
            "</tbody>" +
    	"</table>";
		
		Handlebars.registerHelper("displayExecuteStatus", function(executeStatus){
			var html = "";
			var color = "#919191";
			color = eval('$.config.ui.taskExecutionStatusColor.'+executeStatus.toLowerCase());
			html = "<i class=\"fa fa-circle\" style=\"color:"+color+";\"></i> " + taskExecuteStatusDisplay(executeStatus);
			return new Handlebars.SafeString(html);
		});
		var taskInfo_template = Handlebars.compile(tpl_taskInfo_html);
		
		// PieChart
		var sliceColors = new Array();
		$.ajax({
			url: $.config.server_rest_url+'/tasks/'+id+'/taskExecutionStatus',
			type: 'GET',
			dataType: 'json',
			async: false
		}).done(function(data) {
			switch (data.job) {
                case "EnableDisableCapability":
                case "EnableCapability":
                case "DisableCapability":
                    data.job = i18n.t("ns:Log.Task.TaskName.SwitchCapabilitySetting");
                    break;
                case "LockDevice":
                    data.job = i18n.t("ns:Log.Task.TaskName.LockDevice");
                    break;
				case "UnlockDevice":
                    data.job = i18n.t("ns:Log.Task.TaskName.UnlockDevice");
                    break;
                case "ActivateApp":
                    data.job = i18n.t("ns:Log.Task.TaskName.ActivateApp");
                    break;
                case "DeactivateApp":
                    data.job = i18n.t("ns:Log.Task.TaskName.DeactivateApp");
                    break;
                case "DownloadAndInstallApp":
                    data.job = i18n.t("ns:Log.Task.TaskName.InstallApp");
                    break;
                case "RemoveApp":
                    data.job = i18n.t("ns:Log.Task.TaskName.RemoveApp");
                    break;
                case "UpgradeApp":
                    data.job = i18n.t("ns:Log.Task.TaskName.UpgradeApp");
                    break;
                case "setAllowUserInstallApp":
                    data.job = i18n.t("ns:Log.Task.TaskName.AllowInstallApp");
                    break;
                case "autoRemoveLocalAPP":
                    data.job = i18n.t("ns:Log.Task.TaskName.AutoRemoveLocalApp");
                    break;
				case "UpgradeFirmware":
                    data.job = i18n.t("ns:Log.Task.TaskName.FirmwareUpgrade");
                    break;
                case "setDeviceEventList":
                    data.job = i18n.t("ns:Log.Task.TaskName.SetDeviceEventList");
                    break;
                case "setRule":
                    data.job = i18n.t("ns:Log.Task.TaskName.SetRule");
                    break;
                case "deleteRule":
                    data.job = i18n.t("ns:Log.Task.TaskName.DeleteRule");
                    break;
                case "editRule":
                    data.job = i18n.t("ns:Log.Task.TaskName.EditRule");
                    break;
                case "activeCapabilityEvent":
                    data.job = i18n.t("ns:Log.Task.TaskName.ActivateCapabilityEvent");
                    break;
                case "deleteAllRuleAndEvent":
                    data.job = i18n.t("ns:Log.Task.TaskName.DeleteRuleEvent");
                    break;
                case "Reset":
                    data.job = i18n.t("ns:Log.Task.TaskName.RebootDevice");
                    break;
                case "Restart":
                    data.job = i18n.t("ns:Log.Task.TaskName.RestartDevice");
                    break;                                        
                case "applyManagementPolicy":
                    data.job = i18n.t("ns:Log.Task.TaskName.ApplyManagementPolicy");
                    break;      
                case "AddAppToBlacklist":
                    data.job = i18n.t("ns:Log.Task.TaskName.SetAppBlacklist");
                    break;   
                case "EnableTriggerServerConnection":
                    data.job = i18n.t("ns:Log.Task.TaskName.EnableTrigger");
                    break;
                case "DisableTriggerServerConnection":
                    data.job = i18n.t("ns:Log.Task.TaskName.DisableTrigger");
                    break;   					
                default:
                    return "Unknown";
                    break;
            }

			var taskInfo_html = taskInfo_template(data);
			$("#task_info").empty();
			$("#task_info").append(taskInfo_html);
			$("#taskName").text(data.job);
			// Clear Old
			$("#subtask_status").empty();
			// Render
			var taskData = new Array();
			var taskExecutionStatus = {};
			taskExecutionStatus.names = {};
			var i = 0;
			$.each(data.executeStatusMap,function(key,value){
				if(key.toLowerCase() !== "total"){
					taskData.push(value);
					eval('taskExecutionStatus.names['+i+']="'+key+'";');
					sliceColors.push(eval('$.config.ui.subtaskExecutionStatusColor.'+key.toLowerCase()));
					i++;
				}
			});
			$("#subtask_status").append("<div class=\"col col-sm-5 col-md-5 col-lg-5\"><div id=\"taskExecutionStatusChart\"></div></div>");
			$("#taskExecutionStatusChart").sparkline(taskData, {
				type: 'pie',
				width: '128px',
				height: '128px',
				//borderWidth: '5',
				//borderColor: '#3E3E3E',
				sliceColors: sliceColors,
				tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)',
				tooltipValueLookups: {
					names: taskExecutionStatus.names
				}
			});
			$("#subtask_status").append("<div class=\"col col-sm-7 col-md-7 col-lg-7\"><ul></ul></div>");
			$("#subtask_status").css("margin","10px");
			$("#subtask_status ul").css({"list-style":"none"});
			for(var i=0;i<taskData.length;i++) {
				//console.log(taskExecutionStatus.names[i]);
				switch (taskExecutionStatus.names[i]) {
	                case "Timeout":    taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Timeout"); 	break;
	                case "Completed":  taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Completed");  break;
	                case "Failed":     taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Failed");     break;
	                case "Prepared":   taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Prepared");   break;
	                case "Submitted":  taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Submitted");  break;   
	                case "Postponed":  taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Postponed");  break;                                                  
	                default: 		   taskExecutionStatus.names[i] = "Unknow"; 						break;
            	}
				$("#subtask_status ul").append("<li style=\"margin-bottom: 5px;\"><i class=\"fa fa-square\" style=\"color:"+sliceColors[i]+";margin-right: 5px;\"></i>"+taskExecutionStatus.names[i]+" :"+taskData[i]+"</li>");
			}
		});
		
		$("#task_info table tr:eq(0)").on("click",function(){
			 window.history.back();
		});
	}
	
    /* define bootstrap starts */
    AD.bootstrap = function() {
	   	
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

		$("body").removeClass("loading");
		var hash = window.location.hash;
		var id = hash.replace(/^#ajax\/subtask_list.html\/(\w+)$/, "$1");
	
        adReload(id);

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		
		Backbone.Events.off("SubTaskUpdate");
		Backbone.Events.on("SubTaskUpdate", function(data){
			if(data.taskId === id){
				adReload(id);
			}
		});
    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();


})(jQuery);
