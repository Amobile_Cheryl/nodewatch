(function($) {
    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /* define namespace starts */
    var AD = {};
    AD.page = "ajax/software_policy_list.html";

    // Windows
    function s1() {
        var AD = {};
        AD.page = "ajax/software_policy_list.html";
        AD.collections = {};
        var url = $.config.server_rest_url + "/devicePolicys" + "?search=operatingSystemName:Windows";
        var idAttribute = AD.idAttribute = "id";
        if ($.login.user.checkRole('AppFileOperator'))
        AD.buttons = [
            //button
            {
                "sExtends" : "text",
                "sButtonText" : "<i class='fa fa-plus'></i>&nbsp; " + "Add Policy",//i18n.translate("ns:File.Upload"),
                "sButtonClass" : "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
            },
            {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash) {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
        ];

        AD.columns = [
            {
                title : 'ns:File.Firmware.Title.Name',
                property : 'name',
                cellClassName : 'name',
                filterable : false,
                sortable : true,
                width: '200px',
                callback: function(o) {
                    return '<i class="fa fa-shield"></i>' + o.name;
                }
            },{
                title : 'ns:File.App.Title.Creator',
                property : 'creatorName',
                cellClassName : 'creatorName',
                filterable : false,
                width: '200px',
                sortable : false
            },{
                //title : 'ns:File.Firmware.Title.FileSize',
                title : 'Block Apps [Process Name]',
                property : 'appBlacklist',
                cellClassName : 'appBlacklist',
                filterable : false,
                sortable : false,
                width: '300px',
                callback : function (o) {
                    return o.appBlacklist2;
                }
            },{
                title : 'ns:File.Firmware.Title.Remark',
                property : 'remark',
                cellClassName : 'remark',
                filterable : false,
                sortable : false,
                callback : function (o) {
                    if (o.remark == null) {
                        return "";
                    } else {
                        return o.remark;
                    }
                }
            },{
                title : 'ns:File.Firmware.Title.UploadedTime',
                property : 'creationTime',
                cellClassName : 'creationTime',
                filterable : false,
                sortable : true,
                width: '180px',
                callback: function(o) {
                    return o.creationTime;
                }
            },
            {
                title : 'ns:File.Firmware.Title.Action',
                sortable : false,
                width: '100px',
                visible: $.login.user.checkRole('AppFileOperator'),
                callback : function (o) {
                    var _html = '';
                    // [Action] Delete the firmware
                    if ($.common.isDelete)
                        _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm btn_delete DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                    return _html;
                }
            }
        ];
        /* define namespace ends */

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
        /* collection ends */

        var routes = {};
        routes[AD.page + '/delete/:_id'] = 'act_delete';   // [Action] Delete the File
        routes[AD.page + '/grid/s2']     = 'act_s2';       // [Action] Act to s2
        routes[AD.page] = 'render';

        var MyRouter = Backbone.DG1.extend({
            routes: routes,
            initialize: function(opt) {
                var t = this;
                //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
                Backbone.DG1.prototype.initialize.call(t, opt);
                t.$bt = $(opt.bts).first();
                console.error();
                this.show_grid();
            },
            show_grid: function() {
                var t = this;

                var gvs = "#s1 .cctech-gv";
                var gv = $(gvs).first();

                if(!androidGrid) {
                    var androidGrid = new Backbone.GridView({
                        el: t.$gv,
                        collection: new t.AD.Collection(),
                        buttons: t.AD.buttons,
                        columns: t.AD.columns,
                        AD: t.AD
                    });

                    gv.on("click", ".s2-btn", function (event) {
                        event.preventDefault();

                        var hash = window.location.hash;
                        var url = location.hash.replace(/^#/, '');

                        var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                        url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                        t.navigate("#ajax/software_policy_add.html", {
                            trigger: true
                        });
                    });
                } else {
                    androidGrid.refresh();
                }
                t.$gv.show();
                //pageSetUp();
            },
            act_delete: function(id) {
                event.preventDefault();
                var t = this;

                $.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+"Delete the software policy"+"<span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+"Delete this policy may cause the related delivered task fail. Please make sure that policy has not been applied."+"</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                        $("body").addClass("loading");

                        $.ajax({
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/devicePolicys/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[Android Firmware] Deleted failed");
                            }
                        }).done(function(data){
                            if (typeof(data) === 'undefined'){
                                console.log("[Android Firmware] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: "[Android Software Policy]" + " Deleted successfully",
                                        content: "<i class='fa fa-times'></i> <i>"+ "Deleted Policy" + "</i>",
                                        // title: "[Android Firmware] Deleted successfully",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {
                                    $.smallBox({
                                        title: "[Android Software Policy]" + " Deleted failed",
                                        content: "<i class='fa fa-times'></i> <i>"+ "Deleted failed" + "</i>",
                                        // title: "[Android Firmware] Deleted failed",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[Android Firmware] Deleted failed => " + JSON.stringify(data));
                                };
                            }
                        }).fail(function(){
                            $.smallBox({
                                title: "[Android Software Policy]" + " Deleted failed",
                                content: "<i class='fa fa-times'></i> <i>"+ "Deleted failed" + "</i>",
                                // title: "[Android Firmware] Deleted failed",
                                // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        }).always(function() {
                            $("body").removeClass("loading");
                            // window.location.href = "#ajax/firmware_list.html";
                            t.navigate("#ajax/software_policy_list.html/grid/s2", {
                                trigger: true
                            });
                        });
                    } else {
                        // window.location.href = "#ajax/firmware_list.html";
                        t.navigate("#ajax/software_policy_list.html/grid/s2", {
                            trigger: true
                        });
                    }
                });
            },
            act_s2: function(id) {
                s2();
            }
        });

        pageSetUp();

        AD.app = new MyRouter({
            "AD": AD
        });
    };

    // Android
    function s2() {
        var AD = {};
        AD.page = "ajax/software_policy_list.html";
        AD.collections = {};
        var url = $.config.server_rest_url + "/devicePolicys" + "?search=operatingSystemName:Android";
        var idAttribute = AD.idAttribute = "id";
        if ($.login.user.checkRole('AppFileOperator'))
        AD.buttons = [
            //button
            {
                "sExtends" : "text",
                "sButtonText" : "<i class='fa fa-plus'></i>&nbsp; " + "Add Policy",//i18n.translate("ns:File.Upload"),
                "sButtonClass" : "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
            },
            {
                "sExtends": "text",
                "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                "fnClick": function(nButton, oConfig, oFlash) {
                    $(".cctech-gv table").dataTable().fnDraw(false);
                }
            }
        ];

        AD.columns = [
            {
                title : 'ns:File.Firmware.Title.Name',
                property : 'name',
                cellClassName : 'name',
                filterable : false,
                sortable : true,
                width: '200px',
                callback: function(o) {
                    var aColorSet = ["darkred", "green", "blue", "purple", "orange"];
                    var iRandomColorSetIndex = o.name.charCodeAt(0) % 5;
                    // return '<i class="fa fa-shield"></i>' + o.name;
                    return '<span class="circle" style="background-color:'+ aColorSet[iRandomColorSetIndex]+'; font-size:36px; padding:13px 1px 0px 0px;">'+o.name.charAt(0).toUpperCase()+'</span><spam>'+o.name+'</span>';
                }
            },{
                title : 'ns:File.App.Title.Creator',
                property : 'creatorName',
                cellClassName : 'creatorName',
                filterable : false,
                width: '200px',
                sortable : false
            },{
                //title : 'ns:File.Firmware.Title.FileSize',
                title : 'Block Apps [Package Name]',
                property : 'appBlacklist',
                cellClassName : 'appBlacklist',
                filterable : false,
                sortable : false,
                width: '300px',
                callback : function (o) {
                    return o.appBlacklist2;
                }
            },{
                title : 'ns:File.Firmware.Title.Remark',
                property : 'remark',
                cellClassName : 'remark',
                filterable : false,
                sortable : false,
                callback : function (o) {
                    if (o.remark == null) {
                        return "";
                    } else {
                        return o.remark;
                    }
                }
            },{
                title : 'ns:File.Firmware.Title.UploadedTime',
                property : 'creationTime',
                cellClassName : 'creationTime',
                filterable : false,
                sortable : true,
                width: '180px',
                callback: function(o) {
                    return o.creationTime;
                }
            },
            {
                title : 'ns:File.Firmware.Title.Action',
                sortable : false,
                width: '100px',
                visible: $.login.user.checkRole('AppFileOperator'),
                callback : function (o) {
                    var _html = '';
                    // [Action] Delete the firmware
                    if ($.common.isDelete)
                        _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm btn_delete DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                    return _html;
                }
            }
        ];
        /* define namespace ends */

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
        /* collection ends */

        var routes = {};
        routes[AD.page + '/delete/:_id'] = 'act_delete';   // [Action] Delete the File
        routes[AD.page + '/grid/s2']     = 'act_s2';       // [Action] Act to s2
        routes[AD.page] = 'render';

        var MyRouter = Backbone.DG1.extend({
            routes: routes,
            initialize: function(opt) {
                var t = this;
                //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
                Backbone.DG1.prototype.initialize.call(t, opt);
                t.$bt = $(opt.bts).first();
                console.error();
                this.show_grid();
            },
            show_grid: function() {
                var t = this;

                var gvs = "#s1 .cctech-gv";
                var gv = $(gvs).first();

                if(!androidGrid) {
                    var androidGrid = new Backbone.GridView({
                        el: t.$gv,
                        collection: new t.AD.Collection(),
                        buttons: t.AD.buttons,
                        columns: t.AD.columns,
                        AD: t.AD
                    });

                    gv.on("click", ".s2-btn", function (event) {
                        event.preventDefault();

                        var hash = window.location.hash;
                        var url = location.hash.replace(/^#/, '');

                        var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                        url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                        t.navigate("#ajax/software_policy_add.html", {
                            trigger: true
                        });
                    });
                } else {
                    androidGrid.refresh();
                }
                t.$gv.show();
                //pageSetUp();
            },
            act_delete: function(id) {
                event.preventDefault();
                var t = this;

                $.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+"Delete the software policy"+"<span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+"Delete this policy may cause the related delivered task fail. Please make sure that policy has not been applied."+"</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                        $("body").addClass("loading");

                        $.ajax({
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/devicePolicys/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[Android Firmware] Deleted failed");
                            }
                        }).done(function(data){
                            if (typeof(data) === 'undefined'){
                                console.log("[Android Firmware] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: "[Android Software Policy]" + " Deleted successfully",
                                        content: "<i class='fa fa-times'></i> <i>"+ "Deleted Policy" + "</i>",
                                        // title: "[Android Firmware] Deleted successfully",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {
                                    $.smallBox({
                                        title: "[Android Software Policy]" + " Deleted failed",
                                        content: "<i class='fa fa-times'></i> <i>"+ "Deleted failed" + "</i>",
                                        // title: "[Android Firmware] Deleted failed",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[Android Firmware] Deleted failed => " + JSON.stringify(data));
                                };
                            }
                        }).fail(function(){
                            $.smallBox({
                                title: "[Android Software Policy]" + " Deleted failed",
                                content: "<i class='fa fa-times'></i> <i>"+ "Deleted failed" + "</i>",
                                // title: "[Android Firmware] Deleted failed",
                                // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        }).always(function() {
                            $("body").removeClass("loading");
                            // window.location.href = "#ajax/firmware_list.html";
                            t.navigate("#ajax/software_policy_list.html/grid/s2", {
                                trigger: true
                            });
                        });
                    } else {
                        // window.location.href = "#ajax/firmware_list.html";
                        t.navigate("#ajax/software_policy_list.html/grid/s2", {
                            trigger: true
                        });
                    }
                });
            },
            act_s2: function(id) {
                s2();
            }
        });

        pageSetUp();

        AD.app = new MyRouter({
            "AD": AD
        });
    };

    AD.bootstrap = function() {
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        if (!$.login.user.checkRole('AppFileViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        };

        $("#myTab li").click(function() {
            switch (this.firstElementChild.id) {
                case "s1": //Windows
                    s1();
                    break;
                case "s2": //Android
                    s2();
                    break;
                case "s3": //iPhone
                    s3();
                    break;
            }
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

        // Initial page & after uploading
        var hash = window.location.hash;
        var url = location.hash.replace(/^#/, '');
        var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');

        switch (action) {
            case "grid/u1": //Windows
            case "grid/s1":
                $("#tab2").removeClass('active');
                $("#tab1").addClass('active');
                s1();
                break;
            case "grid/u2": //Android
            case "grid/s2":
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                s2();
                break;
            default:    //Default
                s2();
                break;
        }
    };
    $[AD.page] = AD;
})(jQuery);

function defaultAppIcon(faOperatingSystem){
	return "<div style=\""
				//+"-webkit-border-radius: 10px;  -moz-border-radius: 10px;  border-radius: 10px;"
				+"width: 31px;"
				+"height: 35px;"
				+"background-color: #3678DB;"
				+"text-align: center;"
				+"line-height: 35px;"
				+"margin-right: 12px;"
				+"float:left;"
			+"\">"
				+"<i class=\"fa "+faOperatingSystem+" fa-inverse\" style=\"font-size: 18px;\"></i>"
			+"</div>";
}