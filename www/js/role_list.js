(function($) {

    /* redefine */

    /* define namespace starts */
    // alert("hello!");

    var AD = {};
    AD.collections = {};
    var url = AD.url = $.config.server_url + '/role';
    var url = 'http://220.130.176.238:8080/acs-core/RESTful/role';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "Role";
    
    

    AD.validate = function() {
        return true;
    };


    AD.forms = [
        //form1
        {
            title: 'Role',
            property: 'roleName',
            default_value: ""
        },
        //form2
        {
            title: 'Description',
            property: 'description',
            default_value: ""
        }
    ];

    AD.columns = [
        //columns1
        {
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            sortable: false,
            callback: function(o) {
                if (o)
                    return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
                else
                    return '<input type="checkbox" value="0"></input>';
            }
        },
        //columns2
        {
            title: 'Role',
            property: 'roleName',
            cellClassName: 'Name',
            filterable: true,
            sortable: true
        }, {
            title: 'Description',
            property: 'description',
            cellClassName: 'description',
            filterable: true,
            sortable: true
        },

        /*{
            title: 'User Name',
            property: 'name',
            cellClassName: 'name',
            filterable: true,
            sortable: true
        },
        //columns3
        {
            title: 'User Role',
            property: 'role',
            cellClassName: 'role',
            filterable: false,
            callback: function(o) {

                if (!AD.collections.role) {
                    var coll = Backbone.getCollectionByUrl($.config.server_url + '/role', "id");
                    coll.fetch({
                        async: false,
                        data: {
                            "fields": ["id", "description"]
                        }
                    });
                    // coll.pop();
                    var info = coll.at(coll.length - 1);
                    coll.remove(info);
                    AD.collections.role = coll;
                }
                var coll = AD.collections.role;
                // alert(o.role);
                var p = coll.get(o.role);
                // alert(p);
                if (typeof p !== "undefined")
                    var _html = p.get("roleName");
                else
                    var _html = "";
                // var _html = "test";
                return _html;

            }
        },
        //columns4
        {
            title: 'Email',
            property: 'email',
            cellClassName: 'email',
            filterable: true,
            sortable: true
        },
        //columns5
        {
            title: 'Phone Number',
            property: 'phone',
            cellClassName: 'phone',
            filterable: true,
            sortable: true
        },
        //columns6
        {
            title: 'Create Time',
            property: 'creationTime',
            cellClassName: 'creation_time',
            filterable: true,
            sortable: true
        },
        */
        //columns7
        {
            title: 'Function',
            sortable: false,
            callback: function(o) {
                var _html = '';
                // _html +='<button class="btn btn-mini btn-success" title="Verify"><i class="icon-ok"></i> </button>';
                if (o) {
                    // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Edit</span></a>';
                    // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Remove</span></a>';
                    _html += '<a href="#edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-edit"></i></a>';
                    _html += '<a href="#delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-times"></i></a>';
                }
                return _html;
            }
        }
    ];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */

    /* define bootstrap starts */
    AD.bootstrap = function() {
        AD.app = new Backbone.DG1({
            "AD": AD
        });
        Backbone.emulateHTTP = true;
        Backbone.history.start();
    };
    /* define bootstrap ends */

    $.list = AD;

    /* run bootstrap */
    $.list.bootstrap();


})(jQuery);

(function($) {

    /* redefine */

    /* define namespace starts */

    var AD = {};
    AD.collections = {};
    var url = AD.url = $.config.server_url + '/role';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "Role";


    AD.forms = [{
            title: 'Name',
            property: 'roleName',
            default_value: ""
        }, {
            title: 'Description',
            property: 'description',
            default_value: ""
        }, {
            title: '',
            property: 'createdby',
            useTemplate: false,
            view: function(o) {
                var id = o.get("id");
                if (typeof id === "undefined")
                    this.isAdd = true;
                else
                    this.isAdd = false;
            },
            value: function() { //provide value
                var ret = {};
                if (this.isAdd == true) {
                    ret["createdby"] = $.login.user.id;
                }
                return ret;
            },
            setValue: function(m) { //set the value to this element
                var v = m.get(this.property) || this.default_value;
                // $('input[name="' + this.property + '"]').val(v);
            },
            update: function(m) {

            }

        }

    ];

    AD.columns = [{
        title: '<input type="checkbox" value="all"></input>',
        cellClassName: 'DeleteItem',
        view: {
            type: Backbone.Datagrid.CallbackCell,
            callback: function(o) {
                return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
            }
        }
    }, {
        title: 'Name',
        property: 'roleName',
        cellClassName: 'Name',
        filterable: true,
        sortable: true
    }, {
        title: 'Description',
        property: 'description',
        cellClassName: 'description',
        filterable: true,
        sortable: true
    }, {
        title: 'Created By',
        property: 'createdby',
        cellClassName: 'createdby',
        filterable: false,
        view: {
            type: Backbone.Datagrid.CallbackCell,
            callback: function(o) {
                // var test=o['createdBy'];
                if (!AD.collections.user) {
                    var coll = Backbone.Datagrid.getCollectionByUrl($.config.server_url + '/user', "id");
                    coll.fetch({
                        async: false,
                        data: {
                            "fields": ["id", "name"]
                        }
                    });
                    // coll.pop();
                    AD.collections.user = coll;
                }
                var coll = AD.collections.user;
                // alert(o.createdby);
                var p = coll.get(o.createdby);
                // alert(p);
                var _html = "Matthew Hui";
                // if (typeof p !== "undefined")
                //   var _html = p.get("name");
                // else
                //   var _html = "";
                return _html;
            }
        }
    }, {
        title: 'Edit',
        view: {
            type: Backbone.Datagrid.CallbackCell,
            // label : '刪除',
            callback: function(o) {
                var _html = '';
                // _html +='<button class="btn btn-mini btn-success" title="Verify"><i class="icon-ok"></i> </button>';
                _html += '<a href="#edit/' + o[idAttribute] + '" class="button-icon" title="Edit"><i class="fa fa-edit"></i> </a>';
                _html += '<a href="#" class="button-icon" title="Delete" data-id="' + o[idAttribute] + '"><i class="fa fa-times"></i> </a>';
                return _html;
            }
        }
    }];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */

    /* define bootstrap starts */
    AD.bootstrap = function() {
        AD.app = new Backbone.View1.Router({
            "AD": AD
        });
        Backbone.emulateHTTP = true;
        Backbone.history.start();
		$("body").removeClass("loading");
    };
    /* define bootstrap ends */

    $.list = AD;

    /* run bootstrap */
    $.list.bootstrap();


})(jQuery);