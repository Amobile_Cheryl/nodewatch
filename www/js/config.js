var AMobile = false;
var SpringPaggingMode = true;
var TempDemoMode = true;
var CoreServerName = "developer";
var isDashboardAutoRefresh = true;
// var sNodeWatchServerHost = 'http://amobile.node-watch.com/dm';
var sNodeWatchServerHost = 'http://192.168.101.150/dm';

var oRefreshData = {};
oRefreshData.oDashboard = 
{
    oTimerId: null,
    iInterval: 30000,
    bIsEnable: true
};
oRefreshData.oUserLog = 
{
    oTimerId: null,
    iInterval: 30000,
    bIsEnable: true
};
oRefreshData.oFirmwareFile = 
{
    oTimerId: null,
    iInterval: 30000,
    bIsEnable: true
};
oRefreshData.oAgentFile = 
{
    oTimerId: null,
    iInterval: 30000,
    bIsEnable: true
};
oRefreshData.oAppFile = 
{
    oTimerId: null,
    iInterval: 30000,
    bIsEnable: true
};

(function($)
{
    $.config = {};
    $.config.version = 'Ver 1.1601.1.D';
    $.config.debugger = false;
    $.config.server_host = sNodeWatchServerHost;
    // $.config.server_host = 'http://developer.node-watch.com/dm';
    // $.config.server_host = 'http://amobile.node-watch.com/dm';

    $.config.ui = {};
    //if(AMobile) {
    if (true)
    {
        //$.config.ui.logo = 'img/amobile-logo.png';
        //$.config.ui.logo = 'img/NodeWatch-logo2.png';
        $.config.ui.logo = 'img/For_red_background.png';
        $.config.ui.logo_login = 'img/For_black_background.png';
        $.config.ui.logoStyle = {
            //'margin': '10px 10px',
            '-webkit-filter': 'drop-shadow(0px 4px 1px black)',
            'width': '288px',
            'height': '42.24px',
            'margin-left': '5px',
            'margin-top': '2px'
        };
        $.config.ui.loginLogoStyle = {
            'width': '250px',
            'height': '112px'
        };
        $.config.ui.headerStyle = {
            //'background-color': 'rgba(233, 3, 3, 0.9)'
            //'background-color': 'rgba(60, 60, 60, 1)'
            'background-color': 'rgb(79, 78, 78, 1)'
        };
        $.config.ui.title = 'Node Watch';
        $.config.ui.lockScreenLogoStyle = {
            'margin-right': '10px',
            'width': '150px'
        };
    }
    else
    {
        $.config.ui.logo = 'img/mdm-logo.png';
        $.config.ui.logoStyle = {
            'margin': '7px 7px',
            'height': '35px'
        };
        $.config.ui.loginLogoStyle = {};
        $.config.ui.logoBgStyle = {
            'background': '#f85032',
            /* Old browsers */
            /* IE9 SVG, needs conditional override of 'filter' to 'none' */
            'background': 'url(data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiA/Pgo8c3ZnIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgd2lkdGg9IjEwMCUiIGhlaWdodD0iMTAwJSIgdmlld0JveD0iMCAwIDEgMSIgcHJlc2VydmVBc3BlY3RSYXRpbz0ibm9uZSI+CiAgPGxpbmVhckdyYWRpZW50IGlkPSJncmFkLXVjZ2ctZ2VuZXJhdGVkIiBncmFkaWVudFVuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeDE9IjAlIiB5MT0iMCUiIHgyPSIxMDAlIiB5Mj0iMTAwJSI+CiAgICA8c3RvcCBvZmZzZXQ9IjAlIiBzdG9wLWNvbG9yPSIjZjg1MDMyIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMjglIiBzdG9wLWNvbG9yPSIjZjE2ZjVjIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMzAlIiBzdG9wLWNvbG9yPSIjZTkwMzAzIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iNzElIiBzdG9wLWNvbG9yPSIjZTkwMzAzIiBzdG9wLW9wYWNpdHk9IjEiLz4KICAgIDxzdG9wIG9mZnNldD0iMTAwJSIgc3RvcC1jb2xvcj0iI2U5MDMwMyIgc3RvcC1vcGFjaXR5PSIxIi8+CiAgPC9saW5lYXJHcmFkaWVudD4KICA8cmVjdCB4PSIwIiB5PSIwIiB3aWR0aD0iMSIgaGVpZ2h0PSIxIiBmaWxsPSJ1cmwoI2dyYWQtdWNnZy1nZW5lcmF0ZWQpIiAvPgo8L3N2Zz4=)',
            'background': '-moz-linear-gradient(-45deg, #0A0A0A 0%,#525050 28%,#494949 30%,#000000 71%,#000000 100%)',
            /*background: -moz-linear-gradient(-45deg, #f85032 0%, #f16f5c 28%, #e90303 30%, #e90303 71%, #e90303 100%);*/
            /* FF3.6+ */
            'background': '-webkit-gradient(linear, left top, right bottom, color-stop(0%,#0A0A0A), color-stop(28%,#525050), color-stop(30%,#494949), color-stop(71%,#000000), color-stop(100%,#000000))',
            /*background: -webkit-gradient(linear, left top, right bottom, color-stop(0%,#f85032), color-stop(28%,#f16f5c), color-stop(30%,#e90303), color-stop(71%,#e90303), color-stop(100%,#e90303));*/
            /* Chrome,Safari4+ */
            'background': '-webkit-linear-gradient(-45deg, #0A0A0A 0%,#525050 28%,#494949 30%,#000000 71%,#000000 100%)',
            /*background: -webkit-linear-gradient(-45deg, #f85032 0%,#f16f5c 28%,#e90303 30%,#e90303 71%,#e90303 100%);*/
            /* Chrome10+,Safari5.1+ */
            'background': '-o-linear-gradient(-45deg, #0A0A0A 0%,#525050 28%,#494949 30%,#000000 71%,#000000 100%)',
            /*background: -o-linear-gradient(-45deg, #f85032 0%,#f16f5c 28%,#e90303 30%,#e90303 71%,#e90303 100%);*/
            /* Opera 11.10+ */
            'background': '-ms-linear-gradient(-45deg, #0A0A0A 0%,#525050 28%,#494949 30%,#000000 71%,#000000 100%)',
            /*background: -ms-linear-gradient(-45deg, #f85032 0%,#f16f5c 28%,#e90303 30%,#e90303 71%,#e90303 100%);*/
            /* IE10+ */
            'background': 'linear-gradient(135deg, #0A0A0A 0%,#525050 28%,#494949 30%,#000000 71%,#000000 100%)',
            /*background: linear-gradient(135deg, #f85032 0%,#f16f5c 28%,#e90303 30%,#e90303 71%,#e90303 100%);*/
            /* W3C */
            'filter': 'progid:DXImageTransform.Microsoft.gradient( startColorstr="#f85032", endColorstr="#e90303",GradientType=1 )' /* IE6-8 fallback on horizontal gradient */
        };
        $.config.ui.headerStyle = {
            'background-color': '#000000'
        };
        $.config.ui.title = 'MDM Cloud';
        $.config.ui.lockScreenLogoStyle = {
            'height': '28px',
            'width': '150px',
            'margin-right': '10px'
        };
    }

    $.config.ui.taskExecutionStatusColor = {
        "ready": "#FFA500",
        "progress": "#2E7BCC",
        "completesuccess": "#66AA00",
        "completefail": "#FF0039",
        "done": "#66AA00",
        "running": "#2E7BCC",
        "error": "#FF0039"
    };

    $.config.ui.subtaskExecutionStatusColor = {
        "prepared": "#909090",
        "submitted": "#2E7BCC",
        "completed": "#66AA00",
        "failed": "#FF0039",
        "timeout": "#FFA500",
        "postponed": "#D8D338"
    };

    $.config.server_util_url = $.config.server_host + '/Transfer';

    $.config.server_rest_url = $.config.server_host + '/RESTful';
    $.config.server_new_rest_url = $.config.server_host + '/RESTful/v1';

    $.config.menu = [
        //root
        {
            // "ID": "1",
            "Weight": "0",
            "Level": "0",
            "Name": "root",
            "Url": null,
            "Icon": null,
            "Type": "iii"
        },
        //menu
        {
            // "ID": "2",
            "Weight": "1000",
            "Level": "1",
            "Name": "Dashboard",
            "i18N": "Dashboard",
            "Url": "dashboard.html",
            "Icon": "fa-dashboard",
            "Type": "iii",
            "Permission": "DashboardViewer"
        },
        //cate
        {
            // "ID": "3",
            "Weight": "2000",
            "Level": "1",
            "Name": "Device",
            "i18N": "Device",
            "Url": null,
            "Icon": "fa-tablet",
            "Type": "iii"
        },
        //menu
        {
            // "ID": "33",
            "Weight": "2030",
            "Level": "2",
            "Name": "Type",
            "i18N": "Types",
            "Url": "devicetype_list.html",
            "Icon": "fa-tags",
            "Type": "iii",
            "Permission": "DeviceTypeViewer"
        },
        {
            // "ID": "31",
            "Weight": "2010",
            "Level": "2",
            "Name": "Device",
            "i18N": "Devices",
            "Url": "device_list.html",
            "Icon": "fa-square",
            "Type": "iii",
            "Permission": "DeviceViewer"
        },
        //menu
        {
            // "ID": "32",
            "Weight": "2020",
            "Level": "2",
            "Name": "DeviceGroup",
            "i18N": "Groups",
            "Url": "group_list.html",
            "Icon": "fa-th-large",
            "Type": "iii",
            "Permission": "DeviceGroupViewer"
        },
        //menu

        //////
        /*{
            // "ID": "34",
            "Weight": "2040",
            "Level": "2",
            "Name": "DeviceEventDefinition",
            "i18N": "EventSetting",
            "Url": "eventSetting_list.html",
            "Icon": "fa-bullhorn",
            "Type": "iii",
            "Permission": "DeviceEventDefinitionViewer"
        },
        {
            // "ID": "35",
            "Weight": "2050",
            "Level": "2",
            "Name": "Monitor Setting",
            "i18N": "MonitorSetting",
            "Url": "alertSetting_list.html",
            "Icon": "fa-eye",
            "Type": "iii",
            "Permission": "MonitorViewer"
        },*/
        //////
        //cate
        {
            // "ID": "4",
            "Weight": "4000",
            "Level": "1",
            "Name": "File",
            "i18N": "File",
            "Url": null,
            "Icon": "fa-th",
            "Type": "iii"
        },
        //menu
        /*
        {
            // "ID": "10",
            "Weight": "4010",
            "Level": "2",
            "Name": "Uploaded File",
            "Url": "uploadedfile_list.html",
            "Icon": "fa-upload",
            "Type": "iii"
        },
        */
        //menu
        {
            // "ID": "41",
            "Weight": "4015",
            "Level": "2",
            "Name": "AppFile",
            "i18N": "App",
            "Url": "app_list.html",
            "Icon": "fa-cloud-download",
            "Type": "iii",
            "Permission": "AppFileViewer"
        },
        //menu
        {
            // "ID": "42",
            "Weight": "4020",
            "Level": "2",
            "Name": "FirmwareFile",
            "i18N": "Firmware",
            "Url": "firmware_list.html",
            "Icon": "fa-flash",
            "Type": "iii",
            "Permission": "FirmwareFileViewer"
        },
        //menu
        {
            // "ID": "43",
            "Weight": "4025",
            "Level": "2",
            "Name": "AgentFile",
            "i18N": "Agent",
            "Url": "agent_list.html",
            "Icon": "fa-floppy-o",
            "Type": "iii",
            "Permission": "OmaAgentFileViewer"
        },
        //cate
        // {
        //     // "ID": "6",
        //     "Weight": "4500",
        //     "Level": "1",
        //     "Name": "User",
        //     "Url": null,
        //     "Icon": "fa-user",
        //     "Type": "iii"
        // },
        //menu
        /*{
            // "ID": "42",
            "Weight": "4200",
            "Level": "1",
            "Name": "Policy",
            "i18N": "Policy",
            "Url": null,
            "Icon": "fa-shield",
            "Type": "iii",
            "Permission": "AppFileViewer"
        },*/
        /*{
            // "ID": "42",
            "Weight": "4210",
            "Level": "2",
            "Name": "Event",
            "i18N": "Event",
            "Url": "event_policy_list.html",
            "Icon": "fa-android",
            "Type": "iii",
            "Permission": "FirmwareFileViewer"
        },*/
        {
            // "ID": "42",
            "Weight": "4220",
            "Level": "1",
            "Name": "Software Policy",
            "i18N": "Software",
            "Url": "software_policy_list.html",
            "Icon": "fa-shield",
            "Type": "iii",
            "Permission": "SoftwarePolicy"
        },
        {
            // "ID": "7",
            "Weight": "4510",
            "Level": "1",
            "Name": "User",
            "i18N": "User",
            "Url": "user_list.html",
            "Icon": "fa-group",
            "Type": "iii",
            "Permission": "UserViewer"
        },
        //menu
        // {
        //     // "ID": "8",
        //     "Weight": "4520",
        //     "Level": "2",
        //     "Name": "Roles",
        //     "Url": "role_list.html",
        //     "Icon": "fa-key",
        //     "Type": "iii"
        // },
        //menu
        /*{
            // "ID": "50",
            "Weight": "4530",
            "Level": "2",
            "Name": "Resource",
            "Url": "resource_list.html",
            "Icon": "",
            "Type": "iii"
        },*/
        //menu
        {
            // "ID": "11",
            "Weight": "5000",
            "Level": "1",
            "Name": "Domain",
            "i18N": "Domain",
            "Url": "domain_list.html",
            "Icon": "fa-sitemap",
            "Type": "iii",
            "Permission": "DomainViewer"
        },
        //cate
        /*{
            // "ID": "12",
            "Weight": "6000",
            "Level": "1",
            "Name": "Setting",
            "i18N": "Setting",
            "Url": "setting.html",
            "Icon": "fa-gears",
            "Type": "iii",
            "Permission": "SettingViewer"
        },*/
        //cate
        {
            // "ID": "15",
            "Weight": "8000",
            "Level": "1",
            "Name": "Log",
            "i18N": "Log",
            "Url": null,
            "Icon": "fa-book",
            "Type": "iii"
        },
        //menu
        {
            // "ID": "16",
            "Weight": "8010",
            "Level": "2",
            "Name": "User Log",
            "i18N": "UserLog",
            "Url": "userlog_list.html",
            "Icon": "fa-table",
            "Type": "iii",
            "Permission": "UserLogViewer"
        },
        //menu
        /*{
            // "ID": "17",
            "Weight": "8020",
            "Level": "2",
            "Name": "System Log",
            "i18N": "SystemLog",
            "Url": "systemlog_list.html",
            "Icon": "fa-table",
            "Type": "iii",
            "Permission": "SystemLogViewer"
        },*/
        {
            // "ID": "17",
            "Weight": "8030",
            "Level": "2",
            "Name": "Task Log",
            "i18N": "TaskLog",
            "Url": "task_list.html",
            "Icon": "fa-tasks",
            "Type": "iii",
            "Permission": "TaskViewer"
        },
        /*{
            // "ID": "17",
            "Weight": "8040",
            "Level": "2",
            "Name": "Event Log",
            "i18N": "EventLog",
            "Url": "eventLog_list.html",
            "Icon": "fa-tasks",
            "Type": "iii",
            "Permission": "EventLogViewer"
        },*/
        //menu
        /*{
            // "ID": "18",
            "Weight": "9000",
            "Level": "1",
            "Name": "Report",
            "i18N": "Report",
            "Url": null,
            "Icon": "fa-file-text",
            "Type": "iii",
            "Permission": "ReportViewer"
        },
        {
            // "ID": "18",
            "Weight": "9010",
            "Level": "2",
            "Name": "DeviceReport",
            "i18N": "DeviceReport",
            "Url": "device_report.html",
            "Icon": "fa-tablet",
            "Type": "iii",
            "Permission": "ReportViewer"
        },
        {
            // "ID": "18",
            "Weight": "9020",
            "Level": "2",
            "Name": "EventReport",
            "i18N": "EventReport",
            "Url": "event_report.html",
            "Icon": "fa-bullhorn",
            "Type": "iii",
            "Permission": "ReportViewer"
        },
        {
            // "ID": "18",
            "Weight": "9030",
            "Level": "2",
            "Name": "MonitorReport",
            "i18N": "MonitorReport",
            "Url": "monitor_report.html",
            "Icon": "fa-eye",
            "Type": "iii",
            "Permission": "ReportViewer"
        },
        {
            // "ID": "18",
            "Weight": "9040",
            "Level": "2",
            "Name": "TaskReport",
            "i18N": "TaskReport",
            "Url": "task_report.html",
            "Icon": "fa-tasks",
            "Type": "iii",
            "Permission": "ReportViewer"
        },*/
        //menu
        /*
        {
            // "ID": "19",
            "Weight": "10000",
            "Level": "1",
            "Name": "App Inventory",
            "Url": "inventory.html",
            "Icon": " fa-briefcase",
            "Type": "iii"
        },
        */
        //count
        {
            "count": 18
        }
    ];

    //$.config.menu = _.without($.config.menu, _.findWhere($.config.menu,{"i18N":"Dashboard"}));
    if (AMobile)
    {
        $.config.menu = _.without($.config.menu, _.findWhere($.config.menu,
        {
            "i18N": "EventSetting"
        }));
        $.config.menu = _.without($.config.menu, _.findWhere($.config.menu,
        {
            "i18N": "MonitorSetting"
        }));
        $.config.menu = _.without($.config.menu, _.findWhere($.config.menu,
        {
            "i18N": "Setting"
        }));
        $.config.menu = _.without($.config.menu, _.findWhere($.config.menu,
        {
            "i18N": "Report"
        }));
        $.config.menu = _.without($.config.menu, _.findWhere($.config.menu,
        {
            "i18N": "DeviceReport"
        }));
        $.config.menu = _.without($.config.menu, _.findWhere($.config.menu,
        {
            "i18N": "EventReport"
        }));
        $.config.menu = _.without($.config.menu, _.findWhere($.config.menu,
        {
            "i18N": "MonitorReport"
        }));
        $.config.menu = _.without($.config.menu, _.findWhere($.config.menu,
        {
            "i18N": "TaskReport"
        }));
    }
    //

    /*$.config.GuiFunction = {
        null: "Unknown",
        "1-0-0": "Device",
        "1-0-1": "UploadDevice",
        "1-0-2": "DeleteDevice",
        "1-0-3": "EditDevice",
        "1-0-4": "DeviceManagement",
        "2-0-0": "User",
        "2-1-0": "Account",
        "2-1-1": "AddUser",
        "2-1-2": "DeleteUser",
        "2-1-3": "EditUser",
        "2-2-0": "Role",
        "2-2-1": "AddRole",
        "2-2-2": "DeleteRole",
        "2-2-3": "EditRole"
    };*/

    $.config.SystemLogAction = {
        "A": "CheckTimeoutedTask"
    };

    $.config.firmware_lang = {
        "1": "zh_TW",
        "2": "en_US",
        "3": "jp",
        "4": "kr"
    }

    $.config.ItemsPerPage = 10;

    /*$.config.RolePermission = [
        //item
        {
            "ID": "1",
            "Weight": "0",
            "Level": "0",
            "Name": "root",
            "Url": null,
            "Icon": null,
            "Type": "iii"
        },
        //item
        {
            "ID": "2",
            "Weight": "1000",
            "Level": "1",
            "Name": "Device",
            "value": "device"
        },
        //item
        {
            "ID": "3",
            "Weight": "1010",
            "Level": "2",
            "Name": "App Management",
            "value": "appmngt"
        },
        //item
        {
            "ID": "4",
            "Weight": "1020",
            "Level": "2",
            "Name": "Device Management",
            "value": "devmngt"
        },
        //item
        {
            "ID": "5",
            "Weight": "2000",
            "Level": "1",
            "Name": "App",
            "value": "app"
        },
        //item
        {
            "ID": "6",
            "Weight": "3000",
            "Level": "1",
            "Name": "Setting",
            "value": "setting"
        },
        //item
        {
            "ID": "7",
            "Weight": "3010",
            "Level": "2",
            "Name": "Monitor",
            "value": "monitor"
        },
        //item
        {
            "ID": "8",
            "Weight": "3020",
            "Level": "2",
            "Name": "Alert",
            "value": "alert"
        },
        //item
        {
            "ID": "9",
            "Weight": "3030",
            "Level": "2",
            "Name": "System",
            "value": "system"
        },
        //item
        {
            "ID": "10",
            "Weight": "3040",
            "Level": "2",
            "Name": "Property Builder",
            "value": "propbuild"
        },
        //item
        {
            "ID": "11",
            "Weight": "3050",
            "Level": "2",
            "Name": "Device Type",
            "value": "devtype"
        },
        //item
        {
            "ID": "12",
            "Weight": "4000",
            "Level": "1",
            "Name": "Domain",
            "value": "domain"
        },
        //item
        {
            "ID": "13",
            "Weight": "4000",
            "Level": "1",
            "Name": "Log & Report",
            "value": "logrep"
        }
    ];*/

    $.config.maxAlertSettingTarget = 5;
})(jQuery);
