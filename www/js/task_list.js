(function($)
{
    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');

    var AD = {};
    AD.collections = {};
    var url = $.config.server_rest_url + '/tasks';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "ns:Menu.TaskLog";
    AD.page = "ajax/task_list.html";
    //先將API格式換成cctech的(模擬的時候跟後台提供的API格式不同)
    //AD.api = 'cctech';
    AD.isAdd = false;
    AD.sort = {
        "column": "id",
        "sortDir": "desc"
    };

    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /*                  */

    AD.validate = function()
    {
        return true;
    };

    AD.buttons = [
    {
        "sExtends": "text",
        "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
        "sButtonClass": "edit-group-device-list-btn txt-color-white bg-color-greenLight",
        "fnClick": function(nButton, oConfig, oFlash)
        {
            $(".cctech-gv table").dataTable().fnDraw(false);
        }
    }];

    AD.forms = [];

    AD.columns = [
        {
            title: 'ns:Log.Task.Status',
            property: 'executeStatus',
            // cellClassName: 'groupName',
            filterable: false,
            sortable: false,
            width: '50px',
            callback: function(o)
            {
                var color = "#919191";
                color = eval('$.config.ui.taskExecutionStatusColor.' + o.executeStatus.toLowerCase());
                return "<i class=\"fa fa-circle\" style=\"color:" + color + ";\"></i> ";
            }
        },
        {
            title: 'ns:Log.Task.ID',
            property: 'id',
            width: '80px',
            filterable: false,
            sortable: true
        },
        {
            title: 'ns:Log.Task.Name',
            property: 'job',
            filterable: false,
            sortable: true,
            width: '150px',
            callback: function(o)
            {
                //return '<a href="#ajax/subtask_list.html/' + o.id + '">' + o.job + '</a>';
                switch(o.job)
                {
                    //case "dcmoEnableDisable":
                    case "EnableDisableCapability":
                    case "EnableCapability":
                    case "DisableCapability":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.SwitchCapabilitySetting") + '</a>';
                        break;
                        //case "lockDevice":
                    case "LockDevice":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.LockDevice") + '</a>';
                        break;
                        //case "unlockDevice":
                    case "UnlockDevice":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.UnlockDevice") + '</a>';
                        break;
                        //case "activateDeviceApp":
                    case "ActivateApp":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.ActivateApp") + '</a>';
                        break;
                        //case "deactivateDeviceApp":
                    case "DeactivateApp":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.DeactivateApp") + '</a>';
                        break;
                        //case "installDeviceApp":
                    case "DownloadAndInstallApp":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.InstallApp") + '</a>';
                        break;
                        //case "removeDeviceApp":
                    case "RemoveApp":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.RemoveApp") + '</a>';
                        break;
                        //case "upgradeDeviceApp":
                    case "UpgradeApp":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.UpgradeApp") + '</a>';
                        break;
                    case "setAllowUserInstallApp":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.AllowInstallApp") + '</a>';
                        break;
                    case "autoRemoveLocalAPP":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.AutoRemoveLocalApp") + '</a>';
                        break;
                        //case "firmwareUpgrade":
                    case "UpgradeFirmware":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.FirmwareUpgrade") + '</a>';
                        break;
                    case "setDeviceEventList":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.SetDeviceEventList") + '</a>';
                        break;
                    case "setRule":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.SetRule") + '</a>';
                        break;
                    case "deleteRule":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.DeleteRule") + '</a>';
                        break;
                    case "editRule":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.EditRule") + '</a>';
                        break;
                    case "activeCapabilityEvent":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.ActivateCapabilityEvent") + '</a>';
                        break;
                    case "deleteAllRuleAndEvent":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.DeleteRuleEvent") + '</a>';
                        break;
                        //case "ColdBoot":
                    case "Reset":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.RebootDevice") + '</a>';
                        break;
                        //case "WarmBoot":
                    case "Restart":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.RestartDevice") + '</a>';
                        break;
                    case "applyManagementPolicy":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.ApplyManagementPolicy") + '</a>';
                        break;
                        //case "SetAppBlacklist":
                    case "AddAppToBlacklist":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.SetAppBlacklist") + '</a>';
                        break;
                    case "EnableTriggerServerConnection":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.EnableTrigger") + '</a>';
                        break;
                    case "DisableTriggerServerConnection":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.DisableTrigger") + '</a>';
                        break;
                    case "UpgradeDeltaFirmware":
                        return '<a href="#ajax/subtask_list.html/' + o.id + '">' + i18n.t("ns:Log.Task.TaskName.SystemUpdate") + '</a>';
                        break;
                    default:
                        return "Unknown";
                        break;
                }
            }
        },
        {
            title: 'ns:Log.Task.TaskType',
            property: 'taskType',
            filterable: false,
            sortable: false,
            width: '80px',
            callback: function(o)
            {
                if(o.taskType === "DeviceManagement")
                {
                    return "<i class=\"fa fa-hdd-o\"></i> " + i18n.t("ns:Log.Task.DeviceManagement");
                }
                else if(o.taskType === "ApplicationManagement")
                {
                    return "<i class=\"fa fa-puzzle-piece\"></i> " + i18n.t("ns:Log.Task.ApplicationManagement");
                }
                else
                {
                    return "--";
                }
            }
        },
        //columns2
        {
            title: 'ns:Log.Task.Creator',
            property: 'creatorName',
            filterable: false,
            width: '200px',
            sortable: false
        },
        //columns3
        {
            title: 'ns:Log.Task.SubmitTime',
            property: 'creationTime',
            filterable: false,
            width: '180px',
            sortable: true,
        },
        /*{
            title: 'ns:Log.Task.Schedule',
            property: 'schedule',
            filterable: false,
            sortable: true            
        },*/
        {
            title: 'ns:Log.Task.Remark',
            property: 'remark',
            filterable: false,
            sortable: false
        },
        //columns5
        /*
        {
            title: 'Progress',
            property: 'progress',
            // cellClassName: 'creationTime',
            filterable: false,
            sortable: false
        },
        */
        //column
        /*
        {
            title: 'ns:Log.Task.Action',
            sortable: false,
            width: '140px',
            callback: function(o) {
                var _html = '';
                _html += '<a href="#' + AD.page + '/redo/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-retweet"></i></a>';
                //Redo

                //_html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-times"></i></a>';
                return _html;
            }
        }
        */
    ];
    /* define namespace ends */

    /* model starts */
    AD.Model = Backbone.Model.extend(
    {
        urlRoot: url,
        idAttribute: idAttribute
    });
    /* model ends */

    /* collection starts */
    AD.Collection = Backbone.Collection.extend(
    {
        url: url,
        model: AD.Model
    });
    /* collection ends */
    var routes = {};

    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/redo/:_id'] = 'redo';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend(
    {
        routes: routes,
        redo: function(_id)
        {
            var ret = new AD.Model(
            {
                id: _id
            });
            ret.fetch(
            {
                async: false
            });
            ret.save(
            {
                "status": "redo"
            },
            {
                async: false
            });
            this.navigate("#" + AD.page + "/grid",
            {
                trigger: true
            });
        }
    });

    /* define bootstrap starts */
    AD.bootstrap = function()
    {
        // Multi-Language
        i18n.init(function(t)
        {
            $('[data-i18n]').i18n();
        });

        $("body").removeClass("loading");
        // alert('bootstrap');
        if(!_.isUndefined($[AD.page].app))
        {
            $[AD.page].app.navigate("#" + AD.page + "/grid",
            {
                trigger: true
            });
        }

        AD.app = new MyRouter(
        {
            "AD": AD
        });

        if(!Backbone.History.started)
        {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }

        Backbone.Events.off("TaskUpdate");
        Backbone.Events.on("TaskUpdate", function(data)
        {
            //console.log("TaskLog need to refresh!");
            $(".cctech-gv table").dataTable().fnDraw(false);
        });

        if($(".widget-body-toolbar #status").length == 0)
        {
            $(".widget-body-toolbar").addClass('row');
            $(".widget-body-toolbar").append('<div class="hidden-xs col-lg-5" id="status"></div>');
            $(".widget-body-toolbar #status").append('<div class="hidden-xs col-lg-4" style="margin-top: 5px;"><i class="fa fa-circle" style="font-size: 22px;color:#FFA500;vertical-align: middle;"></i><span style="vertical-align:middle;">&nbsp;Ready</span></div>');
            $(".widget-body-toolbar #status").append('<div class="hidden-xs col-lg-4" style="margin-top: 5px;"><i class="fa fa-circle" style="font-size: 22px;color:#2E7BCC;vertical-align: middle;"></i><span style="vertical-align:middle;">&nbsp;Running</span></div>');
            $(".widget-body-toolbar #status").append('<div class="hidden-xs col-lg-4" style="margin-top: 5px;"><i class="fa fa-circle" style="font-size: 22px;color:#66AA00;vertical-align: middle;"></i><span style="vertical-align:middle;">&nbsp;Done</span></div>');
        }
    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();

})(jQuery);
