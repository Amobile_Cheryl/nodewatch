(function($) {
    console.log('I\'m in the user_list.js');
    // Move variable AD and AD.page outside of function
    var AD = {};
    AD.title = "ns:Menu.UserList";
    AD.page = "ajax/user_list.html";
    // Add adReload function to run this script when user click this .html every time.

    var adReload = function() {
        AD.collections = {};
        AD.buttons = {};
        /*
        (Lock)  isLock
        (Active)    isActive
        User Name   id
        Reseller    domainName
        Role    roleName
        Email   email
        Last Login Time lastLoginTime
        */

        AD.buttons = [
            {
                "sExtends": "text",
                "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                "fnClick": function(nButton, oConfig, oFlash) {
                    $(".cctech-gv table").dataTable().fnDraw(false);
                }
            }
        ];
        AD.columns = [
            //columns
            // {
            //     title: '<i class="fa fa-lock"></i>',
            //     // property: 'isLock',
            //     cellClassName: 'isLock',
            //     filterable: true,
            //     sortable: true,
            //     width: '20px',
            //     callback: function(o) {
            //         if (o['isLock'] === 'Y')
            //             return '<i class="fa fa-lock"></i>';
            //         else
            //             return '<i class="fa fa-unlock-o"></i>';
            //     }
            // },
            //columns
            // {
            //     title: '(Active)',
            //     property: 'isLock',
            //     cellClassName: 'isActive',
            //     filterable: true,
            //     sortable: true,
            //     callback: function(o) {
            //         // if (o['isActive'] === 'Y')
            //         return 'Yes';
            //         // else
            //         // return 'No';
            //     }
            // },
            //columns
            // {
            //     title: 'User ID',
            //     property: 'id',
            //     cellClassName: 'id',
            //     filterable: true,
            //     sortable: true
            // },
            //columns
            {
                title: 'Image',
                filterable: true,
                sortable: false,
                width: '60px',
                callback: function(o) {
                    var html="";
                    var portraitUrl = "img/defaultPortrait.png";
                    //var portrait = o.portrait || $.common.defaultPortrait;
                    if (o.hasOwnProperty('portraitUrl') && o.portraitUrl.length > 0)
                        portraitUrl = o.portraitUrl;

                    html = html.concat('<img src="'+portraitUrl+'" style="height:40px; border-radius: 50%; -webkit-filter: drop-shadow(#9D9D9D 3px 3px 3px);" />');
                    return html;
                }
            },
            {
                title: 'ns:User.Add.Title.Name',
                property: 'nickname',
                filterable: true,
                sortable: true,
                width: '250px',
                callback: function(o) {
                    var html="";
                    var name = o.userName;
                    if (name == null)
                        name = "--";
                    html = html.concat('<a href="#ajax/user_detail.html/' + o.id + '">' + name + '</a>');
                    return html;
                }
            },
            {
                title: 'ns:User.Add.Title.Email',
                property: 'email',
                cellClassName: 'email',
                filterable: true,
                sortable: false,
                width: '300px',
                callback: function(o){
                    if (o.email != null)
                        return '<a href="mailto:'+o.email+'">'+o.email+'</a>';
                    else
                        return '--';
                }
            }, 
            {
                title: 'ns:User.Add.Title.Domain',
                property: 'domainName',
                //cellClassName: 'domainName',
                filterable: true,
                visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
                sortable: false,
                width: '80px',
                callback: function(o){
                    return o.domainName;
                }
                /*callback: function(o) {
                    return o['domain'] && o['domain']['name'] || '(no name)';
                }*/
            }, 
            {
                title: 'ns:User.Add.Title.PermissionClass',
                // property: 'domainName',
                //cellClassName: 'domainName',
                filterable: true,
                sortable: false,
                width: '200px',
                callback: function(o){
                    var permission = $.common.getPermissionClass(o.roleNameSet);
                    return "<i class=\"fa fa-star\" style=\"color:"+permission.color+"\"></i> "+permission.display;
                }                                

            },
            //columns
            /*{
                title: 'Role',
                // property: 'roleName',
                cellClassName: 'roleName',
                filterable: true,
                sortable: true,
                callback: function(o) {
                    return o['role'] && o['role']['name'] || '(no name)';
                }
            },*/
            //column
            //columns
            // {
            //     title: 'ns:User.Add.Title.Remark',
            //     property: 'remark',
            //     //cellClassName: 'domainName',
            //     filterable: true,
            //     sortable: false
            //     /*callback: function(o) {
            //         return o['domain'] && o['domain']['name'] || '(no name)';
            //     }*/
            // },
            //column
            // {
            //     title: 'Create Time',
            //     property: 'creationTime',
            //     cellClassName: 'creation_time',
            //     filterable: true,
            //     sortable: true
            //     // callback: function(o) {
            //     //     var v = new Date();
            //     //     v.setTime(o['creationTime']);
            //     //     return v.getFullYear() + '/' + (v.getMonth() + 1) + '/' + v.getDate() + ' ' + v.getHours() + ':' + (v.getMinutes()) + ':' + (v.getSeconds());
            //     // }
            // },
            //columns7
            {
                title: 'ns:User.Add.Title.Action',
                operator: true,
                sortable: false,
                width: '80px',
                visible: $.login.user.checkRole('UserOperator'),
                callback: function(o) {
                    var permission = $.common.getPermissionClass(o.roleNameSet);
                    var _html = '';
                    // _html +='<button class="btn btn-mini btn-success" title="Verify"><i class="icon-ok"></i> </button>';
                    // console.log($.login.user.userId);
                    // console.log(o.id);
                    if (o.id != $.login.user.userId && ($.login.user.permissionClass.value>permission.value || $.login.user.permissionClass==$.common.PermissionClass.SuperUserRole)) {
                        if (o) {
                            // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Edit</span></a>';
                            // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Remove</span></a>';
                            _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                            if ($.common.isDelete)
                                _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                        }
                    }
                    else{
                        if (o) {
                                // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Edit</span></a>';
                                // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Remove</span></a>';
                                _html += '<a disabled="disabled" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                                if ($.common.isDelete)
                                    _html += '<a disabled="disabled" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                        }   
                    }
                    return _html;
                }
            }
        ];

        var hash = window.location.hash;
        var domainuser = hash.search(/^#ajax\/user_list.html\/domain_user_list\/(\w+)$/);
        var url;
        // Display the list of domain users
        var breadcrumb;
        if (domainuser == 0) {
            console.log('Get domain user list...');
            var id = hash.replace(/^#ajax\/user_list.html\/domain_user_list\/(\w+)$/, "$1");
            url = $.config.server_rest_url + '/domains/' + id + '/users';
            // Modify it's titile
            // var ps = $('h1').get(0);
            // ps.innerText = 'Domain User';
            // Domain column will no longer be used
            console.log(AD.columns);
            AD.columns = $.grep(AD.columns, function(e) {
                return (e.title !== 'Domain' && !_.has(e, "operator"));
            });
            console.log(AD.columns);
            // No need add button
            AD.isAdd = false;

            // No need edit button
            /*
            var editButtonClick = function() {
                // event.preventDefault();
                var hash = window.location.hash;
                var id = hash.replace(/^#ajax\/user_list.html\/domain_user_list\/(\w+)$/, "$1");
                // alert(t.AD.addurl);
                // var addurl = t.AD.addurl || "add";
                window.location.href = '#ajax/domain_user_edit.html/'+id;
            }

            if (id != 1) {
                AD.buttons = [
                    //button
                    {
                        "sExtends": "text",
                        "sButtonText": 'Edit User List',
                        "sButtonClass": "edit-domain-user-list-btn",
                        "fnClick": editButtonClick
                    }
                ];
            }*/

            var domainName = '';
            $.ajax({
                type: "GET",
                async: false,
                url: $.config.server_rest_url + '/domains/' + id,
                success: function(data) {
                    domainName = data.name;
                },
                error: function(data) {
                    console.log('Get Error');
                    // alert('Get Domain Name Failed!!');
                    return;  
                },
            });    

            breadcrumb = "<a href=\"#ajax/domain_list.html\">"
                +"<font data-i18n=\"ns:Menu.Domain\">Domain</font>"
            +"</a>&nbsp;&gt;&nbsp;"
            +"<font>"+domainName+"</font>"
            +"&nbsp;&gt;&nbsp;"
            +"<a href=\"#"+AD.page+"\">"
                +"<font style=\"color:white\" data-i18n=\"ns:Menu.User\">User</font>"
            +"</a>";     

        } else {
            url = $.config.server_rest_url + '/users';
            if ($.login.user.checkRole('UserOperator'))
                AD.isAdd = true;
            else
                AD.isAdd = false;
        }

        $("#breadcrumbs").html(breadcrumb);
        // console.log('url is '+url);
        var idAttribute = AD.idAttribute = "id";

        AD.validate = function() {
            return true;
        };

        /* define namespace ends */

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
    };
    /* collection ends */
    
    var routes = {};
    routes[AD.page + '/domain_user_list/:_id'] = 'show_grid';
    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/edit/:_id'] = 'user_edit';
    routes[AD.page + '/add'] = 'user_add';
    routes[AD.page + '/delete/:_id'] = 'act_delete'; 
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        user_add: function() {
            this.navigate("#ajax/user_modify.html/add", {
                trigger: true
            });
        },
        user_edit: function(_id) {
            this.navigate("#ajax/user_modify.html/edit/" + _id, {
                trigger: true
            });
        },
        act_delete: function(id) {
            //event.preventDefault();
            var t = this;             
            
            $.SmartMessageBox({
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.User.DeleteTheUser")+" <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+i18n.t("ns:Message.User.DeleteTheUserContent")+"</span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                    $("body").addClass("loading");

                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: $.config.server_rest_url + '/users/' + id,
                        dataType: 'json',
                        timeout: 10000,
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log("[User] Deleted failed");
                        } 
                    }).done(function(data){
                        if (typeof(data) === 'undefined'){
                            console.log("[User] Deleted done => response undefined.");
                        } else {
                            if (data.status) {
                                $.smallBox({
                                    title: i18n.t("ns:Message.User.User") + i18n.t("ns:Message.User.DeleteSuccess"),
                                    content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.User.DeleteUser") + "</i>",
                                    // title: "[User] Deleted successfully",
                                    // content: "<i class='fa fa-times'></i> <i>Deleted User</i>",
                                    color: "#648BB2",
                                    iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                            } else {                                            
                                $.smallBox({
                                    title: i18n.t("ns:Message.User.User") + i18n.t("ns:Message.User.DeleteFailed"),
                                    content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.User.DeleteUser") + "</i>",
                                    // title: "[User] Deleted failed",
                                    // //content: "<i class='fa fa-times'></i> <i>Deleted App ID: &nbsp;" + id + "</i>",
                                    // content: "<i class='fa fa-times'></i> <i>Deleted User</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                console.log("[User] Deleted failed => " + JSON.stringify(data));
                            };
                        } 
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.User.User") + i18n.t("ns:Message.User.DeleteFailed"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.User.DeleteUser") + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                        console.log("[User] Deleted failed"); 
                    }).always(function() {
                        $("body").removeClass("loading"); 
                        // window.location.href = "#ajax/app_list.html";
                        t.navigate("#ajax/user_list.html/grid", {
                            trigger: true
                        });
                    });
                } else {
                    // window.location.href = "#ajax/app_list.html";
                    t.navigate("#ajax/user_list.html/grid", {
                        trigger: true
                    });
                }
            });              
        }      
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        if (!$.login.user.checkRole('UserViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        }
        i18n.init(function(t) {
            $('[data-i18n]').i18n();
        });
		$("body").removeClass("loading");
        adReload();

        // alert('bootstrap');
        // if (!_.isUndefined($[AD.page].app)) {
        //     $[AD.page].app.navigate("#" + AD.page + "/grid", {
        //         trigger: true
        //     });
        // }


        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();


})(jQuery);