(function($) {
    /* define namespace starts */

    var AD = {};
    AD.title = "ns:Menu.TaskReport";
	AD.page = "ajax/task_report.html";
	var colorList = {};
		colorList.ready = "bg-color-orange";
		colorList.progress = "bg-color-blueLight";
		colorList.completesuccess = "bg-color-greenLight";
		colorList.completefail = "bg-color-red";
		colorList.Timeout = "bg-color-orange";
		colorList.Completed = "bg-color-greenLight";
		colorList.Failed = "bg-color-red";

	function init(){
		chartRender(1);
		$("#period").slider().on('slideStop', function(event){
			// API Ref:http://www.eyecon.ro/bootstrap-slider/
			var period = event.value;
			chartRender(period);
		});
	}
	function showProgressBar(dataInfo){
		AD.Collection = new Array();
	
		$.each(dataInfo,function(taskType,data){
			AD.Model = new Backbone.Model();
			var collection = new Backbone.Collection({
				model: AD.Model
			});
			var total = 0;
			$.each(data,function(key,value){
				total += value.count;
			});
			$("#"+taskType+"Report").empty();
			$.each(data,function(key,value){
				//if(key.toLowerCase() !== "total"){
					var pg = new Backbone.ProgressBar({
						el:			$("#"+taskType+"Report"),
						id:			value.name,
						link:		null, // '#ajax/subtask_list.html?taskType='+taskType+"&executionStatus="+key,
						label:		value.name,
						remark:		null,
						data:		value.count + "/" + total,
						percentage:	(value.count/total*100).toFixed(2),
						color:		eval('colorList.'+value.name)
					});
					var record = {};
					record.name = value.name;
					record.counter = value.count;
					collection.add(record);
				//}
			});
			AD.Collection.push(collection);
			tableRender(taskType,collection);
		});
	}
	
	function showProgressBar_bak(){
		AD.Collection = new Array();
		//AMDM API : getDeviceReportStatisticsData -> findTaskStatisticDataForAm
		//ReportType.TaskAM
		//ReportType.TaskDM
		var taskTypes = ['ApplicationManagement','DeviceManagement'];
		
		var colorList = {};
		colorList.ready = "bg-color-orange";
		colorList.progress = "bg-color-blueLight";
		colorList.completesuccess = "bg-color-greenLight";
		colorList.completefail = "bg-color-red";
		
		for(var i=0;i<taskTypes.length;i++){
			AD.Model = new Backbone.Model();
			var collection = new Backbone.Collection({
				model: AD.Model
			});
			var total = 0;
			var taskType = taskTypes[i];
            $.ajax({
                url: $.config.server_rest_url + '/taskStatistic?taskType=' + taskType,
                type: 'GET',
                dataType: 'json',
                async: false
            }).done(function(data) {
				$.each(data,function(key,value){
					total += value;
				});
                $.each(data,function(key,value){
					if(key.toLowerCase() !== "total"){
						var pg = new Backbone.ProgressBar({
							el:			$("#"+taskType+"Report"),
							id:			key,
							link:		null, // '#ajax/subtask_list.html?taskType='+taskType+"&executionStatus="+key,
							label:		key,
							remark:		null,
							data:		value + "/" + total,
							percentage:	(value/total*100).toFixed(2),
							color:		eval('colorList.'+key.toLowerCase())
						});
						var record = {};
						record.name = key;
						record.counter = value;
						collection.add(record);
                    }
                });
				AD.Collection.push(collection);
				tableRender(taskType,collection);
            }).fail(function(){
            });
        }
	}
	
	function chartRender(period){     
        //showProgressBar();
		var data;
        $.ajax({
            type: 'GET',
			url: $.config.server_rest_url + '/taskReports/'+period,
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got task info failed. => " + data.responseText);
            }
        }).done(function(data){        
            showProgressBar(data);            
        }).fail(function(){
            //TODO          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotMonitorInfoFailed"),
                content: "<i class='fa fa-tasks'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotMonitorInfoFailed") + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Report] Got task info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        }); 
    };
	
	function tableRender(taskType,collection) {
		var reportGridHtml = "<table class=\"table table-striped noselect\" style=\"\">"
			+"<tbody>"
				+"<tr><th>"+i18n.t("ns:Report.TaskReport.TaskState")+"</th><th>"+i18n.t("ns:Report.TaskReport.NumberOfSubtasks")+"</th>"
				+"{{#each data}}"
				+"<tr><td>{{name}}</td><td>{{counter}}</td></tr>"
				+"{{/each}}"
			+"</tbody>"
		+"</table>";
		var ReportGridView = Backbone.View.extend({
			template: Handlebars.compile(reportGridHtml),
			render: function() {
				var collection = this.collection.toJSON();
				$(this.el).html(this.template({
					data: collection
				}));
				$("#"+taskType+"ReportGrid").html(this.$el);
			}
		});
		$("#"+taskType+"ReportGrid").empty();
		var reportGridView = new ReportGridView({
			collection: collection,
			reportGridHtml: reportGridHtml
		});
		reportGridView.render();
	}
	
    /* define bootstrap starts */
    AD.bootstrap = function() {

    	// Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
        Backbone.emulateHTTP = true;
        Backbone.history.start();
		$("body").removeClass("loading");
		
		init();
    };
    /* define bootstrap ends */

	$[AD.page] = AD;


})(jQuery);