(function($) {        
    var AD = {};
    AD.page = "ajax/agent_upload_windows.html";
    
    var reloadJs = function(){
        /*
         * Load bootstrap wizard dependency
         */
        loadScript("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js", runBootstrapWizard);
        
        //Bootstrap Wizard Validations
        var runBootstrapWizard = function() {
            
            var $validator = $("#wizard-1").validate({
            
                rules: {
                    operatingSystem: {
                        required: true
                    },
                    name: {
                        required: true
                    },
                    fileVersion: {
                        required: true
                    },
                    remark: {
                        required: false
                    },
                    uploadFirmware: {
                        required: true
                    }
                },
                
                messages: {
                    operatingSystem: "Please specify your file type",
                    name:            i18n.t("ns:File.Agent.Message.SpecifyFileName"),
                    fileVersion:     i18n.t("ns:File.Agent.Message.SpecifyFileVersion"),
                    remark:          "Please specify your remark",
                    uploadFirmware:  i18n.t("ns:File.Agent.Message.SpecifyFileToUpload")
                },
                
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });
            
            $("#bootstrap-wizard-1").bootstrapWizard({
                'tabClass': 'form-wizard',
                'onNext': function (tab, navigation, index) {
                    // debugger;
                    var $valid = $("#wizard-1").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    } else {
                        // Save button event                        
                        event.preventDefault();                        
                        // var originalData = {            
                        //     title:     $('input[name="title"]').val(),
                        //     name:      $('input[name="name"]').val(),
                        //     version:   $('input[name="version"]').val(),
                        //     packageId: $('input[name="packageId"]').val(),
                        //     remark:    $('input[name="remark"]').val(),                    
                        //     upload:    $('input[name="upload"]').val() || []
                        // };                        
                        // var data = JSON.stringify(originalData);
                        
                        // get upload progress
                        $("body").addClass("loading");
                        $("#wizard-1").submit(function(e)
                        {
                            var formData = new FormData(this);
                            $.ajax({
                                url:  $.config.server_host + '/upload' ,
                                type: 'POST',
                                data:  formData,
                                mimeType:"multipart/form-data",
                                dataType: 'json',
                                contentType: false,
                                cache: false,
                                processData:false,                                
                                error: function(jqXHR, textStatus, errorThrown) {
                                    console.log("[Agent] Uploaded failed");
                                }                             
                            }).done(function(data) {
                                if (typeof(data) === 'undefined'){
                                    console.log("[Agent] Uploaded done => response undefined.");
                                } else {
                                    if (data.result) {
                                        $.smallBox({
                                            title: i18n.t("ns:Message.Agent.Agent") + i18n.t("ns:Message.Agent.UploadSuccess"),
                                            content: "<i class='fa fa-cloud-upload'></i> <i>"+i18n.t("ns:Message.Agent.UploadSuccess")+"</i>",
                                            color: "#648BB2",
                                            iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                            timeout: 5000
                                        });
                                    } else {                                            
                                        $.smallBox({
                                            title: i18n.t("ns:Message.Agent.Agent") + i18n.t("ns:Message.Agent.UploadFailed"),
                                            content: "<i class='fa fa-cloud-upload'></i> <i>"+i18n.t("ns:Message.Agent.UploadFailed")+"</i>",
                                            color: "#B36464",
                                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                            timeout: 5000
                                        });
                                        console.log("[Agent] Uploaded failed => " + JSON.stringify(data));
                                    };
                                }                                
                            }).fail(function (jqXHR, textStatus){
                                $.smallBox({
                                    title: i18n.t("ns:Message.Agent.Agent") + i18n.t("ns:Message.Agent.UploadFailed"),
                                    content: "<i class='fa fa-cloud-upload'></i> <i>"+i18n.t("ns:Message.Agent.UploadFailed")+"</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                console.log("[Agent] Uploaded failed => " + textStatus);                                
                            }).always(function(){
                                $(".loadingModal").empty();
                                $("body").removeClass("loading");                              
                                window.location.href = "#ajax/agent_list.html/grid/u1";
                            });         
                            e.preventDefault(); //Prevent Default action. 
                        }); 
                        $("#wizard-1").submit(); //Submit the form                    
                        $(".loadingModal").empty();
                        $("body").addClass("loading");
                        $(".loadingModal").append('<div id="progressDiv" class="progress progress-striped active" style="width: 250px"><div id="progressBar" class="progress-bar bg-color-greenLight" role="progressbar" style="width: 0%"></div></div>');
                        $("#progressDiv").css("top","58%").css("position","relative").css("margin-left","auto").css("margin-right","auto");                     
                        setTimeout(getUploadProgress, 300);
                    }
                }
            });            
            $('#btn_back').removeClass("disabled");            
            $('#btn_save').removeClass("disabled");
        }

		runBootstrapWizard();

        // Back button event
        $("#btn_back").click(function(event){        
            event.preventDefault();        
            window.location.href = "#ajax/agent_list.html/grid/u1";        
        });        
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Translate Input of the placeholder
        translatePlaceholder();
		// Reload JavaScript
        reloadJs();        

        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/agent_upload.html\/(\w+)$/, "$1");

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/" + id, {
                trigger: true
            });
        }

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

        $("#uploadFirmware").change(function(event){
            $("#customFileInput span").text(this.files[0].name);
        });
    };
    /* define bootstrap ends */
    $[AD.page] = AD;

    // Add progress bar
    function uploadProgressHandle(uploadProgressResult) {
        try
        {                        
            var $xml = $(uploadProgressResult);
            var isNotFinished       = $xml.find("finished");
            var uploadBytesRead     = $xml.find("bytes_read").text();
            var uploadContentLength = $xml.find("content_length").text();
            var uploadPercent       = $xml.find("percent_complete").text();

            if ((isNotFinished.text() == "") && (uploadPercent == "") && (isNotFinished.length == 0)) {                            
                console.log(uploadProgressResult);                
                setTimeout(getUploadProgress, 100);
            } else {
                if (uploadPercent != "") {
                    console.log("Upload", uploadPercent + "% " + uploadBytesRead + " of " + uploadContentLength + " bytes read");
                    $("#progressBar").attr("style","width: " + uploadPercent + "%");
                    setTimeout(getUploadProgress, 100);
                } else {
                    console.log(uploadProgressResult);            
                    if ($(".loading").length != 0) {
                        $("#progressBar").attr("style","width: 100%");
                        setTimeout(function() {
                            $("#progressBar").text(i18n.t("ns:Message.Processing"));
                        }, 2000);
                    };                    
                }
            }
        } catch (e) {
            console.log(e.message);
        }
    };

    // Get upload progress
    function getUploadProgress() {
        var url = $.config.server_host + "/upload";
            $.ajax({
                url: url,
                type: 'GET',
                dataType: 'xml',
                crossDomain: true,
            success: function(response){              
                uploadProgressHandle(response);
            },
            error: function(jqXHR, textStatus){
                console.log("[Agent] getUploadProgress failed => " + textStatus);
                $(".loadingModal").empty();
                $("body").removeClass("loading");                              
                window.location.href = "#ajax/agent_list.html/grid/u1";
            }
        });
    }

    function translatePlaceholder () {
        $("#name").attr("placeholder", i18n.t("ns:File.Agent.Message.Name"));
        $("#fileVersion").attr("placeholder", i18n.t("ns:File.Agent.Message.Version"));
        $("#remark").attr("placeholder", i18n.t("ns:File.Agent.Message.Remark"));
    }
})(jQuery);
