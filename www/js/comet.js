/*
http://140.92.61.174:8080/mdm-core/CwmpEventTest?msg={"taskId":"2034","groupID":"85","executionStatus":"20/30"}
*/
window.cometFailCount = 0;
window.isStopServerPush = true;
window.comet = {};
window.comet.isCometError = false;
window.comet.failCount = 0;
window.comet.retryTimeout = 5 * 1000;

function connectComet()
{
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for(var i = 0; i < string_length; i++)
    {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    var servleturl = $.config.server_host + "/ServerPushCometServlet?";
    var url = servleturl.concat(randomstring);
    //console.log("[Comet] url=[" + url + "]");

    $.ajax(
    {
        url: url,
        type: 'get',
        crossDomain: true,
        timeout: 0
    }).done(function(data, textStatus, errorThrown)
    {
        window.comet.isCometError = false;
        window.comet.failCount = 0;
        window.comet.retryTimeout = 5 * 1000;
        if(data.length > 0)
        {
            window.console.log("[Comet] Received message=" + data);
            serverEventHandler(data);
        }
    }).fail(function(jqXHR, textStatus, errorThrown)
    {
        console.log("[Comet] textStatus=[" + textStatus + "]");
        console.log("[Comet] jqXHR.status=[" + jqXHR.status + "]");
        if(textStatus === "timeout")
        {
            window.comet.isCometError = false;
            window.comet.failCount = 0;
            window.comet.retryTimeout = 5 * 1000;
        }
        else
        {
            window.comet.isCometError = true;
            window.comet.failCount++;
            console.log("[Comet] Retry count: " + window.comet.failCount);
        }
    }).always(function()
    {
        if(window.comet.isCometError)
        {
            if(window.comet.failCount > 12)
            {
                $.ajax(
                {
                    url: $.config.server_rest_url + '/mySessionData',
                    type: 'GET',
                    dataType: 'json',
                    error: function(data, status, error)
                    {
                        window.location.href = "login.html";
                    }
                }).done(function(data)
                {
                    //var retryDelayTime = window.comet.retryTimeout + window.comet.failCount * 1000;
                    var retryDelayTime = window.comet.retryTimeout;
                    console.log("[Comet] Retry, wait " + (retryDelayTime / 1000) + " seconds");
                    setTimeout(function()
                    {
                        console.log('[Comet] Reconnect');
                        connectComet()
                    }, retryDelayTime);
                }).fail(function()
                {
                    window.location.href = "login.html";
                });
            }
            else
            {
                //var retryDelayTime = window.comet.retryTimeout + window.comet.failCount * 1000;
                var retryDelayTime = window.comet.retryTimeout;
                console.log("[Comet] Retry, wait " + (retryDelayTime / 1000) + " seconds");
                setTimeout(function()
                {
                    console.log('[Comet] Reconnect');
                    connectComet()
                }, retryDelayTime);
            }
        }
        else
        {
            setTimeout(function()
            {
                console.log('[Comet] Reconnect');
                connectComet();
            }, 300);
        }
    });
}

function connectComet_bak()
{
    var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for(var i = 0; i < string_length; i++)
    {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    var servleturl = $.config.server_host + "/ServerPushCometServlet?";
    var url = servleturl.concat(randomstring);

    $.ajax(
    {
        url: url,
        type: 'get',
        crossDomain: true,
        //timeout : 0,
        success: function(response)
        {
            window.comet.isCometError = false;
            window.comet.failCount = 0;
            window.comet.retryTimeout = 5 * 1000;
            window.console.log(response);
            if(response.length > 0)
            {
                serverEventHandler(response);
            }
        },
        error: function()
        {
            window.comet.isCometError = true;
            window.comet.failCount++;
            console.log("Comet retry:" + window.comet.failCount);
            if(window.comet.failCount > 10)
            {
                $.ajax(
                {
                    url: $.config.server_rest_url + '/mySessionData',
                    type: 'GET',
                    dataType: 'json',
                    error: function(data, status, error)
                    {
                        window.location.href = "login.html";
                    }
                }).done(function(data)
                {
                    var retryDelayTime = window.comet.retryTimeout + window.comet.failCount * 1000;
                    console.log("Retry, wait " + retryDelayTime + " seconds");
                    setTimeout(function()
                    {
                        connectComet()
                    }, retryDelayTime);
                }).fail(function()
                {
                    window.location.href = "login.html";
                });
            }
            else
            {
                var retryDelayTime = window.comet.retryTimeout + window.comet.failCount * 1000;
                console.log("Retry, wait " + retryDelayTime + " seconds");
                setTimeout(function()
                {
                    connectComet()
                }, retryDelayTime);
            }
        },
        complete: function()
        {
            /*
            if (window.comet.isCometError) {
                // if a connection problem occurs, try to reconnect each 5 seconds
                var retryDelayTime = window.comet.retryTimeout+window.comet.failCount*250;
                console.log("Retry, wait "+retryDelayTime+" seconds");
                setTimeout(function(){connectComet()}, retryDelayTime);
            } else {
                connectComet();
            }
            */
            if(!window.comet.isCometError)
            {
                setTimeout(function()
                {
                    connectComet()
                }, 100);
            }
        }

    });
}

// Fail
function serverPush(url)
{
    if(window.isStopServerPush) return;
    try
    {
        var chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
        var string_length = 8;
        var randomstring = '';
        for(var i = 0; i < string_length; i++)
        {
            var rnum = Math.floor(Math.random() * chars.length);
            randomstring += chars.substring(rnum, rnum + 1);
        }
        var servleturl = $.config.server_host + "/ServerPushCometServlet?";
        var url = servleturl.concat(randomstring);
        var request;

        if(window.XDomainRequest)
        {
            request = new XDomainRequest();
        }
        else if(window.XMLHttpRequest)
        {
            request = new XMLHttpRequest();
        }
        else if(window.ActiveXObject)
        {
            var aVersions = ["Msxml2.XMLHttp.6.0", "Msxml2.XMLHttp.5.0",
                "Msxml2.XMLHttp.4.0", "Msxml2.XMLHttp.3.0",
                "Msxml2.XMLHttp", "Microsoft.XMLHttp"
            ];
            for(var i = 0; i < aVersions.length; i++)
            {
                try
                {
                    request = new ActiveXObject(aVersions[i]);
                    break;
                }
                catch(e)
                {

                }
            }
        }
        else
        {
            request = new XMLHttpRequest();
        }

        //request.withCredentials = true;
        request.open("GET", url, true);
        request.setRequestHeader("Content-Type", "application/x-javascript;");
        request.setRequestHeader("Access-Control-Allow-Origin", "*");
        request.setRequestHeader("Access-Control-Allow-Credentials", "true");
        //request.setRequestHeader('Access-Control-Allow-Methods', 'PUT, GET, POST, DELETE, OPTIONS');
        //request.setRequestHeader('Access-Control-Allow-Headers', 'Content-Type');
        request.onreadystatechange = function()
        {
            if(request.readyState == 4)
            {
                if(request.status == 200)
                {
                    //Message success responsed.
                    window.console.log("============== Comet Receive ==============");
                    window.console.log(request.responseText);
                    if(request.responseText.length > 0)
                    {
                        serverEventHandler(request.responseText);
                    }
                    window.cometFailCount = 0;
                }
                else if(request.status == 0)
                {
                    window.cometFailCount++;
                    window.console.log("Connect to event push server failed, Retry: " + window.cometFailCount);
                }
                if(window.cometFailCount > 10)
                {
                    onCometFail();
                }
                else
                {
                    //Reconnect
                    serverPush();
                }
            }
        };
        request.send(null);
    }
    catch(e)
    {
        window.console.log("comet error: " + e.message);
    }
}

/*
DeviceTaskUpdate
DeviceAMResult
UserLogUpdate
SystemLogUpdate
TaskUpdate
SubTaskUpdate
*/

function serverEventHandler(sEvent)
{
    sEvent = JSON.parse(sEvent);
    // {"eventType":"TaskStatusChanged","taskId":790,"taskType":"DeviceManagement","job":"UnlockDevice","status":"Progress","deviceGroupId":null,"appId":null}
    try
    {
        console.log("check="+$("#s5").length );
        Backbone.Events.trigger("DeviceTaskUpdate",{"deviceId":1056});

        if(typeof(sEvent.eventType) !== "undefined")
        {
            switch(sEvent.eventType)
            {
                case 'DeviceEvent':
                    break;
                case 'CreateTask':
                    onCreateTaskEvent(sEvent);
                    break;
                case 'AlertUpdate':
                    break;
                case 'EventUpdate':
                    break;
                case 'ProgressData':
                    break;
                case 'DeviceLock':
                    break;
                case 'DeviceAMResult':
                    break;
                case 'fileCountChanged':
                    if($("#status0").length > 0)
                    {
                        setTimeout("fetchFileCount()", 2000);
                        console.log("[Comet] Do dashboard page fileCountChanged");
                    }
                    break;
                case 'onlineUserCountChanged':
                    if($("#status0").length > 0)
                    {
                        setTimeout("fetchOnlineUserCount()", 2000);
                        Backbone.Events.trigger("UserLogUpdate");
                        console.log("[Comet] Do dashboard page onlineUserCountChanged");
                    }
                    break;
                case 'deviceCountChanged':
                    if($("#status0").length > 0)
                    {
                        setTimeout("fetchDeviceCount()", 2000);
                        console.log("[Comet] Do dashboard page deviceCountChanged");
                    }
                    break;
                case 'taskCountChanged':
                    if($("#status0").length > 0)
                    {
                        setTimeout("fetchTaskCount()", 2000);
                        console.log("[Comet] Do dashboard page taskCountChanged");
                    }
                    break;
                case "D" + mqtt.forcusDevice + "/taskChanged":
                    if($("#s5").length > 0)
                    {    
                        Backbone.Events.trigger("DeviceTaskUpdate");
                        console.log("[Comet] Refresh Page for Device Management.Device.Task");
                    }
                    break;
                case "G" + mqtt.focus.groupId + "/taskChanged":
                    if($("#s5").length > 0)
                    {
                        Backbone.Events.trigger("TaskUpdate");
                        console.log("[Comet] Refresh Page for Device Management.Group.Task");
                    }
                    break;
                case 'AppFileChanged':
                    fnRefreshDataForAppFile();
                    break;
                case 'FirmwareFileChanged':
                    fnRefreshDataForFirmwareFile();
                    break;
                case 'AgentFileChanged':
                    fnRefreshDataForAgentFile();
                    break;
                default:
                    //cometPopup(sEvent);
                    break;
            }
            Backbone.Events.trigger(sEvent.eventType, sEvent);
        }
        else
        {
            //cometPopup(sEvent);
            Backbone.Events.trigger("ServerMsg", sEvent);
        }
    }
    catch(err)
    {
        //
    }
}

function onCreateTaskEvent(sEvent)
{
    Backbone.Events.trigger("CreateTask");
    if(typeof(sEvent.groupId !== "undefined"))
    {
        // {"eventType":"CreateTask","groupId":85,"taskId":600}
        var group_id = sEvent.groupId;
        //var taskConsoleData = getCookie("group_task_"+group_id);
        var taskConsoleData = sessionStorage.getItem("group_task_" + group_id);
        var realTimeTaskData = new Array();
        if(taskConsoleData === "")
        {
            //setCookie("group_task_"+group_id,JSON.stringify(new Array()));
            sessionStorage.setItem("group_task_" + group_id, JSON.stringify(new Array()));
        }
        else
        {
            realTimeTaskData = JSON.parse(taskConsoleData);
            // 2014/12/05 Limit storage size
            if(realTimeTaskData.length > 30)
            {
                // find finish task
                var finishTask = _.find(realTimeTaskData, function(realTimeTaskData)
                {
                    if(realTimeTaskData.executeStatus === realTimeTaskData.total)
                    {
                        return realTimeTaskData;
                    }
                });
                if(_.isUndefined(finishTask))
                {
                    // if not found, remove oldest data
                    realTimeTaskData = _.without(realTimeTaskData, _.first(realTimeTaskData));
                }
                else
                {
                    // if found, remove finish task
                    realTimeTaskData = _.without(realTimeTaskData, finishTask);
                }
            }
        }
        var total = sEvent.subtaskCount;
        var percent = 0;
        /*
        var progressHtml = "<div>"
                                +"<div class=\"col col-lg-4 col-md-4 col-sm-4 col-xs-4\">"
                                    +"0/"+total
                                +"</div>"
                                +"<div class=\"col col-lg-8 col-md-8 col-sm-8 col-xs-8\">"
                                    +"<div class=\"progress progress-striped active\">"
                                        +"<div class=\"progress-bar bg-color-greenLight\" aria-valuetransitiongoal=\"0\" style=\"width: 0%\" aria-valuenow=\"0\">0%</div>"
                                    +"</div>"
                                +"</div>"
                            +"</div>";
        */
        //realTimeTaskData.push({'id':sEvent.taskId,'job':sEvent.jobType,'creationTime':sEvent.creationTime,'executeStatus':progressHtml,'total':total});
        realTimeTaskData.push(
        {
            'id': parseInt(sEvent.taskId),
            'job': sEvent.jobType,
            'creationTime': sEvent.creationTime,
            'executeStatus': 0,
            'total': total
        });
        //setCookie("group_task_"+group_id,JSON.stringify(realTimeTaskData));
        sessionStorage.setItem("group_task_" + group_id, JSON.stringify(realTimeTaskData));
    }
}

function cometPopup(sEvent)
{
    var msg = "";
    for(var k in sEvent)
    {
        msg += "<p>" + k + ": " + sEvent[k] + "</p>";
    }
    $.bigBox(
    {
        title: "Server information",
        content: msg,
        color: "#C79121",
        icon: "fa fa-bell fadeInRight animated",
        timeout: 25000
    });
}

function onCometFail()
{
    // window.location.href = "login.html";
}