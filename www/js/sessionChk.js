(function($)
{
    $.ajaxPrefilter(function(options, originalOptions, jqXHR)
    {
        options.crossDomain = {
            crossDomain: true
        };
        options.xhrFields = {
            withCredentials: true
        };
    });
})(jQuery);

(function($)
{
    $.login = {};
    $.login.app = null;
    $.login.user = null;
    $.login.location = {};
    $.login.location.hostname = window.location.hostname;
    $.login.location.pathname = window.location.pathname;
    $.login.href = window.location.href;
    $.login.url = $.config.server_rest_url + '/login';
    $.login.session = $.config.server_rest_url + '/mySessionData';
    /* model starts */
    //user model
    $.login.UserModel = Backbone.Model.extend(
    {
        //urlRoot: 'api/order/',
        urlRoot: $.login.session,
        idAttribute: "userId"
    });
    /* model ends */
    /* collection starts */
    $.login.UserCollection = Backbone.Collection.extend(
    {
        url: $.login.session,
        model: $.login.UserModel
    });
    /* collection ends */
    $.login.Router = Backbone.Router.extend(
    {
        routes:
        {
            "logout": "logout"
        },
        logout: function()
        {
            //console.log("logout");
            var user = $.login.user;
            // alert( user.get("AccID") );
            $.ajax(
            {
                url: $.login.session,
                type: 'POST',
                dataType: 'json',
                headers:
                {
                    "X-HTTP-Method-Override": "DELETE"
                },
                async: false,
                timeout: 10000,
            }).done(function(data)
            {
                // window.location.href="index.html";
            }).fail(function()
            {
                // alert("Logout failure");
                // console.log("Logout failure");
                window.location.href = "index.html";
            });
            // alert("hello");
            window.location.href = "login.html";
            // window.location.Reload();
        }
    });

    function pageDirection()
    {
        /*if (localStorage.userName!=null && !$.login.location.pathname.match("everLogin.html")) {
        	window.location.href = "everLogin.html";
        } else if (localStorage.userName!=null && $.login.location.pathname.match("everLogin.html")) {
        	//
        } else */
        if(!$.login.location.pathname.match("login.html"))
        {
            window.location.href = "login.html";
            // window.location.reload();
        }
    }

    $.checkLogin = function()
    {
        $.ajax(
        {
            url: $.config.server_rest_url + '/mySessionData',
            type: 'GET',
            dataType: 'json',
            async: false,
            error: function(data, status, error)
            {
                pageDirection();
            }
        }).done(function(data)
        {
            if(_.isUndefined(data.sessionId))
            {
                pageDirection();
            }
            else
            {
                $.login.app = new $.login.Router();
                $.login.user = data;
                $.login.user.permissionClass = $.common.getPermissionClass($.login.user.roleNameList);
                $.login.user.checkRole = function(roleName)
                {
                    if(($.inArray(roleName, $.login.user.roleNameList) > -1) || ($.login.user.permissionClass.value === $.common.PermissionClass.SuperUserRole.value))
                    // if ($.inArray(roleName, $.login.user.roleNameList) > -1 || $.login.user.permissionClass.value == $.common.PermissionClass.SuperUserRole.value)
                        return true;
                    else
                        return false;
                };
                
                $.login.user.setRole = function(roleName)
                {
                    $.login.user.roleNameList.push(roleName);
                };
                if($.browser.webkit)
                {
                    // H:
                    //console.log('user\'s permission class is');
                    //console.log($.login.user.permissionClass);
                }
                if(_.isUndefined($.login.user.permissionClass))
                {
                    return;
                }
                // Save cookie
                // if ($.cookie('rememberme')=='true')
                //     $.cookie('userName',$.login.user.userName);
                if(localStorage.getItem('rememberme') == 'true')
                {
                    localStorage.setItem('userName', $.login.user.userName);
                    localStorage.setItem('portrait', $.login.user.portrait);
                }
                if($.login.location.pathname.match("login.html") || $.login.location.pathname.match("everLogin.html"))
                {
                    window.location.href = "index.html";
                    // window.location.href=$.login.user.HomePage || "support.html";
                    // window.location.reload();
                }
                //console.log("Idle: "+ idleTime +" sec.");
            }
        }).fail(function()
        {
            pageDirection();
        });
    }
})(jQuery);
