(function($) {
    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /* define namespace starts */
    var AD = {};
    AD.page = "ajax/app_list.html";       

    // Windows
    function s1() {
        var AD = {};
        AD.page = "ajax/app_list.html";
        AD.collections = {};
        var url = $.config.server_rest_url +"/windowsAppFiles";
        var idAttribute = AD.idAttribute = "id";       

        if ($.login.user.checkRole('AppFileOperator'))
            AD.buttons = [
                //button
                {
                    "sExtends" : "text",
                    "sButtonText" : "<i class='fa fa-cloud-upload'></i>&nbsp; " + i18n.translate("ns:File.Upload"),
                    "sButtonClass" : "upload-btn add-btn btn-default txt-color-white bg-color-blue"
                },
                {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash) {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
            ];

        AD.columns = [
            {
                title : 'Icon',
                filterable : false,
                sortable : false,
                callback: function(o) {
                    if (o.image == null || o.image == "")
                        return '<img src="img/file/windows-icon.png">';                       
                    else
                        return '<img src="' + o.image + '" style="height:35px; width:35px; border-radius: 15%;" onerror=$(this).attr("src","img/file/android-icon.png") />';                    
                }                
            },
            {
                title : 'ns:File.App.Title.Name',
                property : 'label',
                cellClassName : 'label',
                filterable : false,
                sortable : true,               
            },
            {
                title : 'ns:File.App.Title.Domain',
                property : 'domainName',
                cellClassName : 'domainName',
                filterable : false,
                visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
                sortable : false
            },{
                title : 'ns:File.App.Title.Creator',
                property : 'creatorName',
                cellClassName : 'creatorName',
                filterable : false,
                sortable : false
            },{
                title : 'ns:File.App.Title.Version',
                property : 'version',
                cellClassName : 'version',
                filterable : false,
                sortable : false
            },{
                title : 'ns:File.App.Title.FileSize',
                property : 'size',
                cellClassName : 'size', 
                filterable : false,
                sortable : false,
                callback : function (o) {
                    if (o.size == null) {
                        return "--";                    
                    } else {
                        var fileSize = o.size.toFixed(0) + " Bytes";
                        
                        if (o.size > 1024 && o.size < (1024 * 1024)) {
                            fileSize = (o.size / 1024).toFixed(0) + " KB";
                        } else if (o.size > 1024 * 1024) {
                            fileSize = (o.size / (1024 * 1024)).toFixed(1) + " MB";
                        }
                        return fileSize;
                    }
                }
            },{
                title : 'ns:File.App.Title.UploadedTime',
                property : 'creationTime',
                cellClassName : 'creationTime',            
                filterable : false,
                sortable : true
            },/*{
                title : 'ns:File.App.Title.Remark',
                property : 'remark',
                cellClassName : 'remark',
                filterable : false,
                sortable : false, 
                callback : function (o) {
                    if (o.remark == null) {
                        return "--";                    
                    } else {
                        return o.remark;
                    }
                }
            },*/
            //columns Action
            {
                title : 'ns:File.App.Title.Action',
                sortable : false,
                width: '100px',
                visible: $.login.user.checkRole('AppFileOperator'),
                callback : function (o) {
                    var _html = '';
                    if (o) {
                        // [Action] Download the app
                        _html += '<a href="'  + o.fileServerPath + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-teal" style="display: inline;"><i class="fa fa-cloud-download"></i></a>' + "&nbsp;";                            
                        // [Action] Delete the app
                        if ($.common.isDelete)
                            _html += '<a id="delete" href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';    
                    }                
                    return _html;
                }
            }            
        ];
        /* define namespace ends */

        


        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        }); 
        /* collection ends */

        var routes = {};
        routes[AD.page + '/delete/:_id'] = 'act_delete';    // [Action] Delete the File
        routes[AD.page + '/grid/s1']     = 'act_s1';        // [Action] Act to s1
        routes[AD.page] = 'render';

        var MyRouter = Backbone.DG1.extend({        
            routes: routes,
            initialize: function(opt) {            
                var t = this;
                //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
                Backbone.DG1.prototype.initialize.call(t, opt);
                t.$bt = $(opt.bts).first();
                console.error();
                this.show_grid();
            },
            show_grid: function() {
                var t = this;

                var gvs = "#s1 .cctech-gv";
                var gv = $(gvs).first();
                
                if(!windowsGrid) {
                    var windowsGrid = new Backbone.GridView({
                        el: t.$gv,
                        collection: new t.AD.Collection(),
                        buttons: t.AD.buttons,
                        columns: t.AD.columns,
                        AD: t.AD
                    });

                    gv.on("click", ".add-btn", function (event) {
                        event.preventDefault();

                        var hash = window.location.hash;
                        var url = location.hash.replace(/^#/, '');
                        
                        var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                        url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');                       
                        
                        t.navigate("#ajax/app_upload_windows.html", {
                            trigger: true
                        });
                    });
                } else {
                    windowsGrid.refresh();
                }
                t.$gv.show();
                //pageSetUp();
            },
            act_delete: function(id) {
                //event.preventDefault();
                var t = this;             
                
                $.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp;"+i18n.t("ns:Message.App.DeleteWindowsApp")+"<span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>" + i18n.t("ns:Message.App.DeleteAppContent") + "</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                        $("body").addClass("loading");

                        $.ajax({                
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/windowsAppFiles/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[Windows App] Deleted failed");
                            } 
                        }).done(function(data){
                            if (typeof(data) === 'undefined'){
                                console.log("[Windows App] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.App.WindowsApp") + i18n.t("ns:Message.App.DeleteSuccess"),
                                        content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.App.DeleteApp") + "</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {                                            
                                    $.smallBox({
                                        title: i18n.t("ns:Message.App.WindowsApp") + i18n.t("ns:Message.App.DeleteFailed"),
                                        //title: "[Windows App] DeleteFailed",
                                        //content: "<i class='fa fa-times'></i> <i>Deleted App ID: &nbsp;" + id + "</i>",
                                        content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.App.DeleteApp") + "</i>",
                                        //content: "<i class='fa fa-times'></i> <i>Deleted App</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[Windows App] Deleted failed => " + JSON.stringify(data));
                                };
                            } 
                        }).fail(function(){
                            $.smallBox({
                                title: i18n.t("ns:Message.App.WindowsApp") + i18n.t("ns:Message.App.DeleteFailed"),
                                content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.App.DeleteApp") + "</i>",
                                //title: "[Windows App] Deleted failed",
                                // content: "<i class='fa fa-times'></i> <i>Deleted App</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                            console.log("[Windows App] Deleted failed"); 
                        }).always(function() {
                            $("body").removeClass("loading"); 
                            // window.location.href = "#ajax/app_list.html";
                            t.navigate("#ajax/app_list.html/grid/s1", {
                                trigger: true
                            });
                        });
                    } else {
                        // window.location.href = "#ajax/app_list.html";
                        t.navigate("#ajax/app_list.html/grid/s1", {
                            trigger: true
                        });
                    }
                });              
            },
            act_s1: function(id) {
                s1();
            }
        });

        pageSetUp();

        AD.app = new MyRouter({
            "AD": AD
        });
    };

    // Android
    function s2() {        
        var AD = {};
        AD.page = "ajax/app_list.html";
        AD.collections = {};
        var url = $.config.server_rest_url +"/androidAppFiles";
        var idAttribute = AD.idAttribute = "id";
        if ($.login.user.checkRole('AppFileOperator'))
            AD.buttons = [
                //button
                {
                    "sExtends" : "text",
                    "sButtonText" : "<i class='fa fa-cloud-upload'></i>&nbsp; " + i18n.translate("ns:File.Upload"),
                    "sButtonClass" : "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
                },
                {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash) {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
            ];

        AD.columns = [
            {
                title : 'Icon',
                filterable : false,
                sortable : false,
                callback: function(o) {
                    if (o.iconUrl == null || o.iconUrl == "")
                        return '<img src="img/file/android-icon.png">';                       
                    else
                        return '<img src="' + o.iconUrl + '" style="height:35px; width:35px; border-radius: 15%;" onerror=$(this).attr("src","img/file/android-icon.png") />';                    
                }                
            },
            {
                title : 'ns:File.App.Title.Name',
                property : 'label',
                cellClassName : 'label',
                filterable : false,
                sortable : true,               
            },
            {
                title : 'ns:File.App.Title.Domain',
                property : 'domainName',
                cellClassName : 'domainName',
                filterable : false,
                visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
                sortable : false
            },{
                title : 'ns:File.App.Title.Creator',
                property : 'creatorName',
                cellClassName : 'creatorName',
                filterable : false,
                sortable : false
            },{
                title : 'ns:File.App.Title.Version',
                property : 'version',
                cellClassName : 'version',
                filterable : false,
                sortable : false
            },{
                title : 'ns:File.App.Title.FileSize',
                property : 'size',
                cellClassName : 'size', 
                filterable : false,
                sortable : false,
                //width: '120px',
                callback : function (o) {
                    if (o.size == null) {
                        return "--";                    
                    } else {
                        var fileSize = o.size.toFixed(0) + " Bytes";
                        
                        if (o.size > 1024 && o.size < (1024 * 1024)) {
                            fileSize = (o.size / 1024).toFixed(0) + " KB";
                        } else if (o.size > 1024 * 1024) {
                            fileSize = (o.size / (1024 * 1024)).toFixed(1) + " MB";
                        }
                        return fileSize;
                    }
                }
            },
            // {
            //     title : 'ns:File.App.Title.UploadedTime',
            //     property : 'creationTime',
            //     cellClassName : 'creationTime',            
            //     filterable : false,
            //     sortable : true
            // },
            {
                title : 'ns:File.App.Title.PackageId',
                property : 'name',
                cellClassName : 'name',            
                filterable : false,
                sortable : true
            },
            /*{
                title : 'ns:File.App.Title.Remark',
                property : 'remark',
                cellClassName : 'remark',
                filterable : false,
                sortable : false, 
                callback : function (o) {
                    if (o.remark == null) {
                        return "--";                    
                    } else {
                        return o.remark;
                    }
                }
            },*/
            //columns Action
            {
                title : 'ns:File.App.Title.Action',
                sortable : false,
                visible: $.login.user.checkRole('AppFileOperator'),
                //width: '100px',
                callback : function (o) {
                    var _html = '';
                    if (o) {
                        // [Action] Download the app
                        _html += '<a href="'  + o.fileServerPath + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-teal" style="display: inline;"><i class="fa fa-cloud-download"></i></a>' + "&nbsp;";                            
                        // [Action] Delete the app
                        if ($.common.isDelete)
                            _html += '<a id="delete" href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';    
                    }                
                    return _html;
                }
            }            
        ];
        /* define namespace ends */

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });         
        /* collection ends */       
        
        var routes = {};
        routes[AD.page + '/delete/:_id'] = 'act_delete';   // [Action] Delete the File   
        routes[AD.page + '/grid/s2']     = 'act_s2';       // [Action] Act to s2
        routes[AD.page] = 'render';     

        var MyRouter = Backbone.DG1.extend({        
            routes: routes,
            initialize: function(opt) {             
                var t = this;
                //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
                Backbone.DG1.prototype.initialize.call(t, opt);
                t.$bt = $(opt.bts).first();
                console.error();
                this.show_grid();
            },
            show_grid: function() {
                var t = this;

                var gvs = "#s1 .cctech-gv";
                var gv = $(gvs).first();
                
                if(!androidGrid) {                    
                    var androidGrid = new Backbone.GridView({
                        el: t.$gv,
                        collection: new t.AD.Collection(),
                        buttons: t.AD.buttons,
                        columns: t.AD.columns,
                        AD: t.AD
                    });

                    gv.on("click", ".s2-btn", function (event) {
                        event.preventDefault();

                        var hash = window.location.hash;
                        var url = location.hash.replace(/^#/, '');
                        
                        var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                        url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');                       
                        
                        t.navigate("#ajax/app_upload_android.html", {
                            trigger: true
                        });
                    });
                } else {
                    androidGrid.refresh();
                }
                t.$gv.show();
                //pageSetUp();
            },
            act_delete: function(id) {
                //event.preventDefault();             
                var t = this;

                $.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+ i18n.t("ns:Message.App.DeleteAndroidApp") +" <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>" + i18n.t("ns:Message.App.DeleteAppContent") + "</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                        $("body").addClass("loading");

                        $.ajax({                
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/androidAppFiles/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[Android App] Deleted failed");
                            }
                        }).done(function(data){
                            if (typeof(data) === 'undefined'){
                                console.log("[Android App] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.App.AndroidApp") + i18n.t("ns:Message.App.DeleteSuccess"),
                                        content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.App.DeleteApp") + "</i>",
                                        // title: "[Android App] Deleted successfully",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted App</i>",
                                        // title: "[Android App] Deleted successfully",
                                        // // content: "<i class='fa fa-times'></i> <i>Deleted App ID: &nbsp;" + id + "</i>",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted App</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {                                            
                                    $.smallBox({
                                        title: i18n.t("ns:Message.App.AndroidApp") + i18n.t("ns:Message.App.DeleteFailed"),
                                        content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.App.DeleteApp") + "</i>",
                                        // title: "[Android App] Deleted failed",
                                        // // content: "<i class='fa fa-times'></i> <i>Deleted App ID: &nbsp;" + id + "</i>",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted App</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[Android App] Deleted failed => " + JSON.stringify(data));
                                };
                            } 
                        }).fail(function(){
                            $.smallBox({
                                title: i18n.t("ns:Message.App.AndroidApp") + i18n.t("ns:Message.App.DeleteFailed"),
                                content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.App.DeleteApp") + "</i>",
                                // title: "[Android App] Deleted failed",
                                // // content: "<i class='fa fa-times'></i> <i>Deleted App ID: &nbsp;" + id + "</i>",
                                // content: "<i class='fa fa-times'></i> <i>Deleted App</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                            console.log("[Android App] Deleted failed"); 
                        }).always(function() {
                            $("body").removeClass("loading");                                
                            //window.location.href = "#ajax/app_list.html";
                            t.navigate("#ajax/app_list.html/grid/s2", {
                                trigger: true
                            });
                        });
                    } else {                        
                        t.navigate("#ajax/app_list.html/grid/s2", {
                            trigger: true
                        });
                    }
                }); 
            },            
            act_s2: function(id) {
                s2();
            }            
        });

        pageSetUp();

        AD.app = new MyRouter({
            "AD": AD
        });
    };
     
    AD.bootstrap = function() {
        i18n.init(function(t){
            $('[data-i18n]').i18n();            
        });     

        if (!$.login.user.checkRole('AppFileViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        };

        $("#myTab li").click(function() {            
            switch (this.firstElementChild.id) {
                case "s1": //Windows
                    s1();
                    break;
                case "s2": //Android
                    s2();
                    break;
                case "s3": //iPhone
                    s3();
                    break;                
            }
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");
        
        // Initial page & after uploading
        var hash = window.location.hash;
        var url = location.hash.replace(/^#/, '');        
        var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');        

        switch (action) {
            case "grid/u1": //Windows
            case "grid/s1":
                $("#tab2").removeClass('active');
                $("#tab1").addClass('active');
                s1();
                break;
            case "grid/u2": //Android
            case "grid/s2":
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                s2();
                break;
            // case "grid/u3": //iPhone
            // case "grid/s3":
            //     $("#tab1").removeClass('active');
            //     $("#tab2").removeClass('active');
            //     $("#tab3").addClass('active');
            //     s3();
            //     break;
            default:    //Default
                s2();
                break;
        }
    };
    $[AD.page] = AD;
    var fnRefreshDataForAppFile = function() 
    {
        if ($('#divAppFileMain').length > 0) 
        {
            console.log("[Data Refresh] App File data refreshed.");
            $(".cctech-gv table").dataTable().fnDraw(false);
        }   
    }

    // console.log('oRefreshData.oAppFile.bIsEnable='+oRefreshData.oAppFile.bIsEnable)
    // console.log('oRefreshData.oAppFile.oTimerId='+oRefreshData.oAppFile.oTimerId)
    if (oRefreshData.oAppFile.bIsEnable === true && oRefreshData.oAppFile.oTimerId === null)
    {
        oRefreshData.oAppFile.oTimerId = window.setInterval(fnRefreshDataForAppFile, oRefreshData.oAppFile.iInterval);
        // console.log('oRefreshData.oAppFile.oTimerId='+oRefreshData.oAgentFile.oTimerId)
    }    
    
})(jQuery);

function defaultAppIcon(faOperatingSystem){
	return "<div style=\""
				//+"-webkit-border-radius: 10px;  -moz-border-radius: 10px;  border-radius: 10px;"
				+"width: 31px;"
				+"height: 35px;"
				+"background-color: #3678DB;"
				+"text-align: center;"
				+"line-height: 35px;"
				+"margin-right: 12px;"
				+"float:left;"
			+"\">"
				+"<i class=\"fa "+faOperatingSystem+" fa-inverse\" style=\"font-size: 18px;\"></i>"
			+"</div>";
}