(function($) {
    var AD = {};
    AD.page = "ajax/group_event_list.html";
    var id;
    var selectedEvents = [];
    var deselectedEvents = [];
    var adReload = function() 
    {
        AD.collections = {};
        AD.buttons = {};
        AD.columns = [
            {
                title: '<div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-devices"><i></i>&nbsp;&nbsp;&nbsp;&nbsp;</label></div>',
                sortable: false,
                callback: function(o) {
                    if ($.inArray(o.id, selectedEvents) >= 0)
                        return '<div><div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-device" data-event-id="'+o.id+'" checked><i></i></label></div></div>';
                    else
                        return '<div><div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-device" data-event-id="'+o.id+'"><i></i></label></div></div>';
                }
            },
            {
                title : 'Category',
                property : 'category',
                cellClassName : 'category',
                filterable : false,
                sortable : true,
                callback: function(o) {  
                    return '<i class="fa fa-dot-circle-o"></i> ' + o.category;                 
                }
            },{
                title : 'Description',
                property : 'content',
                cellClassName : 'content',
                filterable : false,
                sortable : false,
                callback: function(o) { 
                    var msg = ""; 
                    if (o.type == "Root" && o.content == "=,1")
                        msg = "Device is rooted";    
                    else if (o.type == "BatteryLevel")
                    {
                        msg += "Battery level ";
                        if (o.content.split(",")[0] == ">")
                            msg += "is higher than ";
                        else 
                            msg += "is lower than ";
                        msg += o.content.split(",")[1];
                    }       
                    return msg;                     
                }
            },{
                //title : 'ns:File.Firmware.Title.FileSize',
                title : 'Level',
                property : 'severity',
                cellClassName : 'severity', 
                filterable : false,
                sortable : false,
                callback: function(o) {
                    var color;
                    var des;
                    switch (o.severity)
                    {
                        case 5:
                            color = "#ee0000";
                            des = "Urgent";
                            break;
                        case 4:
                            color = "#ee7700";
                            des = "Critical";
                            break;
                        case 3:
                            color = "#eeee00";
                            des = "High";
                            break;
                        case 2:
                            color = "#a2a200";
                            des = "Medium";
                            break;
                        case 1:
                            color = "#51a200";
                            des = "Low";
                            break;
                        default:
                            color = "#919191";
                            des = "--";
                    }
                    return "<i class=\"fa fa-circle\" style=\"color:"+color+";\"></i> " + des;
                }
            },{
                title : 'Creator',
                property : 'creatorName',
                cellClassName : 'creatorName', 
                filterable : false,
                sortable : true,
            } 
        ];
        /* define namespace ends */
        if (!$.login.user.checkRole('DeviceOperator'))          
        {
            AD.columns = $.grep(AD.columns, function(e) {
                return (!_.has(e, "operator"));
            }); 
        }               
        var url = $.config.server_rest_url + "/deviceEvents";
        AD.isAdd = false;
        AD.buttons = [
            {
                "sExtends": "text",
                "sButtonText" : "<i class='fa fa-save'></i>&nbsp; Save",
                "sButtonClass": "btn-saveDevices txt-color-white bg-color-blue"
            },
        ];

        var idAttribute = AD.idAttribute = "id";
        AD.title = "ns:Menu.EventList";

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
        /* collection ends */
    };


    var routes = {};
    routes[AD.page + '/:_id'] = 'show_grid';
    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/summary/:_id'] = 'show_summary';
    routes[AD.page + '/edit/:_id'] = 'show_edit';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        initialize: function(opt) {
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            // console.error();
        },
        show_edit: function(id) {  
            var t = this;            
            t.navigate("#ajax/device_edit.html/" + id, {
                trigger: true
            });
        },
        show_grid: function() {
            var t = this;
            // init GridView
            if (!t.gv) {
                t.gv = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: !_.isUndefined(t.AD.isAdd) ? t.AD.isAdd : true,
                    sort: t.AD.sort,
                    order: [[1, "desc"]], //t.AD.order,
                    filter: {}, //t.AD.filter,
                    buttons: t.AD.buttons,
                    AD: t.AD
                });

                t.$gv.on("click", ".btn-saveDevices", function(event) 
                {
                    event.preventDefault();
                    var data = {
                        additionDeviceEventIdSet: selectedEvents,
                        removeDeviceEventIdSet: deselectedEvents
                    };
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        headers: {
                            "X-HTTP-Method-Override": "PUT"
                        },
                        async: false,
                        url: $.config.server_rest_url + '/deviceGroups/' + id,
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(data),
                        success: function(data) {
                            console.log('Save Success');
                            window.location.href = '#ajax/group_detail.html/' + id;
                        },
                        error: function(data) {
                        },
                    });                    
                });
                t.$gv.on("click", ".checkbox-device", function(event) {
                    if ($(this).prop("checked"))
                    {
                        selectedEvents.push($(this).attr('data-event-id')); 
                        deselectedEvents.splice(deselectedEvents.indexOf($(this).attr('data-event-id')), 1); 
                    }
                    else
                    {
                        selectedEvents.splice(selectedEvents.indexOf($(this).attr('data-event-id')), 1); 
                        deselectedEvents.push($(this).attr('data-event-id'));
                    }

                    /*if ($(".checkbox-device:checked").length < $(".checkbox-device").length)
                        $(".checkbox-devices").prop("checked", false);
                    else
                        $(".checkbox-devices").prop("checked", true);*/

                    console.log(selectedEvents);
                });

                t.$gv.on("click", ".checkbox-devices", function(event) {
                    if ($(this).prop("checked"))
                    {
                        $.each($(".checkbox-device"), function() {
                            $(this).prop("checked", true);
                            if (selectedEvents.indexOf($(this).attr('data-event-id')) < 0)
                            {
                                selectedEvents.push($(this).attr('data-event-id')); 
                                deselectedEvents.splice(deselectedEvents.indexOf($(this).attr('data-event-id')), 1); 
                            }
                        }); 
                    }
                    else
                    {
                        $.each($(".checkbox-device"), function() {
                            $(this).prop("checked", false);
                            selectedEvents.splice(selectedEvents.indexOf($(this).attr('data-event-id')), 1); 
                            deselectedEvents.push($(this).attr('data-event-id'));
                        });                       
                    }
                    console.log(selectedEvents);
                });
            } 
            else
                t.gv.refresh();

            t.$fv.hide();
            t.$gv.show();

            pageSetUp();
        }       
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        if (!$.login.user.checkRole('DeviceViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        }

        var hash = window.location.hash;
        id = hash.split('/')[2];
        if (!_.isUndefined(id)) {
            $.ajax({
                url: $.config.server_rest_url+'/deviceGroups/'+id,
                type: 'GET',
                dataType: 'json',
                timeout: 10000,
                async: false,
                error: function(data, status, error){
                }
            }).done(function(data) {
                $("#groupName a").prop("href", '#ajax/group_detail.html/' + id);
                $("#groupName font").text(data.name);
                selectedEvents = data.deviceEventIdSet;
                deselectedEvents = [];
                adReload();
                // Multi-Language
                i18n.init(function(t){
                    $('[data-i18n]').i18n();
                });

                AD.app = new MyRouter({
                    "AD": AD
                });

                if (!Backbone.History.started) {
                    Backbone.emulateHTTP = true;
                    Backbone.history.start();
                }
                $("body").removeClass("loading");
            }); 
        };
     };
    /* define bootstrap ends */

    $[AD.page] = AD;
    // $[AD.page].bootstrap();

})(jQuery);
