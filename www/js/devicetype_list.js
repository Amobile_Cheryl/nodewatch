(function($) {
    var currentPage = "";
    var AD = {};
    AD.page = "ajax/devicetype_list.html";
    var idAttribute = AD.idAttribute = "id";
    var device_columns = [
        //column1
        /*
        {
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            callback: function(o) {
                return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
            }
        },
        */
        {
            //title: "ns:DeviceType.Title.Appearance",
            title: 'Image',
            filterable: false,
            sortable: false,
            callback: function(o) {
                var deviceTypeIconUrl = "img/device_info/unknown-128.png";
                if (o.hasOwnProperty('deviceTypeIconUrl') && o.deviceTypeIconUrl.length > 0)
                    deviceTypeIconUrl = o.deviceTypeIconUrl;
                return '<img src="'+deviceTypeIconUrl+'"" style="height:100px;padding:8px;" />';
            }
        },
        {
            title: "ns:DeviceType.Title.Name",
            property: 'name',
            filterable: false,
            sortable: true,
            callback: function(o) {
                //return "<i class=\"fa fa-tags\"></i>" + "&nbsp;" + o.name;
                return o.name;
            }
        },      
        /*{
            title: 'ns:DeviceType.Title.OperatingSystem',
            //property: 'operatingSystemName',
            filterable: false,
            sortable: false,
            callback: function(o) {
                var os = (typeof(o.operatingSystem) === "undefined") ? '' : o.operatingSystem;
                var displayHtml = '';
                if(os==="Android"){
                    displayHtml = '<i class="fa fa-android"></i> Android';
                }else if(os==="iOS"){
                    displayHtml = '<i class="fa fa-apple"></i> iOS';
                }else if(os==="Windows"){
                    displayHtml = '<i class="fa fa-windows"></i> Windows';
                }else{
                    displayHtml = '<i class="fa fa-question"></i> Unknow Operating System';
                }
                return displayHtml;
            }
        },*/
        {
            title: 'ns:DeviceType.Title.ReportInterval',
            property: 'reportInterval',
            filterable: false,
            sortable: true,
            callback: function(o) {
                return o.reportInterval + ' minutes';
            }
        },
        {
            title: 'ns:DeviceType.Title.Remark',
            property: 'remark',
            filterable: false,
            sortable: false
        },
        {
            title: 'ns:DeviceType.Title.Action',
            sortable: false,
            visible: $.login.user.checkRole('DeviceTypeOperator'),
            callback: function(o) {
                var _html = '';
                _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                if ($.common.isDelete)
                    _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                // _html += '<a href="#" class="btn btn-mini btn-warning removeOne" title="Unlock" data-id="' + o[idAttribute] + '"><i class="icon-unlock"></i> </a>';
                return _html;
            }
        }        
    ];

    var routes = {};
        routes[AD.page + '/grid']           = 'main';        
        routes[AD.page + '/edit/:_id']      = 'act_edit_deviceType';
        routes[AD.page + '/delete/:_id']    = 'delete_single';
        routes[AD.page]                     = 'render';     

    var MyRouter = Backbone.DG1.extend({        
        routes: routes,
        initialize: function(opt) {             
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            console.error();
            this.show_grid();
        },
        show_grid: function() {
            var t = this;

            var gvs = "#s1 .cctech-gv";
            var gv = $(gvs).first();
            
            if(!androidGrid) {                    
                var androidGrid = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    buttons: t.AD.buttons,
                    columns: t.AD.columns,
                    AD: t.AD
                });

                gv.on("click", ".s2-btn", function (event) {
                    event.preventDefault();
                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');
                    t.navigate("#ajax/devicetype_add.html", {
                        trigger: true
                    });
                });

                gv.on("click", ".s1-btn", function (event) {
                    event.preventDefault();
                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');
                    t.navigate("#ajax/devicetype_add.html", {
                        trigger: true
                    });
                });
            } else {
                androidGrid.refresh();
            }
            t.$gv.show();
        },
        act_edit_deviceType: function(_id) {
            var t = this;            
            t.navigate("#ajax/devicetype_edit.html/" + _id, {
                trigger: true
            });         
        },
        show_edit: function(_id) {
            var t = this;
            t.$fv.empty();
            t.$gv.hide();

            var model = new t.AD.Model();

            var attrs = {};
            var idAttr = model.idAttribute;
            attrs[idAttr] = _id;
            model.set(attrs);

            model.fetch({
                async: false,
                success: function(model, response, options) {
                    t.fv = new Backbone.FormView({
                        el: t.$fv,
                        Name: "Edit DeviceType",
                        model: model,
                        forms: t.AD.edit.forms,
                        /*
                        lang: {
                            save: 'Save',
                            back: '<< Previous Page'
                        },
                        */
                        button: [
                            {
                                "type": "button",
                                "cls": "btn btn-default",
                                "fn": "window.history.back()",
                                "text": "Cancel"
                            }
                        ]
                    });
                    t.fv.$el.unbind();
                    t.fv.$el.on("click", '[type="submit"]', function(event) {
                        event.preventDefault();
                        t.submit(event, model, t.AD.edit.forms);
                    });
                    t.$fv.show();
                    // 2014/9/16 Kenny
                    var reportSettingList = [
                        {
                            value: 'Battery',
                            label: 'Battery'
                        },
                        {
                            value: 'Bluetooth',
                            label: 'Bluetooth'
                        },
                        {
                            value: 'CPU',
                            label: 'CPU'
                        },
                        {
                            value: 'GPS',
                            label: 'GPS'
                        },
                        {
                            value: 'OS',
                            label: 'OS'
                        }
                    ];
                    $("#reportSettingSelector").multiselect({
                        enableFiltering : true,
                        includeSelectAllOption: true,
                        maxHeight:400,
                        buttonWidth:'auto',
                        numberDisplayed: 0
                    });
                    $("#reportSettingSelector").multiselect('dataprovider', reportSettingList);
                    // 2014/9/16 End
                    pageSetUp();
                },
                error: function(model, response, options) {
                    window.location.hash = "#ajax/error404.html";
                }
            });
        },
        delete_single: function(id){
            var dmURL = $.config.server_rest_url + '/deviceTypes/';
            $.SmartMessageBox({
                // title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; Delete Device <span class='txt-color-orangeDark'><strong> &nbsp;" +
                //     id + "</strong></span> ?",
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.DeviceType.DeleteDeviceType")+" <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                //content: "Delete the device ?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+i18n.t("ns:Message.DeviceType.DeleteDeviceTypeContent")+" </span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + id,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.DeviceType") + i18n.t("ns:Message.Device.DeleteSuccess"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.DeleteDeviceType") +"</i>",
                            // title: "[Device Type] Deleted successfully",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Device Type</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.DeviceType") + i18n.t("ns:Message.Device.DeleteFailed"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.DeleteDeviceType") +"</i>",
                            // title: "[Device Type] Deleted failed",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Device Type</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/devicetype_list.html";
                    });
                } else {
                    window.location.href = "#ajax/devicetype_list.html";
                }
            });
        }, 
        main: function() {
            if (currentPage == "s1")
                s1();
            else if (currentPage == "s2")
                s2();
        }
    });

    // Windows
    function s1() {
        var AD = {};
        AD.page = "ajax/devicetype_list.html";
        AD.collections = {};
        currentPage = "s1";
        var url = $.config.server_rest_url + "/deviceTypes" + "?search=operatingSystemName:Windows";
        if ($.login.user.checkRole('DeviceTypeOperator'))
        AD.buttons = [
            //button
            {
                "sExtends" : "text",
                "sButtonText" : "<i class='fa fa-plus'></i>&nbsp; " + "Add Type",//i18n.translate("ns:File.Upload"),
                "sButtonClass" : "upload-btn s1-btn btn-default txt-color-white bg-color-blue"
            },
            {
                "sExtends": "text",
                "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                "fnClick": function(nButton, oConfig, oFlash) {
                    $(".cctech-gv table").dataTable().fnDraw(false);
                }
            }
        ];

        AD.columns = device_columns;
        AD.Model = Backbone.Model.extend({urlRoot: url, idAttribute: idAttribute});
        AD.Collection = Backbone.Collection.extend({url: url, model: AD.Model}); 
        AD.app = new MyRouter({"AD": AD});        
        pageSetUp();
    };

    // Android
    function s2() { 
        var AD = {};
        AD.page = "ajax/devicetype_list.html";
        AD.collections = {};
        currentPage = "s2";
        var url = $.config.server_rest_url + "/deviceTypes" + "?search=operatingSystemName:Android";
        if ($.login.user.checkRole('DeviceTypeOperator'))
        AD.buttons = [
            //button
            {
                "sExtends" : "text",
                "sButtonText" : "<i class='fa fa-plus'></i>&nbsp; " + "Add Type",//i18n.translate("ns:File.Upload"),
                "sButtonClass" : "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
            },
            {
                "sExtends": "text",
                "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                "fnClick": function(nButton, oConfig, oFlash) {
                    $(".cctech-gv table").dataTable().fnDraw(false);
                }
            }
        ];

        AD.columns = device_columns;
        AD.Model = Backbone.Model.extend({urlRoot: url, idAttribute: idAttribute});
        AD.Collection = Backbone.Collection.extend({url: url, model: AD.Model}); 
        AD.app = new MyRouter({"AD": AD});        
        pageSetUp();
    };
     
    AD.bootstrap = function() {
        i18n.init(function(t){
            $('[data-i18n]').i18n();            
        });     

        if (!$.login.user.checkRole('DeviceTypeViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        };

        $("#myTab li").click(function() {            
            switch (this.firstElementChild.id) {
                case "s1": //Windows
                    s1();
                    break;
                case "s2": //Android
                    s2();
                    break;
                case "s3": //iPhone
                    s3();
                    break;                
            }
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
        
        if (currentPage == "")
            s2();

        if (currentPage == "s1")
        {
            $("#tab2").removeClass('active');
            $("#tab1").addClass('active');
        }
        else if (currentPage == "s2")
        {
            $("#tab1").removeClass('active');
            $("#tab2").addClass('active');
        }
        $("body").removeClass("loading");
    };
    $[AD.page] = AD;
})(jQuery);
