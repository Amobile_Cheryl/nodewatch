(function($) {
    //

    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');

    /*                  */

    //
    /* define namespace starts */

    // var bts = ".btn-success";
    var AD = {};
    AD.collections = {};
    // var url = AD.url = $.config.server_url + '/role';
    var url = 'http://220.130.176.238:8080/acs-core/RESTful/dataFiles';
    //var url = "http://220.130.176.240:8080/III_NetAdmin2/rest/DataFile";
    var idAttribute = AD.idAttribute = "id";
    AD.title = "Uploadedfile";
    AD.page = "ajax/uploadedfile_list.html";
    //AD.api = 'cctech';
    //把Add的按鈕取消
    AD.isAdd = false;
    AD.validate = function() {
        return true;
    };

    AD.buttons = [
        //button
        {
            "sExtends": "text",
            "sButtonText": 'Single File Upload',
            "sButtonClass": "upload-btn"
        }
    ];

    AD.attachment = {};


    _.defaults()
    AD.attachment.forms = [
	{
            title: 'name',
	    property:"name",
	    default_value:""
	},
	{
            title: 'remark',
	    property:"remark",
	    default_value:""
	},
	{
            title: 'version',
	    property:"version",
	    default_value:""
	},
	"fieldset",
	"row",
        {
            title: 'Choose Apps File',
            property: 'appFile',
            // property: 'name',
            default_value: "",
            view: function() {
		var t = this;
                //宣告upload button
                var uploadButton = $('<button/>')
                //增加button的class讓呈現方式改變
                .addClass('btn btn-primary')
                //把upload button 的disable prop設為true目的是讓他無法選取
                .prop('disabled', true)
                    .text('Processing...')
                //設定 upload button 的click event
                .on('click', function(event) {
                    event.preventDefault();
                    var $this = $(this),
                        //取值, 即檔案
                        data = $this.data();

                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function() {
                            //把upload button移除
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function() {
                        $this.remove();
                    });
                });

                //下面是利用handlebars來把Add File的按鈕裝上template
                // var uploadButton = $('<button/>');
                var chooseFile = {
                    title: "Add File...",
		    property:t.property
                };
                var chooseButton = $('#btnChooseFile').html();
                var template = Handlebars.compile(chooseButton);
                var html = template(chooseFile); //String
                $.ajax({
                    url: 'http://220.130.176.240:8080/III_NetAdmin2/f',
                    type: 'get',
                    async: false,
                    data: {
                        property: t.property
                    },
                    success: function(data) {
                        // alert("i am success!");
                    }
                });



                $html = $(html); //according to the html string, it will generate a "jquery" object that copy the string from var html

                $html.find("#"+t.property+"-fileupload").fileupload({
                    url: 'http://220.130.176.240:8080/III_NetAdmin2/f?property='+t.property,
                    dataType: 'json',
                    //是否選擇完檔案立即上傳
                    autoUpload: false,
                    //可以接受的格式, 可直接用|另外增加類型
                    //acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                    //超過會顯示file too large
                    maxFileSize: 5000000, // 5 MB
                    // Enable image resizing, except for Android and Opera,
                    // which actually support image resizing, but fail to
                    // send Blob objects via XHR requests:
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    previewMaxWidth: 100,
                    previewMaxHeight: 100,
                    //是否讓縮圖點擊後另開頁面顯示原圖
                    previewCrop: true
                    //檔案load之後把檔案名稱顯示出來
                }).on('fileuploadadd', function(e, data) {
                    data.context = $('<div/>').appendTo('#'+t.property+'-files');
                    $.each(data.files, function(index, file) {
                        var node = $('<p/>')
                            .append($('<span/>').text(file.name));
                        if (!index) {
                            node
                                .append('<br>')
                                .append(uploadButton.clone(true).data(data));
                        }
                        node.appendTo(data.context);
                    });
                }).on('fileuploadprocessalways', function(e, data) {
                    var index = data.index,
                        file = data.files[index],
                        node = $(data.context.children()[index]);
                    if (file.preview) {
                        node
                            .prepend('<br>')
                            .prepend(file.preview);
                    }
                    //如果檔案error, 把錯誤的資訊顯示出來
                    if (file.error) {
                        node
                            .append('<br>')
                            .append($('<span class="text-danger"/>').text(file.error));
                    }
                    //產生
                    if (index + 1 === data.files.length) {
                        data.context.find('button')
                            .text('Upload')
                            .prop('disabled', !! data.files.error);
                    }
                }).on('fileuploadprogressall', function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 5);
                    $('#'+t.property+'-progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }).on('fileuploaddone', function(e, data) {
                    $('#'+t.property+'-progress .progress-bar').css(
                        'width',
                        '100%'
                    );
                    /*$.each(data.result.files, function(index, file) {
                        if (file.url) {
                            var link = $('<a>')
                                .attr('target', '_blank')
                                .prop('href', file.url);
                            $(data.context.children()[index])
                                .wrap(link);
                        } else if (file.error) {
                            var error = $('<span class="text-danger"/>').text(file.error);
                            $(data.context.children()[index])
                                .append('<br>')
                                .append(error);
                        }
                    });*/
		    $("#"+t.property+"-files button").replaceWith("");
                }).on('fileuploadfail', function(e, data) {
                    $.each(data.files, function(index, file) {
                        var error = $('<span class="text-danger"/>').text('File upload failed.');
                        $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                    });
                }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');

                return $html;
            },
            value: function() {
		var t = this;
                var name;
                var path;

                $.ajax({
                    url: 'http://220.130.176.240:8080/III_NetAdmin2/f',
                    type: 'post',
                    async: false,
                    data: {
                        property: t.property,
                        property2: "doSave"
                    },
                    success: function(data) {
                        path = data.fileServerPath;
                        name = data.name;
                    }
                });
		//alert(data.fileServerPath);
                /*var ret = {
                    "appFile": path
                };*/
		ret={};
		ret[t.property]=path;
                return ret;
            }
	},
	"fieldset",
	"row",
        {
            title: 'Choose Icon File',
            property: 'iconFile',
            // property: 'name',
            default_value: "",
            view: function() {
		var t = this;
                //宣告upload button
                var uploadButton = $('<button/>')
                //增加button的class讓呈現方式改變
                .addClass('btn btn-primary')
                //把upload button 的disable prop設為true目的是讓他無法選取
                .prop('disabled', true)
                    .text('Processing...')
                //設定 upload button 的click event
                .on('click', function(event) {
                    event.preventDefault();
                    var $this = $(this),
                        //取值, 即檔案
                        data = $this.data();

                    $this
                        .off('click')
                        .text('Abort')
                        .on('click', function() {
                            //把upload button移除
                            $this.remove();
                            data.abort();
                        });
                    data.submit().always(function() {
                        $this.remove();
                    });
                });

                //下面是利用handlebars來把Add File的按鈕裝上template
                // var uploadButton = $('<button/>');
                var chooseFile = {
                    title: "Add File...",
		    property:t.property
                };
                var chooseButton = $('#btnChooseFile').html();
                var template = Handlebars.compile(chooseButton);
                var html = template(chooseFile); //String
                $.ajax({
                    url: 'http://220.130.176.240:8080/III_NetAdmin2/uf',
                    type: 'get',
                    async: false,
                    data: {
                        property: t.property
                    },
                    success: function(data) {
                        // alert("i am success!");
                    }
                });



                $html = $(html); //according to the html string, it will generate a "jquery" object that copy the string from var html

                $html.find("#"+t.property+"-fileupload").fileupload({
                    url: 'http://220.130.176.240:8080/III_NetAdmin2/uf?property='+t.property,
                    dataType: 'json',
                    //是否選擇完檔案立即上傳
                    autoUpload: false,
                    //可以接受的格式, 可直接用|另外增加類型
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                    //超過會顯示file too large
                    maxFileSize: 5000000, // 5 MB
                    // Enable image resizing, except for Android and Opera,
                    // which actually support image resizing, but fail to
                    // send Blob objects via XHR requests:
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    previewMaxWidth: 100,
                    previewMaxHeight: 100,
                    //是否讓縮圖點擊後另開頁面顯示原圖
                    previewCrop: true
                    //檔案load之後把檔案名稱顯示出來
                }).on('fileuploadadd', function(e, data) {
                    data.context = $('<div/>').appendTo('#'+t.property+'-files');
                    $.each(data.files, function(index, file) {
                        var node = $('<p/>')
                            .append($('<span/>').text(file.name));
                        if (!index) {
                            node
                                .append('<br>')
                                .append(uploadButton.clone(true).data(data));
                        }
                        node.appendTo(data.context);
                    });
                }).on('fileuploadprocessalways', function(e, data) {
                    var index = data.index,
                        file = data.files[index],
                        node = $(data.context.children()[index]);
                    if (file.preview) {
                        node
                            .prepend('<br>')
                            .prepend(file.preview);
                    }
                    //如果檔案error, 把錯誤的資訊顯示出來
                    if (file.error) {
                        node
                            .append('<br>')
                            .append($('<span class="text-danger"/>').text(file.error));
                    }
                    //產生
                    if (index + 1 === data.files.length) {
                        data.context.find('button')
                            .text('Upload')
                            .prop('disabled', !! data.files.error);
                    }
                }).on('fileuploadprogressall', function(e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 5);
                    $('#'+t.property+'-progress .progress-bar').css(
                        'width',
                        progress + '%'
                    );
                }).on('fileuploaddone', function(e, data) {
                    $('#'+t.property+'-progress .progress-bar').css(
                        'width',
                        '100%'
                    );
                    /*$.each(data.result.files, function(index, file) {
                        if (file.url) {
                            var link = $('<a>')
                                .attr('target', '_blank')
                                .prop('href', file.url);
                            $(data.context.children()[index])
                                .wrap(link);
                        } else if (file.error) {
                            var error = $('<span class="text-danger"/>').text(file.error);
                            $(data.context.children()[index])
                                .append('<br>')
                                .append(error);
                        }
                    });*/
		    $("#"+t.property+"-files button").replaceWith("");
                }).on('fileuploadfail', function(e, data) {
                    $.each(data.files, function(index, file) {
                        var error = $('<span class="text-danger"/>').text('File upload failed.');
                        $(data.context.children()[index])
                            .append('<br>')
                            .append(error);
                    });
                }).prop('disabled', !$.support.fileInput)
                    .parent().addClass($.support.fileInput ? undefined : 'disabled');

                return $html;
            },
            value: function() {
		var t = this;
                var name;
                var path;

                $.ajax({
                    url: 'http://220.130.176.240:8080/III_NetAdmin2/uf',
                    type: 'post',
                    async: false,
                    data: {
                        property: t.property,
                        property2: "doSave"
                    },
                    success: function(data) {
                        path = data.fileServerPath;
                        name = data.name;
                    }
                });
		//alert(data.fileServerPath);
                var ret = {};
		ret[t.property]=path
                return ret;
            }
	}
    ]; // , {
    //     title: 'Upload File',
    //     property: 'name',
    //     default_value: "",
    //     view: function() {
    //         var uploadFile = {
    //             title: "Upload File..."
    //         };
    //         var uploadButton = $('#btnChooseFile').html();
    //         var template2 = Handlebars.compile(uploadButton);
    //         var html2 = template2(uploadFile);

    //         // return $('<span>Add files...</span><input id="fileupload" type="file" name="files[]" multiple>');
    //         return (html2);
    //     }
    // }


    AD.forms = [
        //forms
        {
            title: 'File',
            property: 'name',
            default_value: ""
        },
        //forms
        {
            title: 'Path',
            property: 'fileServerPath',
            default_value: ""
        }

    ];

    AD.columns = [
        //columns1
        {
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            sortable: false,
            callback: function(o) {
                if (o)
                    return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
                else
                    return '<input type="checkbox" value="0"></input>';
            }
        },
        //columns2
        {
            title: 'File',
            property: 'name',
            cellClassName: 'name',
            filterable: false,
            sortable: true
        },
        //column
        {
            title: 'Path',
            property: 'fileServerPath',
            cellClassName: 'fileServerPath',
            filterable: false,
            sortable: true,
            callback: function(o) {
                // console.log(o);
                return '<a href="' + o.fileServerPath + '" >' + o.name + '</a>';
            }
        },
        //column
        {
            title: 'Domain',
            property: 'domainId',
            cellClassName: 'domainName',
            filterable: false,
            sortable: true
        },
        //column
        {
            title: 'Device',
            property: 'deviceId',
            cellClassName: 'deviceName',
            filterable: false,
            sortable: true
        },
        //column
        {
            title: 'Uploaded Time',
            callback: function(o) {
                /*change millisecond to date format*/
                var time = o.creationTime;
                var date = time;
                var str = df.format(date);
                return str;
            },
            filterable: false,
            sortable: true
        },
        //column
        {
            title: 'Remark',
            property: 'remark',
            cellClassName: 'remark',
            filterable: false,
            sortable: true
        },
        //column
        {
            title: 'Action',
            sortable: false,
            callback: function(o) {
                var _html = '';
                _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-times"></i></a>';
                return _html;
            }

        }
    ];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */
    var routes = {};

    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/edit/:_id'] = 'show_edit';
    routes[AD.page + '/add'] = 'show_add';
    routes[AD.page + '/delete/:_id'] = 'delete_single';
    routes[AD.page + '/file_upload'] = 'show_file_upload';
    routes[AD.page] = 'render';

    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        initialize: function(opt) {

            // var t = this;

            // opt.gvs = opt.gvs || gvs;
            // // alert(opt.gvs);
            // opt.fvs = opt.fvs || fvs;
            // // alert(opt.fvs);
            // t.AD = opt.AD;
            // // t.addurl = opt.ADaddurl || "add";

            // t.$gv = $(opt.gvs).first();
            // t.$fv = $(opt.fvs).first();
            // t.$bt = $(opt.bts).first();

            var t = this;

            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);

            // alert(opt.fvs);
            // t.AD = opt.AD;
            // t.addurl = opt.ADaddurl || "add";
            t.$bt = $(opt.bts).first();

        },
        show_grid: function() {
            var t = this;

            /*
            $('body,html').animate({
                scrollTop: 0
            }, 500);*/

            // init GridView
            if (!t.gv) {
                t.gv = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: !_.isUndefined(t.AD.isAdd) ? t.AD.isAdd : true,
                    sort: t.AD.sort,
                    filter: t.AD.filter,
                    buttons: t.AD.buttons,
                    AD: t.AD
                });

                t.$gv.on("click", ".upload-btn", function(event) {
                    event.preventDefault();

                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    // alert(t.AD.addurl);
                    // var addurl = t.AD.addurl || "add";
		    //alert("#" + t.AD.page + "/" + "file_upload");
		    //window.location.hash="#" + t.AD.page + "/" + "file_upload";
                    //t.navigate("#" + t.AD.page + "/" + "file_upload");

		    t.show_file_upload();
                });
            } else
                t.gv.refresh();


            t.$fv.hide();
            t.$gv.show();

            pageSetUp();

            // setTimeout(runDataTables, 500);
        },
        //設定一個show file upload, 讓按下上傳檔案時可以跳轉到的上傳的頁面
        show_file_upload: function() {

            // alert("hello!");
            var t = this;
            //一樣先清空用不到的東西
            t.$fv.empty();
            t.$gv.hide();

            //這邊是要new一個model
            //這model繼承formview
            var m = new t.AD.Model();
            t.fv = new Backbone.FormView({
                el: t.$fv,
                Name: "UploadFile",
                model: new Backbone.Model(),
                forms: t.AD.attachment.forms,
                lang: {
                    save: 'Save',
                    back: 'Cancel'
                }
            });


            t.fv.$el.unbind();
            t.fv.$el.on("click", '[type="submit"]', function(event) {
                var forms = t.AD.forms;
                t.AD.forms = t.AD.attachment.forms;
		var mc = Backbone.Model.extend({
       			urlRoot: "http://220.130.176.238:8080/acs-core/RESTful/appFiles",
		        idAttribute: "id"
		});
		var m = new mc();
                t.submit(event, m);
                t.AD.forms = forms;
            });

//	    t.fv.$el.find('footer button[type="button"]').replaceWith("");
	    t.fv.$el.find('footer button[type="button"]').unbind();
            t.fv.$el.on("click", 'footer button[type="button"]', function(event) {
		event.preventDefault();
		//alert("hello world!");
		t.show_grid();
	    });

            t.$fv.show();

            pageSetUp();

        },
	submit: function(event,m) {
		Backbone.DG1.prototype.submit.call(this, event,m);
		//alert("hello world!");
		this.show_grid();
	}
    });


    /* define bootstrap starts */
    AD.bootstrap = function() {
        // alert('bootstrap');
        // if (!_.isUndefined($[AD.page].app)) {
        //     $[AD.page].app.navigate("#" + AD.page + "/grid", {
        //         trigger: true
        //     });
        // }


        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();


})(jQuery);
