(function($) {

    var AD = {};
    var url = "http://220.130.176.238:8080/acs-core/RESTful/roles";
    var idAttribute = AD.idAttribute = "id";
    AD.title = "Roles";
    AD.page = "ajax/role_edit.html";
   
    function s1(id) {
        console.log('role_edit.js');
        $.ajax({
            type: "GET",
            url: url+'/'+id,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                $('input[name="rolename"]').val(data.name);
                $('input[name="description"]').val(data.remark);
                getData=data;
            },
            async: false
        });

        var role={
            Dashboard:true,
            DeviceGroup:false,
            DeleteDevice:false,
            EditDevice:false,
            CreateGroup:true,
            DeleteGroup:true,
            EditGroup:true,
            DeviceInfo:false,
            UploadDevice:false,
            Summary:false,
            AppManagement:true,
            FirmwareUpgrade:true,
            OSImageUpgrade:true,
            BootloaderUpgrade:true,
            LogoUpgrade:true,
            HWIUpgrade:true,
            AgentUpgrade:true,
            DeviceManagement:true,
            DeviceEvent:true,
            File:false,
            App:false,
            UploadApp:false,
            DeleteApp:false,
            Firmware:false,
            OSImage:false,
            UploadOSImage:false,
            DeleteOSImage:false,
            Bootloader:false,
            UploadBootloader:true,
            DeleteBootloader:true,
            FirmwareLogo:true,
            UploadFirmwareLogo:true,
            DeleteFirmwareLogo:true,
            HWInfo:true,
            UploadHWInfo:true,
            DeleteHWInfo:true,
            Agent:true,
            UploadAgent:true,
            DeleteAgent:true,
            Account:true,
            User:true,
            AddUser:true,
            DeleteUser:true,
            EditUser:true,
            Role:true,
            AddRole:true,
            DeleteRole:true,
            EditRole:true,
            Setting:true,
            EventSetting:true,
            AddEvent:true,
            DeleteEvent:true,
            EditEvent:true,
            AlertSetting:true,
            AddAlert:true,
            DeleteAlert:true,
            EditAlert:true,
            DeviceType:true,
            AddProductModel:true,
            AddProductSKU:true,
            DeleteDeviceType:true,
            EditProductSKU:true,
            System:true,
            SystemConfiguration:true,
            DomainConfiguration:true,
            DomainDeviceCustomField:true,
            ServerConfiguration:true,
            Domain:true,
            AddDomain:true,
            DeleteDomain:true,
            EditDomain:true,
            LogReport:true,
            EventLog:true,
            AlertLog:true,
            UserLog:true,
            SystemLog:true,
            TaskLog:false,
            Report:true
            

        };

        // console.log('Hello');
        // console.log(role);
        // Main Detail
        var source = $("#tpl_permission").html();
        // console.log(source);
        var template = Handlebars.compile(source);
        var html = template(role);
        // console.log(html);
        $("#permission_render").html(html);


        // Data

        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').find(' > i').on('click', function(e) {
            var children = $(this).parent().parent('li.parent_li').find(' > ul > li');
            if (children.is(':visible')) {
                console.log('Expand Children');
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').removeClass().addClass('fa fa-lg fa-plus-circle');
            } else {
                console.log('Collapse Children');
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').removeClass().addClass('fa fa-lg fa-minus-circle');
            }
            e.stopPropagation();
        });


        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').find(' > input').on('click', function(e) {
            console.log('Click input');
            if ($(this).is(':checked')) {
                console.log('Checked');
                recursiveCheck($(this),true);
            } else {
                console.log('Canceled');
                recursiveCheck($(this),false);
            }
            e.stopPropagation();
        });
        
    };




    AD.bootstrap = function() {
        // var p = $("#left-panel nav a[href='ajax/device_list.html']").parents("li");
        // $(p[0]).addClass("active");
        // $(p[1]).find("ul").show();
        // $(p[1]).addClass("active open");
        //alert(href);

        var hash = window.location.hash;
        //alert(hash);
        var id = hash.replace(/^#ajax\/role_edit.html\/(\w+)$/, "$1");
        // console.log(id);
		$("body").removeClass("loading");
        // Initial page
        s1(id);

    };

    $[AD.page] = AD;
})(jQuery);