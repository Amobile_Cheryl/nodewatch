(function($) {
    /* define namespace starts */

    var AD = {};
    AD.collections = {};
    // var url = AD.url = $.config.server_url + '/domain';
    var url = 'http://220.130.176.238:8080/acs-core/RESTful/domains';
    var idAttribute = AD.idAttribute = "id";
    AD.page = "ajax/report_list.html";

    AD.validate = function() {
        return true;
    };

    //設定要出現的form
    AD.forms = [
        //form
        // {
        //     title: 'User Name',
        //     property: 'name',
        //     default_value: ""
        // },

        //droplist 繼承datagrid裡面的defaults相關函數
        _.defaults({
                title: 'Report Type:',
                property: 'report_type',
                label_value: [{
                    'label': '',
                    value: ''
                }, {
                    'label': 'Type A',
                    value: 'a'
                }, {
                    'label': 'Type B',
                    value: 'b'
                }, {
                    'label': 'Type C',
                    value: 'c'
                }, {
                    'label': 'Type D',
                    value: 'd'
                }]
            },
            Backbone.UIToolBox.selector),

        //droplist2
        _.defaults({
            title: 'Report Period:',
            property: 'report_period',
            label_value: [{
                'label': '',
                value: ''
            }, {
                'label': '1 week',
                value: '1'
            }, {
                'label': '2 weeks',
                value: '2'
            }, {
                'label': '3 weeks',
                value: '3'
            }, {
                'label': '4 weeks',
                value: '4'
            }]
        }, Backbone.UIToolBox.selector)

        //submit button
    ];


    /*
    AD.forms = [
        //form config
        {
            title: 'User Name',
            property: 'name',
            default_value: "",
            useTemplate: true,
            fedcls: "input",
            'class': 'col col-6',
            view: function(o) {
                return $('<input type="text" name="' + this.property + '" placeholder="' + this.title + '" class="text" >');
            },
            value: function() { //provide value
                return $('input[name="' + this.property + '"]').val();
            },
            setValue: function(m) { //set the value to this element
                var v = m.get(this.property) || this.default_value;
                $('input[name="' + this.property + '"]').val(v);
            },
            update: function(m) {

            }
        }

        _.defaults({
            title: 'User Role',
            property: 'role',
            label_value: function(argument) {
                if (!AD.collections.role) {
                    var coll = Backbone.getCollectionByUrl($.config.server_url + '/role', "id");
                    coll.fetch({
                        async: false,
                        data: {
                            "fields": ["id", "roleName"]
                        }
                    });
                    // coll.pop();
                    AD.collections.role = coll;
                }
                var coll = AD.collections.role;
                var jcol = coll.toJSON();
                var ret = [];
                for (var i = 0; i < jcol.length; i++) {

                    ret.push({
                        label: jcol[i].roleName,
                        value: jcol[i].id
                    });
                }
                return ret;
            }(),

        }, Backbone.UIToolBox.selector), {
            title: 'Email',
            property: 'email',
            default_value: ""
        },
        _.defaults({
            title: '密碼',
            property: 'password'
        }, Backbone.UIToolBox.password), {
            title: 'Phone Number',
            property: 'phone',
            default_value: "名稱"
        }
    ]; */


    /*
    (Lock)  isLock
    (Active)    isActive
    User Name   id
    Reseller    domainName
    Role    roleName
    Email   email
    Last Login Time lastLoginTime
     */
    AD.columns = [
        //columns
        {
            title: '<i class="fa fa-lock"></i>',
            // property: 'isLock',
            cellClassName: 'isLock',
            filterable: true,
            sortable: true,
            callback: function(o) {
                if (o['isLock'] === 'Y')
                    return '<i class="fa fa-lock"></i>';
                else
                    return '<i class="fa fa-unlock-o"></i>';
            }
        },
        //columns
        {
            title: '(Active)',
            // property: 'isLock',
            cellClassName: 'isActive',
            filterable: true,
            sortable: true,
            callback: function(o) {
                // if (o['isActive'] === 'Y')
                return 'Yes';
                // else
                // return 'No';
            }
        },
        //columns
        {
            title: 'User ID',
            property: 'id',
            cellClassName: 'id',
            filterable: true,
            sortable: true
        },
        //columns
        {
            title: 'User Name',
            property: 'name',
            cellClassName: 'name',
            filterable: true,
            sortable: true
        },
        //columns
        {
            title: 'Reseller',
            // property: 'domainName',
            cellClassName: 'domainName',
            filterable: true,
            sortable: true,
            callback: function(o) {
                return o['domain']['name'];
            }
        },
        //columns
        {
            title: 'Role',
            // property: 'roleName',
            cellClassName: 'roleName',
            filterable: true,
            sortable: true,
            callback: function(o) {
                return o['role']['name'];
            }
        },
        //column
        {
            title: 'Email',
            property: 'email',
            cellClassName: 'email',
            filterable: true,
            sortable: true
        },
        //column
        {
            title: 'Create Time',
            property: 'creationTime',
            cellClassName: 'creation_time',
            filterable: true,
            sortable: true,
            callback: function(o) {
                var v = new Date();
                v.setTime(o['creationTime']);
                return v.getFullYear() + '/' + (v.getMonth() + 1) + '/' + v.getDate() + ' ' + v.getHours() + ':' + (v.getMinutes()) + ':' + (v.getSeconds());
            }
        },
        //columns7
        {
            title: 'Action',
            sortable: false,
            callback: function(o) {
                var _html = '';
                // _html +='<button class="btn btn-mini btn-success" title="Verify"><i class="icon-ok"></i> </button>';
                if (o) {
                    // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Edit</span></a>';
                    // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Remove</span></a>';
                    _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-edit"></i></a>';
                    _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-times"></i></a>';
                }
                return _html;
            }
        }
    ];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */

    //設定畫面路由, render為初始值
    var routes = {};

    // routes[AD.page + '/edit'] = 'show_edit';
    routes[AD.page + '/add'] = 'show_add';
    routes[AD.page + '/report'] = 'show_report';
    // routes[AD.page + '/edit/:_id'] = 'show_edit';
    // routes[AD.page + '/delete/:_id'] = 'delete_single';
    routes[AD.page] = 'render';
    //set default routes
    // routes[AD.page + '/test1'] = 'test1';
    // routes[AD.page + '/test2/:_id'] = 'test2';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,

        //設定點擊Search之後的頁面
        show_report: function() {

            var t = this;
            //將cctech-all的類別都隱藏, 用意是將全部都先隱藏, 需要的再一一call出來用
            $('.cctech-all').hide();
            t.$fv.empty();
            // t.$gv.hide();
            // t.fv = new Backbone.FormView({
            //     el: t.$fv,
            //     Name: "Report",
            //     model: new Backbone.Model()
            // });
            $(t.$fv).find('footer').empty();
            //新增所需button (FormView)
            // $(t.$fv).find('footer').append('<button type="button" class="btn btn-success btn-search">search</button>');
            // $(t.$fv).find('footer').append('<button type="button" class="btn btn-primary btn-export">export</button>');
            $(t.$fv).find('footer').append('<button type="button" class="btn btn-default" onclick=" window.history.back();">Back</button>');

            //compile要使用的template
            var tpl_report_comp = Handlebars.compile($('#tpl_edit').html());
            var report_html = tpl_report_comp({
                Name: "Search"
            });
            // 這邊使用了index.html內的template將report→search的畫面呈現

            // alert(report_html);

            $(".cctech-export").html(report_html);

            $('.cctech-export').show();
            pageSetUp();

        },
        show_add: function() {

            $('.cctech-all').hide();

            var t = this;

            t.$fv.empty();
            t.$gv.hide();

            var m = new t.AD.Model();

            t.fv = new Backbone.FormView({
                el: t.$fv,
                Name: "ns:Menu.Report",
                model: new Backbone.Model(),
                forms: t.AD.forms,
                lang: {
                    save: 'submit',
                    back: '回上一頁'
                }
            });

            //先清空原本的button(用不到)
            $(t.$fv).find('footer').empty();
            //新增所需button (FormView)
            $(t.$fv).find('footer').append('<button type="button" class="btn btn-success btn-search">search</button>');
            $(t.$fv).find('footer').append('<button type="button" class="btn btn-primary btn-export">export</button>');
            $(t.$fv).find('footer').append('<button type="button" class="btn btn-default" onclick=" window.history.back();">Back</button>');
            // $(t.$fv).find('footer').empty();

            t.fv.$el.unbind();
            t.fv.$el.on("click", '.btn-export', function(event) {
                // alert('export');
            });

            //設定  click search button之後的頁面導向 /report
            t.fv.$el.on("click", '.btn-search', function(event) {
                // alert('export');
                t.navigate(AD.page + '/report', {
                    trigger: true
                });
            });

            t.$fv.show();

            pageSetUp();

        },
        render: function() {
            var t = this;
            t.navigate("#" + t.AD.page + "/add", {
                trigger: true
            });
        }
        /*overwrite render end*/
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {

        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    // $[AD.page].bootstrap();


})(jQuery);



// })(jQuery);
//</script>
