(function($) {

    // Move variable AD and AD.page outside of function
    var AD = {};
    AD.page = "ajax/domain_user_list.html";
    
    // Add adReload function to run this script when user click this .html every time.
    var adReload = function(){
        AD.collections = {};

        var hash = window.location.hash;

        var id = hash.replace(/^#ajax\/domain_user_list.html\/(\w+)$/, "$1");

        var url = $.config.server_rest_url+ '/domains/'+ id +'/users';
        // console.log('url is '+url);
        var idAttribute = AD.idAttribute = "id";
        AD.title = "User";

        AD.isAdd = false;

        AD.validate = function() {
            return true;
        };


        AD.columns = [
        //columns
        {
            title: '<i class="fa fa-lock"></i>',
            // property: 'isLock',
            cellClassName: 'isLock',
            filterable: true,
            sortable: true,
            callback: function(o) {
                if (o['isLock'] === 'Y')
                    return '<i class="fa fa-lock"></i>';
                else
                    return '<i class="fa fa-unlock-o"></i>';
            }
        },
        //columns
        // {
        //     title: '(Active)',
        //     property: 'isLock',
        //     cellClassName: 'isActive',
        //     filterable: true,
        //     sortable: true,
        //     callback: function(o) {
        //         // if (o['isActive'] === 'Y')
        //         return 'Yes';
        //         // else
        //         // return 'No';
        //     }
        // },
        //columns
        {
            title: 'User ID',
            property: 'id',
            cellClassName: 'id',
            filterable: true,
            sortable: true
        },
        //columns
        {
            title: 'User Name',
            property: 'name',
            cellClassName: 'name',
            filterable: true,
            sortable: true,
            callback: function(o) {
                return '<a href="#ajax/user_detail.html/' + o.id + '">' + o.name + '</a>';
            }
        },
        //columns
        // {
        //     title: 'Domain',
        //     property: 'domainIdSet',
        //     //cellClassName: 'domainName',
        //     filterable: true,
        //     sortable: true,
        //     /*callback: function(o) {
        //         return o['domain'] && o['domain']['name'] || '(no name)';
        //     }*/
        // },
        //columns
        /*{
            title: 'Role',
            // property: 'roleName',
            cellClassName: 'roleName',
            filterable: true,
            sortable: true,
            callback: function(o) {
                return o['role'] && o['role']['name'] || '(no name)';
            }
        },*/
        //column
        {
            title: 'Email',
            property: 'email',
            cellClassName: 'email',
            filterable: true,
            sortable: true
        },
        //column
        {
            title: 'Create Time',
            property: 'creationTime',
            cellClassName: 'creation_time',
            filterable: true,
            sortable: true,
            callback: function(o) {
                var v = new Date();
                v.setTime(o['creationTime']);
                return v.getFullYear() + '/' + (v.getMonth() + 1) + '/' + v.getDate() + ' ' + v.getHours() + ':' + (v.getMinutes()) + ':' + (v.getSeconds());
            }
        },
        //columns7
        {
            title: 'Action',
            sortable: false,
            callback: function(o) {
                var _html = '';
                // _html +='<button class="btn btn-mini btn-success" title="Verify"><i class="icon-ok"></i> </button>';
                if (o) {
                    // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Edit</span></a>';
                    // _html += '<a class="btn btn-default btn-sm DTTT_button_text"><span>Remove</span></a>';
                    _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-edit"></i></a>';
                    _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text" style="display: inline;"><i class="fa fa-times"></i></a>';
                }
                return _html;
            }
        }
        ];
        /* define namespace ends */

        /* model starts */

        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        /* model ends */

        /* collection starts */

        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });

    };
    /* collection ends */
    var routes = {};

    routes[AD.page + '/:_id'] = 'show_grid';
    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/edit/:_id'] = 'show_edit';
    routes[AD.page + '/add'] = 'user_add';
    routes[AD.page + '/delete/:_id'] = 'delete_single';
    routes[AD.page] = 'render';
    
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        user_add: function(){
            this.navigate("#ajax/user_add.html", {
                trigger: true
            });
        }
    });
    

    /* define bootstrap starts */
    AD.bootstrap = function() {
        adReload();
        // alert('bootstrap');

        // if (!_.isUndefined($[AD.page].app)) {
        //     $[AD.page].app.navigate("#" + AD.page + "/grid", {
        //         trigger: true
        //     });
        // }


        AD.app = new MyRouter({
            "AD": AD
        });
        
        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();


})(jQuery);
