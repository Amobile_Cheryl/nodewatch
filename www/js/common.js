(function($)
{
    $.common = {};

    $.common.isDelete = true;

    // Default Portrait
    $.common.defaultPortrait = "iVBORw0KGgoAAAANSUhEUgAAADIAAAAyCAYAAAAeP4ixAAAB20lEQVRoQ+2ZMbdEMBCFbUWHTkfn//8UpY5OhwrV7pl3Xvawi8idieflSLPFRk6+zJ07Qx5FUTw9B8bjBtFEcZqmnxnjOL5nBkHg+b5vJf6iEen73huGwaPfvZEkiReGoSiQCAidftM0i9PX7ZJACEhqsEHo9AkCHSS3KIrYEWKBUCSqqkIZFs9xI8QCqevaSE46Yk7uwCCS0ZgD5nmu4139HwahvNC5E7IjNCowiLSsFDSaKzBIWZbIgWufIRdL01Q773PCDXJHRCOaW1poG++Ma9kCOb2O2CqIWZZB7yyw/XK73q2cP71FsdFroY5FhwJHxBlp2SqIzvRap4M4Y7/OuBY5hXTCo8WQ5VqqDkjBoIVQ7QO2X7WAlMTQQigGIlEYOYVQDIQW4joYJzdEQThRkYiGSLJzk56b5KIRUYuZSgyt4mudM9u15ouagkjkxiUicoOsaEtUWqat/WVzxAkQtFW5lP2iECR1KoiU9NzbXlaOUEVv21bknoTrYBCIJMDcgDgXo0YgJKGu60TvDbe+b5GjxXF8WHKHQKRenoxvb35z6Mj19SaILfkgMMoU9oBWQTguhG706HNbLvcF8pcyOgpD8z5dbgHyXyAU8LyYvkGuLKe9SKmPFi/UicOGWNCF8AAAAABJRU5ErkJggg==";

    // INPUT: Role name list
    // OUTPUT: Permission class
    // jQuery.inArray() Search for a specified value within an array and return its index (or -1 if not found).
    $.common.getPermissionClass = function(roleNameList)
    {
        if ($.inArray($.common.PermissionClass.SuperUserRole.name, roleNameList) > -1)
        {
            return $.common.PermissionClass.SuperUserRole;
        }
        else if ($.inArray($.common.PermissionClass.SystemOwner.name, roleNameList) > -1)
        {
            return $.common.PermissionClass.SystemOwner;
        }
        else if ($.inArray($.common.PermissionClass.DomainOwner.name, roleNameList) > -1)
        {
            return $.common.PermissionClass.DomainOwner;
        }
        else if (($.inArray($.common.PermissionClass.DomainUser.name, roleNameList) > -1))
        {
            return $.common.PermissionClass.DomainUser;
        }
    };

    // Permission class definition
    $.common.PermissionClass = {
        SuperUserRole:
        {
            name: "SuperUserRole",
            display: "Administrator",
            value: 4,
            color: 'GoldenRod'
        },
        SystemOwner:
        {
            name: "SystemOwner",
            display: "System Owner",
            value: 3,
            color: 'GoldenRod'
        },
        DomainOwner:
        {
            name: "DomainOwner",
            display: "Domain Owner",
            value: 2,
            color: 'CornflowerBlue'
        },
        DomainUser:
        {
            name: "DomainUser",
            display: "Domain User",
            value: 1,
            color: 'IndianRed'
        }
    };

})(jQuery);
