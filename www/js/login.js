(function($) {
	var AD = {};
    AD.collections = {};  

    // Initialize
    var initial = function() {
    	// Allow IE 10+ & Chrome
		if(!(($.browser.msie && ($.browser.version>=10)) || ($.browser.webkit) || ($.browser.name === "Netscape"))){
			window.location.href = "browser.html";
		}
		//$.checkLogin();
		$.ajax({
			url: $.config.server_rest_url+'/mySessionData',
			type: 'GET',
			dataType: 'json',
		}).done(function(data){
			if(_.isUndefined(data.sessionId) == false)
			{
				if ($.login.location.pathname.match("login.html") || $.login.location.pathname.match("everLogin.html")) {
					window.location.href = "index.html";
				}
			}
		});
		// var lang_user = window.navigator.userLanguage || window.navigator.language ;
		// if(lang_user === 'zh-TW'){
		// 	var selectId = document.getElementById('language');
		// 	$("#language").children().each(function(){
		// 		console.log("text = " + $(this).text());
		// 	    if ($(this).text()=="中文"){
		// 	        //jQuery給法
		// 	        $(this).attr("selected", "true");
		// 	        this.selected = true; 
		// 	    }
		// 	});
			
		// }else {
		// 	var selectId = document.getElementById('language');
		// 	$('#selectBox option')[0].attr('selected', 'selected');
		// 	// var option2 = new Option('en-US','English');
		// 	// selectId.options.add(option2);
		// 	// var option1 = new Option('zh-TW','中文');
		// 	// selectId.options.add(option1);
		// 	// var new_count = selectId.length;
		// 	// console.log("new_count = " + new_count);
		// }
		var lang = localStorage.getItem("lang") || 'en-US';
		changeLang(lang);
		if ($.browser.msie || ($.browser.name === "Netscape")) {
			var hta = "AMobile-NodeWatch.hta";
		    if(lang==='en-US'){
		    	hta = "AMobile-NodeWatch-enUS.hta";
		    }
	    	$(".hero").after('<h1>If you still can\'t sign in. Please <a href="' + hta + '">CHECK&nbsp;<i class="fa fa-hand-o-up"></i></a> your browser settings!</h1>')
	  	}
		$("#language").val(lang);
		$("#logo img").attr("src", $.config.ui.logo_login);
		$("#logo img").css($.config.ui.loginLogoStyle);
		$(".login-header-big").text($.config.version);
		
		var version = localStorage.getItem("version") || "";
		if(version !== $.config.version){
			console.log("Version update!");
			localStorage.clear();
			localStorage.setItem("version",$.config.version);
		}
	};
        
    // Run login
    var runLogin = function() {
        //Load bootstrap wizard dependency
        loadScript("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js", runBootstrapWizard);
        
        //Bootstrap Wizard Validations
        var runBootstrapWizard = function() {
            
            var $validator = $("#login-form").validate({            
                // Rules for form validation
	            rules: {
	                email: {
	                    required: true,
	                    //account: true
	                    email: true
	                },
	                password: {
	                    required: true,
	                    password: true
	                }
	            },                
	            // Messages for form validation
	            messages: {
	                // email: i18n.t("ns:Login.EnterValidAccount"),
	                // password: i18n.t("ns:Login.EnterValidPassword")
	            },
                
                highlight: function (element) {
                    $(element).removeClass('valid').addClass('invalid');
                    $(element).parent().removeClass('state-success').addClass('state-error');
                },
                unhighlight: function (element) {
                    $(element).removeClass('invalid').addClass('valid');
                    $(element).parent().removeClass('state-error').addClass('state-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent().length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                    $(".help-block").css({color:"#A90329"});
                }
            });
            $('#login-form').submit(function(e) {
            	var $valid = $("#login-form").valid();
	            if (!$valid) {
	                $validator.focusInvalid();
	                return false;
	            } else {
	                if($.browser.webkit){
						console.log('submit');
					}
					e.preventDefault(); //disable all origin events

					//get all login parameter
					var userName = $('input[name="email"]').val().toLowerCase();
					var password = $('input[name="password"]').val();
					var rememberme = $('input[name="remember"]:checked').length > 0;
					var timezone = $('#timezone').val().toLowerCase();
					var user = new $.login.UserModel({
						username: userName,
						password: password,
						timeZone: timezone
					});
					
					// 20141104 new login
					$.ajax({
						url: $.login.url,
						type: 'POST',
						dataType: 'json',
						async: false, // Firefox do not support async=false , Kenny in 2015/2/1
						// withCredentials: true,
						// crossDomain: true,
						data: JSON.stringify(user),
						contentType: 'application/json',
						timeout: 10000,
						error: function(data, status, error) {
							if($.browser.webkit){
								console.log('login error');
							}
							$.smallBox({
								title : i18n.t("ns:Login.Message.LoginFail"),
								content : i18n.t("ns:Login.Message.LoginFailContent"),
								// title : "Login fail...",
								// content : "The username or password you entered is incorrect.",
								color : "#B36464",
								iconSmall : "fa fa-times bounce animated",
								timeout : 6000
							});
							if($.browser.webkit){
								console.log(data);
							}
							$('input[name="password"]').val('');
							//var response=$.parseJSON(data);
							// alert(error);
						},
						success: function(data) {
							if($.browser.webkit){
								console.log('login success');
							}
							setTimeout(3000);
							$.smallBox({
								title: i18n.t("ns:Login.Message.LoginSuccess"),
								content: i18n.t("ns:Login.Message.LoginSuccessContent"),
								// title: "Login succeed...",
								// content: "Loading...",
								color: "#648BB2",
								iconSmall: "fa fa-refresh fa-inverse fa-2x fadeInRight animated",
								timeout: 5000
							});	
							if($.browser.webkit){
								console.log(data);
							}
							if (rememberme == true)
								localStorage.setItem('rememberme',true);                
							localStorage.setItem("lang",$("#language").val());
							window.location.href = "index.html?setLng="+$("#language").val();	
						},
						fail: function(data) {
							if($.browser.webkit){
								console.log('login fail');
								console.log(data);
							}
						}
					});
	            } 
            });
        }
        runBootstrapWizard();       
    };

	/* define bootstrap starts */
    AD.bootstrap = function() {
		// Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
        
        // Initialize
        initial();

		var d = new Date()
		var n = d.getTimezoneOffset()/60*-1;        
        $('#timezone').val(n);
        // Run login
        runLogin();   

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
    };

    /* define bootstrap ends */
    $[AD.page] = AD;
    $[AD.page].bootstrap();
})(jQuery);