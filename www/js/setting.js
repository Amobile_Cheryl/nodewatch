(function($) {
    /* define namespace starts */

    var AD = {};
    AD.title = "ns:Menu.Setting"
    AD.page = "ajax/setting.html";

	var settingDiaglog = function(type){
		console.log(type);
		
		var title;
		var settingData = {};
		var icon;
		
		if (type === "export") {
			// title = "Export Setting";
			title = i18n.t("ns:Setting.ExportSetting");
			icon = "glyphicon glyphicon-export";
			url = $.config.server_rest_url+'/systemConfigs'; //exportSettings
			settingData = {
				'settings':[
					{
						node_name:'Data Export Time',
						node_value:4,
						type:'text'
					},
					{
						node_name:'Data Clean Inteval',
						node_value:5,
						type:'text'
					},
					{
						node_name:'Create Report Inteval',
						node_value:4,
						type:'text'
					},
					{
						node_name:'Create Alert Log Inteval',
						node_value:15,
						type:'text'
					},
					{
						node_name:'Check Timeout Task',
						node_value:30,
						type:'text'
					}
				]
			};
			loadSettingData(title,icon,url,settingData);
		} else if (type === "sysConfig") {
			title = "Configuration";
			icon = "glyphicon glyphicon-cog";
			url = $.config.server_rest_url+'/systemConfigs';
			settingData = {
				'settings':[
					{
						node_name:'Reload Inteval',
						node_value:'50000',
						type:'text'
					},
					{
						node_name:'Reload Status',
						node_value:'1',
						type:'radio'
					}
				]
			};
			loadSettingData(title,icon,url,settingData);
		} else if (type === "mailSetting") {			
			title = i18n.t("ns:Setting.SystemMailSetting");
			icon = "glyphicon glyphicon-envelope";
			url = $.config.server_rest_url+'/mailServerConfigs';
			settingData = {
				'settings':[
					{
						node_name:'host',
						node_value:'smtp.gmail.com',
						type:'text'
					},
					{
						node_name:'port',
						node_value:'587',
						type:'text'
					},
					{
						node_name:'username',
						node_value:'iiiadmintest@gmail.com',
						type:'text'
					},
					{
						node_name:'password',
						node_value:'********',
						type:'text'
					},
					{
						node_name:'from',
						node_value:'iiiadmintest@gmail.com',
						type:'text'
					},
					{
						node_name:'auth',
						node_value:true,
						type:'radio'
					},
					{
						node_name:'starttls',
						node_value:true,
						type:'radio'
					},
				]
			};
			loadSettingData(title,icon,url,settingData);
		} else if (type === "serverSetting") {
			title = "Server URL Setting";
			icon = "glyphicon glyphicon-barcode";
			url = $.config.server_rest_url+'/serverConfigs';
			settingData = {
				'settings':[
					{
						node_name:'url',
						node_value:location.origin+location.pathname,
						type:'text'
					}
				]
			};
			loadSettingData(title,icon,url,settingData);
		}
		

	};
	
	function loadSettingData(title,icon,url,errorData){
		var settingData = {};
		settingData.settings = [];
		$.getJSON(url, function(data) {
			var type = 'text';
			$.each(data, function(key,val) {
				var item = {};
				// eval("item.node_name='"+key+"'");
				eval("item.node_name='"+translateToMultiLanguage(key)+"'");
				eval("item.node_value='"+val+"'");
				eval("item.type='"+type+"'");
				settingData.settings.push(item);
			});
		}).error(function(){
			settingData = errorData;
		}).complete(function(){
			renderDiaglog(title,icon,url,settingData);
		});
	}
	
	function renderDiaglog(title,icon,url,settingData){
		$("#dialog").dialog({
			autoOpen : false,
			modal : true,
			width: 500,
			title : "<div class='widget-header'><h4><i class='icon-ok'></i><i class='"+icon+"'></i> "+title+"</h4></div>",
			buttons : [{
				html : i18n.t("ns:Setting.Cancel"),
				"class" : "btn btn-default",
				click : function() {
					$(this).dialog("close");
				}
			}, {
				html : "<i class='fa fa-check'></i>&nbsp;"+i18n.t("ns:Setting.Submit"),
				"class" : "btn btn-primary",
				click : function(event) {
					// var data = $('#capacitive_dialog :input').serialize()+'&deviceId='+id;
					//var data = $('#dialog :input').serialize();
					var data = "{\""+$('#dialog :input').serialize().replace(/&/g,'","').replace(/=/g,'":"')+"\"}";
					// var dmURL = 'acs-core/RESTful/setting/';
					console.log(data);
					$.ajax({
						type: 'POST',
						url: url,
						data: data,
			            headers: {
			                "X-HTTP-Method-Override": "PUT"
			            },						
						// error: function(data, status, error){
						// 	$('#dialog').dialog().html('<p>Error Code:'+status+'</p><p>Explanation:'+error+'</p>');
						//  $('#dialog').dialog().html('<p>'+i18n.t("ns:Setting.Message.ErrorCode")+status+'</p><p>'+i18n.t("ns:Setting.Message.Explanation")+error+'</p>');
						// }
					}).done(function(data){
						$('#dialog').dialog("close");
					}).fail(function (data, status, error){
						$('#dialog').dialog().html('<p>'+i18n.t("ns:Setting.Message.ServerNoResponse")+'</p>');
						// 	$('#dialog').dialog().html('<p>Error Code:'+status+'</p><p>Explanation:'+error+'</p>');
					});
				}
			}]
		});
		
		var source = $("#tpl_setting_dialog").html();
		var template = Handlebars.compile(source);
		Handlebars.registerHelper("transformat", function(type,nodeName,value) {
			if(type=='text')
				return new Handlebars.SafeString("<input type='text' name='"+nodeName+"' value='"+value+"' size='"+((value.length>20)?value.length:20)+"'></input>");
			else if(type=='radio')
				return new Handlebars.SafeString("<div class='btn-group' data-toggle='bottons'><label class='btn btn-primary bg-color-green txt-color-white'><input type='radio' name='"+nodeName+"' value='true' />Open</label><label class='btn btn-primary bg-color-red txt-color-white'><input type='radio' name='"+nodeName+"' value='false' />Close</label></div>");
			else if(type=='hidden')
				return new Handlebars.SafeString("<input type='hidden' name='"+nodeName+"' value='"+value+"'></input>");
			else if(type=='custom')
				return new Handlebars.SafeString(value);
			else
				return value;
		});
		var settingHTML = template(settingData);
		$("#dialog").html(settingHTML);
		
		$('#dialog').dialog('open');
	}
	
	function translateToMultiLanguage(key){
		var tmpKey;
		switch(key){
			case 'exportPathForUserLog': tmpKey = i18n.t("ns:Setting.ExportSettingMsg.exportPathForUserLog"); break;
			case 'exportPathForTaskLog': tmpKey = i18n.t("ns:Setting.ExportSettingMsg.exportPathForTaskLog"); break;
			case 'exportPathForSystemLog': tmpKey = i18n.t("ns:Setting.ExportSettingMsg.exportPathForSystemLog"); break;
			case 'historyDataCleanInterval': tmpKey = i18n.t("ns:Setting.ExportSettingMsg.historyDataCleanInterval"); break;
			case 'taskTimeoutInterval': tmpKey = i18n.t("ns:Setting.ExportSettingMsg.taskTimeoutInterval"); break;
			case 'host': tmpKey = i18n.t("ns:Setting.SystemMailSettingMsg.host"); break;
			case 'port': tmpKey = i18n.t("ns:Setting.SystemMailSettingMsg.port"); break;
			case 'userName': tmpKey = i18n.t("ns:Setting.SystemMailSettingMsg.userName"); break;
			case 'password': tmpKey = i18n.t("ns:Setting.SystemMailSettingMsg.password"); break;
			default : tmpKey = key; break;
		}
		return tmpKey;
	}

    /* define bootstrap starts */
    AD.bootstrap = function() {
    	// Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

		var am_action_html =
		"<div class=\"appm_property_bg col-lg-12\">"+
			"{{#each am_actions}}"+
				"<div id=\"{{name}}\" class=\"col-xs-6 col-sm-3 col-md-3 col-lg-2\" style=\"min-width:152px;\">"+
						"<img src=\"{{icon}}\" onerror=\"imgLoadErrorBigger(this,'{{name}}','{{color}}','{{icon}}');\">"+
						"<h5>{{this.label}}</h5>"+
				"</div>"+
			"{{/each}}"+
		"<br class=\"clear\" />"+
		"</div>";
		var amPanel_template = Handlebars.compile(am_action_html);
		var settings = {
			"am_actions":[
				{
					name: "export",
					//icon: "glyphicon-download-alt",
					icon: "fa-upload",
					label: i18n.t("ns:Setting.ExportSetting"),
					color: "#2E7BCC"
				},
				/*
				{
					name: "sysConfig",
					//icon: "glyphicon-remove",
					icon: "fa-cog",
					label: "Configuration",
					color: "#FF0039"
				},
				*/
				{
					name: "mailSetting",
					//icon: "glyphicon-plus",
					icon: "fa-envelope",
					label: i18n.t("ns:Setting.SystemMailSetting"),
					color: "#FFA500"
				}
				/*
				,
				{
					name: "serverSetting",
					//icon: "glyphicon-play",
					icon: "fa-barcode",
					label: "Server URL",
					color: "#3FB618"
				}
				*/
			]
		};
		
		$("#settings").empty();
		$("#settings").append(amPanel_template(settings));
		//$("#am_panel .summary_bg").css("height",am_actions.am_actions.length*300/6);

		$("#settings .appm_property_bg > div").on("click", function(event) {
            var type = $(this).attr("id");
			settingDiaglog(type);
			event.preventDefault();
        });
		
		/*
		$("#settings li").on("click", function(event) {
            var type = $(this).attr("id");
			settingDiaglog(type);
			event.preventDefault();
        });
		*/
		$("body").removeClass("loading");
    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    // $[AD.page].bootstrap();


})(jQuery);

function imgLoadErrorBigger(t,name,color,icon){
	$(t).replaceWith(
	"<div" 
		+" class=\"faa-parent animated-hover\""
		+" style=\""
			+"-webkit-border-radius: 10px;  -moz-border-radius: 10px;  border-radius: 10px;"
			+"width: 148px;"
			+"height: 168px;"
			+"background-color: " +color+";" //#3678DB;"
			+"text-align: center;"
			+"line-height: 229px;"
			+"margin: 0px auto;"
	+"\">"
		+"<i class=\"fa fa-inverse "+icon+" faa-tada\" style=\"font-size: 100px;\"></i>"
	+"</div>"
	);
}


// })(jQuery);
//</script>
