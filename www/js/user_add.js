(function($) {
    var AD = {};
    AD.page = "ajax/user_add.html";
    var defautRoleSetting  = function(){
        /*None*/
        $('#noneButton').click(function() {
            $('input[type="checkbox"]').prop('checked', false);
        });
        /*Device Manager*/
        $('#deviceManagerButton').click(function() {
            $('input[type="checkbox"]').prop('checked', false);
            $('input[name="Devices"]').click();
        });
        /*Appication Manager*/
        $('#applicationManagerButton').click(function() {
            $('input[type="checkbox"]').prop('checked', false);
            $('input[name="File"]').click();
        });
        /*Account Manager*/
        $('#accountManagerButton').click(function() {
            $('input[type="checkbox"]').prop('checked', false);
            $('input[name="User Viewer"]').click();
        });
        /*Administrator*/
        $('#administratorButton').click(function() {
            $('input[type="checkbox"]').prop('checked', true);
        });
    }

    var uploadImage = function(){
        // Image
        var result = $('#result');
        var currentFile;
        var replaceResults = function (img) {
            var content;
            if (!(img.src || img instanceof HTMLCanvasElement)) {
                content = $('<span>Loading image file failed</span>');
            } else {
                content = $('<a target="_blank">').append(img)
                    .attr('download', currentFile.name)
                    .attr('href', img.src || img.toDataURL());
            }
            result.children().replaceWith(content);
            $("#appearance").val(content[0].href);
        };
        var displayImage = function (file, options) {
            currentFile = file;
            if (!loadImage(
                    file,
                    replaceResults,
                    options
                )) {
                result.children().replaceWith(
                    $('<span>Your browser does not support the URL or FileReader API.</span>')
                );
            }
        };
        var dropChangeHandler = function (e) {
            e.preventDefault();
            e = e.originalEvent;
            var target = e.dataTransfer || e.target,
                file = target && target.files && target.files[0],
                options = {
                    maxWidth: result.width(),
                    canvas: true
                };
            if (!file) {
                return;
            }
            loadImage.parseMetaData(file, function (data) {
                displayImage(file, options);
            });
        };
        $("#appearanceInput").on("change",dropChangeHandler);
    }
    var reloadJs = function() {
        defautRoleSetting();
        uploadImage();
    }

    AD.bootstrap = function() {
		$("body").removeClass("loading");
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Translate Input of the placeholder
        translatePlaceholder();

        // Reload JavaScript
        reloadJs();

        // var hash = window.location.hash;
        // var id = hash.replace(/^#ajax\/user_add.html\/(\w+)$/, "$1");

        // if (!_.isUndefined($[AD.page].app)) {
        //     $[AD.page].app.navigate("#" + AD.page + "/" + id, {
        //         trigger: true
        //     });
        // }

        // if (!Backbone.History.started) {
        //     Backbone.emulateHTTP = true;
        //     Backbone.history.start();
        // }

    };
    /* define bootstrap ends */
    $[AD.page] = AD;

    function translatePlaceholder () {
        $("#username").attr("placeholder", i18n.t("ns:User.Message.UserName"));
        $("#description").attr("placeholder", i18n.t("ns:User.Message.Description"));
        $("#password").attr("placeholder", i18n.t("ns:User.Message.Password"));
        $("#repeatpassword").attr("placeholder", i18n.t("ns:User.Message.RepeatPassword"));
        $("#email").attr("placeholder", i18n.t("ns:User.Message.Email"));
        $("#appearanceInput").attr("placeholder", i18n.t("ns:User.Message.DeviceTypeAppearance"));
    }

})(jQuery);