(function($) {
    var AD = {};
    var referer;
    var id;
    var validateRule;
    AD.page = "ajax/user_modify.html";

    var uploadImage = function() {
        // Image
        var result = $('#result');
        var currentFile;
        var replaceResults = function(img) {
            var content;
            if (!(img.src || img instanceof HTMLCanvasElement)) {
                content = $('<span>Loading image file failed</span>');
            } else {
                content = $('<a target="_blank">').append(img)
                    //.attr('download', currentFile.name)
                    .attr('href', img.src || img.toDataURL());
            }
            result.children().replaceWith(content);
            $("#appearance").val(content[0].href);
        };
        var displayImage = function(file, options) {
            currentFile = file;
            if (!loadImage(
                    file,
                    replaceResults,
                    options
                )) {
                result.children().replaceWith(
                    $('<span>Your browser does not support the URL or FileReader API.</span>')
                );
            }
        };
        var dropChangeHandler = function(e) {
            e.preventDefault();
            e = e.originalEvent;
            var target = e.dataTransfer || e.target,
                file = target && target.files && target.files[0],
                options = {
                    maxWidth: result.width(),
                    canvas: true
                };
            if (!file) {
                return;
            }
            loadImage.parseMetaData(file, function(data) {
                displayImage(file, options);
            });
        };
        $("#appearanceInput").on("change", dropChangeHandler);
    }
        // console.log($("#title").children('font'));
    var addPage = function() {
        //$("#title").children('font').text('Add');
        $("#userName").next().attr('data-i18n', 'ns:User.Add.Add');
        $("#userName").next().text('Add');
        $("#formTitle").children('font').text('Add');
        $('input[name="appearance"]').attr('value', "data:image/png;base64," + $.common.defaultPortrait);
        $('#result').children().replaceWith('<img src="data:image/png;base64,' + $.common.defaultPortrait + '"/>');
        $("#permissionTree").attr('style', 'display:none');

        validateRule = {
            userName: {
                required: true,
                //account: true
            },
            password: {
                required: true,
                minlength: 6,
                maxlength: 20,
                password: true           
            },
            repeatPassword: {
                required: true,
                equalTo: '#password'
            },
            email:{
                required: true,
                email: true
            },
            mobilePhone:{
                required: true,
                //tel: true
            },
            // appearanceFile: {
            //     required: true
            // },
            domainPermission: {
                required: true
            },
            domainName: {
                required: true
            }
        };

        $.each($.common.PermissionClass,function(i,item){
            if (item.value <= $.login.user.permissionClass.value && item != $.common.PermissionClass.SuperUserRole)
                $("#domainPermission").append($("<option></option>").attr("value", item.name).text(item.display));
        });

        if ($.login.user.permissionClass.value >= $.common.PermissionClass.SystemOwner.value){
            $.ajax({
                type: "GET",
                async: false,
                url: $.config.server_rest_url + '/domains?pageNumber=1&pageSize=100',
                success: function(data) {
                    domainList = data.content;
                    $.each(data.content, function(i, item) {
                        if (typeof item.id != 'undefined')
                            $("#domainName").append($("<option></option>").attr("value", item.id).text(item.name));
                    })
                    // hide System domain
                    $('option[value="1"]').hide();                         
                },
                error: function(data) {
                    console.log('Get Error');
                    // alert('Get Role List Failed!!');
                    return;
                },
            });            
        }
        else
        {
            $("#domainSelection").attr('style','display:none');
        }        
    }

    var editPage = function(id) {
        //$("#title").children('font').text('Edit');
        $("#userName").next().attr('data-i18n', 'ns:User.Edit.Edit');
        $("#userName").next().text('Edit');
        $("#formTitle").children('font').text('Edit');
        $("#userName").attr('disabled', true);
        
        var url = $.config.server_rest_url + '/users';
        //url += "?pageNumber=1&pageSize=100";
        $.ajax({
            type: "GET",
            url: url + '/' + id,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                // for tab1 to display user informaiton
                $("#userName").text(data.userName + " > ");
                $('input[name="userName"]').val(data.userName);
                $('input[name="description"]').val(data.remark);
                $('input[name="email"]').val(data.email);
				$('input[name="mobilePhone"]').val(data.mobilePhone);
                // $('#result').children('a').attr('href',"data:image/png;base64,"+data.portrait);
                var portrait = data.portrait || $.common.defaultPortrait;
                $('input[name="appearance"]').attr('value', "data:image/png;base64," + portrait);
                $('#result').children().replaceWith('<img src="data:image/png;base64,' + portrait + '"/>');
                $('input').removeAttr('placeholder');
                if(data.email==""){
                    $("#email").attr("placeholder", i18n.t("ns:User.Message.EmailExample"));
                }

                // for tab2 to display permission
                var roleNameList = {};
                console.log('Start to check relative permission.');

                var permission = $.common.getPermissionClass(data.roleNameSet);

                // Edited user's permission is less than DomainOwner, then it only can set DomainUser, DomainOwner
                if (permission.value <= $.common.PermissionClass.DomainOwner.value){
                    $.each($.common.PermissionClass,function(i,item){
                        if (item.value <= $.common.PermissionClass.DomainOwner.value)
                            $("#domainPermission").append($("<option></option>").attr("value", item.name).text(item.display));
                    });                    
                }
                // Edited user's permission is greater than SystemOwner
                else   
                {
                    $.each($.common.PermissionClass,function(i,item){
                        if (item.value == $.common.PermissionClass.SystemOwner.value)
                            $("#domainPermission").append($("<option></option>").attr("value", item.name).text(item.display));
                    });                     
                }

                $("#domainPermission").val(permission.name);
                $("#domainPermission").change();
                $("#domainName").val(data.domainId);

                $.permission_tree_util.permissionTreeCheck(data.roleNameSet);
            },
            async: false
        });

        validateRule = {
            userName: {
                required: true
            },
            // email: {
            //     // required: true,
            //     email: "Your email address must be in the format of name@domain.com"
            // },
            password: {
                // minlength: 6
                // required: true
            },
            repeatPassword: {
                // required: true,
                // minlength: 6,
                equalTo: '#password'

            },
            // appearanceFile: {
            //     required: true
            // },            
            domainPermission: {
                required: true
            },
            domainName: {
                required: true
            }
            // description: {
            //     required: true
            // }
        };
  
        $("#domainSelection").attr('style','display:none');
    }
    var reloadJs = function() {
        // defautRoleSetting();
        // initial condition
        $("#domainName").attr('disabled', 'disabled');
        $('input[type="checkbox"]').prop('disabled', 'disabled');   

        uploadImage();
        var hash = window.location.hash;
        var urlPathArray = hash.split("\/");
        referer = ((typeof urlPathArray[2] != "undefined") ? urlPathArray[2] : "") || "";
        id = ((typeof urlPathArray[3] != "undefined") ? urlPathArray[3] : "") || "";
        if (referer == "add") {
            addPage();
        } else if (referer == "edit") {
            editPage(id);
        }
    }

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
                
		$("body").removeClass("loading");
        var url = $.config.server_rest_url + '/roles';
        url += "?pageNumber=1&pageSize=100";
        var roleList;
        var domainList;

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Translate Input of the placeholder
        translatePlaceholder();
        
		$("#appearanceInput").change(function(event){
			$("#customFileInput span").text(this.files[0].name);
		});
		
        $("#domainPermission").change(function() {
            // SuperUserRole
            if ($("#domainPermission").val() == $.common.PermissionClass.SuperUserRole.name) {
                $("#permissionColor").attr('style', 'color:'+$.common.PermissionClass.SuperUserRole.color);
                $('input[type="checkbox"]').attr('disabled', 'disabled');
                $("#permissionTree").removeAttr('style');
                $('input[type="checkbox"]').prop('checked', true);

                $("#domainName").val(1);
                $("#domainName").attr('disabled', 'disabled');
            }            
            // SystemOwner
            else if ($("#domainPermission").val() == $.common.PermissionClass.SystemOwner.name) {
                $("#permissionColor").attr('style', 'color:'+$.common.PermissionClass.SystemOwner.color);
                $('input[type="checkbox"]').attr('disabled', 'disabled');
                $("#permissionTree").removeAttr('style');
                $('input[type="checkbox"]').prop('checked', true);

                $("#domainName").val(1);
                $("#domainName").attr('disabled', 'disabled');
            } 
            // DomainOwner
            else if ($("#domainPermission").val() == $.common.PermissionClass.DomainOwner.name) {
                $("#permissionColor").attr('style', 'color:'+$.common.PermissionClass.DomainOwner.color);
                $("#permissionTree").removeAttr('style');
                $('input[type="checkbox"]').attr('disabled', 'disabled');
                $('input[type="checkbox"]').prop('checked', true);
                $('input[name="DomainViewer"]').prop('checked', false);
                $('input[name="DomainOperator"]').prop('checked', false);
                $('input[name="AllPermission"]').prop('checked', false);

                $("#domainName").val("");
                $("#domainName").removeAttr('disabled');
            } 
            // DomainUser
            else if ($("#domainPermission").val() == $.common.PermissionClass.DomainUser.name) {
                $("#permissionColor").attr('style', 'color:'+$.common.PermissionClass.DomainUser.color);
                $("#permissionTree").removeAttr('style');
                $('input[type="checkbox"]').removeAttr('disabled');
                $('input[type="checkbox"]').prop('checked', false);


                // DasdboardViewer is required
                $('input[name="DashboardViewer"]').prop('checked', true);
                $('input[name="DashboardViewer"]').attr('disabled', 'disabled');

                // Disable
                $('input[name="DomainViewer"]').attr('disabled', 'disabled');
                $('input[name="DomainOperator"]').attr('disabled', 'disabled');
                $('input[name="UserOperator"]').attr('disabled', 'disabled');
                // $('input[name="AllPermission"]').attr('disabled', 'disabled');

                $("#domainName").val("");
                $("#domainName").removeAttr('disabled');
            } 
            // Default Case (No selection)
            else {
                $("#permissionColor").removeAttr('style');
                $("#permissionTree").attr('style', 'display:none');
                $('input[type="checkbox"]').prop('disabled', 'disabled');
                $('input[type="checkbox"]').prop('checked', false);

                $("#domainName").val("");
                $("#domainName").attr('disabled', 'disabled');
            }
        });

        $.ajax({
            type: "GET",
            async: false,
            url: url,
            success: function(data) {
                roleList = data.content;
            },
            error: function(data) {
                console.log('Get Error');
                // alert('Get Role List Failed!!');
                return;
            },
        });
        
        // $.each(data, function(i, item) {
        //     if (typeof item.id != 'undefined')
        //         $("#domainName").append($("<option></option>").attr("value", item.id).text(item.name));
        // })

        // Reload JavaScript
        reloadJs();


        var $validator = $("#wizard-1").validate({

            rules: validateRule,

            messages: {
                userName:           i18n.t("ns:Message.ThisFieldRequire"),
                password:           i18n.t("ns:Message.ThisFieldRequire"),
                repeatPassword:     i18n.t("ns:Message.ThisFieldRequire"),
                domainPermission:   i18n.t("ns:Message.ThisFieldRequire"),
                domainName:         i18n.t("ns:Message.ThisFieldRequire"),
                mobilePhone:        i18n.t("ns:Message.ThisFieldRequire"),
                email:              i18n.t("ns:Message.EnterValidEmail")
            },

            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });

        $('#bootstrap-wizard-1').bootstrapWizard({
            'tabClass': 'form-wizard',
            'onTabShow': function(tab, navigation, index) {
                switch (index) {
                    case 0:
                        $("#wizard-1 .next a").text(i18n.t("ns:Message.Next"));
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index).removeClass('complete');
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(0).find('.step').text("1")
                        $('#btn_back').removeClass("disabled");
                        break;
                    case 1:
                        $("#wizard-1 .next a").text(i18n.t("ns:Message.Save"));
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                            .html('<i class="fa fa-check"></i>');
                        $('#btn_save').removeClass("disabled");
                        break;
                }
            },
            'onPrevious': function(tab, navigation, index) {
                if ($('#bootstrap-wizard-1').bootstrapWizard('currentIndex') === 0) {
                    window.location.href = "#ajax/user_list.html/grid";
                }
            },
            'onNext': function(tab, navigation, index) {
                // Validate first
                var $valid = $("#wizard-1").valid();
                if (!$valid) {
                    console.log('Not valid');
                    $validator.focusInvalid();
                    return false;
                } else {
                    console.log('Valid');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass(
                        'complete');
                    $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                        .html('<i class="fa fa-check"></i>');
                }


                var wizard = $('#bootstrap-wizard-1');
                // if (wizard.bootstrapWizard('currentIndex') === wizard.bootstrapWizard('navigationLength') - 1) {
                //     $("#wizard-1 .next a").text("Save");
                // } else {
                //     $("#wizard-1 .next a").text("Next");
                // }
                // console.log($('#bootstrap-wizard-1').tab1.rolename);
                // Last page
                if (wizard.bootstrapWizard('currentIndex') == wizard.bootstrapWizard('navigationLength')) {
                    // console.log('Dashboard is :'+$('input[name="dashboard"]').is(':checked'));
                    // console.log('permission is');
                    // console.log($('input[name="All Permission"]').is(':checked'));

                    var comparedRoleIdSet = [];

                    // Get server's role list
                    var roleNameList = [];
                    $.each(roleList, function(index, value) {
                        if (value.name)
                            roleNameList.push(value.name);
                    });
                    console.log('roleNameList is ');
                    console.log(roleNameList);

                    // Collect user's selection
                    var selectedRoleList = $('#permissionTree :input:checked');
                    var selectedRoleNameList = [];
                    $.each(selectedRoleList, function(index, value) {
                        selectedRoleNameList.push(value.name);
                    });
                    selectedRoleNameList.push($("#domainPermission").val());
                    console.log('selectedRoleNameList is ');
                    console.log(selectedRoleNameList);
                    // console.log(selectedRoleList);
                    var comparedRoleNameSet = [];
                    $.each(selectedRoleNameList, function(index, value) {
                        if ($.inArray(value, roleNameList) > -1)
                            comparedRoleNameSet.push(value);
                    });
                    console.log('comparedRoleNameSet is ');
                    console.log(comparedRoleNameSet);

                    $.each(comparedRoleNameSet, function(cindex, cvalue) {
                        $.each(roleList, function(dindex, dvalue) {
                            if (dvalue.name == cvalue) {
                                comparedRoleIdSet.push(dvalue.id);
                            }
                        })
                    });
                    // console.log('roleidset is');
                    // console.log(comparedRoleIdSet);                        



                    var data;
                    if (referer == 'add') {
                        data = {
                            userName: $('input[name="userName"]').val(),
                            email: $('input[name="email"]').val(),
                            mobilePhone: $('input[name="mobilePhone"]').val(),
                            password: $('input[name="password"]').val(),
                            remark: $('input[name="description"]').val(),
                            portrait: $('input[name="appearance"]').val().replace("data:image/png;base64,", ""),
                            roleIdSet: comparedRoleIdSet,
                            domainId: $("#domainName").val()
                        };
                        url = $.config.server_rest_url + '/users';
                        $.ajax({
                            type: "POST",
                            async: false,
                            url: url,
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify(data),
                            success: function(data) {
                                $.smallBox({
                                    title: i18n.t("ns:Message.User.UserAdd") + i18n.t("ns:Message.User.AddSuccess"),
                                    content: "<i class='fa fa-plus'></i> <i>"+i18n.t("ns:Message.User.AddSuccess")+"</i>",
                                    // title: "[User Add] Added successfully",
                                    // content: "<i class='fa fa-plus'></i> <i>Added successfully</i>",
                                    color: "#648BB2",
                                    iconSmall: "fa fa-plus fa-2x fadeInRight animated",
                                    timeout: 5000
                                });                                    
                                console.log('Add Success');
                                window.location.href = '#ajax/user_list.html';
                                // console.log(data);
                            },
                            error: function(data) {
                                $.smallBox({
                                    title: i18n.t("ns:Message.User.UserAdd") + i18n.t("ns:Message.User.AddFailed"),
                                    content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Message.User.AddFailed") + "</i>",
                                    // title: "[User Add] Added failed",
                                    // content: "<i class='fa fa-plus'></i> <i>Added failed</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });                                
                                console.log('Add Error');
                                // alert('Create Failed!!');
                                // console.log(data);   
                            },
                            // type:"json",
                            dataType: "json"
                        });
                    } else if (referer == 'edit') {
                        data = {
                            //email: $('input[name="email"]').val(),
                            mobilePhone: $('input[name="mobilePhone"]').val(),
                            remark: $('input[name="description"]').val(),
                            portrait: $('input[name="appearance"]').val().replace("data:image/png;base64,", ""),
                            roleIdSet: comparedRoleIdSet
                        };
                        if ($('input[name="password"]').val() != "")
                            data.password = $('input[name="password"]').val();

                        url = $.config.server_rest_url + '/users/' + id;
                        $.ajax({
                            type: "POST",
                            async: false,
                            url: url,
                            contentType: 'application/json; charset=utf-8',
                            headers: {
                                "X-HTTP-Method-Override": "PUT"
                            },
                            data: JSON.stringify(data),
                            success: function(data) {
                                $.smallBox({
                                    title: i18n.t("ns:Message.User.UserEdit") + i18n.t("ns:Message.User.EditSuccess"),
                                    content: "<i class='fa fa-pencil-square-o'></i> <i>" + i18n.t("ns:Message.User.EditUserID") + "&nbsp;" + id + "</i>",
                                    color: "#648BB2",
                                    iconSmall: "fa fa-pencil-square-o fa-2x fadeInRight animated",
                                    timeout: 5000
                                });                                
                                console.log('Add Success');
                                window.location.href = '#ajax/user_list.html';
                                // console.log(data);
                            },
                            error: function(data) {
                                $.smallBox({
                                    // title: i18n.t("ns:Message.User.UserEdit") + i18n.t("ns:Message.User.EditSuccess"),
                                    title: i18n.t("ns:Message.User.UserEdit") + i18n.t("ns:Message.User.EditFailed"),
                                    content: "<i class='fa fa-pencil-square-o'></i> <i>" + i18n.t("ns:Message.User.EditUserID") + "&nbsp;" + id + "</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });                                  
                                console.log('Edit Error');
                                // alert('Create Failed!!');
                                // console.log(data);   
                            },
                            // type:"json",
                            dataType: "json"
                        });
                    }


                }
                console.log('Tab number:' + index);
            }
        });

        $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
        // Handle collapse & expand

        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').find(' > i').on('click', function(e) {
            var children = $(this).parent().parent('li.parent_li').find(' > ul > li');
            if (children.is(':visible')) {
                console.log('Expand Children');
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').removeClass().addClass('fa fa-lg fa-plus-circle');
            } else {
                console.log('Collapse Children');
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').removeClass().addClass('fa fa-lg fa-minus-circle');
            }
            e.stopPropagation();
        });

        // Check or uncheck non-leaf
        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').find(' > input').on('click', function(e) {
            // Checked
            if ($(this).is(':checked')) {
                $.permission_tree_util.recursiveCheck($(this), true);
                // Click "AllPermission" input box
                if ($(this).get(0).name == "AllPermission" && $("#domainPermission").val() == 'DomainUser') {
                    $('input[name="DomainViewer"]').prop('checked', false);
                    $('input[name="DomainOperator"]').prop('checked', false);
                    $('input[name="UserOperator"]').prop('checked', false);
                } 
                // Click "UserViewer" input box
                else if ($(this).get(0).name == "UserViewer" && $("#domainPermission").val() == 'DomainUser') {
                    $('input[name="UserOperator"]').prop('checked', false);
                }
            }
            // Unchecked
            else {
                $.permission_tree_util.recursiveCheck($(this), false);
                if ($(this).get(0).name == "AllPermission" && $("#domainPermission").val() == 'DomainUser') {
                    $('input[name="DashboardViewer"]').prop('checked', true);   
                }                
            }
            e.stopPropagation();
        });

        // Check or uncheck every node
        $('#permissionTree :input').on('click', function() {
            if ($(this).is(':checked')) {
                console.log('click');
                $.permission_tree_util.recursiveParentCheck($(this));
            } else {
                console.log('unclick');
                // Operator leaf node don't need to uncheck parent
                if ($(this).get(0).name.indexOf("Operator") < 0)
                    $.permission_tree_util.recursiveParentUncheck($(this));
            }

            // If all the input except 'AllPermission' is checked, we have to check 'AllPermission'.
            var isAllPermissionsChecked = true;
            $.each($('#permissionTree :input'), function(index, value) {
                if (value.name != 'AllPermission' && value.name != 'DomainViewer' && value.name != 'DomainOperator' && value.name != 'UserOperator') {
                    // console.log('Checking ' + value.name);
                    if (!$(value).is(':checked')) {
                        // console.log('Someone is not checked');
                        isAllPermissionsChecked = false;
                    }
                }
            });
            $('input[name="AllPermission"]').prop('checked', isAllPermissionsChecked);
        });

    };
    /* define bootstrap ends */
    $[AD.page] = AD;

    function translatePlaceholder () {
        $("#userName").attr("placeholder", i18n.t("ns:User.Message.UserName"));
        $("#password").attr("placeholder", i18n.t("ns:User.Message.Password"));
        $("#repeatPassword").attr("placeholder", i18n.t("ns:User.Message.RepeatPassword"));
        $("#email").attr("placeholder", i18n.t("ns:User.Message.EmailExample"));
        $("#appearanceInput").attr("placeholder", i18n.t("ns:User.Message.Portrait"));
    }

})(jQuery);
