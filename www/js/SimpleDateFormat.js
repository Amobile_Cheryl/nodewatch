(function() {

    var monname = ["", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
    var monN = ["", "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    var monday = [0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    var weekDAY = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
    var weekd = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"];
    var t, yr, mon, day, weekday, hr, min, sec, ms, dayyr, zone;


    //*****
    DateFormat = function() {
        this.fm = 'yyyy-MM-dd HH:mm:ss';
    }

    //set the format
    DateFormat.prototype.setFormat = function(f) {
        this.fm = f;
        return f;
    }

    DateFormat.prototype.format = function(int) {
        t = this.date = new Date(Number(int));

        yr = t.getFullYear();
        mon = t.getMonth() + 1; //0~11 => 1~12
        day = t.getDate();
        weekday = t.getDay(); //0=Sun,1=Mon...
        hr = t.getHours();
        min = t.getMinutes();
        sec = t.getSeconds();
        ms = t.getMilliseconds();
        zone = t.getTimezoneOffset() / 60;
        dayyr = 0;
        for (i = 0; i < mon; i++)
            dayyr = dayyr + monday[i];
        dayyr = dayyr + day;

        var get = Translate(df.fm);
        var ans = get.toString();
        return ans;
    }

    //parse the date we want & change to 1970 ms
    DateFormat.prototype.parse = function(str) {
        var split_input;
        var a = -1; //am=0, pm=1
        t = this.date = new Date(0);
        var restr = "G|yyyy|yy|M{1,4}|w|W|D|dd?|F|EEEE|EEE|E|a|HH?|kk?|K|h|mm?|ss?|S|z|Z"; //dateformat RegExp String
        var regex = new RegExp(restr, "g");
        if (!this.fm) {
            console.log("eror : fm is null");
            return 0;
        }
        var chracter = this.fm.match(regex); // split the element(dateformat) we want
        var regex2 = this.fm; //initial regex2
        for (i = 0; i <= chracter.length; i++) {
            switch (chracter[i]) {
                /*  \w -> '``' */
                //2 char
                case 'G':
                case 'a':
                    regex2 = regex2.replace(chracter[i], "([A-Z]{2})");
                    break;

                    //4 int
                case 'yyyy':
                    regex2 = regex2.replace(chracter[i], "([0-9]{4})");
                    break;

                    //2 int
                case 'yy':
                case 'MM':
                case 'dd':
                case 'HH':
                case 'kk':
                case 'mm':
                case 'ss':
                    regex2 = regex2.replace(chracter[i], "([0-9]{2})");
                    break;

                    //n char
                case 'MMMM':
                case 'EEEE':
                    regex2 = regex2.replace(chracter[i], "([A-Z]``+)"); //([A-Z]\\w+)
                    break;

                    //3 char
                case 'MMM':
                case 'EEE':
                    regex2 = regex2.replace(chracter[i], "([A-Z]``{2})"); //[A-Z]\\w{2}
                    break;

                    //n int

                case 'w':
                case 'M':
                case 'D':
                case 'd':
                case 'H':
                case 'k':
                case 'K':
                case 'h':
                case 'm':
                case 's':
                case 'S':
                    regex2 = regex2.replace(chracter[i], "([0-9]+)");
                    break;

                    //1 int
                case 'W':
                case 'F':
                case 'E':
                    regex2 = regex2.replace(chracter[i], "([0-9])");
                    break;

                    //3 char
                case 'z':
                    regex2 = regex2.replace(chracter[i], "([``-:, ]+)"); //[\\w-:, ]+
                    break;
                    //- 4 int
                case 'Z':
                    regex2 = regex2.replace(chracter[i], "([+-][0-9]{2}00)");
                    break;

                default:
                    break;
            }
        }
        regex2 = regex2.replace(/``/g, "\\w"); //chang to correct regexp
        //console.log("new regex2= "+regex2);

        var regexp2 = new RegExp(regex2);
        var test = regexp2.test(str); //test the input date match the format or not
        if (!test) {
            // alert("date type does not match");
            console.log("error : str=" + str);
            console.log("error : for=" + this.fm);
        } else {
            split_input = regexp2.exec(str);
        }

        for (i = 0; i < chracter.length; i++) { //set the date
            //console.log("debug ch[i]"+chracter[i]);
            switch (chracter[i]) {
                case 'yyyy':
                    t.setFullYear(split_input[i + 1]);
                    break;
                case 'yy':
                    t.setYear(split_input[i + 1]);
                    break;
                case 'MMMM':
                    var index = monname.indexOf(split_input[i + 1]);
                    if (index == -1);
                        // alert("error:month name (MMMM)" + split_input[i + 1]);
                    else
                        t.setMonth(index);
                    break;
                case 'MMM':
                    var index = monN.indexOf(split_input[i + 1]);
                    if (index == -1);
                        // alert("error:month name (MMM) " + split_input[i + 1]);
                    else
                        t.setMonth(index);
                    break;
                case 'MM':
                case 'M':
                    t.setMonth(Number(split_input[i + 1] - 1));
                    break;
                case 'w':
                    //
                    break;
                case 'W':
                    //
                    break;
                case 'D':
                    /*bug here
					var i = 1;
					var D = split_input[i+1];
					while(D > monday[i]){
						D = D - Number(monday[i++]);
					}
					t.setMonth(i-1);
					t.setDate(D);*/
                    break;
                case 'dd':
                case 'd':
                    t.setDate(split_input[i + 1]);
                    break;
                case 'F':
                    //
                    break;
                case 'EEEE':
                    //var index = weekDAY.indexOf(split_input[i+1]);
                    break;
                case 'EEE':
                    //
                    break;
                case 'E':
                    //
                    break;
                case 'a':
                    if (split_input[i + 1] == "AM")
                        a = 0;
                    else if (split_input[i + 1] == "PM")
                        a = 1;
                    break;
                case 'HH':
                case 'H':
                    t.setHours(split_input[i + 1]);
                    break;
                case 'kk':
                case 'k':
                    t.setHours(Number(split_input[i + 1]) - 1);
                    break;
                case 'K':
                    if (a == 1)
                        t.setHours(Number(split_input[i + 1]) + 12);
                    else if (a == 0)
                        t.setHours(split_input[i + 1]);
                    break;
                case 'h':
                    if (a == 1)
                        t.setHours(Number(split_input[i + 1]) + 12);
                    else if (a == 0)
                        t.setHours(Number(split_input[i + 1]));
                    break;
                case 'mm':
                case 'm':
                    t.setMinutes(Number(split_input[i + 1]));
                    break;
                case 'ss':
                case 's':
                    t.setSeconds(split_input[i + 1]);
                    break;
                case 'S':
                    t.setMilliseconds(split_input[i + 1]);
                    break;
                case 'z':
                    //
                    break;
                case 'Z':
                    //
                    break;

                default:
                    break;
            }
        }

        return this.date.getTime();
    }

    //translate format to date (yy->14)
    function Translate(fstr) {
        var split = fstr.split("");
        var ans = "";
        var temp = "",
            ch = "";
        for (i = 0; i <= fstr.length; i++) {
            if (ch[0] == split[i] && ch.length < 4) {
                ch = ch + split[i];
                continue;
            } else {
                switch (ch) {
                    case 'G':
                        temp = "AD";
                        break;
                    case 'yyyy':
                        temp = yr;
                        break;
                    case 'yy':
                        temp = yr % 100;
                        if (temp < 10)
                            temp = "0" + temp;
                        break;
                    case 'MMMM':
                        temp = monname[mon];
                        break;
                    case 'MMM':
                        //str=monname[mon]
                        temp = monN[mon];
                        break;
                    case 'MM':
                        mon = mon;
                        if (mon < 10)
                            temp = "0" + mon;
                        else
                            temp = mon;
                        break;
                    case 'M':
                        temp = mon;
                        break;
                    case 'w':
                        temp = (dayyr - weekday + 7 + 6) / 7;
                        temp = Math.floor(temp);
                        break;
                    case 'W':
                        temp = Math.floor((day - weekday + 13) / 7);
                        break;
                    case 'D':
                        temp = dayyr;
                        break;
                    case 'dd':
                        if (day < 10)
                            temp = "0" + day;
                        else
                            temp = day;
                        break;
                    case 'd':
                        temp = day;
                        break;
                    case 'F':
                        temp = weekday;
                        break;
                    case 'EEEE':
                        temp = weekDAY[weekday];
                        break;
                    case 'EEE':
                        //str=weekDAY[weekday];
                        temp = weekd[weekday];
                        break;
                    case 'E':
                        temp = weekday;
                        break;
                    case 'a':
                        if (hr >= 12)
                            temp = "PM";
                        else
                            temp = "AM";
                        break;
                    case 'HH':
                        if (hr < 10)
                            temp = "0" + hr;
                        else
                            temp = hr;
                        break;
                    case 'H':
                        temp = hr;
                        break;
                    case 'kk':
                        if (hr == 0)
                            hr = 24;
                        if (hr < 10)
                            temp = "0" + hr;
                        else
                            temp = hr;
                        break;
                    case 'k':
                        temp = hr;
                        break;
                    case 'K':
                        temp = hr % 12;
                        break;
                    case 'h':
                        temp = hr % 12;
                        if (temp == 0)
                            temp = 12;
                        break;
                    case 'mm':
                        if (min < 10)
                            temp = "0" + min;
                        else
                            temp = min;
                        break;
                    case 'm':
                        temp = min;
                        break;
                    case 'ss':
                        if (sec < 10)
                            temp = "0" + sec;
                        else
                            temp = sec;
                        break;
                    case 's':
                        temp = sec;
                        break;
                    case 'S':
                        temp = ms;
                        break;
                    case 'z':
                        temp = "GMT-08:00";
                        break;
                    case 'Z':
                        if (zone < 10 || zone > -10)

                            if (zone < 0)
                                zone = "+" + zone
                        temp = zone + "00";
                        break;

                    default:
                        temp = ch;
                        break;
                }

                ch = split[i];
            }
            ans = ans + temp;
            temp = "";
        }
        //alert(ans);
        return ans;
        //document.FORM.entry.value="";
    }

    //click date->ms
    function clickparse() {
        df = new DateFormat();
        var s = df.setFormat(document.FORM.entry.value);
        t = df.parse(document.FORM.datetext.value);

        console.log("date : " + df.date);
        console.log("parse---ans : " + t);

    }
    //click ms->format
    function clickformat() {
        df = new DateFormat();
        var s = df.setFormat(document.FORM.entry.value);
        var str = df.format(document.FORM.mstext.value);
        //console.log("date : "+df.date);
        console.log("format---ans : " + str);
    }
    /*******test**

	var df =new DateFormat();
	var date = df.parse('2014-12-11 07:11:12 111'); //產生date
	var s = df.setFormat('Gyyyy-yy-MMMM-MMM-MM-M-w-W-D-dd-d EEEE_EEE_E a HH(H):mm(m):ss(s) S');
	var str = df.format("1234567890123");

	console.log("parse-ans : "+date);
	console.log("format-ans : "+str);

	****/
    /****test performance*****
	var st = new Date();
	var type = ["G", "yyyy", "yy", "MMMM", "MMM", "MM", "M", "w", "W", "D", "dd", "d", "F", "EEEE",
	"EEE", "E", "a", "HH", "H", "kk", "k", "K", "h", "mm", "m", "ss", "s", "S"];

	for(i=0; i < 100; i++){
		var s="yyyy";
		for(j = 0; j < Math.round(Math.random()*10); i++){
			s=s+type[Math.floor(Math.random()*24)]+"-";
		}

		var df =new DateFormat();
		var fff = df.setFormat(s);
		df.date = new Date();
//
		t = new Date();
		yr=t.getFullYear();
		mon=t.getMonth()+1;//0~11 => 1~12
		day=t.getDate();
		weekday=t.getDay();//0=Sun,1=Mon...
		hr=t.getHours();
		min=t.getMinutes();
		sec=t.getSeconds();
		ms=t.getMilliseconds();
		zone=t.getTimezoneOffset()/60;
		dayyr=0;
		for(i=0;i<mon;i++)
			dayyr=dayyr+monday[i];
		dayyr=dayyr+day;

		var str = df.format(Math.floor(Math.random()*1000000000000));//test format
		var tran = Translate(s);//test parse
		var mills = df.parse(str);

		//console.log(df.date);
		//console.log(fff);
		//console.log(str);
		//console.log(tran);
		//console.log(mills);

	}
	var end = new Date();
	console.log("run time "+(end-st)/1000);
	*************************/

})(jQuery);
