(function($) {
    var AD = {};
    AD.page = "ajax/group_am.html";
    var getDevices = function(groupInfo, action) {
        var data;
        //Al20150121
        switch (groupInfo.operatingSystem) {
            case "Windows":
                if (action == "fumo") {
                    var url = $.config.server_rest_url + '/firmwareFiles?search=operatingSystemName:Windows,deviceTypeId:'+groupInfo.deviceTypeId;
                } else if (action == "agent") {
                    var url = $.config.server_rest_url + '/omaAgentFiles?search=operatingSystemName:Windows,deviceTypeId:'+groupInfo.deviceTypeId;
                } else if (action == "policy") {
                    var url = $.config.server_rest_url + '/devicePolicys?search=operatingSystemName:Windows';
                } else {
                    var url = $.config.server_rest_url + '/windowsAppFiles?sortField=id';
                };
                break;
            case "Android":
                if (action == "fumo") {
                    var url = $.config.server_rest_url + '/firmwareFiles?search=operatingSystemName:Android,deviceTypeId:'+groupInfo.deviceTypeId;
                } else if (action == "agent") {
                    var url = $.config.server_rest_url + '/omaAgentFiles?search=operatingSystemName:Android,deviceTypeId:'+groupInfo.deviceTypeId;
                } else if (action == "policy") {
                    var url = $.config.server_rest_url + '/devicePolicys?search=operatingSystemName:Android';
                } else {
                    var url = $.config.server_rest_url + '/androidAppFiles?sortField=id';
                };
                break;
        }

        if (action == "fumo") {
            var actionTitle = "fumo";
        } else {
            var actionTitle = action;
        };

        url += "&pageNumber=1&pageSize=100";

        // HTTP GET ?–å?File?„ç¾?‰æ???
        $.ajax({
            type: 'GET',
            url: url,
            data: data,
            timeout: 20000,
            async: false,
            error: function(data, status, error) {
                console.log("[Group AM]:Got file list failed. => " + data.responseText);
            }
        }).done(function(data){
            var devices = new Array();
            var label;
            for (var i = 0; i < data.content.length; i ++){
                //Al20150121
                if (action == "fumo") {
                    (typeof(data.content[i].version) === "undefined" || data.content[i].version === null || data.content[i].version === "")
                    ? (label = data.content[i].name)
                    : (label = data.content[i].name + " (v" + data.content[i].version + ")");
                } else if (action == "agent") {
                    (typeof(data.content[i].version) === "undefined" || data.content[i].version === null || data.content[i].version === "")
                    ? (label = data.content[i].name)
                    : (label = data.content[i].name + " (v" + data.content[i].version + ")");
                } else if (action == "policy"){
                    (typeof(data.content[i].version) === "undefined" || data.content[i].version === null || data.content[i].version === "")
                    ? (label = data.content[i].name)
                    : (label = data.content[i].name + " (v" + data.content[i].version + ")");
                } else {
                    (typeof(data.content[i].version) === "undefined" || data.content[i].version === null || data.content[i].version === "")
                    ? (label = data.content[i].label)
                    : (label = data.content[i].label + " (v" + data.content[i].version + ")");
                };
                var obj = {
                    value: data.content[i].id,
                    label: label
                };
                devices.push(obj);
            }

            /*if (action === "policy")
            {
                if (groupInfo.operatingSystem != "Android")
                    devices = [];
            }
            else */
            if (action === "fumo")
            {
                var obj = {
                    value: 0,
                    label: "Update to latest version"
                };
                devices.unshift(obj);
            }
            else if (action == "wallpaper")
            {
                devices = [];
                if (groupInfo.operatingSystem === "Android")
                {
                    devices.push({value: "1", label: "Ocean"});
                    devices.push({value: "2", label: "Farm"});
                    devices.push({value: "3", label: "Forest"});
                    devices.push({value: "4", label: "Garden"});
                }
            }
            else if (action == "deviceControl")
            {
                devices = [];
                devices.push({value: "lock", label: "Lock"});
                devices.push({value: "unlock", label: "Unlock"});
                devices.push({value: "reset", label: "Reset"});
                devices.push({value: "restart", label: "Restart"});
            }

            if (devices.length > 0)
            {
                var deviceTypeOptions = "";
                console.log('[Policy] action=[' + action + ']');
                if (action === "policy")
                {
                    var asPolicyId = sessionStorage.getItem('sCsvPolicyIdString').split(',');
                    for (var i = 0; i < devices.length; i++)
                    {
                        // console.log('[Policy] A=[' + devices[i].value + ']');
                        if (_.contains(asPolicyId, devices[i].value.toString()))
                        {
                            deviceTypeOptions += '<option value="' + devices[i].value + '" selected>' + devices[i].label + '</option>';
                        }
                        else
                        {
                            deviceTypeOptions += '<option value="' + devices[i].value + '">' + devices[i].label + '</option>';
                        }
                    }
                    //$("#fileSelect").html('<select id="fileSelect" name="deviceType" class="form-control input-lg">'+ deviceTypeOptions +'</select>');
                    console.log('$("#fileSelect")=' + $("#fileSelect").length);
                    console.log('[Policy] sessionStorage.getItem(sCsvPolicyIdString)=[' + sessionStorage.getItem('sCsvPolicyIdString') + ']');
                    // $("#fileSelect").remove();
                    console.log('$("#fileSelect")=' + $("#fileSelect").length);
                    console.log('$("#divInputGroup")=' + $("#divInputGroup").length);
                    console.log('$("#divInputGroup")=' + $("#divInputGroup").find('div').length);
                    console.log('$("#divInputGroup > div")=' + $("#divInputGroup > div").length);
                    // $("#divInutGroup div").remove();
                    $("#divInputGroup span").after('<select id="fileSelect" name="deviceType" class="form-control input-lg" multiple size="5">' + deviceTypeOptions + '</select>');
                    //$("#divInputGroup span").after('<select id="fileSelect" name="deviceType" class="selectpicker form-control input-lg"  data-style="btn-primary" multiple data-max-options="2">' + deviceTypeOptions + '</select>');
                }
                else
                {
                    for (var i = 0; i < devices.length; i++)
                    {
                        deviceTypeOptions += '<option value="' + devices[i].value + '">' + devices[i].label + '</option>';
                    }
                    console.log('不是 policy 的情形');
                    // $("#fileSelect").html(deviceTypeOptions);

                    $("#divInputGroup span").after('<select id="fileSelect" name="deviceType" class="form-control input-lg">' + deviceTypeOptions + '</select>');

                }
                //$("#fileSelect").multiselect('dataprovider', devices);
            }
            else
            {
                $.smallBox(
                {
                    // title: "[Group " + actionTitle + " ] No file can be executed",
                    title: i18n.t("ns:Message.Group.BracketGroup") + i18n.t("ns:Group.Message." + actionTitle) + i18n.t("ns:Message.Group.BracketNoFileExecute"),
                    content: "<i class='fa fa-pencil-square-o'></i> <i>" + i18n.t("ns:Message.Group.GroupName") + " &nbsp;" + groupInfo.name + "</i>",
                    color: "#648BB2",
                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                    timeout: 5000
                });
                window.location.href = "#ajax/group_detail.html/" + groupInfo.id;
            };
        }).fail(function(){
            $.smallBox({
                // title: "[Group " + actionTitle + " ] Got file list failed",
                title: i18n.t("ns:Message.Group.BracketGroup") + i18n.t("ns:Group.Message."+ actionTitle ) + i18n.t("ns:Message.Group.BracketGotFileFailed"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>" + i18n.t("ns:Message.Group.GroupName")+"&nbsp;" + groupInfo.name + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            window.location.href = "#ajax/group_detail.html/" + groupInfo.id;
        });
    };

    var setGroupInfoField = function(groupInfo, action) {
        // Dynamic div
        var actionTitle = action.toUpperCase().charAt(0) + action.substring(1);

        var data = {
			id: groupInfo.id,
            groupName: groupInfo.name,
            // title: actionTitle
            title: i18n.t("ns:Group.Title."+actionTitle)
        };

        // Set title field
        var source = $("#dynamicTitleDiv").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $("#dynamicTitle").html(html);

        // Set header field
        var source = $("#dynamicHeaderDiv").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $("#dynamicHeader").html(html);

        // Set group name field
        $('#groupname').val(groupInfo.name);
        // Set group remark field
        $('#remark').val(groupInfo.remark);
    };

    var getGroupInfo = function(id, action) {
        var data;

        $("body").addClass("loading");
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/deviceGroups/' + id,
            data: data,
            dataType: 'json',
            timeout: 10000,
            async: false,
            error: function(data, status, error){
                console.log("[Group "+ action +"] Got group info failed. => " + data.responseText);
            }
        }).done(function(data){
            // Get device list of multiselect
            getDevices(data, action);
            // Set group info in the field
            setGroupInfoField(data, action);
            // Execute application management
            executeGroupAM(data, action);
        }).fail(function(){
            $.smallBox({
                title: i18n.t("ns:Message.Group.BracketGroup")+ i18n.t("ns:Group.Actions."+ action ) +i18n.t("ns:Message.Group.BracketGotGroupInfoFailed"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.GotGroupInfoFailed") + "</i>",
                // title: "[Group "+ action +"] Got group info failed",
                // content: "<i class='fa fa-pencil-square-o'></i> <i>Got group info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Group "+ action +"] Got group info failed.");
            window.location.href = "#ajax/group_detail.html/" + id;
        }).always(function(){
            $("body").removeClass("loading");
        });
    };

    var executeGroupAM = function(groupInfo, action){
        // Back button event
        $("#btn_back").click(function(event){
            event.preventDefault();
            window.location.href = "#ajax/group_detail.html/" + groupInfo.id;
        });

        // Save button event
        $("#btn_save").click(function(event){
            event.preventDefault();

            var actionUpperCase = action.toUpperCase().charAt(0) + action.substring(1);
            // 2015/01/26 Kenny, add Schedule
			/*
			var originalData = {
                remark: groupInfo.remark || "",
                deviceIdSet: $('#fileSelect').val() || []
            };
			*/
			var isSchedule = $('#scheduleTask').prop('checked');
			var scheduleDate = null;
			var scheduleTime = null;
			if (isSchedule) {
				scheduleDate = $('#scheduleDate').val() || null;
				scheduleTime = $('#scheduleTime').val() || null;
			}
			var originalData = {
					scheduleDate: scheduleDate,
					scheduleTime: scheduleTime
				};
            var data = JSON.stringify(originalData);

            // ?®å??ªå?è¨±å?è£ä?ç­†æ?æ¡?
            if ($('#fileSelect').val() === null || typeof(groupInfo.id) === 'undefined') {
                $.smallBox({

                    title: i18n.t("ns:Message.Group.ChooseFile"),
                    content: "<i class='fa fa-pencil-square-o'></i> <i>" + i18n.t("ns:Message.Group.GroupAM")+i18n.t("ns:Message.Group.GroupName")+"&nbsp;" + groupInfo.name + "</i>",
                    // title: "Please choose a file.",
                    // content: "<i class='fa fa-pencil-square-o'></i> <i>[ Group AM ] Group name: &nbsp;" + groupInfo.name + "</i>",
                    color: "#B36464",
                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                    timeout: 5000
                });
            } else {
                //Al20150121
                var url = "";
                switch (groupInfo.operatingSystem) {
                    case "Windows":
                        if (action == "fumo") {
                            if ($('#fileSelect').val() == 0) //system update
                            {
                                url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/deltaFirmwares/0/actions/upgrade';
                            }
                            else
                            {
                                url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/firmwareFiles/' + $('#fileSelect').val() + "/actions/upgrade";
                            }
                        } else if (action == "agent") {
                            url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/omaAgentFiles/' + $('#fileSelect').val() + "/actions/upgrade";
                        } else if (action == "policy") {
                            url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/devicePolicys/' + $('#fileSelect').val() + "/actions/apply";
                        } else {
                            url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/windowsApps/' + $('#fileSelect').val() + "/actions/" + action;
                        };
                        break;
                    case "Android":
                        if (action == "fumo") {
                            if ($('#fileSelect').val() == 0) //system update
                            {
                                url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/deltaFirmwares/0/actions/upgrade';
                            }
                            else
                            {
                                url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/firmwareFiles/' + $('#fileSelect').val() + "/actions/upgrade";
                            }
                        } else if (action == "agent") {
                            url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/omaAgentFiles/' + $('#fileSelect').val() + "/actions/upgrade";
                        } else if (action == "policy") {
                            console.log('[Policy] $(#fileSelect).val()=[' + $('#fileSelect').val() + ']');
                            url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/devicePolicys/' + $('#fileSelect').val() + "/actions/applyMultiple";
                        } else if (action == "wallpaper") {
                            url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/deviceWallpaper/' + $('#fileSelect').val() + "/actions/apply";
                        } else {
                            url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id +
                                  '/androidApps/' + $('#fileSelect').val() + "/actions/" + action;
                        };
                        break;
                }

                if (action == "deviceControl")
                {
                    url = $.config.server_rest_url + '/deviceGroups/' + groupInfo.id + '/actions/' + $('#fileSelect').val();
                }

                $("body").addClass("loading");

                $.ajax({
                    type: 'POST',
                    crossDomain: true,
                    url: url,
                    contentType: 'application/json',
                    dataType: 'json',
                    data: data,
                    timeout: 10000,
                    error: function(jqXHR, textStatus, errorThrown){
                        console.log("[Group Add]:POST failed");
                    }
                }).done(function(data){
                    if (data.status) {
                        //Al20150121
                        switch (action) {
                            case "install":     var icon = "fa-download";   break;
                            case "remove":      var icon = "fa-trash";      break;
                            case "upgrade":     var icon = "fa-plus";       break;
                            case "active":      var icon = "fa-play";       break;
                            case "inactive":    var icon = "fa-pause";      break;
                            case "fumo":        var icon = "fa-cog";        break;
                            case "agent":       var icon = "fa-floppy-o";   break;
                            default:            var icon = "";              break;
                        }
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.BracketGroup") + i18n.t("ns:Group.Actions."+ action ) + i18n.t("ns:Message.Group.BracketSentToServerSuccess"),
                            content: "<i class='fa " + icon + "'></i> <i>"+ i18n.t("ns:Message.Group.GroupName")+ " &nbsp;" + groupInfo.name + "</i>",
                            // title: "[Group " + action + "] Sent to server successfully",
                            // content: "<i class='fa " + icon + "'></i> <i> Group name: &nbsp;" + groupInfo.name + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    } else {
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.BracketGroup") + i18n.t("ns:Group.Actions."+ action ) + i18n.t("ns:Message.Group.BracketSentToServerFailed"),
                            content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.GroupName")+ " &nbsp;" + groupInfo.name + "</i>",
                            // title: "[Group " + action + "] Sent to server failed",
                            // content: "<i class='fa fa-pencil-square-o'></i> <i> Group name: &nbsp;" + groupInfo.name + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }
                }).fail(function(){
                    $.smallBox({
                        title: i18n.t("ns:Message.Group.BracketGroup") + i18n.t("ns:Group.Actions."+ action ) + i18n.t("ns:Message.Group.BracketSentToServerFailed"),
                        content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.GroupName")+ "&nbsp;" + groupInfo.name + "</i>",
                        // title: "[Group " + action + "] Sent to server failed",
                        // content: "<i class='fa fa-pencil-square-o'></i> <i> Group name: &nbsp;" + groupInfo.name + "</i>",
                        color: "#B36464",
                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                        timeout: 5000
                    });
                }).always(function(){
                    $("body").removeClass("loading");
                    window.location.href = "#ajax/group_detail.html/" + groupInfo.id;
                });
            };
        });
    };

    function padLeft(str, len) {
        str = '' + str;
        if (str.length >= len) {
            return str;
        } else {
            return padLeft("0" + str, len);
        }
    }

    AD.bootstrap = function() {

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        var hash = window.location.hash;
        var urlSplit = hash.replace(/^#ajax\/group_am.html\/(\w+)/, "$1").split('/');

        if (!(typeof urlSplit === 'undefined')) {
            var id = urlSplit[0];
            var action = urlSplit[1];
        }

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#ajax/group_detail.html/" + id, {
                trigger: true
            });
        } else {
            // Get groupInfo
            getGroupInfo(id, action);
        }

        switch (action) {
            case "install":    $("#taskContent").html('<h3>Application</h3>'); break;
            case "remove":     $("#taskContent").html('<h3>Application</h3>'); break;
            case "activate":   $("#taskContent").html('<h3>Application</h3>'); break;
            case "deactivate": $("#taskContent").html('<h3>Application</h3>'); break;
            case "policy":     $("#taskContent").html('<h3>Policy</h3>');      break;
            case "fumo":
                $("#taskContent").html('<h3>OS Image</h3>');
                $("#scheduleForm").hide();
                break;
            case "agent":      $("#taskContent").html('<h3>Agent File</h3>');  break;
            case "wallpaper":  $("#taskContent").html('<h3>Picture</h3>');     break;
            case "deviceControl":     $("#taskContent").html('<h3>Operation</h3>');      break;
        }

        var currentdate = new Date();
        /*var time = "";
        if (currentdate.getHours() > 12)
            time += currentdate.getHours()%12;
        else
            time += currentdate.getHours();
        time += ":";
        time += (Math.ceil((currentdate.getMinutes() + 15)/5))*5 + " ";
        if (currentdate.getHours() > 12)
            time += "PM";
        else
            time += "AM";*/

        $('#scheduleDate').val(padLeft(currentdate.getDate(),2) + "/" + padLeft(currentdate.getMonth()+1,2) + "/" + (currentdate.getYear()+1900));
		$('#scheduleTime').timepicker({
            minuteStep: 5,
            //defaultTime: time
        });
		$('#scheduleTask').on("click",function(){
			var isSchedule = $(this).prop('checked');
			if(isSchedule)
            {
				$('#scheduleDate').attr('disabled', false);
                $('#scheduleTime').attr('disabled', false);
			} else {
                $('#scheduleDate').attr('disabled', true);
                $('#scheduleTime').attr('disabled', true);
			}
		});

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");
    };
    /* define bootstrap ends */
    $[AD.page] = AD;

})(jQuery);
