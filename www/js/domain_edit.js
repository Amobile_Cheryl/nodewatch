(function($) {

    var AD = {};
    var url = $.config.server_rest_url+'/domains';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "Roles";
    AD.page = "ajax/domain_edit.html";
   
    function s1(id) {
        // console.log('domain_edit.js');
        $.ajax({
            type: "GET",
            url: url+'/'+id,
            dataType: 'json',
            success: function(data) {
                // console.log(data);
                $('input[name="domainname"]').val(data.name);
                $('input[name="description"]').val(data.remark);
                // getData=data;
            },
            async: false
        });


        // console.log('Hello');
        // console.log(role);
        // Main Detail
        // var source = $("#tpl_permission").html();
        // console.log(source);
        // var template = Handlebars.compile(source);
        // var html = template(role);
        // console.log(html);
        // $("#permission_render").html(html);
    };




    AD.bootstrap = function() {
        // var p = $("#left-panel nav a[href='ajax/device_list.html']").parents("li");
        // $(p[0]).addClass("active");
        // $(p[1]).find("ul").show();
        // $(p[1]).addClass("active open");
        //alert(href);

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Translate Input of the placeholder
        translatePlaceholder();

        var hash = window.location.hash;
        //alert(hash);
        var id = hash.replace(/^#ajax\/domain_edit.html\/(\w+)$/, "$1");
        // console.log(id);
		$("body").removeClass("loading");
        // Initial page
        s1(id);

    };

    $[AD.page] = AD;

    function translatePlaceholder () {
        $("#domainname").attr("placeholder", i18n.t("ns:Domain.Message.DomainName"));
        $("#description").attr("placeholder", i18n.t("ns:Domain.Message.Description"));
    }

})(jQuery);