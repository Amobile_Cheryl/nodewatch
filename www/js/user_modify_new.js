(function($) {
    var AD = {};
    var referer;
    var id;
    var roleList;
    var domainList;
    AD.page = "ajax/user_modify.html";

    var uploadImage = function() 
    {
        // Image
        var result = $('#result');
        var currentFile;
        var replaceResults = function(img) {
            $("#result img").attr('src', img.toDataURL());
            /*var content;
            if (!(img.src || img instanceof HTMLCanvasElement)) {
                content = $('<span>Loading image file failed</span>');
            } else {        
                content = $('<a target="_blank">').append(img)
                    .attr('download', currentFile.name)
                    .attr('href', img.src || img.toDataURL());
            }
            result.children().replaceWith(content);
            $("#appearance").val(content[0].href);*/
        };
        var displayImage = function(file, options) {
            currentFile = file;
            if (!loadImage(
                    file,
                    replaceResults,
                    options
                )) {
                result.children().replaceWith(
                    $('<span>Your browser does not support the URL or FileReader API.</span>')
                );
            }
        };
        var dropChangeHandler = function(e) {
            e.preventDefault();
            e = e.originalEvent;
            var target = e.dataTransfer || e.target,
                file = target && target.files && target.files[0],
                options = {
                    maxHeight: 50,
                    canvas: true
                };
            if (!file) {
                return;
            }
            loadImage.parseMetaData(file, function(data) {
                displayImage(file, options);
            });
        };
        $("#appearanceInput").on("change", dropChangeHandler);
    }
        // console.log($("#title").children('font'));
    var prepareForm = function() {
        if (referer == "add")
            $('#result').children().replaceWith('<img src="data:image/png;base64,' + $.common.defaultPortrait + '" style="max-height: 50px;"/>');
        else if (referer == "edit")
        {
            $('input[name="email"]').attr('disabled', true);
            $('#domainName').attr('disabled', true);
            $('input[name="password"]').val("*********");
            $('input[name="repeatPassword"]').val("*********");
        }

        // Edited user's permission is less than DomainOwner, then it only can set DomainUser, DomainOwner
        if (permission.value <= $.common.PermissionClass.DomainOwner.value){
            $.each($.common.PermissionClass,function(i,item){
                if (item.value <= $.common.PermissionClass.DomainOwner.value)
                    $("#domainPermission").append($("<option></option>").attr("value", item.name).text(item.display));
            });                    
        }
        // Edited user's permission is greater than SystemOwner
        else   
        {
            $.each($.common.PermissionClass,function(i,item){
                if (item.value == $.common.PermissionClass.SystemOwner.value)
                    $("#domainPermission").append($("<option></option>").attr("value", item.name).text(item.display));
            });                     
        }

        if ($.login.user.permissionClass.value >= $.common.PermissionClass.SystemOwner.value){
            $.ajax({
                type: "GET",
                async: false,
                url: $.config.server_rest_url + '/domains',
                success: function(data) {
                    domainList = data.content;
                    $.each(data.content, function(i, item) {
                        if (typeof item.id != 'undefined')
                            $("#domainName").append($("<option></option>").attr("value", item.id).text(item.name));
                    })
                    // hide System domain
                    $('option[value="1"]').hide();                         
                },
                error: function(data) {
                    console.log('Get Error');
                    // alert('Get Role List Failed!!');
                    return;
                },
            });            
        }

        if (referer == "edit")
        {
            $.ajax({
                type: "GET",
                url: $.config.server_rest_url + '/users/' + id,
                dataType: 'json',
                success: function(data) {
                    console.log(data);
                    // for tab1 to display user informaiton
                    $("#userName").text(data.userName + " > ");
                    $('input[name="username"]').val(data.userName);
                    $('input[name="description"]').val(data.remark);
                    $('input[name="email"]').val(data.email);
                    $('input[name="mobilePhone"]').val(data.mobilePhone);
                    $("#domainName").val(data.domainId);
                    // $('#result').children('a').attr('href',"data:image/png;base64,"+data.portrait);
                    var portrait = data.portrait || $.common.defaultPortrait;
                    $('#result').children().replaceWith('<img src="data:image/png;base64,' + portrait + '"/>');
                    $('input').removeAttr('placeholder');
                    if (data.email == "") {
                        $("#email").attr("placeholder", i18n.t("ns:User.Message.EmailExample"));
                    }
                    $("#result img").attr('src', data.portraitUrl);
                },
                async: false
            });
        }      
    }

    var reloadJs = function() {
    }

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        var hash = window.location.hash;
        var urlPathArray = hash.split("\/");
        referer = ((typeof urlPathArray[2] != "undefined") ? urlPathArray[2] : "") || "";
        id = ((typeof urlPathArray[3] != "undefined") ? urlPathArray[3] : "") || "";
                
		$("body").removeClass("loading");
        var url = $.config.server_rest_url + '/roles';
        url += "?pageNumber=1&pageSize=100";

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Translate Input of the placeholder
        translatePlaceholder();
		
        $("#domainPermission").change(function() {
            if (referer == 'add')
            {
                // SuperUserRole
                if ($("#domainPermission").val() == $.common.PermissionClass.SuperUserRole.name) {
                    $("#domainName").val(1);
                    $("#domainName").attr('disabled', 'disabled');
                }            
                // SystemOwner
                else if ($("#domainPermission").val() == $.common.PermissionClass.SystemOwner.name) {
                    $("#domainName").val(1);
                    $("#domainName").attr('disabled', 'disabled');
                } 
                // DomainOwner
                else if ($("#domainPermission").val() == $.common.PermissionClass.DomainOwner.name) {
                    $("#domainName").val("");
                    $("#domainName").removeAttr('disabled');
                } 
                // DomainUser
                else if ($("#domainPermission").val() == $.common.PermissionClass.DomainUser.name) {
                    $("#domainName").val("");
                    $("#domainName").removeAttr('disabled');
                } 
                else {
                    $("#domainName").val("");
                    $("#domainName").attr('disabled', 'disabled');
                }
            }
        });

        $.ajax({
            type: "GET",
            async: false,
            url: url,
            success: function(data) {
                roleList = data.content;
            },
            error: function(data) {
                console.log('Get Error');
                // alert('Get Role List Failed!!');
                return;
            },
        });
        
        $("#domainName").attr('disabled', 'disabled');
        uploadImage();
        prepareForm();

        var $validator = $("#wizard-1").validate({
            rules: {
                username: {
                    required: true,
                    //account: true
                },
                password: {
                    required: true,
                    minlength: 6,
                    maxlength: 20,
                    password: true           
                },
                repeatPassword: {
                    required: true,
                    equalTo: '#password'
                },
                email:{
                    required: true,
                    email: true
                },
                mobilePhone:{
                    required: true,
                    //tel: true
                },
                domainPermission: {
                    required: true
                },
                domainName: {
                    required: true
                }
            },

            messages: {
                username: i18n.t("ns:Message.ThisFieldRequire"),
                password : {
                    required : i18n.t("ns:Message.ThisFieldRequire"),
                    minlength : "The password length should be equal to or greater than 6"
                },
                repeatPassword: {
                    required : i18n.t("ns:Message.ThisFieldRequire"),
                    equalTo : 'It should be the same to password'
                },
                domainPermission: i18n.t("ns:Message.ThisFieldRequire"),
                domainName: i18n.t("ns:Message.ThisFieldRequire"),
                mobilePhone: i18n.t("ns:Message.ThisFieldRequire"),        
                email : {
                    required : i18n.t("ns:Message.ThisFieldRequire"),
                    email : i18n.t("ns:Message.EnterValidEmail")
                }
            },

            errorPlacement : function(error, element) {
                error.insertAfter(element.parent());
            }
        });
    };

    $("#btn_save").on('click', function(e) {
        e.preventDefault();
        if (!$("#wizard-1").valid())
            return false;

        var comparedRoleIdSet = [];

        // Get server's role list
        var roleNameList = [];
        $.each(roleList, function(index, value) {
            if (value.name)
                roleNameList.push(value.name);
        });
        console.log('roleNameList is ');
        console.log(roleNameList);

        // Collect user's selection
        var selectedRoleList = $('#permissionTree :input:checked');
        var selectedRoleNameList = [];
        $.each(selectedRoleList, function(index, value) {
            selectedRoleNameList.push(value.name);
        });
        selectedRoleNameList.push($("#domainPermission").val());
        console.log('selectedRoleNameList is ');
        console.log(selectedRoleNameList);
        // console.log(selectedRoleList);
        var comparedRoleNameSet = [];
        $.each(selectedRoleNameList, function(index, value) {
            if ($.inArray(value, roleNameList) > -1)
                comparedRoleNameSet.push(value);
        });
        console.log('comparedRoleNameSet is ');
        console.log(comparedRoleNameSet);

        $.each(comparedRoleNameSet, function(cindex, cvalue) {
            $.each(roleList, function(dindex, dvalue) {
                if (dvalue.name == cvalue) {
                    comparedRoleIdSet.push(dvalue.id);
                }
            })
        });

        var data = {
            userName: $('input[name="username"]').val(),
            //password: $('input[name="password"]').val(),
            mobilePhone: $('input[name="mobilePhone"]').val(),
            remark: $('input[name="description"]').val(),
            roleIdSet: comparedRoleIdSet
        };
        if ($("#result img").attr('src').indexOf("data:image/png;base64") >= 0) //new uploaded image
            data.portrait = $("#result img").attr('src');

        if ($('input[name="password"]').val() != "*********")
            data.password = $('input[name="password"]').val();

        if (referer == "add")
        {
            data.email = $('input[name="email"]').val();
            data.domainId = $("#domainName").val();
        }

        if (referer == "add")
            url = $.config.server_rest_url + '/users';
        else if (referer == "edit")
            url = $.config.server_rest_url + '/users/' + id;

        $.ajax({
            type: "POST",
            async: false,
            url: url,
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify(data),
            success: function(data) {
                $.smallBox({
                    title: i18n.t("ns:Message.User.UserAdd") + i18n.t("ns:Message.User.AddSuccess"),
                    content: "<i class='fa fa-plus'></i> <i>"+i18n.t("ns:Message.User.AddSuccess")+"</i>",
                    color: "#648BB2",
                    iconSmall: "fa fa-plus fa-2x fadeInRight animated",
                    timeout: 5000
                });                                    
                console.log('Add Success');
                window.location.href = '#ajax/user_list.html';
                // console.log(data);
            },
            error: function(data) {
                $.smallBox({
                    title: i18n.t("ns:Message.User.UserAdd") + i18n.t("ns:Message.User.AddFailed"),
                    content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Message.User.AddFailed") + "</i>",
                    color: "#B36464",
                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                    timeout: 5000
                });                                
            },
            // type:"json",
            dataType: "json"
        });  
    });

    /* define bootstrap ends */
    $[AD.page] = AD;

    function translatePlaceholder () {
        $("#userName").attr("placeholder", i18n.t("ns:User.Message.UserName"));
        $("#password").attr("placeholder", i18n.t("ns:User.Message.Password"));
        $("#repeatPassword").attr("placeholder", i18n.t("ns:User.Message.RepeatPassword"));
        $("#email").attr("placeholder", i18n.t("ns:User.Message.EmailExample"));
        $("#appearanceInput").attr("placeholder", i18n.t("ns:User.Message.Portrait"));
    }

})(jQuery);
