(function($) {
    /* run bootstrap */

    // Move variable AD and AD.page outside of function
    var AD = {};
    AD.page = "ajax/device_list.html";
    var selectedDevices;
    var adReload = function() 
    {
        selectedDevices = [];

        AD.collections = {};
        AD.buttons = {};
        AD.columns = [
            {
                title: '<div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-devices"><i></i>&nbsp;&nbsp;&nbsp;&nbsp;</label></div>',
                sortable: false,
                callback: function(o) {
                    if ($.inArray(o.id.toString(), selectedDevices) >= 0)
                        return '<div><div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-device" data-device-id="'+o.id+'" checked><i></i></label></div></div>';
                    else
                        return '<div><div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-device" data-device-id="'+o.id+'"><i></i></label></div></div>';
                }
            },
            {
                title: 'Label',
                property: 'label',
                cellClassName: 'label',
                filterable: false,
                sortable: true
            },
            {
                title: 'ns:Device.Title.ID',
                property: 'name',
                filterable: true,
				searchName: 'name',
                sortable: true,
                callback: function(o) {
                    var html = '';
                    if (o.provisionStatus === "Provisioned") {
                        html += '<a href="#ajax/device_detail.html/' + o.id + '" title="Check the device detail">' + o.name + '</a>';
                    } else {
                        html += o.name;
                    }
                    return html;
                }
            },
            //column3
            {
                title: 'Image',
                sortable: false,
                callback: function(o) {
                    return '<img src="data:image/png;base64,'+o.deviceTypeImage+'"" style="height:30px; margin-top:-6px; margin-bottom:-6px;" />';
                }
            },
            {
                title: 'ns:Device.Title.DeviceType',
                cellClassName: 'deviceTypeName',
                filterable: true,
                searchName: 'deviceTypeName',
				searchDom : function(){
					var options = '<option value selected="selected">'+i18n.t("ns:Device.Title.DeviceType")+'</option>';
					$.ajax({
						url: $.config.server_rest_url+'/deviceTypes',
						dataType: 'JSON',
						async: false,
						success: function(deviceTypeCollection){
							for(var i=0;i<deviceTypeCollection.length-1;i++){
								options+='<option>'+deviceTypeCollection[i].name+'</option>';
							}
						}
					});
					return '<select class="form-control" name="deviceType" placeholder="'+i18n.t("ns:Device.Title.DeviceType")+'">'+options+'</select>';
				},
				sortable: false,
                callback: function(o) {
                    return o.deviceTypeName;
                }
            },
            //Column4
            {
                title: 'OS',
                //property: 'operatingSystemName',
                filterable: false,
				searchName: 'operatingSystemName',
				// searchDom : function(){
				// 	var options = '<option value selected="selected">'+i18n.t("ns:DataTables.Search")+' '+i18n.t("ns:Device.Title.OperatingSystem")+'</option>'
				// 		+'<option>Android</option>'
				// 		+'<option>Windows</option>';
				// 	return '<select class="form-control" name="deviceType" placeholder="'+i18n.t("ns:DataTables.Search")+' '+i18n.t("ns:Device.Title.OperatingSystem")+'">'+options+'</select>';
				// },
                sortable: false,
				callback: function(o) {
					var os = (typeof(o.operatingSystemName) === "undefined") ? '' : o.operatingSystemName;
					var displayHtml = '';
					if(os==="Android"){
						displayHtml = '<i class="fa fa-android"></i>'// Android';
					}else if(os==="iOS"){
						displayHtml = '<i class="fa fa-apple"></i>'// iOS';
					}else if(os==="Windows"){
						displayHtml = '<i class="fa fa-windows"></i>'// Windows';
					}else{
						displayHtml = '<i class="fa fa-question"></i>'// Unknown Operating System';
					}
					return displayHtml;
				}
            },
            //Column5
            /*
            {
                title: 'Firmware Language',
                property: 'firmwareLanguageId',
                filterable: false,
                sortable: false,
                callback: function(o) {
                    if ($.config.firmware_lang[o.firmwareLanguageId] == "zh_TW")
                        return "<img src='img/flags/tw.png'>";
                    return $.config.firmware_lang[o.firmwareLanguageId];
                }
            },
            */
            //column6
            {
                title: 'ns:Device.Title.ConnectionStatus',
                property: 'latestConnectTime',
                cellClassName: 'latestConnectTime',
                filterable: false,
                sortable: true,
				callback: function(o) {
					/*change millisecond to date format*/
					var isOnlineStatus = "--";
					if (!_.isEmpty(o.latestConnectTime)) {
						isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:gray;\" title=\""+i18n.t("ns:Message.Device.Offline")+"\"></i>";
						if (!_.isUndefined(o.isOnline)) {
							if(o.isOnline){
								isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:lawngreen;\" title=\""+i18n.t("ns:Message.Device.Online")+"\"></i>";
							}
						}
					}
					return isOnlineStatus+o.latestConnectTime;
				},
            },
            {
                title: 'ns:Device.Title.Domain',
                property: 'domainId',
                filterable: false,
                sortable: false,
                visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
                callback: function(o) {
                    //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId] ;
					return (typeof(o.domainName) === "undefined") ? '' : o.domainName;
                }
            },
            /*{
                title: 'ns:Device.Title.Action',
				operator: true,
				sortable: false,
                callback: function(o) {
                    var _html = '';
                    //_html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                    _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                    // _html += '<a href="#" class="btn btn-mini btn-warning removeOne" title="Unlock" data-id="' + o[idAttribute] + '"><i class="icon-unlock"></i> </a>';
                    return _html;
                }
            }*/
            {
                title: 'ns:Device.Title.Status',
                operator: true,
                sortable: false,
                callback: function(o) {
                    var isLock = '';
                    if (o.hasOwnProperty('isLock') == true && o.isLock == true)
                        isLock = "<i class=\"fa fa-lock txt-color-red\" title=\""+i18n.t("ns:Device.Message.Lock")+"\"></i>";
                    else
                        isLock = "<i class=\"fa fa-unlock txt-color-blueLight\" title=\""+i18n.t("ns:Device.Message.Unlock")+"\"></i>";
                    return isLock;
                }
            }
        ];
        /* define namespace ends */
        var url;
        var hash = window.location.hash;
		var urlPathArray = hash.split("\/");
        var referer = ((typeof urlPathArray[2] != "undefined") ? urlPathArray[2] : "" ) || "";

        if (!$.login.user.checkRole('DeviceOperator'))			
        {
            AD.columns = $.grep(AD.columns, function(e) {
                return (!_.has(e, "operator"));
            }); 
        }               
        url = $.config.server_rest_url + '/devices';
        AD.isAdd = false;
        AD.buttons = [
            {
                "sExtends": "text",
                "sButtonText" : "<i class='fa fa-trash-o'></i>&nbsp; Delete Devices",
                "sButtonClass": "btn-deleteDevices bg-color-redLight txt-color-white"
            },
            {
                "sExtends": "text",
                "sButtonText" : "<i class='fa fa-plus'></i>&nbsp; Import Devices",
                "sButtonClass": "btn-addDevices txt-color-white bg-color-blue"
            },
        ];

        var idAttribute = AD.idAttribute = "id";
        AD.title = "ns:Menu.DeviceList";

        AD.edit = {};
        AD.edit.forms = [ //forms
            {
                'class': 'col col-6 input state-disabled',
                disabled: 'disabled',
                title: 'Serial Number (Unalterable)',
                property: 'cwmpId',
                default_value: ""
            },
            // "row",
            //form
            {
                // 'class': 'col col-2',
                title: 'Device Type',
                property: 'deviceTypeId',
                default_value: ""
            },
            // "fieldset",
            //form
            {
                // 'class': 'col col-2',
                title: 'Firmware Language',
                property: 'firmwareLanguageId',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Domain',
                property: 'domainId',
                default_value: ""
            },
            // "row",
            //form
            {
                // 'class': 'col col-2',
                title: 'Country',
                property: 'country',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Clinic Name',
                property: 'clinicName',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Clinic Location',
                property: 'clinicLocation',
                default_value: ""
            }
        ];


        AD.forms = [
            //forms
            {
                // 'class': 'col col-2 ',
                title: 'Serial Number',
                property: 'cwmpId',
                default_value: ""
            },
            // "row",
            //form
            {
                // 'class': 'col col-2',
                title: 'Device Type',
                property: 'deviceTypeId',
                default_value: ""
            },
            // "fieldset",
            //form
            {
                // 'class': 'col col-2',
                title: 'Firmware Language',
                property: 'firmwareLanguageId',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Domain',
                property: 'domainId',
                default_value: ""
            },
            // "row",
            //form
            {
                // 'class': 'col col-2',
                title: 'Country',
                property: 'country',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Clinic Name',
                property: 'clinicName',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Clinic Location',
                property: 'clinicLocation',
                default_value: ""
            }
        ];

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
        /* collection ends */

    };


    var routes = {};
    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/summary/:_id'] = 'show_summary';
    routes[AD.page + '/edit/:_id'] = 'show_edit';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        initialize: function(opt) {
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            // console.error();
        },
        show_edit: function(id) {  
            var t = this;            
            t.navigate("#ajax/device_edit.html/" + id, {
                trigger: true
            });
        },
        show_grid: function() {
            var t = this;
            var urlPathArray = window.location.hash.split("\/");
            var referer = ((typeof urlPathArray[2] != "undefined") ? urlPathArray[2] : "" ) || "";
            // init GridView
            if (!t.gv) {
                t.gv = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: !_.isUndefined(t.AD.isAdd) ? t.AD.isAdd : true,
                    sort: t.AD.sort,
					order: [[1, "desc"]], //t.AD.order,
                    filter: {}, //t.AD.filter,
                    buttons: t.AD.buttons,
                    AD: t.AD
                });

                //新增單筆device
                /*t.$gv.on("click", ".upload-btn", function(event) {
                    event.preventDefault();

                    //alert("upload-btn");

                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    // alert(t.AD.addurl);
                    // var addurl = t.AD.addurl || "add";
                    t.navigate("#" + t.AD.page + "/" + "add", {
                        trigger: true
                    });

                });*/
                //新增多筆device
                t.$gv.on("click", ".btn-addDevices", function(event) {
                    event.preventDefault();
                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    // alert(t.AD.addurl);
                    // var addurl = t.AD.addurl || "add";
                    //這邊要做csv上傳的連結
                    //t.navigate("#" + t.AD.page + "/" + "addCsv", {
					t.navigate("#ajax/device_import.html", {
                        trigger: true
                    });
                });
                t.$gv.on("click", ".checkbox-device", function(event) {
                    if ($(this).prop("checked"))
                        selectedDevices.push($(this).attr('data-device-id')); 
                    else
                        selectedDevices.splice(selectedDevices.indexOf($(this).attr('data-device-id')), 1); 

                    /*if ($(".checkbox-device:checked").length < $(".checkbox-device").length)
                        $(".checkbox-devices").prop("checked", false);
                    else
                        $(".checkbox-devices").prop("checked", true);*/

                    if (selectedDevices.length > 0)
                        $(".btn-deleteDevices").fadeIn("fast");
                    else
                        $(".btn-deleteDevices").fadeOut("fast");
                    console.log(selectedDevices);
                });

                t.$gv.on("click", ".checkbox-devices", function(event) {
                    if ($(this).prop("checked"))
                    {
                        $.each($(".checkbox-device"), function() {
                            $(this).prop("checked", true);
                            if (selectedDevices.indexOf($(this).attr('data-device-id')) < 0)
                                selectedDevices.push($(this).attr('data-device-id')); 
                        }); 
                    }
                    else
                    {
                        $.each($(".checkbox-device"), function() {
                            $(this).prop("checked", false);
                            selectedDevices.splice(selectedDevices.indexOf($(this).attr('data-device-id')), 1); 
                        });                       
                    }
                    if (selectedDevices.length > 0)
                        $(".btn-deleteDevices").fadeIn("fast");
                    else
                        $(".btn-deleteDevices").fadeOut("fast");
                    console.log(selectedDevices);
                });

                var deleteDevices = function(deviceId)
                {
                    var dmURL = $.config.server_rest_url + '/devices/';
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + deviceId,
                        timeout: 10000
                    })
                    .done(function()
                    {
                        selectedDevices.shift();
                        if (selectedDevices.length == 0)
                        {
                            $.smallBox({
                                title: i18n.t("ns:Message.Device.DeviceDelete") + i18n.t("ns:Message.Device.DeleteSuccess"),
                                content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Device.DeleteDevice") + "</i>",
                                color: "#648BB2",
                                iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                            window.location.href = "#ajax/device_list.html";
                        }
                        else
                            deleteDevices(selectedDevices[0]);
                    })
                    .fail(function()
                    {
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.DeviceDelete") + i18n.t("ns:Message.Device.DeleteFailed"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Device.DeleteDevice")+": &nbsp;" + deviceId + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    })
                }

                t.$gv.on("click", ".btn-deleteDevices", function(event) {
                    $.SmartMessageBox({
                        title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+ i18n.t("ns:Message.Device.DeleteTheDevice") +" <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                        content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> "+ i18n.t("ns:Message.Device.DeleteDeviceContent") +"</span>",
                        buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'
                    }, function(ButtonPressed) {
                        if (ButtonPressed == i18n.t("ns:Message.Yes"))                 
                            deleteDevices(selectedDevices[0]);
                        window.location.href = "#ajax/device_list.html";
                    });
                });
            } else
                t.gv.refresh();

            $(".btn-deleteDevices").css('margin-right','5px');
            $(".btn-deleteDevices").hide();
            t.$fv.hide();
            t.$gv.show();

            pageSetUp();

            // setTimeout(runDataTables, 500);
        }       
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        if (!$.login.user.checkRole('DeviceViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        }

        adReload();

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    // $[AD.page].bootstrap();

})(jQuery);
