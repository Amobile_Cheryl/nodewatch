(function($) {
    var deviceTypesWindows = [];
    var deviceTypesAndroid = [];
    var deviceGroups = [];
    var AD = {};
    AD.page = "ajax/device_import.html";
    var deviceTypes = {};
    
    // Add adReload function to run this script when user click this .html every time.
    function adReload() 
    {
        var csvButton = $('<button/>')
                        .addClass('btn btn-primary pull-right')
                        .text(i18n.t("ns:Device.Import"))
                        .on('click', function(event) {
                            event.preventDefault();

                            if ($("#groupName").val().length == 0)
                            {
                                $.smallBox({
                                    title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddFailed"),
                                    content: "<i class='fa fa-plus'></i> <i>Group can't be empty.</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                return;
                            }

                            if ($("#deviceType").val() == null)
                            {
                                $.smallBox({
                                    title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddFailed"),
                                    content: "<i class='fa fa-plus'></i> <i>Device type can't be empty.</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                return;
                            }

                            var fd = new FormData();
                            fd.append("deviceTypeName", $("#deviceType").val());
                            if ($("#groupName").val() == null)
                                fd.append("deviceGroupName", "Default");
                            else
                                fd.append("deviceGroupName", $("#groupName").val());
                            fd.append("isCheckFile", "false");
                            $("#fileupload").fileupload({formData: fd});
                            /*$("#uploadResultDiv").css("display","none")
                                                 .removeClass()
                                                 .html("");*/
                            var $this = $(this),
                                //取值, 即檔案
                                data = $this.data();

                            $this
                                .off('click')
                                .text(i18n.t("ns:Device.Abort"))
                                .on('click', function() {
                                    //把upload button移除
                                    $this.remove();
                                    data.abort();
                                });
                            data.submit()
                            .done(function(result) {
                                if (result.error > 0)
                                {
                                    $this.text(i18n.t("ns:Message.Device.Fail"));
                                    $this.removeClass('btn-primary').addClass('btn-danger');
                                }
                                else
                                {
                                    $this.text(i18n.t("ns:Device.Done"));
                                    $this.removeClass('btn-primary').addClass('btn-success');
                                }
                                $this.prop('disabled', true);
                            });
                        });

        $('#createGroup').on('click', function(event) 
        {
            if (deviceGroups.length == 0)
            {
                $('#createGroup').prop('checked', true);
                return;
            }

            if ($(this).prop("checked"))
            {
                $("#deviceType").attr('disabled', false);
                $("#groupName").replaceWith(
                    '<input class="form-control input-lg" type="text" name="groupName" id="groupName">'
                );
            }
            else
            {
                $("#groupName").replaceWith(
                    '<select id="groupName" name="groupName" class="form-control input-lg"></select>'
                );
                var groupOptions = "";
                for(var i = 0; i < deviceGroups.length; i++) 
                    groupOptions += '<option>'+deviceGroups[i].name+'</option>';
                $("#groupName").html(groupOptions);
                $('#groupName').on('change', function(event) 
                {
                    for (var  i = 0; i < deviceGroups.length; i++)
                    {
                        if ($("#groupName").val() == deviceGroups[i].name)
                            $("#deviceType").val(deviceGroups[i].deviceTypeName);                 
                    }
                });
                $("#deviceType").attr('disabled', true);
                if (deviceGroups.length > 0)
                    $("#deviceType").val(deviceGroups[0].deviceTypeName);   
            }
        });

        var groupOptions = "";
        for(var i = 0; i < deviceGroups.length; i++) 
            groupOptions += '<option>'+deviceGroups[i].name+'</option>';
        $("#groupName").html(groupOptions);

        $('#groupName').on('change', function(event) 
        {
            for (var  i = 0; i < deviceGroups.length; i++)
            {
                if ($("#groupName").val() == deviceGroups[i].name)
                    $("#deviceType").val(deviceGroups[i].deviceTypeName);                
            }
        });

        var deviceTypeOptions = "";
        for(var i = 0; i < deviceTypesAndroid.length; i++){
            deviceTypeOptions+='<option>' + deviceTypesAndroid[i].name + '</option>';
        }
        $("#deviceType").html(deviceTypeOptions);

        if (deviceGroups.length > 0)
            $("#deviceType").val(deviceGroups[0].deviceTypeName);
        //$("#deviceType").attr('disabled', true);

        $("#fileupload").fileupload({
            url: $.config.server_host + '/RESTful/devices',
            //dataType: 'json',
            //是否選擇完檔案立即上傳
            autoUpload: false,
            //可以接受的格式, 可直接用|另外增加類型
            acceptFileTypes: /(\.|\/)(txt)$/i,
            //超過會顯示file too large
            maxFileSize: 5000000, // 5 MB
            // Enable image resizing, except for Android and Opera,
            // which actually support image resizing, but fail to
            // send Blob objects via XHR requests:
            disableImageResize: /Android(?!.*Chrome)|Opera/
                .test(window.navigator.userAgent),
            previewMaxWidth: 100,
            previewMaxHeight: 100,
            //是否讓縮圖點擊後另開頁面顯示原圖
            previewCrop: true,
            //檔案load之後把檔案名稱顯示出來
            forceIframeTransport: false,
            xhrFields: {
                withCredentials: true
            }
            //,redirect: window.location.origin+'/fn/ajax/cors/result.html?%s'
            //,initialIframeSrc: "javascript:document.write('<script>document.domain=\""+window.location.origin+"\";</script>')"
        }).on('fileuploadadd', function(e, data) {
            $("#files").empty();
            $("#progress_handle button").remove();
            $(".progress-bar.progress-bar-success").css('width', '0%');
            data.context = $('<div/>').appendTo('#files');
            $.each(data.files, function(index, file) {
                var item = $('<div/>');
                var node = $('<span/>').text(file.name);
                node.css('font-size', '20px');
                item.append(node);
                item.appendTo(data.context);
                $("#progress_handle").append(csvButton.clone(true).data(data));
            });
            //$("#btn_addFile").hide();  
            $('#separate').show();
            $("#progress_handle").show();
            $("#files").show();    
        }).on('fileuploadprocessalways', function(e, data) {
            var index = data.index,
                file = data.files[index],
                node = $(data.context.children()[index]);
            if (file.preview) {
                node
                    .prepend('<br>')
                    .prepend(file.preview);
            }
            //如果檔案error, 把錯誤的資訊顯示出來
            if (file.error) {
                node
                    .append('<br>')
                    .append($('<span class="text-danger"/>').text(file.error));
            }
        }).on('fileuploadprogressall', function(e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }).on('fileuploaddone', function(e, data) {
            if (!_.isUndefined(data.result.result)) {
                var contentMessage  = " ",
                    smallBoxMessage = " ";
                if (data.result.result) {                                
                    if (!_.isUndefined(data.result.dupicateDevice) && !_.isUndefined(data.result.invaildDevice)) {
                        if (data.result.dupicateDevice.length == 0 && data.result.invaildDevice.length == 0) {
                            smallBoxMessage += i18n.t("ns:Message.Device.AddSuccess");
                        } else {                                        
                            if (data.result.dupicateDevice.length != 0) {
                                if ($.config.debugger) {
                                    _.each(data.result.dupicateDevice, 
                                        function (value, key, list) {
                                            contentMessage += value + "<br>";
                                        });                                                
                                } else {
                                    contentMessage += i18n.t("ns:Message.Device.MoreDevicesAlreadyUse")+"\n";
                                };         
                                smallBoxMessage += i18n.t("ns:Message.Device.MoreDevicesAlreadyUse")+"\n";                               
                                console.log("[Device Import] Added successfully. " + JSON.stringify(data.result.dupicateDevice));
                            };
                            if (data.result.invaildDevice.length != 0) {
                                if ($.config.debugger) {
                                    _.each(data.result.invaildDevice, 
                                        function (value, key, list) {
                                            contentMessage += value + "<br>";
                                        });
                                } else {
                                    contentMessage += i18n.t("ns:Message.Device.DevicesInvalid")+"\n";
                                };                                        
                                smallBoxMessage += i18n.t("ns:Message.Device.DevicesInvalid")+"\n";
                                console.log("[Device Import] Added successfully. " + JSON.stringify(data.result.invaildDevice));
                            };
                            // <DIV> uploadResult
                            /*$("#uploadResultDiv").css("display","block")
                                            .addClass("alert alert-warning")
                                            .html(contentMessage);*/
                        }                                    
                    } 
                    $.smallBox({                            
                        title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddSuccess"),
                        content: "<i class='fa fa-plus'></i> <i>" + smallBoxMessage + "</i>",
                        color: "#648BB2",
                        iconSmall: "fa fa-plus fa-2x fadeInRight animated",
                        timeout: 5000
                    });                                 
                } else {
                    if (!_.isUndefined(data.result.invaildDevice)){
                        if (data.result.invaildDevice.length != 0) {
                            if ($.config.debugger) {
                                _.each(data.result.invaildDevice, 
                                    function (value, key, list) {
                                        contentMessage += value + "<br>";
                                    });                                            
                            } else {
                                contentMessage += i18n.t("ns:Message.Device.DeviceTypeNotExistOrFormatError");    
                            };                                        
                            console.log("[Device Import] Added failed. " + JSON.stringify(data.result.invaildDevice));
                        } else {
                            contentMessage += i18n.t("ns:Message.Device.DeviceTypeNotExistOrFormatError");    
                        };
                        /*$("#uploadResultDiv").css("display","block")
                                            .addClass("alert alert-danger")                                                        
                                            .html(contentMessage);*/
                    }
                    $.smallBox({
                        title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddFailed"),
                        content: "<i class='fa fa-plus'></i> <i>"+i18n.t("ns:Message.Device.DeviceTypeNotExistOrFormatError")+"</i>",
                        color: "#B36464",
                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                        timeout: 5000
                    });
                };
            };
        }).on('fileuploadfail', function(e, data) {
            $.smallBox({
                title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddFailed"),
                content: "<i class='fa fa-plus'></i> <i>"+ i18n.t("ns:Message.Device.AddFailed") + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 6000
            });
            console.log("[Device Import] File upload failed.");
        }).prop('disabled', !$.support.fileInput)
            .parent().addClass($.support.fileInput ? undefined : 'disabled');

        $("#btn_back").on("click", function(event) {                
            window.location.href = "#ajax/device_list.html/grid";      
        });  

        $('#separate').hide();
        $("#progress_handle").hide();
        $("#files").hide();     
    };


    /* define bootstrap starts */
    AD.bootstrap = function() {

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        $.ajax({
            url: $.config.server_rest_url+'/deviceTypes?pageNumber=1&pageSize=100&pageSort=name%2Casc',
            dataType: 'JSON',
            async: false,
            success: function(deviceTypeCollection) {
                deviceTypesWindows = deviceTypeCollection.content;
            }
        });

        $.ajax({
            url: $.config.server_rest_url+'/deviceTypes?pageNumber=1&pageSize=100&pageSort=name%2Casc',
            dataType: 'JSON',
            async: false,
            success: function(deviceTypeCollection) {
                deviceTypesAndroid = deviceTypeCollection.content;
            }
        });

        $.ajax({
            url: $.config.server_rest_url+'/deviceGroups?pageNumber=1&pageSize=100&pageSort=name%2Casc',
            dataType: 'JSON',
            async: false,
            success: function(deviceGroupCollection) {
                if (deviceGroupCollection.content.length == 0)
                {
                    $('#createGroup').prop('checked', true);
                    $("#deviceType").attr('disabled', false);
                    $("#groupName").replaceWith(
                        '<input class="form-control input-lg" type="text" name="groupName" id="groupName">'
                    );
                }
                else
                {
                    deviceGroups = deviceGroupCollection.content;
                    $("#deviceType").attr('disabled', true);
                }
            }
        });

        adReload();

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;

})(jQuery);
