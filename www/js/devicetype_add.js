(function($) {
	var AD = {};
    AD.page = "ajax/devicetype_add.html";

	var removeDupes = function(arr) {
		var nonDupes = new Array();
		arr.forEach(function(value) {
			if (nonDupes.indexOf(value) == -1) {
				nonDupes.push(value);
			}
		});
		return nonDupes;
	};
	
	/* no work
	function FileViewer(args) {
		for(var p in args)
			this[p] = args[p];
			
		this.reader = new FileReader();
		
		this.reader.onloadend = (function(self) {
			return function() {
				self.loaded();
			}
		})(this);
	}
	
	FileViewer.prototype.load = function() {
		//this.file = this.controller.files[0];
		this.file = this.controller.file;
		this.reader.readAsText(this.file);
	}
	FileViewer.prototype.loaded = function() {
		console.log(this.reader.result);
	}
	
	var fileViewer = undefined;
	*/
	
	var reportSettingListRender = function(){
	
		var xmlFile = $("#dataModel").val();
			if(xmlFile){
			var defaultDataModel;
			if(window.DOMParser) {
				var domParser = new DOMParser();
				defaultDataModel = domParser.parseFromString(xmlFile,"text/xml");
			} else { // Internet Explorer
				defaultDataModel = new ActiveXObject("Microsoft.XMLDOM");
				defaultDataModel.async = false;
				defaultDataModel.loadXML(xmlFile);
			}
			
			var xpath = ["DM>DCMO","DM>DiagMonMO>DiagMonData"]
			var dmNodes = new Array();
			for(var k in xpath){
				var nodes = defaultDataModel.querySelector(xpath[k]).childNodes;
				for(var i=0;i<nodes.length;i++){
					//console.log(xpath[k]+i +": "+nodes[i].tagName);
					if(nodes[i].tagName!==undefined) dmNodes.push(nodes[i].tagName);
				}
			}
			
			var data = removeDupes(dmNodes);
			var reportSettingList = new Array();
			for (var i = 0; i < data.length - 1; i++) {
				var obj = {
					value: data[i],
					label: data[i]
				};
				reportSettingList.push(obj);
			}
			$(".multiselect").multiselect({
				enableFiltering: true,
				includeSelectAllOption: true,
				maxHeight: 300,
				// buttonWidth:'820ptx',
				numberDisplayed: 6
			});

			$('#get-selected').on('click', function() {
				var values = [];

				$('option:selected', $('.multiselect')).each(function() {
					values.push($(this).val());
				});
			});
			$("#reportSettingList").multiselect('dataprovider', reportSettingList);
			
			// Css
			$("#reportSettingList +div button").css("height","46px");
		}
	};
	
	var uploadImage = function(){
		// Start //
		var exifNode = $("#exif"),
			thumbNode = $("#thumbnail"),
			actionsNode = $("#actions");
		// End //
		// Image
		var result = $('#result');
		var currentFile;
		var replaceResults = function (img) {
			var content;
			if (!(img.src || img instanceof HTMLCanvasElement)) {
				content = $('<span>Loading image file failed</span>');
			} else {
				content = $('<a target="_blank">').append(img)
					//.attr('download', currentFile.name)
					.attr('href', img.src || img.toDataURL());
			}
			result.children().replaceWith(content);
			$("#appearance").val(content[0].href);
			// Start //
			if(img.getContext){
				actionsNode.show();
			}
			// End //
		};
		var displayImage = function (file, options) {
			currentFile = file;
			// Start //
			result.show();
			// End //
			if (!loadImage(
					file,
					replaceResults,
					options
				)) {
				result.children().replaceWith(
					$('<span>Your browser does not support the URL or FileReader API.</span>')
				);
			}
		};
		// Start //
		var displayExifData = function(exif) {
			var thumbnail = exif.get('Thumbnail'),
				tags = exif.getAll(),
				table = exifNode.find('table').empty(),
				row = $('<tr></tr>'),
				cell = $('<td></td>'),
				prop;
			if(thumbnail) {
				thumbNode.empty();
				loadImage(thumbnail, function(img) {
					thumbNode.append(img).show();
				}, {orientation: exif.get('Orientation')});
			}
			for(prop in tags) {
				if(tags.hasOwnProperty(prop)) {
					table.append(
						row.clone()
							.append(cell.clone().text(prop))
							.append(cell.clone().text(tags[prop]))
					);
				}
			}
			exifNode.show();
		};
		// End //
		var dropChangeHandler = function (e) {
			e.preventDefault();
			e = e.originalEvent;
			var target = e.dataTransfer || e.target,
				file = target && target.files && target.files[0],
				options = {
					maxWidth: result.width(),
					canvas: true
				};
			if (!file) {
				return;
			}
			// Start //
			exifNode.hide();
			thumbNode.hide();
			result.hide();
			// End //
			loadImage.parseMetaData(file, function (data) {
				// Start //
				if(data.exif) {
					options.orientation = data.exif.get('Orientation');
					displayExifData(data.exif);
				}
				// End //
				displayImage(file, options);
			});
		};
		$("#appearanceInput").on("change",dropChangeHandler);
		// Start //
		$('#edit').on('click', function (event) {
			event.preventDefault();
			var imgNode = result.find('img, canvas'),
				img = imgNode[0];
			imgNode.Jcrop({
				setSelect: [40, 40, img.width - 40, img.height - 40],
				onSelect: function (coords) {
					coordinates = coords;
				},
				onRelease: function () {
					coordinates = null;
				}
			}).parent().on('click', function (event) {
				event.preventDefault();
			});
		});
		$('#crop').on('click', function (event) {
			event.preventDefault();
			var img = result.find('img, canvas')[0];
			if (img && coordinates) {
				replaceResults(loadImage.scale(img, {
					left: coordinates.x,
					top: coordinates.y,
					sourceWidth: coordinates.w,
					sourceHeight: coordinates.h,
					//minWidth: result.width()
					//minWidth: img.width
					minWidth: coordinates.w
				}));
				coordinates = null;
			}
		});
		// End //
	};
	
	AD.bootstrap = function() {
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
      	
      	// Translate Input of the placeholder
        translatePlaceholder();
		
		//fileViewer = new FileViewer({
		//	controller: $("input[type='file']")[0]
		//});
		$("body").removeClass("loading");
	
		$("#dataModelFile").on("change",function(event){
			var files = event.target.files; // FileList object
			//var result = $("#report");
			
			for(var i=0;i<files.length;i++){
				var file = files[i];
				
				if(!file.type.match('xml')) continue;
				
				var fileReader = new FileReader();
				
				fileReader.addEventListener("load", function(event){
					var xmlFile = event.target;
					
					//var div = document.createElement("div");
					
					//div.innerHTML = "<blockquote>"+xmlFile.result+"</blockquote>";
					$("#dataModel").val(xmlFile.result);
				});
				
				fileReader.readAsText(file);
				//fileReader.readAsDataURL(file);
			}
			$("#customFileInput span").text(this.files[0].name);
		});
		
		$("#appearanceInput").change(function(event){
			$("#customFileInput2 span").text(this.files[0].name);
		});
		
		uploadImage();
		/*
		$("#appearance").on("change",function(event){
			var files = event.target.files;
			loadImage(files[0],function(img){
				$("#wizard-1 .tab-content .row:eq(1)").append(img);
			});
		});
		*/
		var validateRule = {
			name: {
				required: true
			},
			operatingSystem: {
				required: true,
			},
			file: {
				required: true
			},
			appearanceFile: {
				required: true,
			},
			reportInterval: {
				required: true,
				digits: true
				//minValue: 3,
				//maxValue: 1440
			}
		};
		
		var $validator = $("#wizard-1").validate({

            rules: validateRule,

            messages: {
				name:            i18n.t("ns:Message.ThisFieldRequire"),
            	operatingSystem: i18n.t("ns:Message.ThisFieldRequire"),
                file:     		 i18n.t("ns:Message.ThisFieldRequire"),
                appearanceFile:  i18n.t("ns:Message.ThisFieldRequire"),
                reportInterval:  i18n.t("ns:Message.ThisFieldRequire")
            },

            highlight: function(element) {
                $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if (element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
		
		$('#bootstrap-wizard-1').bootstrapWizard({
			'tabClass': 'form-wizard',
            'onTabShow': function (tab, navigation, index) {
                switch (index) {                        
                    case 0:
                        $("#wizard-1 .next a").text(i18n.t("ns:Message.Next"));
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index).removeClass('complete');                                             
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(0).find('.step').text("1")
                        $('#btn_back').removeClass("disabled");            
                        break;
                    case 1:
                        $("#wizard-1 .next a").text(i18n.t("ns:Message.Save"));                                
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
                        $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                        .html('<i class="fa fa-check"></i>');                            
                        $('#btn_save').removeClass("disabled");
                        break;
                }
            },                
            'onPrevious': function (tab, navigation, index) {                    
                if($('#bootstrap-wizard-1').bootstrapWizard('currentIndex') === 0) {
                    window.location.href = "#ajax/devicetype_list.html/grid";
                }                
            },
			'onNext': function (tab, navigation, index) {
				var $valid = $("#wizard-1").valid();
				if (!$valid) {
					$validator.focusInvalid();
					return false;
				} else {
					
					var wizard = $('#bootstrap-wizard-1');

					if(wizard.bootstrapWizard('currentIndex')===wizard.bootstrapWizard('navigationLength')-1){
						$("#wizard-1 .next a").text(i18n.t("ns:Message.Save"));
					}else{
						$("#wizard-1 .next a").text(i18n.t("ns:Message.Next"));
					}
					
					// Last page
					if (wizard.bootstrapWizard('currentIndex')==wizard.bootstrapWizard('navigationLength')){
						var reportItemList = [];
						/*
						var data = []; // get current type
						$.each(data,function(index,value){
							if (value.name)
							reportItemList.push(value.name);
						});

						var selectedReportItemList=$("#wizard-1 input:checked");

						var comparedReportItemSet=[];
						$.each(selectedReportItemList,function(index,value){
							if ($.inArray(value.name,reportItemList)>-1)
								comparedReportItemSet.push(value.name);
						});

						var comparedReportIdSet=[];
						$.each(comparedReportItemSet,function(cindex,cvalue){
							$.each(data,function(dindex,dvalue){
								if (dvalue.name==cvalue){
									comparedReportIdSet.push(dvalue.id);
								}
							})
						});
						*/
						//var selector = "#wizard-1 input:checked";
						var selector = "#wizard-1 input[type='checkbox']";
						$.each($(selector),function(index,node){
							//reportItemList.push(node.value);
							console.log(node.value);
							if(node.value !== "multiselect-all"){
								var reportNode = {};
								reportNode.isReport = node.checked;
								reportNode.name = node.value;
								reportItemList.push(reportNode);
							}
						});
						var data = "{\""+$("#wizard-1 input:not([name='multiselect']):not([name='defaultDataModelTree']):not([name='appearance'])").serialize().replace(/&/g,'","').replace(/=/g,'":"')+"\"";
						data+=",\"remark\":"+JSON.stringify($("#remark").val());
						data+=",\"operatingSystem\":"+JSON.stringify($("#operatingSystem").val());
						data+=",\"defaultDataModelTree\":"+JSON.stringify($("#dataModel").val());
						data+=",\"reportSettingNameList\":"+JSON.stringify(reportItemList);
						data+=",\"image\":"+JSON.stringify($('input[name="appearance"]').val().replace("data:image/png;base64,", ""));
						data+="\}";
						//console.log("data:"+data);
						$.ajax({
							url: $.config.server_rest_url+'/deviceTypes',
							type: 'POST',
							dataType: 'json',
							contentType: 'application/json',
							crossDomain: true,
							data: data
						}).done(function(data){
                            $.smallBox({
                            	title: i18n.t("ns:Message.Device.DeviceTypeAdd") + i18n.t("ns:Message.Device.AddSuccess"),
                                content: "<i class='fa fa-plus'></i> <i>"+ i18n.t("ns:Message.Device.AddSuccess") +"</i>",
                                // title: "[Device Type Add] Added successfully",
                                // content: "<i class='fa fa-plus'></i> <i>Added successfully</i>",
                                color: "#648BB2",
                                iconSmall: "fa fa-plus fa-2x fadeInRight animated",
                                timeout: 5000
                            }); 
							window.location.href = '#ajax/devicetype_list.html';
						}).fail(function(xhr, textStatus, errorThrown){
                            $.smallBox({
                                title: i18n.t("ns:Message.Device.DeviceTypeAdd") + i18n.t("ns:Message.Device.AddFailed"),
                                content: "<i class='fa fa-plus'></i> <i>"+ i18n.t("ns:Message.Device.AddFailed") + "</i>",
                                // title: "[Device Type Add] Added failed",
                                // content: "<i class='fa fa-plus'></i> <i>Added failed</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });							
							var cause = JSON.parse(xhr.responseText);
							console.log("failure: "+cause.error);
							// $.smallBox({
							// 	title: "[Device Type Edit] Data failed",
							// 	content: "<i class='fa fa-pencil-square-o'></i> <i>"+cause.error+"</i>",
							// 	color: "#B36464",
							// 	iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
							// 	timeout: 9000
							// });
						}).always(function(){
							//window.location.href = '#ajax/devicetype_list.html';
						});
					}
					console.log('Tab number:'+index);
					
					if(index==1){
						reportSettingListRender();
					}
					$('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
					$('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step').html('<i class="fa fa-check"></i>');
				}				

			}
		});

	};
	
	$[AD.page] = AD;

	function translatePlaceholder () {
        $("#name").attr("placeholder", i18n.t("ns:DeviceType.Message.DeviceTypeName"));
        $("#file").attr("placeholder", i18n.t("ns:DeviceType.Message.DeviceTypeDataModel"));
        $("#appearanceFile").attr("placeholder", i18n.t("ns:DeviceType.Message.DeviceTypeAppearance"));
        $("#remark").attr("placeholder", i18n.t("ns:DeviceType.Message.Remark"));
        $("#reportInterval").attr("placeholder", i18n.t("ns:DeviceType.Message.ReportInterval"));
    }
    
})(jQuery);