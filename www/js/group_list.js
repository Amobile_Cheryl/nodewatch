(function($) {
    var currentPage = "";
    var AD = {};
    AD.page = "ajax/group_list.html";
    var idAttribute = AD.idAttribute = "id";
    var device_columns = [
        //columns CheckBox
        /*
        {
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            width: '20px',
            callback: function(o) {
                return '<input id="checkbox_' + o[idAttribute] + '" type="checkbox" value="' + o[idAttribute] + '"></input>';
            }
        },
        */
        //columns Group Name
        {
            title: 'ns:Group.Title.GroupName', 
            property: 'name',
            // cellClassName: 'groupName',
            filterable: false,
            sortable: true,
            callback: function(o) {
                var groupName = o.name;
                //if (groupName.length > 18)
                //    return '<i class="fa fa-th-large"></i> <a title="' + groupName + '" href="#ajax/group_detail.html/' + o.id + '">'+ groupName.substr(0, 20) + ' ...</a>';
                //else
                    return '<i class="fa fa-th-large"></i> <a href="#ajax/group_detail.html/' + o.id + '">'+ groupName + '</a>';
            }
        },
        //columns Members
        {
            title: 'ns:Group.Title.Members',
            property: 'deviceIdSet',
            // cellClassName: 'members',
            filterable: false,
            sortable: false,
            // width: '150px',
            callback: function(o) {
                //return '<a href="#ajax/group_member_list.html/' + o.id + '">' +  "<img src='img/group/group-icon-3.png'>" + ' ' + ((typeof o.deviceIdSet === "undefined"|| o.deviceIdSet === null || o.deviceIdSet === 0) ? '0' : o.deviceIdSet.length) + '</a>';
                //return '<a href="#ajax/device_list.html/group_device_list/' + o.id + '">' +  "<img src='img/group/group-32-0.png'>" + ((typeof o.deviceIdSet === "undefined"|| o.deviceIdSet === null || o.deviceIdSet === 0) ? '0' : o.deviceIdSet.length) + '</a>';
                return o.deviceIdSet.length;
            }
        },
        //columns Operating System
        {
                title: 'ns:Group.DeviceType',
                property: 'deviceTypeName',
                filterable: false,
                sortable: false,
                // width: '300px',
        },
        //columns Domain
        {
            title: 'ns:Group.Title.Domain',
            property: 'domainName',
            cellClassName: 'domainName',
            filterable: false,
            sortable: false,
            visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
            callback: function(o) {
                //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId] ;
                return (typeof(o.domainName) === "undefined") ? '' : o.domainName;
            }
        },
        //columns Policy
        {
            title: 'Software Policy',
            property: 'devicePolicyName',
            // cellClassName: 'remark',
            filterable: false,
            sortable: false
        },
        //columns Remark
        {
            title: 'ns:Group.Title.Remark',
            property: 'remark',
            // cellClassName: 'remark',
            filterable: false,
            sortable: false
        },
        //columns Action
        {
            title: 'ns:Group.Title.Action',
            sortable: false,
            visible: $.login.user.checkRole('DeviceGroupOperator'),
            callback: function(o) {
                var _html = '';                
                if (o) {                    
                    // [Action] Edit the Group                    
                    //_html += '<a id="edit" href="#' + AD.page + '/edit/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";                    
                    // [Action] Lock the Group
                    // _html += '<a id="lock" href="#' + AD.page + '/lock/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-greenLight" style="display: inline;"><i class="fa fa-lock"></i></a>';
                    // [Action] Unlock the Group
                    // _html += '<a id="unlock" href="#' + AD.page + '/unlock/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-greenLight" style="display: inline;"><i class="fa fa-unlock"></i></a>' + "&nbsp;";
                    // [Action] Install the Group
                    // _html += '<a id="install" href="#' + AD.page + '/install/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-orange" style="display: inline;"><i class="fa fa-download"></i></a>' + "&nbsp;";
                    // [Action] Uninstall the Group
                    //_html += '<a id="unistall" href="#' + AD.page + '/uninstall/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-orange" style="display: inline;"><i class="fa fa-upload"></i></a>' + "&nbsp;";                                      
                    // [Action] Delete the Group
                    if ($.common.isDelete)
                        _html += '<a id="delete" href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                }
                return _html;
            }
        }        
    ];

    var routes = {};
        routes[AD.page + '/grid']           = 'main';        
        routes[AD.page + '/delete/:_id']    = 'act_delete_group';
        routes[AD.page] = 'render';     

    var MyRouter = Backbone.DG1.extend({        
        routes: routes,
        initialize: function(opt) {             
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            console.error();
            this.show_grid();
        },
        show_grid: function() {
            var t = this;

            var gvs = "#s1 .cctech-gv";
            var gv = $(gvs).first();
            
            if(!androidGrid) {                    
                var androidGrid = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    buttons: t.AD.buttons,
                    columns: t.AD.columns,
                    AD: t.AD
                });

                gv.on("click", ".s2-btn", function (event) {
                    event.preventDefault();

                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    t.navigate("#ajax/group_add.html", {
                        trigger: true
                    });
                });

                gv.on("click", ".s1-btn", function (event) {
                    event.preventDefault();

                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    t.navigate("#ajax/group_add.html", {
                        trigger: true
                    });
                });
            } else {
                androidGrid.refresh();
            }
            t.$gv.show();
        },
        act_delete_group: function(id) {                       
            var dmURL = $.config.server_rest_url + '/deviceGroups/';

            $.SmartMessageBox({
                // title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; Delete Group <span class='txt-color-orangeDark'><strong> &nbsp;" +
                //     id + "</strong></span> ?",
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Group.DeleteTheGroup")+" <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                // content: "Delete the group ?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> "+i18n.t("ns:Message.Group.DeleteTheGroupContent")+"</span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + id,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.GroupDelete") + i18n.t("ns:Message.Group.DeleteSuccess"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Group.DeletedGroupId") + "&nbsp;" + id + "</i>",
                            // title: "[Group Delete] Deleted successfully",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Group ID: &nbsp;" + id + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.GroupDelete") + i18n.t("ns:Message.Group.DeleteFailed"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Group.DeletedGroupId") + "&nbsp;" + id + "</i>",
                            // title: "[Group Delete] Deleted failed",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Group ID: &nbsp;" + id + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/group_list.html";
                    });
                } else {
                    window.location.href = "#ajax/group_list.html";
                }
            });
        },  
        main: function() {
            if (currentPage == "s1")
                s1();
            else if (currentPage == "s2")
                s2();
        }
    });

    // Windows
    function s1() {
        var AD = {};
        AD.page = "ajax/group_list.html";
        AD.collections = {};
        currentPage = "s1";
        var url = $.config.server_rest_url + "/deviceGroups" + "?search=operatingSystemName:Windows";
        if ($.login.user.checkRole('DeviceOperator'))
        AD.buttons = [
            //button
            {
                "sExtends" : "text",
                "sButtonText" : "<i class='fa fa-plus'></i>&nbsp; " + "Add Group",//i18n.translate("ns:File.Upload"),
                "sButtonClass" : "upload-btn s1-btn btn-default txt-color-white bg-color-blue"
            },
            {
                "sExtends": "text",
                "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                "fnClick": function(nButton, oConfig, oFlash) {
                    $(".cctech-gv table").dataTable().fnDraw(false);
                }
            }
        ];

        AD.columns = device_columns;
        AD.Model = Backbone.Model.extend({urlRoot: url, idAttribute: idAttribute});
        AD.Collection = Backbone.Collection.extend({url: url, model: AD.Model}); 
        AD.app = new MyRouter({"AD": AD});        
        pageSetUp();
    };

    // Android
    function s2() { 
        var AD = {};
        AD.page = "ajax/group_list.html";
        AD.collections = {};
        currentPage = "s2";
        var url = $.config.server_rest_url + "/deviceGroups" + "?search=operatingSystemName:Android";
        if ($.login.user.checkRole('DeviceOperator'))
        AD.buttons = [
            //button
            {
                "sExtends" : "text",
                "sButtonText" : "<i class='fa fa-plus'></i>&nbsp; " + "Add Group",//i18n.translate("ns:File.Upload"),
                "sButtonClass" : "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
            },
            {
                "sExtends": "text",
                "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                "fnClick": function(nButton, oConfig, oFlash) {
                    $(".cctech-gv table").dataTable().fnDraw(false);
                }
            }
        ];

        AD.columns = device_columns;
        AD.Model = Backbone.Model.extend({urlRoot: url, idAttribute: idAttribute});
        AD.Collection = Backbone.Collection.extend({url: url, model: AD.Model}); 
        AD.app = new MyRouter({"AD": AD});        
        pageSetUp();
    };
     
    AD.bootstrap = function() {
        i18n.init(function(t){
            $('[data-i18n]').i18n();            
        });     

        if (!$.login.user.checkRole('DeviceViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        };

        $("#myTab li").click(function() {            
            switch (this.firstElementChild.id) {
                case "s1": //Windows
                    s1();
                    break;
                case "s2": //Android
                    s2();
                    break;
                case "s3": //iPhone
                    s3();
                    break;                
            }
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
        
        if (currentPage == "")
            s2();

        if (currentPage == "s1")
        {
            $("#tab2").removeClass('active');
            $("#tab1").addClass('active');
        }
        else if (currentPage == "s2")
        {
            $("#tab1").removeClass('active');
            $("#tab2").addClass('active');
        }
        $("body").removeClass("loading");
    };
    $[AD.page] = AD;
})(jQuery);
