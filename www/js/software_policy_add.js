(function($) {        
    var AD = {};
    AD.page = "ajax/software_policy_add.html";

    var reloadJs = function(){
        /*
         * Load bootstrap wizard dependency
         */
        loadScript("js/plugin/bootstrap-wizard/jquery.bootstrap.wizard.min.js", runBootstrapWizard);
        
        //Bootstrap Wizard Validations
        var runBootstrapWizard = function() {
            
            var $validator = $("#wizard-1").validate({
            
                rules: {
                    operatingSystem: {
                        required: true
                    },
                    groupname: {
                        required: true
                    },
                    remark: {
                        required: false
                    }
                },
                
                messages: {
                    operatingSystem:    "Please specify your operating system",
                    groupname:          i18n.t("ns:Group.Message.SpecifyGroupName"),
                    remark:             "Please specify your remark"
                },
                
                highlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
                },
                unhighlight: function (element) {
                    $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
                },
                errorElement: 'span',
                errorClass: 'help-block',
                errorPlacement: function (error, element) {
                    if (element.parent('.input-group').length) {
                        error.insertAfter(element.parent());
                    } else {
                        error.insertAfter(element);
                    }
                }
            });

            $("#bootstrap-wizard-1").bootstrapWizard({
                'tabClass': 'form-wizard',
                'onTabShow': function (tab, navigation, index) {
                    switch (index) {                        
                        case 0:
                            $("#wizard-1 .next a").text(i18n.t("ns:Message.Next"));
                            $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index).removeClass('complete');                                             
                            $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(0).find('.step').text("1")
                            $('#btn_back').removeClass("disabled");            
                            break;
                        case 1:
                            $("#wizard-1 .next a").text(i18n.t("ns:Message.Save"));                                
                            $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).addClass('complete');
                            $('#bootstrap-wizard-1').find('.form-wizard').children('li').eq(index - 1).find('.step')
                            .html('<i class="fa fa-check"></i>');                            
                            $('#btn_save').removeClass("disabled");
                            break;
                    }
                },                
                'onPrevious': function (tab, navigation, index) {                    
                    if($('#bootstrap-wizard-1').bootstrapWizard('currentIndex') === 0) {
                        window.location.href = "#ajax/software_policy_list.html/grid";
                    }                
                },
                'onNext': function (tab, navigation, index) {
                    var $valid = $("#wizard-1").valid();
                    if (!$valid) {
                        $validator.focusInvalid();
                        return false;
                    } else {
                        // Save button event                        
                        // [Slove IE issue] Kenny block at 2015/2/6
                        if($.browser.msie || ($.browser.name === "Netscape")){
                            event.returnValue = false;
                        }else{
                            event.preventDefault();
                        }                       

                        var wizard = $('#bootstrap-wizard-1');

                        if(wizard.bootstrapWizard('currentIndex') === wizard.bootstrapWizard('navigationLength')) {
                            $("body").addClass("loading");

                            var originalData = {
                                name: $("#name").val(), 
                                remark: $("#remark").val() || "",
                                appBlacklist: $("#appBlacklist").val().split(","),
                                operatingSystem: $("#operatingSystem").val()
                                // deviceIdSet:        $('#deviceSelect').val() || []
                            };
                            var data = JSON.stringify(originalData);

                            if (typeof(originalData) === 'undefined'){                                    
                                $.smallBox({
                                    title: i18n.t("ns:Message.Group.GroupAdd") + i18n.t("ns:Message.Group.GotDeviceListFailed"),
                                    content: "<i class='fa fa-plus'></i> <i>"+ i18n.t("ns:Message.Group.DataUndefined") + "</i>",
                                    // title: "[Group add] Got device list failed",
                                    // content: "<i class='fa fa-plus'></i> <i>data undefined</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });            
                                console.log("[Group Add] Got group data info failed => data undefined.");
                                window.location.href = "#ajax/software_policy_list.html";
                            } else {
                                $("#wizard-1").submit(function(e)
                                {                                                     
                                    $.ajax({
                                        type: 'POST',
                                        crossDomain: true,
                                        url: $.config.server_rest_url + '/devicePolicys',            
                                        contentType: 'application/json',
                                        dataType: 'json',
                                        data: data,
                                        timeout: 10000,
                                        error: function(jqXHR, textStatus, errorThrown){
                                            console.log("[Policy Add]:POST failed");
                                        }
                                    }).done(function(data) { 
                                        $.smallBox({
                                            title: "Add Policy " + i18n.t("ns:Message.Group.AddSuccess"),
                                            content: "<i class='fa fa-plus'></i> <i>"+ i18n.t("ns:Message.Group.AddSuccess") + "</i>",
                                            // title: "[Group Add] Added successfully",
                                            // content: "<i class='fa fa-plus'></i> <i>Added successfully</i>",
                                            color: "#648BB2",
                                            iconSmall: "fa fa-plus fa-2x fadeInRight animated",
                                            timeout: 5000
                                        });                                                                 
                                    }).fail(function (jqXHR, textStatus){
                                        $.smallBox({
                                            title: "Add Policy " + i18n.t("ns:Message.Group.AddFailed"),
                                            content: "<i class='fa fa-plus'></i> <i>"+ i18n.t("ns:Message.Group.AddFailed") + "</i>",
                                            // title: "[Group Add] Added failed",
                                            // content: "<i class='fa fa-plus'></i> <i>Added failed</i>",
                                            color: "#B36464",
                                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                            timeout: 5000
                                        });
                                        console.log("[Group Add] Added failed => " + textStatus);
                                    }).always(function(){
                                        $("body").removeClass("loading");     
                                        window.location.href = "#ajax/software_policy_list.html";
                                    });         
                                    e.preventDefault(); //Prevent Default action. 
                                    // e.unbind();
                                }); 
                                $("#wizard-1").submit(); //Submit the form
                            }
                        }       
                    }
                }
            });            
        }

        runBootstrapWizard();
        $('#operatingSystem').on('change', function(event) 
        {
            if ($("#operatingSystem").val() == "Android")
                $("#appBlacklist").attr("placeholder", "Package IDs (ex. com.android.browser,com.android.music)");
            else
                $("#appBlacklist").attr("placeholder", "Process Names (ex. calc.exe,explorer.exe)");
        });
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // multiselect initial
        // multiselectInit();

        // Reload JavaScript
        reloadJs();
        
        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/software_policy_add.html\/(\w+)$/, "$1");

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/" + id, {
                trigger: true
            });
        }

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
        $("body").removeClass("loading");
    };
    /* define bootstrap ends */
    $[AD.page] = AD;

})(jQuery);
