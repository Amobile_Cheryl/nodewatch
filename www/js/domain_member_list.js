(function($) {
    var AD = {};
    AD.page = "ajax/domain_member_list.html";
    var id;
    var selectedDevices = [];
    var deselectedDevices = [];
    var adReload = function() 
    {
        AD.collections = {};
        AD.buttons = {};
        AD.columns = [
            {
                title: '<div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-devices"><i></i>&nbsp;&nbsp;&nbsp;&nbsp;</label></div>',
                sortable: false,
                callback: function(o) {
                    if ($.inArray(o.id, selectedDevices) >= 0)
                        return '<div><div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-device" data-device-id="'+o.id+'" checked><i></i></label></div></div>';
                    else
                        return '<div><div class="smart-form"><label class="checkbox"><input type="checkbox" class="checkbox-device" data-device-id="'+o.id+'"><i></i></label></div></div>';
                }
            },
            {
                title: '',
                sortable: false,
                width: '60px',
                callback: function(o) {
                    return '<img src="'+o.deviceTypeIconUrl+'"" style="height:30px; margin-top:-6px; margin-bottom:-6px;" />';
                }
            },
            {
                title: 'ns:Device.Title.Status',
                filterable: false,
                sortable: false,
                width: '80px',
                callback: function(o) {
                    var isLock;
                    //isLock = (_.isUndefined(o.isLock)) ? "" : "<i class=\"fa fa-lock\" title=\""+i18n.t("ns:Device.Message.Lock")+"\"></i>";
                    if (o.hasOwnProperty('isLock') == true)
                        isLock = o.isLock ? "<i class=\"fa fa-lock txt-color-red\" title=\""+i18n.t("ns:Device.Message.Lock")+"\"></i>" : "<i class=\"fa fa-unlock txt-color-blueLight\" title=\""+i18n.t("ns:Device.Message.Unlock")+"\"></i>";
                    else
                        isLock = "<i class=\"fa fa-unlock txt-color-blueLight\" title=\""+i18n.t("ns:Device.Message.Unlock")+"\"></i>";

                    var isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:gray;\" title=\""+i18n.t("ns:Message.Device.Offline")+"\"></i>";
                    if (!_.isUndefined(o.isOnline)) {
                        if(o.isOnline){
                            isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:lawngreen;\" title=\""+i18n.t("ns:Message.Device.Online")+"\"></i>";
                        }
                    }
                    //var isJailbreak = (_.isUndefined(o.isJailbreak)) ? "" : "<i class=\"fa fa-bug\" style=\"color:crimson;\" title=\""+i18n.t("ns:Message.Device.Jailbreak")+"\"></i>";
                    //return isLock+isJailbreak;
                    //return isJailbreak;
                    return isLock + "&nbsp;" + isOnlineStatus;
                }
            },
            {
                title: 'ns:Device.Title.Label',
                property: 'label',
                filterable: true,
                searchName: 'label',
                sortable: true,
                width: '200px',
                callback: function(o) {
                    if (o.provisionStatus === "Provisioned") {
                        return '<a href="#ajax/device_detail.html/' + o.id + '" title="Check the device detail">' + o.label + '</a>';
                    } else {
                        return o.label;
                    }
                }
            },           
            //column2
            {
                title: 'IMEI & MAC',
                property: 'name',
                filterable: true,
                searchName: 'name',
                sortable: true,
                width: '200px'
            },
            {
                title: 'ns:Device.Title.DeviceType',
                cellClassName: 'deviceTypeName',
                filterable: true,
                searchName: 'deviceTypeName',
                width: '150px',
                searchDom : function(){
                    var options = '<option value selected="selected">'+i18n.t("ns:Device.Title.DeviceType")+'</option>';
                    $.ajax({
                        url: $.config.server_rest_url+'/deviceTypes',
                        dataType: 'JSON',
                        async: false,
                        success: function(deviceTypeCollection){
                            for(var i=0;i<deviceTypeCollection.length-1;i++){
                                options+='<option>'+deviceTypeCollection[i].name+'</option>';
                            }
                        }
                    });
                    return '<select class="form-control" name="deviceType" placeholder="'+i18n.t("ns:Device.Title.DeviceType")+'">'+options+'</select>';
                },
                sortable: false,
                callback: function(o) {
                    return o.deviceTypeName;
                }
            },
            //column6
            {
                title: 'ns:Device.Title.LastReport',
                property: 'latestConnectTime',
                cellClassName: 'latestConnectTime',
                filterable: false,
                sortable: true,
            },
            //column8
            {
                title: 'ns:Device.Title.Domain',
                property: 'domainId',
                filterable: false,
                sortable: false,
                visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
                callback: function(o) {
                    //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId] ;
                    return (typeof(o.domainName) === "undefined") ? '' : o.domainName;
                }
            },
            /*{
                title: 'ns:Device.Title.Action',
                operator: true,
                sortable: false,
                width: '100px',
                callback: function(o) {
                    var _html = '';
                    //_html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                    _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                    // _html += '<a href="#" class="btn btn-mini btn-warning removeOne" title="Unlock" data-id="' + o[idAttribute] + '"><i class="icon-unlock"></i> </a>';
                    return _html;
                }
            }*/
        ];
        /* define namespace ends */
        var url;

        if (!$.login.user.checkRole('DeviceOperator'))          
        {
            AD.columns = $.grep(AD.columns, function(e) {
                return (!_.has(e, "operator"));
            }); 
        }               
        url = $.config.server_rest_url + '/domains/' + id + '/jointDevices';
        AD.isAdd = false;
        AD.buttons = [
            {
                "sExtends": "text",
                "sButtonText" : "<i class='fa fa-save'></i>&nbsp; Save",
                "sButtonClass": "btn-saveDevices txt-color-white bg-color-blue"
            },
        ];

        var idAttribute = AD.idAttribute = "id";
        AD.title = "ns:Menu.DeviceList";

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
        /* collection ends */
    };


    var routes = {};
    routes[AD.page + '/:_id'] = 'show_grid';
    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/summary/:_id'] = 'show_summary';
    routes[AD.page + '/edit/:_id'] = 'show_edit';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        initialize: function(opt) {
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            // console.error();
        },
        show_edit: function(id) {  
            var t = this;            
            t.navigate("#ajax/device_edit.html/" + id, {
                trigger: true
            });
        },
        show_grid: function() {
            var t = this;
            // init GridView
            if (!t.gv) {
                t.gv = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: !_.isUndefined(t.AD.isAdd) ? t.AD.isAdd : true,
                    sort: t.AD.sort,
                    order: [[1, "desc"]], //t.AD.order,
                    filter: {}, //t.AD.filter,
                    buttons: t.AD.buttons,
                    AD: t.AD
                });

                t.$gv.on("click", ".btn-saveDevices", function(event) 
                {
                    event.preventDefault();
                    var data = {
                        additionDeviceIdSet: selectedDevices,
                        removeDeviceIdSet: deselectedDevices
                    };
                    $.ajax({
                        type: 'POST',
                        dataType: 'json',
                        headers: {
                            "X-HTTP-Method-Override": "PUT"
                        },
                        async: false,
                        url: $.config.server_rest_url + '/domains/' + id + '/devices',
                        contentType: 'application/json; charset=utf-8',
                        data: JSON.stringify(data),
                        success: function(data) {
                            console.log('Save Success');
                            $.smallBox({
                                title: "[Domain Edit]" + " Edit Success!",
                                content: "<i class='fa fa-edit'></i> <i>"+ "Edit member successfully" + "</i>",
                                color: "#648BB2",
                                iconSmall: "fa fa-edit fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                            window.location.href = '#ajax/domain_detail.html/' + id;
                        },
                        error: function(data) {
                            $.smallBox({
                                title: "[Domain Edit]" + " Edit Fail..",
                                content: "<i class='fa fa-edit'></i> <i>"+ "Edit member failed" + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        },
                    });                    
                });
                t.$gv.on("click", ".checkbox-device", function(event) {
                    var deviceId = parseInt($(this).attr('data-device-id'));
                    if ($(this).prop("checked"))
                    {
                        if (selectedDevices.indexOf(deviceId) < 0)
                            selectedDevices.push(deviceId); 
                        if (deselectedDevices.indexOf(deviceId) >= 0)
                            deselectedDevices.splice(deselectedDevices.indexOf(deviceId), 1); 
                    }
                    else
                    {
                        if (selectedDevices.indexOf(deviceId) >= 0)
                            selectedDevices.splice(selectedDevices.indexOf(deviceId), 1); 
                        if (deselectedDevices.indexOf(deviceId) < 0)
                            deselectedDevices.push(deviceId);
                    }

                    /*if ($(".checkbox-device:checked").length < $(".checkbox-device").length)
                        $(".checkbox-devices").prop("checked", false);
                    else
                        $(".checkbox-devices").prop("checked", true);*/

                    console.log("select:" + selectedDevices);
                    console.log("deselect:" + deselectedDevices);
                });

                t.$gv.on("click", ".checkbox-devices", function(event) {
                    if ($(this).prop("checked"))
                    {
                        $.each($(".checkbox-device"), function() {
                            var deviceId = parseInt($(this).attr('data-device-id'));
                            if ($(this).prop("checked") == false)
                            {
                                $(this).prop("checked", true);
                                if (selectedDevices.indexOf(deviceId) < 0)
                                    selectedDevices.push(deviceId); 
                                if (deselectedDevices.indexOf(deviceId) >= 0)
                                    deselectedDevices.splice(deselectedDevices.indexOf(deviceId), 1); 
                            }
                        }); 
                    }
                    else
                    {
                        $.each($(".checkbox-device"), function() {
                            var deviceId = parseInt($(this).attr('data-device-id'));
                            if ($(this).prop("checked") == true)
                            {
                                $(this).prop("checked", false);
                                if (selectedDevices.indexOf(deviceId) >= 0)
                                    selectedDevices.splice(selectedDevices.indexOf(deviceId), 1); 
                                if (deselectedDevices.indexOf(deviceId) < 0)
                                    deselectedDevices.push(deviceId);
                            }
                        });                       
                    }
                    console.log(selectedDevices);
                });
            } 
            else
                t.gv.refresh();

            t.$fv.hide();
            t.$gv.show();

            pageSetUp();
        }       
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        if (!$.login.user.checkRole('DeviceViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        }

        var hash = window.location.hash;
        id = hash.split('/')[2];
        if (!_.isUndefined(id)) {
            $.ajax({
                url: $.config.server_rest_url+'/domains/'+id,
                type: 'GET',
                dataType: 'json',
                timeout: 10000,
                async: false,
                error: function(data, status, error){
                }
            }).done(function(data) {
                $("#domainName a").prop("href", '#ajax/domain_detail.html/' + id);
                $("#domainName font").text(data.name);
                selectedDevices = data.deviceIdSet;
                deselectedDevices = [];
            }).fail(function(){
            }); 
        };

        adReload();

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
        $("body").removeClass("loading");
     };
    /* define bootstrap ends */

    $[AD.page] = AD;
    // $[AD.page].bootstrap();

})(jQuery);
