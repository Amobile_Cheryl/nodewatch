(function($) {
    /* define namespace starts */

    var AD = {};
    AD.collections = {};

    var url = $.config.server_rest_url + '/domains';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "ns:Menu.DomainList";
    AD.page = "ajax/domain_list.html";

    AD.validate = function() {
        return true;
    };

    AD.buttons = [
        {
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
            "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
            "fnClick": function(nButton, oConfig, oFlash) {
                $(".cctech-gv table").dataTable().fnDraw(false);
            }
        }
    ];

    AD.forms = [
        //form config
        {
            title: 'Id',
            property: 'id',
            default_value: ""
        }, 
        {
            title: 'Name',
            property: 'name',
            default_value: ""
        },
        /*
        {
            title: 'Send Error Message',
            property: "isSendErrorMessage",
            // useTemplate: false,
            view: function() {
                var html = '<section>';
                html += '<label class="checkbox">';
                html += '<input type="checkbox" name="' + this.property + '" value="Y">';
                html += '<i></i>' + this.title;
                html += '</label>';
                html += '</section>';
                return $(html);
            },
            value: function() {
                var t = this;
                if (t.$el.find('input[type="checkbox"]:checked').length > 0)
                    return true;
                else
                    return false;
            },
            setValue: function(m) {
                var t = this;
                var v = m.get(t.property) || t.default_value || 'N';
                console.log(m.get(t.property));
                if (v === true || v ==="true")
                    t.$el.find('input[type="checkbox"]').prop('checked', true);
                else
                    t.$el.find('input[type="checkbox"]').prop('checked', false);


            }
        },
        */
        //form
        _.defaults({
            title: 'Description',
            property: 'remark',
            default_value: ""
        }, Backbone.UIToolBox.textarea)
    ];

    AD.columns = [
        //columns
        /*
        {
            title: '<i class="fa fa-lock"></i>',
            property: 'isDefault',
            cellClassName: 'isDefault',
            filterable: true,
            sortable: true,
            callback: function(o) {
                // if (o['isLock']==='Y')
                    return '<i class="fa fa-lock"></i>';
                // else
                //     return '<i class="fa fa-unlock-o"></i>';
            }
        },
        */
/*                    {
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            sortable: false,
            width: '50px',
            callback: function(o) {
                if (!o.isDefault) {
                    return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
                }
            }
        }, */
        // {
        //     title: 'Id',
        //     property: 'id',
        //     sortable: true
        // },
        //columns
        {
            title: 'ns:Domain.Title.Name',
            property: 'name',
            filterable: false,
            sortable: true,
            callback: function(o){
                return '<i class="fa fa-sitemap"></i><a href="#ajax/domain_detail.html/' + o.id + '"> '+ o.name + '</a>';
            }
        },
        //columns
        {
            title: 'ns:Domain.Title.Devices',
            property: 'numberOfDevice',
            filterable: true,
            sortable: false,
        },
        //columns
        {
            title: 'ns:Domain.Title.Users',
            property: 'numberOfUser',
            filterable: true,
            sortable: false,
            callback: function(o) {
                return '<a href="#ajax/user_list.html/domain_user_list/' + o.id + '">' +'<i class=\"fa fa-users\" style="text-decoration: none; color:black;"></i> '+ o.numberOfUser + '</a>';
            }
        },
        //columns
        // {
        //     title: 'Send Error Message',
        //     property: 'isSendErrorMessage',
        //     cellClassName: 'isSendErrorMessage',
        //     filterable: true,
        //     sortable: true,
        //     callback: function(o) {
        //      var _html = '';

        //      if(o['isSendErrorMessage']) {
        //          _html+='Yes';
        //      }
        //      else  {
        //          _html+='No';
        //      }
        //      return _html;
        //     }
        // },

        //columns
        {
            title: 'Description',
            property: 'remark',
            filterable: true,
            sortable: false
        },
        //columns
        {
            title: 'ns:Domain.Title.Action',
            sortable: false,
            callback: function(o) {

                var _html = '';
                if (o.id != $.login.user.domainId) {
                    if (o) {
                        _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                        if ($.common.isDelete)
                            _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                    }
                }
                else{
                    if (o) {
                            _html += '<a disabled="disabled" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                            if ($.common.isDelete)
                                _html += '<a disabled="disabled" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                    }   
                }
                return _html;
            }
        }
    ];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */
    var routes = {};

    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/edit/:_id'] = 'domain_edit';
    routes[AD.page + '/add'] = 'domain_add';
    routes[AD.page + '/delete/:_id'] = 'act_delete'; 
    routes[AD.page] = 'render';
    routes[AD.page + '/devices/:_id'] = 'show_devices';
    routes[AD.page + '/users/:_id'] = 'show_devices';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        show_devices: function(_id) {
            // alert()
            $("#myModal").modal('show');
            pageSetUp();
        },
        show_users: function(_id) {
            // alert()
            $("#myModal").modal('show');
            pageSetUp();
        },
        domain_add: function() {
            this.navigate("#ajax/domain_add.html", {
                trigger: true
            });
        },
        domain_edit: function(_id) {
            this.navigate("#ajax/domain_edit.html/" + _id, {
                trigger: true
            });
        },
        act_delete: function(id) {
            //event.preventDefault();
            var t = this;             
            
            $.SmartMessageBox({
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; " + i18n.t("ns:Message.Domain.DeleteDomain") + " <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> <font>" + i18n.t("ns:Message.Domain.DeleteDomainContent") + "</font></span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'
                // buttons: '[No][Yes]'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                    $("body").addClass("loading");

                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: $.config.server_rest_url + '/domains/' + id,
                        dataType: 'json',
                        timeout: 10000,
                        error: function(jqXHR, textStatus, errorThrown) {
                            console.log("[Domain] Deleted failed");
                        } 
                    }).done(function(data){
                        if (typeof(data) === 'undefined'){
                            console.log("[Domain] Deleted done => response undefined.");
                        } else {
                            if (data.status) {
                                $.smallBox({
                                    title: i18n.t("ns:Domain.Domain") + i18n.t("ns:Domain.DeleteSuccess"),
                                    content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Domain.DeleteDomain") + "</i>",
                                    // title: "[Domain] Deleted successfully",
                                    // content: "<i class='fa fa-times'></i> <i>Deleted Domain</i>",
                                    color: "#648BB2",
                                    iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                            } else {                                            
                                $.smallBox({
                                    title: i18n.t("ns:Domain.Domain") + i18n.t("ns:Domain.DeleteFailed"),
                                    content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Domain.DeleteDomain") + "</i>",
                                    // title: "[Domain] Deleted failed",
                                    // //content: "<i class='fa fa-times'></i> <i>Deleted App ID: &nbsp;" + id + "</i>",
                                    // content: "<i class='fa fa-times'></i> <i>Deleted Domain</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                console.log("[Domain] Deleted failed => " + JSON.stringify(data));
                            };
                        } 
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Domain.Domain") + i18n.t("ns:Domain.DeleteFailed"),
                            content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Domain.DeleteDomain") + "</i>",
                            // title: "[Domain] Deleted failed",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Domain</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                        console.log("[Domain] Deleted failed"); 
                    }).always(function() {
                        $("body").removeClass("loading"); 
                        // window.location.href = "#ajax/app_list.html";
                        t.navigate("#ajax/domain_list.html/grid", {
                            trigger: true
                        });
                    });
                } else {
                    // window.location.href = "#ajax/app_list.html";
                    t.navigate("#ajax/domain_list.html/grid", {
                        trigger: true
                    });
                }
            });
        }                    
        // show_edit: function(_id) {
        //  alert('hello?');
        // }
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
                    
        $("body").removeClass("loading");
        // alert('bootstrap');
        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/grid", {
                trigger: true
            });
        }


        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();


})(jQuery);
