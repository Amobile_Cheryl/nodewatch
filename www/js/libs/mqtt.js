//=====================================================================================================================
// Variables
//=====================================================================================================================
var mqtt = {};
mqtt.isBrokerConnected = false;
mqtt.clinet = null;
mqtt.forcusDevice = null;
mqtt.focus = {};
mqtt.focus.groupId = 0;

var data = {
    messages: []
};

new Vue(
{
    el: '#chat',
    data: data
});
//=====================================================================================================================
// Functions
//=====================================================================================================================
var checkMqttConnection = function()
{
    // var string = (mqtt.isBrokerConnected === true) ? "連線中" : "已斷線";
    // console.log("[MQTT] 連線檢查 (" + string + ")");
    if (mqtt.isBrokerConnected) return;
    // console.log("[MQTT] Try to reconnect to Broker ...");
    connectMqttBroker();
}

var connectMqttBroker = function()
{
    //console.log("[MQTT] connectMqttBroker (" + mqtt.isBrokerConnected + ")");
    console.log("[MQTT] Start connecting to broker ...");
    var endpoint = createEndpoint(
        'ap-southeast-1', // Your Region
        'a2rnihbj6hafk5.iot.ap-southeast-1.amazonaws.com', // Require 'lowercamelcase'!!
        'AKIAIFDEQQK2W4FDYMUQ',
        'OQnNNxUyw97ydDCzQzM4w1H5g57RD7OfrxZqWU51');
    var clientTmp = new Paho.MQTT.Client(endpoint, Math.random().toString(36).substring(7));
    var connectOptions = {
        useSSL: true,
        timeout: 12,
        mqttVersion: 4,
        keepAliveInterval: 15,
        onSuccess: onMqttConnectSuccess,
        onFailure: onMQttConnectFailure
    };
    clientTmp.connect(connectOptions);
    clientTmp.onMessageArrived = onMqttMessageArrived;
    clientTmp.onConnectionLost = onMQttConnectionLost;
    mqtt.client = clientTmp;
}

function SigV4Utils()
{}

SigV4Utils.sign = function(key, msg)
{
    var hash = CryptoJS.HmacSHA256(msg, key);
    return hash.toString(CryptoJS.enc.Hex);
};

SigV4Utils.sha256 = function(msg)
{
    var hash = CryptoJS.SHA256(msg);
    return hash.toString(CryptoJS.enc.Hex);
};

SigV4Utils.getSignatureKey = function(key, dateStamp, regionName, serviceName)
{
    var kDate = CryptoJS.HmacSHA256(dateStamp, 'AWS4' + key);
    var kRegion = CryptoJS.HmacSHA256(regionName, kDate);
    var kService = CryptoJS.HmacSHA256(serviceName, kRegion);
    var kSigning = CryptoJS.HmacSHA256('aws4_request', kService);
    return kSigning;
};

function createEndpoint(regionName, awsIotEndpoint, accessKey, secretKey)
{
    var time = moment.utc();
    var dateStamp = time.format('YYYYMMDD');
    var amzdate = dateStamp + 'T' + time.format('HHmmss') + 'Z';
    var service = 'iotdevicegateway';
    var region = regionName;
    var secretKey = secretKey;
    var accessKey = accessKey;
    var algorithm = 'AWS4-HMAC-SHA256';
    var method = 'GET';
    var canonicalUri = '/mqtt';
    var host = awsIotEndpoint;

    var credentialScope = dateStamp + '/' + region + '/' + service + '/' + 'aws4_request';
    var canonicalQuerystring = 'X-Amz-Algorithm=AWS4-HMAC-SHA256';
    canonicalQuerystring += '&X-Amz-Credential=' + encodeURIComponent(accessKey + '/' + credentialScope);
    canonicalQuerystring += '&X-Amz-Date=' + amzdate;
    canonicalQuerystring += '&X-Amz-SignedHeaders=host';

    var canonicalHeaders = 'host:' + host + '\n';
    var payloadHash = SigV4Utils.sha256('');
    var canonicalRequest = method + '\n' + canonicalUri + '\n' + canonicalQuerystring + '\n' + canonicalHeaders + '\nhost\n' + payloadHash;

    var stringToSign = algorithm + '\n' + amzdate + '\n' + credentialScope + '\n' + SigV4Utils.sha256(canonicalRequest);
    var signingKey = SigV4Utils.getSignatureKey(secretKey, dateStamp, region, service);
    var signature = SigV4Utils.sign(signingKey, stringToSign);

    canonicalQuerystring += '&X-Amz-Signature=' + signature;
    return 'wss://' + host + canonicalUri + '?' + canonicalQuerystring;
}

function send(content)
{
    var message = new Paho.MQTT.Message(content);
    message.destinationName = "amobile";
    mqtt.client.send(message);
    console.log("sent topic=[" + message.destinationName + "] message=[" + content + "]");
}

function subscribe()
{
    // console.log("[MQTT] Broker Connected");
    //client.subscribe("amobile/#");
    mqtt.client.subscribe("MqttTest");
    mqtt.client.subscribe(CoreServerName + "/website/device/android/appChanged");
    mqtt.client.subscribe(CoreServerName + "/website/device/android/gps");
    mqtt.client.subscribe(CoreServerName + "/website/device/android/battery/powerPlugged");
    mqtt.client.subscribe(CoreServerName + "/website/device/android/battery/powerUnplugged");
    mqtt.client.subscribe(CoreServerName + "/website/dashboard/deviceCountChanged");
    mqtt.client.subscribe(CoreServerName + "/website/dashboard/taskCountChanged");
    mqtt.client.subscribe(CoreServerName + "/website/dashboard/fileCountChanged");
    mqtt.client.subscribe(CoreServerName + "/website/dashboard/onlineUserCountChanged");
    console.log("[MQTT] Topic (" + CoreServerName + ") Subscribed");    
    /*
        var subscribeOptions = {
        qos: 0,  // QoS
        invocationContext: {foo: true},  // Passed to success / failure callback
        onSuccess: onSuccessCallback,
        onFailure: onFailureCallback,
        timeout: 10
    };
    client.subscribe(“topic”, subscribeOptions);
    */
}

function subscribeForDevice(deviceId)
{
    if(mqtt.forcusDevice !== deviceId)
    {
        if(mqtt.forcusDevice !== null)
        {
            //var topicForUnsubscribe = CoreServerName + "/website/D" + mqtt.forcusDevice + "/appChanged";
           // mqtt.client.unsubscribe(topicForUnsubscribe);
            var topicForUnsubscribe = CoreServerName + "/website/D" + mqtt.forcusDevice + "/taskChanged";
            // mqtt.client.unsubscribe(topicForUnsubscribe);
            console.log("[MQTT] Topic (" + topicForUnsubscribe + ") unsubscribed");
        }
        //var topic = CoreServerName + "/website/D" + deviceId + "/appChanged";
        //mqtt.client.subscribe(topic);
        var topic = CoreServerName + "/website/D" + deviceId + "/taskChanged";
        // mqtt.client.subscribe(topic);
        console.log("[MQTT] Topic (" + topic + ") subscribed");
        mqtt.forcusDevice = deviceId;
    } else {
        console.log("[MQTT] Topic nothing need change");
    };
}

function subscribeForGroup(sGroupId)
{
    //console.log("[MQTT] mqtt.focus.groupId=["+typeof(mqtt.focus.groupId)+"]");
    //console.log("[MQTT] groupId=["+typeof(sGroupId)+"]");
    var groupId = parseInt(sGroupId);
    //console.log("[MQTT] groupId=["+typeof(groupId)+"]");
    if(mqtt.focus.groupId !== groupId)
    {
        if(mqtt.focus.groupId !== 0)
        {
            var topicForUnsubscribe = CoreServerName + "/website/G" + mqtt.focus.groupId + "/taskChanged";
            // mqtt.client.unsubscribe(topicForUnsubscribe);
            console.log("[MQTT] Topic (" + topicForUnsubscribe + ") unsubscribed");
        }
        var topic = CoreServerName + "/website/G" + groupId + "/taskChanged";
        // mqtt.client.subscribe(topic);
        console.log("[MQTT] Topic (" + topic + ") subscribed");
        mqtt.focus.groupId = groupId;
    }
    else
    {
        console.log("[MQTT] Topic nothing need change");
    };
}

function onMqttConnectSuccess(responseObjectParameter)
{
    console.log("[MQTT] Broker Connected (OK)");
    mqtt.isBrokerConnected = true;
    subscribe();
}

function onMQttConnectFailure(responseObjectParameter)
{
    console.log(responseObjectParameter);
    console.error("[MQTT] Connection to broker failed, check initial parameters.");
}

function onMQttConnectionLost(e)
{
    mqtt.isBrokerConnected = false;
    console.log(e);
    console.error("[MQTT] Broker connection is lost !!");
}

function onMqttMessageArrived(message)
{
    data.messages.push(message.payloadString);
    console.log("[MQTT] Message received. Topic (" + message.destinationName + "), Content (" + message.payloadString + "), QoS (" + message.qos + "), Retained (" + message.retained + "), Duplicate(" + message.duplicate + ")");
    // Read Only, set if message might be a duplicate sent from broker
    // console.log("Duplicate: " + message.duplicate);
    // console.log("sessionStorage=" + sessionStorage.currentDeviceName);

    // if ($("#status0").length > 0)
    // {
    //     console.log("[MQTT] device=" + $("#status0").text());
    //     $("#status0").text("5550");
    // }
    // $("#status1").text("5551");
    // $("#status2").text("5552");
    // $("#status3").text("5553");

    // if ($("#plug").length > 0)
    // {
    //     console.log("[MQTT] Found plug");
    //     console.log("[MQTT] device=" + device_info.id);

    //     console.log("[MQTT] Found plug");

    //     chargingStatus = " <i class='fa fa-plug' style=\"color:green;font-size:16px\"></i>";
    //     $("#plug").show();
    // }

    var simpleTopic = message.destinationName.substr(message.destinationName.indexOf("/") + 1);
    switch(simpleTopic)
    {
        //---[ Message from device ]-----------------------------------------------------------------------------------[START]
        /*
        case "website/device/android/appBlocked":
            var jsonMessage = $.parseJSON(message.payloadString);
            if($("#inventory_scan").length > 0 && sessionStorage.currentDeviceName === jsonMessage.name)
            {
                Backbone.Events.trigger("AppBlocked", jsonMessage);
                console.log("[MQTT] Add block icon");
            } else {
                console.log("[MQTT] Message discarded");
            }
            break;
        */    
        case "website/device/android/appChanged":
            var jsonMessage = $.parseJSON(message.payloadString);
            if($("#inventory_scan").length > 0 && sessionStorage.currentDeviceName === jsonMessage.name)
            {
                $("#inventory_scan").click();
                console.log("[MQTT] Do app inventory scan");
            }
            break;
        case "website/device/android/gps":
            var jsonMessage = $.parseJSON(message.payloadString);

            //if ($("#refresh-gps").length > 0 && sessionStorage.currentDeviceName === jsonMessage.name)
            if($("#refresh-gps").length > 0)
            {
                $("#refresh-gps").click();
                console.log("[MQTT] Do device summary page change");
            }
            break;
        case "website/device/android/battery/powerPlugged":
            var jsonMessage = $.parseJSON(message.payloadString);
            if($("#refresh-gps").length > 0 && sessionStorage.currentDeviceName === jsonMessage.name)
            {
                var chargingStatus = "State: Charging" + " <i class=\"fa fa-plug\" style=\"color:green;font-size:16px\"></i>";
                $("#divDeviceBatteryChargeStatus").html(chargingStatus);
                console.log("[MQTT] Do device bettery Plugged change");
            }
            break;
        case "website/device/android/battery/powerUnplugged":
            var jsonMessage = $.parseJSON(message.payloadString);
            if ($("#refresh-gps").length > 0 && sessionStorage.currentDeviceName === jsonMessage.name)
            {
                var chargingStatus = "State: Not charging" + " <i class=\"fa fa-bolt\" style=\"color:orange;font-size:16px\"></i>";
                $("#divDeviceBatteryChargeStatus").html(chargingStatus);
                console.log("[MQTT] Do device bettery Unplugged change");
            }
            break;
            //---[ Message from device ]-----------------------------------------------------------------------------------[END]
            //---[ Message from server ]-----------------------------------------------------------------------------------[START]
        case "MqttTest":
            if($("#status0").length > 0)
            {
                setTimeout("fetchTaskCount()", 2000);
                console.log("[MQTT] Do dashboard page taskCountChanged");
            }
            break;
        case "website/dashboard/deviceCountChanged":
            if($("#status0").length > 0)
            {
                setTimeout("fetchDeviceCount()", 2000);
                console.log("[MQTT] Do dashboard page deviceCountChanged");
            }
            break;
        case "website/dashboard/taskCountChanged":
            if($("#status0").length > 0)
            {
                setTimeout("fetchTaskCount()", 2000);
                console.log("[MQTT] Do dashboard page taskCountChanged");
            }
            break;
        case "website/dashboard/fileCountChanged":
            if($("#status0").length > 0)
            {
                setTimeout("fetchFileCount()", 2000);
                console.log("[MQTT] Do dashboard page fileCountChanged");
            }
            break;
        case "website/dashboard/onlineUserCountChanged":
            Backbone.Events.trigger("UserLogUpdate");
            if($("#status0").length > 0)
            {
                setTimeout("fetchOnlineUserCount()", 2000);
                Backbone.Events.trigger("UserLogUpdate");
                console.log("[MQTT] Do dashboard page onlineUserCountChanged");
            }
            break;
        case "website/D" + mqtt.forcusDevice + "/taskChanged":
            if($("#s5").length > 0)
            {                
                Backbone.Events.trigger("DeviceTaskUpdate");
                console.log("[MQTT] Refresh Page for Device Management.Device.Task");
            }
            break;
        case "website/G" + mqtt.focus.groupId + "/taskChanged":
            if($("#s5").length > 0)
            {                
                Backbone.Events.trigger("TaskUpdate");
                console.log("[MQTT] Refresh Page for Device Management.Group.Task");
            }
            break;
        /*    
        case "website/D" + mqtt.forcusDevice + "/appChanged":            
            if($("#inventory_scan").length > 0 )
            {
                $("#inventory_scan").click();
                console.log("[MQTT] Do app inventory scan");
            }
            break;
        */       
            //---[ Message from server ]-----------------------------------------------------------------------------------[END]
        default:
            console.log("[MQTT] Message discarded");
            break;
    }

    // 有用，但整個畫面更新有點頓
    //location.reload();
}

function fetchFileCount()
{
    $.ajax(
    {
        type: 'GET',
        url: $.config.server_rest_url + '/windowsAppFiles?isCount=true',
        dataType: 'json',
        timeout: 20000,
        error: function(data, status, error)
        {
            console.log("[Dashboard] Got windows apps info failed. => " + data.responseText);
        },
        success: function(windowsApps)
        {
            $.ajax(
            {
                type: 'GET',
                url: $.config.server_rest_url + '/androidAppFiles?isCount=true',
                dataType: 'json',
                timeout: 20000,
                error: function(data, status, error)
                {
                    console.log("[Dashboard] Got android apps info failed. => " + data.responseText);
                },
                success: function(androidApps)
                {
                    $.ajax(
                    {
                        type: 'GET',
                        url: $.config.server_rest_url + '/firmwareFiles?isCount=true',
                        dataType: 'json',
                        timeout: 20000,
                        error: function(data, status, error)
                        {
                            console.log("[Dashboard] Got firmware apps info failed. => " + data.responseText);
                        },
                        success: function(firmwareFiles)
                        {
                            $.ajax(
                            {
                                type: 'GET',
                                url: $.config.server_rest_url + '/omaAgentFiles?isCount=true',
                                dataType: 'json',
                                timeout: 20000,
                                error: function(data, status, error)
                                {
                                    console.log("[Dashboard] Got agent files info failed. => " + data.responseText);
                                },
                                success: function(omaAgentFiles)
                                {
                                    var appCnt = windowsApps.amount + androidApps.amount + firmwareFiles.amount + omaAgentFiles.amount;
                                    $("#status2").text(appCnt);
                                    // jQuery(
                                    // {
                                    //     Counter: 0
                                    // }).animate(
                                    // {
                                    //     Counter: appCnt
                                    // },
                                    // {
                                    //     duration: 3,
                                    //     easing: 'swing',
                                    //     step: function()
                                    //     {
                                    //         $("#status2").text(Math.ceil(this.Counter));
                                    //     }
                                    // });
                                }
                            });
                        }
                    });
                }
            });
        }
    });
}

function fetchOnlineUserCount()
{
    $.ajax(
    {
        type: 'GET',
        url: $.config.server_rest_url + '/onlineUserCount',
        dataType: 'json',
        timeout: 20000,
        error: function(data, status, error)
        {
            console.log("[Dashboard] Got users info failed. => " + data.responseText);
        },
        success: function(status)
        {
            $("#status3").text(Math.ceil(status.amount));
            // jQuery(
            // {
            //     Counter: 0
            // }).animate(
            // {
            //     Counter: status.amount
            // },
            // {
            //     duration: 3,
            //     easing: 'swing',
            //     step: function()
            //     {
            //         $("#status3").text(Math.ceil(this.Counter));
            //     }
            // });
        }
    });
}

function fetchDeviceCount()
{
    $.ajax(
    {
        type: 'GET',
        url: $.config.server_rest_url + '/devices?isCount=true',
        dataType: 'json',
        timeout: 20000,
        error: function(data, status, error)
        {
            console.log("[Dashboard] Got devices info failed. => " + data.responseText);
        },
        success: function(status)
        {
            $("#status0").text(Math.ceil(status.amount));
            // jQuery(
            // {
            //     Counter: 0
            // }).animate(
            // {
            //     Counter: status.amount
            // },
            // {
            //     duration: 3,
            //     easing: 'swing',
            //     step: function()
            //     {
            //         $("#status0").text(Math.ceil(this.Counter));
            //     }
            // });
        }
    });
}

function fetchTaskCount()
{
    $.ajax(
    {
        type: 'GET',
        url: $.config.server_rest_url + '/tasks?isCount=true',
        dataType: 'json',
        timeout: 20000,
        error: function(data, status, error)
        {
            console.log("[Dashboard] Got tasks info failed. => " + data.responseText);
        },
        success: function(status)
        {
            $("#status1").text(Math.ceil(status.amount));
            // jQuery(
            // {
            //     Counter: 0
            // }).animate(
            // {
            //     Counter: status.amount
            // },
            // {
            //     duration: 300,
            //     easing: 'swing',
            //     step: function()
            //     {
            //         $("#status1").text(Math.ceil(this.Counter));
            //     }
            // });
        }
    });
}

/*
 * 频率控制 返回函数连续调用时，fn 执行频率限定为每多少时间执行一次
 * @param fn {function}  需要调用的函数
 * @param delay  {number}    延迟时间，单位毫秒
 * @param immediate  {bool} 给 immediate参数传递false 绑定的函数先执行，而不是delay后执行。
 * @return {function}实际调用函数
 */
var throttle = function(fn, delay, immediate, debounce)
{
    var curr = +new Date(), //当前事件
        last_call = 0,
        last_exec = 0,
        timer = null,
        diff, //时间差
        context, //上下文
        args,
        exec = function()
        {
            last_exec = curr;
            fn.apply(context, args);
        };
    return function()
    {
        curr = +new Date();
        context = this,
            args = arguments,
            diff = curr - (debounce ? last_call : last_exec) - delay;
        clearTimeout(timer);
        if(debounce)
        {
            if(immediate)
            {
                timer = setTimeout(exec, delay);
            }
            else if(diff >= 0)
            {
                exec();
            }
        }
        else
        {
            if(diff >= 0)
            {
                exec();
            }
            else if(immediate)
            {
                timer = setTimeout(exec, -diff);
            }
        }
        last_call = curr;
    }
};

/*
 * 空闲控制 返回函数连续调用时，空闲时间必须大于或等于 delay，fn 才会执行
 * @param fn {function}  要调用的函数
 * @param delay   {number}    空闲时间
 * @param immediate  {bool} 给 immediate参数传递false 绑定的函数先执行，而不是delay后后执行。
 * @return {function}实际调用函数
 */
var debounce = function(fn, delay, immediate)
{
    return throttle(fn, delay, immediate, true);
};
//=====================================================================================================================
// Codes
//=====================================================================================================================
// connectMqttBroker();
// mqtt.timer = window.setInterval(checkMqttConnection, 5000);


