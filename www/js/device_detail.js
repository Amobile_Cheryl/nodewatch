(function($) {
    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    var device_info = {}; // REST: GET /device/{deviceId}
    var mapReady = false;
    // Holisun replace origianl string google with amap
    var sMapSelected = "Google Maps"; // Google Maps | AMap

    var AD = {};
    AD.page = "ajax/device_detail.html";

    var gMap;
    var gMarker = null;
    var contentString;
    var amMap;
    var aMarker = null;
    var taskData; // Double Comet Issue
    var isLocationEnabled = false;

    var metro_style = [{
        "featureType": "transit",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#d3d3d3"
        }, {
            "visibility": "on"
        }]
    }, {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "landscape",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#eee8ce"
        }]
    }, {
        "featureType": "water",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#b8cec9"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels.text.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#000000"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels.text.stroke",
        "stylers": [{
            "visibility": "off"
        }, {
            "color": "#ffffff"
        }]
    }, {
        "featureType": "administrative",
        "elementType": "labels.text.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#000000"
        }]
    }, {
        "featureType": "road",
        "elementType": "geometry.fill",
        "stylers": [{
            "visibility": "on"
        }, {
            "color": "#ffffff"
        }]
    }, {
        "featureType": "road",
        "elementType": "geometry.stroke",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "road",
        "elementType": "labels.icon",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "water",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }, {
        "featureType": "poi",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#d3cdab"
        }]
    }, {
        "featureType": "poi.park",
        "elementType": "geometry.fill",
        "stylers": [{
            "color": "#ced09d"
        }]
    }, {
        "featureType": "poi",
        "elementType": "labels",
        "stylers": [{
            "visibility": "off"
        }]
    }];
    var metroStyleMap;
    $(function() {
        try {
            metroStyleMap = new google.maps.StyledMapType(metro_style, {
                name: "Metro"
            });
        } catch (err) {
            // mapReady = true;
            console.log("Map err : " + err);
        }

    });



    function fnRefreshMap(deviceLocation) {

        if (!_.isUndefined(gMap)) {
            try {
                var lat = parseFloat(deviceLocation.Latitude);
                var lng = parseFloat(deviceLocation.Longitude);
                console.log("lat : " + lat + " lng : " + lng);
                if (isLocationEnabled == true && lat >= -90 && lat <= 90 && lng >= -180 && lng <= 180) {
                    var latlng = new google.maps.LatLng(lat, lng);
                    gMap.setCenter(latlng);

                    if (gMarker === null) {
                        gMarker = new google.maps.Marker({
                            position: latlng,
                            map: gMap
                        });
                    } else {
                        gMarker.setMap(gMap);
                    }

                    var infowindow = new google.maps.InfoWindow();
                    google.maps.event.addListener(gMarker, 'click', (function(gMarker) {
                        return function() {
                            infowindow.setContent(contentString);
                            infowindow.open(gMap, gMarker);
                        }
                    })(gMarker));

                    if (aMarker === null) {
                        aMarker = new AMap.Marker({
                            position: [lng, lat]
                        });
                    } else {
                        aMarker.setMap(amMap);
                    }

                    var oAMapInfoWindow = new AMap.InfoWindow();

                    aMarker.on('click', function(e) {
                        oAMapInfoWindow.setContent(contentString);
                        oAMapInfoWindow.open(amMap, e.target.getPosition());
                    })
                }
            } catch (err) {
                console.log(err.message);
            }
        } else {
            fakeImage();
        }
    }

    function fakeImage() {
        var fakeGmapImg = "img/test_img/fake_map_1.png";
        var fakeAmapImg = "img/test_img/fake_map_2.png";
        if (sMapSelected == "AMap") {
            if (document.getElementById('fakeimg_amap')) {
                $('#fakeimg_amap').attr("src", fakeAmapImg);
            } else {
                $('#amap').prepend('<img id="fakeimg_amap" src=' + fakeAmapImg + ' style="width:100%;height:100%"/>')
            }
        } else {
            if (document.getElementById('fakeimg_map')) {
                $('#fakeimg_map').attr("src", fakeGmapImg);
            } else {
                $('#map').prepend('<img id="fakeimg_map" src=' + fakeGmapImg + ' style="width:100%;height:100%"/>')
            }
        }
    }

    function s1() {
        subscribeForDevice(device_info.id);
        var id = device_info.id;
        device_info.ip = device_info.ip || 'Unknown';
        device_info.holder = device_info.holder || 'Unknown';
        Handlebars.registerHelper("safeString", function(isRoot) {
            var isGetRoot = "<i class=\"fa fa-bug\" style=\"color:ghostwhite;font-size:24px;\" title=\"" + i18n.t("ns:Message.Device.Jailbreak") + "\"></i>";
            if (!_.isUndefined(isRoot)) {
                if (isRoot) {
                    isGetRoot = "<i class=\"fa fa-bug\" style=\"color:crimson;font-size:24px;\" title=\"" + i18n.t("ns:Message.Device.Jailbreak") + "\"></i>";
                }
            }
            return new Handlebars.SafeString(isGetRoot);
        });

        (device_info.isLock == true) ? device_info.lockStatusIcon = "img/status_lock.png": device_info.lockStatusIcon = "img/status_unlock.png";
        (device_info.isOnline == true) ? device_info.onlineStatusIcon = "img/status_online.png": device_info.onlineStatusIcon = "img/status_offline.png";

        var source = $("#tpl_device_info").html();
        var template = Handlebars.compile(source);
        var html = template(device_info);
        $("#device_info").html(html);
        $('#rdToggle').on("click", function(event) {
            console.log('check#1)');
            event.preventDefault();
            if ($(this).prop("checked"))
                startRemoteDesktop(id);
            else
                stopRemoteDesktop(id);
        });

        $("#deviceTypeName").text(i18n.t("ns:Device.DeviceType"));
        $("#lastConnectionTime").text(i18n.t("ns:Device.LastConnectionTime"));
        //
        fnGetDeviceSummary(device_info.id);

        if (mapReady == false) {
            mapReady = true;
            gMap = useIpToFindLocation();
            // amap();
        }

        pageSetUp();
    };

    function fnGetDeviceSummary(iDeviceId) {
        var capabilities = ['Hardware', 'Battery', 'OS', 'InternalMemory', 'LocationService'];
        for (var key in capabilities) {

            var url = $.config.server_rest_url + '/devices/' + iDeviceId + '/capabilities/' + capabilities[key];
            $.ajax({
                    url: url,
                    type: 'GET',
                    dataType: 'json',
                    timeout: 10000,
                    error: function(data, status, error) {
                        //alert("Server busy, please try again!");
                    }
                })
                .done(function(data) {
                    var device_detail_data = data;
                    $("#" + device_detail_data.id.toLowerCase() + "-detail").empty();
                    if (device_detail_data.id == 'Hardware') {
                        for (var i in device_detail_data.nodeBeanList) {
                            var hwInfo = '';
                            device_detail_data.nodeBeanList[i].value = device_detail_data.nodeBeanList[i].value || '--';
                            hwInfo = '<p id="' + device_detail_data.nodeBeanList[i].name + '">' + device_detail_data.nodeBeanList[i].name + ": " + device_detail_data.nodeBeanList[i].value + '</p>';
                            $("#" + device_detail_data.id.toLowerCase() + "-detail").append(hwInfo);
                            $("#" + device_detail_data.nodeBeanList[i].name).jTruncate({ length: 14, minTrail: 6, moreAni: "fast", lessAni: "fast" });
                        }
                        /*$.each(device_detail_data.dmdb, function(k, v) {
                            console.log(k + ' is ' + v);
                        });*/
                    } else if (device_detail_data.id == 'OS') {
                        for (var i in device_detail_data.nodeBeanList) {
                            if (device_detail_data.nodeBeanList[i].name == 'OS_Name') {
                                var os = device_detail_data.nodeBeanList[i].value;
                                var os_img = "unknown-128.png";
                                if (device_info.operatingSystemName.indexOf("Android") >= 0) {
                                    os_img = "android-128.png";
                                } else if (device_info.operatingSystemName.indexOf("Windows") >= 0) {
                                    os_img = "windows-128.png";
                                }
                                $("#os_bg").css({ "background": "url(img/device_info/" + os_img + ") left top no-repeat" });

                                var osInfo = '';
                                osInfo += '<p id="' + device_detail_data.nodeBeanList[i].name + '">OS Version: ' + device_detail_data.nodeBeanList[i].value + '</p>';
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(osInfo);
                                $("#" + device_detail_data.nodeBeanList[i].name).jTruncate({ length: 14, minTrail: 6, moreAni: "fast", lessAni: "fast" });
                            } else if (device_detail_data.nodeBeanList[i].name == 'DMClientVersion') {
                                var agentInfo = '';
                                agentInfo += '<p id="' + device_detail_data.nodeBeanList[i].name + '">Agent: ' + device_detail_data.nodeBeanList[i].value + '</p>';
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(agentInfo);
                                $("#" + device_detail_data.nodeBeanList[i].name).jTruncate({ length: 14, minTrail: 6, moreAni: "fast", lessAni: "fast" });
                            } else if (device_detail_data.nodeBeanList[i].name == 'BootloaderVersion') {
                                var blValue = device_detail_data.nodeBeanList[i].value;
                                if (blValue === "unknown")
                                    blValue = "--";

                                var blInfo = '';
                                blInfo = '<p id="' + device_detail_data.nodeBeanList[i].name + '">BootLdr: ' + blValue + '</p>';
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(blInfo);
                                $("#" + device_detail_data.nodeBeanList[i].name).jTruncate({ length: 14, minTrail: 6, moreAni: "fast", lessAni: "fast" });
                            } else if (device_detail_data.nodeBeanList[i].name == 'OS_Version') {
                                var ovInfo = '<p id="' + device_detail_data.nodeBeanList[i].name + '">Firmware: ' + device_detail_data.nodeBeanList[i].value + '</p>';
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(ovInfo);
                                $("#" + device_detail_data.nodeBeanList[i].name).jTruncate({ length: 12, minTrail: 6, moreAni: "fast", lessAni: "fast" });
                            } else if (device_detail_data.nodeBeanList[i].name == 'Rooted') {
                                var value = "No";
                                if (device_detail_data.nodeBeanList[i].value === "true")
                                    value = "Yes";

                                var ovInfo = '<p id="' + device_detail_data.nodeBeanList[i].name + '">Rooted: ' + value + '</p>';
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(ovInfo);
                                $("#" + device_detail_data.nodeBeanList[i].name).jTruncate({ length: 12, minTrail: 6, moreAni: "fast", lessAni: "fast" });
                            } else {
                                miscInfo = '<p id="' + device_detail_data.nodeBeanList[i].name + '">' + device_detail_data.nodeBeanList[i].name + ": " + device_detail_data.nodeBeanList[i].value + "</p>";
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(miscInfo);
                                $("#" + device_detail_data.nodeBeanList[i].name).jTruncate({ length: 14, minTrail: 6, moreAni: "fast", lessAni: "fast" });
                            }
                        }
                    } else if (device_detail_data.id == 'Battery') {
                        for (var i in device_detail_data.nodeBeanList) {
                            if (device_detail_data.nodeBeanList[i].name == 'BatteryLevel') {
                                var battery_quantity = device_detail_data.nodeBeanList[i].value.replace("%", "");
                                var battery_color = "#26C039";
                                switch (true) {
                                    case (battery_quantity > 20 && battery_quantity <= 60):
                                        battery_color = "#E1920A";
                                        break;
                                    case (battery_quantity <= 20):
                                        battery_color = "#FF3333";
                                        break;
                                    default:
                                        break;
                                }
                                $(".battery-quantity").css({ "background-color": battery_color });

                                var bTemp = "";
                                if (battery_quantity === '--' || battery_quantity.length == 0) {
                                    $("#battery-currently-quantity").css({ "height": "100%", "text-align": "center", "line-height": "50px" });
                                    bTemp = "<p>" + "Level: 100%</p>";
                                } else {
                                    $("#battery-currently-quantity").css({ "height": (100 - battery_quantity) + "%", "text-align": "center", "line-height": "50px" });
                                    bTemp = "<p>" + "Level: " + device_detail_data.nodeBeanList[i].value.replace(/[^0-9.]+/, "") + "%</p>";
                                }

                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(bTemp);
                            } else if (device_detail_data.nodeBeanList[i].name == 'BatteryBeingCharged') {
                                var chargingStatus = "";
                                if (device_detail_data.nodeBeanList[i].value == 'Charging') {
                                    chargingStatus = " <i class=\"fa fa-plug\" style=\"color:green;font-size:16px\"></i>";
                                    $("#plug").show();
                                } else if (device_detail_data.nodeBeanList[i].value == 'Not charging') {
                                    chargingStatus = " <i class=\"fa fa-bolt\" style=\"color:orange;font-size:16px\"></i>";
                                } else {
                                    chargingStatus = "";
                                }
                                var bCharged = "<p id='divDeviceBatteryChargeStatus'>" + "State: " + device_detail_data.nodeBeanList[i].value + chargingStatus + "</p>";
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(bCharged);
                            } else if (device_detail_data.nodeBeanList[i].name == 'BatteryTemperature') {
                                var bTemp = "";
                                if (device_detail_data.nodeBeanList[i].value === 'Unknown' || device_detail_data.nodeBeanList[i].value.length == 0)
                                    bTemp += "<p>" + "Temp: --&deg;C</p>";
                                else
                                    bTemp += "<p>" + "Temp: " + device_detail_data.nodeBeanList[i].value.replace(/[^0-9.]+/, "") + "&deg;C" + "</p>";
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(bTemp);
                            } else {
                                miscInfo = '<p id="' + device_detail_data.nodeBeanList[i].name + '">' + device_detail_data.nodeBeanList[i].name + ": " + device_detail_data.nodeBeanList[i].value + "</p>";
                                $("#" + device_detail_data.id.toLowerCase() + "-detail").append(miscInfo);
                                $("#" + device_detail_data.nodeBeanList[i].name).jTruncate({ length: 14, minTrail: 6, moreAni: "fast", lessAni: "fast" });
                            }
                        }
                    } else if (device_detail_data.id == 'InternalMemory') {
                        var MemUsage = '';
                        var builtinTotalMemorySizeObj = _.findWhere(device_detail_data.nodeBeanList, { "name": "BuiltinTotalMemorySize" });
                        var builtinAvailableMemorySizeObj = _.findWhere(device_detail_data.nodeBeanList, { "name": "BuiltinAvailableMemorySize" });
                        var builtinTotalMemorySize = builtinTotalMemorySizeObj.value.replace(/[^\d]+/, "");
                        var builtinAvailableMemorySize = builtinAvailableMemorySizeObj.value.replace(/[^\d]+/, "")
                        if (!_.isEmpty(builtinTotalMemorySize) && !_.isEmpty(builtinAvailableMemorySize)) {
                            var availableMemoryPercent = Math.round((builtinAvailableMemorySize / builtinTotalMemorySize) * 100);
                            //var MemoryBar = "<div class=\"progress vertical bottom wide-bar\" style=\"width:70%;text-align:center;margin-left:10px;\">"
                            var MemoryBar = "<div class=\"progress right\" style=\"text-align:center;margin-left:10px;height: 40px;\">"
                                //+builtinTotalMemorySizeObj.value
                                //+"<div class=\"progress-bar bg-color-blueLight\" aria-valuetransitiongoal=\""+availableMemoryPercent+"\" style=\"height: "+availableMemoryPercent+"%;\" aria-valuenow=\""+availableMemoryPercent+"\">"+builtinAvailableMemorySizeObj.value+" ("+availableMemoryPercent+"%)</div>"
                                + "<div class=\"progress-bar bg-color-blueLight\" aria-valuetransitiongoal=\"" + availableMemoryPercent + "\" style=\"width: " + availableMemoryPercent + "%;line-height: 40px;\" aria-valuenow=\"" + availableMemoryPercent + "\">" + availableMemoryPercent + "%</div>" + "</div>"
                                //+builtinAvailableMemorySizeObj.value+"/"+builtinTotalMemorySizeObj.value;
                            MemUsage += MemoryBar;
                            MemUsage += "<p>" + "Total" + ": " + builtinTotalMemorySizeObj.value + "</p>";
                            MemUsage += "<p>" + "Available" + ": " + builtinAvailableMemorySizeObj.value + "</p>";
                        } else {
                            MemUsage += "<p>" + "Total" + ": " + builtinTotalMemorySizeObj.value + "</p>";
                            MemUsage += "<p>" + "Available" + ": " + builtinAvailableMemorySizeObj.value + "</p>";
                        }
                        $("#" + device_detail_data.id.toLowerCase() + "-detail").html(MemUsage);
                    } else if (device_detail_data.id == 'LocationService') {
                        if (device_detail_data.dcmoState == true) {
                            $("#locationEnabled").text("ON");
                            $("#locationEnabled").css("color", "green");
                            isLocationEnabled = true;
                        } else {
                            $("#locationEnabled").text("OFF");
                            $("#locationEnabled").css("color", "red");
                            isLocationEnabled = false;
                        }

                        $.getJSON($.config.server_rest_url + '/devices/' + iDeviceId + '/capabilities/GPS',
                            function(data) {
                                var deviceLocation = {};
                                for (var i = 0; i < data.nodeBeanList.length; i++)
                                    eval('deviceLocation.' + data.nodeBeanList[i].name + '="' + data.nodeBeanList[i].value + '"');
                                if (_.has(deviceLocation, 'Latitude') && _.has(deviceLocation, 'Longitude')) {
                                    fnRefreshMap(deviceLocation);
                                }
                            }
                        );
                    }
                }).fail(function() {
                    //
                });
        }
    }

    function s2_init() {
        var id = device_info.id;
        var appFiles = '';
        var os = (typeof(device_info.operatingSystemName) === "undefined") ? '' : device_info.operatingSystemName;
        if (os === "Android") {
            appFiles = '/androidAppFiles';
        } else if (os === "iOS") {
            appFiles = '/iphoneAppFiles';
        } else if (os === "Windows") {
            appFiles = '/windowsAppFiles';
        } else {
            appFiles = '/windowsAppFiles';
        }
        appFiles += "?pageNumber=1&pageSize=100&pageSort=label%2Casc";

        Handlebars.registerHelper('if_System', function(block) {
            if ($.login.user.domainName == "System")
                return block.fn();
            else
                return block.inverse();
        });

        Handlebars.registerHelper('ifvalue', function(conditional, options) {
            if (options.hash.value === conditional) {
                return options.fn(this)
            } else {
                return options.inverse(this);
            }
        });

        Handlebars.registerHelper('if_DeviceOperator', function(block) {
            if ($.login.user.checkRole('DeviceOperator'))
                return block.fn();
            else
                return block.inverse();
        });

        // Backbone MVC
        var AvailableAppModel = Backbone.Model.extend({});
        var AvailableAppCollection = Backbone.Collection.extend({
            sort_key: 'id',
            model: AvailableAppModel,
            initialize: function() {
                this.sort_order = 'asc';
            },
            comparator: function(a, b) {
                var a = a.get(this.sort_key);
                var b = b.get(this.sort_key);
                if (this.sort_order === "asc") {
                    return a > b ? 1 : a < b ? -1 : 0;
                } else {
                    return a > b ? -1 : a < b ? 1 : 0;
                }
            },
            sortByField: function(fieldName, order) {
                this.sort_key = fieldName;
                this.sort_order = order;
                this.sort();
            },
            filterByField: function(fieldName, fieldValue) {
                filtered = this.filter(function(apps) {
                    return apps.get(fieldName).match(fieldValue);
                });
                var collection = new AvailableAppCollection();
                collection.add(filtered);
                return collection;
            }
        });

        var availableAppsHtmlTemp = $("#tpl_s2_available").html();
        var AvailableAppsView = Backbone.View.extend({
            template: Handlebars.compile(availableAppsHtmlTemp),
            initialize: function() {
                this.collection.on('sort', this.render, this);
            },
            render: function() {
                var availableApp = this.collection.toJSON();
                $(this.el).html(this.template({
                    available_data: availableApp
                }));
                $("#s2_available").html(this.$el);
                $("#availableTitle").text(i18n.t("ns:Message.AvailableApps"));
                $(".btn-install").text(i18n.t("ns:Message.Install"));
                $(".version").text(i18n.t("ns:Message.Version"));
                // create available icon
                $(available_data).each(function(index, data) {
                    $("#available-" + available_data[index].id).attr("onerror", "$(this).attr('src','img/file/" + os.toLowerCase() + "-icon.png')");
                });
            },
            update: function(apps) {}
        });

        var InstalledAppModel = Backbone.Model.extend({});
        var InstalledAppCollection = Backbone.Collection.extend({
            sort_key: 'moduleType',
            model: InstalledAppModel,
            initialize: function() {
                this.sort_order = 'asc';
            },
            comparator: function(a, b) {
                var a = a.get(this.sort_key);
                var b = b.get(this.sort_key);
                if (this.sort_order === "asc") {
                    return a > b ? 1 : a < b ? -1 : 0;
                } else {
                    return a > b ? -1 : a < b ? 1 : 0;
                }
            },
            sortByField: function(fieldName, order) {
                this.sort_key = fieldName;
                this.sort_order = order;
                this.sort();
            },
            filterByField: function(fieldName, fieldValue) {
                filtered = this.filter(function(apps) {
                    return apps.get(fieldName).match(fieldValue);
                });
                var collection = new InstalledAppCollection();
                collection.add(filtered);
                return collection;
            }
        });

        var installedAppHtmlTemp = $("#tpl_s2_installed").html();
        var InstalledAppsView = Backbone.View.extend({
            template: Handlebars.compile(installedAppHtmlTemp),
            initialize: function() {
                this.collection.on('sort', this.render, this);
            },
            render: function() {
                var installedApp = this.collection.toJSON();
                /*for (var i = 0; i < installedApp.length; i++)
                    installedApp[i].iconFileUrl = "img/file/android-icon.png";*/
                $(this.el).html(this.template({
                    installed_data: installedApp
                }));
                $("#s2_installed").html(this.$el);
                $("#s2_installed").i18n();
                $("#installedTitle").text(i18n.t("ns:Message.InstallApps"));
                $(".version").text(i18n.t("ns:Message.Version"));
                // create default icon
                $(installed_data).each(function(index, data) {
                    $(".installed-" + installed_data[index].id).attr("onerror", "$(this).attr('src','img/file/" + os.toLowerCase() + "-icon.png')");
                });
            },
            update: function(apps) {}
        });
        // End Backbone MVC

        availableAppCollection = new AvailableAppCollection();
        availableAppsView = new AvailableAppsView({ collection: availableAppCollection });
        installedAppCollection = new InstalledAppCollection();
        installedAppsView = new InstalledAppsView({ collection: installedAppCollection });

        //DATA
        var available_data = new Array();
        var installed_data = new Array();

        // Get available apps
        $.ajax({
            url: $.config.server_rest_url + appFiles,
            type: 'GET',
            dataType: 'json',
            async: false
        }).done(function(data) {
            available_data = data.content;
            $.ajax({
                url: $.config.server_rest_url + '/devices/' + id + '/apps',
                type: 'GET',
                dataType: 'json',
                timeout: 30000,
            }).done(function(data) {
                installed_data = data.content;
                console.log("[Installed data] => ");
                console.log(installed_data);

                // To fix multi id=0 app's bug by modifying its' id as negative integer by Ken 20141231
                var decreasedCount = 0;
                for (var i = installed_data.length - 1; i >= 0; i--) {
                    if (installed_data[i].id == 0) installed_data[i].id = --decreasedCount;
                }

                console.log("[Modified Installed data] => ");
                console.log(installed_data);

                // create installed icon
                $(installed_data).each(function(index) {
                    if (installed_data[index].moduleType == "user") {
                        $(available_data).each(function(index_) {
                            // 2016-7-25 Holisun
                            // 當要進行遠端app安裝時，原本限制名稱與版本相同者無法進行。但實際上，目前並沒有 upgrade 功能，名稱相同就無法進行了。
                            // 所以，修改為將版本檢查拿掉。
                            if (installed_data[index].packageId == available_data[index_].name)

                            //if (installed_data[index].packageId == available_data[index_].name &&
                            //  installed_data[index].version == available_data[index_].version)
                            {
                                installed_data[index].moduleType = "repo";
                                return false;
                            }
                        });
                    }
                    if (installed_data[index].iconUrl != null) {
                        installed_data[index].iconFileUrl = installed_data[index].iconUrl;
                    } else {
                        installed_data[index].iconFileUrl = 'img/file/android-icon.png';
                    }
                });

                installedAppCollection.add(installed_data);
                installedAppsView.render();

                var installedAppList = [];
                var target_data = [];
                for (var i = 0; i < available_data.length; i++) {
                    var in_installed = false;
                    for (var j = 0; j < installed_data.length; j++) {
                        // Accorrding to Alvin's requirement, he wants to compare list using package name and version. Modified by Ken 20141111
                        if (installed_data[j].packageId == available_data[i].name && installed_data[j].version == available_data[i].version) {
                            in_installed = true;
                            installedAppList.push(available_data[i].id); // Modified by Bryan
                            break;
                        }
                    }
                    target_data.push(available_data[i]);
                }

                // create availabled icon
                $(target_data).each(function(index, data) {
                    target_data[index].iconFileUrl = target_data[index].iconUrl;
                });

                availableAppCollection.add(available_data);
                availableAppsView.render();

                // Modified by Bryan
                for (var i = 0; i < installedAppList.length; i++) {
                    $("#s2_available [data-id=" + installedAppList[i] + "]").attr("disabled", "disabled");
                    $("#s2_available [data-id=" + installedAppList[i] + "]").css("background-color", "Transparent")
                        .css("border-color", "Transparent")
                        .css("color", "black");
                    $("#s2_available [data-id=" + installedAppList[i] + "]").text(i18n.t("ns:Message.Installed"));
                }

                pageSetUp();
            }).fail(function() {
                console.log("error");
            }).always(function(data) {
                console.log("complete");
                $("body").removeClass("loading");
            });
        });
    }

    function s2() {
        s2_init();
        Backbone.Events.off("TaskStatusChanged");
        Backbone.Events.on("TaskStatusChanged", function(result) {
            console.log("Device AM Result: " + result);

            var isDeviceFinishTask = sessionStorage.getItem("isDevice_" + device_info.id + "_FinishAMTask");
            if (!_.isUndefined(isDeviceFinishTask)) {
                if (isDeviceFinishTask === "false") {
                    sessionStorage.setItem("isDevice_" + device_info.id + "_FinishAMTask", true);
                    if (window.location.hash.search(AD.page) >= 0) {
                        onDeviceAMResult(result);
                    }
                }
            }
        });
        Backbone.Events.off("AppBlockedXXX");
        Backbone.Events.on("AppBlockedXXX", function(jsonData) {
            console.log("Try to add block icon on some apps");
            console.log("$(installedApp-com.adobe.flashplayer)=" + $("div#installedApp-com\\.adobe\\.flashplayer").length);
            var jqDivImage = $(document.getElementById("installedApp-" + jsonData.packageId)).find('div').eq(0);
            jqDivImage.append("<img src='img/blocked_app.png' class='installed-com.adobe.reader' style='height: 30px; width: inherit; margin-top: -40px; margin-left: -20px;'>");
        });
    }

    function onDeviceAMResult(result) {
        if (_.isUndefined(taskData) || (taskData.taskId !== result.taskId)) {
            taskData = result;
            // $("#s2 a[data-name='PHM Registry Editor']").text("Progess");
            // $("#s2 a[data-name='PHM Registry Editor']").html("<i class='fa fa-spinner fa-spin'></i>");
            $("#refreshDiv").css("display", "block");
            $("#inventory_scan i").addClass("fa-spin");
            s2();
            $("#inventory_scan i").removeClass("fa-spin");
            $("#refreshDiv").css("display", "none");
            sessionStorage.removeItem("isDevice_" + device_info.id + "_FinishAMTask");
        }
    }

    /* 2014/06/04 Kenny Add */
    function s3() {
        var id = device_info.id;

        var capacitive_category = {};
        // Load category
        $.ajax({
            //url: $.config.server_rest_url+'/device/'+id+'/capabilities',
            url: $.config.server_rest_url + '/deviceTypes/' + device_info.deviceTypeId,
            type: 'GET',
            dataType: 'json',
            timeout: 10000,
            error: function(data, status, error) {
                //alert("Server busy, please try again!");
            }
        }).done(function(data) {
            capacitive_category.capacitive_category = new Array();
            var capacitive_type_items = {};
            capacitive_type_items.capacitive_type_name = i18n.t("ns:Device.Capability");
            capacitive_type_items.capacitive_type_items = new Array();
            var items = new Array();
            for (var index in data.reportSettingList) {
                var capacitive_type_item = {};
                capacitive_type_item.item_id = data.reportSettingList[index].name;
                capacitive_type_item.item_img = 'img/inventory_icons/' + data.reportSettingList[index].name.toLowerCase() + '.png';
                capacitive_type_item.item_name = i18n.t("ns:Device.Capabilities." + data.reportSettingList[index].name);
                items.push(capacitive_type_item);
            }
            capacitive_type_items.capacitive_type_items = _.sortBy(items, function(o) {
                return o.item_id;
            });
            capacitive_category.capacitive_category.push(capacitive_type_items);

            var browserWidth = $(window).width();
            if (browserWidth >= 1180) {
                $("#s3").css("height", (((data.reportSettingList.length / 5) + 1) * 204 + 132));
            }
        }).fail(function() {
            capacitive_category = {
                'capacitive_category': [{
                    capacitive_type_name: 'System',
                    capacitive_type_items: [{
                        item_id: 'CPU',
                        item_img: 'img/inventory_icons/cpu.png',
                        item_name: 'CPU'
                    }, {
                        item_id: 'InternalMemory',
                        item_img: 'img/inventory_icons/harddisk.png',
                        item_name: 'Internal Memory'
                    }, {
                        item_id: 'Battery',
                        item_img: 'img/inventory_icons/battery.png',
                        item_name: 'Battery'
                    }]
                }, {
                    capacitive_type_name: 'I/O',
                    capacitive_type_items: [{
                        item_id: 'TouchPanel',
                        item_img: 'img/inventory_icons/touchpanel.png',
                        item_name: 'TouchPanel'
                    }, {
                        item_id: 'Camera',
                        item_img: 'img/inventory_icons/camera.png',
                        item_name: 'Camera'
                    }, {
                        item_id: 'Keyboard',
                        item_img: 'img/inventory_icons/keyboard.png',
                        item_name: 'Keyboard'
                    }, {
                        item_id: 'BarcodeScanner',
                        item_img: 'img/inventory_icons/barcodescanner.png',
                        item_name: 'Barcode Scanner'
                    }, {
                        item_id: 'Bluetooth',
                        item_img: 'img/inventory_icons/bluetooth.png',
                        item_name: 'Bluetooth'
                    }, {
                        item_id: 'GPS',
                        item_img: 'img/inventory_icons/gps.png',
                        item_name: 'GPS'
                    }, {
                        item_id: 'USB',
                        item_img: 'img/inventory_icons/usb.png',
                        item_name: 'USB'
                    }]
                }, {
                    capacitive_type_name: 'Connectivity',
                    capacitive_type_items: [{
                        item_id: 'WLAN',
                        item_img: 'img/inventory_icons/connectivity.png',
                        item_name: 'WLAN'
                    }, {
                        item_id: 'WWAN',
                        item_img: 'img/inventory_icons/wifi.png',
                        item_name: 'WWAN'
                    }]
                }, {
                    capacitive_type_name: 'Software',
                    capacitive_type_items: [{
                        item_id: 'OS',
                        item_img: 'img/inventory_icons/software.png',
                        item_name: 'OS'
                    }, {
                        item_id: 'Firmware',
                        item_img: 'img/inventory_icons/firmware.png',
                        item_name: 'Firmware'
                    }]
                }]
            };
        }).always(function() {
            var source = $("#tpl_device_capacitive").html();
            var template = Handlebars.compile(source);

            var html = template(capacitive_category);

            $("#s3_capacitive").html(html);

            $("#s3_capacitive a").click(function(item) {
                show_capacitive_diaglog(this.id);
                return false;
            });
        });

        function show_capacitive_diaglog(hrefId) {
            item_id = hrefId.replace("capacitive-", "")
            var tpl_capacitive_dialog_source = $("#tpl_capacitive_dialog").html();
            var tpl_capacitive_dialog_template = Handlebars.compile(tpl_capacitive_dialog_source);

            Handlebars.registerHelper("labelformat", function(nodeName) {
                return nodeName.replace(/([a-z])([A-Z])/g, '$1 $2');
            });

            Handlebars.registerHelper("transformat", function(type, nodeName, value) {
                if (type == 'text')
                    return new Handlebars.SafeString("<input type='text' name='" + nodeName + "' value='" + value + "'></input>");
                else if (type == 'radio')
                    return new Handlebars.SafeString("<div class='btn-group' data-toggle='bottons'><label class='btn btn-primary bg-color-green txt-color-white'><input type='radio' name='" + nodeName + "' value='true' />Open</label><label class='btn btn-primary bg-color-red txt-color-white'><input type='radio' name='" + nodeName + "' value='false' />Close</label></div>");
                else if (type == 'hidden')
                    return new Handlebars.SafeString("<input type='hidden' name='" + nodeName + "' value='" + value + "'></input>");
                else
                    return value;
            });

            var capacitive_data;
            $.ajax({
                url: $.config.server_rest_url + '/devices/' + id + '/capabilities/' + item_id,
                type: 'GET',
                dataType: 'json',
                timeout: 10000,
                error: function(data, status, error) {
                    //alert("Server busy, please try again!");
                }
            }).done(function(data) {
                capacitive_data = data;
                capacitive_data.id = i18n.t("ns:Device.Capabilities." + capacitive_data.id);
                console.log(capacitive_data);
            }).fail(function() {

                var capacitive_data_demo = {
                    'id': item_id,
                    "isDcmo": true,
                    "dcmoState": true,
                    "lastScanTime": '2014/07/17 16:34:00',
                    "nodeBeanList": [{
                        "name": 'Mode',
                        "value": '802.11 b/g'
                    }, {
                        "name": 'SSID',
                        "value": 'GET_THEN_BETTER',
                        type: 'text'
                    }, {
                        "name": 'MAC',
                        "value": '00:0A:00:EF:9C:96'
                    }]
                };
            }).always(function() {
                var tpl_capacitive_dialog_html = tpl_capacitive_dialog_template(capacitive_data);
                var dialog_buttons = [{
                    html: i18n.t("ns:Device.Actions.Cancel"),
                    "class": "btn btn-default",
                    click: function() {
                        $(this).dialog("close");
                    }
                }];
                if ((typeof(capacitive_data) !== "undefined") && (capacitive_data.isDcmo !== null)) {
                    var submitBtn = {
                        html: "<i class='fa fa-check'></i>&nbsp;" + i18n.t("ns:Device.Submit"),
                        "class": "btn btn-primary",
                        click: function(event) {
                            var data = $.parseJSON("{\"" + $('#capacitive_dialog :input').serialize().replace(/&/g, '","').replace(/=/g, '":"') + "\"}");
                            var dmURL = $.config.server_rest_url + '/devices/' + id + '/capabilities/' + item_id;
                            console.log(data);
                            $.ajax({
                                beforeSend: function(xhr) {
                                    xhr.setRequestHeader('X-HTTP-Method-Override', 'PUT');
                                },
                                type: 'POST', // PUT
                                url: dmURL,
                                crossDomain: true,
                                data: data,
                                dataType: 'json',
                                contentType: 'application/json',
                                timeout: 10000,
                            }).done(function(data) {
                                $('#capacitive_dialog').dialog("close");
                            }).fail(function(data, status, error) {
                                $('#capacitive_dialog').dialog({
                                    buttons: [{
                                        html: i18n.t("ns:Device.OK"),
                                        "class": "btn btn-primary",
                                        click: function() {
                                            $(this).dialog("close");
                                        }
                                    }]
                                }).html('<p>' + i18n.t("ns:Device.ServerNoResponse") + '</p>');
                            });
                        }
                    };
                    dialog_buttons.push(submitBtn);
                }

                $("#capacitive_dialog").dialog({
                    autoOpen: false,
                    modal: true,
                    width: 450,
                    title: "<div class='widget-header'><h4><i class='icon-ok'></i>" + i18n.t("ns:Device.Title.DeviceAttribute") + "</h4></div>",
                    buttons: dialog_buttons
                });

                $("#capacitive_dialog").html(tpl_capacitive_dialog_html);

                $('#capacitive_dialog').dialog('open');
                $('#capacitive_dialog').i18n();
                $('#refresh').on("click", function(event) {
                    $.ajax({
                        url: $.config.server_rest_url + '/devices/' + id + '/agentCapabilities/' + item_id,
                        type: 'GET',
                        dataType: 'json',
                        error: function(data, status, error) {
                            $.smallBox({
                                title: i18n.t("ns:Message.ErrorOccurred"),
                                content: "<i>" + i18n.t("ns:Message.ErrorTryAgain") + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-times bounce animated",
                                timeout: 6000
                            });
                            $("#refresh i").removeClass("fa-spin");
                        }
                    }).done(function(data) {
                        $("#refresh i").removeClass("fa-spin");
                        if (data.state) {
                            var tpl_DiagMon_Html = "{{#each baseModelBeanList}}" + "<tr>" + "<td><b>{{labelformat this.label}}: </b></td>" + "<td><b>{{transformat this.type this.id this.value}}</b></td>" + "</tr>" + "{{/each}}";
                            var diagMon_template = Handlebars.compile(tpl_DiagMon_Html);
                            var diagMon_html = diagMon_template(data);
                            //$("#capacitive_dialog table").empty();
                            $("#capacitive_dialog table").html(diagMon_html);
                            // TODO
                            if (!_.isUndefined(data.lastScanTime)) {
                                $("#capacitive_dialog > label > b").text(data.lastScanTime);
                            }
                        } else {
                            if (!_.isUndefined(data.lastScanTime)) {
                                $("#capacitive_dialog > label > b").text(data.lastScanTime);
                            }
                            $.smallBox({
                                title: i18n.t("ns:Message.ErrorOccurred"),
                                content: "<i>" + i18n.t("ns:Message.ErrorTryAgain") + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-times bounce animated",
                                timeout: 6000
                            });
                        }
                    }).fail(function() {
                        $.smallBox({
                            title: i18n.t("ns:Message.ErrorOccurred"),
                            content: "<i>" + i18n.t("ns:Message.ErrorTryAgain") + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-times bounce animated",
                            timeout: 6000
                        });
                        $("#refresh i").removeClass("fa-spin");
                    });
                    $("#refresh i").addClass("fa-spin");
                });
            });

        }
        pageSetUp();
    };

    function s4(id) {
        var AD = {};
        AD.collections = {};
        //var url = 'http://220.130.176.238:8080/acs-core/RESTful/deviceEvents';
        var url = $.config.server_rest_url + '/devices/' + id + '/deviceEvents';
        var idAttribute = AD.idAttribute = "id";
        AD.buttons = [{
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
            "sButtonClass": "edit-group-device-list-btn txt-color-white bg-color-blue",
            "fnClick": function(nButton, oConfig, oFlash) {
                eventGrid.refresh();
            }
        }];

        AD.columns = [{
            title: i18n.t("ns:Device.Table.EventName"),
            property: 'eventName',
            filterable: false,
            sortable: true,
            callback: function(o) {
                return o.eventName;
            }
        }, {
            title: i18n.t("ns:Device.Table.EventType"),
            property: 'eventType',
            filterable: false,
            sortable: true
        }, {
            title: i18n.t("ns:Device.Table.Remark"),
            property: 'remark',
            filterable: false,
            sortable: false
        }, {
            title: i18n.t("ns:Device.Table.EventTime"),
            property: 'eventTime',
            filterable: false,
            sortable: false
        }];

        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });

        var gvs = "#s4 .cctech-gv";
        var gv = $(gvs).first();

        if (!eventGrid) {
            var eventGrid = new Backbone.GridView({
                el: gv,
                collection: new AD.Collection(),
                columns: AD.columns,
                title: 'Events',
                AD: AD,
                buttons: AD.buttons
            });
        } else {
            eventGrid.refresh();
        }

        pageSetUp();
    };


    function s5(id) {
        var AD = {};
        AD.collections = {};
        var url = $.config.server_rest_url + '/devices/' + id + '/tasks'; //?type=AM';
        var idAttribute = AD.idAttribute = "id";
        AD.buttons = [{
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
            "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
            "fnClick": function(nButton, oConfig, oFlash) {
                $(".cctech-gv table").dataTable().fnDraw();
            }
        }];
        AD.sort = {
            "column": "taskId",
            "sortDir": "desc"
        };
        AD.columns = [{
            title: i18n.t("ns:Device.TaskType"),
            property: 'taskType',
            filterable: false,
            sortable: false,
            width: '250px',
            callback: function(o) {
                //return '<a href="#ajax/subtask_list.html/' + o.id + '">' + o.job + '</a>';
                switch (o.taskType) {
                    case "EnableDisableCapability":
                    case "EnableCapability":
                    case "DisableCapability":
                        return i18n.t("ns:Log.Task.TaskName.SwitchCapabilitySetting");
                        break;
                    case "LockDevice":
                        return i18n.t("ns:Log.Task.TaskName.LockDevice");
                        break;
                    case "UnlockDevice":
                        return i18n.t("ns:Log.Task.TaskName.UnlockDevice");
                        break;
                    case "ActivateApp":
                        return i18n.t("ns:Log.Task.TaskName.ActivateApp");
                        break;
                    case "DeactivateApp":
                        return i18n.t("ns:Log.Task.TaskName.DeactivateApp");
                        break;
                    case "DownloadAndInstallApp":
                        return i18n.t("ns:Log.Task.TaskName.InstallApp");
                        break;
                    case "RemoveApp":
                        return i18n.t("ns:Log.Task.TaskName.RemoveApp");
                        break;
                    case "UpgradeApp":
                        return i18n.t("ns:Log.Task.TaskName.UpgradeApp");
                        break;
                    case "setAllowUserInstallApp":
                        return i18n.t("ns:Log.Task.TaskName.AllowInstallApp");
                        break;
                    case "autoRemoveLocalAPP":
                        return i18n.t("ns:Log.Task.TaskName.AutoRemoveLocalApp");
                        break;
                    case "UpgradeFirmware":
                        return i18n.t("ns:Log.Task.TaskName.FirmwareUpgrade");
                        break;
                    case "setDeviceEventList":
                        return i18n.t("ns:Log.Task.TaskName.SetDeviceEventList");
                        break;
                    case "setRule":
                        return i18n.t("ns:Log.Task.TaskName.SetRule");
                        break;
                    case "deleteRule":
                        return i18n.t("ns:Log.Task.TaskName.DeleteRule");
                        break;
                    case "editRule":
                        return i18n.t("ns:Log.Task.TaskName.EditRule");
                        break;
                    case "activeCapabilityEvent":
                        return i18n.t("ns:Log.Task.TaskName.ActivateCapabilityEvent");
                        break;
                    case "deleteAllRuleAndEvent":
                        return i18n.t("ns:Log.Task.TaskName.DeleteRuleEvent");
                        break;
                    case "Reset":
                        return i18n.t("ns:Log.Task.TaskName.RebootDevice");
                        break;
                    case "Restart":
                        return i18n.t("ns:Log.Task.TaskName.RestartDevice");
                        break;
                    case "applyManagementPolicy":
                        return i18n.t("ns:Log.Task.TaskName.ApplyManagementPolicy");
                        break;
                    case "AddAppToBlacklist":
                        return i18n.t("ns:Log.Task.TaskName.SetAppBlacklist");
                        break;
                    case "EnableTriggerServerConnection":
                        return i18n.t("ns:Log.Task.TaskName.EnableTrigger");
                        break;
                    case "DisableTriggerServerConnection":
                        return i18n.t("ns:Log.Task.TaskName.DisableTrigger");
                        break;
                    case "UpgradeDeltaFirmware":
                        return i18n.t("ns:Log.Task.TaskName.SystemUpdate");
                        break;
                    default:
                        return "Unknown";
                        break;
                }
            }
        }, {
            title: 'ns:Log.Task.Creator',
            property: 'creatorName',
            filterable: false,
            sortable: false,
            width: '250px',
        }, {
            // title: 'State',
            title: i18n.t("ns:Device.State"),
            property: 'state',
            // cellClassName: 'id',
            filterable: false,
            sortable: false,
            width: '160px',
            callback: function(o) {
                var color = "#919191";
                color = eval('$.config.ui.subtaskExecutionStatusColor.' + o.state.toLowerCase());
                return "<i class=\"fa fa-circle\" style=\"color:" + color + ";\"></i> " + o.state;
            }
        }, {
            title: i18n.t("ns:Device.Time"),
            property: 'finishTime',
            filterable: false,
            sortable: true,
            width: '250px',
        }, {
            title: i18n.t("ns:Device.Remark"),
            property: 'remark',
            filterable: false,
            sortable: false,
            width: '300px',
        }];
        /* define namespace ends */

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
        /* collection ends */

        var gvs = "#s5 .cctech-gv";
        var gv = $(gvs).first();

        if (!taskGrid) {
            var taskGrid = new Backbone.GridView({
                el: gv,
                collection: new AD.Collection(),
                columns: AD.columns,
                title: 'Tasks',
                AD: AD,
                sort: AD.sort,
                buttons: AD.buttons
            });
        } else {
            taskGrid.refresh();
        }
        //this.gv.show();
        Backbone.Events.off("DeviceTaskUpdate");
        // Backbone.Events.on("DeviceTaskUpdate", function(result)
        // {
        //     console.log("comet refresh="+result.deviceId);
        //     taskGrid.refresh();
        //     if(_.has(result, "deviceId"))
        //     {
        //         if(result.deviceId === id)
        //         {
        //             taskGrid.refresh();
        //         }
        //     }
        // });
        Backbone.Events.on("DeviceTaskUpdate", function() {
            taskGrid.refresh();
        });

        // Backbone.Events.trigger("DeviceTaskUpdate",{"deviceId":1056});
        pageSetUp();
    };

    function macHexToDex(mac) {
        var revId = mac;
        if (mac.length == 12) {
            var revIdHex2Dec = "";

            for (var i = 0; i < 12; i += 2) {
                var hexString = mac.substring(i, i + 2);
                var decInt = parseInt(hexString, 16);
                if (decInt < 10)
                    revIdHex2Dec += padLeft(decInt, 2);
                else if (decInt < 100)
                    revIdHex2Dec += padLeft(decInt, 1);
                else
                    revIdHex2Dec += decInt;
            }
            revId = revIdHex2Dec;
            revId = "1" + revId;
        }
        return revId;
    }

    function padLeft(str, lenght) {
        if (str.length > lenght)
            return str;
        else
            return padLeft("0" + str, lenght);
    }
    //=================================================================================================================
    // Remote Desktop (Tag)
    //=================================================================================================================
    function s6(id) {
        console.log('[Remote Desktop] sVncState=[' + sessionStorage.sVncState + ']');
        console.log('[Remote Desktop] $("#noVNC_canvas_div")=[' + $("#noVNC_canvas_div").length + ']');
        /*
        if (sessionStorage.sWebPageDeviceRemoteDesktopStateIsNormal === 'true')
        {
            console.log('eeeeeeeeeee');
            return;
        };
        */

        if (!(sessionStorage.sVncState in {
                'disconnected': 1,
                'failed': 1,
                'fatal': 1,
                'unknown': 1
            })) {
            return;
        };

        var onOpt = [{
            "name": "./AMobileMO/RemoteDesktop/RptAddr",
            "value": "rm.node-watch.com:5500"
        }, {
            "name": "./AMobileMO/RemoteDesktop/RptID",
            "value": device_info.name
        }, {
            "name": "./AMobileMO/RemoteDesktop/Enable",
            "value": "true"
        }];
        var offOpt = [{
            "name": "./AMobileMO/RemoteDesktop/Enable",
            "value": "false"
        }];
        var url = $.config.server_rest_url + '/devices/' + device_info.id + '/operations/replaceDataModelTreeNode';

        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(onOpt),
            contentType: 'application/json',
            dataType: "json",
        }).done(function(data, textStatus, jqXHR) {
            if (data.status === true) {
                console.log('[Remote Desktop] Establish VNC connection (VNC Server <---> Repeater) ......... OK');
            } else {
                //console.error('[Remote Desktop] Establish VNC connection (VNC Server <---> Repeater) ......... Failed');
                funRemoteDesktopFailHandler(jqXHR, textStatus, errorThrown, '[Remote Desktop] Establish VNC connection (VNC Server <---> Repeater) ......... Failed');
                return;
            }
            var rptId = "ID:" + macHexToDex(device_info.name);
            console.log('[Remote Desktop] Establish VNC connection (VNC Viewer <---> Repeater) .........');
            //noVNC_API.disconnect();
            window.noVNC_API.vnc({
                host: "rm.node-watch.com",
                port: "6080",
                resize: "downscale",
                cursor: "false",
                repeaterID: rptId
            });
            sessionStorage.setItem('sWebPageDeviceRemoteDesktopStateIsNormal', 'true');

        }).fail(function(jqXHR, textStatus, errorThrown) {
            funRemoteDesktopFailHandler(jqXHR, textStatus, errorThrown, '[Remote Desktop] Establish VNC connection (VNC Server <---> Repeater) ......... Failed');
        });
    };

    var funRemoteDesktopFailHandler = function(jqXHR, textStatus, errorThrown, message) {
            // textStatus的值：null, timeout, error, abort, parsererror
            console.log('textStatus=[' + textStatus + ']'); // error
            //console.log('jqXHR.readyState=[' + jqXHR.readyState + ']');
            console.log('jqXHR.status=[' + jqXHR.status + ']'); // 404
            console.log('jqXHR.statusText=[' + jqXHR.statusText + ']'); // Not Found
            //console.log('errorThrown=[' + errorThrown + ']');  // Not Found
            //console.log('jqXHR.responseText=' + jqXHR.responseText);
            console.error(message);
            $.smallBox({
                title: "Remote Desktop",
                content: "<i class='fa fa-warning'></i> <i> Remote desktop can NOT display because some connections are failed!! </i>",
                color: "#B36464",
                timeout: 5000
            });
        }
        //end of s6
        /*
            function s7(id)
            {
                $.ajax(
                {
                    type: "GET",
                    url: "http://192.168.102.231:8000/data/",
                    dataType: "json",
                    timeout: 10000,
                    crossDomain: true,
                    error: function(data, status, error)
                    {
                        console.log("Server busy, please try again!");
                        console.log("status="+status);
                        console.log("error="+error);
                    }
                }).done(function(data)
                {
                    console.log("data.records[0].IP=" + data.records[0].IP);

                    $('#BOE_IP').text(data.records[0].IP);
                    $('#BOE_OP').text(data.records[0].OP);
                    $('#BOE_FDeviceID').text(data.records[0].FDeviceID);
                    $('#BOE_Class').text(data.records[0].Class);

                    $('#BOE_DT0').text(data.records[1].DT0);
                    $('#BOE_DT1').text(data.records[1].DT1);
                    $('#BOE_DT31').text(data.records[1].DT31);
                    $('#BOE_DT32').text(data.records[1].DT32);
                    $('#BOE_DT34').text(data.records[1].DT34);
                    $('#BOE_DT35').text(data.records[1].DT35);
                    $('#BOE_DT49').text(data.records[1].DT49);

                    $("#BOE_DT165").text(data.records[2].DT165);
                    $("#BOE_DT37").text(data.records[2].DT37);
                    $("#BOE_DT41").text(data.records[2].DT41);
                    $("#BOE_DT45").text(data.records[2].DT45);
                    $("#BOE_DT39").text(data.records[2].DT39);
                    $("#BOE_DT43").text(data.records[2].DT43);
                    $("#BOE_DT47").text(data.records[2].DT47);

                    $("#BOE_DT200").text(data.records[3].DT200);
                    $("#BOE_DT201").text(data.records[3].DT201);
                    $("#BOE_DT202").text(data.records[3].DT202);
                    $("#BOE_DT203").text(data.records[3].DT203);
                    $("#BOE_DT204").text(data.records[3].DT204);
                    $("#BOE_DT205").text(data.records[3].DT205);
                    $("#BOE_DT206").text(data.records[3].DT206);

                    $("#BOE_R221").text(data.records[4].R221);
                    $("#BOE_R224").text(data.records[4].R224);
                    $("#BOE_R228").text(data.records[4].R228);
                    $("#BOE_R222").text(data.records[4].R222);
                    $("#BOE_R225").text(data.records[4].R225);
                    $("#BOE_R22F").text(data.records[4].R22F);
                    $("#BOE_R227").text(data.records[4].R227);

                    $("#BOE_R237").text(data.records[5].R237);
                    $("#BOE_R240").text(data.records[5].R240);
                    $("#BOE_R239").text(data.records[5].R239);
                    $("#BOE_R218").text(data.records[5].R218);
                    $("#BOE_R23E").text(data.records[5].R23E);
                    $("#BOE_R28C").text(data.records[5].R28C);
                    $("#BOE_R23B").text(data.records[5].R23B);

                }).fail(function()
                {
                    console.log("fail");
                });

            };
        */
    function s72(id) {
        var AD = {};
        AD.collections = {};
        var url = $.config.server_rest_url + '/devices/' + id + '/tasks'; //?type=AM';
        var idAttribute = AD.idAttribute = "id";
        AD.buttons = [{
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
            "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
            "fnClick": function(nButton, oConfig, oFlash) {
                $(".cctech-gv table").dataTable().fnDraw();
            }
        }];

        AD.sort = {
            "column": "taskId",
            "sortDir": "desc"
        };
        AD.columns = [{
            title: i18n.t('ns:DeviceSpec.Column_1'),
            property: 'taskType',
            filterable: false,
            sortable: false,
            width: '250px',
            callback: function(o) {
                //return '<a href="#ajax/subtask_list.html/' + o.id + '">' + o.job + '</a>';
                switch (o.taskType) {
                    case "EnableDisableCapability":
                    case "EnableCapability":
                    case "DisableCapability":
                        return i18n.t("ns:Log.Task.TaskName.SwitchCapabilitySetting");
                        break;
                    case "LockDevice":
                        return i18n.t("ns:Log.Task.TaskName.LockDevice");
                        break;
                    case "UnlockDevice":
                        return i18n.t("ns:Log.Task.TaskName.UnlockDevice");
                        break;
                    case "ActivateApp":
                        return i18n.t("ns:Log.Task.TaskName.ActivateApp");
                        break;
                    case "DeactivateApp":
                        return i18n.t("ns:Log.Task.TaskName.DeactivateApp");
                        break;
                    case "DownloadAndInstallApp":
                        return i18n.t("ns:Log.Task.TaskName.InstallApp");
                        break;
                    case "RemoveApp":
                        return i18n.t("ns:Log.Task.TaskName.RemoveApp");
                        break;
                    case "UpgradeApp":
                        return i18n.t("ns:Log.Task.TaskName.UpgradeApp");
                        break;
                    case "setAllowUserInstallApp":
                        return i18n.t("ns:Log.Task.TaskName.AllowInstallApp");
                        break;
                    case "autoRemoveLocalAPP":
                        return i18n.t("ns:Log.Task.TaskName.AutoRemoveLocalApp");
                        break;
                    case "UpgradeFirmware":
                        return i18n.t("ns:Log.Task.TaskName.FirmwareUpgrade");
                        break;
                    case "setDeviceEventList":
                        return i18n.t("ns:Log.Task.TaskName.SetDeviceEventList");
                        break;
                    case "setRule":
                        return i18n.t("ns:Log.Task.TaskName.SetRule");
                        break;
                    case "deleteRule":
                        return i18n.t("ns:Log.Task.TaskName.DeleteRule");
                        break;
                    case "editRule":
                        return i18n.t("ns:Log.Task.TaskName.EditRule");
                        break;
                    case "activeCapabilityEvent":
                        return i18n.t("ns:Log.Task.TaskName.ActivateCapabilityEvent");
                        break;
                    case "deleteAllRuleAndEvent":
                        return i18n.t("ns:Log.Task.TaskName.DeleteRuleEvent");
                        break;
                    case "Reset":
                        return i18n.t("ns:Log.Task.TaskName.RebootDevice");
                        break;
                    case "Restart":
                        return i18n.t("ns:Log.Task.TaskName.RestartDevice");
                        break;
                    case "applyManagementPolicy":
                        return i18n.t("ns:Log.Task.TaskName.ApplyManagementPolicy");
                        break;
                    case "AddAppToBlacklist":
                        return i18n.t("ns:Log.Task.TaskName.SetAppBlacklist");
                        break;
                    case "EnableTriggerServerConnection":
                        return i18n.t("ns:Log.Task.TaskName.EnableTrigger");
                        break;
                    case "DisableTriggerServerConnection":
                        return i18n.t("ns:Log.Task.TaskName.DisableTrigger");
                        break;
                    case "UpgradeDeltaFirmware":
                        return i18n.t("ns:Log.Task.TaskName.SystemUpdate");
                        break;
                    default:
                        return "Unknown";
                        break;
                }
            }
        }, {
            //title: 'ns:DeviceSpec.Column_2',
            title: i18n.t('ns:DeviceSpec.Column_2'),
            property: 'creatorName',
            filterable: false,
            sortable: false,
            width: '250px',
        }, {
            title: i18n.t('ns:DeviceSpec.Column_3'),
            property: 'state',
            // cellClassName: 'id',
            filterable: false,
            sortable: false,
            width: '160px',
            callback: function(o) {
                var color = "#919191";
                color = eval('$.config.ui.subtaskExecutionStatusColor.' + o.state.toLowerCase());
                return "<i class=\"fa fa-circle\" style=\"color:" + color + ";\"></i> " + o.state;
            }
        }, {
            title: i18n.t('ns:DeviceSpec.Column_4'),
            property: 'finishTime',
            filterable: false,
            sortable: true,
            width: '250px',
        }, {
            title: i18n.t('ns:DeviceSpec.Column_5'),
            property: 'remark',
            filterable: false,
            sortable: false,
            width: '300px',
        }];
        /* define namespace ends */

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });
        /* collection ends */
        var gvs = "#s7 .cctech-gv";
        var gv = $(gvs).first();

        if (!taskGrid) {
            var taskGrid = new Backbone.GridView({
                el: gv,
                collection: new AD.Collection(),
                columns: AD.columns,
                title: 'Tasks',
                AD: AD,
                sort: AD.sort,
                buttons: AD.buttons
            });
        } else {
            taskGrid.refresh();
        }
        //this.gv.show();
        Backbone.Events.off("DeviceTaskUpdate");
        Backbone.Events.on("DeviceTaskUpdate", function(result) {
            if (_.has(result, "deviceId")) {
                if (result.deviceId === id) {
                    taskGrid.refresh();
                }
            }
        });
        // Backbone.Events.trigger("DeviceTaskUpdate",{"deviceId":1056});

        pageSetUp();
    };


    function editDevice(id) {
        console.log("Edit device: " + id);
    }

    function lock(lockType) {
        //console.log("Lock device: "+id+" Status: "+lockType);
        // var lockStatus = "Unlock";
        var lockStatus = i18n.t("ns:Device.Message.Unlock");
        var icon = "fa-unlock";
        if (lockType) {
            lockStatus = i18n.t("ns:Device.Message.Lock");
            icon = "fa-lock";
        }
        // ask verification
        $.SmartMessageBox({
            title: "<i class='fa " + icon + " txt-color-orangeDark'></i> " + lockStatus + i18n.t("ns:Message.Device.Device") + " " + "<span class='txt-color-orangeDark'><strong>" +
                device_info.name + "</strong></span> ?",
            content: lockStatus + i18n.t("ns:Message.Device.DeviceContent"),
            buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

        }, function(ButtonPressed) {
            if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                //$.root_.addClass('animated fadeOutUp');
                setTimeout(doLock(lockType), 1000);
            }

        });
    }

    function doLock(lockType) {
        console.log("Lock device: " + device_info.name + " Status: " + lockType);
        var lockStatus = "Unlock";
        var icon = "fa-unlock";
        if (lockType) {
            var lockStatus = "Lock";
            icon = "fa-lock";
        }
        var url = $.config.server_rest_url + '/devices/' + device_info.id + '/actions/' + lockStatus.toLowerCase();
        $.ajax({
            url: url,
            type: 'POST', // PUT
            dataType: 'json',
            timeout: 10000,
            error: function(data, status, error) {
                //alert("Server busy, please try again!");
            }
        }).done(function(data) {
            // if ajax done
            $.smallBox({
                title: i18n.t("ns:Device.Message." + lockStatus) + i18n.t("ns:Message.Device.Device"),
                content: "<i class='fa fa-clock-o'></i> <i>" + i18n.t("ns:Device.Message." + lockStatus) + i18n.t("ns:Message.Device.Device") + " :" + device_info.name + "</i>",
                // title: lockStatus+" Device",
                // content: "<i class='fa fa-clock-o'></i> <i>You "+lockStatus+" device :"+device.name+"</i>",
                color: "#648BB2",
                iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
                timeout: 5000
            });
        }).fail(function() {});
    }

    function reboot(bootType) {
        //console.log("Boot device: "+id+" Status: "+bootType);
        var bootStatus = i18n.t("ns:Device.Message.Reset");
        var icon = "fa-share-square-o";
        if (bootType) {
            bootStatus = i18n.t("ns:Device.Message.Restart");
            icon = "fa-power-off";
        }
        // ask verification
        $.SmartMessageBox({
            title: "<i class='fa " + icon + " txt-color-orangeDark'></i> " + bootStatus + i18n.t("ns:Message.Device.Device") + " <span class='txt-color-orangeDark'><strong>" +
                device_info.name + "</strong></span> ?",
            content: bootStatus + i18n.t("ns:Message.Device.DeviceContent"),
            buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

        }, function(ButtonPressed) {
            if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                //$.root_.addClass('animated fadeOutUp');
                setTimeout(doReboot(bootType), 1000);
            }

        });
    }

    function doReboot(bootType) {
        console.log("Boot device: " + device_info.name + " Status: " + bootType);
        var bootStatus = "Reset";
        var icon = "fa-share-square-o";
        if (bootType) {
            bootStatus = "Restart";
            icon = "fa-power-off";
        }
        var url = $.config.server_rest_url + '/devices/' + device_info.id + '/actions/' + bootStatus.toLowerCase();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            timeout: 10000,
            error: function(data, status, error) {
                //alert("Server busy, please try again!");
            }
        }).done(function(data) {
            // if ajax done
            if (data.status) {
                $.smallBox({
                    title: i18n.t("ns:DeviceGroup.Actions." + bootStatus),
                    content: "<i class='fa fa-clock-o'></i> <i>[" + device_info.name + "]:" + i18n.t("ns:Message.Device.SendToServerSuccess") + "</i>",
                    // title: bootStatus+" Device",
                    // content: "<i class='fa fa-clock-o'></i> <i>You "+bootStatus+" device :"+device.name+"</i>",
                    color: "#648BB2",
                    iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
                    timeout: 5000
                });
            } else {
                $.smallBox({
                    title: i18n.t("ns:DeviceGroup.Actions." + bootStatus),
                    content: "<i class='fa fa-clock-o'></i> <i>[" + device_info.name + "]:" + i18n.t("ns:Message.Device.SendToServerFailed") + "</i>",
                    color: "#B36464",
                    iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
                    timeout: 5000
                });
            };
        }).fail(function() {
            console.log("[" + bootType + "]:Failed.");
        });
    }

    function firmwareUpgrade() {
        var id = device_info.id;
        var searchParams = '?search=operatingSystemName:' + device_info.operatingSystemName + ',deviceTypeId:' + device_info.deviceTypeId;
        var availablefirmwareData = {};
        availablefirmwareData.type = new Array();
        var firmwareType = {};
        // firmwareType.firmwareType = 'Firmware';
        firmwareType.firmwareType = i18n.t("ns:File.Firmware.Firmware");
        firmwareType.files = new Array();
        $.ajax({
            dataType: 'json',
            //url: $.config.server_rest_url+'/firmwareFiles',
            url: $.config.server_rest_url + '/firmwareFiles' + searchParams,
            success: function(data) {
                $.each(data.content, function(index, file) {
                    if (typeof(file.id) !== "undefined") {
                        var FirmwareFile = {};
                        FirmwareFile.fileId = file.id;
                        FirmwareFile.fileName = file.name;
                        FirmwareFile.fileVersion = " (v" + file.version + ")";
                        FirmwareFile.size = file.size;
                        FirmwareFile.remark = file.remark;
                        firmwareType.files.push(FirmwareFile);
                    }
                });
                var sysUpdate = {};
                sysUpdate.fileId = 0;
                sysUpdate.fileName = "Latest Version";
                sysUpdate.fileVersion = "";
                sysUpdate.size = 0;
                sysUpdate.remark = "";
                firmwareType.files.unshift(sysUpdate);
                availablefirmwareData.type.push(firmwareType);

                //<b>Firmware: {{currentFirmware}}</b><br />
                var source = $("#tpl_firmwareUpgrade").html();
                var template = Handlebars.compile(source);
                var firmwareHTML = template(availablefirmwareData);
                $("#dialog").html(firmwareHTML);
                $("#selectFile").text(i18n.t("ns:Device.SelectFile"));
                $('#dialog').dialog('open');
            }
        });

        $("#dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            title: "<div class='widget-header'><h4><i class='icon-ok'></i><i class='fa fa-upload'></i>" + i18n.t("ns:Device.Actions.FirmwareUpgrade") + "</h4></div>",
            buttons: [{
                html: i18n.t("ns:Device.Actions.Cancel"),
                "class": "btn btn-default",
                click: function() {
                    $(this).dialog("close");
                }
            }, {
                html: "<i class='fa fa-check'></i>&nbsp; " + i18n.t("ns:Device.Actions.Execute"),
                "class": "btn btn-primary",
                click: function(event) {
                    // var data = $('#capacitive_dialog :input').serialize()+'&deviceId='+id;
                    var hash = window.location.hash;
                    var id = hash.replace(/^#ajax\/device_detail.html\/(\w+)$/, "$1");
                    var firmwareFileId = $('#firmware').val();
                    var url = "";
                    if (firmwareFileId == 0)
                        url = $.config.server_rest_url + '/devices/' + id + '/deltaFirmwares/0/actions/upgrade';
                    else
                        url = $.config.server_rest_url + '/devices/' + id + '/firmwareFiles/' + firmwareFileId + '/actions/upgrade';
                    $.ajax({
                        type: 'POST',
                        url: url,
                        //data: data,
                        /*
                        data: {
                            DeviceId: id,
                            param: data
                        },
                        */
                        timeout: 10000,
                        error: function(data, status, error) {
                            $('#dialog').dialog().html('<p>Error Code:' + status + '</p><p>Explanation:' + error + '</p>');
                        }
                    }).done(function(data) {
                        $('#dialog').dialog('close');
                        if (data.status) {
                            $.smallBox({
                                title: i18n.t("ns:Device.Actions.FirmwareUpgrade"),
                                content: "<i class='fa fa-clock-o'></i> <i>[" + device_info.name + "]:" + i18n.t("ns:Message.Device.SendToServerSuccess") + "</i>",
                                color: "#648BB2",
                                iconSmall: "fa fa-upload fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        } else {
                            $.smallBox({
                                title: i18n.t("ns:Device.Actions.FirmwareUpgrade"),
                                content: "<i class='fa fa-clock-o'></i> <i>[" + device_info.name + "]:" + i18n.t("ns:Message.Device.SendToServerFailed") + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-upload fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        };
                    }).fail(function() {
                        $('#dialog').dialog().html('<p>Server no response!</p>');
                        console.log("[Firmware Upgrade]:Failed.");
                    });
                }
            }]
        });
    }

    //Al20150121
    function agentUpgrade() {
        var id = device_info.id;
        var searchParams = '?search=operatingSystemName:' + device_info.operatingSystemName;
        var availableagentData = {};
        availableagentData.type = new Array();
        var agentType = {};
        agentType.agentType = i18n.t("ns:File.Agent.Agent");
        agentType.files = new Array();
        $.ajax({
            dataType: 'json',
            //url: $.config.server_rest_url+'/firmwareFiles',
            url: $.config.server_rest_url + '/omaAgentFiles' + searchParams,
            success: function(data) {
                $.each(data.content, function(index, file) {
                    if (typeof(file.id) !== "undefined") {
                        var AgentFile = {};
                        AgentFile.fileId = file.id;
                        AgentFile.fileName = file.name;
                        AgentFile.fileVersion = file.version;
                        AgentFile.size = file.size;
                        AgentFile.remark = file.remark;
                        agentType.files.push(AgentFile);
                    }
                });
                availableagentData.type.push(agentType);

                //<b>Firmware: {{currentFirmware}}</b><br />
                var source = $("#tpl_agentUpgrade").html();
                var template = Handlebars.compile(source);
                var agentHTML = template(availableagentData);
                $("#dialog").html(agentHTML);
                $("#selectFile").text(i18n.t("ns:Device.SelectFile"));
                $('#dialog').dialog('open');
            }
        });
        agentDialog();
    }

    //Al20150121
    function agentDialog() {
        $("#dialog").dialog({
            autoOpen: false,
            modal: true,
            width: 450,
            title: "<div class='widget-header'><h4><i class='icon-ok'></i><i class='fa fa-floppy-o'></i>&nbsp;" + i18n.t("ns:Device.Actions.AgentUpgrade") + "</h4></div>",
            buttons: [{
                html: i18n.t("ns:Device.Actions.Cancel"),
                "class": "btn btn-default",
                click: function() {
                    $(this).dialog("close");
                }
            }, {
                html: "<i class='fa fa-check'></i>&nbsp; " + i18n.t("ns:Device.Actions.Execute"),
                "class": "btn btn-primary",
                click: function(event) {
                    // var data = $('#capacitive_dialog :input').serialize()+'&deviceId='+id;
                    var hash = window.location.hash;
                    var id = hash.replace(/^#ajax\/device_detail.html\/(\w+)$/, "$1");
                    var agentFileId = $('#agent').val();
                    var url = $.config.server_rest_url + '/devices/' + id + '/omaAgentFiles/' + agentFileId + '/actions/upgrade'
                    $.ajax({
                        type: 'POST',
                        url: url,
                        timeout: 10000,
                        error: function(data, status, error) {
                            $('#dialog').dialog().html('<p>Error Code:' + status + '</p><p>Explanation:' + error + '</p>');
                        }
                    }).done(function(data) {
                        $('#dialog').dialog('close');
                        if (data.status) {
                            $.smallBox({
                                title: i18n.t("ns:Device.Actions.AgentUpgrade"),
                                content: "<i class='fa fa-clock-o'></i> <i>[" + device_info.name + "]:" + i18n.t("ns:Message.Device.SendToServerSuccess") + "</i>",
                                color: "#648BB2",
                                iconSmall: "fa fa-floppy-o fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        } else {
                            $.smallBox({
                                title: i18n.t("ns:Device.Actions.AgentUpgrade"),
                                content: "<i class='fa fa-clock-o'></i> <i>[" + device_info.name + "]:" + i18n.t("ns:Message.Device.SendToServerFailed") + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-floppy-o fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        };
                    }).fail(function() {
                        $('#dialog').dialog().html('<p>Server no response!</p>');
                        console.log("[Agent Upgrade]:Failed.");
                    });
                }
            }]
        });
    }

    function deleteDevice(id) {
        //console.log("Delete device: "+id);
        $.SmartMessageBox({
            title: "<i class='fa fa-trash-o txt-color-orangeDark'></i> " + i18n.t("ns:Message.Device.DeleteDevice") + " <span class='txt-color-orangeDark'><strong>" +
                id + "</strong></span> ?",
            content: i18n.t("ns:Message.Device.DeleteDeviceQuestionMark"),
            buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

        }, function(ButtonPressed) {
            if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                //$.root_.addClass('animated fadeOutUp');
                setTimeout(doDelete(id), 1000);
            }

        });
    }

    //=================================================================================================================
    // Remote Desktop (Drop-Down List)
    //=================================================================================================================
    function startRemoteDesktop(id) {
        console.log('check#9');
        var onOpt = [{
            "name": "./AMobileMO/RemoteDesktop/RptAddr",
            "value": "rm.node-watch.com:5500"
        }, {
            "name": "./AMobileMO/RemoteDesktop/RptID",
            "value": device_info.name
        }, {
            "name": "./AMobileMO/RemoteDesktop/Enable",
            "value": "true"
        }];
        var offOpt = [{
            "name": "./AMobileMO/RemoteDesktop/Enable",
            "value": "false"
        }];
        var url = $.config.server_rest_url + '/devices/' + device_info.id + '/operations/replaceDataModelTreeNode';
        console.log('check#9-1');
        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(offOpt),
            contentType: 'application/json',
            dataType: "json",
        }).done(function(data) {
            console.log('check#5');
            sleep(3);
            console.log(data);
            $.ajax({
                type: "POST",
                url: url,
                data: JSON.stringify(onOpt),
                contentType: 'application/json',
                dataType: "json",
            }).done(function(data) {
                console.log('check#6');
                console.log(data);
                $('#rdToggle').prop('checked', true);
                //var rptId = "ID:" + getParameterByName('deviceName');
                //noVNC_API.vnc({host:"rm.node-watch.com", port:"6080", password:"1", repeaterID:rptId});
                window.open("rd.html?" + "repeaterID=" + device_info.name);
            }).fail(function(data) {
                console.log("fail" + data.responseText);
            });
        }).fail(function() {
            console.log("fail");
            $('#rdToggle').removeAttr("checked");
        }).always(function() {
            console.log("always");
        });
    }

    function sleep(sec) {
        var time = new Date().getTime();
        while (new Date().getTime() - time < sec * 1000);
    }

    function stopRemoteDesktop(id) {
        var offOpt = [{ "name": "./AMobileMO/RemoteDesktop/Enable", "value": "false" }];
        var url = $.config.server_rest_url + '/devices/' + device_info.id + '/operations/replaceDataModelTreeNode';

        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(offOpt),
            contentType: 'application/json',
            dataType: "json",
        }).done(function(data) {
            console.log(data);
        }).fail(function(data) {
            console.log("fail" + data.responseText);
        });
        $('#rdToggle').removeAttr("checked");
    }

    function doDelete(id) {
        // if ajax done
        $.smallBox({
            title: i18n.t("ns:Message.Device.DeleteDevice"), // i18n.t("ns:Device.DeleteDevice")
            content: "<i class='fa fa-clock-o'></i> <i>" + i18n.t("ns:Message.Device.DeleteDeviceId") + id + "</i>",
            // title: "Delete Device", // i18n.t("ns:Device.DeleteDevice")
            // content: "<i class='fa fa-clock-o'></i> <i>You delete device id:"+id+"</i>",
            color: "#648BB2",
            iconSmall: "fa fa-trash-o fa-2x fadeInRight animated",
            timeout: 5000
        });
        window.location.href = "#ajax/device_list.html/grid";
    }

    function initial() {
        var id = device_info.id;
        contentString = '<div id="info-map" style="width:300px; height:85px; padding:0px;">' + '<div>' + '<div style="display:inline-block; width:86px; verticle-align:top; float:left;">' + '<img src="' + device_info.deviceTypeIconUrl + '" class="thumbnail" style="width:80%; verticle-align:top;" />' + '</div>' + '<div style="display:inline-block; width:200px; float:left;">' + '<h4>' + device_info.name + '</h4>' + '<b>' + device_info.latestConnectTime + '</b>' + '<br/>' + '<p>' + '<a href="#ajax/device_detail.html/' + device_info.id + '" class="btn btn-xs btn-default pull-right">' + '<i class="fa fa-fw fa-map-marker"></i>More Info' + '</a>' + '</p>' + '</div>' + '</div>' + '</div>';

        $("#deviceName font").text(device_info.name);
        $("#deviceName").attr("href", "#" + AD.page + "/" + id);

        $("#device_detail_body").on("click", "#myTab1 a", function(event) {

            var href = $(event.target).attr("href");
            sessionStorage.setItem('sWebPageDeviceTabName', href);
            if (typeof href == "undefined") {
                href = $(event.target).parent().attr("href");
            }

            if (href === "#s1") {
                s1();
            } else if (href === "#s2") {
                s2();
            } else if (href === "#s3") {
                s3();
            } else if (href === "#s4") {
                s4(id);
            } else if (href === "#s5") {
                s5(id);
            } else if (href === "#s6") {
                s6(id);
            } else if (href === "#s7") {
                s7(id);
            } else if (href === "#editDevice") {
                editDevice(id);
                event.preventDefault();
            } else if (href === "#lock") {
                lock(true);
                event.preventDefault();
            } else if (href === "#unlock") {
                lock(false);
                event.preventDefault();
            } else if (href === "#reset") {
                reboot(false);
                event.preventDefault();
            } else if (href === "#restart") {
                reboot(true);
                event.preventDefault();
            } else if (href === "#firmwareUpgrade") {
                firmwareUpgrade();
                event.preventDefault();
            } else if (href === "#agentUpgrade") {
                agentUpgrade();
                event.preventDefault();
            } else if (href === "#systemUpdate") {
                systemUpdate();
                event.preventDefault();
            } else if (href === "#deleteDevice") {
                deleteDevice(id);
                event.preventDefault();
            } else if (href == "#remoteDesktop") {
                //window.open("rd_demo.html");  //for demo
                startRemoteDesktop(id);
                event.preventDefault();
            }
        });

        // Initial page
        s1();

        $("#googleSelected").on("click", function(event) {
            $('#amap').css("display", "none");
            $('#map').css("display", "");
            sMapSelected = "Google Maps";
            //gMap = fnCreateGoogleMap();
            //amap();
            $.getJSON($.config.server_rest_url + '/devices/' + id + '/capabilities/GPS',
                function(data) {
                    var deviceLocation = {};
                    for (var i = 0; i < data.nodeBeanList.length; i++)
                        eval('deviceLocation.' + data.nodeBeanList[i].name + '="' + data.nodeBeanList[i].value + '"');
                    if (_.has(deviceLocation, 'Latitude') && _.has(deviceLocation, 'Longitude'))
                        fnRefreshMap(deviceLocation);
                }
            );
        });

        $("#amapSelected").on("click", function(event) {
            $('#map').css("display", "none");
            $('#amap').css("display", "");
            sMapSelected = "AMap";
            //gMap = fnCreateGoogleMap();
            //amap();
            $.getJSON($.config.server_rest_url + '/devices/' + id + '/capabilities/GPS',
                function(data) {
                    var deviceLocation = {};
                    for (var i = 0; i < data.nodeBeanList.length; i++)
                        eval('deviceLocation.' + data.nodeBeanList[i].name + '="' + data.nodeBeanList[i].value + '"');
                    if (_.has(deviceLocation, 'Latitude') && _.has(deviceLocation, 'Longitude'))
                        fnRefreshMap(deviceLocation);
                }
            );
        });

        // Scan GPS
        $('#refresh-gps').hover(function() {
            $(this).css('cursor', 'pointer');
        });
        $("#refresh-gps").on("click", function(event) {
            if ($("#refresh-gps i").hasClass("fa-spin")) {
                console.warn('Action has been canceled, because the previous process is running.');
                return;
            }

            $("#refresh-gps i").addClass("fa-spin");
            $.ajax({
                url: $.config.server_rest_url + '/devices/' + id + '/agentCapabilities/GPS',
                type: 'GET',
                // dataType: 'json',
                timeout: 10000
            }).done(function(data) {
                //if(data.onlineStatus){
                /*if (data.content[0].state){
                    var deviceLocation = {};
                    for(var i=0;i<data.content[0].baseModelBeanList.length;i++)
                        eval('deviceLocation.'+data.content[0].baseModelBeanList[i].label+'="'+data.content[0].baseModelBeanList[i].value+'"');
                    if(_.has(deviceLocation,'Latitude')&&_.has(deviceLocation,'Longitude'))
                        fnRefreshMap(deviceLocation);
                    device_info.latestConnectTime = data.content[0].lastScanTime;
                }*/
                $.ajax({
                        url: $.config.server_rest_url + '/devices/' + device_info.id,
                        type: 'GET',
                        dataType: 'json',
                        timeout: 10000
                    })
                    .done(function(device) {
                        device_info = device;
                        //s1();
                        fnGetDeviceSummary(device_info.id);
                    });
            }).fail(function() {
                $.smallBox({
                    title: i18n.t("ns:Message.ErrorOccurred"),
                    content: "<i>" + i18n.t("ns:Message.ErrorTryAgain") + "</i>",
                    // title : "An error occurred on the server",
                    // content : "<i>Please try again in a minute.</i>",
                    color: "#B36464",
                    iconSmall: "fa fa-times bounce animated",
                    timeout: 5000
                });
            }).always(function() {
                $("#refresh-gps i").removeClass("fa-spin");
            });
        });

        // Scan App Inventory
        $("#inventory_scan").hover(function() {
            //console.log("inventory_scan mouse hover");
            $(this).css('cursor', 'pointer');
        });

        $("#inventory_scan").on("click", function(event) {
            if ($("#inventory_scan i").hasClass("fa-spin")) {
                console.warn('Action has been canceled, because the previous process is running.');
                return;
            }

            $("#refreshDiv").css("display", "block");
            $("#inventory_scan i").addClass("fa-spin");
            $.ajax({
                url: $.config.server_rest_url + '/devices/' + id + '/actions/scanAppInventory',
                type: 'POST',
                // dataType: 'json',
                timeout: 15000
            }).done(function(data) {
                s2();
                $.smallBox({
                    title: " " + i18n.t("ns:Message.Device.Device"),
                    content: "<i class='fa fa-check fa-inverse'></i> <i>" + i18n.t("ns:Message.Device.ScanAppSuccess"),
                    // title: " Device",
                    // content: "<i class='fa fa-check fa-inverse'></i> <i>Scan apps successfully.",
                    color: "#648BB2",
                    iconSmall: "fa fa-refresh fa-inverse fa-2x fadeInRight animated",
                    timeout: 5000
                });
            }).fail(function() {
                $.smallBox({
                    title: i18n.t("ns:Message.ErrorOccurred"),
                    content: "<i>" + i18n.t("ns:Message.ErrorTryAgain") + "</i>",
                    // title : "An error occurred on the server",
                    // content : "<i>Please try again in a minute.</i>",
                    color: "#B36464",
                    iconSmall: "fa fa-times bounce animated",
                    timeout: 5000
                });
            }).always(function() {
                $("#inventory_scan i").removeClass("fa-spin");
                $("#refreshDiv").css("display", "none");
            });
        });
        //Al20150121
        $("#s1").on("click", ".btn-agentUpgrade", function(event) {
            agentUpgrade();
        });

        $("#s2").on("click", "#activate", function(event) {
            console.log("activate");
            event.preventDefault();
            var $target = $(event.target);
            var originalData = {
                id: $target.data("id"),
                method: "activate",
                appName: $target.data("name"),
                appVersion: $target.data("version"),
                //pkgID: $target.data("pkgid"),
                packageId: $target.data("packageid"),
                appFileUrl: $target.data("fileurl"),
                isServerInstall: $target.data("serverinstall")
            };
            am_operation(originalData, 'fa-play');
        });

        $("#s2").on("click", "#deactivate", function(event) {
            console.log("deactivate");
            event.preventDefault();
            var $target = $(event.target);
            //var html = $target.html();
            var originalData = {
                id: $target.data("id"),
                method: "deactivate",
                appName: $target.data("name"),
                appVersion: $target.data("version"),
                //pkgID: $target.data("pkgid"),
                packageId: $target.data("packageid"),
                appFileUrl: $target.data("fileurl"),
                isServerInstall: $target.data("serverinstall")
            };
            am_operation(originalData, 'fa-pause');
        });

        /*$("#s2").on("click", "#block", function(event) {
            console.log("block");
            console.log("deactivate");
            event.preventDefault();
            var $target = $(event.target);
            //var html = $target.html();
            var originalData = {
                id: $target.data("id"),
                method: "block",
                appName: $target.data("name"),
                appVersion: $target.data("version"),
                //pkgID: $target.data("pkgid"),
                packageId: $target.data("packageid"),
                appFileUrl: $target.data("fileurl"),
                isServerInstall: $target.data("serverinstall")
            };
            am_operation(originalData,'fa-minus-circle');
        });

        $("#s2").on("click", "#unblock", function(event) {
            console.log("unblock");
            console.log("deactivate");
            event.preventDefault();
            var $target = $(event.target);
            //var html = $target.html();
            var originalData = {
                id: $target.data("id"),
                method: "unblock",
                appName: $target.data("name"),
                appVersion: $target.data("version"),
                //pkgID: $target.data("pkgid"),
                packageId: $target.data("packageid"),
                appFileUrl: $target.data("fileurl"),
                isServerInstall: $target.data("serverinstall")
            };
            am_operation(originalData,'fa-check-circle');
        });*/

        $("#s2").on("click", "#delete", function(event) {
            event.preventDefault();
            var $target = $(event.target);
            if (!$target.is("a"))
                $target = $($target.parent("a"));
            var originalData = {
                method: "remove",
                id: $target.data("id"),
                appName: $target.data("name"),
                packageId: $target.data("packageid"),
                appVersion: $target.data("version"),
                isServerInstall: $target.data("serverinstall")
            };
            am_operation(originalData, 'fa-trash');
        });

        $("#s2").on("click", ".btn-install", function(event) {
            event.preventDefault();
            var $target = $(event.target);
            var originalData = {
                //DeviceId: id,
                method: "install",
                id: $target.data("id"),
                //appUID: 25,
                appName: $target.data("name"),
                appVersion: $target.data("version"),
                //pkgID: $target.data("pkgid"),
                packageId: $target.data("packageid"),
                appFileUrl: $target.data("fileurl"),
                isServerInstall: true
            };
            am_operation(originalData, 'fa-download');
        });

        function am_operation(originalData, icon) {
            if (_.isUndefined(originalData.appVersion) || originalData.appVersion == "") {
                var contentMessage = "";
            } else {
                var contentMessage = i18n.t("ns:Message.Device." + originalData.method) + " (" + originalData.appVersion + ")";
            };
            $.SmartMessageBox({
                title: "<i class='fa " + icon + " txt-color-orangeDark' style='font-size:32px;'></i> " + i18n.t("ns:Message.Device." + originalData.method) + " <span class='txt-color-orangeDark'><strong>" + originalData.appName + "</strong></span> ?",
                content: contentMessage,
                buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                    var data = JSON.stringify(originalData);
                    if (typeof(device_info.operatingSystemName) === "undefined") {
                        console.log("device.operatingSystemName is undefined");
                    } else {
                        switch (device_info.operatingSystemName) {
                            case "Windows":
                                var osName = "windowsApps/";
                                break;
                            case "Android":
                                var osName = "androidApps/";
                                break;
                            case "iPhone":
                                // TODO
                                break;
                        }

                        $("body").addClass("loading");
                        var url = $.config.server_rest_url + '/devices/' + id + '/' +
                            osName + originalData.id + '/actions/' + originalData.method;
                        $.ajax({
                            //url: 'http://220.130.176.238/AMDM/AM',
                            //url: $.config.server_rest_url+'/devices/' + id + osName + originalData.id + '/actions/'+originalData.method,
                            url: url,
                            type: 'POST',
                            dataType: 'json',

                            //headers: {"X-HTTP-Method-Override": "DELETE"},
                            crossDomain: true,
                            //data: data,
                            beforeSend: function(xhr, settings) {
                                if ((originalData.method === 'remove') && (originalData.isServerInstall === true)) {
                                    deletePayload = {};
                                    deletePayload.packageId = originalData.packageId;
                                    settings.data = JSON.stringify(deletePayload);
                                    settings.contentType = "application/json";
                                    xhr.setRequestHeader("Content-Type", settings.contentType);
                                }
                            },
                            timeout: 10000,
                            error: function(data, status, error) {
                                $.smallBox({
                                    title: i18n.t("ns:Message.ErrorOccurred"),
                                    content: "<i>" + i18n.t("ns:Message.ErrorTryAgain") + "</i>",
                                    // title : "An error occurred on the server",
                                    // content : "<i>Please try again in a minute.</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-times bounce animated",
                                    timeout: 6000
                                });
                            }
                        }).done(function(data) {
                            if (data.status === false) {
                                var content = "";
                                if (data.remark !== "null") {
                                    content = data.error;
                                }
                                $.smallBox({
                                    title: i18n.t("ns:Message.Device." + originalData.method) + i18n.t("ns:Message.Device.App"),
                                    // title : originalData.method+" Fail",
                                    content: "[" + originalData.appName + "]" + i18n.t("ns:Message.Device.SendToServerFailed") + "<i>" + content + "</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-times bounce animated",
                                    timeout: 6000
                                });
                            } else {
                                $.smallBox({
                                    title: i18n.t("ns:Message.Device." + originalData.method) + i18n.t("ns:Message.Device.App"),
                                    content: "<i class='fa fa-check fa-inverse'></i> <i>" + "[" + originalData.appName + "]" + i18n.t("ns:Message.Device.SendToServerSuccess") + "</i>",
                                    // title: " Device",
                                    // content: "<i class='fa fa-check fa-inverse'></i> <i>The task to "+originalData.method+" app ["+originalData.appName+"] is sent successfully.</i>",
                                    color: "#648BB2",
                                    iconSmall: "fa " + icon + " fa-inverse fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                                // Wait comet (Timeout 10 seconds)
                                sessionStorage.setItem("isDevice_" + id + "_FinishAMTask", false);
                                setTimeout(function() {
                                    checkTaskFinish(id);
                                }, 10 * 1000);
                            }
                            // s2(id);
                        }).fail(function() {
                            console.log("error");
                        }).always(function() {
                            console.log("complete");
                            $("body").removeClass("loading");
                        });
                    };
                } else {
                    switch (originalData.method) {
                        case 'active':
                            // com.iii.dmagent => org\.iii\.dmagent
                            originalData.packageId = originalData.packageId.replace(/\./g, "\\.");
                            var status = $("#st" + originalData.packageId).prop("checked");
                            $("#st" + originalData.packageId).prop("checked", !status);
                            break;
                        case 'inactive':
                            originalData.packageId = originalData.packageId.replace(/\./g, "\\.");
                            var status = $("#st" + originalData.packageId).prop("checked");
                            $("#st" + originalData.packageId).prop("checked", !status);
                            break;
                        default:
                            break;
                    }

                }
            });
        }

        function checkTaskFinish(id) {
            var isDeviceFinishTask = sessionStorage.getItem("isDevice_" + id + "_FinishAMTask");
            if (!_.isUndefined(isDeviceFinishTask)) {
                if (isDeviceFinishTask === "false") {
                    checkDeviceLastTask(id);
                    //sessionStorage.removeItem("isDevice_"+id+"_FinishAMTask");
                }
            }
        }

        function checkDeviceLastTask(id) {
            //"http://url:8080/mdm-core/RESTful/devices/{deviceId}/tasks?page=1&size=1&sortDir=desc&sortField=taskId"
            var url = $.config.server_rest_url + '/devices/' + id + "/tasks?page=1&size=1&sortDir=desc&sortField=taskId";
            $.getJSON(url, function(result) {
                if (result.length > 1) {
                    var taskStatus = result[0].state.toLowerCase();
                    var subtaskId = result[0].id;
                    var subtask = {};
                    subtask.id = subtaskId;
                    subtask.count = 0;
                    sessionStorage.setItem("device_" + id + "_SubtaskId", JSON.stringify(subtask));
                    taskExecutionHandler(id, taskStatus, subtaskId);
                }
            });
        }

        function checkTaskLoop(id, subtaskId) {
            var isDeviceFinishTask = sessionStorage.getItem("isDevice_" + id + "_FinishAMTask");
            if (!_.isUndefined(isDeviceFinishTask)) {
                if (isDeviceFinishTask === "false") {
                    var subtask = sessionStorage.getItem("device_" + id + "_SubtaskId");
                    if (!_.isUndefined(subtask)) {
                        subtask = JSON.parse(subtask);
                        if (subtask.count < 10) {
                            subtask.count++;
                            sessionStorage.setItem("device_" + id + "_SubtaskId", JSON.stringify(subtask));
                            var url = $.config.server_rest_url + '/subtasks/' + subtaskId;
                            $.getJSON(url, function(result) {
                                var taskStatus = result.state.toLowerCase();
                                taskExecutionHandler(id, taskStatus, subtaskId);
                            });
                        } else {
                            sessionStorage.removeItem("device_" + id + "_SubtaskId");
                        }
                    }
                }
            }
        }

        function taskExecutionHandler(id, taskStatus, subtaskId) {
            switch (taskStatus) {
                case "completed":
                    $("#refreshDiv").css("display", "block");
                    $("#inventory_scan i").addClass("fa-spin");
                    $.ajax({
                        url: $.config.server_rest_url + '/devices/' + id + '/actions/scanAppInventory',
                        type: 'POST',
                        // dataType: 'json',
                        timeout: 15000
                    }).done(function(data) {
                        s2();
                        $("#inventory_scan i").removeClass("fa-spin");
                        $("#refreshDiv").css("display", "none");
                    }).fail(function() {});
                    sessionStorage.removeItem("isDevice_" + id + "_FinishAMTask");
                    sessionStorage.removeItem("device_" + id + "_SubtaskId");
                    break;
                case "failed":
                    console.log("Device task execution error");
                    sessionStorage.removeItem("isDevice_" + id + "_FinishAMTask");
                    sessionStorage.removeItem("device_" + id + "_SubtaskId");
                    break;
                case "timeout":
                    console.log("Device task execution timeout");
                    sessionStorage.removeItem("isDevice_" + id + "_FinishAMTask");
                    sessionStorage.removeItem("device_" + id + "_SubtaskId");
                    break;
                default:
                    console.log("check task:" + subtaskId + " status");
                    setTimeout(function() {
                        checkTaskLoop(id, subtaskId);
                    }, 10 * 1000);
                    break;
            }
        }

        //Intro
        function startIntro() {
            var intro = introJs();
            intro.setOptions({
                steps: [{
                    //element: document.querySelector('#myTab1 > li:eq(0)), //'#myTab1 .active'
                    element: document.querySelector("#myTab1 a[href$='#s1']"),
                    intro: "Show detail information about this device.",
                    position: 'right'
                }, {
                    element: document.querySelector("#myTab1 a[href$='#s2']"),
                    intro: "You can install App to this device.",
                    position: 'bottom'
                }, {
                    element: document.querySelector("#myTab1 a[href$='#s3']"),
                    intro: "Or setting peripheral of this device.",
                    position: 'bottom'
                }, {
                    //element: document.querySelector('#myTab1 .pull-right'),
                    element: document.querySelector("#myTab1 .pull-right > div"),
                    intro: "Do something about this device.",
                    position: 'left'
                }, {
                    element: document.querySelector("#myTab1 a[href$='#s5"),
                    intro: "Your management action results will be listed here.",
                    position: 'bottom'
                }, {
                    element: document.querySelector("#myTab1 a[href$='#s6"),
                    intro: "Your management action results will be listed here.",
                    position: 'bottom'
                }, {
                    element: document.querySelector("#myTab1 a[href$='#s7"),
                    intro: "Your management action results will be listed here.",
                    position: 'bottom'
                }, {
                    intro: "Congratulations! Let's start.",
                }]
            });

            intro.start();
        }
    }

    function useIpToFindLocation() {
        $.getJSON("http://freegeoip.net/json/", function(data) {
            var country_code = data.country_code;
            var country = data.country_name;
            var ip = data.ip;
            var time_zone = data.time_zone;
            var latitude = parseFloat(data.latitude);
            var longitude = parseFloat(data.longitude);

            myLatlng = [
                ['AMobile', latitude, longitude]
            ];
            console.log("latitude : " + latitude + " longitude : " + longitude);
            fnCreateGoogleMap();
            amap(latitude, longitude);
        }).fail(function() {
            console.log("test error");
            fakeImage();
        });
    }

    function success() {
        var latitude = position.coords.latitude;
        var longitude = position.coords.longitude;
        console.log("lat : " + latitude + " long : " + longitude);

    }

    function error() {
        console.log("Unable to retrieve your location");
        //output.innerHTML = "Unable to retrieve your location";
    };
    var myLatlng;

    function fnCreateGoogleMap() {
        // navigator.geolocation.getCurrentPosition(success);
        // var myLatlng = [
        //     ['AMobile', 24.998666, 121.487833]
        // ];
        if (typeof(myLatlng) == "undefined") {
            myLatlng = [
                ['AMobile', 24.998666, 121.487833]
            ];
        }
        console.log("fnCreateGoogleMap");
        var map;
        try {
            console.log("latitude : " + myLatlng[0][1] + " longitude : " + myLatlng[0][2]);
            var latlng = new google.maps.LatLng(myLatlng[0][1], myLatlng[0][2]); //¥x?W®y¼?

            var myOptions = {
                zoom: 13,
                center: latlng,
                mapTypeId: google.maps.MapTypeId.TERRAIN,
                streetViewControl: true
            };

            map = new google.maps.Map(document.getElementById("map"), myOptions);
            map.mapTypes.set('metro_style', metroStyleMap);
            map.setMapTypeId('metro_style');
            var infowindow = new google.maps.InfoWindow();
            google.maps.event.addDomListener(window, 'resize', function() {
                var center = map.getCenter();
                google.maps.event.trigger(map, 'resize');
                map.setCenter(center);
            });
            return map;
        } catch (err) {
            console.log("fnCreateGoogleMap err : " + err);
            // $('#map').prepend('<img id="fakeimg" src="img/test_img/fake_map.png" style="width:360px;height:360px"/>')
            return map;
        } finally {
            //code for finally block
        }
    };

    function amap(latitude, longitude) {
        if (typeof AMap !== 'undefined') {
            amMap = new AMap.Map('amap', {
                resizeEnable: true,
                zoom: 13,
                center: [longitude, latitude]
            });
            AMap.plugin(['AMap.ToolBar', 'AMap.Scale'], function() {
                var toolBar = new AMap.ToolBar();
                var scale = new AMap.Scale();
                amMap.addControl(toolBar);
                amMap.addControl(scale);
            });

            // map.plugin('AMap.Geolocation', function()
            // {
            //     geolocation = new AMap.Geolocation(
            //     {
            //         timeout: 5000, //超过10秒后停止定位，默认：无穷大
            //     });
            //     map.addControl(geolocation);
            // });
        }
    }

    AD.bootstrap = function() {
        mapReady = false;
        i18n.init(function(t) {
            $('[data-i18n]').i18n();
            /*
            var pageName = window.location.href.replace(/^.+#ajax\//,"").replace(/\.html(.+)/,"");
            var pageValue = getCookie(pageName);
            if(pageValue===""){
                setCookie(pageName,"done",0);
                $.SmartMessageBox({
                    title: "<i class='fa fa-trash-o txt-color-orangeDark'></i> This is the first time you use, <span class='txt-color-orangeDark'>to begin tips </span> ?",
                    content: "These tips can help you get started quickly!",
                    buttons: '[Skip][Yes]'

                }, function(ButtonPressed) {
                    if (ButtonPressed == "Yes") {
                        startIntro();
                    }

                });
                //startIntro();
            }
            */
        });
        $("body").removeClass("loading");
        /*
        $("#myTab1").resize(function(){
            var width = $(this).height();
            if(width>40){
                $('body').addClass("minified");
            }
        });
        */

        var p = $("#left-panel nav a[href='ajax/device_detail.html']").parents("li");
        $(p[0]).addClass("active");
        $(p[1]).find("ul").show();
        $(p[1]).addClass("active open");

        if (sMapSelected == "Google Maps") {
            $('#amap').css("display", "none");
            $('#map').css("display", "");
        } else if (sMapSelected == "AMap") {
            $('#map').css("display", "none");
            $('#amap').css("display", "");
        }

        // 網址列＃號以後的字串, ex, #ajax/device_detail.html/331
        var hash = window.location.hash;
        // device ID, ex, 331
        var id = hash.replace(/^#ajax\/device_detail.html\/(\w+)$/, "$1");

        //console.log("id=[" + id + "]");

        $.ajax({
            url: $.config.server_rest_url + '/devices/' + id,
            type: 'GET',
            dataType: 'json',
            timeout: 10000,
            error: function(data, status, error) {}
        }).done(function(data) {
            // Holisun
            sessionStorage.setItem('currentDeviceName', data.name);
            sessionStorage.setItem('oDevice', JSON.stringify(data));
            sessionStorage.setItem('sWebPageDeviceTabName', '#s1');
            sessionStorage.setItem('sWebPageDeviceRemoteDesktopStateIsNormal', 'false');
            sessionStorage.setItem('sVncState', 'unknown');
            device_info = data;
            initial();
            // Holisun
            //if (device_info.deviceTypeName == "G0550")
            // $("#deviceAction.dropdown-menu").append('<li><a href="#remoteDesktop"><i class="fa fa-laptop"></i><span data-i18n="ns:Device.Actions.RemoteDesktop">Remote Desktop</span></a></li>');
        }).fail(function() {});
        //startIntro();

    };

    $[AD.page] = AD;

    //var element_canvas = document.getElementById("noVNC_canvas_div");
    //element_canvas.onblur = fnConfirmRemoveVncConnection;


    Backbone.Events.on("VNC_Failed", function(state) {
        console.log("[VNC] XXXXXXXXXXXXXXXXX=" + state);
        sessionStorage.setItem('sWebPageDeviceRemoteDesktopStateIsNormal', 'false');
        $.smallBox({
            title: "Remote desktop closed.",
            content: "",
            color: "lightblue",
            timeout: 5000
        });
    });



    //fnDoFirmwareUpgrade150();
})(jQuery);

function deviceCapacitiveImgLoadError(t, name, color, icon) {
    $(t).replaceWith(
        "<div" + " class=\"faa-parent animated-hover\"" + " style=\"" + "-webkit-border-radius: 10px;  -moz-border-radius: 10px;  border-radius: 10px;" + "width: 148px;" + "height: 168px;" + "background-color: " + color + ";" //#3678DB;"
        + "text-align: center;" + "line-height: 229px;" + "margin: 0px auto;" + "\">" + "<i class=\"fa fa-inverse " + icon + " faa-tada\" style=\"font-size: 100px;\"></i>" + "</div>"
    );
}

function fnDoFirmwareUpgrade150() {
    // 韌體更新任務開始後，需幾分鐘後開始進行檢查？ (5)
    var iCheckPrefixMinute = 5;
    // 韌體更新任務開始後，每分鐘檢查，需檢查幾次？ (30)
    var iCheckTaskCount = 30;
    // Group 清單是實際順序，所以不是流水號，要特別注意
    var aGroupIdList = [77, 78, 79, 80, 81, 87, 83, 84, 85, 86, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107];
    //var aGroupIdList = [77, 78, 79];
    var aFirmwareFileIdList = [52, 53, 54];

    console.log('[DL150] 開始進行韌體更新 (150) .........');

    for (var i = 0; i < aGroupIdList.length; i++) {
        var iGroupId = aGroupIdList[i];
        var iFirmwareFileId = aFirmwareFileIdList[0];
        console.log('[DL150] iGroupId=[' + iGroupId + ']------------------------------------------------------------------------------[START]');
        console.log('[DL150] iFirmwareFileId=[' + iFirmwareFileId + ']');

        // 先取得目前 group task 的狀態
        var oTaskStatus = fnGetTaskStatusByLoop(23);
        if (oTaskStatus === null) {
            console.error('[DL150][' + iGroupId + '] 無法取得 group task 資料，只好跳過此 group 不處理。');
            break;
        }
        console.log('[DL150][' + iGroupId + '] ready=[' + oTaskStatus.Ready + '] running=[' + oTaskStatus.Running + '] Done=[' + oTaskStatus.Done + ']');

        console.log('[DL150][' + iGroupId + '] 韌體更新任務開始下達 .........');
        var bResult = fnDoTaskFirmwareUpgradeByLoop(iGroupId, iFirmwareFileId);
        if (bResult === null) {
            console.error('[DL150][' + iGroupId + '] 無法執行任務，只好跳過此 group 不處理。');
            break;
        }

        console.log('[DL150][' + iGroupId + '] 韌體更新任務已經完成下達');

        // 韌體更新任務開始後，需等幾分鐘後才開始進行檢查
        //sleep(iCheckPrefixMinute * 60);

        // 每隔 60 秒檢查執行結果，共檢查 30 次。若是發現 iDone 加 1，則跳出迴圈。若是累計至 1800 秒，則也是跳出迴圈。
        for (var j = 0; j < iCheckTaskCount; j++) {
            var oTaskStatusNow = fnGetTaskStatusByLoop(23);
            console.log('[DL150][' + iGroupId + '][' + j + '] [Check] Ready=[' + oTaskStatusNow.Ready + '] Running=[' + oTaskStatusNow.Running + '] Done=[' + oTaskStatusNow.Done + ']');
            // 需要修正的地方
            //if (oTaskStatusNow.Ready === 0 && oTaskStatusNow.Running === 0 && (oTaskStatusNow.Done - oTaskStatus.Done) === 1)
            if (oTaskStatusNow.Ready === 0 && oTaskStatusNow.Running === 0) {
                console.log('[DL150][' + iGroupId + '][' + j + '] 偵測到任務已經執行完畢, Break ，繼續處理下個 group。------------------------------------[END]');
                break;
            }
            // 需要修正的地方
            sleep(10);
            //sleep(60);
            if (j === (iCheckTaskCount - 1)) {
                console.warn('[DL150][' + iGroupId + '][' + j + '] 偵測到任務尚未執行完畢，但因為逾時，只好不管，繼續處理下個 group。------------------------------------[END]');
            }
        };
    };
    console.log('[DL150] 完成韌體更新 (150) OK');
}

function fnDoTaskFirmwareUpgrade(iGroupId, iFirmwareFileId) {
    var sUrl = $.config.server_rest_url + '/deviceGroups/' + iGroupId + '/firmwareFiles/' + iFirmwareFileId + '/actions/upgrade';
    // console.log('[DL150] sUrl=[' + sUrl + ']');
    var bResult = false;
    // 需要修正的地方
    /*
    $.ajax(
    {
        type: "POST",
        url: sUrl,
        contentType: 'application/json',
        dataType: "json",
        async: false
    }).done(function(data, textStatus, jqXHR)
    {
        if (data.status === true)
        {
            bResult = true;
        } else {
            console.error('[DL150] Do task failed (NWS failed)');
        }
    }).fail(function(jqXHR, textStatus, errorThrown)
    {
        console.error('[DL150] Do task failed (AJAX failed)');
    });
*/
    // 需要修正的地方
    bResult = true;
    return bResult;
}

// 此函式先不使用，畢竟重複執行韌體更新任務是很可怕的一件事。
// 考慮到網路不穩，確保能執行任務
function fnDoTaskFirmwareUpgradeByLoop(iGroupId, iFirmwareFileId) {
    // 每隔 30 秒檢查，需檢查幾次？ (2)
    var iCheckTaskCount = 2;
    var bResult = null;
    for (var i = 0; i < iCheckTaskCount; i++) {
        bResult = fnDoTaskFirmwareUpgrade(iGroupId, iFirmwareFileId);
        // console.log('[DL150][' + i + '] oTaskStatus=[' + oTaskStatus + ']');
        if (bResult !== null && bResult === true) {
            console.log('[DL150][' + i + '] 任務已經執行成功. Break');
            break;
        }
        sleep(30);
    };
    return bResult;
}

function fnGetTaskStatus(iGroupId) {
    var sUrl = $.config.server_rest_url + '/deviceGroups/' + iGroupId + '/taskExecutionStatus';
    // console.log('[DL150] sUrl=[' + sUrl + ']');
    var oTaskStatus = null;

    $.ajax({
        type: "GET",
        url: sUrl,
        contentType: 'application/json',
        dataType: "json",
        async: false
    }).done(function(data, textStatus, jqXHR) {
        oTaskStatus = data;
    }).fail(function(jqXHR, textStatus, errorThrown) {
        console.error('[DL150] Get task status failed');
    });

    return oTaskStatus;
}

// 考慮到網路不穩，確保能取得資料
function fnGetTaskStatusByLoop(iGroupId) {
    // 每隔 10 秒檢查，需檢查幾次？ (18)
    var iCheckTaskCount = 18;
    var oTaskStatus = null;
    for (var i = 0; i < iCheckTaskCount; i++) {
        oTaskStatus = fnGetTaskStatus(iGroupId);
        if (oTaskStatus !== null) {
            console.log('[DL150][' + i + '] 成功取得 task status. Break');
            break;
        }
        sleep(10);
    };
    return oTaskStatus;
}

function sleep(sec) {
    var time = new Date().getTime();
    while (new Date().getTime() - time < sec * 1000);
}
