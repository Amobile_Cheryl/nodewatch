(function($) {
    pageSetUp();

    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /*                  */

    var AD = {};
    AD.collections = {};
    var url = AD.url = $.config.server_rest_url+"/userLogs?search=action:login";
    var idAttribute = AD.idAttribute = "id";
    var dataUpperBound = 9999;
    AD.title = "User Log";
    AD.page = "ajax/dashboard.html";
    //AD.api = 'cctech';
    AD.isAdd = false;
	AD.sort = {
		"column": "id",
		"sortDir": "desc"
	};

    AD.validate = function() {
        return true;
    };

    AD.columns = [
        //columns2
        {
            title: 'ns:Log.UserLog.Name',
            property: 'userName',
            filterable: true,
            sortable: false,
            width: '250px'
        },
        //columns3
        /*
        {
            title: 'Action',
            property: 'action',
            //cellClassName: 'guiFunction',
            filterable: true,
            sortable: true,
            callback: function(o) {
                return $.config.GuiFunction[o[this.property]];
            }
        },
        */
        //columns4
        {
            title: 'Description',//'ns:Log.UserLog.Remark',
            property: 'remark',
            //cellClassName: 'remark',
            filterable: true,
            sortable: false,
            width: '500px'
        },
        //columns5
        {
            title: 'ns:Log.UserLog.Time',
            property: 'creationTime',
            //cellClassName: 'creationTime',
            filterable: true,
            sortable: true,
            width: '180px'
        }
    ];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });
    /* collection ends */
    AD.title = "ns:Menu.UserLog";
    var routes = {};
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        initialize: function(opt) {
            var t = this;
            t.options = opt;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);



            opt.gvs = opt.gvs || ".cctech-gv";
            t.$bt = $(opt.bts).first();
            t.$gv = $(opt.gvs).first();
            // console.error();

            // if ($('#datatable_tabletools_1_info').parent('div').hasClass('col-sm-6')) {
            //     $('#datatable_tabletools_1_info').parent('div').removeClass('col-sm-6');
            //     $('#datatable_tabletools_1_info').parent('div').addClass('col-sm-4');
            // }
            if (!t.gv) {
                t.gv = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(), //wait for impl
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: false,
                    sort: t.AD.sort,
                    filter: t.AD.filter,
                    buttons: t.AD.buttons,
                    per_page: 6,
                    AD: t.AD
                })

                ;
            } else {
                t.gv.refresh();
            }
			
			Backbone.Events.off("UserLogUpdate");
			Backbone.Events.on("UserLogUpdate", function(data){
				//console.log("UserLog need to refresh!");
				t.gv.refresh();
			});
            //add by Alvin to hide unused toolbar
            $(".cctech-gv .widget-body-toolbar").hide();
        }
    });
    
    function getTaskInfo() {
        // Piechart
        var taskTypes = ['ApplicationManagement','DeviceManagement'];
        
        for(var i=0;i<taskTypes.length;i++){
			var taskType = taskTypes[i];
            $.ajax({
                url: $.config.server_rest_url + '/taskStatistic?taskType=' + taskType,
                //url: $.config.server_rest_url+'/deviceGroups/'+id+'/taskExecutionStatus?type='+taskType,
                type: 'GET',
                dataType: 'json',
                async: false
            }).done(function(data) {
                var sliceColors = new Array();           
				// Clear Oldp
                $("#"+taskType.toLowerCase()+"_piechart").empty();
                $("#"+taskType.toLowerCase()+"_info").empty();
                // Render
                var taskData = new Array();
                var taskExecutionStatus = {};
                taskExecutionStatus.names = {};
                var i = 0;
                $.each(data,function(key,value){
                    if(key.toLowerCase() !== "total"){
                        taskData.push(value);
                        eval('taskExecutionStatus.names['+i+']="'+key+'";');
                        sliceColors.push(eval('$.config.ui.taskExecutionStatusColor.'+key.toLowerCase()));
                        i++;
                    }
                });
                $("#"+taskType.toLowerCase()+"_piechart").sparkline(taskData, {
                    type: 'pie',
                    width: '128px',
                    height: '128px',
                    //borderWidth: '0',
                    //borderColor: '#3E3E3E',
                    sliceColors: sliceColors,
                    tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)',
                    tooltipValueLookups: {
                        names: taskExecutionStatus.names
                    }
                });
				$("#"+taskType.toLowerCase()+"_piechart").bind('sparklineClick', function(ev){
					var sparkline = ev.sparklines[0],
						region = sparkline.getCurrentRegionFields(),
						clickTaskType = ev.target.id.replace("_piechart","");
					console.log("Clicked on:"+clickTaskType+" "+taskExecutionStatus.names[sparkline.getRegion()]);
					//taskExecutionStatus.names[sparkline.getRegion()]
				});
                $("#"+taskType.toLowerCase()+"_info").append("<ul></ul>");
                $("#"+taskType.toLowerCase()+"_info ul").css({"list-style":"none"});                
                for(var i=0;i<taskData.length;i++) {
                    /*switch (taskExecutionStatus.names[i]) {
                        case "Ready":           taskExecutionStatus.names[i] = i18n.t("ns:Dashboard.Ready");    break;
                        case "Progress":        taskExecutionStatus.names[i] = i18n.t("ns:Dashboard.Progress"); break;
                        case "CompleteSuccess": taskExecutionStatus.names[i] = i18n.t("ns:Dashboard.Success");  break;
                        case "CompleteFail":    taskExecutionStatus.names[i] = i18n.t("ns:Dashboard.Fail");     break;                        
                        default:                taskExecutionStatus.names[i] = "Unknow";                        break;
                    }*/
                    if (taskExecutionStatus.names[i] == "Ready")
                        taskExecutionStatus.names[i] = "Ready(Queue)";
                    $("#"+taskType.toLowerCase()+"_info ul").append("<li style=\"margin-bottom: 5px;\"><i class=\"fa fa-square\" style=\"color:"+sliceColors[i]+";margin-right: 5px;\"></i>"+taskExecuteStatusDisplay(taskExecutionStatus.names[i])+" :"+taskData[i]+"</li>");
                }
            }).fail(function(){          
                // TODO
                // $.smallBox({
                //     title: "[Dashboard] Got task info failed",
                //     content: "<i class='fa fa-hdd-o'></i> <i>Got task info failed</i>",
                //     color: "#B36464",
                //     iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                //     timeout: 5000
                // });
                // console.log("[Dashboard] Got task info failed.");
            });
        }
    };

    // Get status info.
    var getStatusInfo = function() {
        $("body").addClass("loading");
        
        var data;
        var statusInfo = [];

        // 取得現有devices info.
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/devices?isCount=true',
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got devices info failed. => " + data.responseText);
            }
        }).done(function(data){
            _.isUndefined(data.amount) ? (statusInfo[0] = "--") : (statusInfo[0] = data.amount);                        
        }).fail(function(){          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotDeviceInfoFailed"),
                content: "<i class='fa fa-hdd-o'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotDeviceInfoFailed") + "</i>",
                // title: "[Dashboard] Got devices info failed",
                // content: "<i class='fa fa-hdd-o'></i> <i>Got devices info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Dashboard] Got devices info failed.");
        }).always(function(){
            $("body").removeClass("loading");            
        });

        // 取得現有tasks info.
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/tasks?isCount=true',
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got tasks info failed. => " + data.responseText);
            }
        }).done(function(data){        
            _.isUndefined(data.amount) ? (statusInfo[1] = "--") : (statusInfo[1] = data.amount);                        
        }).fail(function(){          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotTaskInfoFailed"),
                content: "<i class='fa fa-tasks'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotTaskInfoFailed") + "</i>",
                // title: "[Dashboard] Got tasks info failed",
                // content: "<i class='fa fa-tasks'></i> <i>Got tasks info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Dashboard] Got tasks info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        });

        // 取得現有windows apps info.
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/windowsAppFiles?isCount=true',
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got windows apps info failed. => " + data.responseText);
            }
        }).done(function(data){        
            _.isUndefined(data.amount) ? (statusInfo[2] = "--") : (statusInfo[2] = data.amount);                        
        }).fail(function(){          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotWindowsAppInfoFailed"),
                content: "<i class='fa fa-cloud-download'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotAppInfoFailed") + "</i>",
                // title: "[Dashboard] Got windows apps info failed",
                // content: "<i class='fa fa-cloud-download'></i> <i>Got apps info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Dashboard] Got windows apps info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        });

        // 取得現有android apps info.
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/androidAppFiles?isCount=true',
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got android apps info failed. => " + data.responseText);
            }
        }).done(function(data){        
            _.isUndefined(data.amount) ? (statusInfo[3] = "--") : (statusInfo[3] = data.amount);                        
        }).fail(function(){          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotAndroidAppInfoFailed"), 
                content: "<i class='fa fa-cloud-download'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotAppInfoFailed") + "</i>",
                // title: "[Dashboard] Got android apps info failed",
                // content: "<i class='fa fa-cloud-download'></i> <i>Got apps info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Dashboard] Got android apps info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        });

        // 取得現有firmware files info.
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/firmwareFiles?isCount=true',
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got firmware apps info failed. => " + data.responseText);
            }
        }).done(function(data){
            _.isUndefined(data.amount) ? (statusInfo[4] = "--") : (statusInfo[4] = data.amount);                        
        }).fail(function(){          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotFirmwareAppInfoFailed"),
                content: "<i class='fa fa-cloud-download'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotAppInfoFailed") + "</i>",
                // title: "[Dashboard] Got firmware apps info failed",
                // content: "<i class='fa fa-cloud-download'></i> <i>Got apps info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Dashboard] Got firmware apps info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        });

        // 取得現有firmware files info.
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/omaAgentFiles?isCount=true',
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got agent files info failed. => " + data.responseText);
            }
        }).done(function(data){
            _.isUndefined(data.amount) ? (statusInfo[5] = "--") : (statusInfo[5] = data.amount);                        
        }).fail(function(){          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotFirmwareAppInfoFailed"),
                content: "<i class='fa fa-cloud-download'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotAppInfoFailed") + "</i>",
                // title: "[Dashboard] Got firmware apps info failed",
                // content: "<i class='fa fa-cloud-download'></i> <i>Got apps info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Dashboard] Got firmware apps info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        });

        // 取得現有users info.
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/onlineUserCount',
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got users info failed. => " + data.responseText);
            }
        }).done(function(data){        
            _.isUndefined(data.amount) ? (statusInfo[6] = "--") : (statusInfo[6] = data.amount);                        
        }).fail(function(){          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotUserInfoFailed"),
                content: "<i class='fa fa-group'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotUserInfoFailed") + "</i>",
                // title: "[Dashboard] Got users info failed",
                // content: "<i class='fa fa-group'></i> <i>Got users info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Dashboard] Got users info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        });
        
        if (typeof(showStatusBox) === "undefined" || showStatusBox === null) {
            statusInfo = ["--","--","--","--","--","--","--"];
            showStatusBox(statusInfo);
        } else {
            showStatusBox(statusInfo);
        };
    };

    // Show the status of devices/tasks/apps/users
    var showStatusBox = function(statusInfo) {        
        var StatusBox = Backbone.View.extend({
            initialize: function(options){
                this.render(options);
            },
            render: function(options){
                var tpl_html = 
                    '<div id="" class="col-xs-6 col-sm-6 col-md-6 col-lg-3" style="height: 110px; ">' //min-width: 230px;
                        + '<div id="{{id}}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="'
                            +'background-color: {{color}};'
                            //+'height: 110px;'
                            //+'width: 280px;'
                            //+'margin-right: 30px;'
                        +'">'
                            +'<div class="row" style="'
                                + 'height: 80px;'                                
                            +'">'
                                +'<div class="col-xs-8 col-sm-7 col-md-7 col-lg-7" style="'
                                    //+'text-align: center;'
                                    +'color: white;'
                                    +'height: 80px;'
                                +'">'
                                    +'<span id={{index}} title={{transformat data}} style="font-size: 52px;line-height: 52px;">'
                                        +'{{transformat data}}'
                                    +'</span>'
                                    +'<span id="plus-{{index}}" style="font-size: 52px;line-height: 52px;margin-left: -4px;">'                                        
                                    +'</span>'
                                    +'<br />'
                                    +'<span data-i18n="ns:Dashboard.' + '{{desc}}"' + '>'
                                        +'{{desc}}'
                                    +'</span>'
                                +'</div>'
                                +'<div class="col-xs-4 col-sm-5 col-md-5 col-lg-5" style="'
                                    +'text-align: center;'
                                    +'vertical-align: middle;'
                                    +'line-height: 85px;'
                                    +'font-size: 65px;'
                                    +'color: rgba(0,0,0,0.4);'
                                +'">'
                                    +'<i id="icon-{{index}}" class="fa {{fa-icon}}"></i>'
                                +'</div>'
                            +'</div>'
                            +'<div class="row" style="'
                                +'text-align: center;'
                                +'vertical-align: middle;'
                                +'background-color: rgba(96,96,96,0.3);'
                                +'height: 30px;'
                                +'line-height: 30px;'
                                +'color: white;'
                            +'"><a id="link-{{index}}" href="{{url}}" style="color: white;" data-i18n="ns:Message.MoreInfo">More Info <i class="fa fa-chevron-circle-down"></i></a>'
                            +'</div>'
                        +'</div>'
                    +'</div>';

                Handlebars.registerHelper("transformat", function(data) {
                   return new Handlebars.SafeString(data);
                });
                
                var template = Handlebars.compile(tpl_html);
                this.$el.append(template(options));
            }
        });

        var fileAmountInfo = function(){
            var fileAmount = 0;
            var undefinedFlag = 0;
            for (var i = 2; i <= 5; i++) {
                if (statusInfo[i] === "--") {
                    undefinedFlag++;                    
                } else {
                    fileAmount += statusInfo[i];
                };                
            };

            if (undefinedFlag === 3) {
                return "--";
            } else {
                return fileAmount;
            };            
        };

        var boxes = [
            {
                // Devices
                el: $("#status-grid"),
                'index':   'status0', 
                'color':   '#95a5a6',
                'data':    statusInfo[0],
                'desc':    'Devices',
                'fa-icon': 'fa-hdd-o',
                'url':     '#ajax/device_list.html'
            },
            {
                // Tasks
                el: $("#status-grid"),
                'index':   'status1', 
                'color':   '#baa668',
                'data':    statusInfo[1],
                'desc':    'Tasks',
                'fa-icon': 'fa-tasks',
                'url':     '#ajax/task_list.html'
            },
            {
                // Files
                el: $("#status-grid"),
                'index':   'status2', 
                'color':   '#3498db',                
                'data':    fileAmountInfo(),
                'desc':    'TotalFiles',
                'fa-icon': 'fa-windows',
                'url':     '#ajax/app_list.html'
            },
            {
                // Users
                el: $("#status-grid"),
                'index':   'status3', 
                'color':   '#f1c40f',
                'data':    statusInfo[6],
                'desc':    'OnlineUsers',
                'fa-icon': 'fa-group',
                'url':     '#ajax/user_list.html'
            }
        ];
        var statusBoxex = new Array();
        $.each(boxes,function(index){
            statusBoxex.push(new StatusBox(boxes[index]));
            switch (index) {
                case 0:           
                    if (statusInfo[0] > dataUpperBound) {
                        $("#status0").css('margin-left','-7px');
                        $("#plus-status0").html("+");
                        $("#plus-status0").css('margin-left','-4px');
                        $("#icon-status0").css('margin-left','12px');
                    };
                    if (!$.login.user.checkRole('DeviceViewer')) {
                        $("#link-status0").removeAttr("href");
                    };
                    break;
                case 1:
                    if (statusInfo[1] > dataUpperBound) {
                        $("#status1").css('margin-left','-7px');
                        $("#plus-status1").html("+");
                        $("#plus-status1").css('margin-left','-4px');
                        $("#icon-status1").css('margin-left','12px');
                    };
                    if (!$.login.user.checkRole('TaskViewer')) {
                        $("#link-status1").removeAttr("href");
                    };
                    break;
                case 2: 
                    if (statusInfo[2] + statusInfo[3] + statusInfo[4] + statusInfo[5]> dataUpperBound) {
                        $("#status2").css('margin-left','-7px');
                        $("#plus-status2").html("+");
                        $("#plus-status2").css('margin-left','-4px');
                        $("#icon-status2").css('margin-left','12px');
                    };
                    if (!$.login.user.checkRole('AppFileViewer') && !$.login.user.checkRole('FirmwareFileViewer')) {
                        $("#link-status2").removeAttr("href");
                    };
                    break;
                case 3:
                    if (statusInfo[6] > dataUpperBound) {
                        $("#status3").css('margin-left','-7px');
                        $("#plus-status3").html("+");
                        $("#plus-status3").css('margin-left','-4px');
                        $("#icon-status3").css('margin-left','12px');
                    };
                    if (!$.login.user.checkRole('UserLogViewer')) {
                        $("#link-status3").removeAttr("href");
                    };
                    break;                                        
            } 
        });               
    }
    
    AD.bootstrap = function() {
        i18n.init(function(t){
            $('[data-i18n]').i18n();            
        });        
        
        // User log permission
        if ($.login.user.checkRole('UserLogViewer')) {
            $("#userlog").removeAttr("style")            
        } else {
            $("#userlog").attr("style","display:none;");    
        };

        // Get status info
        getStatusInfo();
        // Get task info
        //getTaskInfo();
		//setInterval(getTaskInfo,5000); // Auto-Refresh
        // Get event info
        //getEventInfo(); 

        if ($("#bar-chart").length) 
        {      
            $.ajax({
                url: $.config.server_rest_url+'/deviceGroups?pageNumber=1&pageSize=100&pageSort=name%2Casc',
                dataType: 'JSON',
                success: function(groupsCollection) {
                    var maxValue = 10;
                    var label = [];
                    var value = [];
                    var readyTask = {};
                    readyTask.data = [];
                    readyTask.bars = {show : true, barWidth : 0.1, order : 1};
                    var runningTask = {};
                    runningTask.data = [];
                    runningTask.bars = {show : true, barWidth : 0.1, order : 2};
                    var doneTask = {};
                    doneTask.data = [];
                    doneTask.bars = {show : true, barWidth : 0.1, order : 3};
                    for (var i = 0; i < groupsCollection.content.length; i++)
                    {
                        $.ajax({
                            url: $.config.server_rest_url+'/deviceGroups/'+groupsCollection.content[i].id+'/taskExecutionStatus',
                            dataType: 'JSON',
                            async: false,
                            success: function(taskStatus) {
                                label.push([i, groupsCollection.content[i].name]);
                                readyTask.data.push([i, taskStatus.Ready]);
                                runningTask.data.push([i, taskStatus.Running]);
                                //doneTask.data.push([i, taskStatus.Done]);

                                if (maxValue > taskStatus.Ready)
                                    maxValue = taskStatus.Ready;
                                if (maxValue > taskStatus.Running)
                                    maxValue = taskStatus.Running;
                                if (maxValue > taskStatus.Done)
                                    maxValue = taskStatus.Done;
                            }
                        });
                    }

                    /*var options = {
                        colors : ["#2E7BCC", "#FFA500"],
                        series: {stack: 0,
                            lines: {show: false, steps: false },
                            bars: {show: true, barWidth: 0.9, align: 'center',},
                        },
                        xaxis: {ticks: [[1,'One'], [2,'Two'], [3,'Three'], [4,'Four'], [5,'Five']]},
                        tooltip : true,
                        tooltipOpts : {
                            content : "<b>%x</b> = <span>%y</span>",
                            defaultTheme : false
                        }
                    };*/

                    var statusOptions = {
                        colors : ["#2E7BCC", "#FFA500", "#66AA00"],
                        series: {stack: 0,
                            lines: {show: false, steps: false },
                            bars: {show: true, barWidth: 0.9, align: 'center',},
                        },
                        xaxis: {ticks: label},
                        yaxis: {min:0, max: maxValue+5,  tickSize: parseInt((maxValue+5)/10)},
                        tooltip : true,
                        tooltipOpts : {
                            content : "<b>%x</b> = <span>%y</span>",
                            defaultTheme : false
                        }
                    };

                    /*var data = [
                        {
                            data: [[1,300], [2,300], [3,300], [4,300], [5,300]],
                            bars : {
                                show : true,
                                barWidth : 0.5,
                                order : 3
                            }
                        },
                        {
                            data: [[1,800], [2,600], [3,400], [4,200], [5,0]],
                            bars : {
                                show : true,
                                barWidth : 0.5,
                                order : 3
                            }
                        },
                    ];*/

                    var statusData = [readyTask, runningTask, doneTask];
                    $.plot($("#bar-chart"), statusData, statusOptions);
                }
            });
        }


        AD.app = new MyRouter({
            "AD": AD
        });
        
        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		
		Backbone.Events.off("TaskUpdate");
		Backbone.Events.on("TaskUpdate", function(data){
			//getTaskInfo();
		});
    }
    
    $[AD.page] = AD;
    
})(jQuery)
