  (function($) {
      var currentPage = "";
      var AD = {};
      AD.page = "ajax/device_sublist_piechart.html";
      var idAttribute = AD.idAttribute = "id";
      var startLooking;
      var H = Handlebars;
      // console.log('[StepCheck] : start');
      var device_columns = [{
              title: getTitle('IMEI & MAC'),
              sortable: false,
              width: '10%',
              callback: function(o) {
                  var columnHtml = '<div style="text-align:center;height=200px">' + '<span style = "position: relative; font-size: 14px;">' + '{{edge_name}}' + '</span>' + '<div style="position:relative;top:5px;text-align:center;visibility:">' + '<img class="icon" src=' + "img/lora_edge.png" + ' style="width: 80px;height: 80px">' + '</div>' + '</div>';
                  var columnH = H.compile(columnHtml);
                  var setValue = { edge_name: o.edge_name };
                  var finalHtml = columnH(setValue);
                  return finalHtml;
              }

          },

          {
              title: getTitle(i18n.t("ns:Device.Title.Status")), //'ns:Device.Title.Status',
              property: 'label',
              filterable: false,
              searchName: 'label',
              sortable: false,
              width: '45%',
              callback: function(o) {

                  var image_array = ["img/test_img/graphic-water_16.png", "img/test_img/graphic-sun_16.png",
                      "img/test_img/graphic-temperature_16.png", "img/test_img/graphic-battery_17.png"
                  ];
                  var unit_array = ["%", "lux", "℃", "%", ];
                  var allValueSet = [];

                  setStatusValue(0, o.humi);
                  setStatusValue(1, o.lumi);
                  setStatusValue(2, o.temp);
                  setStatusValue(3, o.power);

                  function setStatusValue(id, value) {
                      var valueSet = {};
                      if (value != "undefined" && value != -1) {
                          valueSet = {
                              dataId: id.toString(),
                              dataName: o.edge_name + '_' + id,
                              dataValue: value, //getStatusFinalValue(id, value).toString(),
                              dataUnit: unit_array[id],
                              dataImage: image_array[id]
                          };
                          allValueSet.push(valueSet);
                      }

                  }
                  var context = {
                      dateEdgeName: o.edge_name,
                      dataDate: o.date,
                      comments: allValueSet
                  };
                  Handlebars.registerHelper('if_value', function(id, value) {

                      if (id == 1) {
                          return Math.ceil(value / 10);
                      } else {
                          return value;
                      }
                  });
                  var columnHtml = '<div>' +
                      '{{#each comments}}' +
                      '<div style="position:relative">' +
                      '<div class="chart" id="water" style="position:relative;width: 120px;height: 120px">' +
                      '<span id = "chart_pie_' + '{{dataId}}' + '" ' + 'class="pie_chart pie_chart_' + '{{dataName}}' + '"' + ' data-percent="' + '{{#if_value dataId dataValue}} {{this.dataValue}} {{else}} {{this.dataValue}} {{/if_value}}' + '"></span>' +
                      '<div style="position:absolute;width: 120px;height: 120px;text-align:center;top:20px;left:2px">' +
                      '<img class="icon" src=' + '{{dataImage}}' + ' style="width: 40px;height: 36px ; margin-left: 5px">' +
                      '<p class="text" style="line-height: 1;position:relative;top:10px">' +
                      '<span id = statusValue_' + '{{dataName}}' + ' style="font-size: 16px;font-weight: bold;margin-left: 2px">' + '{{dataValue}}' + '</span>' +
                      '<span style="font-size: 15px">' + '{{dataUnit}}' + '</span>' +
                      '</p>' +
                      '</div>' +
                      '</div>' +
                      '</div>' +
                      '{{/each}}' +
                      '</div>';


                  var columnH = H.compile(columnHtml);
                  var finalHtml = columnH(context);
                  $.each(allValueSet, function(index, value) {
                      setPie(value.dataName)
                  });
                  // console.log('[StepCheck] : setPie finished!');
                  return finalHtml;
              }
          }, {
              title: getTitle(i18n.t("ns:Device.Title.Switch")),
              operator: true,
              sortable: false,
              width: '35%',
              callback: function(o) {
                  var _switch = o.gpio;
                  var count = 0;
                  var allValueSet = [];

                  $.each(_switch, function(i, value) {
                      if (value != -1) {
                          count++;
                          var ifHide = 'inline';
                          if (o.edge_name == '9b667c0e28000100') {
                              if (i == 1 || i == 2) {
                                  ifHide = 'none';
                              }
                          }
                          var dataValue = {
                              dataId: i,
                              dataUri: '#' + AD.page + '/switch/' + i + ',' + value + ',' + o.edge_name + ',' + o.device_name + ',' + o.gpio,
                              dataEdgeName: o.edge_name + '_' + i,
                              dataDeviceName: o.device_name,
                              dataGpio: _switch,
                              dataIconImage: getSwitchIcon(value).switchIcon,
                              dataImage: getSwitchIcon(value).switch,
                              dataHide: ifHide
                          };
                          allValueSet.push(dataValue);
                      }
                  });

                  if (count == 0) {
                      return '<div style="text-align:left;padding-left:6px">' + i18n.t("ns:Device.NotAvailable") + '</div>';
                  }
                  var context = {
                      comments: allValueSet
                  };

                  var columnHtml = '<div>' + '{{#each comments}}' +
                      '<div style="position:relative;width:40%;height:100px;float:left;text-align:center;background:#DFDFE3;margin:2px;display:' + '{{dataHide}}' + '">' +
                      '<div style="margin-top:10px">' +
                      '<span id="switch_select_' + '{{dataEdgeName}}"' + ' class="select_opts" style="font-size:16px">' + '--' + '</span>' +
                      '</div>' +
                      '<img class="icon" id="switchIcon_{{dataEdgeName}}" src={{dataIconImage}}>' +
                      '<a href={{dataUri}} id="switch_url_{{dataEdgeName}}">' +
                      '<img class="icon" id= "switch_{{dataEdgeName}}" src={{dataImage}} style="width: 80px;height: 45px; margin-top:10px">' +
                      '</a>' +
                      '</div>' +
                      '{{/each}}' +
                      '</div>';

                  var columnH = H.compile(columnHtml);
                  var finalHtml = columnH(context);

                  return finalHtml;
              }

          }, {
              title: getTitle(i18n.t('ns:Device.Title.LastReport')),
              filterable: false,
              sortable: false,
              width: '10%',
              callback: function(o) {
                  var context = {
                      dateEdgeName: o.edge_name,
                      dataDate: o.date
                  };
                  var columnHtml = '<span id = statusTime_' + '{{dateEdgeName}}' + ' style="font-size:10px;color:#46444C;text-align:center" >' + '{{dataDate}}' +
                      '</span>';
                  var columnH = H.compile(columnHtml);
                  var finalHtml = columnH(context);
                  return finalHtml;
              }
          }

      ];

      function getStatusFinalValue(id, value) {
          if (id == 1) {
              return Math.ceil(value / 10);
          } else {
              return value;
          }
      }

      function getSwitchIcon(value) {
          var iconObject = {
              switchIcon: '',
              switch: ''
          }
          switch (value) {
              case 0:
                  iconObject.switchIcon = 'img/test_img/power-n32_off.png';
                  iconObject.switch = 'img/Off.png';
                  return iconObject;
              case 1:
                  iconObject.switchIcon = 'img/test_img/power-n32_on.png';
                  iconObject.switch = 'img/On.png';
                  return iconObject;
          }
      }

      function setPie(name) {
          $(function() {
              // console.log("name = " + name + " i = " + i);
              $('.pie_chart_' + name).easyPieChart({
                  animate: 500,
                  barColor: '#006E51',
                  scaleColor: '#B0B0B0',
                  lineWidth: 3,
                  trackColor: '#B0B0B0',
                  size: 120
              });

          });
      }

      function getTitle(title) {
          // console.log('[StepCheck] : getTitle');
          return '<div style="text-align:center;"><span style = "font-size: 14px;">' + title + '</span></div>';
      };
      var updateModel = Backbone.Model.extend({
          data: null
      });
      var updateModelCollection = Backbone.Collection.extend({
          initialize: function(options) {
              this.bind("add", options.view.update);
          }
      });

      var installedAppsView = Backbone.View.extend({
          el: $('#myTabContent1'),
          initialize: function() {
              mUpdateModelCollection = new updateModelCollection({ view: this });
          },
          update: function(model) {
              var data = model.get('data');
              $.each(JSON.parse(data), function(idx, dataObject) {

                  if (idx == 'content') {
                      if (dataObject.length === 0) {
                          console.log("Interval : Stop");
                          clearInterval(startLooking);
                      } else {
                          // console.log("Interval : Do");
                          $.each(dataObject, function(idx, obj) {

                              $('#statusTime_' + obj.edge_name).text(obj.date);
                              setUpdateValue(0, obj.humi);
                              setUpdateValue(1, obj.lumi);
                              setUpdateValue(2, obj.temp);
                              setUpdateValue(3, obj.power);

                              function setUpdateValue(id, value) {
                                  if (value != "undefined" && value != -1) {
                                      var _id = obj.edge_name + '_' + id;
                                      if (document.getElementById('statusValue_' + _id)) {
                                          $('#statusValue_' + _id).text(value);
                                          var chart = window.chart = $('.pie_chart_' + _id).data('easyPieChart');
                                          chart.update(getStatusFinalValue(id, value));
                                      }
                                  }
                              }
                              console.log("name : " + obj.edge_name + " gpio : " + obj.gpio + " data : " + obj.date);
                              var _switch = obj.gpio;
                              var switchControl, switchIcon, switchUrl, switchDiv;
                              var temp_select = [1, -1, -1, 1];
                              $.each(_switch, function(idx, value) {
                                  var _id = obj.edge_name + '_' + idx;
                                  switchControl = $('#switch_' + _id);
                                  switchIcon = $('#switchIcon_' + _id);
                                  switchUrl = $('#switch_url_' + _id);

                                  if (document.getElementById('switch_' + _id)) {
                                      var icon = getSwitchIcon(value);
                                      switchUrl.attr("href", '#' + AD.page + '/switch/' + idx + ',' + value + ',' + obj.edge_name + ',' + obj.device_name + ',' + obj.gpio);
                                      switchIcon.attr("src", icon.switchIcon + '?' + Math.random()); //動態改變icon 
                                      switchControl.attr("src", icon.switch+'?' + Math.random()); //動態改變icon 

                                      if (obj.edge_name == '9b667c0e28000100') {
                                          if (temp_select[idx] != -1) {
                                              $('#switch_select_' + _id).text('Light');
                                          }
                                      }


                                  }
                              });


                          });
                      }
                  }
              });
          },


      });
      var mUpdateModelCollection;
      var mInstalledAppsView = new installedAppsView();

      function detectChange() {
          var dmURL = $.config.server_new_rest_url + "/edges?device_name=" + getDeviceName();

          $.ajax({
              type: 'GET',
              url: dmURL,
              timeout: 10000
          }).done(function(data) {
              mUpdateModel = new updateModel({
                  'data': data
              });

              mUpdateModelCollection.add(mUpdateModel);
          }).fail(function() {

          }).always(function() {});
      };

      var routes = {};
      routes[AD.page + '/grid'] = 'main';
      routes[AD.page + '/delete/:_id'] = 'delete_single';
      routes[AD.page] = 'render';
      routes[AD.page + '/switch/:_id'] = 'switch_button';

      var MyRouter = Backbone.DG1.extend({
          routes: routes,
          initialize: function(opt) {
              var t = this;
              //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
              Backbone.DG1.prototype.initialize.call(t, opt);
              t.$bt = $(opt.bts).first();
              console.error();
              this.show_grid();

          },
          show_grid: function() {
              var t = this;
              var gvs = "#s1 .cctech-gv";
              var gv = $(gvs).first();
              // console.log("gvs" + gvs);
              if (!androidGrid) {
                  var androidGrid = new Backbone.GridView({
                      el: t.$gv,
                      collection: new t.AD.Collection(),
                      // buttons: t.AD.buttons,// Remove Refresh Button
                      columns: t.AD.columns,
                      filter: t.AD.filter,
                      AD: t.AD
                  });

                  gv.on("click", ".s2-btn", function(event) {
                      event.preventDefault();
                      var hash = window.location.hash;
                      var url = location.hash.replace(/^#/, '');

                      var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                      url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');
                      t.navigate("#ajax/device_import.html", {
                          trigger: true
                      });
                  });

                  gv.on("click", ".s1-btn", function(event) {
                      event.preventDefault();
                      var hash = window.location.hash;
                      var url = location.hash.replace(/^#/, '');

                      var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                      url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');
                      t.navigate("#ajax/device_import.html", {
                          trigger: true
                      });
                  });
              } else {
                  androidGrid.refresh();
              }
              t.$gv.show();
          },
          main: function() {
              // console.log('[StepCheck] : main');
              s2();
          },
          switch_button: function(id) {
              var arr = id.split(',');
              var switchId = arr[0];
              var switchStatus = arr[1];
              var switchEdgeName = arr[2];
              var switchDeviceName = arr[3];
              var switchDeviceGpio = [parseInt(arr[4]), parseInt(arr[5]), parseInt(arr[6]), parseInt(arr[7])];
              var showId = parseInt(arr[0]) + 1;
              console.log("switchDeviceGpio = " + switchDeviceGpio);
              var _title = '';
              if (switchStatus == 0) {
                  _title = "<i class='fa fa-cog fa-spin fa-2x fa-fw #95AAD3'></i> &nbsp; " + i18n.t("ns:Message.Device.SendTurnOnMessage") + " " + "Edge : " + switchEdgeName + " " + i18n.t("ns:Message.Device.SwitchNo") + showId + " " + i18n.t("ns:Message.Device.ToServer") + " <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?";
                  switchStatus = 1;
              } else {
                  _title = "<i class='fa fa-cog fa-spin fa-2x fa-fw #95AAD3'></i> &nbsp; " + i18n.t("ns:Message.Device.SendTurnOffMessage") + " " + "Edge : " + switchEdgeName + " " + i18n.t("ns:Message.Device.SwitchNo") + showId + " " + i18n.t("ns:Message.Device.ToServer") + " <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?";
                  switchStatus = 0;
              }
              $.SmartMessageBox({
                  title: _title,
                  content: '<span style="font-size:18px; color:#F8CDCD; padding-top:5px">' + i18n.t("ns:Message.Device.SwitchHint") + '</span>', //"<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> " + i18n.t("ns:Message.Device.DeleteDeviceContent") + "</span>",
                  buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']',

              }, function(ButtonPressed) {
                  if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                      var dmURL = $.config.server_new_rest_url + "/edges/" + switchEdgeName + "/message";
                      switchDeviceGpio[switchId] = switchStatus;
                      console.log("new switchDeviceGpio = " + switchDeviceGpio);
                      $.ajax({
                          type: 'POST',
                          url: dmURL,
                          data: JSON.stringify({ 'gpio': switchDeviceGpio }),
                          timeout: 10000
                      }).done(function() {

                          $.smallBox({
                              title: i18n.t("ns:Message.Device.SwitchSuccessfully"), //i18n.t("ns:Message.Device.DeviceDelete") + i18n.t("ns:Message.Device.DeleteSuccess"),
                              content: "<i class='fa fa-circle-o'></i> <i>" + i18n.t("ns:Message.Device.SwitchSent") + " " + i18n.t("ns:Message.Device.OnlySuccessfully") + "</i>",
                              // title: "[Device Delete] Deleted successfully",
                              // content: "<i class='fa fa-times'></i> <i>Deleted Device : &nbsp;" + id + "</i>",
                              color: "#648BB2",
                              iconSmall: "fa fa-check fa-2x fadeInRight animated",
                              timeout: 5000
                          });
                      }).fail(function() {
                          console.log("[switch_button] : fail!");
                          $.smallBox({
                              title: i18n.t("ns:Message.Device.SwitchFailed"),
                              content: "<i class='fa fa-exclamation'></i> <i>" + i18n.t("ns:Message.Device.SwitchSent") + " " + i18n.t("ns:Message.Device.OnlyFailed") + "</i>",
                              // title: "[Device Delete] Deleted failed",
                              // content: "<i class='fa fa-times'></i> <i>Deleted Device : &nbsp;" + id + "</i>",
                              color: "#B36464",
                              iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                              timeout: 5000
                          });
                      }).always(function() {
                          console.log("[switch_button] : always!");
                          window.location.href = "#" + AD.page + "/?title=" + switchDeviceName;
                          intervalControl();
                      });
                  } else {
                      console.log("[switch_button] : press No!");
                      window.location.href = "#" + AD.page + "/?title=" + switchDeviceName;
                      intervalControl();

                  }
              });
          }
      });

      // // Windows
      // function s1()
      // {
      //     console.log("function s1");
      //     var AD = {};
      //     AD.page = "ajax/device_sublist_piechart.html";
      //     AD.collections = {};
      //     currentPage = "s1";
      //     var url = $.config.server_rest_url + "/devices?";
      //     if ($.login.user.checkRole('DeviceOperator'))
      //     {    
      //         AD.buttons = [
      //             //button
      //             // {
      //             //     "sExtends": "text",
      //             //     "sButtonText": "<i class='fa fa-plus'></i>&nbsp; " + "Add Device", //i18n.translate("ns:File.Upload"),
      //             //     "sButtonClass": "upload-btn s1-btn btn-default txt-color-white bg-color-blue"
      //             // },
      //             {
      //                 "sExtends": "text",
      //                 "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
      //                 "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
      //                 "fnClick": function(nButton, oConfig, oFlash)
      //                 {
      //                     $(".cctech-gv table").dataTable().fnDraw(false);
      //                 }
      //             }
      //         ];
      //     }    
      //     //=============================================================================================================
      //     // Define default value of search criteria
      //     //=============================================================================================================
      //     AD.filter={};
      //     AD.filter.search={};
      //     AD.filter.search.operatingSystemName = "Windows";    

      //     AD.columns = device_columns;
      //     AD.Model = Backbone.Model.extend(
      //     {
      //         urlRoot: url,
      //         idAttribute: idAttribute
      //     });
      //     AD.Collection = Backbone.Collection.extend(
      //     {
      //         url: url,
      //         model: AD.Model
      //     });
      //     AD.app = new MyRouter(
      //     {
      //         "AD": AD
      //     });
      //     pageSetUp();
      // };
      function getDeviceName() {
          var location_url = window.location.toString();
          var device_name = location_url.substring(location_url.indexOf("title=") + "title=".length, location_url.length);
          return device_name;
      }

      function intervalControl() {
          if (startLooking != 'undefined') {
              clearInterval(startLooking);
          }
          startLooking = setInterval(detectChange, 1500);
      }
      // Android
      function s2() {
          var AD = {};
          AD.page = "ajax/device_sublist_piechart.html";
          AD.collections = {};
          currentPage = "s2";

          var url = $.config.server_new_rest_url + "/edges?device_name=" + getDeviceName();
          // console.log("url : " + url);
          if ($.login.user.checkRole('DeviceOperator')) {
              AD.buttons = [
                  //button
                  // {
                  //     "sExtends": "text",
                  //     "sButtonText": "<i class='fa fa-plus'></i>&nbsp; " + "Add Device", //i18n.translate("ns:File.Upload"),
                  //     "sButtonClass": "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
                  // },
                  {
                      "sExtends": "text",
                      "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
                      "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                      "fnClick": function(nButton, oConfig, oFlash) {
                          $(".cctech-gv table").dataTable().fnDraw(false);
                      }
                  }
              ];
          }
          //=============================================================================================================
          // Define default value of search criteria
          //=============================================================================================================
          AD.filter = {};
          AD.filter.search = {};
          AD.filter.search.operatingSystemName = "Android";

          AD.columns = device_columns;
          AD.Model = Backbone.Model.extend({
              urlRoot: url,
              idAttribute: idAttribute
          });
          AD.Collection = Backbone.Collection.extend({
              url: url,
              model: AD.Model
          });
          AD.app = new MyRouter({
              "AD": AD
          });
          pageSetUp();
          intervalControl();
          // console.log("[StepCheck] : s2() finish!");
      };


      AD.bootstrap = function() {
          // console.log('[StepCheck] : bootstrap');
          i18n.init(function(t) {

              $('[data-i18n]').i18n();

          });

          if (!$.login.user.checkRole('DeviceViewer')) {

              window.location.href = "#ajax/dashboard.html/grid";
          };
          //網址列＃號以後的字串, ex, #ajax/device_detail.html/331
          var hash = window.location.hash;
          // device ID, ex, 331
          var start = hash.indexOf("=");
          var end = hash.lenght;
          var id = hash.substring(start + 1, end);
          // hash.replace(/^#ajax\/device_sublist.html\/(\w+)$/, "$1");

          $("#deviceName font").text(id);
          // $("#zdeviceName").attr("href","#ajax/device_detail.html/" + id);

          $("#myTab li").click(function() {
              switch (this.firstElementChild.id) {
                  case "s2": //Android
                      // console.log("tab s2");
                      s2();
                      break;
              }
          });

          if (!Backbone.History.started) {
              Backbone.emulateHTTP = true;
              Backbone.history.start();
          }

          if (currentPage == "") {
              s2();
          }

          if (currentPage == "s1") {
              // console.log("currentPage s1");
              $("#tab2").removeClass('active');
              $("#tab1").addClass('active');
          } else if (currentPage == "s2") {
              // console.log("currentPage s2");
              $("#tab1").removeClass('active');
              $("#tab2").addClass('active');
          }
          $("body").removeClass("loading");
      };
      $[AD.page] = AD;
  })(jQuery);
