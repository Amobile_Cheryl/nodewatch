(function($) {
	/* define date format*/
    // df = new DateFormat();
    // df.setFormat('yyyy-MMM-dd HH:mm:ss');
    var iFirmwarefileDataRefreshPeriod = 30000;

	/* define namespace starts */
    var AD = {};
    AD.page = "ajax/firmware_list.html";	    
    
    // Windows
    function s1() {
    	var AD = {};
    	AD.page = "ajax/firmware_list.html";
		AD.collections = {};
		// AD.title = "ns:File.Firmware.OSImage";
		var url = $.config.server_rest_url + "/firmwareFiles" + "?search=operatingSystemName:" + "Windows";
		var idAttribute = AD.idAttribute = "id";

		if ($.login.user.checkRole('FirmwareFileOperator'))
			AD.buttons = [
		    	//button
		        {
		            "sExtends" : "text",
		            "sButtonText" : "<i class='fa fa-cloud-upload'></i>&nbsp; " + i18n.translate("ns:File.Upload"),
		            "sButtonClass" : "upload-btn s1-btn btn-default txt-color-white bg-color-blue"
		        },
		        {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash) {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
		    ];

		AD.columns = [
	        {
	            title : 'ns:File.Firmware.Title.Name',
	            property : 'name',
	            cellClassName : 'name',
	            filterable : false,
	            sortable : true,
	            width: '250px',
	            callback: function(o) { 	            
	            	return '<i class="fa fa-windows"></i>' + o.name;
            	}
	        },	   
            /*{
	            title : 'ns:File.Firmware.Title.Domain',
	            property : 'domainId',
	            cellClassName : 'domainName',
	            filterable : false,
	            visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
	            sortable : false,
	            callback: function(o) {
	                //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknown' : $.config.Domain[o.domainId] ;
	                return (typeof(o.domainName) === "undefined") ? 'DefaultDomain' : o.domainName;
	            }
	        },*/{
                title : 'ns:File.App.Title.Creator',
                property : 'creatorName',
                cellClassName : 'creatorName',
                filterable : false,
                sortable : false,
                callback: function(o) { 	            
	            	return o.creatorName;
            	}
            },{
	            title : 'ns:File.Firmware.Title.Version',
	            property : 'version',
	            cellClassName : 'version',
	            filterable : false,
	            sortable : false
	        },{
	            title : 'ns:File.Firmware.Title.FileSize',
	            property : 'size',
            	cellClassName : 'size', 
	            filterable : false,
	            sortable : false,
	            callback : function (o) {
	                if (o.size == null) {
	                    return "Unknown";                    
	                } else {
	           			var fileSize = o.size.toFixed(0) + " Bytes";
				        
				        if (o.size > 1024 && o.size < (1024 * 1024)) {
				          	fileSize = (o.size / 1024).toFixed(0) + " KB";
				        } else if (o.size > 1024 * 1024) {
				          	fileSize = (o.size / (1024 * 1024)).toFixed(1) + " MB";
				        }
				        return fileSize;
	                }
	            }
	        },{
	            title : 'ns:File.Firmware.Title.UploadedTime',
	            property : 'creationTime',
            	cellClassName : 'creationTime', 
	            filterable : false,
	            sortable : true,
	            callback: function(o) {
                   	/*change millisecond to date format*/
                	// var time = o.creationTime;
                 	// var date = time;
                 	// var str = df.format(date);
                 	// return str;
                 	return o.creationTime;
                }
	        },{
	            title : 'ns:File.Firmware.Title.Remark',
	            property : 'remark',
	            cellClassName : 'remark',
	            filterable : false,
	            sortable : false, 
	            callback : function (o) {
	                if (o.remark == null) {
	                    return "Unknown";                    
	                } else {
	                    return o.remark;
	                }
	            }
	        },
            //columns Action
			{
	            title : 'ns:File.Firmware.Title.Action',
	            sortable : false,
	            width: '100px',
	            visible: $.login.user.checkRole('FirmwareFileOperator'),
	            callback : function (o) {
	                var _html = '';
	                // [Action] Download the firmware
                        _html += '<a href="'  + o.fileServerPath + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-teal" style="display: inline;"><i class="fa fa-cloud-download"></i></a>' + "&nbsp;";                            
	                // [Action] Delete the firmware
	                if ($.common.isDelete)
	                	_html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm btn_delete DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
	                return _html;
	            }
	        }	        
	        
	    ];
		/* define namespace ends */

		

		/* model starts */
		AD.Model = Backbone.Model.extend({
			urlRoot: url,
			idAttribute: idAttribute
		});
		/* model ends */

		/* collection starts */
		AD.Collection = Backbone.Collection.extend({
			url: url,
			model: AD.Model
		});	
		/* collection ends */

		var routes = {};
		routes[AD.page + '/delete/:_id'] = 'act_delete';   			 // [Action] Delete the File
		routes[AD.page + '/grid/s1']     = 'act_s1';                 // [Action] Act to s1
		routes[AD.page] = 'render';

		var MyRouter = Backbone.DG1.extend({        
        	routes: routes,
        	initialize: function(opt) {
            	var t = this;
	            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
	            Backbone.DG1.prototype.initialize.call(t, opt);
	            t.$bt = $(opt.bts).first();
	            console.error();
	            this.show_grid();
        	},
        	show_grid: function() {
        		var t = this;

        		var gvs = "#s1 .cctech-gv";
				var gv = $(gvs).first();
				
				if(!firmwareGrid) {
					var firmwareGrid = new Backbone.GridView({
						el: t.$gv,
						collection: new t.AD.Collection(),
						buttons: t.AD.buttons,
						columns: t.AD.columns,
						AD: t.AD
					});

					gv.on("click", ".s1-btn", function (event) {
		                event.preventDefault();

		                var hash = window.location.hash;
		                var url = location.hash.replace(/^#/, '');
		                
		                var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
		                url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');		               
		                
		                t.navigate("#ajax/firmware_upload_windows.html", {
                            trigger: true
                        });
		            });
				} else {
					firmwareGrid.refresh();
				}
				t.$gv.show();
				//pageSetUp();
        	},
        	act_delete: function(id) {
        		//event.preventDefault();         	
	            var t = this;

	            $.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Firmware.DeleteFirmwareFile")+"<span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+i18n.t("ns:Message.Firmware.DeleteFirmwareFileContent")+"</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                    	$("body").addClass("loading");

                        $.ajax({                
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/firmwareFiles/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[Windows Firmware] Deleted failed");
                            }
                        }).done(function(data){
                        	if (typeof(data) === 'undefined'){
                                console.log("[Windows Firmware] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Firmware.WindowsFirmware") + i18n.t("ns:Message.Firmware.DeleteSuccess"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                        // title: "[Windows Firmware] Deleted successfully",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {                                            
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Firmware.WindowsFirmware") + i18n.t("ns:Message.Firmware.DeletedFailed"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                        // title: "[Windows Firmware] Deleted failed",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[Windows Firmware] Deleted failed => " + JSON.stringify(data));
                                };
                            }                            
                        }).fail(function(){
                            $.smallBox({
                                title: i18n.t("ns:Message.Firmware.WindowsFirmware") + i18n.t("ns:Message.Firmware.DeletedFailed"),
                                content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                // title: "[Windows Firmware] Deleted failed",
                                // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            }); 
                        }).always(function() {
                        	$("body").removeClass("loading");    
                            // window.location.href = "#ajax/firmware_list.html";
                            t.navigate("#ajax/firmware_list.html/grid/s1", {
	                            trigger: true
	                        });
                        });
                    } else {
                        // window.location.href = "#ajax/firmware_list.html";
                        t.navigate("#ajax/firmware_list.html/grid/s1", {
                            trigger: true
                        });
                    }
                });	             
        	},
            act_s1: function(id) {
                s1();
            }
        });

		pageSetUp();

		AD.app = new MyRouter({
            "AD": AD
        });
    };

    // Android
    function s2() {
    	var AD = {};
    	AD.page = "ajax/firmware_list.html";
		AD.collections = {};

		var url = $.config.server_rest_url + "/firmwareFiles" + "?search=operatingSystemName:" + "Android";
		var idAttribute = AD.idAttribute = "id";

		if ($.login.user.checkRole('FirmwareFileOperator'))
			AD.buttons = [
		    	//button
		        {
		            "sExtends" : "text",
		            "sButtonText" : "<i class='fa fa-cloud-upload'></i>&nbsp; " + i18n.translate("ns:File.Upload"),
		            "sButtonClass" : "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
		        },
		        {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash) {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
		    ];

		AD.columns = [
	        {
	            title : 'ns:File.Firmware.Title.Name',
	            property : 'name',
	            cellClassName : 'name',
	            filterable : false,
	            sortable : true,
	            width: '250px',
	            callback: function(o) { 	            
	            	return '<span style="color: #000000;" title="Ver. ' + o.version +'">' + o.name + '</span>';
            	}
	        },/*{
	            title : 'ns:File.Firmware.Title.Domain',
	            property : 'domainId',
	            cellClassName : 'domainName',
	            filterable : false,
	            visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
	            sortable : false,
	            callback: function(o) {
	                //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknown' : $.config.Domain[o.domainId] ;
	                return (typeof(o.domainName) === "undefined") ? 'DefaultDomain' : o.domainName;
	            }
	        },*/{
                title : 'ns:File.App.Title.Creator',
                property : 'creatorName',
                cellClassName : 'creatorName',
                filterable : false,
                sortable : false,
                callback: function(o) { 	            
	            	return o.userName;
            	}
            },
            {
	            title : 'ns:File.Firmware.Title.DeviceType',
	            property : 'deviceTypeName',
	            cellClassName : 'deviceTypeName',
	            filterable : false,
	            sortable : false
	        },
	        {
	            title : 'ns:File.Firmware.Title.FileSize',
	            property : 'size',
            	cellClassName : 'size', 
	            filterable : false,
	            sortable : false,
	            callback : function (o) {
	                if (o.size == null) {
	                    return "Unknown";                    
	                } else {
	           			var fileSize = o.size.toFixed(0) + " Bytes";
				        
				        if (o.size > 1024 && o.size < (1024 * 1024)) {
				          	fileSize = (o.size / 1024).toFixed(0) + " KB";
				        } else if (o.size > 1024 * 1024) {
				          	fileSize = (o.size / (1024 * 1024)).toFixed(1) + " MB";
				        }
				        return fileSize;
	                }
	            }
	        },{
	            title : 'ns:File.Firmware.Title.UploadedTime',
	            property : 'creationTime',
            	cellClassName : 'creationTime', 
	            filterable : false,
	            sortable : true,
	            callback: function(o) {
                   	/*change millisecond to date format*/
                	// var time = o.creationTime;
                 	// var date = time;
                 	// var str = df.format(date);
                 	// return str;
                 	return o.creationTime;
                }
	        },
	        {
	            title : 'ns:File.Firmware.Title.Remark',
	            property : 'remark',
	            cellClassName : 'remark',
	            filterable : false,
	            sortable : false, 
	            callback : function (o) {
	                if (o.remark == null) {
	                    return "Unknown";                    
	                } else {
	                    return o.remark;
	                }
	            }
	        },
			{
	            title : 'ns:File.Firmware.Title.Action',
	            sortable : false,
	            width: '100px',
	            visible: $.login.user.checkRole('FirmwareFileOperator'),
	            callback : function (o) {
	                var _html = '';
	                // [Action] Download the firmware
                        _html += '<a href="'  + o.fileServerPath + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-teal" style="display: inline;"><i class="fa fa-cloud-download"></i></a>' + "&nbsp;";                            
	                // [Action] Delete the firmware
	                if ($.common.isDelete)
	                	_html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm btn_delete DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
	                return _html;
	            }
	        }	        
	    ];
		/* define namespace ends */

		/* model starts */
		AD.Model = Backbone.Model.extend({
			urlRoot: url,
			idAttribute: idAttribute
		});
		/* model ends */

		/* collection starts */
		AD.Collection = Backbone.Collection.extend({
			url: url,
			model: AD.Model
		});			
		/* collection ends */		
		
		var routes = {};
		routes[AD.page + '/delete/:_id'] = 'act_delete';   // [Action] Delete the File
		routes[AD.page + '/grid/s2']     = 'act_s2';       // [Action] Act to s2
		routes[AD.page] = 'render';		

		var MyRouter = Backbone.DG1.extend({        
        	routes: routes,
        	initialize: function(opt) {
            	var t = this;
	            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
	            Backbone.DG1.prototype.initialize.call(t, opt);
	            t.$bt = $(opt.bts).first();
	            console.error();
	            this.show_grid();
        	},
        	show_grid: function() {
        		var t = this;

        		var gvs = "#s1 .cctech-gv";
				var gv = $(gvs).first();
				
				if(!bootLoaderGrid) {
					var bootLoaderGrid = new Backbone.GridView({
						el: t.$gv,
						collection: new t.AD.Collection(),
						buttons: t.AD.buttons,
						columns: t.AD.columns,
						// title: "ns:File.Firmware.OSImage",
						AD: t.AD
					});
					
					gv.on("click", ".s2-btn", function (event) {
		                event.preventDefault();

		                var hash = window.location.hash;
		                var url = location.hash.replace(/^#/, '');
		                
		                var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
		                url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');		               
		                
		                t.navigate("#ajax/firmware_upload_android.html", {
                            trigger: true
                        });
		            });
				} else {
					bootLoaderGrid.refresh();
				}
				t.$gv.show();
				//pageSetUp();
        	},
        	act_delete: function(id) {
        		//event.preventDefault();
        		var t = this;         	
        		
        		$.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Firmware.DeleteFirmwareFile")+"<span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+i18n.t("ns:Message.Firmware.DeleteFirmwareFileContent")+"</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                    	$("body").addClass("loading");

                        $.ajax({                
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/firmwareFiles/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[Android Firmware] Deleted failed");
                            }
                        }).done(function(data){
                            if (typeof(data) === 'undefined'){
                                console.log("[Android Firmware] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Firmware.AndroidFirmware") + i18n.t("ns:Message.Firmware.DeleteSuccess"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                        // title: "[Android Firmware] Deleted successfully",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {                                            
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Firmware.AndroidFirmware") + i18n.t("ns:Message.Firmware.DeletedFailed"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                        // title: "[Android Firmware] Deleted failed",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[Android Firmware] Deleted failed => " + JSON.stringify(data));
                                };
                            } 
                        }).fail(function(){
                            $.smallBox({
                                title: i18n.t("ns:Message.Firmware.AndroidFirmware") + i18n.t("ns:Message.Firmware.DeletedFailed"), 
                                content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                // title: "[Android Firmware] Deleted failed",
                                // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            }); 
                        }).always(function() {
                        	$("body").removeClass("loading");
                            // window.location.href = "#ajax/firmware_list.html";
                            t.navigate("#ajax/firmware_list.html/grid/s2", {
                                trigger: true
                            });
                        });
                    } else {
                        // window.location.href = "#ajax/firmware_list.html";
                        t.navigate("#ajax/firmware_list.html/grid/s2", {
                            trigger: true
                        });
                    }
                }); 
        	},
        	act_s2: function(id) {
                s2();
            }
        });

		pageSetUp();

		AD.app = new MyRouter({
            "AD": AD
        });
    };
	
    function s3() {
    	var AD = {};
    	AD.page = "ajax/firmware_list.html";
		AD.collections = {};
		var url = $.config.server_rest_url +"/iOSFirmwareFiles";
		var idAttribute = AD.idAttribute = "id";

		AD.buttons = [
	    	//button
	        {
	            "sExtends" : "text",
	            "sButtonText" : "<i class='fa fa-cloud-upload'></i>&nbsp; " + i18n.translate("ns:File.Upload"),
	            "sButtonClass" : "upload-btn s3-btn btn-default txt-color-white bg-color-blue"
	        }
	    ];

		AD.columns = [
	        {
	            title : 'ns:File.Firmware.Title.Name',
	            property : 'name',
	            cellClassName : 'name',
	            filterable : false,
	            sortable : false,
	            width: '200px',
	            callback: function(o) { 	            
	            	return '<img src="img/file/android-icon.png">' + o.name;						
            	}
	        },{
	            title : 'ns:File.Firmware.Title.Domain',
	            property : 'domainId',
	            cellClassName : 'domainName',
	            filterable : false,
	            sortable : false,
	            callback: function(o) {
	                //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknown' : $.config.Domain[o.domainId] ;
	                return (typeof(o.domainName) === "undefined") ? 'DefaultDomain' : o.domainName;
	            }
	        },{
	            title : 'ns:File.Firmware.Title.Version',
	            property : 'firmwareVersion',
	            cellClassName : 'firmwareVersion',
	            filterable : false,
	            sortable : false
	        },{
	            title : 'ns:File.Firmware.Title.FileSize',
	            property : 'size',
            	cellClassName : 'size', 
	            filterable : false,
	            sortable : false,
	            callback : function (o) {
	                if (o.size == null) {
	                    return "Unknown";                    
	                } else {
	           			var fileSize = o.size.toFixed(0) + " Bytes";
				        
				        if (o.size > 1024 && o.size < (1024 * 1024)) {
				          	fileSize = (o.size / 1024).toFixed(0) + " KB";
				        } else if (o.size > 1024 * 1024) {
				          	fileSize = (o.size / (1024 * 1024)).toFixed(1) + " MB";
				        }
				        return fileSize;
	                }
	            }
	        },{
	            title : 'ns:File.Firmware.Title.UploadedTime',
	            property : 'creationTime',
            	cellClassName : 'creationTime', 
	            filterable : false,
	            sortable : false,
	            callback: function(o) {
                   	/*change millisecond to date format*/
                	// var time = o.creationTime;
                 	// var date = time;
                 	// var str = df.format(date);
                 	// return str;
                    return o.creationTime;
                }
	        },{
	            title : 'ns:File.Firmware.Title.Remark',
	            property : 'remark',
	            cellClassName : 'remark',
	            filterable : false,
	            sortable : false, 
	            callback : function (o) {
	                if (o.remark == null) {
	                    return "Unknown";                    
	                } else {
	                    return o.remark;
	                }
	            }
	        }
	        ,{
	            title : 'ns:File.Firmware.Title.Action',
	            sortable : false,
	            width: '100px',
	            callback : function (o) {
	                var _html = '';
	                // [Action] Download the firmware
                        _html += '<a href="'  + o.fileServerPath + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-teal" style="display: inline;"><i class="fa fa-cloud-download"></i></a>' + "&nbsp;";                            
	                // [Action] Delete the firmware
	                //_html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm btn_delete DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
	                return _html;
	            }
	        }
	    ];
		/* define namespace ends */

		/* model starts */
		AD.Model = Backbone.Model.extend({
			urlRoot: url,
			idAttribute: idAttribute
		});
		/* model ends */

		/* collection starts */
		AD.Collection = Backbone.Collection.extend({
			url: url,
			model: AD.Model
		});
		/* collection ends */

		var routes = {};
		routes[AD.page + '/delete/:_id'] = 'act_delete';   // [Action] Delete the File
		routes[AD.page + '/grid/s3']     = 'act_s3';       // [Action] Act to s3
		routes[AD.page] = 'render';

		var MyRouter = Backbone.DG1.extend({        
        	routes: routes,
        	initialize: function(opt) {
            	var t = this;
	            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
	            Backbone.DG1.prototype.initialize.call(t, opt);
	            t.$bt = $(opt.bts).first();
	            console.error();
	            this.show_grid();
        	},
        	show_grid: function() {
        		var t = this;

        		var gvs = "#s1 .cctech-gv";
				var gv = $(gvs).first();
				
				if(!logoGrid) {
					var logoGrid = new Backbone.GridView({
						el: t.$gv,
						collection: new t.AD.Collection(),
						buttons: t.AD.buttons,
						columns: t.AD.columns,
						AD: t.AD
					});

					gv.on("click", ".s3-btn", function (event) {
		                event.preventDefault();

		                var hash = window.location.hash;
		                var url = location.hash.replace(/^#/, '');

		                var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
		                url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');		               
		                
		                t.navigate("#ajax/firmware_upload.html?os=ios", {
                            trigger: true
                        });
		            });
				} else {
					logoGrid.refresh();
				}
				t.$gv.show();
				pageSetUp();
        	},
        	act_delete: function(id) {
        		//event.preventDefault();         	
        		var t = this;

        		$.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Firmware.DeleteFirmwareFile")+"<span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+i18n.t("ns:Message.Firmware.DeleteFirmwareFileContent")+"</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                    	$("body").addClass("loading");

                        $.ajax({                
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/firmwareFiles/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[iOS Firmware] Deleted failed");
                            }
                        }).done(function(data){
                            if (typeof(data) === 'undefined'){
                                console.log("[iOS Firmware] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Firmware.iOSFirmware") + i18n.t("ns:Message.Firmware.DeleteSuccess"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                        // title: "[iOS Firmware] Deleted successfully",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {                                            
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Firmware.iOSFirmware") + i18n.t("ns:Message.Firmware.DeletedFailed"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                        // title: "[iOS Firmware] Deleted failed",
                                        // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[iOS Firmware] Deleted failed => " + JSON.stringify(data));
                                };
                            }                            
                        }).fail(function(){
                            $.smallBox({
                                title: i18n.t("ns:Message.Firmware.iOSFirmware") + i18n.t("ns:Message.Firmware.DeletedFailed"),
                                content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Firmware.DeleteFirmware") + "</i>",
                                // title: "[iOS Firmware] Deleted failed",
                                // content: "<i class='fa fa-times'></i> <i>Deleted Firmware</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            }); 
                        }).always(function() {
                        	$("body").removeClass("loading");
                            // window.location.href = "#ajax/firmware_list.html";
                        	t.navigate("#ajax/firmware_list.html/grid/s3", {
	                            trigger: true
	                        });    
                        });
                    } else {
                        // window.location.href = "#ajax/firmware_list.html";
                        t.navigate("#ajax/firmware_list.html/grid/s3", {
                            trigger: true
                        });
                    }
                }); 
        	},
            act_s3: function(id) {
                s3();
            }
        });

		pageSetUp();

		AD.app = new MyRouter({
            "AD": AD
        });
    };
		
    AD.bootstrap = function() {
		i18n.init(function(t){
			$('[data-i18n]').i18n();			
		});		

		if (!$.login.user.checkRole('FirmwareFileViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        }

	    $("#myTab li").click(function() {
			switch (this.firstElementChild.id) {
		        case "s1":
		            s1();
		            break;
		        case "s2":
		            s2();
		            break;
		        case "s3":
		            s3();
		            break;
		    }
		});

	    if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

		// Initial page & after uploading
        var hash = window.location.hash;
        var url = location.hash.replace(/^#/, '');        
        var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');        

        switch (action) {
            case "grid/u1": //Windows
            case "grid/s1":
                $("#tab2").removeClass('active');
                $("#tab1").addClass('active');
                s1();
                break;
            case "grid/u2": //Android
            case "grid/s2":
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                s2();
                break;
            // case "grid/u3": //iPhone
            // case "grid/s3":
            //     $("#tab1").removeClass('active');
            //     $("#tab2").removeClass('active');
            //     $("#tab3").addClass('active');
            //     s3();
            //     break;
            default:    //Default
                s2();
                break;
        }       
    };

    $[AD.page] = AD;
    var fnRefreshDataForFirmwareFile = function() 
    {
        if ($('#divFirmwareFileMain').length > 0) 
        {
            console.log("[Data Refresh] Firmware File data refreshed.");
            $(".cctech-gv table").dataTable().fnDraw(false);
        }   
    }

    if (oRefreshData.oFirmwareFile.bIsEnable === true && oRefreshData.oFirmwareFile.oTimerId === null)
    {
        oRefreshData.oFirmwareFile.oTimerId = window.setInterval(fnRefreshDataForFirmwareFile, oRefreshData.oFirmwareFile.iInterval);
    }
})(jQuery);