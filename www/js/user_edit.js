(function($) {

    var AD = {};
    var url = $.config.server_rest_url+'/users';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "Users";
    AD.page = "ajax/user_edit.html";
   
    function recursiveParentCheck(node){
        // console.log('node is ');
        // console.log(node.attr('name'));
        var parent = $(node).parents('li').find(' > span > input');
        // console.log('parent is ');
        $.each(parent,function(index,value){
            console.log(value.name);
            if (value.name!='AllPermission'){
                // console.log(value.name + ' is not SuperUserRole');
                $(value).prop('checked', true);
            }
        })
    }

    function s1(id) {
        console.log('user_edit.js');
        $.ajax({
            type: "GET",
            url: url+'/'+id,
            dataType: 'json',
            success: function(data) {
                console.log(data);
                // for tab1 to display user informaiton
                $('input[name="username"]').val(data.name);
                $('input[name="description"]').val(data.remark);
                $('input[name="email"]').val(data.email);

                // for tab2 to display permission
                var roleNameList = {};
                console.log('Start to check relative permission.')
                $.each(data.roleNameSet,function(index,value){
                    $('input[name="'+value+'"]').prop('checked',true);
                    recursiveParentCheck($('input[name="'+value+'"]'));
                });
                // getData=data;
            },
            async: false
        });



        // console.log('Hello');
        // console.log(role);
        // Main Detail
        // var source = $("#tpl_permission").html();
        // console.log(source);
        // var template = Handlebars.compile(source);
        // var html = template(role);
        // console.log(html);
        // $("#permission_render").html(html);
    };




    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Translate Input of the placeholder
        translatePlaceholder();

		$("body").removeClass("loading");
        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/user_edit.html\/(\w+)$/, "$1");
        s1(id);
    };

    $[AD.page] = AD;

    function translatePlaceholder () {
        $("#username").attr("placeholder", i18n.t("ns:User.Message.UserName"));
        $("#description").attr("placeholder", i18n.t("ns:User.Message.Description"));
        $("#password").attr("placeholder", i18n.t("ns:User.Message.Password"));
        $("#repeatpassword").attr("placeholder", i18n.t("ns:User.Message.RepeatPassword"));
        $("#email").attr("placeholder", i18n.t("ns:User.Message.Email"));
        $("#appearanceInput").attr("placeholder", i18n.t("ns:User.Message.DeviceTypeAppearance"));
    }
    
})(jQuery);