(function($) {
	var UnknownImgB64 = "iVBORw0KGgoAAAANSUhEUgAAACwAAABaCAYAAADD/7TdAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfeCQoDOw5fASmMAAAAB3RFWHRBdXRob3IAqa7MSAAAAAx0RVh0RGVzY3JpcHRpb24AEwkhIwAAAAp0RVh0Q29weXJpZ2h0AKwPzDoAAAAOdEVYdENyZWF0aW9uIHRpbWUANfcPCQAAAAl0RVh0U29mdHdhcmUAXXD/OgAAAAt0RVh0RGlzY2xhaW1lcgC3wLSPAAAACHRFWHRXYXJuaW5nAMAb5ocAAAAHdEVYdFNvdXJjZQD1/4PrAAAACHRFWHRDb21tZW50APbMlr8AAAAGdEVYdFRpdGxlAKju0icAAB8YSURBVGhDZZt5kOVVdcfP7+29d8/SM0wzMwwzDKBEBDIsMyyDIoooYFmJW5SKELWiFdEsxmwm+SPEaJlYsaxKSgUMSklcgmDFgWKRTWTEkX1kBhhm6enpnl7evr+X7+fc36+HJLf7/u527tnuueee+3vvRZVqra9kURRZLpezbCZtpGqtYZVK2et9/UVeJimyVCqlds+inulpltJ8+jKZTKin09aP+pa2lEWqWw/onqVVj2JsUZRybAly+Oh2u+IhY9lsxofana61Wi0fg8eoXKn2IQSzBw8etG/ccpvd/r07bP74gnU0udvpOOJIE5YZpt53NtWnXhF0JtTfow7DyjGo6qoIRzodWafdZVoYCP+BYT2cfZhS3rBhvd14/fX2oQ+839auXSOm29brSUFouJDP20OPPGyf/bPP25Hpo7ZmctLSaBDigR+vo+cIRmNmIqcoMj7GaNJmklpiiqoLpf8eMI6MKZGUTk+ox50uB3NQ1szcnG3auMG++uUv2o6Ltluj2bRIKu/v27/PPvzRj5m0bRlpoVwqW6vdFgUhdIkDPpAHjDBOoSUUlZT6nBmnqGkMsgA+gxwS494Cn1JPZqK1Uy2sXkxGeGWe2awNj4xYSyu8Zs1q+963b7V166YslUmn7Ov//k2rVuvO4MzRY7ZUKlmt3rBmq2PNZkuSqWx3rK12S0vaUp1cb3W1xKrLzjqdnjVVtqUZ6l0x0xXTXXHQ1fL0ZCtep89zvCYylZBl53GJrLVGw+bn5y0t5mePz9u3br/d91fUaLT6F+58m2U0/eVXX2FvIIU0KHmlqVgZrlh0gFYpeZLZohBIqdEL1uFpeZ6y27+jE+MqsXxZYzBl+t3wgwBg9jlitCfh+1Li+vXrbWhs2B65978tdfjotKt/bn5Wmun6Lg9YwpI5Y77BVFcDu0yyM0KfjyYPsaISwRmjj9mUzKHi28A76GNQBsUElT1HqLbKSPuoK4YXpOmOluXIkRlLVcsVK8oEWG75InCEXU2ZVPASkjglNbJ8iReghDvf3WjX5QxqZqrL7TnAgs+dmkr+HM7Rn+jDBKIow5CPpUQPWksyi3qzhutMu7+t1CryfVkB9mWXLeuRO23rxvWONiH9/U5Lrk65Kzej3O12Qt3hBMOcOPfjsid45rY9N73eaTWlpJDxs95uat8otxpla8qG8SKZfNaWykXR6EkA/TUE2O1ps8jP9fttq1el7XZdbU1U7ohAIBIIJUSWkZMF19R4sy141U/AxKWyM+k5CP56wVCKtyU8Suhpd/Z78FK2rvjCVOGBFUuBoKcOUqfR1JLgaLQYWiIOFF8SlXrwrxFmhTE5GB0GKsN55qvj9uqGhUlQinjA6DaJ0QZTUh0zofT/gB8YHwerNn9HCsGmI7yHNmFqbHRMrkjLKikxdHwoZJhDzTeP/lJsCGcJZNB13yBc9MuNCZkGffOwfOQemU1En28mHdUyO6cAE45HtHTY9NkfaruXgDbCglIlK8WGXFhYtOj5F/f2d7zlSivJSwwNDothMSIESJucQ7ghRwUmr4M0r56OyqzVK4v2rnddZVu3bNF+qGrvaoaWFWY58gcHB+2LN3/RVqydtKmp9TY3O2ul4lLwSIFcnFBCrBBqeuCJqrWarV67zh594D42oACkCVTquxpIB2dqYBD9uiZCZ1z6UyX7XlDCUVoqWk1ep16tWqNes0atasXiopcRvl1ksHlg3XMwXQSdJgQ8ea/+wlryxOyqlZJIqf+F37zU337xW6xUnpcmhsJE58UfnnD4vmBOBMQBlbcFj92VimUxwnGuMU4RwEgIpk09unK1Sq0Z2uTIgzgo1ESvJKbEmAN1hNEfHiM/MGhPPPowDO/rb7/kchFcsOHhIR3HYbfGU0JyDGJVjITVUz0mFgiAWKuQCCQBM7m0jtKsbyrsENtm1KERIp5L9n0iGDZuULQ/VA8HSFNhwoj22l0/uNOiF9HwZW+10uJxyw0M2IrRcZsYH7OOtAK6hKGA+n8n7znxiGFNAVRGplC0Y7Mz3sF8mOlIwygjIybw5QhP6NrWWZBKZSwtdwpVPFVXzOa1p7JadcxrbHzC7viP2yzaGzNcFMMp7eA3bDnNTttyigc9IQVmnSUtPV7BTy01w1IGbbhvUg+Qg0OD9vSvf20vvLg3DMVpdGTY1ip0faXWs4nzd3pQtPDsbts2kbWF6dfs0JqzbGTTGVZ5/gkrbDzdWs89aRkdIkSOYytW2u23fiuYxCWXv9UW5+f8pNs8dZJtXLvaGjqqg72SYoaVYDKwFk4eHxBYCGr0kPYGtFLPvbTPDhw+zJT/lZhrIytsYN1ma776gtnwmI2/aYc1Duy1yoFnLb/yZGsuzFp6eMSietGGRlfqSK7a+LgY/vY35db2vtS/5C0yifnjOkq7dvENH7dt7/+wVaXxwE0g4qyryQb0Xh6JPP8nZbUSDx+etmdKFctxPcL+BYsCPADqyR1qI0VyeQz0GjWLdIlI5QetreXHFbZkCr2nnrShu79vjU5TsfGY3XnHd4MN79gphhfEsJz/ee/7kL3/j26yUXRIXCt60Eiip4Rxr7kfVB0e1EYU9lNHu+jlxZKV5PCJt32SgAgjfYrysk+nYFiVuoL1VYMDVlNscUBx909uvc3GfrbL+wnmf/ifd1pKJumGz1IzF2ffVWVY2h7Xzs0pQO+K5pBMJFupW162nW20LFeqWr9a8xCUICUpkY4gns3lQkEA7PK9glBVMS5xbp9YN9QFrHihY3lJcs0ZW3TxlDni+nAfSr7CMYOqn3Di9KBNQryGNDMvIusHCopFtctziuTUl9VYQa4vI0aAyygTV3C1ogTGVSglZHRYcEsm4zkyXipzKxYcfdyQ/aatuWOFgh2RGVEuh7ZK1PA0wEfP6WjeecU7FG8ecy2cI5O45g8+ZjnZTUMmsTaXsdlm24kyDT+Qdq0piag7/xg3SHNyT3M6Sl/UrZt+ljpWTpyC6ZxwlYEdgFvS9Ig2flv4j6rrsR/9yMYf+qn46FhBm/And/2QWOI3/UuveLsVneGOnf/B6+3c9/yONRYXRS1E/K45VC/cTmaZQxE/wYmv/GA2Y79ZXLL7dTtgwzHOnNenmMX46QvuLQTB/7JqLeKap35hQ/fdY3WdoEMjo3b3j3/kgaLAYo0JEHvG13Ilwa449z1IV18ShRGfcg3nstmWuSS5JZtryS5LsvO2Nk1VMBX1VQVX1XhV8ygrnRP1qmj4uHJF9broVNq60AoPAiAwq5iSObQabXErAV15mJ52Lkeo/yE4E2SbfpSyq12jElFlSnBcmQBjy1IK1O29KqI00jrBmEMBrAdKeIt0qLNysjWP7pwB3we6poEImuryfawqi1DXrQg0viBJ5uarMNmFINREo5EY6EVdZdmwJEf2LubCHMacZTGiObxaqmpTirIvL66Q8QjEMKA5RG3YD4W6fC6lt3wMOw9jyRwS9z6hg7OkSyVVEfLAI/abWkFH4e8XMBmkcbckAcQl7onXSAQwDZUluUIm+k4XHIR9uwondxNI+IXU6YpZ4WNFuQL3UGH4d6UhAPP9ksqq4MC94XoPgPhiPyi8DUKVaJpRTEaMoWWYgmYYAYfiXTFc1UmWaKYvTsDTc+bjlaCu1BXiAAMsOJmmRhj2EnMMhwyl1ioZI4mkz2ZVyFxrAAYRsYIj9j7BwjiT/EFBZ2RtabQqDS8rQSV4Ap1QhjrMOUV/UqJJWqlYeTFK9ZyY5zBspFgIYHypYQwr86VGL7TV73asuguhERlGgI/n4Et1NoNWiGJplBAIGokyGElKXx2VMMYUfzcXj4WSUZWa4Jvu9SkgiLMeLoge3gaRM0YjFgYYkXYh1NfgdWpCIBSxJvUnxQTGYgjwqIZTYD51SCTmGYBCgTKyOm3dZJLxQB3yQqAHKJAKLQa7DogdjDGfx5gK1eXRrMiGkxrccp1SgPHExOUUCyECxC4hMcf1HLcDCh6soB/h2az2psbd1eABVHiQrtIZEcLEHLwJw1KD9qDbZg/tq0QrbcGUcWnazUAHbWgMQ6SteiKd9wSiysGH+6YTbgd3avGTh/YCTKcOHjpoA7p7JXbEGBrtgJyaZqNtkARm2f7BFDg8YKArIj6mDVeSH04MLRiHiGh+zL4cBZ4CRlGKcAlx8B2aBl3hc7NQC36g+/qc2rhxgzV1VofjOTAQFCHtaa0IRDhAOnITfieTz6WOdlhKDylVQaPstWJHGsapCkc69uPqURJT7pPCqeiHk+pQpY5SYBv/HDgJfJAouOeBNtVNdjTsK+G+ksTSB2mZIjjBYALYD++/fKOp6TYuCEykBcO4NGCVQSelupZ9MwkVLAdzUT+EHCbA+jPmBdoORgkN8Ro+O/GBAM7SBk0iPTtfgJICG0b7YMGWljGh4bhO4M6x6MyCTX0OAjh9qsA0zPLHK6qQAOCJasL+8baGfaoyn0qVKuUgIMgDVJjiBJR8o6kfxskdPV0AxjRADpsvwJd1u0UgD0WVYNzfI3tDRKmjNv8XpfikC2wB4pBKSTtkpydCK1esCNvDufa1RzNBy+5z+VfG3zqOwH3QmiqQS/4QZBEPIU1AwAmxGs5wwOUHgtoeDlAVzOt1TDt5UlJb7tc8Xjgm8GGEQoRdo1Jb8L9qoUnVFeL4BvRx4OINyoMyuLRACCZdZrL/wRxwJ/RIJ7iSHqCoO05vxcwuJ62YIxNEEpB4ihkQV54DTGhjv3TT9C49Q7snDyEbjtGgZZIzrT5MgdXzYR7K4PWmz5dAIuKrAzwD3h/+4inavGwSBuMnzPtHVpoKIyw12WMFYMAsE3GzkfY5qYiX2Uzu0pQSfB5pMUtzwEXcjoD4a5/nUOAEhiAJhsJ8yCRm6XarHPaENOttJnmNugaVORBcXCXcHTY0Xavbfl3xGzqCcRDzza79arFkdTHbr7fUUbZeteHC9HXlAahXkyANXbOYIBz9ast6xZr1NZf94VrVGDeQrkwucKEEUeRR9ioMp91nypTZWMAoY6+cdH6f00GBRrpa7rIG//Kad9ium260obFRO1ou2dUXnGOHv/AZs7FxO3l81Pr/drN944PXmi1VLa8btJVrdsv177XPXXuV2fElW6vb7x2f/LAdvvnzdv22s31c/tC9ACvlbztdx0rsMHGPAlMIpKsUKxB646dvOpZckwjSe3KGHWVc1rmbT7FLTpmyAY1eduZWXSLNMgGBffqyC+2kUzd4vay1/dNrrrTGP3zOPnPdVXawUrPff8Op9tgX/tjO2LzRfvzifpsulW10bMT+6ffeYw9++kZbv3pSStGKYDtOnXRC22jYP2+Bff6QBiC3Mf7QOMuqnraWPzs4YB+54M32g2f22t/e/7jdsO1NtnHtpMP/Slf6y9evs69fvt2+o/GbLjrXzlyz0qK/+pLNiLFL162xmWrdrv/e3fbgB661D56+2a/yX77iYtsyOmIrMyn7yrVXymwUIrD04iMwGjapZwRRxdnEhVGiXSD9UIBV6no0xPh1551tkwN5O1Kt2dVv2GzFesM+eekFNpzL2YDi1D+57xHrZdJ2+96XoWTHxOCFkytt+9QaK8rdHdC8kzlllLK6GQ8qVIRqPpuzQ4Ldc/SYdmUcBom+M60S5WHl4f2I7nR+q5WdwjBAGhOjwb2wS7HfVGHAdmgpb37gMXvwub32/Sefsc/fc7+tkXYiaee1pZL96+O/tL9/6Am3wy89utvmyhX7m3e/zQYLebvuu3fZSgn1kXfutD9/4HG779C07T523P7uoZ/bL6dn/H3GY/sPyL4S+4VNcRQrkDqW4Dzue+XV/gXbL7Vycd5SsqEN173P1l/xTmuVFn1XuitI521yfMRemztuOU3n6t8QgpNWTFgunbGCNu6dsksITowMK6KST2623CyeFWPWbNjgyKhtGh2256eP2cTEmOak3FxWDw36a7CjC0s+3z+nk9C94WHL7dltA7t+bDV/ob3C7vmvu3wfxp7L5XBJ/HST1EHTMo9Oy147Nmt5wUValpy0NSpTWCqWbG5hwRYWRQxMYnyxXLb5WtU63Y49e3hay8UK5a1Wq9nzLLuYWiyV7Ojiokj1bFZMU09nk4tUrF1YiTOJswFfm+LTRTgOA9ixW4VysB0UjB/0F9OkRBDl8FYybcmHCxm0I4bwp4SLKb6349pQpj/LhzRiSvYbpbMEuRaJUeIP6CIwBwjVwE9IjkL7zHE6uphhl08dHXEpB4Ny/JRLbNrdHP2sgOA4D7G6Iu5IOMJO0LgY5maBf+erOD4JRkXEI0radPlTMIJzt6UWhwtMMRbGSRrxftyaCPIHMEkHjYsHcogkO5RPlfy2oQE8XhuhVOJRllrSMa9efaqQqt/flCv5Ea+S9eFtFQ1nhvNW3PuxCzExg8AOJwi62YDhOA5zwaUCvcYa1gAxrzMp74CmWSq06UT409L4MS6V+DJKQt5CkvDd7r9BLs26K+KPtoA5rbgmgcstlZVTn3q9zmolTJPgh+SwSA1qJIj746RhDSAdtOEFXwxBRQ/SsIRRydJhKui1wqspEoKxKprnKlAdYgJzGuBys2DMe5QkALT8/Ro1zCkeDXCBF2dKsILDPukKCECeRGeAeqYdZ5rhYKE7oGwQBzCdB/+Bq9D2OTRC0+uq8DrKP2cOI1pFavQzNcAnfoNV8j7966rEDg0AvvmEBIZdo2Sxxid2bldoHE6ZGQvUpkP1cLPwkfAMw8IdtOykHb/AlR2ONoVXA8Osmk/1uaqQxBMY/GXgghy2v1j2mdrpos8l1GGTSbRVcXtT5vUrRoC98SbdKSr5HEfshXf7LTfOoT8Aw3gshv4Ck5Q+rAYf+OS1EU/A+SxO3YLHEklcnNfJNZbXeaYG7cBobMvM0UxiV9TN8tW1OaHixGLEyUpBHIL0JeYTvqwQEidakoIJKgVgWy0fvj6fcdqemC9kqYJOIRwuDErPVpPGAnnTgcCuDf41HBasQNh8oY2GA1HszPeCOmHUr/AxP4FVJQ3gE/h3vnxl1dBwcoWCj4KCKN4qzSroL/BxArDKWSlT7hICdAakmHe53XLfR0yANkGCD3SbJgtxBy0Cy0mphAtDAhgKl1PNE1rXonD5psK01ORBwNWXsMTbbrfQi00vo/EBabgc8ZYJvmQKYvzw9CHx55sMLHES4mJLJ5iI8VaHF3x9TXSuWWaVkQThNoKMw0imNKh5bx4esBXSwpkys9WEj4JJDot87KsnxchZIwMaz9mmwbxN8K1bofAVgagSb+1nmoqNY7ZYcW48Uyetd4X6cobucHCEy5+AVPC5hvhXCCg4MTWkwIel5HuQE4qPCQ3HxNw5w0P2TLFqpwwVbEZHdU4aOU0M5TR/KpexrcMFWyNhclHaXqjUbVU2bQutjuVl1cNsLrSuzAfrAzo5OLTGRCsrhYgj13xb98ZwIQ1KUhLT3hHMz199SpasApchaQZkMDgiYmi8JnOYV3C+VYzt5ytkYvK4mGBVFnRLmVNepXldCfpsqWLrJODhRsNDy6YYqsssTtLG2qBNPqK+guBOVoC0QhnTw8z4AmpQKAYo0wiXPrgMDLLCtOpiJicEOXVk1OEXURFgazVk42XFD7O6QSPhAd0+jokRNuNhMe4fEGo+nyixiWcaLRdmT7EMETHbtxkJhvBV0dqnOazSiM6EksLSsmybc6DMHpIgzlFg0VmHGx7KYZciQE2aW9JttiSnW5S6izLGepSxpX7GipGWKj9orXTBovywzWksVRhUqFiwvmwzlc0rhMxbI5W1Jb4ak1EoqTG5JIWVclUyixpLJ2YOSRjxbYelgDkJOi8Bj6qeuFm4CYdKSNJw2O0w64dCWj54YNTSQxOWGhyzqDBiqfyQds2gtVN56+n2kRJMT4y0hBRkHv+qDN5GJKhLcjCzgr53UIQyMGgNyLSYbwDFXU24OIqB55LAbP6dtfBQU+Mss0vgBDVZ7Wa9atVa0WrVkjWqS56blaJK8pL6i9aslWxJ471G2XrNunUaNesq91sN66vd47uSrabG1CcT6qqPsR59ZPo6gvGvUvLFVN5NhMw3Y4KUiAXXQdsee+zb/2r/t3fssHq5ZAWAdJ/b+M6rbbAe7NNTgA51JZoE3U0toZ96jjQG8xp1dPT6fj3d2Z7o8SJJgHo32u9ZKTdgRx77maV/ercOMNFQ//27fmrRS/tf6W/bfrHVKiXLy5jSV15tqYsusx4/iUDjMa5A+kSZUWWhrc2B1HHfMv2kooFkjM4+38fpiGM6gcmyqQhWg2g+rceFS3C6tOaefsqG7r3HNy9x9ANi2KO18ObSo3kPGviSXCbqWFZ9XLn4JB6rws5wNSU2CN/vlQmYdjg+3wUDgeavlhtcq0ydfvx2X5t1aqxtF26s2DlTNTt/g27Mg4QEWb9G+bmh83Tr0Dqb7Bc0i89dgrjOXpwyREXhhTUz+IyiaqNTl1kzdcSac7tl0z3dH8WuNFlrNu3s1avspvO3OfgZp221Pa+8ajfc8X1pZEgK69v5K7Q5tfRo5Y1DKXu8XFWAJPztUfvEhm/YttOOWqmdt5Xpkv3Lnqvs7gM7dTtpumZvHH6jvbZi0vqr5GUOHbV7o1/qBBWXWJ1ER/iU/4rF1RMvn67vh59/zpYOzMpfanl03La0gdqCQeO/aI/Zz1efZdMb32T3FHO2tcBkBO7L8fdtXDt/UAfBlIKqgXza0tpEnkTm4NyozVXOsqada5X2G63UUOBF0jKuWmzb4wcO2e5G1U5aN2ZrNp1q+booJrxp+fDNMgkOcyUGPOkqnjsif1q1/Pjplj7lrdYdGpOTb1tVEwe6NTtvcb99qLHfNhSPWGXhmMQWjuCzBNezY+WKHSxWeLMabs3g1uodL6atsdixTn3YZhYjq7ckBXtA8443W1Zs12ziwEF7+fGnrNiYkcsL79pIFLHbixEmA9CV/XVTM+pKW3nPEcvIDxNW6syT3Vbs9iefs5t2PW2PPr3HHn7pVckYhOZrycfqclliui2kMzoJm9KMb92oab9oXmR3PlOwf3xkwv55z3m2r7xZXOgqwLcQ103YtFVcwGfmjtqjS69YWisEP7DnXgdNT8/M9s/4rbPlW8s2pJHm1ddZa+fbLSW/2+k2LNvMWTfXs7SY8uuONNnX6UcIOargpCBeZ4mOYoH5fcBUQaskuCOKuFpQi8d0+limJ5/dH1UfcxTMaHO7icY7KxvpoJL3aKZ7lt/9hA3e8wP//g+f+T147y6Ljh6b7W8962wdCGWPQTunnm6dTVssJS/gMayLyElGbByCdD4N4pofrFNeBR0KFk0ShBOuQp7TT+untMyx4KRxbilAuxmFUapUiFmERrINWHb6sOX2v+g/78F+H7z/XjE8M9c/XRqu6/TiC88hzoVJTQKH20ggeyIFh+OEvP265AzQyxxwqZSwxLiUCIFTIoVlVkOxyP/HA10hU9DU0QrynuT+Xbu06biOOOXAgr8X4J2X+vmWX8QH5wpW/Ovcyn38Nv2axPvgSCeIwyYZr+MwZL5NyDjBj+iAS/gDnLTPhmfDLs8PuIgtCJI87pDgvm7MY8Xmjs8JAGbjJMbRAtKF7+koiwd/F+FjYdl7MK2V6PU1KA/ANQethL+gYzcfEecSwHuIcBVC04ym3bSoBVgq6hMMjDHmtxXvV2axxEoqn1P0Rc2RKLF8kgoEafXzqQ5Rflrxakpawi9y4gmja8BXvisbZ2VAIWLueSjJwiRlah7moH4HUmKeV4HVM2ZSiHyOupwuIARoo6MjVlxcUgQ5NKizIm/8CDD5MAYwbNc1JZsujIzY+OSkja1c5VEVeJ22IB3aH8wVEZnCyPiEja5apdVmO4ogmDChmJEwISQul6um+DXXmGhxR1/GKtkQOAiTKwy4ElMDOpHS0ty4iLT9w+0A5KPiDC3yU4by/IItHZv1r7FgFI6Xh2uMEjIipyVo1GrWrFZdWCcqnF4CJuRMcXWov6O4pDirU1VzWKUgSgyocX6/tGJiwirVip15xlZLjY+O2tS6KRseG/dvRoMA0r58moMG+D0Qvz1ybJgDY0AFkJB9U0BQ8UhDBwa/3cC9ab62ECvtc50lcDBHDGFq4G4pRnYTA4P6Qc7vk9BuYXDYxsXfpg0b3JLsDz9+ox0/Pq8zfJ0NDQ/7l0GJHzowKgFakpKPvqg3FZ0t/2BK1xu+UM+vtfgRFr+3aGgcrTTpU7BUE+PVSsXqiq8b8XhL9ZZK2sD6l1HBr0A+/CKs7YorKCZZuXrSjkwfsU994uMuYFRvNFG+feqmz9q3br3V1p98spYsLBdSMuavnbRKNPChaJEmOeiUWkgxWGwKMgmZiB8qykMDA1bnQNImKugOCOGmVoNJjgGSMY7k14rTBw/Z1e++xr7z7VusoPn+M+G8NlxDUv/FX3/BvvbVr8hnFmxEmvaXf5qEq/HFQgjVMW8SfbyJJLlASgnDwc0JRoVvB9XTusSmiJN9TB5ImvSXOPHcZLa/8dc1jfSe9/6uff1rX7VV0nRTK+YMM4D95nQ0vyaJbrn1NnvwoYf893Jo0BlE6yqCLxBaP6bjoF+Jw5Z6spl8JdTBxwex/5A9cn0HOiTwASOFK2kA2xZALp+1i3ULuuHGj9rW0/hOfPJj7L79D6ceV68lK/GHAAAAAElFTkSuQmCC";

    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /*                  */

    /* define namespace starts */

    var AD = {};
    AD.collections = {};
	var url = $.config.server_rest_url+'/deviceTypes';
	var idAttribute = AD.idAttribute = "id";
    AD.title = "ns:Menu.TypeList";
    AD.page = "ajax/devicetype_list.html";
    //AD.api = 'cctech';

    if ($.login.user.checkRole('DeviceTypeOperator'))
    	AD.isAdd = true;
    else
    	AD.isAdd = false;

    AD.forms = [
        //forms
        {
            // 'class': 'col col-2 ',
            title: 'id',
            property: 'id',
            default_value: ""
        },
        // "row",
        //form
        {
            // 'class': 'col col-2',
            title: 'name',
            property: 'name',
            default_value: ""
        },
        // "fieldset",
        //form
        {
            // 'class': 'col col-2',
            title: 'Product Type',
            property: 'productTypeId',
            default_value: 1
        },
        //form
        {
            // 'class': 'col col-2',
            title: 'DataModel',
            property: 'defaultDataModelTree',
            default_value: ""
        },
        // "row",
        //form
        {
            // 'class': 'col col-2',
            title: 'Layout',
            property: 'dcmoLayout',
            default_value: ""
        },
        //form
        {
            // 'class': 'col col-2',
            title: 'Report Interval',
            property: 'reportInterval',
            default_value: 3
        },
        //form
        {
            // 'class': 'col col-2',
            title: 'Report Setting',
            property: 'reportSettingList',
            default_value: ""
        },
		//form
        {
            // 'class': 'col col-2',
            title: 'Provision Command',
            property: 'provisionCommand',
            default_value: ""
        }
    ];

	AD.edit = {};
	AD.edit.forms = [
		{
			title: 'Report interval',
			property: 'reportInterval'
		},
		/*
		_.defaults({
			title: 'Report Setting',
			property: 'reportSettingList',
			label_value: function(argument) {
				if(!AD.collections.reportSettingList) {
					var coll = Backbone.getCollectionByUrl($.config.server_rest_url+'/deviceTypes',"id");
					var onErrorHandler = function(collection, response, options){
						console.log("load devicetype "+id+" error");
					};
					coll.fetch({
						async: false,
						data: {
							"fields": ["name","isReport"]
						},
						error: onErrorHandler
					});
					AD.collections.reportSettingList = coll;
				}
				var coll = AD.collections.reportSettingList;
				var jcol = coll.toJSON();
				var ret = [];
				for (var i = 0; i< jcol.length-1; i++) {
					ret.push({
						label: jcol[i].name,
						value: jcol[i].name
					});
				}
				return ret;
			}()
		},Backbone.UIToolBox.selector)
		*/
		{
			title: 'Report Setting',
			property: 'reportSettingList',
			default_value: '',
			view: function(o) {
				var t = this;
				var selector = "<div class=\"btn-group\">"
						+"<select id=\"reportSettingSelector\" class=\"multiselect\" multiple=\"multiple\" style=\"display: none;\">"
						+"</select>"
					+"</div>";
					/*
					+"<div class=\"btn-group\">"
						+"<button type=\"button\" id=\"get-selecte\" disabled=\"disabled\" class=\"btn btn-primary\">"
							+"Choose the items"
						+"</button>"
					+"</div>";
					*/
				return selector;
			},
			value: function(){
				var t = this;
			}
		}
	];

    AD.columns = [
        //column1
        /*
		{
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            callback: function(o) {
                return '<input type="checkbox" value="' + o[idAttribute] + '"></input>';
            }
        },
		*/
        {
			//title: "ns:DeviceType.Title.Appearance",
			title: 'Image',
			filterable: false,
			sortable: false,
			callback: function(o) {
                var deviceTypeIconUrl = "img/device_info/unknown-128.png";
                if (o.hasOwnProperty('deviceTypeIconUrl') && o.deviceTypeIconUrl.length > 0)
                	deviceTypeIconUrl = o.deviceTypeIconUrl;
                return '<img src="'+deviceTypeIconUrl+'"" style="height:100px;" />';
			}
		},
        {
            title: "ns:DeviceType.Title.Name",
            property: 'name',
            filterable: false,
            sortable: true,
            callback: function(o) {
                //return "<i class=\"fa fa-tags\"></i>" + "&nbsp;" + o.name;
				return o.name;
            }
        },		
		{
			title: 'ns:DeviceType.Title.OperatingSystem',
			//property: 'operatingSystemName',
			filterable: false,
			sortable: false,
			callback: function(o) {
				var os = (typeof(o.operatingSystem) === "undefined") ? '' : o.operatingSystem;
				var displayHtml = '';
				if(os==="Android"){
					displayHtml = '<i class="fa fa-android"></i> Android';
				}else if(os==="iOS"){
					displayHtml = '<i class="fa fa-apple"></i> iOS';
				}else if(os==="Windows"){
					displayHtml = '<i class="fa fa-windows"></i> Windows';
				}else{
					displayHtml = '<i class="fa fa-question"></i> Unknow Operating System';
				}
				return displayHtml;
			}
		},
        {
            title: 'ns:DeviceType.Title.ReportInterval',
            property: 'reportInterval',
            filterable: false,
            sortable: true,
			callback: function(o) {
				return o.reportInterval + ' minutes';
			}
        },
        {
            title: 'ns:DeviceType.Title.Remark',
            property: 'remark',
            filterable: false,
            sortable: false
        },
		{
            title: 'ns:DeviceType.Title.Action',
			sortable: false,
			visible: $.login.user.checkRole('DeviceTypeOperator'),
            callback: function(o) {
                var _html = '';
                _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                if ($.common.isDelete)
                	_html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                // _html += '<a href="#" class="btn btn-mini btn-warning removeOne" title="Unlock" data-id="' + o[idAttribute] + '"><i class="icon-unlock"></i> </a>';
                return _html;
            }
        }        
    ];
    /* define namespace ends */

    AD.buttons = [
        {
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
            "sButtonClass": "edit-group-device-list-btn txt-color-white bg-color-greenLight",
            "fnClick": function(nButton, oConfig, oFlash) {
                $(".cctech-gv table").dataTable().fnDraw();
            }
        }
    ];

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */
    var routes = {};

    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/edit/:_id'] = 'act_edit_deviceType';
    routes[AD.page + '/add'] = 'devicetype_add';
    routes[AD.page + '/delete/:_id'] = 'delete_single';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        devicetype_add: function() {
			this.navigate("#ajax/devicetype_add.html", {
				trigger: true
			});
            /*
			var t = this;

            t.$fv.empty();
            t.$gv.hide();

            var m = new t.AD.Model();

            t.fv = new Backbone.FormView({
                el: t.$fv,
                Name: "Add",
                model: new Backbone.Model(),
                forms: t.AD.forms,
                lang: {
                    save: 'Save',
                    back: 'Back'
                }
            });

            t.fv.$el.unbind();
            t.fv.$el.on("click", '[type="submit"]', function(event) {
                t.submit(event, m);
            });
            t.$fv.show();

            pageSetUp();
			*/

        },
		act_edit_deviceType: function(_id) {
            var t = this;            
            t.navigate("#ajax/devicetype_edit.html/" + _id, {
                trigger: true
            });			
		},
        show_edit: function(_id) {
            var t = this;
            t.$fv.empty();
            t.$gv.hide();

            var model = new t.AD.Model();

            var attrs = {};
            var idAttr = model.idAttribute;
            attrs[idAttr] = _id;
            model.set(attrs);

            model.fetch({
                async: false,
                success: function(model, response, options) {
                    t.fv = new Backbone.FormView({
                        el: t.$fv,
                        Name: "Edit DeviceType",
                        model: model,
                        forms: t.AD.edit.forms,
                        /*
						lang: {
                            save: 'Save',
                            back: '<< Previous Page'
                        },
						*/
						button: [
							{
								"type": "button",
								"cls": "btn btn-default",
								"fn": "window.history.back()",
								"text": "Cancel"
							}
						]
                    });
                    t.fv.$el.unbind();
                    t.fv.$el.on("click", '[type="submit"]', function(event) {
                        event.preventDefault();
                        t.submit(event, model, t.AD.edit.forms);
                    });
                    t.$fv.show();
					// 2014/9/16 Kenny
					var reportSettingList = [
						{
							value: 'Battery',
							label: 'Battery'
						},
						{
							value: 'Bluetooth',
							label: 'Bluetooth'
						},
						{
							value: 'CPU',
							label: 'CPU'
						},
						{
							value: 'GPS',
							label: 'GPS'
						},
						{
							value: 'OS',
							label: 'OS'
						}
					];
					$("#reportSettingSelector").multiselect({
						enableFiltering : true,
						includeSelectAllOption: true,
						maxHeight:400,
						buttonWidth:'auto',
						numberDisplayed: 0
					});
					$("#reportSettingSelector").multiselect('dataprovider', reportSettingList);
					// 2014/9/16 End
                    pageSetUp();
                },
                error: function(model, response, options) {
                    window.location.hash = "#ajax/error404.html";
                }
            });
        },
        delete_single: function(id){
			var dmURL = $.config.server_rest_url + '/deviceTypes/';
			$.SmartMessageBox({
                // title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; Delete Device <span class='txt-color-orangeDark'><strong> &nbsp;" +
                //     id + "</strong></span> ?",
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.DeviceType.DeleteDeviceType")+" <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                //content: "Delete the device ?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+i18n.t("ns:Message.DeviceType.DeleteDeviceTypeContent")+" </span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + id,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.DeviceType") + i18n.t("ns:Message.Device.DeleteSuccess"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.DeleteDeviceType") +"</i>",
                            // title: "[Device Type] Deleted successfully",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Device Type</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.DeviceType") + i18n.t("ns:Message.Device.DeleteFailed"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.DeleteDeviceType") +"</i>",
                            // title: "[Device Type] Deleted failed",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Device Type</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/devicetype_list.html";
                    });
                } else {
                    window.location.href = "#ajax/devicetype_list.html";
                }
            });
		}		
    });



    /* define bootstrap starts */
    AD.bootstrap = function() {

		i18n.init(function(t){
			$('[data-i18n]').i18n();
		});
	
        // alert('bootstrap');
        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/grid", {
                trigger: true
            });
        }


        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();

})(jQuery);
