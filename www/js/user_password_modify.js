(function($) {
    var AD = {};
    AD.page = "ajax/user_password_modify.html";
    AD.bootstrap = function() {
		$("body").removeClass("loading");

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
        
        $('#username').text($.login.user.userName);
        $('#save').on('click', function(e) {
            e.preventDefault(); //disable all origin events

            var $valid = $("#change-password-form").valid();
            if (!$valid) {
                console.log('Not valid');
                $validator.focusInvalid();
                return;
            } else {
                console.log('Valid');
            }

            var data = {
                oldPassword: $('input[name="currentpassword"]').val(),
                newPassword: $('input[name="newpassword"]').val()
            };

            url = $.config.server_rest_url + '/changeMyPassword';
            $.ajax({
                type: "POST",
                async: false,
                url: url,
                contentType: 'application/json; charset=utf-8',
                headers: {
                    "X-HTTP-Method-Override": "PUT"
                },
                data: JSON.stringify(data),
                success: function(data) {
                    $.smallBox({
                        title: i18n.t("ns:Message.User.ChangePasswordSuccess"), 
                        content: "<i class='fa fa-pencil-square-o'></i>"+ i18n.t("ns:Message.User.PasswordChangeSuccess"),
                        // title: "Change password success!",
                        // content: "<i class='fa fa-pencil-square-o'></i>Your password is changed successfully.",
                        color: "#648BB2",
                        iconSmall: "fa fa-pencil-square-o fa-2x fadeInRight animated",
                        timeout: 5000
                    });  
                    history.back();            
                },
                error: function(data) {
                    $.smallBox({
                        title: i18n.t("ns:Message.User.ChangePasswordFailed"), 
                        // title: "Change password failed!",
                        content: "<i class='fa fa-pencil-square-o'></i>"+data.responseText,
                        color: "#B36464",
                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                        timeout: 5000
                    });                          
                },
                dataType: "json"
            });


        //     var data = {
        //         password: $('input[name="newpassword"]').val()
        //     };

        //     url = $.config.server_rest_url + '/users/' + $.login.user.get('userId');
        //     $.ajax({
        //         type: "POST",
        //         async: false,
        //         url: url,
        //         contentType: 'application/json; charset=utf-8',
        //         headers: {
        //             "X-HTTP-Method-Override": "PUT"
        //         },
        //         data: JSON.stringify(data),
        //         success: function(data) {
        //             $.smallBox({
        //                 title: "Change password success!",
        //                 content: "<i class='fa fa-pencil-square-o'></i>Your password is changed successfully.",
        //                 color: "#648BB2",
        //                 iconSmall: "fa fa-pencil-square-o fa-2x fadeInRight animated",
        //                 timeout: 5000
        //             });              
        //         },
        //         error: function(data) {
        //             $.smallBox({
        //                 title: "Change password failed!",
        //                 content: "<i class='fa fa-pencil-square-o'></i>"+data.responseText,
        //                 color: "#B36464",
        //                 iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
        //                 timeout: 5000
        //             });                          
        //         },
        //         dataType: "json"
        //     });


        });


        var validateRule = {
            currentpassword: {
                required: true
            },
            newpassword: {
                required: true
            },
            confirmnewpassword: {
                required: true,
                equalTo: '#newpassword'
            }
        };

        var $validator = $("#change-password-form").validate({

            rules: validateRule,

            messages: {
                // rolename: "User name MUST be filled",
                // description: "Please specify something to remind you",
                // email: "Please use the available email address"
                confirmnewpassword: i18n.t("ns:User.EnterSameValue")
            },

            // highlight: function(element) {
            //     $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
            // },
            // unhighlight: function(element) {
            //         $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
            //     }
                // ,
                // errorElement: 'span',
                // errorClass: 'help-block',
                // errorPlacement: function(error, element) {
                //     if (element.parent('.input-group').length) {
                //         error.insertAfter(element.parent());
                //     } else {
                //         error.insertAfter(element);
                //     }
                // }
        });
    }
    $[AD.page] = AD;

})(jQuery);
