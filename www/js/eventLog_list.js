(function($) {
    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');

    var AD = {};
    AD.collections = {};
    var url = $.config.server_rest_url + '/deviceEventLogs';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "ns:Menu.EventLog";
    AD.page = "ajax/eventLog_list.html";
    //先將API格式換成cctech的(模擬的時候跟後台提供的API格式不同)
    //AD.api = 'cctech';
    AD.isAdd = false;
	AD.sort = {
		"column": "id",
		"sortDir": "desc"
	};

    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /*                  */

    AD.validate = function() {
        return true;
    };
	
	AD.buttons = [
		{
			"sExtends": "text",
			"sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
			"sButtonClass": "edit-group-device-list-btn txt-color-white bg-color-greenLight",
			"fnClick": function(nButton, oConfig, oFlash) {
				$(".cctech-gv table").dataTable().fnDraw();
			}
		}
	];
	
	AD.forms = [
    ];

    AD.columns = [
        //columns1
        /*{
            title: 'ID',
            property: 'deviceEventId',
			//width: '80px',
            filterable: false,
            sortable: true
		},*/
		/*{
            title: 'Target',
            property: 'eventName',
            filterable: false,
            sortable: true,
        },*/
        {
            title : 'Event',
            property : 'eventContent',
            cellClassName : 'eventContent',
            filterable : false,
            sortable : false,
            callback: function(o) { 
                var msg = ""; 
                if (o.type == "Root" && o.eventContent == "=,1")
                    msg = "Device is rooted";    
                else if (o.type == "BatteryLevel")
                {
                    msg += "Battery level ";
                    if (o.eventContent.split(",")[0] == ">")
                        msg += "is higher than ";
                    else 
                        msg += "is lower than ";
                    msg += o.eventContent.split(",")[1];
                }       
                return msg;                     
            }
        },
        {
            title: 'From',
            property: 'deviceName',
            filterable: false,
            sortable: false,
        },
        {
            title: 'Occured',
            property: 'creationTime',
            filterable: false,
            sortable: false
        },
        {
            title: 'Level',
			property: 'severity',
            filterable: false,
            sortable: true, 
            callback: function(o) {
                    var color;
                    var des;
                    switch (o.severity)
                    {
                        case 5:
                            color = "#ee0000";
                            des = "Critical";
                            break;
                        case 4:
                            color = "#ee7700";
                            des = "Very High";
                            break;
                        case 3:
                            color = "#eeee00";
                            des = "High";
                            break;
                        case 2:
                            color = "#a2a200";
                            des = "Medium";
                            break;
                        case 1:
                            color = "#51a200";
                            des = "Low";
                            break;
                        default:
                            color = "#919191";
                            des = "--";
                    }
                    return "<i class=\"fa fa-circle\" style=\"color:"+color+";\"></i> " + des;
                }    
        }
    ];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */
    var routes = {};

    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/redo/:_id'] = 'redo';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
		redo: function(_id){
			var ret = new AD.Model({
				id:_id
			});
			ret.fetch({async:false});
			ret.save({
				"status" : "redo"
			},{async:false});
			this.navigate("#"+AD.page+"/grid",{trigger:true});
		}
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        $("body").removeClass("loading");
		// alert('bootstrap');
        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/grid", {
                trigger: true
            });
        }


        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		
		Backbone.Events.off("TaskUpdate");
		Backbone.Events.on("TaskUpdate", function(data){
			//console.log("TaskLog need to refresh!");
			$(".cctech-gv table").dataTable().fnDraw();
		});

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();


})(jQuery);
