(function($) {
    var AD = {};
    AD.page = "ajax/agent_list.html";	    
    
    // Windows
    function s1() {
    	var AD = {};
    	AD.page = "ajax/agent_list.html";
		AD.collections = {};

		var url = $.config.server_rest_url + "/omaAgentFiles" + "?search=operatingSystemName:" + "Windows";
		var idAttribute = AD.idAttribute = "id";

		if ($.login.user.checkRole('OmaAgentFileOperator'))
			AD.buttons = [
		    	//button
		        {
		            "sExtends" : "text",
		            "sButtonText" : "<i class='fa fa-cloud-upload'></i>&nbsp; " + i18n.translate("ns:File.Upload"),
		            "sButtonClass" : "upload-btn s1-btn btn-default txt-color-white bg-color-blue"
		        },
		        {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash) {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
		    ];

		AD.columns = [
	        {
	            title : 'ns:File.Agent.Title.Name',
	            property : 'name',
	            cellClassName : 'name',
	            filterable : false,
	            sortable : true,
	        },	   
            /*{
	            title : 'ns:File.Agent.Title.Domain',
	            property : 'domainId',
	            cellClassName : 'domainName',
	            filterable : false,
	            visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
	            sortable : false,
	            callback: function(o) {
	                return (typeof(o.domainName) === "undefined") ? 'DefaultDomain' : o.domainName;
	            }
	        },*/{
                title : 'ns:File.Agent.Title.Creator',
                property : 'creatorName',
                cellClassName : 'creatorName',
                filterable : false,
                sortable : false
            },{
	            title : 'ns:File.Agent.Title.Version',
	            property : 'version',
	            cellClassName : 'version',
	            filterable : false,
	            sortable : false
	        },{
	            title : 'ns:File.Agent.Title.FileSize',
	            property : 'size',
            	cellClassName : 'size', 
	            filterable : false,
	            sortable : false,
	            callback : function (o) {
	                if (o.size == null) {
	                    return "Unknown";                    
	                } else {
	           			var fileSize = o.size.toFixed(0) + " Bytes";
				        
				        if (o.size > 1024 && o.size < (1024 * 1024)) {
				          	fileSize = (o.size / 1024).toFixed(0) + " KB";
				        } else if (o.size > 1024 * 1024) {
				          	fileSize = (o.size / (1024 * 1024)).toFixed(1) + " MB";
				        }
				        return fileSize;
	                }
	            }
	        },{
	            title : 'ns:File.Agent.Title.UploadedTime',
	            property : 'creationTime',
            	cellClassName : 'creationTime', 
	            filterable : false,
	            sortable : true,
	            callback: function(o) {
                 	return o.creationTime;
                }
	        },{
	            title : 'ns:File.Agent.Title.Remark',
	            property : 'remark',
	            cellClassName : 'remark',
	            filterable : false,
	            sortable : false, 
	            callback : function (o) {
	                if (o.remark == null) {
	                    return "Unknown";                    
	                } else {
	                    return o.remark;
	                }
	            }
	        },
            //columns Action
			{
	            title : 'ns:File.Agent.Title.Action',
	            sortable : false,
	            width: '100px',
	            visible: $.login.user.checkRole('AgentFileOperator'),
	            callback : function (o) {
	                var _html = '';
	                // [Action] Download the agent
                        _html += '<a href="'  + o.fileServerPath + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-teal" style="display: inline;"><i class="fa fa-cloud-download"></i></a>' + "&nbsp;";                            
	                // [Action] Delete the agent
	                if ($.common.isDelete)
	                	_html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm btn_delete DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
	                return _html;
	            }
	        }	        
	        
	    ];

		/* model starts */
		AD.Model = Backbone.Model.extend({
			urlRoot: url,
			idAttribute: idAttribute
		});
		/* model ends */

		/* collection starts */
		AD.Collection = Backbone.Collection.extend({
			url: url,
			model: AD.Model
		});	
		/* collection ends */

		var routes = {};
		routes[AD.page + '/delete/:_id'] = 'act_delete';   			 // [Action] Delete the File
		routes[AD.page + '/grid/s1']     = 'act_s1';                 // [Action] Act to s1
		routes[AD.page] = 'render';

		var MyRouter = Backbone.DG1.extend({        
        	routes: routes,
        	initialize: function(opt) {
            	var t = this;
	            //JavaScript中使?�super繼承?�方�? 使用prototype. ... .call()
	            Backbone.DG1.prototype.initialize.call(t, opt);
	            t.$bt = $(opt.bts).first();
	            console.error();
	            this.show_grid();
        	},
        	show_grid: function() {
        		var t = this;

        		var gvs = "#s1 .cctech-gv";
				var gv = $(gvs).first();
				
				if(!agentGrid) {
					var agentGrid = new Backbone.GridView({
						el: t.$gv,
						collection: new t.AD.Collection(),
						buttons: t.AD.buttons,
						columns: t.AD.columns,
						AD: t.AD
					});

					gv.on("click", ".s1-btn", function (event) {
		                event.preventDefault();

		                var hash = window.location.hash;
		                var url = location.hash.replace(/^#/, '');
		                
		                var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
		                url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');		               
		                
		                t.navigate("#ajax/agent_upload_windows.html", {
                            trigger: true
                        });
		            });
				} else {
					agentGrid.refresh();
				}
				t.$gv.show();
				//pageSetUp();
        	},
        	act_delete: function(id) {
        		event.preventDefault();         	
	            var t = this;

	            $.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Agent.DeleteAgentFile")+"<span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+i18n.t("ns:Message.Agent.DeleteAgentFileContent")+"</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                    	$("body").addClass("loading");

                        $.ajax({                
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/agent/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[Windows Agent] Deleted failed");
                            }
                        }).done(function(data){
                        	if (typeof(data) === 'undefined'){
                                console.log("[Windows Agent] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Agent.WindowsAgent") + i18n.t("ns:Message.Agent.DeleteSuccess"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Agent.DeleteAgent") + "</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {                                            
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Agent.WindowsAgent") + i18n.t("ns:Message.Agent.DeletedFailed"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Agent.DeleteAgent") + "</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[Windows Agent] Deleted failed => " + JSON.stringify(data));
                                };
                            }                            
                        }).fail(function(){
                            $.smallBox({
                                title: i18n.t("ns:Message.Agent.WindowsAgent") + i18n.t("ns:Message.Agent.DeletedFailed"),
                                content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Agent.DeleteAgent") + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            }); 
                        }).always(function() {
                        	$("body").removeClass("loading");    
                            t.navigate("#ajax/agent_list.html/grid/s1", {
	                            trigger: true
	                        });
                        });
                    } else {
                        t.navigate("#ajax/agent_list.html/grid/s1", {
                            trigger: true
                        });
                    }
                });	             
        	},
            act_s1: function(id) {
                s1();
            }
        });

		pageSetUp();

		AD.app = new MyRouter({
            "AD": AD
        });
    };

    // Android
    function s2() {
    	var AD = {};
    	AD.page = "ajax/agent_list.html";
		AD.collections = {};

		var url = $.config.server_rest_url + "/omaAgentFiles" + "?search=operatingSystemName:" + "Android";
		var idAttribute = AD.idAttribute = "id";

		if ($.login.user.checkRole('OmaAgentFileOperator'))
			AD.buttons = [
		    	//button
		        {
		            "sExtends" : "text",
		            "sButtonText" : "<i class='fa fa-cloud-upload'></i>&nbsp; " + i18n.translate("ns:File.Upload"),
		            "sButtonClass" : "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
		        },
		        {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; "+ i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash) {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
		    ];

		AD.columns = [
	        {
	            title : 'ns:File.Agent.Title.Name',
	            property : 'name',
	            cellClassName : 'name',
	            filterable : false,
	            sortable : true,
	        },/*{
	            title : 'ns:File.Agent.Title.Domain',
	            property : 'domainId',
	            cellClassName : 'domainName',
	            filterable : false,
	            visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
	            sortable : false,
	            callback: function(o) {
	                //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknown' : $.config.Domain[o.domainId] ;
	                return (typeof(o.domainName) === "undefined") ? 'DefaultDomain' : o.domainName;
	            }
	        },*/{
                title : 'ns:File.Agent.Title.Creator',
                property : 'creatorName',
                cellClassName : 'creatorName',
                filterable : false,
                sortable : false
            },{
	            title : 'ns:File.Agent.Title.Version',
	            property : 'version',
	            cellClassName : 'version',
	            filterable : false,
	            sortable : false
	        },{
	            title : 'ns:File.Agent.Title.FileSize',
	            property : 'size',
            	cellClassName : 'size', 
	            filterable : false,
	            sortable : false,
	            callback : function (o) {
	                if (o.size == null) {
	                    return "Unknown";                    
	                } else {
	           			var fileSize = o.size.toFixed(0) + " Bytes";
				        
				        if (o.size > 1024 && o.size < (1024 * 1024)) {
				          	fileSize = (o.size / 1024).toFixed(0) + " KB";
				        } else if (o.size > 1024 * 1024) {
				          	fileSize = (o.size / (1024 * 1024)).toFixed(1) + " MB";
				        }
				        return fileSize;
	                }
	            }
	        },{
	            title : 'ns:File.Agent.Title.UploadedTime',
	            property : 'creationTime',
            	cellClassName : 'creationTime', 
	            filterable : false,
	            sortable : true,
	            callback: function(o) {
                 	return o.creationTime;
                }
	        },
	        {
	            title : 'ns:File.Agent.Title.Remark',
	            property : 'remark',
	            cellClassName : 'remark',
	            filterable : false,
	            sortable : false, 
	            callback : function (o) {
	                if (o.remark == null) {
	                    return "Unknown";                    
	                } else {
	                    return o.remark;
	                }
	            }
	        },
			{
	            title : 'ns:File.Agent.Title.Action',
	            sortable : false,
	            visible: $.login.user.checkRole('FirmwareFileOperator'),
	            callback : function (o) {
	                var _html = '';
	                // [Action] Download the agent
                        _html += '<a href="'  + o.fileServerPath + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-teal" style="display: inline;"><i class="fa fa-cloud-download"></i></a>' + "&nbsp;";                            
	                // [Action] Delete the agent
	                if ($.common.isDelete)
	                	_html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm btn_delete DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
	                return _html;
	            }
	        }	        
	    ];
		/* define namespace ends */

		


		/* model starts */
		AD.Model = Backbone.Model.extend({
			urlRoot: url,
			idAttribute: idAttribute
		});
		/* model ends */

		/* collection starts */
		AD.Collection = Backbone.Collection.extend({
			url: url,
			model: AD.Model
		});			
		/* collection ends */		
		
		var routes = {};
		routes[AD.page + '/delete/:_id'] = 'act_delete';   // [Action] Delete the File
		routes[AD.page + '/grid/s2']     = 'act_s2';       // [Action] Act to s2
		routes[AD.page] = 'render';		

		var MyRouter = Backbone.DG1.extend({        
        	routes: routes,
        	initialize: function(opt) {
            	var t = this;
	            //JavaScript中使?�super繼承?�方�? 使用prototype. ... .call()
	            Backbone.DG1.prototype.initialize.call(t, opt);
	            t.$bt = $(opt.bts).first();
	            console.error();
	            this.show_grid();
        	},
        	show_grid: function() {
        		var t = this;

        		var gvs = "#s1 .cctech-gv";
				var gv = $(gvs).first();
				
				if(!bootLoaderGrid) {
					var bootLoaderGrid = new Backbone.GridView({
						el: t.$gv,
						collection: new t.AD.Collection(),
						buttons: t.AD.buttons,
						columns: t.AD.columns,
						AD: t.AD
					});
					
					gv.on("click", ".s2-btn", function (event) {
		                event.preventDefault();

		                var hash = window.location.hash;
		                var url = location.hash.replace(/^#/, '');
		                
		                var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
		                url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');		               
		                
		                t.navigate("#ajax/agent_upload_android.html", {
                            trigger: true
                        });
		            });
				} else {
					bootLoaderGrid.refresh();
				}
				t.$gv.show();
				//pageSetUp();
        	},
        	act_delete: function(id) {
        		event.preventDefault();
        		var t = this;         	
        		
        		$.SmartMessageBox({
                    title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Agent.DeleteAgentFile")+"<span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span>?",
                    content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'>"+i18n.t("ns:Message.Agent.DeleteAgentFileContent")+"</span>",
                    buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

                }, function(ButtonPressed) {
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                    	$("body").addClass("loading");

                        $.ajax({                
                            beforeSend: function(xhr) {
                                xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                            },
                            type: 'POST',
                            url: $.config.server_rest_url + '/omaAgentFiles/' + id,
                            dataType: 'json',
                            timeout: 10000,
                            error: function(jqXHR, textStatus, errorThrown) {
                                console.log("[Android Agent] Deleted failed");
                            }
                        }).done(function(data){
                            if (typeof(data) === 'undefined'){
                                console.log("[Android Agent] Deleted done => response undefined.");
                            } else {
                                if (data.status) {
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Agent.AndroidAgent") + i18n.t("ns:Message.Agent.DeleteSuccess"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Agent.DeleteAgent") + "</i>",
                                        color: "#648BB2",
                                        iconSmall: "fa fa-times fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                } else {                                            
                                    $.smallBox({
                                        title: i18n.t("ns:Message.Agent.AndroidAgent") + i18n.t("ns:Message.Agent.DeletedFailed"),
                                        content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Agent.DeleteAgent") + "</i>",
                                        color: "#B36464",
                                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                        timeout: 5000
                                    });
                                    console.log("[Android Agent] Deleted failed => " + JSON.stringify(data));
                                };
                            } 
                        }).fail(function(){
                            $.smallBox({
                                title: i18n.t("ns:Message.Agent.AndroidAgent") + i18n.t("ns:Message.Agent.DeletedFailed"), 
                                content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Agent.DeleteAgent") + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            }); 
                        }).always(function() {
                        	$("body").removeClass("loading");
                            t.navigate("#ajax/agent_list.html/grid/s2", {
                                trigger: true
                            });
                        });
                    } else {
                        t.navigate("#ajax/agent_list.html/grid/s2", {
                            trigger: true
                        });
                    }
                }); 
        	},
        	act_s2: function(id) {
                s2();
            }
        });

		pageSetUp();

		AD.app = new MyRouter({
            "AD": AD
        });
    };
		
    AD.bootstrap = function() {
		i18n.init(function(t){
			$('[data-i18n]').i18n();			
		});		
		
		if (!$.login.user.checkRole('OmaAgentFileViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        }

	    $("#myTab li").click(function() {
			switch (this.firstElementChild.id) {
		        case "s1":
		            s1();
		            break;
		        case "s2":
		            s2();
		            break;
		    }
		});

	    if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

		// Initial page & after uploading
        var hash = window.location.hash;
        var url = location.hash.replace(/^#/, '');        
        var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');        

        switch (action) {
            case "grid/u1": //Windows
            case "grid/s1":
                $("#tab2").removeClass('active');
                $("#tab1").addClass('active');
                s1();
                break;
            case "grid/u2": //Android
            case "grid/s2":
                $("#tab1").removeClass('active');
                $("#tab2").addClass('active');
                s2();
                break;
            default:    //Default
                s2();
                break;
        }       
    };

    $[AD.page] = AD;
    var fnRefreshDataForAgentFile = function() 
    {
        if ($('#divAgentFileMain').length > 0) 
        {
            console.log("[Data Refresh] Agent File data refreshed.");
            $(".cctech-gv table").dataTable().fnDraw(false);
        }   
    }

    // console.log('oRefreshData.oAgentFile.bIsEnable='+oRefreshData.oAgentFile.bIsEnable)
    // console.log('oRefreshData.oAgentFile.oTimerId='+oRefreshData.oAgentFile.oTimerId)
    if (oRefreshData.oAgentFile.bIsEnable === true && oRefreshData.oAgentFile.oTimerId === null)
    {
        oRefreshData.oAgentFile.oTimerId = window.setInterval(fnRefreshDataForAgentFile, oRefreshData.oAgentFile.iInterval);
        // console.log('oRefreshData.oAgentFile.oTimerId='+oRefreshData.oAgentFile.oTimerId)
    }    
})(jQuery);