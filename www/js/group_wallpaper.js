(function($) {        
    var AD = {};
    AD.page = "ajax/group_wallpaper.html";

    var setGroupInfoField = function(groupInfo, action) {
        // Dynamic div
        var actionTitle = action.toUpperCase().charAt(0) + action.substring(1);
        
        var data = {
            id: groupInfo.id,
            groupName: groupInfo.name,
            // title: actionTitle
            title: i18n.t("ns:Group.Title."+actionTitle)
        };        

        // Set title field
        var source = $("#dynamicTitleDiv").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $("#dynamicTitle").html(html);

        // Set header field
        var source = $("#dynamicHeaderDiv").html();
        var template = Handlebars.compile(source);
        var html = template(data);
        $("#dynamicHeader").html(html);

        // Set group name field
        $('#groupname').val(groupInfo.name);
        // Set group remark field
        $('#remark').val(groupInfo.remark);
    };

    var getGroupInfo = function(id, action) {                
        var data;        

        $("body").addClass("loading");
        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/deviceGroups/' + id,
            data: data,
            dataType: 'json',
            timeout: 10000,
            async: false,
            error: function(data, status, error){
                console.log("[Group "+ action +"] Got group info failed. => " + data.responseText);            
            }
        }).done(function(data){        
            // Set group info in the field
            setGroupInfoField(data, action);
        }).fail(function(){
            $.smallBox({
                title: i18n.t("ns:Message.Group.BracketGroup")+ i18n.t("ns:Group.Actions."+ action ) +i18n.t("ns:Message.Group.BracketGotGroupInfoFailed"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Group.GotGroupInfoFailed") + "</i>",
                // title: "[Group "+ action +"] Got group info failed",
                // content: "<i class='fa fa-pencil-square-o'></i> <i>Got group info failed</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Group "+ action +"] Got group info failed.");
            window.location.href = "#ajax/group_detail.html/" + id;
        }).always(function(){
            $("body").removeClass("loading");
        });
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        var hash = window.location.hash;
        var urlSplit = hash.replace(/^#ajax\/group_wallpaper.html\/(\w+)/, "$1").split('/');        
        
        if (!(typeof urlSplit === 'undefined')) {
            var id = urlSplit[0];
            var action = urlSplit[1];
        }

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#ajax/group_detail.html/" + id, {
                trigger: true
            });
        } else {
            // Get groupInfo
            getGroupInfo(id, action);      
        }

        $('#scheduleTime').timepicker();
        $('#scheduleTask').on("click",function(){
            var isSchedule = $(this).prop('checked');
            if(isSchedule){
                $('#scheduleSetting').show();
            } else {
                $('#scheduleSetting').hide();
            }
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }

        $('.superbox').SuperBox();
        $("body").removeClass("loading");
    };
    /* define bootstrap ends */
    $[AD.page] = AD;

    function translatePlaceholder () {
        $("#groupname").attr("placeholder", i18n.t("ns:Group.Message.GroupName"));
        $("#remark").attr("placeholder", i18n.t("ns:Group.Message.Remark"));
    }

})(jQuery);
