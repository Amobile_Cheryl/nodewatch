(function($) {
    /* define namespace starts */

    var AD = {};
    AD.title = "ns:Menu.DeviceReport";
	AD.page = "ajax/device_report.html";
	var colorList = ["bg-color-blue", "bg-color-greenLight", "bg-color-red", "bg-color-yellow",
					 "bg-color-blueLight","bg-color-teal", "bg-color-orange", "bg-color-blueDark"];

	function init(){
		chartRender();
		
	}
	function showProgressBar(dataInfo){
		AD.Collection = new Array();
		
		
		var total = 0;
		$.ajax({
			type: 'GET',
			url: $.config.server_rest_url + '/devices?isCount=true',
			dataType: 'json',
			async: false,
			timeout: 5000
		}).done(function(result){
			total = result.amount;
			$.each(dataInfo,function(dataType,data){
				AD.Model = new Backbone.Model();
				var collection = new Backbone.Collection({
					model: AD.Model
				});
				var i = 0;
				$.each(data,function(key,value){
					if(value.count !== 0){	
						var pg = new Backbone.ProgressBar({
							el:			$("#"+dataType+"Report"),
							id:			value.name,
							link:		null,
							label:		value.name,
							remark:		null,
							data:		value.count + "/" + total,
							percentage:	(value.count/total*100).toFixed(2),
							color:		colorList[i%8]
						});
						var record = {};
						record.name = value.name;
						record.counter = value.count;
						collection.add(record);
						i++;
					}
				});
				AD.Collection.push(collection);
				tableRender(dataType,collection);
			});
		});
	}
	
	function showProgressBar_bak(){
		AD.Collection = new Array();
		var dataTypes = [
			{
				type: 'DeviceType',
				url: '/report?reportType=DeviceType',
				link: null
			},{
				type: 'LockState',
				url: '/report?reportType=DeviceLock',
				link: null
			},{
				type: 'Group',
				url: '/report?reportType=DeviceGroup',
				link: null
			}];
		
		var colorList = ["bg-color-blue", "bg-color-greenLight", "bg-color-red", "bg-color-yellow",
                         "bg-color-blueLight","bg-color-teal", "bg-color-orange", "bg-color-blueDark"];
		
		for(var i=0;i<dataTypes.length;i++){
			AD.Model = new Backbone.Model();
			var collection = new Backbone.Collection({
				model: AD.Model
			});
			var total = 0;
			var dataType = dataTypes[i];
            $.ajax({
                url: $.config.server_rest_url + dataType.url,
                type: 'GET',
                dataType: 'json',
                async: false
            }).done(function(data) {
                $.each(data,function(key,value){
					var pg = new Backbone.ProgressBar({
						el:			$("#"+dataType.type+"Report"),
						id:			key,
						link:		dataType.link,
						label:		key,
						remark:		null,
						data:		value + "/" + total,
						percentage:	(value/total*100).toFixed(2),
						color:		eval('colorList.'+key.toLowerCase())
					});
					var record = {};
					record.name = key;
					record.counter = value;
					collection.add(record);
                });
				AD.Collection.push(collection);
				tableRender(dataType.type,collection);
            }).fail(function(){
            });
        }
	}
	
	function chartRender(){     
        //showProgressBar();
		var data;
        $.ajax({
            type: 'GET',
			url: $.config.server_rest_url + '/deviceReports', // TODO peroid
            data: data,
            dataType: 'json',
            async: false,
            timeout: 20000,
            error: function(data, status, error){
                console.log("[Dashboard] Got device info failed. => " + data.responseText);
            }
        }).done(function(data){        
            showProgressBar(data);            
        }).fail(function(){
            //TODO          
            $.smallBox({
                title: i18n.t("ns:Message.Dashboard.Dashboard") + i18n.t("ns:Message.Dashboard.GotMonitorInfoFailed"),
                content: "<i class='fa fa-tasks'></i> <i>"+ i18n.t("ns:Message.Dashboard.GotMonitorInfoFailed") + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Report] Got device info failed.");
        }).always(function(){
            $("body").removeClass("loading");
        });         
    };
	
	function tableRender(dataType,collection) {
		var reportGridHtml = "<table class=\"table table-striped noselect\" style=\"\">"
			+"<tbody>"
				+"<tr><th>"+i18n.t("ns:Report.DeviceReport.Item")+"</th><th>"+i18n.t("ns:Report.DeviceReport.NumberOfDevices")+"</th>"
				+"{{#each data}}"
				+"<tr><td>{{name}}</td><td>{{counter}}</td></tr>"
				+"{{/each}}"
			+"</tbody>"
		+"</table>";
		var ReportGridView = Backbone.View.extend({
			template: Handlebars.compile(reportGridHtml),
			render: function() {
				var collection = this.collection.toJSON();
				$(this.el).html(this.template({
					data: collection
				}));
				$("#"+dataType+"ReportGrid").html(this.$el);
			}
		});
		var reportGridView = new ReportGridView({
			collection: collection,
			reportGridHtml: reportGridHtml
		});
		reportGridView.render();
	}
	
    /* define bootstrap starts */
    AD.bootstrap = function() {

		// Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        Backbone.emulateHTTP = true;
        Backbone.history.start();
		$("body").removeClass("loading");
		
		init();
    };
    /* define bootstrap ends */

	$[AD.page] = AD;


})(jQuery);