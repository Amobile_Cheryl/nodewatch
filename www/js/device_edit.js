(function($) {        
    var AD = {};
    AD.page = "ajax/device_edit.html";

    var getDeviceInfo = function(id, data) {
        $("body").addClass("loading");

        $.ajax({
            type: 'GET',
            url: $.config.server_rest_url + '/devices/' + id,
            data: data,
            dataType: 'json',
            timeout: 15000,
            async: false,
            error: function(data, status, error){
                console.log("[Device edit] Got device info failed. => " + data.responseText);                
            }
        }).done(function(data){
            $("#deviceName").text(data.name);
            $('input[name="devicename"]').val(data.name);
            $('input[name="operatingSystem"]').val(data.operatingSystemName);
            $('input[name="remark"]').val(data.remark); 
            // deviceIdSet = data.deviceIdSet;
            console.log(data);       
        }).fail(function(){
            $.smallBox({
                title: i18n.t("ns:Message.Device.DeviceEdit") + i18n.t("ns:Message.Device.GotDeviceInfoFailed"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Device.EditDeviceID") + "&nbsp;" + id + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            console.log("[Device edit] Got device info failed.");
            window.location.href = "#ajax/device_list.html";            
        }).always(function(){
            $("body").removeClass("loading");
        });
    };

    var putDevice = function(id, data) {
        if (id != null) {
            $.ajax({
                beforeSend: function(xhr) {
                    xhr.setRequestHeader('X-HTTP-Method-Override', 'PUT');
                },
                type: 'POST',
                crossDomain: true,
                url: $.config.server_rest_url + '/devices/' + id,            
                contentType: 'application/json',
                data: data,
                timeout: 10000,
                error: function(data, status, error){
                    console.log("[Device edit] POST failed. => " + data.responseText);
                }
            }).done(function(data){
                $.smallBox({
                    title: i18n.t("ns:Message.Device.DeviceEdit") + i18n.t("ns:Message.Device.EditedSuccess"),
                    content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Device.EditDeviceID") + "&nbsp;" + id + "</i>",
                    color: "#648BB2",
                    iconSmall: "fa fa-pencil-square-o fa-2x fadeInRight animated",
                    timeout: 5000
                });
            }).fail(function(){
                $.smallBox({
                    title: i18n.t("ns:Message.Device.DeviceEdit") + i18n.t("ns:Message.Device.EditedFailed"),
                    content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Device.EditDeviceID")+"&nbsp;" + id + "</i>",
                    color: "#B36464",
                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                    timeout: 5000
                });                   
            }).always(function(){
                window.location.href = "#ajax/device_list.html";
                //window.history.back();
            });
        }
    };

    var reloadJs = function(){
        var data;
        var deviceIdSet = [];
        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/device_edit.html\/(\w+)$/, "$1");

        //event.preventDefault();        
                
        if (typeof(id) === 'undefined'){
            console.log("[Device edit] Got device info failed.");
            $.smallBox({
                title: i18n.t("ns:Message.Device.GotDeviceInfoFailed"),
                content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Device.EditDeviceID") + "&nbsp;" + data.name + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
            window.location.href = "#ajax/device_list.html";              
        } else {
            getDeviceInfo(id, data);    
        }  

        // Back button event
        $("#btn_back").click(function(event){        
            event.preventDefault();        
            window.location.href = "#ajax/device_list.html";
        });

        // Save button event
        $("#btn_save").click(function(event){                
            event.preventDefault();

            var originalData = {            
                label: $('input[name="label"]').val() || ""                    
                // ,deviceIdSet: $('#deviceSelect').val() || []
            };
            var data = JSON.stringify(originalData);
            putDevice(id, data);
        });
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
        $("body").removeClass("loading");

        reloadJs();        

        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/device_edit.html\/(\w+)$/, "$1");

        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/" + id, {
                trigger: true
            });
        }

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
    };
    /* define bootstrap ends */
    $[AD.page] = AD;

})(jQuery);
