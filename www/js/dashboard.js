(function($)
{
    pageSetUp();

    // Dashboard data refresh period (millisecond)
    var dashboardDataRefreshPeriod = 15000;

    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /*                  */

    var AD = {};
    AD.collections = {};
    var url = AD.url = $.config.server_rest_url + "/userLogs?search=action:login";
    var idAttribute = AD.idAttribute = "id";
    var dataUpperBound = 9999;
    AD.title = "User Log";
    AD.page = "ajax/dashboard.html";
    //AD.api = 'cctech';
    AD.isAdd = false;
    AD.sort = {
        "column": "id",
        "sortDir": "desc"
    };

    AD.validate = function()
    {
        return true;
    };

    AD.columns = [
    {
        title: 'ns:Log.UserLog.Name',
        property: 'userName',
        filterable: true,
        sortable: false,
        width: '250px'
    },
    {
        title: 'Description', //'ns:Log.UserLog.Remark',
        property: 'remark',
        //cellClassName: 'remark',
        filterable: true,
        sortable: false,
        width: '500px'
    },
    {
        title: 'ns:Log.UserLog.Time',
        property: 'creationTime',
        //cellClassName: 'creationTime',
        filterable: true,
        sortable: true,
        width: '180px'
    }];

    AD.Model = Backbone.Model.extend(
    {
        urlRoot: url,
        idAttribute: idAttribute
    });

    AD.Collection = Backbone.Collection.extend(
    {
        url: url,
        model: AD.Model
    });
    AD.title = "ns:Menu.UserLog";
    var routes = {};
    routes[AD.page] = 'render';
    var currentDeiveGroupPageNumber=1;
    var MyRouter = Backbone.DG1.extend(
    {
        routes: routes,
        initialize: function(opt)
        {
            var t = this;
            t.options = opt;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            opt.gvs = opt.gvs || ".cctech-gv";
            t.$bt = $(opt.bts).first();
            t.$gv = $(opt.gvs).first();

            if (!t.gv)
            {
                t.gv = new Backbone.GridView(
                {
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: false,
                    sort: t.AD.sort,
                    filter: t.AD.filter,
                    buttons: t.AD.buttons,
                    per_page: 6,
                    AD: t.AD
                });
            }
            else
            {
                t.gv.refresh();
            }

            Backbone.Events.off("UserLogUpdate");
            Backbone.Events.on("UserLogUpdate", function(data)
            {
                //console.log("UserLog need to refresh!");
                t.gv.refresh();
            });
            //add by Alvin to hide unused toolbar
            $(".cctech-gv .widget-body-toolbar").hide();
        }
    });

    var showGroupTaskBlock = function(groupsCollection)
    {
        var maxValue = 5;
        var readyTask = {};
        var runningTask = {};
        var doneTask = {};
        var reqCnt = 0;
        var blocks = [];
        $.each(groupsCollection.content, function(index)
        {
            $.ajax(
            {
                url: $.config.server_rest_url + '/deviceGroups/' + groupsCollection.content[index].id + '/taskExecutionStatus',
                dataType: 'JSON',
                success: function(taskStatus)
                {
                    reqCnt++;

                    var group_block_data = {
                        'group_name': groupsCollection.content[index].name,
                        'ready_percent': taskStatus.Ready / 10 * 100 * 0.84,
                        'running_percent': taskStatus.Running / 10 * 100 * 0.84,
                        'done_task': taskStatus.Done,
                        'ready_task': taskStatus.Ready,
                        'running_task': taskStatus.Running,
                        'index': index
                    }

                    var tpl_group_task_block = $("#tpl_group_task_block").html();
                    var tpl_group_task_block_template = Handlebars.compile(tpl_group_task_block);
                    var tpl_group_task_block_html = tpl_group_task_block_template(group_block_data);
                    var group_block_info = {
                        'data': group_block_data,
                        'html': tpl_group_task_block_html
                    };
                    blocks.push(group_block_info);

                    if (reqCnt >= groupsCollection.content.length)
                    {
                        $("#pieChartProcessing").hide();
                        $('#task_block').empty();
                        blocks.sort(function(a, b)
                        {
                            return a.data.index - b.data.index
                        });

                        for (var l = 0; l < blocks.length; l++)
                            $('#task_block').append(blocks[l].html);

                        $('.pie_chart_ready').each(function()
                        {
                            $this = $(this);
                            var barColor = $this.css('color') || $this.data('pie-color'),
                                trackColor = $this.data('pie-track-color') || '#DEEBF3',
                                size = parseInt($this.data('pie-size')) || 25;
                            $this.easyPieChart(
                            {
                                barColor: barColor,
                                trackColor: trackColor,
                                scaleColor: false,
                                lineCap: 'butt',
                                lineWidth: parseInt(size / 8.5),
                                animate: false,
                                rotate: -150,
                                size: size,
                                onStep: function(value)
                                {
                                    this.$el.find('span')
                                        .text(~~value);
                                }
                            });
                        });

                        $('.pie_chart_running').each(function()
                        {
                            $this = $(this);
                            var barColor = $this.css('color') || $this.data('pie-color'),
                                trackColor = $this.data('pie-track-color') || '#DEEBF3',
                                size = parseInt($this.data('pie-size')) || 25;
                            $this.easyPieChart(
                            {
                                barColor: barColor,
                                trackColor: trackColor,
                                scaleColor: false,
                                lineCap: 'butt',
                                lineWidth: parseInt(size / 8.5),
                                animate: false,
                                rotate: -150,
                                size: size,
                                onStep: function(value)
                                {
                                    this.$el.find('span')
                                        .text(~~value);
                                }
                            });
                        });

                        $.each(blocks, function(index, val)
                        {
                            if (blocks[index] !== undefined)
                            {
                                $("#done_count_" + index).text(Math.ceil(blocks[index].data.done_task));
                                $("#ready_count_" + index).text(Math.ceil(blocks[index].data.ready_task));
                                $("#running_count_" + index).text(blocks[index].data.running_task);
                            //     jQuery(
                            //     {
                            //         Counter: blocks[index].data.done_task
                            //     }).animate(
                            //     {
                            //         Counter: blocks[index].data.done_task
                            //     },
                            //     {
                            //         duration: 500,
                            //         easing: 'swing',
                            //         step: function()
                            //         {
                            //             $("#done_count_" + index).text(Math.ceil(this.Counter));
                            //         }
                            //     });
                            //     jQuery(
                            //     {
                            //         Counter: blocks[index].data.ready_task
                            //     }).animate(
                            //     {
                            //         Counter: blocks[index].data.ready_task
                            //     },
                            //     {
                            //         duration: 500,
                            //         easing: 'swing',
                            //         step: function()
                            //         {
                            //             $("#ready_count_" + index).text(Math.ceil(this.Counter));
                            //         }
                            //     });

                            //     jQuery(
                            //     {
                            //         Counter: blocks[index].data.running_task
                            //     }).animate(
                            //     {
                            //         Counter: blocks[index].data.running_task
                            //     },
                            //     {
                            //         duration: 500,
                            //         easing: 'swing',
                            //         step: function()
                            //         {
                            //             $("#running_count_" + index).text(Math.ceil(this.Counter));
                            //         }
                            //     });
                             };
                        });

                        // jQuery(
                        // {
                        //     Counter: 0
                        // }).animate(
                        // {
                        //     Counter: blocks[0].data.done_task
                        // },
                        // {
                        //     duration: 500,
                        //     easing: 'swing',
                        //     step: function()
                        //     {
                        //         $("#done_count_0").text(Math.ceil(this.Counter));
                        //     }
                        // });

                        // jQuery(
                        // {
                        //     Counter: 0
                        // }).animate(
                        // {
                        //     Counter: blocks[0].data.ready_task
                        // },
                        // {
                        //     duration: 500,
                        //     easing: 'swing',
                        //     step: function()
                        //     {
                        //         $("#ready_count_0").text(Math.ceil(this.Counter));
                        //     }
                        // });

                        // jQuery(
                        // {
                        //     Counter: 0
                        // }).animate(
                        // {
                        //     Counter: blocks[0].data.running_task
                        // },
                        // {
                        //     duration: 500,
                        //     easing: 'swing',
                        //     step: function()
                        //     {
                        //         $("#running_count_0").text(Math.ceil(this.Counter));
                        //     }
                        // });

                        // if (blocks[1] !== undefined)
                        // {

                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[1].data.done_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#done_count_1").text(Math.ceil(this.Counter));
                        //         }
                        //     });
                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[1].data.ready_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#ready_count_1").text(Math.ceil(this.Counter));
                        //         }
                        //     });

                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[1].data.running_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#running_count_1").text(Math.ceil(this.Counter));
                        //         }
                        //     });
                        // };

                        // if (blocks[2] !== undefined)
                        // {
                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[2].data.done_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#done_count_2").text(Math.ceil(this.Counter));
                        //         }
                        //     });

                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[2].data.ready_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#ready_count_2").text(Math.ceil(this.Counter));
                        //         }
                        //     });

                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[2].data.running_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#running_count_2").text(Math.ceil(this.Counter));
                        //         }
                        //     });
                        // };

                        // if (blocks[3] !== undefined)
                        // {
                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[3].data.done_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#done_count_3").text(Math.ceil(this.Counter));
                        //         }
                        //     });

                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[3].data.ready_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#ready_count_3").text(Math.ceil(this.Counter));
                        //         }
                        //     });

                        //     jQuery(
                        //     {
                        //         Counter: 0
                        //     }).animate(
                        //     {
                        //         Counter: blocks[3].data.running_task
                        //     },
                        //     {
                        //         duration: 500,
                        //         easing: 'swing',
                        //         step: function()
                        //         {
                        //             $("#running_count_3").text(Math.ceil(this.Counter));
                        //         }
                        //     });
                        // };
                    }
                }
            });
        });
    }

    AD.bootstrap = function()
    {
        $("body").addClass("loading");
        i18n.init(function(t)
        {
            $('[data-i18n]').i18n();
        });

        // User log permission
        if ($.login.user.checkRole('UserLogViewer'))
        {
            $("#userlog").removeAttr("style")
        }
        else
        {
            $("#userlog").attr("style", "display:none;");
        };

        var boxes = [
        {
            // Devices
            el: $("#status-grid"),
            'index': 'status0',
            'color': '#95a5a6',
            'desc': 'Devices',
            'fa-icon': 'fa-hdd-o',
            'url': '#ajax/device_list.html'
        },
        {
            // Tasks
            el: $("#status-grid"),
            'index': 'status1',
            'color': '#baa668',
            'desc': 'Tasks',
            'fa-icon': 'fa-tasks',
            'url': '#ajax/task_list.html'
        },
        {
            // Files
            el: $("#status-grid"),
            'index': 'status2',
            'color': '#3498db',
            'desc': 'TotalFiles',
            'fa-icon': 'fa-windows',
            'url': '#ajax/app_list.html'
        },
        {
            // Users
            el: $("#status-grid"),
            'index': 'status3',
            'color': '#f1c40f',
            'desc': 'OnlineUsers',
            'fa-icon': 'fa-group',
            'url': '#ajax/user_list.html'
        }];

        var StatusBox = Backbone.View.extend(
        {
            initialize: function(options)
            {
                this.render(options);
            },
            render: function(options)
            {
                var tpl_html =
                    '<div id="" class="col-xs-12 col-sm-6 col-md-3 col-lg-3" style="height: 110px; margin-bottom: 10px;">' //min-width: 230px;
                    + '<div id="{{id}}" class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="' + 'background-color: {{color}};' + '">' + '<div class="row" style="' + 'height: 80px;' + '">' + '<div class="col-xs-8 col-sm-7 col-md-7 col-lg-7" style="' + 'color: white;' + 'height: 80px;' + '">' + '<span id="{{index}}" style="font-size: 52px;line-height: 52px;">' + '0' + '</span>' + '<span id="plus-{{index}}" style="font-size: 52px;line-height: 52px;margin-left: -4px;">' + '</span>' + '<br />' + '<span data-i18n="ns:Dashboard.' + '{{desc}}"' + '>' + '{{desc}}' + '</span>' + '</div>' + '<div class="col-xs-4 col-sm-5 col-md-5 col-lg-5" style="' + 'text-align: center;' + 'vertical-align: middle;' + 'line-height: 85px;' + 'font-size: 65px;' + 'color: rgba(0,0,0,0.4);' + '">' + '<i id="icon-{{index}}" class="fa {{fa-icon}}"></i>' + '</div>' + '</div>' + '<div class="row" style="' + 'text-align: center;' + 'vertical-align: middle;' + 'background-color: rgba(96,96,96,0.3);' + 'height: 30px;' + 'line-height: 30px;' + 'color: white;' + '"><a id="link-{{index}}" href="{{url}}" style="color: white;" data-i18n="ns:Message.MoreInfo">More Info <i class="fa fa-chevron-circle-down"></i></a>' + '</div>' + '</div>' + '</div>';

                var template = Handlebars.compile(tpl_html);
                var html = template(options);
                this.$el.append(html);
            }
        });

        for (var i = 0; i < boxes.length; i++) {
            new StatusBox(boxes[i]);
        }

        $.ajax(
        {
            type: 'GET',
            url: $.config.server_rest_url + '/devices?isCount=true',
            dataType: 'json',
            timeout: 20000,
            error: function(data, status, error)
            {
                console.log("[Dashboard] Got devices info failed. => " + data.responseText);
            },
            success: function(status)
            {
                $("#status0").text(status.amount);
                // jQuery({ Counter: 0 }).animate({ Counter: status.amount }, {
                //     duration: 500,
                //     easing: 'swing',
                //     step: function () {
                //       $("#status0").text(Math.ceil(this.Counter));
                //     }
                // });
            }
        });

        $.ajax(
        {
            type: 'GET',
            url: $.config.server_rest_url + '/tasks?isCount=true',
            dataType: 'json',
            timeout: 20000,
            error: function(data, status, error)
            {
                console.log("[Dashboard] Got tasks info failed. => " + data.responseText);
            },
            success: function(status)
            {
                $("#status1").text(status.amount);
                // jQuery({ Counter: 0 }).animate({ Counter: status.amount }, {
                //     duration: 300,
                //     easing: 'swing',
                //     step: function () {
                //       $("#status1").text(Math.ceil(this.Counter));
                //     }
                // });
            }
        });

        $.ajax(
        {
            type: 'GET',
            url: $.config.server_rest_url + '/windowsAppFiles?isCount=true',
            dataType: 'json',
            timeout: 20000,
            error: function(data, status, error)
            {
                console.log("[Dashboard] Got windows apps info failed. => " + data.responseText);
            },
            success: function(windowsApps)
            {
                $.ajax(
                {
                    type: 'GET',
                    url: $.config.server_rest_url + '/androidAppFiles?isCount=true',
                    dataType: 'json',
                    timeout: 20000,
                    error: function(data, status, error)
                    {
                        console.log("[Dashboard] Got android apps info failed. => " + data.responseText);
                    },
                    success: function(androidApps)
                    {
                        $.ajax(
                        {
                            type: 'GET',
                            url: $.config.server_rest_url + '/firmwareFiles?isCount=true',
                            dataType: 'json',
                            timeout: 20000,
                            error: function(data, status, error)
                            {
                                console.log("[Dashboard] Got firmware apps info failed. => " + data.responseText);
                            },
                            success: function(firmwareFiles)
                            {
                                $.ajax(
                                {
                                    type: 'GET',
                                    url: $.config.server_rest_url + '/omaAgentFiles?isCount=true',
                                    dataType: 'json',
                                    timeout: 20000,
                                    error: function(data, status, error)
                                    {
                                        console.log("[Dashboard] Got agent files info failed. => " + data.responseText);
                                    },
                                    success: function(omaAgentFiles)
                                    {
                                        var appCnt = windowsApps.amount + androidApps.amount + firmwareFiles.amount + omaAgentFiles.amount;
                                        $("#status2").text(appCnt);
                                        // jQuery({ Counter: 0 }).animate({ Counter: appCnt }, {
                                        //     duration: 300,
                                        //     easing: 'swing',
                                        //     step: function () {
                                        //       $("#status2").text(Math.ceil(this.Counter));
                                        //     }
                                        // });
                                    }
                                });
                            }
                        });
                    }
                });
            }
        });

        $.ajax(
        {
            type: 'GET',
            url: $.config.server_rest_url + '/onlineUserCount',
            dataType: 'json',
            timeout: 20000,
            error: function(data, status, error)
            {
                console.log("[Dashboard] Got users info failed. => " + data.responseText);
            },
            success: function(status)
            {
                $("#status3").text(status.amount);
                // jQuery({ Counter: 0 }).animate({ Counter: status.amount }, {
                //     duration: 300,
                //     easing: 'swing',
                //     step: function () {
                //       $("#status3").text(Math.ceil(this.Counter));
                //     }
                // });
            }
        });

        setTimeout(getDeviceGroupData, 0);

        AD.app = new MyRouter(
        {
            "AD": AD
        });

        if (!Backbone.History.started)
        {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
        $("body").removeClass("loading");
    }

    $[AD.page] = AD;

    var getDeviceGroupData = function()
    {
        $.ajax(
        {
            url: $.config.server_rest_url + '/deviceGroups',
            //url: $.config.server_rest_url+'/deviceGroupDetails?pageSort=taskCount,DESC',
            dataType: 'JSON',
            success: function(groupsCollection)
            {
                var pSize = 4;
                var totalPages = groupsCollection.pageMetaData.amount / pSize;
                if (groupsCollection.pageMetaData.amount % pSize != 0)
                    totalPages += 1;
                $('#pagination-demo').twbsPagination(
                {
                    totalPages: totalPages,
                    visiblePages: 4,
                    onPageClick: function(event, page)
                    {
                        currentDeiveGroupPageNumber = page;
                        //console.log("Device Group 設定頁數=[" + currentDeiveGroupPageNumber+"]");
                        $("#pieChartProcessing").show();
                        $.ajax(
                        {
                            url: $.config.server_rest_url + '/deviceGroups?pageNumber=' + page + '&pageSize=' + pSize + '&pageSort=name%2Casc',
                            dataType: 'JSON',
                            success: function(groupsCollection)
                            {
                                showGroupTaskBlock(groupsCollection);
                            }
                        });
                    }
                });
            }
        });
    }

    var fnRefreshDataForDashboard = function()
    {
        if ($('#status0').length === 0) return;

        console.log("[Data Refresh] Dashboard data refreshed.");
        $.ajax(
        {
            url: $.config.server_rest_url + '/deviceGroups?pageNumber=' + currentDeiveGroupPageNumber + '&pageSize=4&pageSort=name%2Casc',
            dataType: 'JSON',
            success: function(groupsCollection)
            {
                showGroupTaskBlock(groupsCollection);
            }
        });
    }

    if (oRefreshData.oDashboard.bIsEnable === true && oRefreshData.oDashboard.oTimerId === null)
    {
        oRefreshData.oDashboard.oTimerId = window.setInterval(fnRefreshDataForDashboard, oRefreshData.oDashboard.iInterval);
    }
    console.log("[XXXXXXXXXXXXXXXXXXXXXX] #breadCrumb="+$('#breadCrumb > div > h1 > font:first').text());
})(jQuery)
