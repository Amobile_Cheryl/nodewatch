(function($) {
	var AD = {};
    AD.page = "ajax/devicetype_edit.html";

	var reportSettingListRender = function(id){
		
		$.getJSON($.config.server_rest_url+'/deviceTypes/'+id,function(data){
			$("#deviceTypeName").text(data.name);
			$("#remark").val(data.remark);
			$("#reportInterval").val(data.reportInterval);
			var reportItemList = new Array();
			var reportSettingList = new Array();
			for(var i in data.reportSettingList){
				//console.log(data.reportSettingList[i].name);
				var node = data.reportSettingList[i];
				/*
				var reportNode = {};
				reportNode.isReport = node.isReport;
				reportNode.name = node.name;
				reportItemList.push(reportNode);
				*/
				//
				if(node.isReport){
					reportItemList.push(node.name);
				}
				var obj = {
					value: node.name,
					label: node.name
				};
				reportSettingList.push(obj);
			}

			$('.multiselect').multiselect({
				enableFiltering: true,
				includeSelectAllOption: true,
				maxHeight: $('body').height()-510,
				// buttonWidth:'820ptx',
				numberDisplayed: 0
			});

			$('#get-selected').on('click', function() {
				var values = [];

				$('option:selected', $('.multiselect')).each(function() {
					values.push($(this).val());
				});
			});
			$("#reportSettingList").multiselect('dataprovider', reportSettingList);
			$("#reportSettingList").multiselect('select', reportItemList);
			
			// Css
			$("#reportSettingList +div button").css("height","46px");
		});
	};
	
	AD.bootstrap = function() {
		// Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
		
        // Translate Input of the placeholder
        translatePlaceholder();

		$("body").removeClass("loading");
		var id = window.location.hash.replace(/^#ajax\/devicetype_edit.html\/(\w+)$/, "$1");
	
		$("#btn_back").click(function(event){        
            event.preventDefault();        
            window.location.href = "#ajax/devicetype_list.html";        
        });
		
		$('#btn_save').click(function(event){
			event.preventDefault();   
			var reportItemList = [];
			var selector = "#wizard-1 input[type='checkbox']";
			$.each($(selector),function(index,node){
				//reportItemList.push(node.value);
				console.log(node.value);
				if(node.value !== "multiselect-all"){
					var reportNode = {};
					reportNode.isReport = node.checked;
					reportNode.name = node.value;
					reportItemList.push(reportNode);
				}
			});
			var data = "{\""
			+$("#wizard-1 input:not([name='multiselect'])").serialize().replace(/&/g,'","').replace(/=/g,'":"')+"\""
			+",\"remark\":"+JSON.stringify($("#remark").val())
			+",\"reportSettingNameList\":"+JSON.stringify(reportItemList)
			+"\}";
			//console.log("data:"+data);
			if(id != null) {
				$.ajax({
					url: $.config.server_rest_url+'/deviceTypes/'+id,
					type: 'POST',
					dataType: 'json',
					contentType: 'application/json',
					crossDomain: true,
                    headers: {
                        "X-HTTP-Method-Override": "PUT"
                    },					
					data: data,
					async:false
				}).done(function(data){
	                $.smallBox({
	                    title: i18n.t("ns:Message.Device.DeviceTypeEdit") + i18n.t("ns:Message.Device.EditSuccess"),
	                    content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Device.EditDeviceTypeID") + " &nbsp;" + id + "</i>",
	                    // title: "[Device Type edit] Edited successfully",
	                    // content: "<i class='fa fa-pencil-square-o'></i> <i>Edited Group ID: &nbsp;" + id + "</i>",
	                    color: "#648BB2",
	                    iconSmall: "fa fa-pencil-square-o fa-2x fadeInRight animated",
	                    timeout: 5000
	                });					
					console.log("success");
					window.location.href = '#ajax/devicetype_list.html';
				}).fail(function(xhr, textStatus, errorThrown){
	                $.smallBox({
	                	// title: i18n.t("ns:Message.Device.DeviceTypeEdit") + i18n.t("ns:Message.Device.EditSuccess"),
	                 	// content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Device.EditDeviceTypeID") + " &nbsp;" + id + "</i>",
	                    title: i18n.t("ns:Message.Device.DeviceTypeEdit") + i18n.t("ns:Message.Device.EditFailed"),
	                    content: "<i class='fa fa-pencil-square-o'></i> <i>"+ i18n.t("ns:Message.Device.EditDeviceTypeID") + " &nbsp;" + id + "</i>",
	                    // title: "[Device Type edit] Edited failed",
	                    // content: "<i class='fa fa-pencil-square-o'></i> <i>Edited Device Type ID: &nbsp;" + id + "</i>",
	                    color: "#B36464",
	                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
	                    timeout: 5000
	                });        					
					var cause = JSON.parse(xhr.responseText);
					console.log("failure: "+cause.error);
					// $.smallBox({
					// 	title: "[Device Type Edit] Data failed",
					// 	content: "<i class='fa fa-pencil-square-o'></i> <i>"+cause.error+"</i>",
					// 	color: "#B36464",
					// 	iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
					// 	timeout: 9000
					// });
				}).always(function(){
					//
				});
			}
		});
		
		reportSettingListRender(id);
	};
	
	$[AD.page] = AD;

	// Translate Input of the placeholder
    function translatePlaceholder () {
        $("#remark").attr("placeholder", i18n.t("ns:DeviceType.Message.Remark"));
        $("#reportInterval").attr("placeholder", i18n.t("ns:DeviceType.Message.ReportInterval"));
    }

})(jQuery);