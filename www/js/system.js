(function($)
{
    $.system = {};
    $.system.header = {};

    //user model
    $.system.header.Model = Backbone.Model.extend(
    {
        //urlRoot: 'api/order/',
        urlRoot: $.system.header.url,
        idAttribute: "ID"
    });
    /* model end */

    /* collection starts */
    $.system.header.Collection = Backbone.Collection.extend(
    {
        url: $.system.header.url,
        model: $.system.header.Model
    });
    /* collection ends */

    $.system.menu = {};
    $.system.menu.url = $.config.server_url + '/menu';

    /* model starts */
    $.system.menu.MenuModel = Backbone.Model.extend(
    {
        //urlRoot: 'api/order/',
        urlRoot: $.system.menu.url,
        idAttribute: "ID"
    });
    /* model end */

    /* collection starts */
    $.system.menu.MenuCollection = Backbone.Collection.extend(
    {
        url: $.system.menu.url,
        model: $.system.menu.MenuModel
    });
    /* collection ends */

    function matchInArray(src_keyword, array)
    {
        // console.log('src_keyword is '+src_keyword);
        // console.log('array is '+array);
        var search = new RegExp(src_keyword, "gi");
        if (_.isUndefined(array))
        {
            return 0;
        }
        else
        {
            return $.map(array, function(value)
            {
                // console.log('value is '+value);
                if (value.match(search)) return value;
                return 0;
            });
        }
    };

    $.system.MenuView = Backbone.View.extend(
    {
        template: Handlebars.compile($('#tpl_menu').html()),
        render: function()
        {

            var json = $.config.menu;
            // json.forEach(function(element){
            // console.log(element.Name);
            // }) ;
            json.pop();

            /**/
            //2015/02/01 Kenny add this code, Not yet finish
            if (_.isUndefined($.login))
            {
                window.location.href = "login.html";
            }

            var pathname = $.login.location.pathname;
            console.log("pathname="+pathname);

            //找出目前所選的頁面是那一頁
            for (var i = 0; i < json.length; i++)
            {
                if (json[i].Url != null && pathname.match(json[i].Url))
                {
                    json[i].Active = true;
                    break;
                }
            };

            // 把這頁排成樹狀格式
            var menu = {}; // root
            var parents = [];
            parents.push(menu); // list of parent
            var level = 0;

            for (var i = 0; i < json.length; i++)
            {
                // m is the menuitem
                var m = json[i];

                // console.log('m is '+m.Name);
                // console.log($.config.RolePermission);
                // console.log(matchInArray(m.Name,$.config.RolePermission));
                // var match=matchInArray(m.Name,$.config.RolePermission);
                // console.log(match);
                // console.log(match.length);
                // if (match.length==0) {
                //     console.log(m.Name+' is not in the role.');
                // continue;
                // }
                // 目前的mi不是這一階的，是下一階層的
                if (m.Level > level)
                {
                    //seek and get array of menu item
                    var mi = parents[parents.length - 1].menuitem;
                    // console.log('mi is '+mi);
                    // console.log('mi\'s name is '+mi.Name);
                    //seek add add to path
                    parents.push(mi[mi.length - 1]);
                }
                // 目前的mi不是這一階的，是上一階層的
                else if (m.Level < level)
                {
                    for (var j = 0; j < level - m.Level; j++)
                        parents.pop();
                }
                // 目前的mi是這一階的
                // do nothing
                ;

                // seek
                var target = parents[parents.length - 1];

                if (typeof target.menuitem === 'undefined')
                    target.menuitem = [];

                target.menuitem.push(m);
                // console.log('target.menuitem is '+target.menuitem.length)

                level = m.Level;

                if (m.Active === true)
                {
                    for (var j = 0; j < parents.length; j++)
                    {
                        parents[j].Active = true;
                    }
                }

            }
            // var data = {
            //     UserName : this.model.get("Name")
            // };

            // alert();
            // alert(this.$target.html());
            //console.log($('#tpl_menu').html());
            // console.log('menu.menuitem[0] is ');
            // console.log(menu.menuitem[0]);
            // console.log(menu.menuitem[0].menuitem.length);

            /* Menu item check */
            // console.log('length is '+menu.menuitem[0].menuitem.length);
            // console.log(menu.menuitem[0].menuitem);

            var userPermissionClassValue = (_.isUndefined($.login.user.permissionClass)) ? 0 : $.login.user.permissionClass.value;
            console.log("[Menu] userPermissionClassValue="+userPermissionClassValue);
            console.log("[Menu] $.common.PermissionClass.SystemOwner.value="+$.common.PermissionClass.SystemOwner.value);
            if (userPermissionClassValue < $.common.PermissionClass.SuperUserRole.value) //from systemowner to SuperUserRole
            {
                console.log('It\'s not a SuperUserRole or SystemOwner');
                for (var i = menu.menuitem[0].menuitem.length - 1; i >= 0; i--)
                {
                    console.log('Checking...');
                    console.log(menu.menuitem[0].menuitem[i].Permission);
                    // Level 1&2 exist simultaneously
                    if (menu.menuitem[0].menuitem[i].Level == 1 && menu.menuitem[0].menuitem[i].Url == null)
                    {
                        var menuitem = menu.menuitem[0].menuitem[i].menuitem;
                        // console.log(menu.menuitem[0].menuitem[i].Permission);
                        for (var j = menuitem.length - 1; j >= 0; j--)
                        {
                            console.log('Checking Childs...');
                            console.log(menuitem[j].Permission);
                            // var match=matchInArray(menuitem[j].Permission,$.login.user.get("roleNameList"));
                            var match = $.login.user.checkRole(menuitem[j].Permission);
                            if (match === false)
                            {
                                console.log(menuitem[j].Permission + ' is no permission.');
                                menuitem.splice(j, 1);
                            }
                        }
                        if (menuitem.length === 0)
                            menu.menuitem[0].menuitem.splice(i, 1);
                    }
                    // Only Level 1       
                    else
                    {
                        // var match=matchInArray(menu.menuitem[0].menuitem[i].Permission,$.login.user.get("roleNameList"));
                        var match = $.login.user.checkRole(menu.menuitem[0].menuitem[i].Permission);
                        if (match === false)
                        {
                            console.log(menu.menuitem[0].menuitem[i].Name + ' is no permission.');
                            menu.menuitem[0].menuitem.splice(i, 1);
                        }
                    }
                }
                console.log('Checking Over!!!');
            }
            else{
                console.log('It\'s a SuperUserRole or SystemOwner');
            }

            menu.menuitem[0].Name = $.login.user.userName;
            var portrait = $.login.user.portrait || $.common.defaultPortrait;
            menu.menuitem[0].Portrait = portrait;
            //console.log(menu.menuitem[0].menuitem);;
            this.$el.html(this.template(menu.menuitem[0]));
        }
    });

    $.system.bootstrap = function()
    {

        /*
    var headerColl = new $.system.header.Collection();

    headerColl.fetch({
      success:function(collection, response, options) {
        var json=collection.toJSON();
        var unactive_user=json[0]['unactive_user']
        $("#msgbox > ul > li > a > span").html(unactive_user);
      },
      error:function(collection, response, options) {

      }
    });
    */
        // var header = new $.system.HeaderView({
        //   el: $(".navbar").first(),
        //   model: $.system.
        // })

        //are attached to the view as this.options for future reference. There are several special options that, if passed, will be attached directly to the view: model, collection, el, id, className, tagName and attributes.
        // var nav = new $.system.NavView({
        //   el: $(".navbar").first(),
        //   model: $.login.user
        // });

        // $.system.nav = nav;

        // nav.render();

        // alert("hello!");
        var menu = new $.system.MenuView(
        {
            el: $("#left-panel"),
            collection: new $.system.menu.MenuCollection
        });
        menu.render();
    };

    $.system.bootstrap();
})(jQuery);
