(function($) {
    // Move variable AD and AD.page outside of function
    var AD = {};
    AD.page = "ajax/device_import.html";
    // Add adReload function to run this script when user click this .html every time.
    function adReload() {
        AD.collections = {};
        AD.buttons = {};
       
        /* define namespace ends */

        var hash = window.location.hash;
        var url;
        AD.isAdd = false;

        // Display the list of group members
        /* Group Device */
		var urlPathArray = hash.split("\/");
        AD.title = "ns:Menu.Devices";

        AD.csv = {};
        AD.csv.forms = [
            //form
            {
                // title: 'Choose File',
                title: i18n.t("ns:Device.ChooseFile"),
                // property: 'name',
                default_value: "",
                view: function() {
					//console.log("[AddCSV] Prepare csv fileupload component.");
                    //宣告upload button
                    var csvButton = $('<button/>')
                        //增加button的class讓呈現方式改變
                        .addClass('btn btn-primary')
                        //把upload button 的disable prop設為true目的是讓他無法選取
                        .prop('disabled', true)
                        // .text('Processing...')
                        .text(i18n.t("ns:Message.Processing"))
                        //設定 upload button 的click event
                        .on('click', function(event) {
                            event.preventDefault();
                            $("#uploadResultDiv").css("display","none")
                                                .removeClass()
                                                .html("");
                            var $this = $(this),
                                //取值, 即檔案
                                data = $this.data();

                            $this
                                .off('click')
                                .text(i18n.t("ns:Device.Abort"))
                                .on('click', function() {
                                    //把upload button移除
                                    $this.remove();
                                    data.abort();
                                });
                            data.submit().always(function() {
                                $this.remove();
								//$("#uploadResult iframe")[0].src = window.location.origin+"/fn/ajax/cors/result.html?test"
                            });
                        });

                    //下面是利用handlebars來把Add File的按鈕裝上template
                    // var uploadButton = $('<button/>');
                    var chooseFile = {
                        // title: "Add CSV..."
                        title: i18n.t("ns:Device.AddCSV")
                    };
                    var chooseButton = $('#btnChooseFile').html();
                    var template = Handlebars.compile(chooseButton);
                    var html = template(chooseFile); //String

                    /*
                    $.ajax({
                        url: $.config.server_util_url +'/import',
                        type: 'POST',
                        async: false,
                        data: {
                            property: "Attachmentcsv"
                        },
                        success: function(data) {
                            // alert("i am success!");
                        }
                    });
                    */

                    var fd = new FormData();
                    fd.append("deviceTypeName", "G0550");
                    fd.append("deviceGroupName", "alvin");
                    fd.append("isCheckFile", "true");

                    $html = $(html); //according to the html string, it will generate a "jquery" object that copy the string from var html

					$html.find("#fileupload").fileupload({
                        url: $.config.server_host + '/RESTful/devices',
                        //url: 'http://140.92.60.113:8080/AMDM' +'/import?type=csv',
                        //dataType: 'json',
                        formData: fd,
                        //是否選擇完檔案立即上傳
                        autoUpload: false,
                        //可以接受的格式, 可直接用|另外增加類型
                        acceptFileTypes: /(\.|\/)(csv)$/i,
                        //超過會顯示file too large
                        maxFileSize: 5000000, // 5 MB
                        // Enable image resizing, except for Android and Opera,
                        // which actually support image resizing, but fail to
                        // send Blob objects via XHR requests:
                        disableImageResize: /Android(?!.*Chrome)|Opera/
                            .test(window.navigator.userAgent),
                        previewMaxWidth: 100,
                        previewMaxHeight: 100,
                        //是否讓縮圖點擊後另開頁面顯示原圖
                        previewCrop: true,
                        //檔案load之後把檔案名稱顯示出來
                        forceIframeTransport: false,
						xhrFields: {
							withCredentials: true
						}
						//,redirect: window.location.origin+'/fn/ajax/cors/result.html?%s'
						//,initialIframeSrc: "javascript:document.write('<script>document.domain=\""+window.location.origin+"\";</script>')"
                    }).on('fileuploadadd', function(e, data) {
						$("#files").empty();
                        data.context = $('<div/>').appendTo('#files');
                        $.each(data.files, function(index, file) {
                            var node = $('<p/>')
                                .append($('<span/>').text(file.name));
                            if (!index) {
                                node
                                    .append('<br>')
                                    .append(csvButton.clone(true).data(data));
                            }
                            node.appendTo(data.context);
                        });
                    }).on('fileuploadprocessalways', function(e, data) {
                        var index = data.index,
                            file = data.files[index],
                            node = $(data.context.children()[index]);
                        if (file.preview) {
                            node
                                .prepend('<br>')
                                .prepend(file.preview);
                        }
                        //如果檔案error, 把錯誤的資訊顯示出來
                        if (file.error) {
                            node
                                .append('<br>')
                                .append($('<span class="text-danger"/>').text(file.error));
                        }
                        //產生
                        if (index + 1 === data.files.length) {
                            data.context.find('button')
                                .text(i18n.t("ns:Device.Upload"))
                                .prop('disabled', !!data.files.error);
                        }
                    }).on('fileuploadprogressall', function(e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress .progress-bar').css(
                            'width',
                            progress + '%'
                        );
                    }).on('fileuploaddone', function(e, data) {
                        if (!_.isUndefined(data.result.result)) {
                            var contentMessage  = " ",
                                smallBoxMessage = " ";
                            if (data.result.result) {                                
                                if (!_.isUndefined(data.result.dupicateDevice) && !_.isUndefined(data.result.invaildDevice)) {
                                    if (data.result.dupicateDevice.length == 0 && data.result.invaildDevice.length == 0) {
                                        smallBoxMessage += i18n.t("ns:Message.Device.AddSuccess");
                                    } else {                                        
                                        if (data.result.dupicateDevice.length != 0) {
                                            if ($.config.debugger) {
                                                _.each(data.result.dupicateDevice, 
                                                    function (value, key, list) {
                                                        contentMessage += value + "<br>";
                                                    });                                                
                                            } else {
                                                contentMessage += i18n.t("ns:Message.Device.MoreDevicesAlreadyUse")+"\n";
                                            };         
                                            smallBoxMessage += i18n.t("ns:Message.Device.MoreDevicesAlreadyUse")+"\n";                               
                                            console.log("[Device Import] Added successfully. " + JSON.stringify(data.result.dupicateDevice));
                                        };
                                        if (data.result.invaildDevice.length != 0) {
                                            if ($.config.debugger) {
                                                _.each(data.result.invaildDevice, 
                                                    function (value, key, list) {
                                                        contentMessage += value + "<br>";
                                                    });
                                            } else {
                                                contentMessage += i18n.t("ns:Message.Device.DevicesInvalid")+"\n";
                                            };                                        
                                            smallBoxMessage += i18n.t("ns:Message.Device.DevicesInvalid")+"\n";
                                            console.log("[Device Import] Added successfully. " + JSON.stringify(data.result.invaildDevice));
                                        };
                                        // <DIV> uploadResult
                                        $("#uploadResultDiv").css("display","block")
                                                        .addClass("alert alert-warning")
                                                        .html(contentMessage);
                                    }                                    
                                } 
                                $.smallBox({                            
                                    title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddSuccess"),
                                    content: "<i class='fa fa-plus'></i> <i>" + smallBoxMessage + "</i>",
                                    color: "#648BB2",
                                    iconSmall: "fa fa-plus fa-2x fadeInRight animated",
                                    timeout: 5000
                                });                                 
                            } else {
                                if (!_.isUndefined(data.result.invaildDevice)){
                                    if (data.result.invaildDevice.length != 0) {
                                        if ($.config.debugger) {
                                            _.each(data.result.invaildDevice, 
                                                function (value, key, list) {
                                                    contentMessage += value + "<br>";
                                                });                                            
                                        } else {
                                            contentMessage += i18n.t("ns:Message.Device.DeviceTypeNotExistOrFormatError");    
                                        };                                        
                                        console.log("[Device Import] Added failed. " + JSON.stringify(data.result.invaildDevice));
                                    } else {
                                        contentMessage += i18n.t("ns:Message.Device.DeviceTypeNotExistOrFormatError");    
                                    };
                                    $("#uploadResultDiv").css("display","block")
                                                        .addClass("alert alert-danger")                                                        
                                                        .html(contentMessage);
                                }
                                $.smallBox({
                                    title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddFailed"),
                                    content: "<i class='fa fa-plus'></i> <i>"+i18n.t("ns:Message.Device.DeviceTypeNotExistOrFormatError")+"</i>",
                                    color: "#B36464",
                                    iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                    timeout: 5000
                                });
                            };
                        };
                        // $.smallBox({                            
                        //     title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddSuccess"),
                        //     content: "<i class='fa fa-plus'></i> <i>" + i18n.t("ns:Message.Device.AddSuccess") + "</i>",
                        //     color: "#648BB2",
                        //     iconSmall: "fa fa-plus fa-2x fadeInRight animated",
                        //     timeout: 5000
                        // });                                    
                        $(data.context.children().children()[2]).text(i18n.t("ns:Device.Done"));
                        $("#wid-id-1 .smart-form footer button").text(i18n.t("ns:Device.Done"));
                        /*
                        $.each(data.result.files, function(index, file) {
                            if (file.url) {
                                var link = $('<a>')
                                    .attr('target', '_blank')
                                    .prop('href', file.url);
                                $(data.context.children()[index])
                                    .wrap(link);
                            } else if (file.error) {
                                var error = $('<span class="text-danger"/>').text(file.error);
                                $(data.context.children()[index])
                                    .append('<br>')
                                    .append(error);
                            }
                        });
                        */
                    }).on('fileuploadfail', function(e, data) {
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.DeviceAdd") + i18n.t("ns:Message.Device.AddFailed"),
                            content: "<i class='fa fa-plus'></i> <i>"+ i18n.t("ns:Message.Device.AddFailed") + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 6000
                        });
                        console.log("[Device Import] File upload failed.");
                        // <DIV> uploadResult

                        // $.each(data.files, function(index, file) {
                        //     var error = $('<span class="text-danger"/>').text('File upload failed.');
                        //     $(data.context.children()[index])
                        //         .append('<br>')
                        //         .append(error);
                        // });
						$("#wid-id-1 .smart-form footer button").text("Close");
                    }).prop('disabled', !$.support.fileInput)
                        .parent().addClass($.support.fileInput ? undefined : 'disabled');

					return $html;
                },
                value: function() {
                    var name;
                    var path;
                    $.ajax({
                        url: $.config.server_util_url + '/import',
                        //url: 'http://140.92.60.113:8080/AMDM' +'/import',
                        type: 'POST',
                        async: false,
                        data: {
                            type: "csv",
                            mode: "doSave"
                        },
                        success: function(data) {
                            path = data.fileServerPath;
                            name = data.name;
                        }
                    });
                    var ret = {
                        "name": name,
                        "fileServerPath": path
                    };
                    return ret;
                }

            }
        ];

        /* model starts */

        AD.csv.Model = Backbone.Model.extend({
            urlRoot: $.config.server_util_url + '/import',
            idAttribute: "id"
        });


        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.csv.Model
        });

        /* collection ends */
		$("#widget-grid .cctech-fv").empty();

		var m = new AD.csv.Model();

		var fv = new Backbone.FormView({
			el: $("#widget-grid .cctech-fv"),
			Name: "ns:Device.Actions.Add",
			model: new Backbone.Model(),
			forms: AD.csv.forms,
			button: [{
				"type": "button",
				"cls": "btn btn-default",
				"fn": "window.history.back()",
				// "text": "Cancel"
                "text": i18n.t("ns:Device.Cancel")
			}]
		});

		fv.$el.unbind();
		fv.$el.on("click", '[type="submit"]', function(event) {
			var forms = AD.forms;
			AD.forms = AD.csv.forms;
			submit(event, m);
			AD.forms = forms;
		});
    };


    /* define bootstrap starts */
    AD.bootstrap = function() {

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        adReload();

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;

})(jQuery);
