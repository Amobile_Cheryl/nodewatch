(function($) {
    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');

    var AD = {};
    AD.page = "ajax/group_detail.html";
    var initialStatus = false;

    var taskGrid = null;

    function s1(id) {
        subscribeForGroup(id);
        /* define date format*/
        df = new DateFormat();
        df.setFormat('yyyy-MMM-dd HH:mm:ss');
        /*                  */

        /* define namespace starts */
        var hash = window.location.hash;

        AD.collections = {};
        var url = $.config.server_rest_url + '/deviceGroups/' + id + '/devices';
        var idAttribute = AD.idAttribute = "id";
        AD.title = "ns:Group.Title.Members";
        AD.isAdd = false;

        AD.validate = function() {
            return true;
        };

        AD.columns = [{
                title: 'ns:Device.Title.Status',
                filterable: false,
                sortable: false,
                width: '75px',
                callback: function(o) {
                    var isLock;
                    isLock = '<img src="img/status_unlock.png" style="height: 20px;margin-right: 6px;" />';
                    if (!_.isUndefined(o.isLock)) {
                        if (o.isLock)
                            isLock = o.isLock ? '<img src="img/status_lock.png" style="height:20px; margin-right: 6px;" />' : '<img src="img/status_unlock.png" style="height:25px;" />';
                    }

                    var isOnlineStatus = '<img src="img/status_offline.png" style="height:20px; margin-right: 0px;" />';
                    if (!_.isUndefined(o.isOnline)) {
                        if (o.isOnline)
                            isOnlineStatus = '<img src="img/status_online.png" style="height:20px; margin-right: 0px;" />';
                    }
                    //var isJailbreak = (_.isUndefined(o.isJailbreak)) ? "" : "<i class=\"fa fa-bug\" style=\"color:crimson;\" title=\""+i18n.t("ns:Message.Device.Jailbreak")+"\"></i>";
                    //return isLock+isJailbreak;
                    //return isJailbreak;
                    return isLock + "&nbsp;" + isOnlineStatus;
                }
            }, {
                title: 'ns:Device.Title.Label',
                property: 'label',
                filterable: false,
                sortable: false,
                width: '200px',
                callback: function(o) {
                    if (o.provisionStatus === "Provisioned") {
                        return '<a href="#ajax/device_detail.html/' + o.id + '" title="Check the device detail">' + o.label + '</a>';
                    } else {
                        return o.label;
                    }
                }
            }, {
                title: 'IMEI & MAC',
                property: 'name',
                filterable: false,
                width: '200px',
                sortable: false
            },
            /*{
                title: 'ns:Device.Title.OperatingSystem',
                //property: 'operatingSystemName',
                filterable: false,
                searchName: 'operatingSystemName',
                // searchDom : function(){
                //  var options = '<option value selected="selected">'+i18n.t("ns:DataTables.Search")+' '+i18n.t("ns:Device.Title.OperatingSystem")+'</option>'
                //      +'<option>Android</option>'
                //      +'<option>Windows</option>';
                //  return '<select class="form-control" name="deviceType" placeholder="'+i18n.t("ns:DataTables.Search")+' '+i18n.t("ns:Device.Title.OperatingSystem")+'">'+options+'</select>';
                // },
                sortable: false,
                callback: function(o) {
                    var os = (typeof(o.operatingSystemName) === "undefined") ? '' : o.operatingSystemName;
                    var displayHtml = '';
                    if(os==="Android"){
                        displayHtml = '<i class="fa fa-android"></i> Android';
                    }else if(os==="iOS"){
                        displayHtml = '<i class="fa fa-apple"></i> iOS';
                    }else if(os==="Windows"){
                        displayHtml = '<i class="fa fa-windows"></i> Windows';
                    }else{
                        displayHtml = '<i class="fa fa-question"></i> Unknown Operating System';
                    }
                    return displayHtml;
                }
            },*/
            //Column3
            {
                title: 'ns:Device.Title.FirmwareVersion',
                property: 'firmwareVersion',
                filterable: false,
                sortable: true,
            },
            //column4
            {
                title: 'ns:Device.Title.LastReport',
                property: 'latestConnectTime',
                cellClassName: 'latestConnectTime',
                filterable: false,
                sortable: false
            },
            //column5
            /*{
                title: 'ns:Device.Title.Domain',
                property: 'domainId',
                filterable: false,
                sortable: false,
                visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
                callback: function(o) {
                    //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknown' : $.config.Domain[o.domainId] ;
                    return (typeof(o.domainName) === "undefined") ? '' : o.domainName;
                }
            }*/
        ];
        /* define namespace ends */

        AD.buttons = [
            //button
            {
                "sExtends": "text",
                "sButtonText": "<i class='fa fa-edit'></i>&nbsp; " + i18n.translate("ns:Domain.Edit"),
                "sButtonClass": "txt-color-white bg-color-blue",
                "fnClick": function(nButton, oConfig, oFlash) {
                    var hash = window.location.hash;
                    var id = hash.replace(/^#ajax\/group_detail.html\/(\w+)$/, "$1");
                    window.location.href = "#ajax/group_member_list.html/" + id;
                }
            },
        ];

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });

        /* collection ends */
        var gvs = "#group_member .cctech-gv";
        var gv = $(gvs).first();

        if (!MemberGrid) {
            var MemberGrid = new Backbone.GridView({
                el: gv,
                collection: new AD.Collection(),
                columns: AD.columns,
                title: AD.title,
                AD: AD,
                sort: AD.sort,
                buttons: AD.buttons
            });
        }
        MemberGrid.refresh();
        //setupGroupEvent(id);

        i18n.init(function(t) {
            $('[data-i18n]').i18n();
        });
    };

    function setupGroupEvent(id) {
        var eventTable = {};
        eventTable.title = "ns:Group.Title.Events";
        var url = $.config.server_rest_url + '/deviceGroups/' + id + '/deviceEvents';
        eventTable.idAttribute = "id";
        eventTable.buttons = [{
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-plus'></i>&nbsp; " + i18n.translate("ns:Domain.Edit"),
            "sButtonClass": "edit-group-device-list-btn txt-color-white bg-color-blue",
            "fnClick": function(nButton, oConfig, oFlash) {
                var hash = window.location.hash;
                var id = hash.replace(/^#ajax\/group_detail.html\/(\w+)$/, "$1");
                window.location.href = "#ajax/group_event_list.html/" + id;
            }
        }];

        eventTable.sort = {
            "column": "id",
            "sortDir": "desc"
        };
        eventTable.columns = [{
            title: 'Category',
            property: 'category',
            cellClassName: 'category',
            filterable: false,
            sortable: true,
            callback: function(o) {
                return '<i class="fa fa-dot-circle-o"></i> ' + o.category;
            }
        }, {
            title: 'Description',
            property: 'content',
            cellClassName: 'content',
            filterable: false,
            sortable: false,
            callback: function(o) {
                var msg = "";
                if (o.type == "Root" && o.content == "=,1")
                    msg = "Device is rooted";
                else if (o.type == "BatteryLevel") {
                    msg += "Battery level ";
                    if (o.content.split(",")[0] == ">")
                        msg += "is higher than ";
                    else
                        msg += "is lower than ";
                    msg += o.content.split(",")[1];
                }
                return msg;
            }
        }, {
            //title : 'ns:File.Firmware.Title.FileSize',
            title: 'Level',
            property: 'severity',
            cellClassName: 'severity',
            filterable: false,
            sortable: false,
            callback: function(o) {
                var color;
                var des;
                switch (o.severity) {
                    case 5:
                        color = "#ee0000";
                        des = "Critical";
                        break;
                    case 4:
                        color = "#ee7700";
                        des = "Very High";
                        break;
                    case 3:
                        color = "#eeee00";
                        des = "High";
                        break;
                    case 2:
                        color = "#a2a200";
                        des = "Medium";
                        break;
                    case 1:
                        color = "#51a200";
                        des = "Low";
                        break;
                    default:
                        color = "#919191";
                        des = "--";
                }
                return "<i class=\"fa fa-circle\" style=\"color:" + color + ";\"></i> " + des;
            }
        }, {
            title: 'Mail to',
            property: 'email',
            cellClassName: 'email',
            filterable: false,
            sortable: true,
        }];
        /* define namespace ends */

        /* model starts */

        eventTable.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: eventTable.idAttribute
        });

        /* collection starts */
        eventTable.Collection = Backbone.Collection.extend({
            url: url,
            model: eventTable.Model
        });

        /* collection ends */
        var egv = $("#group_event .cctech-gv").first();

        if (!eventGrid) {
            var eventGrid = new Backbone.GridView({
                el: egv,
                collection: new eventTable.Collection(),
                columns: eventTable.columns,
                title: eventTable.title,
                //AD: eventTable,
                sort: eventTable.sort,
                buttons: eventTable.buttons
            });
        } else
            eventGrid.refresh();
    };

    function group_info_init(group) {
        var group_info_html =
            '<div>' +
            '<div class="col-xs-2 col-lg-7">' +
            '<div class="col-xs-2" style="padding-left: 0px; padding-right: 0px; height: 110px; width: 100px; text-align: center; line-height: 110px;">' +
            '<img id="deviceTypeImage" src="{{deviceTypeImage}}" style="max-width: 90px; max-height: 90px;">' +
            '</div>' +
            '<div class="hidden-xs hidden-sm hidden-md col-lg-1" style="margin-left:18px; margin-top:8px; padding: 0px; height:95px; width:20px; border-left:3px solid #ccc;">' +
            '</div>' +
            '<div class="hidden-xs hidden-sm hidden-md col-lg-8" style="padding-left: 0px; padding-right: 0px;height: 110px;">' +
            '<div class="col-md-12" style="font-size:20px; font-weight:bold; padding-top: 22px;">' +
            'Device Type&nbsp;&nbsp;:&nbsp;&nbsp;{{deviceTypeName}}' +
            '</div>' +
            '<div class="col-md-4" style="padding-top: 15px; font-size:18px;">' +
            'Members&nbsp;&nbsp;:&nbsp;&nbsp;{{members}}' +
            '</div>' +
            '<div class="col-md-8" style="padding-top: 15px; font-size:18px;">' +
            'Software Policy&nbsp;&nbsp;:&nbsp;&nbsp;{{softwarePolicy}}' +
            '</div>' +
            '</div>' +
            '</div>' +
            '<div id="am_panel" class="col-xs-10 col-lg-5" style="margin-top: 55px; padding-right: 5px;">' +
            '</div>' +
            '</div>';

        var groupInfo_template = Handlebars.compile(group_info_html);

        var groupInfo_temp_content = {
            deviceTypeImage: "",
            deviceTypeName: group.deviceTypeName,
            operatingSystem: group.operatingSystem,
            softwarePolicy: group.devicePolicyName,
            members: group.deviceIdSet.length
        };
        $("#group_info").append(groupInfo_template(groupInfo_temp_content));

        $.ajax({
            url: $.config.server_rest_url + '/deviceTypes?pageNumber=1&pageSize=100&pageSort=name%2Casc',
            dataType: 'JSON',
            success: function(deviceTypeCollection) {
                deviceTypes = deviceTypeCollection;
                for (var i = 0; i < deviceTypeCollection.content.length; i++) {
                    if (deviceTypeCollection.content[i].name == group.deviceTypeName) {
                        $("#deviceTypeImage").attr("src", deviceTypeCollection.content[i].deviceTypeIconUrl);
                        break;
                    }
                }
            }
        });
    }

    function am_init(id) {
        var am_action_html =
            "<div class=\"col-lg-12\">" +
            "{{#each am_actions}}" +
            "<div class=\"col-xs-1 pull-right\" style=\"min-width:52px;min-height:52px;\">" +
            "<a href={{link}}" + " id=\"{{name}}\" title=\"{{this.label}}\">" +
            "<img src=\"{{icon}}\" onerror=\"imgLoadErrorBigger(this,'{{name}}','{{color}}','{{icon}}');\">" +
            "</a>" +
            "</div>" +
            "{{/each}}" +
            "</div>";

        var amPanel_template = Handlebars.compile(am_action_html);
        var am_temp_content1 = {
            "am_actions": [{
                name: "--", //"wallpaper",
                //icon: "glyphicon-download-alt",
                icon: "fa-picture-o",
                label: "Wallpaper Applying",
                color: "#2E4073",
                link: "javascript:void(0);"
            }, {
                name: "policy",
                //icon: "glyphicon-cog",
                icon: "fa-shield",
                label: "Policy Applying",
                color: "#8C3232",
                link: "#ajax/group_am.html/" + id + "/policy"
            }, {
                name: "deactivate",
                //icon: "glyphicon-pause",
                icon: "fa-pause",
                label: i18n.t("ns:DeviceGroup.Actions.Deactivate"),
                color: "#5320CD",
                link: "#ajax/group_am.html/" + id + "/deactivate"
            }, {
                name: "activate",
                //icon: "glyphicon-play",
                icon: "fa-play",
                label: i18n.t("ns:DeviceGroup.Actions.Activate"),
                color: "#3FB618",
                link: "#ajax/group_am.html/" + id + "/activate"
            }, {
                name: "remove",
                //icon: "glyphicon-remove",
                icon: "fa-trash-o",
                label: i18n.t("ns:DeviceGroup.Actions.Remove"),
                color: "#FF0039",
                link: "#ajax/group_am.html/" + id + "/remove"
            }, {
                name: "install",
                //icon: "glyphicon-download-alt",
                icon: "fa-download",
                label: i18n.t("ns:DeviceGroup.Actions.Install"),
                color: "#2E7BCC",
                link: "#ajax/group_am.html/" + id + "/install"
            }, ]
        };

        var am_temp_content2 = {
            "am_actions": [{
                name: "--", //"wallpaper",
                //icon: "glyphicon-download-alt",
                icon: "fa-picture-o",
                label: "Wallpaper Applying",
                color: "#2E4073",
                link: "javascript:void(0);"
            }, {
                name: "deactivate",
                //icon: "glyphicon-pause",
                icon: "fa-pause",
                label: i18n.t("ns:DeviceGroup.Actions.Deactivate"),
                color: "#5320CD",
                link: "#ajax/group_am.html/" + id + "/deactivate"
            }, {
                name: "remove",
                //icon: "glyphicon-remove",
                icon: "fa-trash-o",
                label: i18n.t("ns:DeviceGroup.Actions.Remove"),
                color: "#FF0039",
                link: "#ajax/group_am.html/" + id + "/remove"
            }, ]
        };

        //$("#am_panel #opt_first_row").empty();
        $("#am_panel").append(amPanel_template(am_temp_content1));
        //$("#am_panel #opt_second_row").append(amPanel_template(am_temp_content2));
        //$("#am_panel .summary_bg").css("height",am_actions.am_actions.length*300/6);
    }

    function s3_init(id) {
        var action_html =
            "<div class=\"appm_property_bg col-lg-12\">" +
            "<br class=\"clear\" />" +
            "</div>";

        var Panel_template = Handlebars.compile(action_html);

        var task_html = "<div id=\"realTimeTask\">" + "<div style=\"text-align:right;\"><span style=\"border:initial;font-size:24px;\"><i class=\"fa fa-paint-brush\"></i>&nbsp;<font data-i18n=\"ns:Group.Clear\"> Clear</font></span></div>" + "<table class=\"table table-striped table-bordered table-hover dataTable\" style=\"clear: both\">" + "<thead>" + "<tr>" + "<th><font data-i18n=\"ns:Group.TaskID\">Task Id</font></th>" + "<th><font data-i18n=\"ns:Group.TaskName\">Task Name</font></th>" + "<th><font data-i18n=\"ns:Group.SubmitTime\">SubmitTime</font></th>" + "<th><font data-i18n=\"ns:Group.Status\">Status</font></th>" + "<th style=\"width: 64px;\"><font data-i18n=\"ns:Group.Action\">Action</font></th>" + "</tr>" + "</thead>" + "<tbody id=\"task-console\">"
            // taskView append
            + "</tbody>" + "</table>" + "</div>";
        //Al20150121
        var task_console = "<% _.each(task, function(taskData) { %>" + "<tr id=\"console-<%= taskData.id %>\">"
            //+"<td><%= taskData.id %></td>"
            + "<td><a href=\"#ajax/group_detail.html/" + id + "/s5/<%= taskData.id %>\"><%= taskData.id %></a></td>" + "<td>" + "<% switch (taskData.job) { case 'installDeviceApp': %>" + "<i class=\"fa fa-download\" style=\"color:#2E7BCC\"></i>" + i18n.t("ns:Group.TaskNames.InstallApp") + "</td>" + "<% break; case 'removeDeviceApp': %>" + "<i class=\"fa fa-trash-o\" style=\"color:#FF0039\"></i>" + i18n.t("ns:Group.TaskNames.RemoveApp") + "</td>" + "<% break; case 'activateDeviceApp': %>" + "<i class=\"fa fa-play\" style=\"color:#3FB618\"></i>" + i18n.t("ns:Group.TaskNames.ActivateApp") + "</td>" + "<% break; case 'deactivateDeviceApp': %>" + "<i class=\"fa fa-pause\" style=\"color:#66AA00\"></i>" + i18n.t("ns:Group.TaskNames.DeactivateApp") + "</td>" + "<% break; case 'upgradeDeviceApp': %>" + "<i class=\"fa fa-plus\" style=\"color:#FFA500\"></i>" + i18n.t("ns:Group.TaskNames.UpgradeApp") + "</td>" + "<% break; case 'firmwareUpgrade': %>" + "<i class=\"fa fa-cog\" style=\"color:#E671B8\"></i>" + i18n.t("ns:Group.TaskNames.FirmwareUpgrade") + "</td>" + "<% break; case 'agent': %>" + "<i class=\"fa-floppy-o\" style=\"color:#5B5B5B\"></i>" + i18n.t("ns:Group.TaskNames.AgentUpgrade") + "</td>" + "<% break; default: %>" + "<% break; } %>"
            // +"<%= taskData.job %></td>"
            + "<td><%= taskData.creationTime %></td>"
            //+"<td><%= taskData.executeStatus %></td>"
            + "<td>" + "<div>" + "<div class=\"executeStatus col col-lg-4 col-md-4 col-sm-4 col-xs-4\">" + "<%= taskData.executeStatus %>" + "/" + "<%= taskData.total %>" + "</div>" + "<div class=\"col col-lg-8 col-md-8 col-sm-8 col-xs-8\">" + "<% if((taskData.total - taskData.executeStatus)===0) { %>" + "<div class=\"progress progress-striped\">" + "<% } else { %>" + "<div class=\"progress progress-striped active\">" + "<% } %>" + "<div class=\"progress-bar bg-color-greenLight\" aria-valuetransitiongoal=\"" + "<%= Math.floor((taskData.executeStatus/taskData.total)*100) %>" + "\" style=\"width: " + "<%= Math.floor((taskData.executeStatus/taskData.total)*100) %>" + "%;\" aria-valuenow=\"" + "<%= Math.floor((taskData.executeStatus/taskData.total)*100) %>" + "\">" + "<%= Math.floor((taskData.executeStatus/taskData.total)*100) %>" + "%</div>" + "</div>" + "</div>" + "</div>" + "</td>" + "<td style=\"font-size:22px;\">" + "<a href=\"#ajax/group_detail.html/" + id + "/delete/<%= taskData.id %>\"><i class=\"fa fa-close\"></a>" + "</td>" + "</tr>" + "<% }); %>";

        $("#task_panel #task_table").after(task_html);

        $("#task_panel #realTimeTask > div > span").hover(function() {
            $(this).css('cursor', 'pointer');
        });
        $("#task_panel #realTimeTask > div > span").on("click", function() {
            //setCookie("group_task_"+id,JSON.stringify(new Array()));
            sessionStorage.setItem("group_task_" + id, JSON.stringify(new Array()));
            tasks.reset();
            taskView.render();
        });

        i18n.init(function(t) {
            $('[data-i18n]').i18n();
        });

        ////
        var TaskModel = Backbone.Model.extend({});
        var TaskCollection = Backbone.Collection.extend({
            sort_key: 'id',
            model: TaskModel,
            initialize: function() {
                this.sort_order = 'desc';
            },
            comparator: function(a, b) {
                var a = a.get(this.sort_key);
                var b = b.get(this.sort_key);
                if (this.sort_order === "desc") {
                    return a > b ? -1 : a < b ? 1 : 0;
                } else {
                    return a > b ? 1 : a < b ? -1 : 0;
                }
            },
            sortByField: function(fieldName) {
                this.sort_key = fieldName;
                this.sort();
            }
        });
        var TaskView = Backbone.View.extend({
            template: _.template(task_console),
            events: {
                "click": "getDetail"
            },
            render: function() {
                var taskData = this.collection.toJSON();
                if (taskData.length > 0) {
                    taskData.forEach(function(task) {
                        if (task.creationTime == null)
                            task.creationTime = '--';
                    });
                }
                $(this.el).html(this.template({
                    //task: this.collection.toJSON() // template each task
                    task: taskData
                }));
                $("#task_panel #realTimeTask #task-console").empty();
                $("#task_panel #realTimeTask #task-console").append(this.el.children);
                return this;
            },
            updateRecord: function(taskData) {
                $("#task_panel #realTimeTask #task-console #console-" + taskData.id + " td:eq(2)").text(taskData.creationTime);
                $("#task_panel #realTimeTask #task-console #console-" + taskData.id + " .executeStatus").text(taskData.executeStatus + '/' + taskData.total);
                var progressPercent = taskData.percent;
                $("#task_panel #realTimeTask #task-console #console-" + taskData.id + " .progress-bar").attr("style", "width:" + progressPercent + "%");
                $("#task_panel #realTimeTask #task-console #console-" + taskData.id + " .progress-bar").attr("aria-valuenow", progressPercent);
                $("#task_panel #realTimeTask #task-console #console-" + taskData.id + " .progress-bar").text(progressPercent + "%");
                if ((taskData.total - taskData.executeStatus) === 0) {
                    $("#task_panel #realTimeTask #task-console #console-" + taskData.id + " .progress").removeClass("active")
                }
            },
            getDetail: function() {
                console.log(this);
            }
        });

        tasks = new TaskCollection();
        taskView = new TaskView({ collection: tasks });

        // get task-console of this group from cookies
        //var taskConsoleData = getCookie("group_task_"+id);
        var taskConsoleData = sessionStorage.getItem("group_task_" + id);
        var realTimeTaskData = new Array();
        if ((taskConsoleData === "") || (taskConsoleData === null)) {
            //$("#realTimeTask").hide();
            //setCookie("group_task_"+id,JSON.stringify(new Array()));
            sessionStorage.setItem("group_task_" + id, JSON.stringify(new Array()));
        } else {
            realTimeTaskData = JSON.parse(taskConsoleData);
            $("#task_panel #realTimeTask").show();
        }
        tasks.add(realTimeTaskData);

        taskView.render();

    }

    function refreshRealTimeTask() {
        //var date = new Date();
        //console.log("S2:"+date.toLocaleTimeString());
        if ($("#myTab1 li:eq(1)").hasClass("active") || $("#myTab1 li:eq(2)").hasClass("active")) {
            tasks.each(function(task) {
                //if((task.executeStatus === "CompleteFail") || (task.executeStatus === "CompleteSuccess")){
                if ((task.attributes.total - task.attributes.executeStatus) === 0) {
                    // Finish
                } else
                    realTimeTask(task.id);
            });
            //taskView.render();
            setTimeout(refreshRealTimeTask, 1000);
        }
    }

    function s2(id) {
        var refreshRealTimeTaskJob = setTimeout(refreshRealTimeTask, 1000);
        if (taskView)
            taskView.render();
    };

    function s3(id) {
        var refreshRealTimeTaskJob = setTimeout(refreshRealTimeTask, 1000);
        if (taskView)
            taskView.render();
    };

    function s5(id) {
        subscribeForGroup(id);
        loadTaskPieChart(id);

        // Datagrid
        var AD = {};
        AD.collections = {};
        AD.title = "ns:DeviceGroup.Task";
        var url = $.config.server_rest_url + '/deviceGroups/' + id + '/taskStatus';
        var idAttribute = AD.idAttribute = "id";
        AD.buttons = [{
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
            "sButtonClass": "edit-group-device-list-btn txt-color-white bg-color-blue",
            "fnClick": function(nButton, oConfig, oFlash) {
                taskGrid.refresh();
                loadTaskPieChart(id);
            }
        }];

        AD.sort = {
            "column": "id",
            "sortDir": "desc"
        };
        AD.columns = [{
                title: 'ns:Group.Status',
                filterable: false,
                width: '50px',
                sortable: false,
                callback: function(o) {
                    var color = "#919191";
                    if (o.executeStatus === "Ready")
                        color = "#FFA500";
                    else if (o.executeStatus === "Running")
                        color = "#2E7BCC";
                    else if (o.executeStatus === "Done")
                        color = "#66AA00";
                    return "<i class=\"fa fa-circle\" style=\"color:" + color + ";\"></i> "; //+o.executeStatus;
                }
            }, {
                title: 'ns:Group.TaskName',
                property: 'job',
                filterable: false,
                sortable: false,
                width: '130px',
                callback: function(o) {
                    //return '<a href="#ajax/subtask_list.html/' + o.id + '">' + o.job + '</a>';
                    switch (o.job) {
                        //case "dcmoEnableDisable":
                        case "EnableDisableCapability":
                        case "EnableCapability":
                        case "DisableCapability":
                            return i18n.t("ns:Log.Task.TaskName.SwitchCapabilitySetting");
                        case "LockDevice":
                            return i18n.t("ns:Log.Task.TaskName.LockDevice");
                        case "UnlockDevice":
                            return i18n.t("ns:Log.Task.TaskName.UnlockDevice");
                        case "ActivateApp":
                            return i18n.t("ns:Log.Task.TaskName.ActivateApp");
                        case "DeactivateApp":
                            return i18n.t("ns:Log.Task.TaskName.DeactivateApp");
                        case "DownloadAndInstallApp":
                            return i18n.t("ns:Log.Task.TaskName.InstallApp");
                        case "RemoveApp":
                            return i18n.t("ns:Log.Task.TaskName.RemoveApp");
                        case "UpgradeApp":
                            return i18n.t("ns:Log.Task.TaskName.UpgradeApp");
                        case "setAllowUserInstallApp":
                            return i18n.t("ns:Log.Task.TaskName.AllowInstallApp");
                        case "autoRemoveLocalAPP":
                            return i18n.t("ns:Log.Task.TaskName.AutoRemoveLocalApp");
                        case "UpgradeFirmware":
                            return i18n.t("ns:Log.Task.TaskName.FirmwareUpgrade");
                        case "setDeviceEventList":
                            return i18n.t("ns:Log.Task.TaskName.SetDeviceEventList");
                        case "setRule":
                            return i18n.t("ns:Log.Task.TaskName.SetRule");
                        case "deleteRule":
                            return i18n.t("ns:Log.Task.TaskName.DeleteRule");
                        case "editRule":
                            return i18n.t("ns:Log.Task.TaskName.EditRule");
                        case "activeCapabilityEvent":
                            return i18n.t("ns:Log.Task.TaskName.ActivateCapabilityEvent");
                        case "deleteAllRuleAndEvent":
                            return i18n.t("ns:Log.Task.TaskName.DeleteRuleEvent");
                        case "Reset":
                            return i18n.t("ns:Log.Task.TaskName.RebootDevice");
                        case "Restart":
                            return i18n.t("ns:Log.Task.TaskName.RestartDevice");
                        case "applyManagementPolicy":
                            return i18n.t("ns:Log.Task.TaskName.ApplyManagementPolicy");
                        case "AddAppToBlacklist":
                            return i18n.t("ns:Log.Task.TaskName.SetAppBlacklist");
                        case "EnableTriggerServerConnection":
                            return i18n.t("ns:Log.Task.TaskName.EnableTrigger");
                        case "DisableTriggerServerConnection":
                            return i18n.t("ns:Log.Task.TaskName.DisableTrigger");
                        case "UpgradeDeltaFirmware":
                            return i18n.t("ns:Log.Task.TaskName.SystemUpdate");
                        default:
                            return "Unknown";
                    }
                }
            },
            //columns2
            {
                title: 'ns:Group.Creator',
                property: 'creatorName',
                filterable: false,
                width: '250px',
                sortable: false
            },
            //columns3
            {
                title: 'ns:Group.SubmitTime',
                property: 'creationTime',
                // cellClassName: 'name',
                filterable: false,
                width: '180px',
                sortable: false
            }, {
                title: 'ns:Log.Task.Schedule',
                filterable: false,
                width: '180px',
                sortable: false,
                callback: function(o) {
                    if (o.schedule.length == 0)
                        return "--";
                    return o.schedule;
                }
            },
            /*{
                title: 'ns:Group.Remark',
                property: 'remark',
                filterable: false,
                width: '350px',
                sortable: false
            },*/
            //columns5
            {
                title: 'ns:Group.Progress',
                property: 'progress',
                // cellClassName: 'creationTime',
                filterable: false,
                width: '80px',
                sortable: false,
                callback: function(o) {
                    // $("#group_task").css("display","none");
                    // $("#group_task_detail").css("display","");
                    // return '<i onClick="$(\"#group_task\").css(\"display\",\"none\");$(\"#group_task_detail\").css(\"display\",\"\");">'+o.progress+'</a>';
                    //return '<font onClick="showProgress('+id+',\''+encodeURI(JSON.stringify(o))+'\');" style="cursor:pointer;">'+o.progress+' <i class="fa fa-chevron-circle-right"></font>';
                    return '<a href="#ajax/group_detail.html/' + id + '/s5/' + o.id + '">' + o.progress + ' <i class="fa fa-chevron-circle-right"></a>';
                }
            },
            //columns6
            {
                title: 'ns:Group.Action',
                width: '100px',
                sortable: false,
                callback: function(o) {
                    var _html = '';
                    if (o.executeStatus == 'Queue') {
                        _html += '<a href="#ajax/group_detail.html/' + id + '/tasks/' + o.id + '/cancel" title="Cancel" class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #DF6B39; font-weight: bold;"><i class="fa fa-lg fa-times"></i></a>';
                    } else if (o.executeStatus == 'Done') {
                        _html += '<a href="#ajax/group_detail.html/' + id + '/tasks/' + o.id + '/redo" title="Redo" class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #2E4DA4; font-weight: bold;"><i class="fa fa-lg fa-refresh"></i></a>';
                    } else if (o.executeStatus == 'Ready' || o.executeStatus == 'Running') {
                        _html += '<a href="#ajax/group_detail.html/' + id + '/tasks/' + o.id + '/terminate" title="Terminate" class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #C10000; font-weight: bold;"><i class="fa fa-lg fa-square"></i></a>';
                    }
                    /*if (o.job!="DownloadAndInstallApp"&&o.job!="RemoveApp"&&o.job!="ActivateApp"&&o.job!="DeactivateApp")
                    {
                        _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #ACABAD;" disabled><i class="fa fa-inverse fa-play faa-tada"></i></a>';
                        _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #ACABAD;" disabled><i class="fa fa-inverse fa-pause faa-tada"></i></a>';
                        _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #ACABAD;" disabled><i class="fa fa-times"></i></a>';
                    }
                    else
                    {
                        if(o.executeStatus=="CompleteSuccess"||o.executeStatus=="CompleteFail")
                        {
                            _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #9AF093;" disabled><i class="fa fa-inverse fa-play faa-tada"></i></a>';
                            _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #537BF0;" disabled><i class="fa fa-inverse fa-pause faa-tada"></i></a>';
                            _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #FF5178;" disabled><i class="fa fa-times"></i></a>';
                        }
                        else
                        {
                            _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #3FB618;"><i class="fa fa-inverse fa-play faa-tada"></i></a>';
                            _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #5320CD;"><i class="fa fa-inverse fa-pause faa-tada"></i></a>';
                            _html += '<a class="btn btn-default btn-sm DTTT_button_text txt-color-white" style="background-color: #FF0039;"><i class="fa fa-times"></i></a>';
                        }

                    }*/
                    return _html;
                }
            }
        ];
        /* define namespace ends */

        /* model starts */
        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });
        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model,
            initialize: function() {
                this.on('change', this.someChange, this);
            },
            someChange: function(model) {
                //console.log("Task data changed!");
                loadTaskPieChart(id);
            }
        });
        /* collection ends */

        var gvs = "#group_task .cctech-gv";
        var gv = $(gvs).first();

        //if (!taskGrid) {
        taskGrid = new Backbone.GridView({
            el: gv,
            collection: new AD.Collection(),
            columns: AD.columns,
            title: AD.title,
            AD: AD,
            sort: AD.sort,
            buttons: AD.buttons
        });
        //} else {
        //  taskGrid.refresh();
        //}
        //this.gv.show();
        Backbone.Events.off("TaskUpdate");
        // Backbone.Events.on("TaskUpdate", function(result){
        //  if(_.has(result,"groupId")){
        //      if(result.groupId === id){
        //          taskGrid.refresh();
        //      }
        //  }
        // });
        Backbone.Events.on("TaskUpdate", function() {
            console.log("[MQTT] Task Refresh");
            taskGrid.refresh();
            loadTaskPieChart(id);
        });
        //pageSetUp();
        if ($(".widget-body-toolbar #status").length == 0) {
            $(".widget-body-toolbar").addClass('row');
            $(".widget-body-toolbar").append('<div class="hidden-xs col-lg-5" id="status"></div>');
            $(".widget-body-toolbar #status").append('<div class="hidden-xs col-lg-4" style="margin-top: 5px;"><i class="fa fa-circle" style="font-size: 22px;color:#FFA500;vertical-align: middle;"></i><span style="vertical-align:middle;">&nbsp;Ready</span></div>');
            $(".widget-body-toolbar #status").append('<div class="hidden-xs col-lg-4" style="margin-top: 5px;"><i class="fa fa-circle" style="font-size: 22px;color:#2E7BCC;vertical-align: middle;"></i><span style="vertical-align:middle;">&nbsp;Running</span></div>');
            $(".widget-body-toolbar #status").append('<div class="hidden-xs col-lg-4" style="margin-top: 5px;"><i class="fa fa-circle" style="font-size: 22px;color:#66AA00;vertical-align: middle;"></i><span style="vertical-align:middle;">&nbsp;Done</span></div>');
        }
        $('#group_task').i18n();
    };

    function editGroup(id) {
        // [Slove IE issue] Kenny block at 2015/2/6
        if ($.browser.msie || ($.browser.name === "Netscape")) {
            event.returnValue = false;
        } else {
            event.preventDefault();
        }
        window.location.href = "#ajax/group_edit.html/" + id;
    }

    function lock(group, lockType) {
        //console.log("Lock device: "+id+" Status: "+lockType);
        var lockStatus = "Unlock";
        var icon = "fa-unlock";
        if (lockType) {
            lockStatus = "Lock";
            icon = "fa-lock";
        }
        // ask verification
        $.SmartMessageBox({
            title: "<i class='fa " + icon + " txt-color-orangeDark'></i> " + i18n.t("ns:Message.Group." + lockStatus) + i18n.t("ns:Message.Group.Group") + " <span class='txt-color-orangeDark'><strong>" +
                group.name + "</strong></span> ?",
            content: i18n.t("ns:Message.Group." + lockStatus) + i18n.t("ns:Message.Group.TheGroup"),
            buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

        }, function(ButtonPressed) {
            if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                //$.root_.addClass('animated fadeOutUp');
                setTimeout(doLock(group.id, lockType), 1000);
            }

        });
    }

    function doLock(id, lockType) {
        console.log("Lock group: " + id + " Status: " + lockType);
        var lockStatus = "Unlock";
        var icon = "fa-unlock";
        if (lockType) {
            lockStatus = "Lock";
            icon = "fa-lock";
        }
        var url = $.config.server_rest_url + '/deviceGroups/' + id + '/actions/' + lockStatus.toLowerCase();
        $.ajax({
            url: url,
            type: 'POST', // PUT
            dataType: 'json',
            timeout: 10000,
            error: function(data, status, error) {
                //alert("Server busy, please try again!");
            }
        }).done(function(data) {
            // if ajax done
            $.smallBox({
                title: i18n.t("ns:Message.Group.Groups") + i18n.t("ns:Message.Group." + lockStatus) + i18n.t("ns:Message.Group.Success"),
                content: "<i class='fa " + icon + "'></i> <i>" + i18n.t("ns:Message.Group." + lockStatus) + i18n.t("ns:Message.Group.GroupID") + id + "</i>",
                color: "#648BB2",
                iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
                timeout: 5000
            });
        }).fail(function() {
            $.smallBox({
                title: i18n.t("ns:Message.Group.Groups") + i18n.t("ns:Message.Group." + lockStatus) + i18n.t("ns:Message.Group.Failed"),
                content: "<i class='fa " + icon + "'></i> <i>" + i18n.t("ns:Message.Group." + lockStatus) + i18n.t("ns:Message.Group.GroupID") + id + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
        });
    }

    function switchTrigger(group, enable) {
        //console.log("Lock device: "+id+" Status: "+lockType);
        var switchStatus = "DisableTrigger";
        var icon = "fa-unlock";
        if (enable) {
            switchStatus = "EnableTrigger";
            icon = "fa-lock";
        }
        // ask verification
        $.SmartMessageBox({
            title: "<i class='fa " + icon + " txt-color-orangeDark'></i> " + i18n.t("ns:Message.Group." + switchStatus) + " ?",
            buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

        }, function(ButtonPressed) {
            if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                //$.root_.addClass('animated fadeOutUp');
                setTimeout(doSwitchTrigger(group.id, enable), 1000);
            }

        });
    }

    function doSwitchTrigger(id, enable) {
        var switchStatus = "DisableTrigger";
        var switchAction = "disableTriggerServerConnection";
        var icon = "fa-unlock";
        if (enable) {
            switchStatus = "EnableTrigger";
            switchAction = "enableTriggerServerConnection";
            icon = "fa-lock";
        }
        var url = $.config.server_rest_url + '/deviceGroups/' + id + '/actions/' + switchAction;
        $.ajax({
            url: url,
            type: 'POST', // PUT
            dataType: 'json',
            timeout: 10000,
            error: function(data, status, error) {
                //alert("Server busy, please try again!");
            }
        }).done(function(data) {
            // if ajax done
            $.smallBox({
                title: i18n.t("ns:Message.Group.Groups") + i18n.t("ns:Message.Group." + switchStatus) + i18n.t("ns:Message.Group.Success"),
                content: "<i class='fa " + icon + "'></i> <i>" + i18n.t("ns:Message.Group." + switchStatus) + i18n.t("ns:Message.Group.GroupID") + id + "</i>",
                color: "#648BB2",
                iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
                timeout: 5000
            });
        }).fail(function() {
            $.smallBox({
                title: i18n.t("ns:Message.Group.Groups") + i18n.t("ns:Message.Group." + switchStatus) + i18n.t("ns:Message.Group.Failed"),
                content: "<i class='fa " + icon + "'></i> <i>" + i18n.t("ns:Message.Group." + switchStatus) + i18n.t("ns:Message.Group.GroupID") + id + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
        });
    }

    // Holisun: Copy from switchTrigger()
    function switchTriggerApp(group, enable) {
        //console.log("Lock device: "+id+" Status: "+lockType);
        var switchStatus = "DisableTriggerApp";
        var icon = "fa-unlock";
        if (enable) {
            switchStatus = "EnableTriggerApp";
            icon = "fa-lock";
        }
        // ask verification
        $.SmartMessageBox({
            title: "<i class='fa " + icon + " txt-color-orangeDark'></i> " + i18n.t("ns:Message.Group." + switchStatus) + " ?",
            buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

        }, function(ButtonPressed) {
            if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                //$.root_.addClass('animated fadeOutUp');
                setTimeout(doSwitchTriggerApp(group.id, group.name, enable), 1000);
            }

        });
    }

    // Holisun: Copy from doSwitchTrigger()
    function doSwitchTriggerApp(id, name, enable) {
        var switchStatus = "DisableTriggerApp";
        var switchAction = "disableTriggerServerConnectionByMqtt";
        var icon = "fa-unlock";
        if (enable) {
            switchStatus = "EnableTriggerApp";
            switchAction = "enableTriggerServerConnectionByMqtt";
            icon = "fa-lock";
        }
        var url = $.config.server_rest_url + '/deviceGroups/' + id + '/actions/' + switchAction;
        $.ajax({
            url: url,
            type: 'POST', // PUT
            dataType: 'json',
            timeout: 10000,
            error: function(data, status, error) {
                //alert("Server busy, please try again!");
            }
        }).done(function(data) {
            // if ajax done
            $.smallBox({
                title: i18n.t("ns:Message.Group.Groups") + i18n.t("ns:Message.Group." + switchStatus) + i18n.t("ns:Message.Group.Success"),
                content: "<i class='fa " + icon + "'></i> <i>" + i18n.t("ns:Message.Group." + switchStatus) + i18n.t("ns:Message.Group.GroupID") + name + "</i>",
                color: "#648BB2",
                iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
                timeout: 5000
            });
        }).fail(function() {
            $.smallBox({
                title: i18n.t("ns:Message.Group.Groups") + i18n.t("ns:Message.Group." + switchStatus) + i18n.t("ns:Message.Group.Failed"),
                content: "<i class='fa " + icon + "'></i> <i>" + i18n.t("ns:Message.Group." + switchStatus) + i18n.t("ns:Message.Group.GroupID") + name + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
        });
    }

    function reboot(group, bootType) {
        //console.log("Boot device: "+id+" Status: "+bootType);
        var bootStatus = "Reset";
        var icon = "fa-power-off";
        if (bootType) {
            bootStatus = "Restart";
            icon = "fa-share-square-o";
        }
        // ask verification
        $.SmartMessageBox({
            title: "<i class='fa " + icon + " txt-color-orangeDark'></i> " + i18n.t("ns:Message.Group." + bootStatus) + " " + i18n.t("ns:Message.Group.InGroup") + " " + "<span class='txt-color-orangeDark'><strong>" +
                group.name + "</strong></span> ?",
            content: i18n.t("ns:Message.Group." + bootStatus),
            buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

        }, function(ButtonPressed) {
            if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                //$.root_.addClass('animated fadeOutUp');
                setTimeout(doReboot(group.id, bootType), 1000);
            }

        });
    }

    function doReboot(id, bootType) {
        console.log("Boot group: " + id + " Status: " + bootType);
        var bootStatus = "Reset";
        var icon = "fa-share-square-o";
        if (bootType) {
            bootStatus = "Restart";
            icon = "fa-power-off";
        }
        var url = $.config.server_rest_url + '/deviceGroups/' + id + '/actions/' + bootStatus.toLowerCase();
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            timeout: 10000,
            error: function(data, status, error) {
                //alert("Server busy, please try again!");
            }
        }).done(function(data) {
            // if ajax done
            $.smallBox({
                title: i18n.t("ns:Message.Group.Groups") + i18n.t("ns:Message.Group." + bootStatus) + i18n.t("ns:Message.Group.Success"),
                content: "<i class='fa " + icon + "'></i> <i>" + i18n.t("ns:Message.Group." + bootStatus) + i18n.t("ns:Message.Group.GroupID") + id + "</i>",
                color: "#648BB2",
                iconSmall: "fa " + icon + " fa-2x fadeInRight animated",
                timeout: 5000
            });
        }).fail(function() {
            $.smallBox({
                title: i18n.t("ns:Message.Group.Groups") + i18n.t("ns:Message.Group." + bootStatus) + i18n.t("ns:Message.Group.Failed"),
                content: "<i class='fa " + icon + "'></i> <i>" + i18n.t("ns:Message.Group." + bootStatus) + i18n.t("ns:Message.Group.GroupID") + id + "</i>",
                color: "#B36464",
                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                timeout: 5000
            });
        });
    }

    function deleteGroup(id) {
        //console.log("Delete device: "+id);
        $.SmartMessageBox({
            title: "<i class='fa fa-trash-o txt-color-orangeDark'></i> " + i18n.t("ns:Message.Group.DeleteGroupSMB") + " <span class='txt-color-orangeDark'><strong>" +
                id + "</strong></span> ?",
            content: i18n.t("ns:Message.Group.DeleteGroupContent"),
            buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

        }, function(ButtonPressed) {
            if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                //$.root_.addClass('animated fadeOutUp');
                setTimeout(doDelete(id), 1000);
            }

        });
    }

    function doDelete(id) {
        // if ajax done
        $.smallBox({
            title: i18n.t("ns:Message.Group.GroupDelete") + i18n.t("ns:Message.Group.DeleteGroup"),
            content: "<i class='fa fa-clock-o'></i> <i>" + i18n.t("ns:Message.Group.DeleteGroupId") + id + "</i>",
            // title: "[Group Delete] Delete group", // i18n.t("ns:Device.DeleteDevice")
            // content: "<i class='fa fa-clock-o'></i> <i>You delete group id:"+id+"</i>",
            color: "#648BB2",
            iconSmall: "fa fa-trash-o fa-2x fadeInRight animated",
            timeout: 5000
        });
        window.location.href = "#ajax/group_list.html/grid";
    }

    function initial(
        group) {
        var id = group.id;
        // Store the policies in sessionStorage
        // var array = group.devicePolicyIdSet.split(',');
        console.log('[Policy] group.devicePolicyIdSet=[' + group.devicePolicyIdSet + ']');
        sessionStorage.setItem('sCsvPolicyIdString', group.devicePolicyIdSet);

        group_info_init(group);
        am_init(id);
        s3_init(id);
        $("#groupName font").text(group.name);
        $("#groupName").attr("href", "#" + AD.page + "/" + id);
        $("#group_detail_body").on("click", "#myTab1 a", function(event) {
            window.location.hash = "#" + AD.page + "/" + id;
            var href = $(event.target).attr("href");
            if (typeof href == "undefined") {
                href = $(event.target).parent().attr("href");
                console.log("href : " + href);
            }
            if (href === "#s1") {
                s1(id);
            } else if (href === "#s2") {
                s2(id);
            } else if (href === "#s3") {
                s3(id);
            } else if (href === "#s5") {
                s5(id);
            } else if (href === "#editGroup") {
                event.preventDefault();
                editGroup(id);
            } else if (href === "#lock") {
                event.preventDefault();
                lock(group, true);
            } else if (href === "#unlock") {
                event.preventDefault();
                lock(group, false);
            } else if (href === "#enableTrigger") {
                event.preventDefault();
                switchTrigger(group, true);
            } else if (href === "#disableTrigger") {
                event.preventDefault();
                switchTrigger(group, false);
            } else if (href === "#enableTriggerApp") {
                event.preventDefault();
                switchTriggerApp(group, true);
            } else if (href === "#disableTriggerApp") {
                event.preventDefault();
                switchTriggerApp(group, false);
            } else if (href === "#reset") {
                event.preventDefault();
                reboot(group, false);
            } else if (href === "#restart") {
                event.preventDefault();
                reboot(group, true);
            }
        })
        $("#agentUpgrade").attr("href", '#ajax/group_am.html/' + id + '/agent');
        $("#firmwareUpgrade").attr("href", '#ajax/group_am.html/' + id + '/fumo');
        $("#deviceControl").attr("href", '#ajax/group_am.html/' + id + '/deviceControl');
        initialStatus = true;
    }

    var routes = {};
    routes[AD.page + '/:_id'] = 'render';
    routes[AD.page + '/:_id/s5'] = 'taskPanel';
    routes[AD.page + '/:_id/s5/:_taskId'] = 'taskDetail';
    routes[AD.page + '/:_id/delete/:_taskId'] = 'taskRemove';
    routes[AD.page + '/:_id/tasks/:_taskId/cancel'] = 'taskCancel';
    routes[AD.page + '/:_id/tasks/:_taskId/terminate'] = 'taskTerminate';
    routes[AD.page + '/:_id/tasks/:_taskId/redo'] = 'taskRedo';
    var Router = Backbone.Router.extend({
        routes: routes,
        render: function(id) {
            // TODO initial devicegroup
            //console.log("Is Initial? "+initialStatus);
        },
        taskCancel: function(id, taskId) {
            var t = this;
            $.SmartMessageBox({
                    title: "<i class='fa fa-lg fa-times txt-color-orangeDark'></i> &nbsp; Cancel this task ?",
                    buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'
                },
                function(ButtonPressed) {
                    var url = $.config.server_rest_url + '/tasks/' + taskId + '/actions/cancel';
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                        $.ajax({
                            url: url,
                            type: 'POST', // PUT
                            dataType: 'json',
                            timeout: 10000,
                            error: function(data, status, error) {
                                console.log("error");
                            }
                        }).done(function() {
                            $.smallBox({
                                title: i18n.t("CancelTask Success"),
                                content: "<i class='fa fa-lg fa-times'></i> <i>" + i18n.t("CancelTask") + ": &nbsp;" + taskId + "</i>",
                                color: "#648BB2",
                                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        }).fail(function() {
                            $.smallBox({
                                title: i18n.t("CancelFailed"),
                                content: "<i class='fa fa-lg fa-times'></i> <i>" + i18n.t("CancelTask") + ": &nbsp;" + taskId + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        }).always(function() {
                            taskGrid.refresh();
                            window.location.href = "#" + "ajax/group_detail.html" + "/" + id;
                        });
                    } else {
                        window.location.href = "#" + "ajax/group_detail.html" + "/" + id;
                    }
                });
        },
        taskTerminate: function(id, taskId) {
            var t = this;
            $.SmartMessageBox({
                    title: "<i class='fa fa-lg fa-square txt-color-orangeDark'></i> &nbsp; Terminate this task ?",
                    buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'
                },
                function(ButtonPressed) {
                    var url = $.config.server_rest_url + '/tasks/' + taskId + '/actions/terminate';
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                        $.ajax({
                            url: url,
                            type: 'POST', // PUT
                            dataType: 'json',
                            timeout: 10000,
                            error: function(data, status, error) {
                                console.log("error");
                            }
                        }).done(function() {
                            $.smallBox({
                                title: i18n.t("TerminateTask Success"),
                                content: "<i class='fa fa-lg fa-square'></i> <i>" + i18n.t("TerminateTask") + ": &nbsp;" + taskId + "</i>",
                                color: "#648BB2",
                                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        }).fail(function() {
                            $.smallBox({
                                title: i18n.t("TerminateFailed"),
                                content: "<i class='fa fa-lg fa-square'></i> <i>" + i18n.t("TerminateTask") + ": &nbsp;" + taskId + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        }).always(function() {
                            taskGrid.refresh();
                            window.location.href = "#" + "ajax/group_detail.html" + "/" + id;
                        });
                    } else {
                        window.location.href = "#" + "ajax/group_detail.html" + "/" + id;
                    }
                });
        },
        taskRedo: function(id, taskId) {
            var t = this;
            $.SmartMessageBox({
                    title: "<i class='fa fa-lg fa-refresh txt-color-orangeDark'></i> &nbsp; Redo this task ?",
                    buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'
                },
                function(ButtonPressed) {
                    var url = $.config.server_rest_url + '/tasks/' + taskId + '/actions/redo';
                    if (ButtonPressed == i18n.t("ns:Message.Yes")) {
                        $.ajax({
                            url: url,
                            type: 'POST', // PUT
                            dataType: 'json',
                            timeout: 10000,
                            error: function(data, status, error) {
                                console.log("error");
                            }
                        }).done(function() {
                            $.smallBox({
                                title: i18n.t("RedoTask Success"),
                                content: "<i class='fa fa-lg fa-refresh'></i> <i>" + i18n.t("RedoTask") + ": &nbsp;" + taskId + "</i>",
                                color: "#648BB2",
                                iconSmall: "fa fa-check fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        }).fail(function() {
                            $.smallBox({
                                title: i18n.t("RedoFailed"),
                                content: "<i class='fa fa-lg fa-refresh'></i> <i>" + i18n.t("RedoTask") + ": &nbsp;" + taskId + "</i>",
                                color: "#B36464",
                                iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                                timeout: 5000
                            });
                        }).always(function() {
                            taskGrid.refresh();
                            window.location.href = "#" + "ajax/group_detail.html" + "/" + id;
                        });
                    } else {
                        window.location.href = "#" + "ajax/group_detail.html" + "/" + id;
                    }
                });
        },
        taskPanel: function(id) {
            var t = this;
            if (initialStatus) {
                $("#myTab1 li[class='active']").removeClass("active");
                $("#myTab1 a[href='#s5']").parent().addClass("active");
                $("#myTabContent1 > div").removeClass("active in");
                $("#s5").addClass("active in");
                s5(id);
            } else {
                t.navigate("#" + AD.page + "/" + id, { trigger: true });
            }
        },
        taskDetail: function(id, taskId) {
            var t = this;
            if (initialStatus) {
                //s5(id);
                showSubtask(id, taskId);
            } else {
                t.navigate("#" + AD.page + "/" + id, { trigger: true });
            }
        },
        taskRemove: function(id, taskId) {
            var t = this;
            if (initialStatus) {
                tasks.remove(taskId);
                $("#console-" + taskId).remove();
                // sessionStorage
                var taskConsoleData = JSON.parse(sessionStorage.getItem("group_task_" + id));
                taskConsoleData = _.without(taskConsoleData, _.findWhere(taskConsoleData, { 'id': parseInt(taskId) }));
                sessionStorage.setItem("group_task_" + id, JSON.stringify(taskConsoleData));
                window.location.hash = "#" + AD.page + "/" + id;
            } else {
                t.navigate("#" + AD.page + "/" + id, { trigger: true });
            }
        }
    });

    function realTimeTask(taskId) {
        // http://host:port/mdm-core/RESTful/tasks/30/taskExecutionStatus
        var url = $.config.server_rest_url + '/tasks/' + taskId + '/taskExecutionStatus';
        $.getJSON(url, function(taskExecutionStatus) {
            // results = {"id":30,"creationTime":"2014-08-29 16:27:12","creator":5,"creatorName":"Holisun","domainId":1,"remark":"App : unitech AMtest 2.0","schedule":"Immediately","job":"installDeviceApp","executeStatus":"Ready","progress":null,"name":"Install App : unitech AMtest 2.0","executeStatusMap":{"total":0,"Timeout":0,"Completed":0,"Failed":0,"Prepared":0,"Submitted":0,"Postponed":0}}
            if (!_.isUndefined(taskExecutionStatus)) {
                var executeStatus = taskExecutionStatus.executeStatusMap.Timeout + taskExecutionStatus.executeStatusMap.Completed + taskExecutionStatus.executeStatusMap.Failed;
                var total = taskExecutionStatus.executeStatusMap.total;
                var percent = Math.floor((executeStatus / total) * 100);
                /*
                var progressHtml = "";
                //if(percent > 0){
                    progressHtml = "<div>"
                                        +"<div class=\"col col-lg-4 col-md-4 col-sm-4 col-xs-4\">"
                                            +executeStatus+"/"+total
                                        +"</div>"
                                        +"<div class=\"col col-lg-8 col-md-8 col-sm-8 col-xs-8\">"
                                            +"<div class=\"progress progress-striped active\">"
                                                +"<div class=\"progress-bar bg-color-greenLight\" aria-valuetransitiongoal=\""+percent+"\" style=\"width: "+percent+"%;\" aria-valuenow=\""+percent+"\">"+percent+"%</div>"
                                            +"</div>"
                                        +"</div>"
                                    +"</div>";
                //}
                */
                //tasks.set({'id':taskExecutionStatus.id.toString(),'job':taskExecutionStatus.job,'creationTime':taskExecutionStatus.creationTime,'executeStatus':progressHtml},{remove:false});
                tasks.set({ 'id': taskExecutionStatus.id.toString(), 'job': taskExecutionStatus.job, 'creationTime': taskExecutionStatus.creationTime, 'executeStatus': executeStatus, 'total': total }, { remove: false });
                // Update cookie
                var hash = window.location.hash;
                var group_id = hash.replace(/^#ajax\/group_detail.html\/(\w+)$/, "$1");
                //var taskConsoleData = getCookie("group_task_"+group_id);
                var taskConsoleData = JSON.parse(sessionStorage.getItem("group_task_" + group_id));
                var record = _.findWhere(taskConsoleData, { 'id': parseInt(taskId) });
                var taskData = {
                    "id": parseInt(taskId),
                    "job": taskExecutionStatus.job,
                    "creationTime": taskExecutionStatus.creationTime ? taskExecutionStatus.creationTime : '--',
                    "executeStatus": executeStatus,
                    "total": total,
                    "percent": percent
                };
                taskConsoleData = _.without(taskConsoleData, _.findWhere(taskConsoleData, { 'id': parseInt(taskId) }));
                taskConsoleData.push(taskData);
                //setCookie("group_task_"+group_id,taskConsoleData);
                sessionStorage.setItem("group_task_" + group_id, JSON.stringify(taskConsoleData));
                /*
                var taskConsoleData = getCookie("group_task_"+group_id);
                realTimeTaskData = JSON.parse(taskConsoleData);
                var match = _.where(realTimeTaskData, {'id':sEvent.taskId});
                match[0].executeStatus = progressHtml;
                //realTimeTaskData.push({'id':sEvent.taskId,'job':sEvent.jobType,'creationTime':sEvent.creationTime,'executeStatus':progressHtml});
                setCookie("group_task_"+group_id,JSON.stringify(realTimeTaskData));
                */

                taskView.updateRecord(taskData);
            }
        });
    }


    function backToTask(id) {
        //s5(id);
        $("#group_task").show(500);
        $("#group_task #widget-grid").show(500);
        $("#group_task_detail").hide(500);
        window.location.href = "#ajax/group_detail.html" + "/" + id;
    }

    function loadTaskPieChart(id) {
        // Piechart
        var taskTypes = ['DM', 'AM'];
        for (var i = 0; i < taskTypes.length; i++) {
            var taskType = taskTypes[i];
            $.ajax({
                url: $.config.server_rest_url + '/deviceGroups/' + id + '/taskExecutionStatus?type=' + taskType,
                type: 'GET',
                dataType: 'json',
                async: false
            }).done(function(data) {
                var sliceColors = new Array();
                // Clear Old
                $("#" + taskType.toLowerCase() + "_piechart").empty();
                $("#" + taskType.toLowerCase() + "_info").empty();
                // Render
                var taskData = new Array();
                var taskExecutionStatus = {};
                taskExecutionStatus.names = {};
                var i = 0;
                var total = 0;
                $.each(data, function(key, value) {
                    if (key.toLowerCase() !== "total") {
                        total += value;
                        taskData.push(value);
                        eval('taskExecutionStatus.names[' + i + ']="' + key + '";');
                        sliceColors.push(eval('$.config.ui.taskExecutionStatusColor.' + key.toLowerCase()));
                        i++;
                    }
                });
                $("#" + taskType.toLowerCase() + "_piechart").sparkline(taskData, {
                    type: 'pie',
                    width: '128px',
                    height: '128px',
                    //borderWidth: '0',
                    //borderColor: '#3E3E3E',
                    sliceColors: sliceColors,
                    tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)',
                    tooltipValueLookups: {
                        names: taskExecutionStatus.names
                    }
                });
                $("#" + taskType.toLowerCase() + "_info").append("<ul></ul>");
                $("#" + taskType.toLowerCase() + "_info ul").css({ "list-style": "none" });
                for (var i = 0; i < taskData.length; i++) {
                    /*switch (taskExecutionStatus.names[i]) {
                        case "Ready":           taskExecutionStatus.names[i] = i18n.t("ns:Dashboard.Ready");    break;
                        case "Progress":        taskExecutionStatus.names[i] = i18n.t("ns:Dashboard.Progress"); break;
                        case "CompleteSuccess": taskExecutionStatus.names[i] = i18n.t("ns:Dashboard.Success");  break;
                        case "CompleteFail":    taskExecutionStatus.names[i] = i18n.t("ns:Dashboard.Fail");     break;
                        case "Done":            taskExecutionStatus.names[i] = "Done"
                        default:                taskExecutionStatus.names[i] = "Unknown";                        break;
                    }*/
                    if (taskExecutionStatus.names[i] == "Ready")
                        taskExecutionStatus.names[i] = "Ready(Queue)";
                    $("#" + taskType.toLowerCase() + "_info ul").append("<li style=\"margin-bottom: 5px;\"><i class=\"fa fa-square\" style=\"color:" + sliceColors[i] + ";margin-right: 5px;\"></i>" + taskExecuteStatusDisplay(taskExecutionStatus.names[i]) + " :" + taskData[i] + "</li>");
                }
            });
        }
    }

    function loadSubtaskPieChart(taskInfo) {
        // PieChart
        var sliceColors = new Array();
        $.ajax({
            url: $.config.server_rest_url + '/tasks/' + taskInfo.id + '/taskExecutionStatus',
            type: 'GET',
            dataType: 'json',
            async: false
        }).done(function(data) {
            // Clear Old
            $("#subtask_status").empty();
            // Render
            var taskData = new Array();
            var taskExecutionStatus = {};
            taskExecutionStatus.names = {};
            var i = 0;
            $.each(data.executeStatusMap, function(key, value) {
                if (key.toLowerCase() !== "total") {
                    taskData.push(value);
                    eval('taskExecutionStatus.names[' + i + ']="' + key + '";');
                    sliceColors.push(eval('$.config.ui.subtaskExecutionStatusColor.' + key.toLowerCase()));
                    i++;
                }
            });
            $("#subtask_status").append("<div class=\"col col-sm-5 col-md-5 col-lg-5\"><div id=\"taskExecutionStatusChart\"></div></div>");
            $("#taskExecutionStatusChart").sparkline(taskData, {
                type: 'pie',
                width: '128px',
                height: '128px',
                //borderWidth: '0',
                //borderColor: '#3E3E3E',
                sliceColors: sliceColors,
                tooltipFormat: '<span style="color: {{color}}">&#9679;</span> {{offset:names}} ({{percent.1}}%)',
                tooltipValueLookups: {
                    names: taskExecutionStatus.names
                }
            });
            $("#subtask_status").append("<div class=\"col col-sm-7 col-md-7 col-lg-7\"><ul></ul></div>");
            $("#subtask_status").css("margin", "10px");
            $("#subtask_status ul").css({ "list-style": "none" });
            for (var i = 0; i < taskData.length; i++) {
                switch (taskExecutionStatus.names[i]) {
                    case "Timeout":
                        taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Timeout");
                        break;
                    case "Completed":
                        taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Completed");
                        break;
                    case "Failed":
                        taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Failed");
                        break;
                    case "Prepared":
                        taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Prepared");
                        break;
                    case "Submitted":
                        taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Submitted");
                        break;
                    case "Postponed":
                        taskExecutionStatus.names[i] = i18n.t("ns:Log.Task.Postponed");
                        break;
                    default:
                        taskExecutionStatus.names[i] = "Unknown";
                        break;
                }
                $("#subtask_status ul").append("<li style=\"margin-bottom: 5px;\"><i class=\"fa fa-square\" style=\"color:" + sliceColors[i] + ";margin-right: 5px;\"></i>" + taskExecutionStatus.names[i] + " :" + taskData[i] + "</li>");
            }
        });
    }

    function showSubtask(id, taskId) {
        $.ajax({
            url: $.config.server_rest_url + '/tasks/' + taskId,
            type: 'GET',
            dataType: 'json',
            timeout: 10000
        }).done(function(data) {
            showProgress(id, encodeURI(JSON.stringify(data)));
            $("#myTab1 li[class='active']").removeClass("active");
            $("#myTab1 a[href='#s5']").parent().addClass("active");
            $("#myTabContent1 > div").removeClass("active in");
            $("#s5").addClass("active in");
        }).fail(function() {});

    }

    function showProgress(id, taskInfoStr) {
        var taskInfo = JSON.parse(decodeURI(taskInfoStr));
        switch (taskInfo.job) {
            case "EnableDisableCapability":
            case "EnableCapability":
            case "DisableCapability":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.SwitchCapabilitySetting");
                break;
            case "LockDevice":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.LockDevice");
                break;
            case "UnlockDevice":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.UnlockDevice");
                break;
            case "ActivateApp":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.ActivateApp");
                break;
            case "DeactivateApp":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.DeactivateApp");
                break;
            case "DownloadAndInstallApp":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.InstallApp");
                break;
            case "RemoveApp":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.RemoveApp");
                break;
            case "UpgradeApp":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.UpgradeApp");
                break;
            case "setAllowUserInstallApp":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.AllowInstallApp");
                break;
            case "autoRemoveLocalAPP":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.AutoRemoveLocalApp");
                break;
            case "UpgradeFirmware":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.FirmwareUpgrade");
                break;
            case "setDeviceEventList":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.SetDeviceEventList");
                break;
            case "setRule":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.SetRule");
                break;
            case "deleteRule":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.DeleteRule");
                break;
            case "editRule":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.EditRule");
                break;
            case "activeCapabilityEvent":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.ActivateCapabilityEvent");
                break;
            case "deleteAllRuleAndEvent":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.DeleteRuleEvent");
                break;
            case "Reset":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.RebootDevice");
                break;
            case "Restart":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.RestartDevice");
                break;
            case "applyManagementPolicy":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.ApplyManagementPolicy");
                break;
            case "AddAppToBlacklist":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.SetAppBlacklist");
                break;
            case "EnableTriggerServerConnection":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.EnableTrigger");
                break;
            case "DisableTriggerServerConnection":
                taskInfo.job = i18n.t("ns:Log.Task.TaskName.DisableTrigger");
                break;
            default:
                taskInfo.job = "Unknown";
                break;
        }

        var tpl_taskInfo_html = "<table class=\"table table-striped\" style=\"clear: both\">" +
            "<thead><tr role=\"row\">" +
            "<th style=\"cursor:pointer; width: 120px;font-size: 13px;background-color: #A6CD95;\"><i class=\"fa fa-lg fa-chevron-circle-left\"></i>&nbsp;<font data-i18n=\"ns:Group.BackToList\">Back to list</font></th>" +
            "<th></th></tr></thead>" +
            "<tbody>" +
            "<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.ID\"></font></td><td>{{id}}</td></tr>" +
            "<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.Name\"></font></td><td>{{job}}</td></tr>" +
            "<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.SubmitTime\"></font></td><td>{{creationTime}}</td></tr>" +
            "<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.Status\"></font></td><td>{{displayExecuteStatus executeStatus}}</td></tr>" +
            "<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.Remark\"></font></td><td>{{remark}}</td></tr>" +
            "<tr><td style=\"font-weight: bold;\"><font data-i18n=\"ns:Log.Task.Creator\"></font></td><td>{{creatorName}}</td></tr>" +
            //"<tr><td>Action:<i class=\"fa fa-refresh\"></i>Redo</td></tr>"+
            "</tbody>" +
            "</table>";
        Handlebars.registerHelper("displayExecuteStatus", function(executeStatus) {
            var html = "";
            var color = "#919191";
            color = eval('$.config.ui.taskExecutionStatusColor.' + executeStatus.toLowerCase());
            html = "<i class=\"fa fa-circle\" style=\"color:" + color + ";\"></i> " + taskExecuteStatusDisplay(executeStatus);
            return new Handlebars.SafeString(html);
        });
        var taskInfo_template = Handlebars.compile(tpl_taskInfo_html);

        var taskInfo_html = taskInfo_template(taskInfo);
        $("#task_info").empty();
        $("#task_info").append(taskInfo_html);
        $('#task_info').i18n();
        loadSubtaskPieChart(taskInfo);

        // Grid
        var AD = {};
        AD.collections = {};
        var url = $.config.server_rest_url + '/tasks/' + taskInfo.id + '/subtasks';
        var idAttribute = AD.idAttribute = "id";
        AD.buttons = [{
            "sExtends": "text",
            "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
            "sButtonClass": "edit-group-device-list-btn txt-color-white bg-color-blue",
            "fnClick": function(nButton, oConfig, oFlash) {
                subtaskGrid.refresh();
                loadSubtaskPieChart(taskInfo);
            }
        }];
        AD.sort = {
            "column": "finishTime",
            "sortDir": "desc"
        };
        AD.columns = [{
                title: 'ns:Group.ID',
                property: 'id',
                width: '80px',
                filterable: false,
                sortable: true
            }, {
                title: 'ns:Group.DeviceName',
                //property: 'deviceId',
                filterable: false,
                sortable: false,
                callback: function(o) {
                    if (_.isNull(o.deviceName)) {
                        return i18n.t("ns:Group.Message.RecordDelete");
                    } else {
                        if (o.provisionStatus == "Provisioned") {
                            return '<a href="#ajax/device_detail.html/' + o.deviceId + '">' + o.deviceName + '</a>';
                        } else {
                            return o.deviceName;
                        }
                    };
                }
            }, {
                title: 'ns:Group.Status',
                property: 'state',
                filterable: false,
                sortable: false,
                callback: function(o) {
                    var color = "#919191";
                    /*
                    if(o.state === "Prepared"){
                        color = "#FFA500";
                    }else if(o.state === "Submitted"){
                        color = "#2E7BCC";
                    }else if(o.state === "Completed"){
                        color = "#66AA00";
                    }else if(o.state === "Failed"){
                        color = "#FF0039";
                    }else if(o.state === "Timeout"){
                        color = "#909090";
                    }else if(o.state === "Postponed"){
                        color = "#D8D338";
                    }
                    */
                    color = eval('$.config.ui.subtaskExecutionStatusColor.' + o.state.toLowerCase());
                    return "<i class=\"fa fa-circle\" style=\"color:" + color + ";\"></i> " + o.state;
                }
            }, {
                title: 'ns:Group.LastUpdateTime',
                property: 'finishTime',
                callback: function(o) {
                    return o.finishTime;
                },
                filterable: false,
                sortable: true
            },
            /*{
                title: 'ns:Group.Remark',
                property: 'remark',
                filterable: false,
                sortable: false
            }*/
        ];
        /* define namespace ends */

        /* model starts */

        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        /* model ends */

        /* collection starts */

        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model,
            initialize: function() {
                this.on('change', this.someChange, this);
            },
            someChange: function(model) {
                //console.log("Subtask data changed!");
                loadSubtaskPieChart(taskInfo);
            }
        });

        /* collection ends */
        var gvs = "#group_task_detail .cctech-gv";
        var gv = $(gvs).first();

        if (!subtaskGrid) {
            var subtaskGrid = new Backbone.GridView({
                el: gv,
                collection: new AD.Collection(),
                columns: AD.columns,
                title: 'Subtasks',
                AD: AD,
                sort: AD.sort,
                buttons: AD.buttons
            });
        } else {
            subtaskGrid.refresh();
        }

        Backbone.Events.off("SubTaskUpdate");
        Backbone.Events.on("SubTaskUpdate", function(data) {
            if (data.taskId === taskInfo.id) {
                subtaskGrid.refresh();
            }
        });
        //pageSetUp();

        //$("#group_task").css("display","none");
        //$("#group_task_detail").css("display","");
        $("#group_task").hide(500);
        $("#group_task_detail").show(500);

        $("#task_info table tr:eq(0)").on("click", function() {
            backToTask(id);
        });
    }

    AD.bootstrap = function() {

        devicesLocation = undefined;
        $("body").removeClass("loading");

        if ($.login.user.checkRole('SystemOwner')) {
            $("#groupAction.dropdown-menu").append(
                '<li role="presentation" class ="divider"></li>' +
                '<li role="presentation" class="dropdown-header"><span data-i18n="ns:DeviceGroup.Actions.SystemOwnerOnly">System Owner Only</span></li>' +
                '<li><a href="#enableTrigger"><i class="fa fa-lock"></i><span data-i18n="ns:DeviceGroup.Actions.EnableTrigger">Enable Trigger</span></a></li>' +
                '<li><a href="#disableTrigger"><i class="fa fa-unlock"></i><span data-i18n="ns:DeviceGroup.Actions.DisableTrigger">Disable Trigger</span></a></li>');
            // Holisun: MQTT function switch Trigger Server
            /*
            $("#groupAction.dropdown-menu").append(
                '<li><a href="#enableTriggerApp"><i class="fa fa-lock"></i><span data-i18n="ns:DeviceGroup.Actions.EnableTriggerApp">Enable Trigger APP</span></a></li>' +
                '<li><a href="#disableTriggerApp"><i class="fa fa-unlock"></i><span data-i18n="ns:DeviceGroup.Actions.DisableTriggerApp">Disable Trigger APP</span></a></li>');
            */
        }

        var router = new Router;
        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }

        var hash = window.location.hash;
        var id = hash.split('/')[2];
        //var id = hash.replace(/^#ajax\/group_detail.html\/(\w+)$/, "$1");
        s1(id);
        if (!_.isUndefined(id)) {
            $.ajax({
                url: $.config.server_rest_url + '/deviceGroups/' + id,
                type: 'GET',
                dataType: 'json',
                timeout: 10000,
                error: function(data, status, error) {}
            }).done(function(data) {
                initial(data);
            }).fail(function() {});
        };
    };

    $[AD.page] = AD;
})(jQuery);

function imgLoadError(t, item) {
    $(t).replaceWith(
        "<div style=\"" + "-webkit-border-radius: 2px;  -moz-border-radius: 2px;  border-radius: 2px;" + "width: 32px;" + "height: 36px;" + "background-color: #3678DB;" + "text-align: center;" + "line-height: 44px;" + "\">" + "<i class=\"fa fa-question fa-inverse\" style=\"font-size: 24px;\"></i>" + "</div>"
    );
}

function imgLoadErrorBigger(t, name, color, icon) {
    $(t).replaceWith(
        "<div" + " class=\"faa-parent animated-hover\"" + " style=\"" + "width: 50px;" + "height: 50px;" + "background-color: " + color + ";" //#3678DB;"
        + "text-align: center;" + "border-radius: 5px;" + "margin: 1px auto;" + "\">" + "<i class=\"fa fa-inverse " + icon + " faa-tada\" style=\"font-size: 25px; margin-top: 13px\"></i>" + "</div>"
    );
}
