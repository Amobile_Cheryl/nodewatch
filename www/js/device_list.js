
(function($)
{
    var currentPage = "";
    var AD = {};
    AD.page = "ajax/device_list.html";
    var idAttribute = AD.idAttribute = "id";
    var device_columns = [
        {
            title: 'Image',
            sortable: false,
            width: '60px',
            callback: function(o)
            {
                if(o.haveEdges > 0){
                    return '<img src="img/lora_node.png" style="width:53px;height:53px; margin-right:-5px;" />';
                }else {
                    return '<img src="' + o.deviceTypeIconUrl + '"" style="height:30px; margin-right:-5px;" />';
                }
            }
        },
        {
            title: 'ns:Device.Title.Status',
            filterable: false,
            sortable: false,
            width: '75px',
            callback: function(o)
            {
                var isLock;
                isLock = '<img src="img/status_unlock.png" style="height: 20px;margin-right: 6px;" />';
                if (!_.isUndefined(o.isLock))
                {
                    if (o.isLock)
                        isLock = o.isLock ? '<img src="img/status_lock.png" style="height:20px; margin-right: 6px;" />' : '<img src="img/status_unlock.png" style="height:25px;" />';
                }

                var isOnlineStatus = '<img src="img/status_offline.png" style="height:20px; margin-right: 0px;" />';
                if (!_.isUndefined(o.isOnline))
                {
                    if (o.isOnline)
                        isOnlineStatus = '<img src="img/status_online.png" style="height:20px; margin-right: 0px;" />';
                }

                
                //var isJailbreak = (_.isUndefined(o.isJailbreak)) ? "" : "<i class=\"fa fa-bug\" style=\"color:crimson;\" title=\""+i18n.t("ns:Message.Device.Jailbreak")+"\"></i>";
                //return isLock+isJailbreak;
                //return isJailbreak;
                return isLock + "&nbsp;" + isOnlineStatus;
            }
        },
        {
            title: 'ns:Device.Title.Label',
            property: 'label',
            filterable: false,
            searchName: 'label',
            sortable: true,
            width: '150px',
        },
        //column2
        {
            title: 'IMEI & MAC',
            property: 'name',
            filterable: true,
            searchName: 'name',
            sortable: true,
            width: '160px',
            callback: function(o)
            {
                
                if (o.provisionStatus === "Provisioned" || o.provisionStatus === "New")
                {
                    var more_link = "#ajax/device_sublist_piechart.html/grid?title=" + o.name;
                    var baseLink = null;
                        
                    if(o.haveEdges > 0){
                        if(o.provisionStatus === "New"){
                            baseLink = '<div style="weight : 300px; height : auto; ">' + o.name + '<UL style = "padding-top : 10px"><LI><a href=' + more_link + '>' + i18n.t("ns:Device.SeeMoreEdges") + '</a></LI></UL></div>';
                        }else {
                            baseLink = '<div style="weight : 300px; height : auto; "><b><a href="#ajax/device_detail.html/' + o.id + '" title="Check the device detail" >' + o.name + '</a></b><UL style = "padding-top : 10px"><LI><a href=' + more_link + '>' + "See More Edges..." + '</a></LI></UL></div>';
                        }
                    }else {
                         baseLink = '<div style="weight : 300px; height : auto; "><b><a href="#ajax/device_detail.html/' + o.id + '" title="Check the device detail" >' + o.name + '</a></b></div>';
                    }
                        return  baseLink; 
                }
                else
                {
                    return o.name;
                }
            }
        },
        {
            title: 'ns:Device.Title.DeviceType',
            cellClassName: 'deviceTypeName',
            filterable: true,
            searchName: 'deviceTypeName',
            width: '90px',
            searchDom: function()
            {
                var options = '<option value="" selected="selected"></option>';
                $.ajax(
                {
                    // search=operatingSystemName:Android&
                    url: $.config.server_rest_url + '/deviceTypes?pageNumber=1&pageSize=100&pageSort=name',
                    dataType: 'JSON',
                    async: false,
                    success: function(deviceTypeCollection)
                    {
                        for (var i = 0; i < deviceTypeCollection.content.length - 1; i++)
                        {
                            options += '<option value="' + deviceTypeCollection.content[i].id + '">' + deviceTypeCollection.content[i].name + '</option>';
                        }
                    }
                });
                return '<span class="searchbox_1"><b>' + i18n.t("ns:Device.Title.DeviceType") + ' </b><select class="search_1" name="deviceTypeId">' + options + '</select> <i style="color:red" class="fa fa-remove" /></span>';
            },
            sortable: false,
            callback: function(o)
            {
                return o.deviceTypeName;
            }
        },
        //column6
        {
            title: 'ns:Device.Title.FirmwareVersion',
            property: 'firmwareVersion',
            filterable: false,
            sortable: true,
            width: '250px'
        },
        {
            title: 'ns:Device.Title.LastReport',
            property: 'latestConnectTime',
            cellClassName: 'latestConnectTime',
            filterable: false,
            sortable: true,
            width: '140px'
        },
        //column8
        {
            title: 'ns:Device.Title.Domain',
            property: 'domainId',
            filterable: false,
            sortable: false,
            width: '80px',
            visible: $.login.user.permissionClass.value >= $.common.PermissionClass.SystemOwner.value,
            callback: function(o)
            {
                //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId] ;
                return (typeof(o.domainName) === "undefined") ? '' : o.domainName;
            }
        },
        {
            title: 'ns:Device.Title.Action',
            operator: true,
            sortable: false,
            width: '70px',
            callback: function(o)
            {
                var _html = '';
                //_html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                // console.log("[title: 'ns:Device.Title.Actiono][idAttribute] = " + o[idAttribute] +" idAttribute = " + idAttribute );
                // _html += '<a href="#" class="btn btn-mini btn-warning removeOne" title="Unlock" data-id="' + o[idAttribute] + '"><i class="icon-unlock"></i> </a>';
                return _html;
            }
        }
    ];

    var routes = {};
    routes[AD.page + '/grid'] = 'main';
    routes[AD.page + '/delete/:_id'] = 'delete_single';
    routes[AD.page] = 'render';

    var MyRouter = Backbone.DG1.extend(
    {
        routes: routes,
        initialize: function(opt)
        {
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            console.error();
            this.show_grid();
        },
        show_grid: function()
        {
            var t = this;
            var gvs = "#s1 .cctech-gv";
            var gv = $(gvs).first();

            if (!androidGrid)
            {
                var androidGrid = new Backbone.GridView(
                {
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    buttons: t.AD.buttons,
                    columns: t.AD.columns,
                    filter: t.AD.filter,
                    AD: t.AD
                });

                gv.on("click", ".s2-btn", function(event)
                {
                    event.preventDefault();
                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');
                    t.navigate("#ajax/device_import.html",
                    {
                        trigger: true
                    });
                });

                gv.on("click", ".s1-btn", function(event)
                {
                    event.preventDefault();
                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');
                    t.navigate("#ajax/device_import.html",
                    {
                        trigger: true
                    });
                });
            }
            else
            {
                androidGrid.refresh();
            }
            t.$gv.show();
        },
        delete_single: function(id)
        {
            var dmURL = $.config.server_rest_url + '/devices/';
            $.SmartMessageBox(
            {
                // title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; Delete Device <span class='txt-color-orangeDark'><strong> &nbsp;" +
                //     id + "</strong></span> ?",
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; " + i18n.t("ns:Message.Device.DeleteTheDevice") + " <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                //content: "Delete the device ?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> " + i18n.t("ns:Message.Device.DeleteDeviceContent") + "</span>",
                buttons: '[' + i18n.t("ns:Message.No") + '][' + i18n.t("ns:Message.Yes") + ']'

            }, function(ButtonPressed)
            {
                if (ButtonPressed == i18n.t("ns:Message.Yes"))
                {
                    $.ajax(
                    {
                        beforeSend: function(xhr)
                        {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + id,
                        timeout: 10000
                    }).done(function()
                    {
                        $.smallBox(
                        {
                            
                            title: i18n.t("ns:Message.Device.DeviceDelete") + i18n.t("ns:Message.Device.DeleteSuccess"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.DeleteDevice") + ": &nbsp;" + id + "</i>",
                            // title: "[Device Delete] Deleted successfully",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Device : &nbsp;" + id + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function()
                    {
                        $.smallBox(
                        {
                            title: i18n.t("ns:Message.Device.DeviceDelete") + i18n.t("ns:Message.Device.DeleteFailed"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.DeleteDevice") + ": &nbsp;" + id + "</i>",
                            // title: "[Device Delete] Deleted failed",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Device : &nbsp;" + id + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function()
                    {
                        window.location.href = "#" + AD.page + "/grid";
                        // console.log("location.href : " + window.location.href);
                    });
                }
                else
                {
                    window.location.href = "#" + AD.page + "/grid";
                }
            });
        },
        main: function()
        {
            if (currentPage == "s1")
                s1();
            else if (currentPage == "s2")
                s2();
        }
    });

    // Windows
    function s1()
    {
        var AD = {};
        AD.page = "ajax/device_list.html";
        AD.collections = {};
        currentPage = "s1";
        var url = $.config.server_rest_url + "/devices?";
        if ($.login.user.checkRole('DeviceOperator'))
        {    
            AD.buttons = [
                //button
                {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-plus'></i>&nbsp; " + "Add Device", //i18n.translate("ns:File.Upload"),
                    "sButtonClass": "upload-btn s1-btn btn-default txt-color-white bg-color-blue"
                },
                {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash)
                    {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
            ];
        }    
        //=============================================================================================================
        // Define default value of search criteria
        //=============================================================================================================
        AD.filter={};
        AD.filter.search={};
        AD.filter.search.operatingSystemName = "Windows";    

        AD.columns = device_columns;
        AD.Model = Backbone.Model.extend(
        {
            urlRoot: url,
            idAttribute: idAttribute
        });
        AD.Collection = Backbone.Collection.extend(
        {
            url: url,
            model: AD.Model
        });
        AD.app = new MyRouter(
        {
            "AD": AD
        });
        pageSetUp();
    };

    // Android
    function s2()
    {
        var AD = {};
        AD.page = "ajax/device_list.html";
        AD.collections = {};
        currentPage = "s2";
        //var url = $.config.server_rest_url + "/devices" + "?search=operatingSystemName:Android";
        var url = $.config.server_new_rest_url + "/devices?";
        if ($.login.user.checkRole('DeviceOperator')) 
        {
            AD.buttons = [
                //button
                {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-plus'></i>&nbsp; " + "Add Device", //i18n.translate("ns:File.Upload"),
                    "sButtonClass": "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
                },
                {
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-refresh'></i>&nbsp; " + i18n.translate("ns:System.Refresh"),
                    "sButtonClass": "upload-btn txt-color-white bg-color-greenLight",
                    "fnClick": function(nButton, oConfig, oFlash)
                    {
                        $(".cctech-gv table").dataTable().fnDraw(false);
                    }
                }
            ];
        }
        //=============================================================================================================
        // Define default value of search criteria
        //=============================================================================================================
        AD.filter={};
        AD.filter.search={};
        AD.filter.search.operatingSystemName = "Android";    

        AD.columns = device_columns;
        AD.Model = Backbone.Model.extend(
        {
            urlRoot: url,
            idAttribute: idAttribute
        });
        AD.Collection = Backbone.Collection.extend(
        {
            url: url,
            model: AD.Model
        });
        AD.app = new MyRouter(
        {
            "AD": AD
        });
        //debugger;
        pageSetUp();
    };

    AD.bootstrap = function()
    {
        i18n.init(function(t)
        {
            $('[data-i18n]').i18n();
        });

        if (!$.login.user.checkRole('DeviceViewer'))
        {
            window.location.href = "#ajax/dashboard.html/grid";
        };

        $("#myTab li").click(function()
        {
            switch (this.firstElementChild.id)
            {
                case "s1": //Windows
                    s1();
                    break;
                case "s2": //Android
                    s2();
                    break;
                case "s3": //iPhone
                    s3();
                    break;
            }
        });

        if (!Backbone.History.started)
        {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }

        if (currentPage == ""){
            s2();
        }

        if (currentPage == "s1")
        {
            $("#tab2").removeClass('active');
            $("#tab1").addClass('active');
        }
        else if (currentPage == "s2")
        {
            $("#tab1").removeClass('active');
            $("#tab2").addClass('active');
        }
        $("body").removeClass("loading");
    };
    $[AD.page] = AD;
})(jQuery);
