(function($) {
    /* run bootstrap */
    var summary_template = Handlebars.compile($('#tpl_summary').html());
    //tpl_summary_edit_lan
    var summary_edit_lan_template = Handlebars.compile($('#tpl_edit_lan').html());
    var summary_edit_wan_template = Handlebars.compile($('#tpl_edit_wan').html());
    var summary_edit_wireless_template = Handlebars.compile($('#tpl_edit_wireless').html());
    var summary_edit_port_mapping_template = Handlebars.compile($('#tpl_edit_port_mapping').html());

    /* redefine */

    /* define date format*/
    df = new DateFormat();
    df.setFormat('yyyy-MMM-dd HH:mm:ss');
    /*                  */

    /* define namespace starts */

    // Move variable AD and AD.page outside of function
    var AD = {};
    AD.page = "ajax/device_list.html";
    // Add adReload function to run this script when user click this .html every time.

    var addDeviceList;
    var removeDeviceList;
    var groupDeviceList;
    var adReload = function() {
        addDeviceList = [];
        removeDeviceList = [];
        groupDeviceList = [];

        AD.collections = {};
        AD.buttons = {};
        AD.columns = [
            //column0
            // Only using with domain device list modification
			{
                // title: '<input type="checkbox" value="all"></input>',
                title: '',
                cellClassName: '',
                sortable: false,
                width: '50px',
                visible: false,
                callback: function(o) {
                    //console.log(o.domainName);
                    if (o.domainName !== 'System' && !($.inArray(o.id,removeDeviceList)>-1))
                    {
                        return '<input type="checkbox" class="editCheckbox" checked value="' + o[idAttribute] + '"></input>';
                    }
                    else if (o.domainName === 'System' && $.inArray(o.id,addDeviceList)>-1)
                    {
                        return '<input type="checkbox" class="editCheckbox" checked value="' + o[idAttribute] + '"></input>';
                    }
                    else 
                    {
                        return '<input type="checkbox" class="editCheckbox" value="' + o[idAttribute] + '"></input>';
                    }
                }
            },
			//column1
            // Only using with group device list modification
            {
                // title: '<input type="checkbox" value="all"></input>',
                title: '',
                cellClassName: '',
                sortable: false,
                width: '50px',
                visible: false,
                callback: function(o) {
                    if ($.inArray(o.id,groupDeviceList) > -1)
                    {
                        return '<input type="checkbox" class="editCheckbox" checked value="' + o[idAttribute] + '"></input>';
                    }
                    else 
                    {
                        return '<input type="checkbox" class="editCheckbox" value="' + o[idAttribute] + '"></input>';
                    }
                }
            },
			
			{
                title: '',//'ns:Device.Title.Status',
                //property: 'isLock',
                filterable: false,
                sortable: false,
                width: '50px',
                callback: function(o) {
					var isLock;
					//isLock = (_.isUndefined(o.isLock)) ? "" : "<i class=\"fa fa-lock\" title=\""+i18n.t("ns:Device.Message.Lock")+"\"></i>";
					if (o.hasOwnProperty('isLock') == true)
						isLock = o.isLock ? "<i class=\"fa fa-lock txt-color-red\" title=\""+i18n.t("ns:Device.Message.Lock")+"\"></i>" : "<i class=\"fa fa-unlock txt-color-blueLight\" title=\""+i18n.t("ns:Device.Message.Unlock")+"\"></i>";
					else
						isLock = "<i class=\"fa fa-unlock txt-color-blueLight\" title=\""+i18n.t("ns:Device.Message.Unlock")+"\"></i>";
					//var isJailbreak = (_.isUndefined(o.isJailbreak)) ? "" : "<i class=\"fa fa-bug\" style=\"color:crimson;\" title=\""+i18n.t("ns:Message.Device.Jailbreak")+"\"></i>";
					//return isLock+isJailbreak;
					//return isJailbreak;
					return isLock;
                }
            },
            //column alias
            /*{
                title: 'ns:Device.Title.Label',
                property: 'label',
                filterable: true,
                searchName: 'label',
                sortable: true,
                callback: function(o) {
                    var label = o.label;
                    var _html = '';
                    if (label == null || label.length == 0)
                        label = '--';
                    _html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" title="Modify the label">' + label + '</a>';
                    return _html;
                    //return '<a href="#ajax/device_detail.html/' + o.id + '">' + o.name + '</a>';
                }
            },*/			
            //column2
            {
                title: 'ns:Device.Title.ID',
                property: 'name',
                filterable: true,
				searchName: 'name',
                sortable: true,
                callback: function(o) {
                    // console.log(o);
                    //return "<a >" + o.cwmpId + "</a>";
                    //if(o.id === 14)
                    //  var id = "AAAAAAA0000003";
                    //else
                    //  var id = "AAAAAAA0000004"
                    if (o.provisionStatus === "Provisioned") {
                        return '<a href="#ajax/device_detail.html/' + o.id + '" title="Check the device detail">' + o.name + '</a>';
                    } else {
                        return o.name;
                    }
                    // return '<a href="#ajax/device_detail.html/' + o.cwmpId + '">' + o.cwmpId + '</a>';
                    // return '<a href="#ajax/domain_list.html/devices/' + o['id'] + '">' + n + '</a>';
                }
            },
            //column3
            {
                title: 'Image',
                sortable: false,
                callback: function(o) {
                    return '<img src="'+o.deviceTypeIconUrl+'"" style="height:30px; margin-top:-6px; margin-bottom:-6px;" />';
                }
            },
            {
                title: 'ns:Device.Title.DeviceType',
                cellClassName: 'deviceTypeName',
                filterable: true,
                searchName: 'deviceTypeName',
				searchDom : function(){
					var options = '<option value selected="selected">'+i18n.t("ns:Device.Title.DeviceType")+'</option>';
					$.ajax({
						url: $.config.server_rest_url+'/deviceTypes',
						dataType: 'JSON',
						async: false,
						success: function(deviceTypeCollection){
							for(var i=0;i<deviceTypeCollection.length-1;i++){
								options+='<option>'+deviceTypeCollection[i].name+'</option>';
							}
						}
					});
					return '<select class="form-control" name="deviceType" placeholder="'+i18n.t("ns:Device.Title.DeviceType")+'">'+options+'</select>';
				},
				sortable: false,
                callback: function(o) {
                    return o.deviceTypeName;
                }
            },
            //Column4
            {
                title: 'ns:Device.Title.OperatingSystem',
                //property: 'operatingSystemName',
                filterable: false,
				searchName: 'operatingSystemName',
				// searchDom : function(){
				// 	var options = '<option value selected="selected">'+i18n.t("ns:DataTables.Search")+' '+i18n.t("ns:Device.Title.OperatingSystem")+'</option>'
				// 		+'<option>Android</option>'
				// 		+'<option>Windows</option>';
				// 	return '<select class="form-control" name="deviceType" placeholder="'+i18n.t("ns:DataTables.Search")+' '+i18n.t("ns:Device.Title.OperatingSystem")+'">'+options+'</select>';
				// },
                sortable: false,
                width: '200px',
				callback: function(o) {
					var os = (typeof(o.operatingSystemName) === "undefined") ? '' : o.operatingSystemName;
					var displayHtml = '';
					if(os==="Android"){
						displayHtml = '<i class="fa fa-android"></i> Android';
					}else if(os==="iOS"){
						displayHtml = '<i class="fa fa-apple"></i> iOS';
					}else if(os==="Windows"){
						displayHtml = '<i class="fa fa-windows"></i> Windows';
					}else{
						displayHtml = '<i class="fa fa-question"></i> Unknown Operating System';
					}
					return displayHtml;
				}
            },
            //Column5
            /*
            {
                title: 'Firmware Language',
                property: 'firmwareLanguageId',
                filterable: false,
                sortable: false,
                callback: function(o) {
                    if ($.config.firmware_lang[o.firmwareLanguageId] == "zh_TW")
                        return "<img src='img/flags/tw.png'>";
                    return $.config.firmware_lang[o.firmwareLanguageId];
                }
            },
            */
            //column6
            {
                title: 'ns:Device.Title.ConnectionStatus',
                property: 'latestConnectTime',
                cellClassName: 'latestConnectTime',
                filterable: false,
                sortable: true,
				callback: function(o) {
					/*change millisecond to date format*/
					var isOnlineStatus = "--";
					if (!_.isEmpty(o.latestConnectTime)) {
						isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:gray;\" title=\""+i18n.t("ns:Message.Device.Offline")+"\"></i>";
						if (!_.isUndefined(o.isOnline)) {
							if(o.isOnline){
								isOnlineStatus = "<i class=\"fa fa-circle\" style=\"color:lawngreen;\" title=\""+i18n.t("ns:Message.Device.Online")+"\"></i>";
							}
						}
					}
					return isOnlineStatus+o.latestConnectTime;
				},
            },
            // column7
            /* {
                title: 'Device Connection Interval',
                callback: function(o) {
                    // var sum = o.periodicInformInterval;
                    // var hour = sum / 3600000;
                    // var temp = sum % 3600000;
                    // var min = temp / 60000;
                    // temp = temp % 60000;
                    // var sec = temp / 1000;
                    // hour = hour.toFixed(0);
                    // min = min.toFixed(0);
                    // sec = sec.toFixed(0);
                    // if (hour < 10) {
                        // hour.toString();
                        // hour = "0" + hour;
                    // }
                    // if (min < 10) {
                        // min.toString();
                        // min = "0" + min;
                    // }
                    // if (sec < 10) {
                        // sec.toString();
                        // sec = "0" + sec;
                    // }
                    // return (hour + ":" + min + ":" + sec);
                    return o.periodicInformInterval;
                },
                filterable: false,
                sortable: true
            }, */
            //column8
            {
                title: 'ns:Device.Title.Domain',
                property: 'domainId',
                filterable: false,
                sortable: false,
                visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
                callback: function(o) {
                    //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId] ;
					return (typeof(o.domainName) === "undefined") ? '' : o.domainName;
                }
            }
            ,
            {
                title: 'ns:Device.Title.Action',
				operator: true,
				sortable: false,
				width: '100px',
                callback: function(o) {
                    var _html = '';
                    //_html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                    _html += '<a href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                    // _html += '<a href="#" class="btn btn-mini btn-warning removeOne" title="Unlock" data-id="' + o[idAttribute] + '"><i class="icon-unlock"></i> </a>';
                    return _html;
                }
            }
        ];
        /* define namespace ends */


        var hash = window.location.hash;
        var groupDevice = hash.search(/^#ajax\/device_list.html\/group_device_list\/(\w+)$/);
        var domainDevice = hash.search(/^#ajax\/device_list.html\/domain_device_list\/(\w+)$/);
        var url;
		var breadcrumb;
        console.log("[groupDevice]:" + groupDevice);
        console.log("[domainDevice]:" + domainDevice);
        AD.isAdd = false;

        // Display the list of group members
        /* Group Device */
		var urlPathArray = hash.split("\/");
        var referer = ((typeof urlPathArray[2] != "undefined") ? urlPathArray[2] : "" ) || "";
		//if (groupDevice == 0) {
		if(referer == "group_device_list") {
			//var p = $("#left-panel nav a[href='ajax/device_list.html']").parents("li");
			//$(p[0]).removeClass("active");
			//$(p[1]).find("ul").show();
			//$(p[1]).addClass("active open");
		
            console.log('Get group device list...');
			//$("#widget-grid .cctech-gv a[href*='delete']").hide();
            //var id = hash.replace(/^#ajax\/device_list.html\/group_device_list\/(\w+)$/, "$1");
			var id = ((typeof urlPathArray[3] != "undefined") ? urlPathArray[3] : "" ) || "";
			var detail = ((typeof urlPathArray[4] != "undefined") ? urlPathArray[4] : "" ) || "";
			url = $.config.server_rest_url + '/deviceGroups/' + id + '/devices';
			if(detail=="capability"){
				var item = ((typeof urlPathArray[5] != "undefined") ? urlPathArray[5] : "" ) || "";
				url += '/capability/'+item;
			}
            // Modify it's titile
            // var ps = $('h1').get(0);
            // ps.innerText = 'Group Device';
            // Domain column will no longer be used
            console.log(AD.columns);
            AD.columns = $.grep(AD.columns, function(e) {
                return (e.property !== 'domainId' && !_.has(e, "operator"));
            });
			var removeMemberCol = {
                title: 'ns:Device.Title.Action',
				operator: true,
				// sortable: true,
                sortable: false,
				width: '100px',
                callback: function(o) {
                    var _html = '';
                    //_html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                    _html += '<a href="#' + AD.page + '/removeFromGroup/' + id + '/devices/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                    // _html += '<a href="#" class="btn btn-mini btn-warning removeOne" title="Unlock" data-id="' + o[idAttribute] + '"><i class="icon-unlock"></i> </a>';
                    return _html;
                }
            };
			AD.columns.push(removeMemberCol);
            console.log(AD.columns);
            // Default domain can't edit it's device list
            if (id != 1) {
                AD.buttons = [
                    //button
                    {
                        "sExtends": "text",
                        "sButtonText" : "<i class='fa fa-edit'></i>&nbsp; " + i18n.translate("ns:Group.Edit"),
                        "sButtonClass": "edit-group-device-list-btn txt-color-white bg-color-blue"
                    }
                ];
            }
			var groupName = '';
            $.ajax({
                type: "GET",
                async: false,
                url: $.config.server_rest_url + '/deviceGroups/' + id,
                success: function(data) {
                    groupName = data.name;
                },
                error: function(data) {
                    console.log('Get Error');
                    // alert('Get Group Name Failed!!');
                    return;  
                },
            });            

			breadcrumb = "<font data-i18n=\"ns:Menu.Device\">Device Management</font> > " + "<a href=\"#ajax/group_list.html\">"
				+"<font data-i18n=\"ns:Menu.Groups\">Groups</font>"
			+"</a>&nbsp;&gt;&nbsp;"
			+"<font>"+groupName+"</font>"
			+"&nbsp;&gt;&nbsp;"
			+"<a href=\"#"+AD.page+"\">"
				+"<font style=\"color:white\" data-i18n=\"ns:Menu.Devices\">Devices</font>"
			+"</a>";
        }
        // Display the list of domain devices
        /* Domain Device */
        //else if (domainDevice == 0) {
		else if(referer == "domain_device_list") {
            console.log('Get domain device list...');
            var id = hash.replace(/^#ajax\/device_list.html\/domain_device_list\/(\w+)$/, "$1");
            url = $.config.server_rest_url + '/domains/' + id + '/devices';
            // Modify it's titile
            // var ps = $('div > div > h1 > span > font');
            // ps.text('Domain Device');
            // console.log('ps is');
            // console.log(ps);
            // Domain column will no longer be used
            console.log(AD.columns);
            AD.columns = $.grep(AD.columns, function(e) {
                return (e.property !== 'domainId' && !_.has(e, "operator"));
            });
            console.log(AD.columns);
            // Default domain can't edit it's device list
            if (id != 1) {
                AD.buttons = [
                    //button
                    {
                        "sExtends": "text",
                        "sButtonText" : "<i class='fa fa-edit'></i>&nbsp; " + i18n.translate("ns:Domain.Edit"),
                        "sButtonClass": "edit-domain-device-list-btn txt-color-white bg-color-blue"
                    }
                ];
                var removeMemberCol = {
                    title: 'ns:Device.Title.Action',
                    operator: true,
                    // sortable: true,
                    sortable: false,
                    width: '100px',
                    callback: function(o) {
                        var _html = '';
                        //_html += '<a href="#' + AD.page + '/edit/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";
                        _html += '<a href="#' + AD.page + '/removeFromDomain/' + id + '/devices/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                        // _html += '<a href="#" class="btn btn-mini btn-warning removeOne" title="Unlock" data-id="' + o[idAttribute] + '"><i class="icon-unlock"></i> </a>';
                        return _html;
                    }
                };
                AD.columns.push(removeMemberCol);                
            }

            var domainName = '';
            $.ajax({
                type: "GET",
                async: false,
                url: $.config.server_rest_url + '/domains/' + id,
                success: function(data) {
                    domainName = data.name;
                },
                error: function(data) {
                    console.log('Get Error');
                    // alert('Get Domain Name Failed!!');
                    return;  
                },
            });         

			breadcrumb = "<a href=\"#ajax/domain_list.html\">"
				+"<font data-i18n=\"ns:Menu.Domain\">Domain</font>"
			+"</a>&nbsp;&gt;&nbsp;"
			+"<font>"+domainName+"</font>"
			+"&nbsp;&gt;&nbsp;"
			+"<a href=\"#"+AD.page+"\">"
				+"<font style=\"color:white\" data-i18n=\"ns:Menu.Devices\">Devices</font>"
			+"</a>";
        }
        else if(referer == "group_device_list_edit") {
            console.log('Get group device list...');
            var id = hash.replace(/^#ajax\/device_list.html\/group_device_list_edit\/(\w+)$/, "$1");
            // url = $.config.server_rest_url + '/domains/' + id + '/jointDevices';
            // console.log('url is ');
            // console.log(url);
            // Modify it's titile
            // var ps = $('div > div > h1 > span > font');
            // ps.text('Domain Device');
            // console.log('ps is');
            // console.log(ps);
            
            // Domain column will no longer be used
            console.log(AD.columns);
            AD.columns[1].visible=true;
            AD.columns = $.grep(AD.columns, function(e) {
                return (e.property !== 'domainId' && !_.has(e, "operator"));
            });
            console.log(AD.columns);
            // Default domain can't edit it's device list

            AD.buttons = [
                //button
                {
                    "sExtends": "text",
                    "sButtonText" : "<i class='fa fa-save'></i>&nbsp; " + i18n.translate("ns:Message.Save"),
                    "sButtonClass": "save-group-device-list-btn txt-color-white bg-color-blue"
                }
            ];

            var groupName = '';
            var operatingSystem = '';
            $.ajax({
                type: "GET",
                async: false,
                url: $.config.server_rest_url + '/deviceGroups/' + id,
                success: function(data) {
                    domainId = data.domainId;
                    groupName = data.name;                    
                    operatingSystem = data.operatingSystem;
                    groupDeviceList = data.deviceIdSet;
                    // url = $.config.server_rest_url + '/domains/' + domainId + '/devices?operatingSystemName=' + operatingSystem;
                    url = $.config.server_rest_url + '/devices?search=operatingSystemName:' + operatingSystem;
                    console.log('url is ');
                    console.log(url);
                },
                error: function(data) {
                    console.log('Get Error');
                    // alert('Get Domain Name Failed!!');
                    return;  
                },
            });         

            breadcrumb = "<font data-i18n=\"ns:Menu.Device\">Device Management</font>&nbsp;&gt;&nbsp;"
            +"<a href=\"#ajax/group_list.html\">"
            +"<font data-i18n=\"ns:Menu.Groups\">Groups</font>"
            +"</a>&nbsp;&gt;&nbsp;"
            +"<font>"+groupName+"</font>"
            +"&nbsp;&gt;&nbsp;"
            +"<font style=\"color:white\" data-i18n=\"ns:Group.Edit\">Edit</font>";
        }        
        else if(referer == "domain_device_list_edit") {
            console.log('Get domain device list...');
            var id = hash.replace(/^#ajax\/device_list.html\/domain_device_list_edit\/(\w+)$/, "$1");
            url = $.config.server_rest_url + '/domains/' + id + '/jointDevices';
            console.log('url is ');
            console.log(url);
            // Modify it's titile
            // var ps = $('div > div > h1 > span > font');
            // ps.text('Domain Device');
            // console.log('ps is');
            // console.log(ps);
            
            // Domain column will no longer be used
            console.log(AD.columns);
            AD.columns[0].visible=true;
            AD.columns = $.grep(AD.columns, function(e) {
                return (e.property !== 'domainId' && !_.has(e, "operator"));
            });
            console.log(AD.columns);
            // Default domain can't edit it's device list

            AD.buttons = [
                //button
                {
                    "sExtends": "text",
                    "sButtonText" : "<i class='fa fa-save'></i>&nbsp; " + i18n.translate("ns:Message.Save"),
                    "sButtonClass": "save-domain-device-list-btn txt-color-white bg-color-blue"
                }
            ];

            var domainName = '';
            $.ajax({
                type: "GET",
                async: false,
                url: $.config.server_rest_url + '/domains/' + id,
                success: function(data) {
                    domainName = data.name;
                },
                error: function(data) {
                    console.log('Get Error');
                    // alert('Get Domain Name Failed!!');
                    return;  
                },
            });         

            breadcrumb = "<a href=\"#ajax/domain_list.html\">"
                +"<font data-i18n=\"ns:Menu.Domain\">Domain</font>"
            +"</a>&nbsp;&gt;&nbsp;"
            +"<font>"+domainName+"</font>"
            +"&nbsp;&gt;&nbsp;"
            +"<a href=\"#"+AD.page+"\">"
                +"<font style=\"color:white\" data-i18n=\"ns:Menu.Devices\">Devices</font>"
            +"</a>";
        }        
		else if(referer == "eventSetting_list") {
			console.log('Get event setting device list...');
			var id = ((typeof urlPathArray[3] != "undefined") ? urlPathArray[3] : "" ) || "";
			url = $.config.server_rest_url + '/deviceEventDefinitions/' + id + '/devices';
			AD.columns = $.grep(AD.columns, function(e) {
                return (e.property !== 'domainId' && !_.has(e, "operator"));
            });
			console.log(AD.columns);
			AD.buttons = [];
		}
		else if(referer == "alertSetting_list") {
			console.log('Get alert setting device list...');
			var id = ((typeof urlPathArray[3] != "undefined") ? urlPathArray[3] : "" ) || "";
			url = $.config.server_rest_url + '/alertDefinitions/' + id + '/devices';
			AD.columns = $.grep(AD.columns, function(e) {
                return (e.property !== 'domainId' && !_.has(e, "operator"));
            });
			console.log(AD.columns);
			AD.buttons = [];
		}
        /* Device */
        else {
			//var p = $("#left-panel nav a[href='ajax/device_list.html']").parents("li");
			//$(p[0]).addClass("active");
			//$(p[1]).find("ul").show();
			//$(p[1]).addClass("active open");
            if ($.login.user.checkRole('DeviceOperator'))			
                AD.isAdd = true;
            else
                AD.columns = $.grep(AD.columns, function(e) {
                    return (!_.has(e, "operator"));
                });                
            url = $.config.server_rest_url + '/devices';
            
			/*
			breadcrumb = "<a href=\"#"+AD.page+"\">"
				+"<font style=\"color:white\" data-i18n=\"ns:Menu.Devices\">Devices</font>"
			+"</a>";
			*/
			breadcrumb = "<font data-i18n=\"ns:Menu.Device\">Device Management</font> > " + "<font style=\"color:white\" data-i18n=\"ns:Menu.Devices\">Devices</font>";
        }
		$("#breadcrumbs").html(breadcrumb);
        console.log('url is ' + url);

        var idAttribute = AD.idAttribute = "id";
        AD.title = "ns:Menu.DeviceList";

        //AD.api = 'cctech';

        AD.csv = {};
        AD.csv.forms = [
            //form
            {
                title: 'Choose File',
                // property: 'name',
                default_value: "",
                view: function() {
					//console.log("[AddCSV] Prepare csv fileupload component.");
                    //宣告upload button
                    var csvButton = $('<button/>')
                        //增加button的class讓呈現方式改變
                        .addClass('btn btn-primary')
                        //把upload button 的disable prop設為true目的是讓他無法選取
                        .prop('disabled', true)
                        // .text('Processing...')
                        .text(i18n.t("ns:Message.Processing"))
                        //設定 upload button 的click event
                        .on('click', function(event) {
                            event.preventDefault();
                            var $this = $(this),
                                //取值, 即檔案
                                data = $this.data();

                            $this
                                .off('click')
                                .text('Abort')
                                .on('click', function() {
                                    //把upload button移除
                                    $this.remove();
                                    data.abort();
                                });
                            data.submit().always(function() {
                                $this.remove();
                            });
                        });

                    //下面是利用handlebars來把Add File的按鈕裝上template
                    // var uploadButton = $('<button/>');
                    var chooseFile = {
                        title: "Add CSV..."
                    };
                    var chooseButton = $('#btnChooseFile').html();
                    var template = Handlebars.compile(chooseButton);
                    var html = template(chooseFile); //String

                    /*
                    $.ajax({
                        url: $.config.server_util_url +'/import',
                        type: 'POST',
                        async: false,
                        data: {
                            property: "Attachmentcsv"
                        },
                        success: function(data) {
                            // alert("i am success!");
                        }
                    });
                    */

                    $html = $(html); //according to the html string, it will generate a "jquery" object that copy the string from var html

                    $html.find("#fileupload").fileupload({
                        url: $.config.server_util_url + '/import?type=csv',
                        //url: 'http://140.92.60.113:8080/AMDM' +'/import?type=csv',
                        dataType: 'json',
                        //是否選擇完檔案立即上傳
                        autoUpload: false,
                        //可以接受的格式, 可直接用|另外增加類型
                        acceptFileTypes: /(\.|\/)(csv)$/i,
                        //超過會顯示file too large
                        maxFileSize: 5000000, // 5 MB
                        // Enable image resizing, except for Android and Opera,
                        // which actually support image resizing, but fail to
                        // send Blob objects via XHR requests:
                        disableImageResize: /Android(?!.*Chrome)|Opera/
                            .test(window.navigator.userAgent),
                        previewMaxWidth: 100,
                        previewMaxHeight: 100,
                        //是否讓縮圖點擊後另開頁面顯示原圖
                        previewCrop: true,
                        //檔案load之後把檔案名稱顯示出來
                        forceIframeTransport: true
                    }).on('fileuploadadd', function(e, data) {
						$("#files").empty();
                        data.context = $('<div/>').appendTo('#files');
                        $.each(data.files, function(index, file) {
                            var node = $('<p/>')
                                .append($('<span/>').text(file.name));
                            if (!index) {
                                node
                                    .append('<br>')
                                    .append(csvButton.clone(true).data(data));
                            }
                            node.appendTo(data.context);
                        });
                    }).on('fileuploadprocessalways', function(e, data) {
                        var index = data.index,
                            file = data.files[index],
                            node = $(data.context.children()[index]);
                        if (file.preview) {
                            node
                                .prepend('<br>')
                                .prepend(file.preview);
                        }
                        //如果檔案error, 把錯誤的資訊顯示出來
                        if (file.error) {
                            node
                                .append('<br>')
                                .append($('<span class="text-danger"/>').text(file.error));
                        }
                        //產生
                        if (index + 1 === data.files.length) {
                            data.context.find('button')
                                .text('Upload')
                                .prop('disabled', !!data.files.error);
                        }
                    }).on('fileuploadprogressall', function(e, data) {
                        var progress = parseInt(data.loaded / data.total * 100, 10);
                        $('#progress .progress-bar').css(
                            'width',
                            progress + '%'
                        );
                    }).on('fileuploaddone', function(e, data) {
                        $(data.context.children().children()[2]).text("Done");
						$("#wid-id-1 .smart-form footer button").text("Done");
                        /*
                        $.each(data.result.files, function(index, file) {
                            if (file.url) {
                                var link = $('<a>')
                                    .attr('target', '_blank')
                                    .prop('href', file.url);
                                $(data.context.children()[index])
                                    .wrap(link);
                            } else if (file.error) {
                                var error = $('<span class="text-danger"/>').text(file.error);
                                $(data.context.children()[index])
                                    .append('<br>')
                                    .append(error);
                            }
                        });
                        */
                    }).on('fileuploadfail', function(e, data) {
                        $.each(data.files, function(index, file) {
                            var error = $('<span class="text-danger"/>').text('File upload failed.');
                            $(data.context.children()[index])
                                .append('<br>')
                                .append(error);
                        });
						$("#wid-id-1 .smart-form footer button").text("Close");
                    }).prop('disabled', !$.support.fileInput)
                        .parent().addClass($.support.fileInput ? undefined : 'disabled');

					return $html;
                },
                value: function() {
                    var name;
                    var path;
                    $.ajax({
                        url: $.config.server_util_url + '/import',
                        //url: 'http://140.92.60.113:8080/AMDM' +'/import',
                        type: 'POST',
                        async: false,
                        data: {
                            type: "csv",
                            mode: "doSave"
                        },
                        success: function(data) {
                            path = data.fileServerPath;
                            name = data.name;
                        }
                    });
                    var ret = {
                        "name": name,
                        "fileServerPath": path
                    };
                    return ret;
                }

            }
        ];
        AD.summary = {};
        AD.summary.forms = [];
        AD.edit = {};
        AD.edit.forms = [ //forms
            {
                'class': 'col col-6 input state-disabled',
                disabled: 'disabled',
                title: 'Serial Number (Unalterable)',
                property: 'cwmpId',
                default_value: ""
            },
            // "row",
            //form
            {
                // 'class': 'col col-2',
                title: 'Device Type',
                property: 'deviceTypeId',
                default_value: ""
            },
            // "fieldset",
            //form
            {
                // 'class': 'col col-2',
                title: 'Firmware Language',
                property: 'firmwareLanguageId',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Domain',
                property: 'domainId',
                default_value: ""
            },
            // "row",
            //form
            {
                // 'class': 'col col-2',
                title: 'Country',
                property: 'country',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Clinic Name',
                property: 'clinicName',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Clinic Location',
                property: 'clinicLocation',
                default_value: ""
            }
        ];


        AD.forms = [
            //forms
            {
                // 'class': 'col col-2 ',
                title: 'Serial Number',
                property: 'cwmpId',
                default_value: ""
            },
            // "row",
            //form
            {
                // 'class': 'col col-2',
                title: 'Device Type',
                property: 'deviceTypeId',
                default_value: ""
            },
            // "fieldset",
            //form
            {
                // 'class': 'col col-2',
                title: 'Firmware Language',
                property: 'firmwareLanguageId',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Domain',
                property: 'domainId',
                default_value: ""
            },
            // "row",
            //form
            {
                // 'class': 'col col-2',
                title: 'Country',
                property: 'country',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Clinic Name',
                property: 'clinicName',
                default_value: ""
            },
            //form
            {
                // 'class': 'col col-2',
                title: 'Clinic Location',
                property: 'clinicLocation',
                default_value: ""
            }
        ];



        /* model starts */

        AD.csv.Model = Backbone.Model.extend({
            urlRoot: $.config.server_util_url + '/import',
            idAttribute: "id"
        });

        AD.Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        /* model ends */

        /* collection starts */
        AD.Collection = Backbone.Collection.extend({
            url: url,
            model: AD.Model
        });

        /* collection ends */

    };


    var routes = {};
	routes[AD.page + '/eventSetting_list/:_id'] = 'show_grid';
	routes[AD.page + '/alertSetting_list/:_id'] = 'show_grid';
    routes[AD.page + '/group_device_list/:_id'] = 'show_grid';
    routes[AD.page + '/domain_device_list/:_id'] = 'show_grid';
    routes[AD.page + '/domain_device_list_edit/:_id'] = 'show_grid';
    routes[AD.page + '/group_device_list_edit/:_id'] = 'show_grid';
    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/summary/:_id'] = 'show_summary';
    routes[AD.page + '/edit/:_id'] = 'show_edit';
    routes[AD.page + '/add'] = 'show_add';
    routes[AD.page + '/addCsv'] = 'show_addCsv';
    routes[AD.page + '/delete/:_id'] = 'delete_single';
	routes[AD.page + '/removeFromGroup/:group_id/devices/:deviceId'] = 'removeFromGroup';
    routes[AD.page + '/removeFromDomain/:domain_id/devices/:deviceId'] = 'removeFromDomain';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        initialize: function(opt) {
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            // console.error();
        },
        show_summary: function() {

            var t = this;

            t.$fv.empty();
            t.$gv.hide();

            var m = new t.AD.Model();

            t.fv = new Backbone.FormView({
                el: t.$fv,
                Name: "Summary",
                model: new Backbone.Model(),
                forms: t.AD.summary.forms,
                lang: {
                    save: 'Save',
                    back: 'Back'
                }
            });

            t.fv.$el.unbind();
            t.fv.$el.on("click", '[type="submit"]', function(event) {
                t.submit(event, m);
            });
            t.$fv.show();

            pageSetUp();

        },
        show_add: function() {

            var t = this;

            t.$fv.empty();
            t.$gv.hide();

            var m = new t.AD.Model();

            t.fv = new Backbone.FormView({
                el: t.$fv,
                Name: "Add",
                model: new Backbone.Model(),
                forms: t.AD.forms,
                lang: {
                    save: 'Save',
                    back: 'Back'
                }
            });

            t.fv.$el.unbind();
            t.fv.$el.on("click", '[type="submit"]', function(event) {
                t.submit(event, m);
            });
            t.$fv.show();

            pageSetUp();

        },
        show_addCsv: function(opt) {
            var t = this;
			$(".cctech-fv").empty();
            t.$fv.empty();
            t.$gv.hide();

            var m = new AD.csv.Model();

            t.fv = new Backbone.FormView({
                el: t.$fv,
                Name: "ns:Device.Actions.Add",
                model: new Backbone.Model(),
                forms: t.AD.csv.forms,
                button: [{
                    "type": "button",
                    "cls": "btn btn-default",
                    "fn": "window.history.back()",
                    "text": "Cancel"
                }]
            });

            t.fv.$el.unbind();
            t.fv.$el.on("click", '[type="submit"]', function(event) {
                var forms = t.AD.forms;
                t.AD.forms = t.AD.csv.forms;
                t.submit(event, m);
                t.AD.forms = forms;
            });
            t.$fv.show();
			var csvExampleHtml = "<div id=\"csvExample\" class=\"col col-lg-5 col-md-12 col-sm-12 hidden-xs\">"
				+"CSV Example:"
				+"<table class=\"table table-striped noselect\" style=\"\">"
					+"<tbody>"
						+"<tr><th>"+i18n.t("ns:Device.ExampleTitle.DeviceSerialNumber")+"</th><th>"+i18n.t("ns:Device.ExampleTitle.AgentAccount")+"</th><th>"+i18n.t("ns:Device.ExampleTitle.AgentPassword")+"</th><th>"+i18n.t("ns:Device.ExampleTitle.ServerAccount")+"</th><th>"+i18n.t("ns:Device.ExampleTitle.ServerPassword")+"</th><th>"+i18n.t("ns:Device.ExampleTitle.DeviceType")+"</th></tr>"
						+"<tr><td>DevSN001,</td><td>funambol,</td><td>funambol,</td><td>funambol,</td><td>srvpwd,</td><td>G0550</td></tr>"
						+"<tr><td>DevSN002,</td><td>funambol,</td><td>funambol,</td><td>funambol,</td><td>srvpwd,</td><td>G0550</td></tr>"
						+"<tr><td>DevSN003,</td><td>funambol,</td><td>funambol,</td><td>funambol,</td><td>srvpwd,</td><td>G0550</td></tr>"
						+"<tr><td>DevSN004,</td><td>funambol,</td><td>funambol,</td><td>funambol,</td><td>srvpwd,</td><td>G0550</td></tr>"
					+"</tbody>"
				+"</table>"
				+"<div class=\"pull-right\"><a href=\"data/devices.csv\" target=\"_blank\"><i class=\"fa fa-download fa-lg\"></i></a> Download Example</div>"
			+"</div>";
			$("#wid-id-1 section").after(csvExampleHtml);
            pageSetUp();
        },
        show_edit: function(id) {  
            var t = this;            
            t.navigate("#ajax/device_edit.html/" + id, {
                trigger: true
            });
        },
        show_grid: function() {
            var t = this;
            var urlPathArray = window.location.hash.split("\/");
            var referer = ((typeof urlPathArray[2] != "undefined") ? urlPathArray[2] : "" ) || "";
            // init GridView
            if (!t.gv) {
                t.gv = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: !_.isUndefined(t.AD.isAdd) ? t.AD.isAdd : true,
                    sort: t.AD.sort,
					order: [[1, "desc"]], //t.AD.order,
                    filter: {}, //t.AD.filter,
                    buttons: t.AD.buttons,
                    AD: t.AD
                });
                //Edit group device list
                t.$gv.on("click", ".edit-group-device-list-btn", function(event) {
                    event.preventDefault();
                    var hash = window.location.hash;
                    var id = hash.replace(/^#ajax\/device_list.html\/group_device_list\/(\w+)$/, "$1");
                    window.location.href = "#ajax/device_list.html/group_device_list_edit/" + id;
                    AD.bootstrap();
                    // alert(t.AD.addurl);
                    // var addurl = t.AD.addurl || "add";
                    // t.navigate("#ajax/group_edit.html/" + id, {
                    //     trigger: true
                    // });
                });

                //Save group device list
                // Add this referer to avoid binding this click event twice
                if (referer=='group_device_list_edit'){
                    t.$gv.on("click", ".save-group-device-list-btn", function(event) {
                        event.preventDefault();
                        var hash = window.location.hash;
                        //alert(hash);
                        var id = hash.replace(/^#ajax\/device_list.html\/group_device_list_edit\/(\w+)$/, "$1");
                        var url = $.config.server_rest_url + '/deviceGroups/' + id;

                        var data = {
                            additionDeviceIdSet: addDeviceList,
                            removeDeviceIdSet: removeDeviceList
                        };
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            headers: {
                                "X-HTTP-Method-Override": "PUT"
                            },
                            async: false,
                            url: url,
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify(data),
                            success: function(data) {
                                console.log('Save Success');
                                window.location.href = '#ajax/group_detail.html/' + id;
                                // window.history.back();
                                // console.log(data);
                            },
                            error: function(data) {
                                console.log('Save Error');
                                // alert('Save Failed!!');
                                // console.log(data); 
                            },
                        });                    
                    });                
                }

                //Edit domain device list
                t.$gv.on("click", ".edit-domain-device-list-btn", function(event) {
                    event.preventDefault();
                    var hash = window.location.hash;
                    var id = hash.replace(/^#ajax\/device_list.html\/domain_device_list\/(\w+)$/, "$1");
                    window.location.href = "#ajax/device_list.html/domain_device_list_edit/" + id;
                    // window.location.replace("#ajax/device_list.html/domain_device_list_edit/" + id);
                    AD.bootstrap();
                    // $(".cctech-gv table").dataTable().fnDraw();
                    // location.reload();
                    // t.navigate("#ajax/device_list.html/domain_device_list_edit/" + id, {
                    //     trigger: true
                    // });
                    // AD.bootstrap();
                });

                //Save domain device list
                // Add this referer to avoid binding this click event twice
                if (referer=='domain_device_list_edit'){
                    t.$gv.on("click", ".save-domain-device-list-btn", function(event) {
                        event.preventDefault();
                        var hash = window.location.hash;
                        console.log('hash is');
                        console.log(hash);
                        //alert(hash);
                        var id = hash.replace(/^#ajax\/device_list.html\/domain_device_list_edit\/(\w+)$/, "$1");
                        var url = $.config.server_rest_url + '/domains/' + id + '/devices';
                        console.log('url is');
                        console.log(url);

                        var data = {
                            additionDeviceIdSet: addDeviceList,
                            removeDeviceIdSet: removeDeviceList
                        };
                        $.ajax({
                            type: 'POST',
                            dataType: 'json',
                            headers: {
                                "X-HTTP-Method-Override": "PUT"
                            },
                            async: false,
                            url: url,
                            contentType: 'application/json; charset=utf-8',
                            data: JSON.stringify(data),
                            success: function(data) {
                                console.log('Save Success');
                                window.location.href = '#ajax/domain_list.html';
                                // t.navigate("#ajax/domain_list.html", {
                                //     trigger: true
                                // });
                                // window.history.back();
                                // console.log(data);
                            },
                            error: function(data) {
                                console.log('Save Error');
                                // alert('Save Failed!!');
                                // console.log(data); 
                            },
                        });                    
                    });
                }

                //新增單筆device
                t.$gv.on("click", ".upload-btn", function(event) {
                    event.preventDefault();

                    //alert("upload-btn");

                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    // alert(t.AD.addurl);
                    // var addurl = t.AD.addurl || "add";
                    t.navigate("#" + t.AD.page + "/" + "add", {
                        trigger: true
                    });

                });
                //新增多筆device
                t.$gv.on("click", ".add-btn", function(event) {
                    event.preventDefault();

                    //alert("upload-btn-csv");

                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    // alert(t.AD.addurl);
                    // var addurl = t.AD.addurl || "add";
                    //這邊要做csv上傳的連結
                    //t.navigate("#" + t.AD.page + "/" + "addCsv", {
					t.navigate("#ajax/device_import.html", {
                        trigger: true
                    });

                });
            } else
                t.gv.refresh();


            t.$fv.hide();
            t.$gv.show();

            pageSetUp();

            // setTimeout(runDataTables, 500);
        },
		delete_single: function(id){
			var dmURL = $.config.server_rest_url + '/devices/';
			$.SmartMessageBox({
                // title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; Delete Device <span class='txt-color-orangeDark'><strong> &nbsp;" +
                //     id + "</strong></span> ?",
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+ i18n.t("ns:Message.Device.DeleteTheDevice") +" <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                //content: "Delete the device ?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> "+ i18n.t("ns:Message.Device.DeleteDeviceContent") +"</span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + id,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.DeviceDelete") + i18n.t("ns:Message.Device.DeleteSuccess"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Device.DeleteDevice")+": &nbsp;" + id + "</i>",
                            // title: "[Device Delete] Deleted successfully",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Device : &nbsp;" + id + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.DeviceDelete") + i18n.t("ns:Message.Device.DeleteFailed"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Device.DeleteDevice")+": &nbsp;" + id + "</i>",
                            // title: "[Device Delete] Deleted failed",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Device : &nbsp;" + id + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/device_list.html";
                    });
                } else {
                    window.location.href = "#ajax/device_list.html";
                }
            });
		},
		removeFromGroup: function(groupId,deviceId){
			var dmURL = $.config.server_rest_url + '/deviceGroups/'+groupId+'/devices/'+deviceId;
			$.SmartMessageBox({
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+ i18n.t("ns:Message.Device.RemoveGroupMember") +" <span class='txt-color-orangeDark'><strong> &nbsp;" +
                    deviceId + "</strong></span> ?",
                content: i18n.t("ns:Message.Device.RemoveDeviceContent"),
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.RemoveGroupMember") + i18n.t("ns:Message.Device.RemoveSuccess"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.RemoveDevice") + "&nbsp;" + deviceId + "</i>",
                            // title: "[Remove Group Member] Removed successfully",
                            // content: "<i class='fa fa-times'></i> <i>Remove Device : &nbsp;" + deviceId + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.RemoveGroupMember") + i18n.t("ns:Message.Device.RemoveFailed"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.RemoveDevice") + "&nbsp;" + deviceId + "</i>",
                            // title: "[Remove Group Member]] Remove failed",
                            // content: "<i class='fa fa-times'></i> <i>Remove Device : &nbsp;" + deviceId + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        //window.location.href = "#ajax/device_list.html";
						window.history.back();
                    });
                }else{
					window.history.back();
				}
            });
		},
        removeFromDomain: function(domainId,deviceId){
            var dmURL = $.config.server_rest_url + '/domains/'+domainId+'/devices/'+deviceId;
            $.SmartMessageBox({
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Device.RemoveDomainMember")+" <span class='txt-color-orangeDark'><strong> &nbsp;" +
                    deviceId + "</strong></span> ?",
                content: i18n.t("ns:Message.Device.RemoveDeviceContent"),
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.RemoveGroupMember") + i18n.t("ns:Message.Device.RemoveSuccess"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.RemoveDevice") + "&nbsp;" + deviceId + "</i>",
                            // title: "[Remove Group Member] Removed successfully",
                            // content: "<i class='fa fa-times'></i> <i>Remove Device : &nbsp;" + deviceId + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Device.RemoveGroupMember") + i18n.t("ns:Message.Device.RemoveFailed"),
                            content: "<i class='fa fa-times'></i> <i>" + i18n.t("ns:Message.Device.RemoveDevice") + "&nbsp;" + deviceId + "</i>",
                            // title: "[Remove Group Member]] Remove failed",
                            // content: "<i class='fa fa-times'></i> <i>Remove Device : &nbsp;" + deviceId + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        //window.location.href = "#ajax/device_list.html";
						window.history.back();
                    });
                }else{
					window.history.back();
				}
            });
        }        

    });

    $('body').on('click', '.editCheckbox', function (event) {
        if ($(this).is(':checked')) {
            console.log('click');
            if ($.inArray(parseInt(this.value),removeDeviceList) > -1){
                removeDeviceList.splice($.inArray(parseInt(this.value),removeDeviceList),1);
            }
            else
            {
                addDeviceList.push(parseInt(this.value));
            }
        } else {
            console.log('unclick');
            if ($.inArray(parseInt(this.value),addDeviceList) > -1){
                addDeviceList.splice($.inArray(parseInt(this.value),addDeviceList),1);
            }
            else
            {
                removeDeviceList.push(parseInt(this.value));
            }
        }
        console.log('addDeviceList is');
        console.log(addDeviceList);
        console.log('removeDeviceList is');
        console.log(removeDeviceList);
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        if (!$.login.user.checkRole('DeviceViewer')) {
            window.location.href = "#ajax/dashboard.html/grid";
        }

        adReload();

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
		$("body").removeClass("loading");

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    // $[AD.page].bootstrap();

})(jQuery);
