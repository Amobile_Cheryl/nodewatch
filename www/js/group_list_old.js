(function($) {

    /* define namespace starts */
    var AD = {};
    AD.collections = {};   
    var url = $.config.server_rest_url + "/deviceGroups";
    var idAttribute = AD.idAttribute = "id";
    AD.title = "ns:Menu.GroupList";
    AD.page = "ajax/group_list.html";

    AD.isAdd = false;
    if ($.login.user.checkRole('DeviceGroupOperator'))
    {
        AD.buttons = [
            {
                "sExtends": "text",
                "sButtonText" : "<i class='fa fa-plus'></i>&nbsp; Create Group",
                "sButtonClass": "btn-createGroup txt-color-white bg-color-blue"
            },
        ];
    }

    AD.validate = function() {
        return true;
    };    
    
    /*
    Group id    groupid
    Group Name  groupname
    members     members
    Reseller    domainName
    Remark      remark
     */
    AD.columns = [
		//columns CheckBox
        /*
		{
            title: '<input type="checkbox" value="all"></input>',
            cellClassName: 'DeleteItem',
            width: '20px',
            callback: function(o) {
                return '<input id="checkbox_' + o[idAttribute] + '" type="checkbox" value="' + o[idAttribute] + '"></input>';
            }
        },
		*/
        //columns Group Name
        {
            title: 'ns:Group.Title.GroupName', 
            property: 'name',
            // cellClassName: 'groupName',
            filterable: false,
            sortable: true,
            callback: function(o) {
                var groupName = o.name;
                //if (groupName.length > 18)
                //    return '<i class="fa fa-th-large"></i> <a title="' + groupName + '" href="#ajax/group_detail.html/' + o.id + '">'+ groupName.substr(0, 20) + ' ...</a>';
                //else
                    return '<i class="fa fa-th-large"></i> <a href="#ajax/group_detail.html/' + o.id + '">'+ groupName + '</a>';
            }
        },
        //columns Members
        {
            title: 'ns:Group.Title.Members',
            property: 'deviceIdSet',
            // cellClassName: 'members',
            filterable: false,
            sortable: false,
            // width: '150px',
            callback: function(o) {
                //return '<a href="#ajax/group_member_list.html/' + o.id + '">' +  "<img src='img/group/group-icon-3.png'>" + ' ' + ((typeof o.deviceIdSet === "undefined"|| o.deviceIdSet === null || o.deviceIdSet === 0) ? '0' : o.deviceIdSet.length) + '</a>';
                //return '<a href="#ajax/device_list.html/group_device_list/' + o.id + '">' +  "<img src='img/group/group-32-0.png'>" + ((typeof o.deviceIdSet === "undefined"|| o.deviceIdSet === null || o.deviceIdSet === 0) ? '0' : o.deviceIdSet.length) + '</a>';
                return o.deviceIdSet.length;
            }
        },
        //columns Operating System
        {
                title: 'ns:Group.Title.OperatingSystem',
                //property: 'operatingSystem',
                filterable: false,
                sortable: false,
                // width: '300px',
                callback: function(o) {
                    var os = (typeof(o.operatingSystem) === "undefined") ? '' : o.operatingSystem;                  
                    switch(os) {
                        case "Android": var displayHtml = '<i class="fa fa-android"></i>Android'; break;
                        case "iPhone":  var displayHtml = '<i class="fa fa-apple"></i>iPhone';    break;
                        case "Windows": var displayHtml = '<i class="fa fa-windows"></i>Windows'; break;
                        default:        var displayHtml = '<i class="fa fa-question"></i>Unknow Operating System'; break;
                    }
                    return displayHtml;
                }
        },
        //columns Domain
        {
            title: 'ns:Group.Title.Domain',
            property: 'domainName',
            cellClassName: 'domainName',
            filterable: false,
            sortable: false,
            visible: $.login.user.permissionClass.value>=$.common.PermissionClass.SystemOwner.value,
            callback: function(o) {
                //return (typeof($.config.Domain[o.domainId]) === "undefined") ? 'Unknow' : $.config.Domain[o.domainId] ;
                return (typeof(o.domainName) === "undefined") ? '' : o.domainName;
            }
        },
        //columns Policy
        {
            title: 'Software Policy',
            property: 'devicePolicyName',
            // cellClassName: 'remark',
            filterable: false,
            sortable: false
        },
        //columns Remark
        {
            title: 'ns:Group.Title.Remark',
            property: 'remark',
            // cellClassName: 'remark',
            filterable: false,
            sortable: false
        },
        //columns Action
        {
            title: 'ns:Group.Title.Action',
            sortable: false,
            visible: $.login.user.checkRole('DeviceGroupOperator'),
            callback: function(o) {
                var _html = '';                
                if (o) {                    
                    // [Action] Edit the Group                    
                    //_html += '<a id="edit" href="#' + AD.page + '/edit/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-blue" style="display: inline;"><i class="fa fa-edit"></i></a>' + "&nbsp;";                    
                    // [Action] Lock the Group
                    // _html += '<a id="lock" href="#' + AD.page + '/lock/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-greenLight" style="display: inline;"><i class="fa fa-lock"></i></a>';
                    // [Action] Unlock the Group
                    // _html += '<a id="unlock" href="#' + AD.page + '/unlock/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-greenLight" style="display: inline;"><i class="fa fa-unlock"></i></a>' + "&nbsp;";
                    // [Action] Install the Group
                    // _html += '<a id="install" href="#' + AD.page + '/install/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-orange" style="display: inline;"><i class="fa fa-download"></i></a>' + "&nbsp;";
                    // [Action] Uninstall the Group
                    //_html += '<a id="unistall" href="#' + AD.page + '/uninstall/' + o.id + '" class="btn btn-default btn-sm DTTT_button_text txt-color-orange" style="display: inline;"><i class="fa fa-upload"></i></a>' + "&nbsp;";                                      
                    // [Action] Delete the Group
                    if ($.common.isDelete)
                        _html += '<a id="delete" href="#' + AD.page + '/delete/' + o[idAttribute] + '" class="btn btn-default btn-sm DTTT_button_text txt-color-white bg-color-red" style="display: inline;"><i class="fa fa-times"></i></a>';
                }
                return _html;
            }
        }        
    ];
    /* define namespace ends */

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    /* collection starts */

    AD.Collection = Backbone.Collection.extend({
        url: url,
        model: AD.Model
    });

    /* collection ends */
    var routes = {};
    routes[AD.page + '/grid']           = 'show_grid';              // Show the datatable
    //routes[AD.page + '/deleteGroup']    = 'btn_delete_group';     // [Button] Delete Group(s)
    routes[AD.page + '/edit/:_id']      = 'act_edit_group';         // [Action] Edit the Group 'show_edit'
    routes[AD.page + '/lock/:_id']      = 'act_lock_group';         // [Action] Lock the Group 'show_edit'
    routes[AD.page + '/unlock/:_id']    = 'act_unlock_group';       // [Action] Unlock the Group
    routes[AD.page + '/install/:_id']   = 'act_install_group';      // [Action] Install the Group 'show_edit'
    //routes[AD.page + '/uninstall/:_id'] = 'act_uninstall_group';  // [Action] Uninstall the Group    
    routes[AD.page + '/delete/:_id']    = 'act_delete_group';       // [Action] Delete the Group
    routes[AD.page] = 'render';

    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        initialize: function(opt) {
            var t = this;
            //JavaScript中使用super繼承的方式: 使用prototype. ... .call()
            Backbone.DG1.prototype.initialize.call(t, opt);
            t.$bt = $(opt.bts).first();
            console.error();
        },
        // Show the datatable
        show_grid: function() {
            var t = this;
            // init GridView
            
            if (!t.gv) {
                t.gv = new Backbone.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: !_.isUndefined(t.AD.isAdd) ? t.AD.isAdd : true,
                    sort: t.AD.sort,
                    filter: t.AD.filter,
                    buttons: t.AD.buttons,
                    AD: t.AD
                });
                
                // [Button] Create a Group
                t.$gv.on("click", ".btn-createGroup", function(event) {
                    event.preventDefault();

                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    t.navigate("#ajax/group_add.html", {
                        trigger: true
                    });
                    // $('#addToGroupDynamicDialog').dialog("close");
                });
                // [Button] Delete group
                // t.$gv.on("click", ".delete-btn", function(event) {
                //     event.preventDefault();                    

                //     var hash = window.location.hash;
                //     var url = location.hash.replace(/^#/, '');

                //     var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                //     url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                //     t.navigate("#" + t.AD.page + "/deleteGroup", {                        
                //         trigger: true
                //     });
                //     $('#addToGroupDynamicDialog').dialog("close");
                // });
            } else 
                t.gv.refresh();
            
            t.$fv.hide();
            t.$gv.show();

            pageSetUp();
        },                
        // [Button] Delete Group(s)
        btn_delete_group: function() {            
            
        },
        // [Action] Lock the Group
        act_lock_group: function(id) {                         
            var dmURL = $.config.server_rest_url + '/deviceGroups/';

            $.SmartMessageBox({
                title: "<i class='fa fa-lock txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Group.LockGroup")+" <span class='txt-color-orangeDark'><strong> &nbsp;" +
                    id + "</strong></span> ?",
                content: i18n.t("ns:Message.Group.LockGroupContent"),
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        type: 'POST',
                        url: dmURL + id + '/actions/lock',
                        dataType: 'json',
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.GroupLock") + i18n.t("ns:Message.Group.LockSuccess"),
                            content: "<i class='fa fa-lock'></i> <i>"+ i18n.t("ns:Message.Group.LockGroupID") + "&nbsp;" + id + "</i>",
                            // title: "[Group Lock] Locked successfully",
                            // content: "<i class='fa fa-lock'></i> <i>Locked Group ID: &nbsp;" + id + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-lock fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.GroupLock") + i18n.t("ns:Message.Group.LockFailed"),
                            content: "<i class='fa fa-lock'></i> <i>"+ i18n.t("ns:Message.Group.LockGroupID") + " &nbsp;" + id + "</i>",
                            // title: "[Group Lock] Locked failed",
                            // content: "<i class='fa fa-lock'></i> <i>Locked Group ID: &nbsp;" + id + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/group_list.html";
                    });
                } else {
                    window.location.href = "#ajax/group_list.html";
                }
            });
            // Dialog 
            // $('#dynamicGroupDialog').dialog({
            //     autoOpen : false,
            //     width : 400,
            //     resizable : false,
            //     modal : true,
            //     title : "<div class='widget-header'><h4><i class='fa fa-lock'></i> Lock the Group</h4></div>",
            //     buttons : [{
            //         id : "btn_cancel",
            //         html : "Cancel",
            //         "class" : "btn btn-default",
            //         click : function() {
            //             $["ajax/group_list.html"].app.navigate("#" + "ajax/group_list.html" + "/grid", {
            //                 trigger: true
            //             });
            //             $(this).dialog("close");
            //         }
            //     }, {
            //         id : "btn_lock",
            //         html : "<i class='fa fa-lock'></i>&nbsp; Lock",
            //         "class" : "btn btn-primary",
            //         click : function(event) {
            //             $.ajax({                
            //                 type: 'POST',
            //                 url: dmURL + id + '/actions/lock',
            //                 dataType: 'json',
            //                 timeout: 10000
            //             }).done(function(){
            //                 //動態產生lockDialogDiv                
            //                 var lock_group_dialog_html = Handlebars.compile($('#lockGroupDiv_Done').html());
            //                 $('#dynamicGroupDialog').html(lock_group_dialog_html).dialog('open');
            //             }).fail(function(){
            //                 //動態產生lockDialogDiv                
            //                 var lock_group_dialog_html = Handlebars.compile($('#lockGroupDiv_Fail').html());
            //                 $("#dynamicGroupDialog").html(lock_group_dialog_html).dialog('open');                           
            //             }).always(function() {
            //                 $('#btn_lock').remove();
            //                 $('#btn_cancel').addClass('btn-primary').html("<i class='fa fa-check'></i>&nbsp; OK");
            //             });
            //         }
            //     }]
            // });

            // var lock_group_dialog_html = Handlebars.compile($('#lockGroupDiv').html());
            // $('#dynamicGroupDialog').html(lock_group_dialog_html).dialog('open');
        },
        // [Action] Unlock the Group
        act_unlock_group: function(id) {                          
            var dmURL = $.config.server_rest_url + '/deviceGroups/';

            $.SmartMessageBox({
                title: "<i class='fa fa-lock txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Group.UnlockGroup")+" <span class='txt-color-orangeDark'><strong> &nbsp;" +
                    id + "</strong></span> ?",
                content: i18n.t("ns:Message.Group.UnlockGroupContent"),
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        type: 'POST',
                        url: dmURL + id + '/actions/unlock',
                        dataType: 'json',
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.GroupUnlock") + i18n.t("ns:Message.Group.UnlockSuccess"),
                            content: "<i class='fa fa-unlock'></i> <i>"+ i18n.t("ns:Message.Group.UnlockGroupID") + "&nbsp;" + id + "</i>",
                            // title: "[Group Unlock] Unlocked successfully",
                            // content: "<i class='fa fa-unlock'></i> <i>Unlocked Group ID: &nbsp;" + id + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-unlock fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.GroupUnlock") + i18n.t("ns:Message.Group.UnockFailed"),
                            content: "<i class='fa fa-unlock'></i> <i>"+ i18n.t("ns:Message.Group.UnlockGroupID") + "&nbsp;" + id + "</i>",
                            // title: "[Group Unlock] Unocked failed",
                            // content: "<i class='fa fa-unlock'></i> <i>Unlocked Group ID: &nbsp;" + id + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/group_list.html";
                    });
                } else {
                    window.location.href = "#ajax/group_list.html";
                }
            });

            // Dialog 
            // $('#dynamicGroupDialog').dialog({
            //     autoOpen : false,
            //     width : 400,
            //     resizable : false,
            //     modal : true,
            //     title : "<div class='widget-header'><h4><i class='fa fa-unlock'></i> Unock the Group</h4></div>",
            //     buttons : [{
            //         id : "btn_cancel",
            //         html : "Cancel",
            //         "class" : "btn btn-default",
            //         click : function() {
            //             $["ajax/group_list.html"].app.navigate("#" + "ajax/group_list.html" + "/grid", {
            //                 trigger: true
            //             });
            //             $(this).dialog("close");
            //         }
            //     }, {
            //         id : "btn_unlock",
            //         html : "<i class='fa fa-unlock'></i>&nbsp; Unlock",
            //         "class" : "btn btn-primary",
            //         click : function(event) {
            //             $.ajax({                
            //                 type: 'POST',
            //                 url: dmURL + id + '/actions/unlock',
            //                 dataType: 'json',
            //                 timeout: 10000
            //             }).done(function(){
            //                 //動態產生unlockDialogDiv                
            //                 var unlock_group_dialog_html = Handlebars.compile($('#unlockGroupDiv_Done').html());
            //                 $('#dynamicGroupDialog').html(unlock_group_dialog_html).dialog('open');
            //             }).fail(function(){
            //                 //動態產生unlockDialogDiv                
            //                 var unlock_group_dialog_html = Handlebars.compile($('#unlockGroupDiv_Fail').html());
            //                 $("#dynamicGroupDialog").html(unlock_group_dialog_html).dialog('open');                           
            //             }).always(function() {
            //                 $('#btn_unlock').remove();
            //                 $('#btn_cancel').addClass('btn-primary').html("<i class='fa fa-check'></i>&nbsp; OK");
            //             });
            //         }
            //     }]
            // });

            // var unlock_group_dialog_html = Handlebars.compile($('#unlockGroupDiv').html());
            // $('#dynamicGroupDialog').html(unlock_group_dialog_html).dialog('open');
        },
        // [Action] Install the Group
        act_install_group: function(id) {  
            var t = this;                        
            t.navigate("#ajax/group_am.html/" + id + "/install", {
                trigger: true
            });
        },
        // [Action] Uninstall the Group
        // act_uninstall_group: function(id) {  
        //     var t = this;                        
        //     t.navigate("#ajax/group_uninstall.html/" + id, {
        //         trigger: true
        //     });
        // },
        // [Action] Edit the Group
        act_edit_group: function(id) {  
            var t = this;            
            t.navigate("#ajax/group_edit.html/" + id, {
                trigger: true
            });
        },  
        // [Action] Delete the Group
        act_delete_group: function(id) {                       
            var dmURL = $.config.server_rest_url + '/deviceGroups/';

            $.SmartMessageBox({
                // title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; Delete Group <span class='txt-color-orangeDark'><strong> &nbsp;" +
                //     id + "</strong></span> ?",
                title: "<i class='fa fa-times txt-color-orangeDark'></i> &nbsp; "+i18n.t("ns:Message.Group.DeleteTheGroup")+" <span class='txt-color-orangeDark'><strong> &nbsp;" + "</strong></span> ?",
                // content: "Delete the group ?",
                content: "<i class='fa fa-exclamation-circle txt-color-redLight'> &nbsp;</i><span class='txt-color-redLight'> "+i18n.t("ns:Message.Group.DeleteTheGroupContent")+"</span>",
                buttons: '['+i18n.t("ns:Message.No")+']['+i18n.t("ns:Message.Yes")+']'

            }, function(ButtonPressed) {
                if (ButtonPressed == i18n.t("ns:Message.Yes")) {                    
                    $.ajax({                
                        beforeSend: function(xhr) {
                            xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
                        },
                        type: 'POST',
                        url: dmURL + id,
                        timeout: 10000
                    }).done(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.GroupDelete") + i18n.t("ns:Message.Group.DeleteSuccess"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Group.DeletedGroupId") + "&nbsp;" + id + "</i>",
                            // title: "[Group Delete] Deleted successfully",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Group ID: &nbsp;" + id + "</i>",
                            color: "#648BB2",
                            iconSmall: "fa fa-times fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).fail(function(){
                        $.smallBox({
                            title: i18n.t("ns:Message.Group.GroupDelete") + i18n.t("ns:Message.Group.DeleteFailed"),
                            content: "<i class='fa fa-times'></i> <i>"+ i18n.t("ns:Message.Group.DeletedGroupId") + "&nbsp;" + id + "</i>",
                            // title: "[Group Delete] Deleted failed",
                            // content: "<i class='fa fa-times'></i> <i>Deleted Group ID: &nbsp;" + id + "</i>",
                            color: "#B36464",
                            iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                            timeout: 5000
                        });
                    }).always(function(){
                        window.location.href = "#ajax/group_list.html";
                    });
                } else {
                    window.location.href = "#ajax/group_list.html";
                }
            });

            // Dialog 
            // $('#dynamicGroupDialog').dialog({
            //     autoOpen : false,
            //     width : 400,
            //     resizable : false,
            //     modal : true,
            //     title : "<div class='widget-header'><h4><i class='fa fa-times'></i> Delete the Group</h4></div>",
            //     buttons : [{
            //         id : "btn_group_cancel",
            //         html : "Cancel",
            //         "class" : "btn btn-default",
            //         click : function() {
            //             $["ajax/group_list.html"].app.navigate("#" + "ajax/group_list.html" + "/grid", {
            //                 trigger: true
            //             });
            //             $(this).dialog("close");
            //         }
            //     }, {
            //         id : "btn_group_delete",
            //         html : "<i class='fa fa-times'></i>&nbsp; Delete",
            //         "class" : "btn btn-primary",
            //         click : function(event) {
            //             $.ajax({                
            //                 beforeSend: function(xhr) {
            //                     xhr.setRequestHeader('X-HTTP-Method-Override', 'DELETE');
            //                 },
            //                 type: 'POST',
            //                 url: dmURL + id,
            //                 timeout: 10000
            //             }).done(function(){
            //                 //動態產生deleteDialogDiv                
            //                 var delete_group_dialog_html = Handlebars.compile($('#deleteGroupDiv_Done').html());
            //                 $('#dynamicGroupDialog').html(delete_group_dialog_html);
            //             }).fail(function(){
            //                 //動態產生deleteDialogDiv                
            //                 var delete_group_dialog_html = Handlebars.compile($('#deleteGroupDiv_Fail').html());
            //                 $("#dynamicGroupDialog").html(delete_group_dialog_html);                           
            //             }).always(function() {
            //                 $('#btn_group_delete').remove();
            //                 $('#btn_group_cancel').addClass('btn-primary').html("<i class='fa fa-check'></i>&nbsp; OK");
            //             });
            //         }
            //     }]
            // });

            // var delete_group_dialog_html = Handlebars.compile($('#deleteGroupDiv').html());
            // $('#dynamicGroupDialog').html(delete_group_dialog_html).dialog('open');  
        }
    });    
    
    /* define bootstrap starts */
    AD.bootstrap = function() {
		i18n.init(function(t){
			$('[data-i18n]').i18n();
		});
		$("body").removeClass("loading");
	
        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/grid", {
                trigger: true
            });
        }

        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }
    };

    /* define bootstrap ends */
    $[AD.page] = AD;
    $[AD.page].bootstrap();
})(jQuery);
