(function($) {
	function l(e) {
		return e.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
	}
	function c(e, t, n) {
		if (t < e.length) return e;
		else return Array(t - e.length + 1).join(n || " ") + e;
	}
	function p() {
		var d=new Date(Date.UTC.apply(Date, arguments));
		d.setHours(-8);
		return d;
	}

	var s = {
		dd: {
			property: "UTCDate",
			getPattern: function() {
				return "(0?[1-9]|[1-2][0-9]|3[0-1])\\b"
			}
		},
		MM: {
			property: "UTCMonth",
			getPattern: function() {
				return "(0?[1-9]|1[0-2])\\b"
			}
		},
		yy: {
			property: "UTCYear",
			getPattern: function() {
				return "(\\d{2})\\b"
			}
		},
		yyyy: {
			property: "UTCFullYear",
			getPattern: function() {
				return "(\\d{4})\\b"
			}
		},
		hh: {
			property: "UTCHours",
			getPattern: function() {
				return "(0?[0-9]|1[0-9]|2[0-3])\\b"
			}
		},
		mm: {
			property: "UTCMinutes",
			getPattern: function() {
				return "(0?[0-9]|[1-5][0-9])\\b"
			}
		},
		ss: {
			property: "UTCSeconds",
			getPattern: function() {
				return "(0?[0-9]|[1-5][0-9])\\b"
			}
		},
		ms: {
			property: "UTCMilliseconds",
			getPattern: function() {
				return "([0-9]{1,3})\\b"
			}
		},
		HH: {
			property: "Hours12",
			getPattern: function() {
				return "(0?[1-9]|1[0-2])\\b"
			}
		},
		PP: {
			property: "Period12",
			getPattern: function() {
				return "(AM|PM|am|pm|Am|aM|Pm|pM)\\b"
			}
		}
	};
	var o = [];
	for (var u in s) o.push(u);
	o[o.length - 1] += "\\b";
	o.push(".");
	var a = new RegExp(o.join("\\b|"));
	o.pop();
	var f = new RegExp(o.join("\\b|"), "g");


	DateFormatter = function(options) {
		this.format = options.format;
		this._compileFormat();
	};

	DateFormatter.prototype = {
		formatDate: function(e) {
			return this.format.replace(f, function(t) {
				var n, r, i, o = t.length;
				if (t === "ms") o = 1;
				r = s[t].property;
				if (r === "Hours12") {
					i = e.getUTCHours();
					if (i === 0) i = 12;
					else if (i !== 12) i = i % 12
				} else if (r === "Period12") {
					if (e.getUTCHours() >= 12) return "PM";
					else return "AM"
				} else {
					n = "get" + r;
					i = e[n]()
				} if (n === "getUTCMonth") i = i + 1;
				if (n === "getUTCYear") i = i + 1900 - 2e3;
				return c(i.toString(), o, "0")
			})
		},
		parseDate: function(e) {
			var t, n, r, i, s, o = {};
			// alert(this._formatPattern);
			if (!(t = this._formatPattern.exec(e))) return null;
			for (n = 1; n < t.length; n++) {
				r = this._propertiesByIndex[n];
				if (!r) continue;
				s = t[n];
				if (/^\d+$/.test(s)) s = parseInt(s, 10);
				o[r] = s
			}
			return this._finishParsingDate(o)
		},
		_finishParsingDate: function(e) {
			var t, n, r, i, s, o, u;
			t = e.UTCFullYear;
			if (e.UTCYear) t = 2e3 + e.UTCYear;
			if (!t) t = 1970;
			if (e.UTCMonth) n = e.UTCMonth - 1;
			else n = 0;
			r = e.UTCDate || 1;
			i = e.UTCHours || 0;
			s = e.UTCMinutes || 0;
			o = e.UTCSeconds || 0;
			u = e.UTCMilliseconds || 0;
			if (e.Hours12) {
				i = e.Hours12
			}
			if (e.Period12) {
				if (/pm/i.test(e.Period12)) {
					if (i != 12) i = (i + 12) % 24
				} else {
					i = i % 12
				}
			}
			return p(t, n, r, i, s, o, u)
		},
		_compileFormat: function() {
			var e, t, n = [],
				r = [],
				i = this.format,
				o = {}, u = 0,
				f = 0;
			while (e = a.exec(i)) {
				t = e[0];
				if (t in s) {
					u++;
					o[u] = s[t].property;
					n.push("\\s*" + s[t].getPattern(this) + "\\s*");
					r.push({
						pattern: new RegExp(s[t].getPattern(this)),
						property: s[t].property,
						start: f,
						end: f += t.length
					})
				} else {
					n.push(l(t));
					r.push({
						pattern: new RegExp(l(t)),
						character: t,
						start: f,
						end: ++f
					})
				}
				i = i.slice(t.length)
			}
			this._mask = r;
			this._maskPos = 0;
			this._formatPattern = new RegExp("^\\s*" + n.join("") + "\\s*$");
			this._propertiesByIndex = o
		}
	}
})(jQuery);