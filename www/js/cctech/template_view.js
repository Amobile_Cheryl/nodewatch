(function($) {
    var default_converter = function() {
        return {
            data: this.collection.toJSON()
        };
    };

    var TemplateView = Backbone.TemplateView = Backbone.View.extend({
        initialize: function(options) {
            this.$el = $(this.el);

            // 取得 collection !
            this.collection = Backbone.TemplateView.getCollectionByUrl(options.url, options.idAttribute);

            //setup default value
            this.options = _.defaults(options, {
                paginated: false,
                data: {}
            });

            if (this.options.paginated === true) {
                this.options = _.defaults(this.options, {
                    page: 1,
                    perPage: 10,
                });
            }

            this.template = this.options.template || Handlebars.compile("<div></div>");
            this.converter = this.options.converter || default_converter;

            this._prepare();
        },
        _preparePager: function() {
            this.pager = new Pager({
                currentPage: this.options.page,
                perPage: this.options.perPage
            });

            this.pager.on('change:currentPage', function() {
                this._page();
            }, this);
            this.pager.on('change:perPage', function() {
                this.page(1);
            }, this);
        },
        _prepareSorter: function() {
            this.order({});
        },
        _prepareFilter: function() {
            this.filter({});
        },
        _prepare: function() {
            this._prepareSorter();
            this._prepareFilter();
            if (this.options.paginated === true)
                this._preparePager();
        },
        // render-->request-->async-->_render
        render: function() {
            this._request(this.options);
            return this;
        },
        _render: function() {
            this.$el.empty();
            this.renderTemplate();
            if (this.options.paginated)
                this.renderPagination();
            this.trigger("rendered");
            return this;
        },
        renderTemplate: function() {
            var data = this.converter();
            this.$el.append(this.template(data));
        },
        renderPagination: function() {
            var pagination = new Pagination({
                pager: this.pager
            });
            pagination.render();
        },
        refresh: function() {
            this.render();
        },
        order: function(obj) {
            this.sorter = new Backbone.Model(obj);
        },
        where_in: function(obj) {
            this.options.data.where_in = obj;
        },
        filter: function(obj) {
            this.options.data.where = obj;
        },
        _page: function() {
            this._request(this.options);
        },
        _getRequestData: function() {
            if (this.options.paginated === true)
                return {
                    page: this.pager.get('currentPage'),
                    per_page: this.pager.get('perPage')
                };
            return {};
        },
        _request: function(opt) {
            var options = _.clone(opt) || {};
            var silent = options.silent;

            var pager = this._getRequestData();
            _.extend(options.data, pager);

            //setup sorter
            if (typeof this.sorter.get("column") !== 'undefined')
                options.data.order = this.sorter.get("column") + " " + this.sorter.get("order");
            else if (_.isUndefined(options.data.order))
                options.data.order = this.collection.model.prototype.idAttribute + " " + "DESC";

            //success function 
            options.success = _.bind(function(collection) {
                var info = this.info = collection.at(collection.length - 1);
                collection.remove(info);
                // var info = this.info = collection.pop();
                if (this.options.paginated === true) {
                    this.pager.set('total', info.get("count"));
                }
                this._render();
            }, this);
            options.silent = true;

            this.collection.fetch(options);
        }
    }, {
        getCollectionByUrl: function(url, idAttribute) {
            var Model = Backbone.Model.extend({
                urlRoot: url,
                idAttribute: idAttribute
            });

            var Collection = Backbone.Collection.extend({
                url: url,
                model: Model
            });
            return new Collection();
        }
    });

    var Pager = TemplateView.Pager = Backbone.Model.extend({
        initialize: function() {
            this.on('change:perPage change:total', function() {
                this.totalPages(this.get('total'));
            }, this);
            if (this.has('total')) {
                this.totalPages(this.get('total'));
            }
        },

        totalPages: function(total) {
            if (_.isNumber(total)) {
                this.set('totalPages', Math.ceil(total / this.get('perPage')));
            } else {
                this.set('totalPages', undefined);
            }
        },

        page: function(page) {
            if (this.inBounds(page)) {
                if (page === this.get('currentPage')) {
                    this.trigger('change:currentPage');
                } else {
                    this.set('currentPage', page);
                }
            }
        },

        next: function() {
            this.page(this.get('currentPage') + 1);
        },

        prev: function() {
            this.page(this.get('currentPage') - 1);
        },

        hasTotal: function() {
            return this.has('totalPages');
        },

        hasNext: function() {
            if (this.hasTotal()) {
                return this.get('currentPage') < this.get('totalPages');
            } else {
                return this.get('hasNext');
            }
        },

        hasPrev: function() {
            if (this.has('hasPrev')) {
                return this.get('hasPrev');
            } else {
                return this.get('currentPage') > 1;
            }
        },

        inBounds: function(page) {
            return !this.hasTotal() || page > 0 && page <= this.get('totalPages');
        },

        set: function() {
            var args = [];
            for (var i = 0; i < arguments.length; i++) {
                args.push(arguments[i]);
            }
            args[2] = _.extend({}, args[2], {
                validate: true
            });
            Backbone.Model.prototype.set.apply(this, args);
        },

        validate: function(attrs) {
            if (attrs.perPage < 1) {
                throw new Error('perPage must be greater than zero.');
            }
        }
    }); // end of Pager

    //<div class="paging">
    //    <span class="current">1</span>
    //    <a href="#">2</a>
    //    <span class="dots">…</span>
    //    <a href="#">6</a>
    //    <a href="#">Next</a>
    //</div>
    var Pagination = TemplateView.Pagination = Backbone.View.extend({
        el: "#pager",
        className: 'paging',

        events: {
            'click a:not(.disabled)': 'page',
            'click a.disabled ': function(e) {
                e.preventDefault();
            }
        },

        initialize: function() {
            this.$el = $(this.el);
            this.pager = this.options.pager;
        },

        render: function() {
            var $ul = $('<div></div>'),
                $li;
            $ul.addClass(this.className);
            // $li = $('<li class="prev"><a href="#">«</a></li>');

            if (!this.pager.hasPrev()) {
                $li = $('<span class="current">prev</span>');
            } else {
                $li = $('<a class="prev" href="#">prev</a>');
            }

            $ul.append($li);

            if (this.pager.hasTotal()) {
                for (var i = 1; i <= this.pager.get('totalPages'); i++) {
                    $li = $('<li></li>');
                    if (i === this.pager.get('currentPage')) {
                        $li = $('<span class="current">' + i + '</span>');
                    } else {
                        $li = $('<a href="#">' + i + '</a>');
                    }
                    $ul.append($li);
                }
            }

            if (!this.pager.hasNext()) {
                $li = $('<span class="current">next</span>');
            } else {
                $li = $('<a class="next" href="#">next</a>');
            }
            $ul.append($li);

            this.$el.html($ul);
            return this;
        },

        page: function(event) {
            event.preventDefault();
            var $target = $(event.target),
                page;
            if ($target.hasClass('prev')) {
                this.pager.prev();
            } else if ($target.hasClass('next')) {
                this.pager.next();
            } else {
                this.pager.page(parseInt($(event.target).html(), 10));
            }
            return false;
        }
    }); //end of Pagination
})(jQuery);