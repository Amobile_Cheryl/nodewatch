/* 這個library需要include Backbone 和 Backbone.Datagrid */
(function($) {

    function runDataTables() {

        /*
         * BASIC
         */

        // $('#dt_basic').dataTable({
        //     "sPaginationType": "bootstrap_full"
        // });

        // /* END BASIC */

        // /* Add the events etc before DataTables hides a column */
        // $("#datatable_fixed_column thead input").keyup(function() {
        //     oTable.fnFilter(this.value, oTable.oApi._fnVisibleToColumnIndex(oTable.fnSettings(), $("thead input").index(this)));
        // });

        // $("#datatable_fixed_column thead input").each(function(i) {
        //     this.initVal = this.value;
        // });
        // $("#datatable_fixed_column thead input").focus(function() {
        //     if (this.className == "search_init") {
        //         this.className = "";
        //         this.value = "";
        //     }
        // });
        // $("#datatable_fixed_column thead input").blur(function(i) {
        //     if (this.value == "") {
        //         this.className = "search_init";
        //         this.value = this.initVal;
        //     }
        // });


        // var oTable = $('#datatable_fixed_column').dataTable({
        //     "sDom": "<'dt-top-row'><'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        //     //"sDom" : "t<'row dt-wrapper'<'col-sm-6'i><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'>>",
        //     "oLanguage": {
        //         "sSearch": "Search all columns:"
        //     },
        //     "bSortCellsTop": true
        // });



        // /*
        //  * COL ORDER
        //  */
        // $('#datatable_col_reorder').dataTable({
        //     "sPaginationType": "bootstrap",
        //     "sDom": "R<'dt-top-row'Clf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
        //     "fnInitComplete": function(oSettings, json) {
        //         $('.ColVis_Button').addClass('btn btn-default btn-sm').html('Columns <i class="icon-arrow-down"></i>');
        //     }
        // });

        // /* END COL ORDER */

        /* TABLE TOOLS */

        var data = {
            iTotalRecords: 100,
            iTotalDisplayRecords: 100,
            sEcho: 0,
            aaData: [{
                "id": 1,
                "creationTime": "2013-12-06 20:53:38.0",
                "email": "matthew.him@gmail.com",
                "entityVersion": 0,
                "functionAuthorization": null,
                "lastLoginTime": null,
                "loginCount": 0,
                "name": "Matthew Hui",
                "password": "7c1fc247b3e4d29ebba64b7ae8f405d84c400f81",
                "permissionType": null,
                "phone": "0925482204",
                "remark": null,
                "role": 3,
                "type": "2"
            }, {
                "id": 15,
                "creationTime": "2013-12-09 19:24:13.0",
                "email": "matthew.him1@gmail.com",
                "entityVersion": 0,
                "functionAuthorization": null,
                "lastLoginTime": null,
                "loginCount": 0,
                "name": "him",
                "password": "7c1fc247b3e4d29ebba64b7ae8f405d84c400f81",
                "permissionType": null,
                "phone": "0988236624",
                "remark": null,
                "role": 1,
                "type": null
            }, {
                "id": 13,
                "creationTime": "2013-12-09 16:32:10.0",
                "email": "admin@gmail.com",
                "entityVersion": 0,
                "functionAuthorization": null,
                "lastLoginTime": null,
                "loginCount": 0,
                "name": "admin",
                "password": "30531c2885ce61b385dc81d2a375f6bef80607d5",
                "permissionType": null,
                "phone": "0975543297",
                "remark": null,
                "role": 1,
                "type": null
            }, {
                "id": 19,
                "creationTime": "2013-12-17 15:35:57.0",
                "email": "matthew.him2@gmail.com",
                "entityVersion": 0,
                "functionAuthorization": null,
                "lastLoginTime": null,
                "loginCount": 0,
                "name": "Hello",
                "password": "7c1fc247b3e4d29ebba64b7ae8f405d84c400f81",
                "permissionType": null,
                "phone": "0988236624",
                "remark": null,
                "role": 2,
                "type": null
            }, {
                "id": 1,
                "creationTime": "2013-12-06 20:53:38.0",
                "email": "matthew.him@gmail.com",
                "entityVersion": 0,
                "functionAuthorization": null,
                "lastLoginTime": null,
                "loginCount": 0,
                "name": "Matthew Hui",
                "password": "7c1fc247b3e4d29ebba64b7ae8f405d84c400f81",
                "permissionType": null,
                "phone": "0925482204",
                "remark": null,
                "role": 3,
                "type": "2"
            }, {
                "id": 15,
                "creationTime": "2013-12-09 19:24:13.0",
                "email": "matthew.him1@gmail.com",
                "entityVersion": 0,
                "functionAuthorization": null,
                "lastLoginTime": null,
                "loginCount": 0,
                "name": "him",
                "password": "7c1fc247b3e4d29ebba64b7ae8f405d84c400f81",
                "permissionType": null,
                "phone": "0988236624",
                "remark": null,
                "role": 1,
                "type": null
            }, {
                "id": 13,
                "creationTime": "2013-12-09 16:32:10.0",
                "email": "admin@gmail.com",
                "entityVersion": 0,
                "functionAuthorization": null,
                "lastLoginTime": null,
                "loginCount": 0,
                "name": "admin",
                "password": "30531c2885ce61b385dc81d2a375f6bef80607d5",
                "permissionType": null,
                "phone": "0975543297",
                "remark": null,
                "role": 1,
                "type": null
            }, {
                "id": 19,
                "creationTime": "2013-12-17 15:35:57.0",
                "email": "matthew.him2@gmail.com",
                "entityVersion": 0,
                "functionAuthorization": null,
                "lastLoginTime": null,
                "loginCount": 0,
                "name": "Hello",
                "password": "7c1fc247b3e4d29ebba64b7ae8f405d84c400f81",
                "permissionType": null,
                "phone": "0988236624",
                "remark": null,
                "role": 2,
                "type": null
            }]
        };

        var fnServerObjectToArray = function() {
            return function(sSource, aoData, fnCallback) {
                fnCallback(data);
            }
        };

        var oTable = $('#datatable_tabletools').dataTable({
            // "sPaginationType": "scrolling",
            // "bProcessing": true,
            "bServerSide": true,
            "fnServerData": fnServerObjectToArray(),
            "sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
            "oTableTools": {
                "aButtons": ["copy", "print", {
                    "sExtends": "collection",
                    "sButtonText": 'Save <span class="caret" />',
                    "aButtons": ["csv", "xls", "pdf"]
                }],
                "sSwfPath": "js/plugin/datatables/media/swf/copy_csv_xls_pdf.swf"
            },
            "fnInitComplete": function(oSettings, json) {
                $(this).closest('#dt_table_tools_wrapper').find('.DTTT.btn-group').addClass('table_tools_group').children('a.btn').each(function() {
                    $(this).addClass('btn-sm btn-default');
                });
            }
        });

        /* END TABLE TOOLS */

    };


    //提供 Url 和 idAttribute 得到 Collection
    Backbone.Datagrid.getCollectionByUrl = function(url, idAttribute) {
        var Model = Backbone.Model.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        var Collection = Backbone.Collection.extend({
            url: url,
            model: Model
        });
        return new Collection();
    };

    //針對Backbone.Datagrid的修改！
    var abstract_form_cell = {
        default_value: "",
        useTemplate: true,
        view: function(o) {
            return $('<input type="text" name="' + this.property + '" placeholder="' + this.title + '" class="text" >');
        },
        value: function() { //provide value
            return $('input[name="' + this.property + '"]').val();
        },
        setValue: function(m) { //set the value to this element
            var v = m.get(this.property) || this.default_value;
            $('input[name="' + this.property + '"]').val(v);
        },
        update: function(m) {

        }
    };

    Backbone.Datagrid.prototype.initialize = function() {
        this.columns = this.options.columns;
        this.where_in = this.options.where_in;
        this.options = _.defaults(this.options, {
            paginated: false,
            page: 1,
            perPage: 10,
            tableClassName: 'table',
            emptyMessage: '<p>沒有回傳任何結果.</p>'
        });

        this.collection.on('reset', this.render, this);
        this._prepare();
    };


    Backbone.Datagrid.prototype._prepare = function() {
        this._prepareSorter();
        this._prepareFilter();
        // this._preparePager();
        this._prepareColumns();
        this.refresh();
    };

    Backbone.Datagrid.prototype._prepareColumns = function() {
        if (!this.columns || _.isEmpty(this.columns)) {
            this._defaultColumns();
        } else {
            var arr = [];
            _.each(this.columns, function(column, i) {
                if (column.view !== false)
                    arr.push(this._prepareColumn(column, i));
            }, this);
            this.columns = arr;
        }
    };

    Backbone.Datagrid.prototype.renderTable = function() {
        var $table = $('<table></table>', {
            'id': 'datatable_tabletools',
            'class': this.options.tableClassName
        });
        this.$el.append('<div class="widget-body-toolbar"></div>');
        this.$el.append($table);

        var header = new Backbone.Datagrid.Header({
            columns: this.columns,
            sorter: this.sorter
            /*,
            filter: this.filter*/
        });
        $table.append(header.render().el);

        $table.append('<tbody></tbody>');

        if (this.collection.isEmpty()) {
            this.$el.append(this.options.emptyMessage);
        } else {
            this.collection.forEach(this.renderRow, this);
        }
    }

    Backbone.Datagrid.prototype._prepareFilter = function() {
        this.filter = new Filter();
        this.filter.on('change', function() {
            // this._sort(this.sorter.get('column'), this.sorter.get('order'));
            this.pager.set('currentPage', 1);
            this._filtered();
        }, this);
    };

    Backbone.Datagrid.prototype._filtered = function() {
        this._filterRequest();
    };

    Backbone.Datagrid.prototype._filterRequest = function() {
        this._request();
    };

    var Filter = Backbone.Datagrid.Filter = Backbone.Model.extend({

    }); // end of Filter


    Backbone.Datagrid.Header.prototype.initialize = function() {
        this.columns = this.options.columns;
        this.sorter = this.options.sorter;
        this.filter = this.options.filter;
    };

    /*
    var ori = Backbone.Datagrid.Header.prototype.render;

    Backbone.Datagrid.Header.prototype.render = function() {
        ori.call(this);


        var model = new Backbone.Model();
        var headerColumn, columns = [];
        _.each(this.columns, function(column, i) {
            headerColumn = _.clone(column);
            headerColumn.property = column.property || column.index;
            headerColumn.view = {
                type: FilterCell, //加一行，用我們的FilterCell把
                filter: this.filter
            };

            model.set(headerColumn.property, column.title);
            columns.push(headerColumn);
        }, this);

        var row = new Backbone.Datagrid.Row({
            model: model,
            columns: columns,
            header: true
        });
        this.$el.append(row.render().el);
        return this;
    };
    */

    var FilterCell = Backbone.Datagrid.FilterCell = Backbone.Datagrid.Cell.extend({
        initialize: function() {
            FilterCell.__super__.initialize.call(this);

            this.filter = this.options.filter;

            if (this.column.filterable) {
                _.bindAll(this, "click", "blur", "change");
            }
        },

        events: {
            click: "click",
            "blur .input_cell": "blur",
            "keypress .input_cell": "keypress",
            "change select.input_cell": "change"
        },

        change: function(e) {
            this.blur(e);
        },
        render: function() {
            this._prepareValue();
            var html = '';

            if (_.isArray(this.column.filterable)) {
                var v = this.filter.get(this.column.property);
                this.$el.addClass("editable");
                var arr = this.column.filterable;
                for (var i = 0; i < arr.length; i++) {
                    if (arr[i].value == v) {
                        html = arr[i].label;
                        break;
                    }
                };
            } else if (this.column.filterable) {
                // alert("render");
                var v = this.filter.get(this.column.property);
                this.$el.addClass("editable");

                html = v;
            } else
                html = "&nbsp;";
            this.$el.html(html);
            // alert(html);
            return this;
        },

        click: function(e) {
            if (this.column.filterable) {
                var w = this.$el.width();
                // alert(w);
                var v = this.filter.get(this.column.property);
                if (_.isArray(this.column.filterable)) {
                    var _html = '<select class="' + this.column.property + ' input_cell">';
                    _html += '<option value=""></option>';
                    var arr = this.column.filterable
                    for (var i = 0; i < arr.length; i++) {
                        _html += '<option value="' + arr[i].value + '">' + arr[i].label + '</option>';
                    };
                } else if (v)
                    var _html = '<input type="text" class="input_cell" value="' + v + '"></input>';
                else
                    var _html = '<input type="text" class="input_cell" value=""></input>';

                this.$el.html(_html);
                this.$el.css("width", w + "px");
                this.$el.find(".input_cell").focus();
            }
        },
        keypress: function(e) {
            if (e.keyCode != 13) return;
            this.blur(e);
        },
        blur: function(e) {
            var val = this.$el.find(".input_cell").val();
            if (val !== "")
                this.filter.set(this.column.property, val);
            else
                this.filter.unset(this.column.property);

            if (_.isArray(this.column.filterable)) {
                var val = this.$el.find('option[value="' + val + '"]').html();
                // alert(val);
                this.$el.html(val);
            } else {
                this.$el.html(val);
            }
        }
    }); // end of FilterCell

    Backbone.Datagrid.prototype._request = function(options) {
        options = options || {};
        var success = options.success;
        var silent = options.silent;

        options.data = this._getRequestData();
        options.data.like = this.filter.toJSON();

        if (typeof this.sorter.get("column") !== 'undefined')
            options.data.order = this.sorter.get("column") + " " + this.sorter.get("order");
        else
            options.data.order = this.collection.model.prototype.idAttribute + " " + "DESC";

        if (typeof this.where_in !== 'undefined')
            options.data.where_in = this.where_in.toJSON();

        options.success = _.bind(function(collection) {
            // var json = collection.toJSON();
            // var info = json.pop();
            collection.remove(collection.at(collection.length - 1));
            // var info = collection.pop();
            /*
            this.pager.set('total', info.get("count"));
            */
            if (!this.columns || _.isEmpty(this.columns)) {
                this._prepareColumns();
            }
            if (success) {
                success();
            }
            /*
            if (this.options.paginated) {
                this.pager.update(collection);
            }
            */
            if (!silent) {
                collection.trigger('reset', collection);
            }
        }, this);
        options.silent = true;

        this.collection.fetch(options);
    };

    Backbone.Datagrid.prototype._getRequestData = function() {
        if (this.collection.data && _.isFunction(this.collection.data)) {
            // alert("A");
            return this.collection.data(this.pager, this.sorter, this.filter);
        } else if (this.collection.data && typeof this.collection.data === 'object') {
            // alert("B");
            var data = {};
            _.each(this.collection.data, function(value, param) {
                if (_.isFunction(value)) {
                    value = value(this.pager, this.sorter, this.filter);
                }
                data[param] = value;
            }, this);
            return data;
        } else if (this.options.paginated) {
            // return {
            //     page: this.pager.get('currentPage'),
            //     per_page: this.pager.get('perPage')
            // };
            return {
                page: 1,
                per_page: 10
            };
        }

        return {};
    }

    // finish the overwrite of Backbone.Datagrid

    Backbone.GridView = Backbone.View.extend(
    {
        template: Handlebars.compile($('#tpl_table').html()),
        select: [],
        initialize: function(options)
        {
            this.columns = options.columns;
            this.title = options.title || "清單列表";

            var that = this;

            var data = {
                collection: this.collection,
                paginated: true,
                // inMemory: true,
                columns: this.columns,
                rowEditable: false,
                attributes:
                {
                    class: "widget-body no-padding"
                },
                tableClassName: 'table table-striped table-bordered table-hover'
                // pager: pager
            };

            if(options.where_in)
                data["where_in"] = options.where_in;

            this.datagrid = new Backbone.Datagrid(data).render();

            this.$el.html(this.template(
            {
                title: this.title
            }));

            //'<div class="widget-body-toolbar"></div>' + 
            this.$el.find('.widget-body').replaceWith(this.datagrid.$el);

            /*
            this.datagrid.pager.on("change:currentPage", function() {
                event.preventDefault();
                that._clearSelect();
            });*/

            this.$el.on("click", "input[type='checkbox']", function(event)
            {
                that._select(event, this);
            });

            this.$el.on("click", ".refresh", function(event)
            {
                event.preventDefault();
                that.refresh();
            });

            this.$el.on("click", ".remove", function(event)
            {
                event.preventDefault();
                that.remove();
            });

            this.$el.on("click", ".removeOne", function(event)
            {
                event.preventDefault();
                that.removeOne(this);
            });

            // this.$el.on("change", ".per_page select", function(event) {
            //     that.per_page(this);
            // })
        },
        per_page: function(target)
        {
            this.datagrid.page(1);
            this.datagrid.perPage(parseInt($(target).val(), 10));
        },
        removeOne: function(target)
        {
            var id = $(target).data("id");
            var model = this.collection.get(id);
            model.destroy(
            {
                async: false
            });
            this.datagrid.refresh();
            this.select = [];
            this.trigger("change:select", this.select);
        },
        remove: function()
        {
            // alert(this.select);
            var that = this;
            // alert(this.select);
            $.each(this.select, function(i, v)
            {
                // alert(v);
                var model = that.collection.get(v);
                model.destroy(
                {
                    async: false
                });
            });
            this.select = [];
            this.datagrid.refresh();
            this.trigger("change:select", this.select);
        },
        _clearSelect: function()
        {
            this.select = [];
            this.trigger("change:select", this.select);
        },

        _select: function(event, target)
        {
            $t = $(target);
            if($t.val() === "all")
            {
                if(typeof $t.attr('checked') === 'undefined')
                {
                    this.$el.find("input[type='checkbox']").removeAttr('checked');
                }
                else
                {
                    this.$el.find("input[type='checkbox']").attr('checked', $t.attr('checked'));
                }
            }

            var array = [];
            this.$el.find("input[type='checkbox'][checked][value!='all']").each(function()
            {
                array.push($(this).val());
            });

            this.select = array;
            this.trigger("change:select", this.select);
            // alert(that.select);
        },

        refresh: function()
        {
            this.select = [];
            this.datagrid.filter.clear();
            this.datagrid.sorter.clear();
            this.datagrid.refresh();
        }
    });

    Backbone.FormView = Backbone.View.extend({
        template: Handlebars.compile($('#tpl_edit').html()),
        cell_template: Handlebars.compile($('#tpl_edit_cell').html()),

        initialize: function() {
            this.forms = this.options.forms;
            var flen = this.forms.length;
            for (var i = 0; i < flen; i++) {
                spec = this.forms[i];
                _.defaults(spec, abstract_form_cell);
            };
            this.options = _.defaults(this.options, {
                fromClassName: 'form-horizontal uni',
                Name: 'Form'
            });

            this._prepare();
        },
        _prepare: function() {
            this.render();
        },
        render: function() {
            this._renderForm(this.model);
            return this;
        },
        _renderForm: function(model) {

            //先取得template
            this.$el = $(this.el).append(this.template({
                Name: this.options.Name
            }));

            //產生form
            var $form = $('<form></form>', {
                'class': this.options.formClassName
            });
            $form.addClass('form-horizontal uni'); //硬幹的把className加進去

            //把form加進template中
            this.$el.find(".widget-body").append($form);

            //針對每一個column來進行iteration
            var len = this.forms.length;
            for (var i = 0; i < len; i++) {
                var spec = this.forms[i];
                //用cell_template
                if (spec.useTemplate === true) {
                    var data = {
                        property: spec.property,
                        title: spec.title
                    };
                    $cell = $(this.cell_template(data));
                    $el = spec.view(model); //call external function
                    $cell.find(".edit-cell-content").append($el);
                    $el = $cell;
                } else { //不用cell_template
                    $el = spec.view(model); //call external function
                }
                //把他append到form
                spec.$el = $el;
                $form.append($el);
            }

            $form.append("<hr>");
            $form.append('<a href="#" class="btn btn-primary submit">Save</a>');
            $form.append('<a href="#grid" class="btn btn-warning">Cancel</a>');

            //set up default for all property
            for (var i = 0; i < len; i++) {
                var spec = this.forms[i];
                spec.setValue(model);
            }
        }

    });

    var defaultErrorHandler = function(model, xhr, options) {
        // alert("error:" + xhr.responseText);
    };

    var gvs = ".cctech-gv";
    var fvs = ".cctech-fv";
    /* Router starts */
    Backbone.View1 = {};
    var o;
    Backbone.View1.Router = Backbone.Router.extend(o = {
        routes: {
            'add': "show_add",
            'edit/:_id': 'show_edit',
            'grid': 'show_grid',
            '': "render"
        },

        render: function() {
            this.navigate("#grid", {
                trigger: true
            });
        },

        initialize: function(options) {
            this.AD = options.AD;

            this.$gv = $(gvs).first();
            this.$fv = $(fvs).first();

        },

        show_add: function() {
            var that = this;
            this.$fv.empty();
            this.$gv.hide();

            var model = new that.AD.Model();

            this.fv = new Backbone.FormView({
                el: this.$fv,
                Name: "Add",
                model: new Backbone.Model(),
                forms: this.AD.forms
            });
            // this.fv.render();

            this.fv.$el.unbind();
            this.fv.$el.on("click", ".submit", function(event) {
                that.submit(event, model);
            });
            this.$fv.show();

        },

        show_edit: function(_id) {
            var that = this;
            this.$fv.empty();
            this.$gv.hide();

            // var idAttr=this.AD.Model.idAttribute;
            var attrs = {};
            var model = new this.AD.Model();
            attrs[model.idAttribute] = _id;

            var collection = new this.AD.Collection();

            // model.set(model.idAttrib);
            collection.fetch({
                async: false,
                data: {
                    where: attrs
                }
            });

            model = collection.get(_id);

            this.fv = new Backbone.FormView({
                el: this.$fv,
                Name: "Edit",
                model: model,
                forms: this.AD.forms
            });
            // this.fv.render();

            this.fv.$el.unbind();
            this.fv.$el.on("click", ".submit", function(event) {
                that.submit(event, model);
            });
            this.$fv.show();

        },

        show_grid: function() {
            $('body,html').animate({
                scrollTop: 0
            }, 500);
            // init GridView
            if (!this.gv) {
                this.gv = new Backbone.GridView({
                    el: this.$gv,
                    collection: new this.AD.Collection(),
                    columns: this.AD.columns,
                    title: this.AD.title
                });
            } else
                this.gv.refresh();

            this.$fv.hide();
            this.$gv.show();

            pageSetUp();

            setTimeout(runDataTables, 500);
        },

        submit: function(event, model) {
            event.preventDefault();

            var $fv = this.fv.$el;

            var clen = this.AD.forms.length;

            for (var i = 0; i < clen; i++) {
                var c = this.AD.forms[i];
                var v = c.value();
                if (_.isObject(v))
                    model.set(v);
                else
                    model.set(c.property, v);
            }

            if (this.AD.validate && !this.AD.validate(model))
                return;

            var that = this;

            model.save({}, {
                async: false,
                error: this.AD.error || defaultErrorHandler,
                success: function(model, response, options) {
                    for (var i = 0; i < clen; i++) {
                        var c = that.AD.forms[i];
                        var v = c.update(model);
                    }
                    that.navigate("#grid", {
                        trigger: true
                    });
                }
            });



        }
    }, {
        myextend: function(a, b) {
            var target = _.clone(a);
            _.extend(target.routes, o.routes);
            var routes = target.routes;
            _.extend(target, o);
            target.routes = routes;
            return Backbone.Router.extend(target, b);
        }
    });

})(jQuery);

(function($) {
    Backbone.UIToolBox = {};
})(jQuery);

(function($) {
    /*
  全部必須
  title: '管理員', //必填
  fields:["MngrID","MngrName","Email"], //必填
  url: ,//必填 抓東西的idAttribute

  idAttribute:,  //option 抓東西的id
  property: 'MngrID', // options..選fields第一個
  label_fields:[], //options...選fields 2~最後
  matcher:["MngrName","Email"], //options，選fields 2~最後
  value_field:"MngrID", //options....選第一個
  name_field:"Name", //options....選第二個
  select, function//options...沒有就空白

  example:

  _.defaults({
      title: '管理員',
      fields: ["MngrID", "MngrName", "Email"],
      url: '../bn/index.php/hotelmngr/gen',
      select: function() {
        try {
          var ret = $.hostel_list.app.gv[0].select;
        } catch(err) {
          return [];
        }
        return ret;
      }
    },Backbone.UIToolBox.autocomp)
 */
    var match_function = function(model, matcher, fields) {
        var flen = fields.length;
        for (var i = 0; i < flen; i++) {
            var name = fields[i];
            if (matcher.test(model.get(name)))
                return true;
        }
        return false;
    };

    var label_generate = function(model, labels) {
        var len = labels.length;
        var arr = [];
        for (var i = 0; i < len; i++)
            arr.push(model.get(labels[i]));
        return arr.join('-');
    };

    Backbone.UIToolBox.autocomp = {
        view: function(o) {
            //設定default值
            var fields = this.fields;
            var flen = fields.length;
            this.idAttribute = this.idAttribute || fields[0];
            this.property = this.property || fields[0];
            this.label_fields = this.label_fields || fields.slice(1, flen);
            this.match_fields = this.match_fields || fields.slice(1, flen);
            this.value_field = this.value_field || fields[0];
            this.name_field = this.name_field || fields[1];
            this.select = this.select || function() {
                return [];
            };


            var that = this;
            var $html = $('<input type="text" name="' + this.property + '" placeholder="' + this.title + '" class="text" >');
            that.fetch();
            $html.ready(function() {
                $html.autocomplete({
                    minLength: 0,
                    delay: 750,
                    source: function(request, response) {
                        that.fetch();
                        var coll = that.collection;
                        var arr = [];
                        for (var i = 0; i < coll.length; i++) {
                            var model = coll.at(i);
                            var matcher = new RegExp("^" + $.ui.autocomplete.escapeRegex(request.term), "i");
                            if (match_function(model, matcher, that.match_fields))
                                arr.push({
                                    label: label_generate(model, that.label_fields),
                                    value: model.get(that.value_field)
                                });
                        }
                        response(arr);
                    },
                    select: function(event, ui) {
                        this.item = ui.item;
                    },
                    close: function(event, ui) {
                        var item = this.item || {};
                        var model = that.collection.get(item.value);

                        if (model) {
                            var name = model.get(that.name_field);
                            // alert(name);
                            $('input[name=' + that.property + ']').val(name);
                            $('input[name=' + that.property + ']').data("id", item.value);
                        } else {
                            $('input[name=' + that.property + ']').data("id", "");
                        }
                    },
                    change: function(event, ui) {
                        // alert("change!");
                        var id = $('input[name=' + that.property + ']').data("id");
                        var name = $('input[name=' + that.property + ']').val();
                        var model = that.collection.get(id);


                        if (!model) {

                            // alert("gg la!");
                            $('input[name=' + that.property + ']').data("id", "");
                            return;
                        }

                        var model_name = model.get(that.name_field);
                        if (model_name !== name) {
                            // alert("gg la2!");
                            $('input[name=' + that.property + ']').data("id", "");
                            return;
                        }

                        that.overwrite(o, model);
                    }
                });
            });
            return $html;
        },
        fetch: function(o) {
            if (!this.collection) {
                var coll = this.collection = Backbone.Datagrid.getCollectionByUrl(this.url, this.idAttribute);
                coll.fetch({
                    async: false,
                    data: {
                        "fields": this.fields,
                    }
                });
                this.info = coll.pop();
            }
        },
        setValue: function(o) {
            var v = o.get(this.property);
            var sel = this.select();
            if (typeof v === "undefined" && sel) {
                v = sel[0];
            }
            var model = this.collection.get(v);
            if (model) {
                name = model.get(this.name_field);
                $('input[name=' + this.property + ']').val(name);
                $('input[name=' + this.property + ']').data("id", v);
            }
        },
        value: function(m) {
            var v = $('input[name=' + this.property + ']').data("id");
            // alert("id="+v);
            var model = this.collection.get(v);
            if (_.isUndefined(model)) {
                // alert(this.title + "不能是空白或不存在項目");
                throw this.title + "不能是空白或不存在項目";
            }
            return v;
        },
        overwrite: function(mori, mitem) {
            //do nothing by default
        }
    }
    /* Router ends */

})(jQuery);

(function($) {
    Backbone.UIToolBox.password = {
        view: function(o) {
            return '<input type="password" name="' + this.property + '" placeholder="Password" class="password">';
        },
        default_value: "",
        value: function() { //provide value
            var v = $('input[name="' + this.property + '"]').val();
            return v ? SHA1(v) : undefined;
        },
        setValue: function(o) { //set the value to this element
            $('input[name="' + this.property + '"]').val();
        }
    };
})(jQuery);


(function($) {
    var datetimepicker_template;
    var df = new DateFormatter({
        format: "yyyy-MM-dd hh:mm:ss"
    });
    /*
		_.defaults({
			title: '狀態',
		    	property: 'UpTime',
		    label_value: [ {label:"未驗證",value:"0"},{label:"已驗證",value:"1"}],
		},Backbone.UIToolBox.selector)
 	*/
    Backbone.UIToolBox.datetimepicker = {
        view: function(m) {
            if (!datetimepicker_template)
                datetimepicker_template = Handlebars.compile($('#tpl_datetimepicker').html());
            var html = datetimepicker_template(this);
            var $html = $(html);
            $html = $html.datetimepicker({
                "language": "zh_TW"
            });
            this.$dtp = $html.data("datetimepicker");
            return $html;
        },
        setValue: function(m) {
            var v = m.get(this.property) || new Date();

            if (typeof v === "string") {
                v = df.parseDate(v);
            }

            v.setHours(v.getHours() + 8);
            this.$dtp.setValue(v);
        }
    }; //datetimepicker
})(jQuery);

(function($) {
    /*
		_.defaults({
			title: '狀態',
		    property: 'Verified',
		    label_value: [ {label:"未驗證",value:"0"},{label:"已驗證",value:"1"}],
		},Backbone.UIToolBox.selector)
 	*/
    Backbone.UIToolBox.selector = {
        view: function(o) {
            this.default_value = this.default_value || this.label_value[0].value;

            var lv = this.label_value;
            var lvlen = lv.length;
            var _html = '<select class="' + this.property + '">';
            for (var i = 0; i < lvlen; i++)
                _html += '<option value="' + lv[i].value + '">' + lv[i].label + '</option>';
            _html += '</select>';
            return _html;
        },
        value: function() {
            return $("select." + this.property).val();
        },
        setValue: function(o) {
            var v = o.get(this.property) || this.default_value;
            $("select." + this.property).val(v);
        }
    }

})(jQuery);


(function($) {
    var section_template;
    /*
		tabs: [{
				property: "Description1",
				title: "產品說明",
				active: true
			}, {
				property: "Description2",
				title: "產品規格"
			}, {
				property: "Description3",
				title: "了解更多"
			}
		],
		default_value: [description1_template(), description2_template(), description3_template()],
 	*/
    Backbone.UIToolBox.section = {
        useTemplate: false,
        view: function(o) {
            var that = this;
            if (!section_template)
                section_template = Handlebars.compile($('#tpl_section').html());
            var html = section_template({
                tabs: this.tabs
            });
            var $html = $(html);
            $html.ready(function() {
                // extremely bad!
                setTimeout(function() {
                    $html.find('.cleditor').cleditor({
                        width: "98%",
                        docCSSFile: that.docCSSFile
                    });
                }, 500);
            });
            return $html;
        },
        value: function() {
            var obj = {};
            for (var i = 0; i < this.tabs.length; i++) {
                var p = this.tabs[i].property;
                obj[p] = this.$el.find("[name='" + p + "']").val();
            }
            return obj;
        },
        setValue: function(m) {
            for (var i = 0; i < this.tabs.length; i++) {
                var p = this.tabs[i].property;
                var v = m.get(p) || this.default_value[i];
                // alert($("input[name='"+p+"'").length);
                // alert(v);
                this.$el.find("[name='" + p + "']").val(v);
            }
        }
    }
})(jQuery);

(function($) {

    var jqfu_template;
    /*
		 _.defaults({
          property:"Product.ProductPhoto",
          idAttribute:"ProductID"
        },Backbone.UIToolBox.photo)
	 */
    Backbone.UIToolBox.photo = {
        useTemplate: false,
        view: function(o) {
            var that = this;
            if (!jqfu_template)
                jqfu_template = Handlebars.compile($('#tpl_jqfu').html());
            var tid = o.get(this.idAttribute);
            var data = {}
            if (typeof tid !== 'undefined') {
                data.tid = tid;
                data.name = this.property;
            }
            var html = jqfu_template();
            var $html = $(html);
            $html.ready(function() {
                $html.fileupload({
                    url: '../jqfu/server/php/'
                });
                $html.addClass('fileupload-processing');
                $html.fileupload('option', {
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    maxFileSize: 5000000,
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
                });

                that.data = {
                    url: $html.fileupload('option', 'url'),
                    dataType: 'json',
                    context: $html[0],
                    data: data
                };
                $.ajax(that.data).always(function() {
                    $(this).removeClass('fileupload-processing');
                }).done(function(result) {
                    $(this).fileupload('option', 'done').call(this, null, {
                        result: result
                    });
                });
            });
            return $html;
        },
        update: function(o) {
            tmp_photos = $("#fileupload").find(".template-download .name a");

            if (tmp_photos.length > 0) {
                var arr = [];
                for (var i = 0; i < tmp_photos.length; i++) {
                    var id = $(tmp_photos[i]).attr("download").replace(/^(\d+)\.[.]+$/, "$1");
                    arr.push(id.replace(/\..+$/g, ""));
                };


                var data = {
                    pids: arr,
                    name: this.property,
                    id: o.get("id")
                };
                $.ajax({
                    url: '../bn/index.php/photo/addto',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    async: false,
                    complete: function(xhr, textStatus) {
                        //called when complete
                        // alert("complete");
                    },
                    success: function(data, textStatus, xhr) {
                        //called when successful
                        // alert("success");
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        //called when there is an error
                        // alert("error");
                    }
                });

            }
        }
    };


    //	
    Backbone.UIToolBox.photo.copyto = function(old_name, old_id, new_name, new_id) {
        var that = this;
        var data = {
            old_name: old_name,
            old_id: old_id, // this is product id
            new_name: new_name,
            new_id: new_id // this is shelves id
        };

        $.ajax({
            url: '../bn/index.php/photo/copyto',
            type: 'POST',
            dataType: 'json',
            data: data,
            async: false,
            complete: function(xhr, textStatus) {
                // get all photo from database
                $.ajax(that.data).always(function() {
                    $(this).removeClass('fileupload-processing');
                }).done(function(result) {

                    $(this).fileupload('option', 'done')
                        .call(this, null, {
                            result: result
                        });
                });
            },
            success: function(data, textStatus, xhr) {
                //called when successful
                // alert("success");
            },
            error: function(xhr, textStatus, errorThrown) {
                //called when there is an error
                // alert("i am error");
            }
        });
    };
})(jQuery);

(function($) {

    var file_template;

    var file_tpl = '#tpl_jqfu';
    /*
		 _.defaults({
          property:"Product.ProductPhoto",
          idAttribute:"ProductID"
        },Backbone.UIToolBox.file)
	 */
    Backbone.UIToolBox.file = {
        useTemplate: false,
        view: function(o) {
            var that = this;
            if (!file_template)
                file_template = Handlebars.compile($(file_tpl).html());
            var tid = o.get(this.idAttribute);
            var data = {}
            if (typeof tid !== 'undefined') {
                data.tid = tid;
                data.name = this.property;
            }
            var html = file_template();
            var $html = $(html);
            $html.ready(function() {
                $html.fileupload({
                    url: $.config.server_url + '/../uf'
                });
                $html.addClass('fileupload-processing');
                $html.fileupload('option', {
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    maxFileSize: 5000000
                    // acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
                });

                that.data = {
                    url: $html.fileupload('option', 'url'),
                    dataType: 'json',
                    context: $html[0],
                    data: data
                };
                $.ajax(that.data).always(function() {
                    $(this).removeClass('fileupload-processing');
                }).done(function(result) {
                    $(this).fileupload('option', 'done').call(this, null, {
                        result: result
                    });
                });
            });
            return $html;
        },
        update: function(o) {
            tmp_photos = $("#fileupload").find(".template-download .name a");

            if (tmp_photos.length > 0) {
                var arr = [];
                for (var i = 0; i < tmp_photos.length; i++) {
                    var id = $(tmp_photos[i]).attr("download").replace(/^(\d+)\.[.]+$/, "$1");
                    arr.push(id.replace(/\..+$/g, ""));
                };


                var data = {
                    pids: arr,
                    name: this.property,
                    id: o.get("id")
                };
                $.ajax({
                    url: '../bn/index.php/photo/addto',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    async: false,
                    complete: function(xhr, textStatus) {
                        //called when complete
                        // alert("complete");
                    },
                    success: function(data, textStatus, xhr) {
                        //called when successful
                        // alert("success");
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        //called when there is an error
                        // alert("error");
                    }
                });

            }
        }
    };


    //	
    Backbone.UIToolBox.photo.copyto = function(old_name, old_id, new_name, new_id) {
        var that = this;
        var data = {
            old_name: old_name,
            old_id: old_id, // this is product id
            new_name: new_name,
            new_id: new_id // this is shelves id
        };

        $.ajax({
            url: '../bn/index.php/photo/copyto',
            type: 'POST',
            dataType: 'json',
            data: data,
            async: false,
            complete: function(xhr, textStatus) {
                // get all photo from database
                $.ajax(that.data).always(function() {
                    $(this).removeClass('fileupload-processing');
                }).done(function(result) {

                    $(this).fileupload('option', 'done')
                        .call(this, null, {
                            result: result
                        });
                });
            },
            success: function(data, textStatus, xhr) {
                //called when successful
                // alert("success");
            },
            error: function(xhr, textStatus, errorThrown) {
                //called when there is an error
                // alert("i am error");
            }
        });
    };
})(jQuery);

(function($) {
    /*
	_.defaults({
	  title:'權重'
          property:"Weight",
	  default_value:50     //optional
        },Backbone.UIToolBox.slider)
	 */
    Backbone.UIToolBox.slider = {
        default_value: 50,
        view: function(o) {
            var that = this;
            var v = o.get(this.property) || this.default_value;
            var html = '<div style="width:260px"><div id="' + this.property + '-amount">' + v + '</div><div class="slider-yellow slider"></div></div>';
            var $html = $(html);
            $html.ready(function() {
                $html.find(".slider").slider({
                    value: v,
                    orientation: "horizontal",
                    range: "min",
                    animate: true,
                    slide: function(event, ui) {
                        $("#" + that.property + "-amount").html(ui.value);
                    }
                });
            });
            return $html;
        },
        value: function(o) {
            var that = this;
            return $("#" + that.property + "-amount").html();
        },
        setValue: function(o) {

        }
    }
})(jQuery);


(function($) {

    /*
_.defaults({
    title: '分類',
    property: 'ProductCategoryID',
    term: {
		url: '../bn/index.php/news_term/gen'
    },
    set: {
		url: '../bn/index.php/news_termset/gen'
    }
})
 */
    Backbone.UIToolBox.multichoice = {

        default_term: {
            //        url: '../bn/index.php/news_term/gen',
            idAttribute: "ID",
            data: {
                fields: ["ID", "Name"]
            }
        },
        default_set: {
            //        url: '../bn/index.php/news_termset/gen',
            idAttribute: "ID",
            ContentIDMap: {
                "ContentField": "ID",
                "SetField": "ContentID"
            },
            TermIDMap: {
                "TermField": "ID",
                "SetField": "TermID"
            }
        },
        view: function(o) {
            //抓取 term 資料
            var that = this;

            that.set = _.defaults(that.set, that.default_set);
            that.term = _.defaults(that.term, that.default_term);


            that.fetch_term();
            var _html = "";
            var json = that.term_collection.toJSON();
            for (var i = 0; i < json.length; i++) {
                _html += '<input type="checkbox" name="' + this.property + '" value="' + json[i][that.set.TermIDMap.TermField] + '">' + json[i][that.term.data.fields[1]] + '&nbsp';
            }
            return _html;
        },
        fetch_term: function(o) {
            var that = this;
            if (!that.term_collection) {
                that.term_collection = new Backbone.Datagrid.getCollectionByUrl(that.term.url, that.term.idAttribute);
            }

            that.term_collection.fetch({
                async: false,
                data: that.term.data
            });

            that.term_info = that.term_collection.pop();
        },
        fetch_set: function(m) {
            var that = this;
            if (!that.set_collection) {
                that.set_collection = new Backbone.Datagrid.getCollectionByUrl(that.set.url, that.set.idAttribute);
            }

            var id = m.get(that.set.ContentIDMap.ContentField);

            if (!_.isUndefined(id)) {
                var where = {};
                where[that.set.ContentIDMap.SetField] = id;
                // where = _.defaults(where,data.where);
                // data.where = where;

                that.set_collection.fetch({
                    async: false,
                    data: {
                        where: where
                    }
                });

                that.set_info = that.set_collection.pop();
            }
        },
        value: function() {

        },
        setValue: function(m) {
            var that = this;
            that.fetch_set(m);
            var json = that.set_collection.toJSON();
            for (var i = 0; i < json.length; i++) {
                $('input[name="' + this.property + '"][value="' + json[i][that.set.TermIDMap.SetField] + '"]').prop("checked", true);
            }
        },
        update: function(m) {
            var that = this;
            var id = m.get(that.set.ContentIDMap.ContentField);

            that.fetch_set(m);
            // var len = this.collection2.length;
            var len = that.set_collection.length;

            // remove old relation first
            for (var i = 0; i < len; i++) {
                var remove_model = this.set_collection.pop();
                remove_model.destroy({}, {
                    async: false
                });
            };

            //
            var target = $('input[name="' + this.property + '"][type="checkbox"]:checked');

            var update_model = Backbone.Model.extend({
                urlRoot: that.set.url,
                idAttribute: that.set.idAttribute
            });

            for (var i = 0; i < target.length; i++) {
                var v = $(target[i]).val();

                var update = {};
                update[that.set.ContentIDMap.SetField] = id;
                update[that.set.TermIDMap.SetField] = v;

                model = new update_model(update);
                model.save({}, {
                    async: false
                });
            };
        }
    };

})(jQuery);

(function($) {
    my = {
        isCid: function(id) {
            tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"
            A1 = new Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3);
            A2 = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5);
            Mx = new Array(9, 8, 7, 6, 5, 4, 3, 2, 1, 1);

            if (typeof id !== "string" || id.length != 10) return false;

            i = tab.indexOf(id.charAt(0));

            if (i == -1) return false;
            sum = A1[i] + A2[i] * 9;

            for (i = 1; i < 10; i++) {
                v = parseInt(id.charAt(i));
                if (isNaN(v)) return false;
                sum = sum + v * Mx[i];
            }

            if (sum % 10 != 0) return false;
            return true;
        },
        isEmail: function(email) {
            var regex = /^\w+((\-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|\-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
            return typeof email === 'string' && email.match(regex);
        },
        isPhone: function(phone) {
            var regex = /^[0-9][0-9\-]+[0-9]$/;
            return typeof phone === 'string' && phone.match(regex);
        },
        isBankAccount: function(bankaccount) {
            var regex = /^[0-9][0-9\-]+[0-9]$/;
            return typeof bankaccount === 'string' && bankaccount.match(regex);
        },
        isDigitString: function(digit) {
            var regex = /^\d+$/;
            return typeof digit === 'string' && digit.match(regex);
        },
        isNotEmpty: function(str) {
            return str;
        },
        isEmpty: function(str) {
            return !str;
        }
    };
})(jQuery);