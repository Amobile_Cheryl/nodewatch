$.fn.dataTableExt.sErrMode = 'throw';
(function($) {
    var B = Backbone;
    var H = Handlebars;

    var container_id = "tpl_device_capacitive";
    var tpl_capacitive_html = '<script type="text/template" id="' + container_id + '">' + '{{#each capacitive_category}}' + '<h1 class="appm_h1">{{capacitive_type_name}}</h1>' + '<div class="appm_property_bg col-lg-12">' + '{{#each this.capacitive_type_items}}' + '<div class="col-md-2"><a href="#" id="capacitive-{{this.item_id}}"><img src="{{this.item_img}}" onerror="imgLoadError(this,\'{{this.item_name}}\');"><h5>{{this.item_name}}</h5></a></div>' + '{{else}}' + '<span>No content<span>' + '{{/each}}' + '<br class="clear" />' + '</div>' + '{{/each}}' + '</script>';
    var tpl_capacitive = H.compile(tpl_capacitive_html);

    B.CapacitiveCategoryView = B.View.extend({
        initialize: function(options) {
            var t = this;
            t.options = options;
            t.id = options.id;
            t.model = options.model;

            var html = tpl_capacitive(t.model);
            $("#capacitive").html(html);
            $("#capacitive a").click(function(item) {
                show_capacitive_diaglog(t.id);
                return false;
            });
        },
        show_capacitive_diaglog: function(id) {
            console.log('show_capacitive_diaglog id:' + id);
        }
    });
})(jQuery);

(function($) {
    var B = Backbone;
    var H = Handlebars;

    B.ProgressBar = B.View.extend({
        initialize: function(options) {
            this.render(options);
        },
        render: function(options) {
            var tpl_html = '<span class="col-xs-6 col-sm-6 col-md-12 col-lg-12">' + '<span class="text"> {{label}}: {{remark}}' + '<span class="pull-right">{{data}}</span>' + '</span>'
                //+'<a href="#ajax/device_list.html/eventSetting_list/{{id}}">'
                + '<a href="{{link}}">' + '<div class="progress left">' + '<div class="progress-bar {{#if color}}{{color}}{{else}}bg-color-blueDark{{/if}}" style="width: {{percentage}}%;">{{percentage}}%</div>' + '</div>' + '</a>' + '</span>';
            var template = H.compile(tpl_html);
            this.$el.append(template(options));
        }
    });

    /* Usage */
    /*
    var pg = new Backbone.ProgressBar({
    	el:			$("#myTabContent .row"),
    	id:			1,
    	link:		'#ajax/device_list.html/eventSetting_list/1',
    	label:		'CPU',
    	data:		'50/200',
    	percentage:	50/200*100,
    	color:		'#00FF00'
    });
    */
})(jQuery);

(function($) {
    var B = Backbone;

    var id = Math.floor((Math.random() * 1000) + 1);
    var tpl_chart = '<div id="donut-graph-' + id + '" class="chart no-padding"></div>';

    B.DountGraphView = B.View.extend({
        initialize: function(options) {
            var t = this;
            t.$el.html(tpl_chart);
        }
    });

    if ($('#' + id).length) {
        Morris.Donut({
            element: id,
            data: model,
            colors: [
                '#0BA462',
                '#FC3',
                '#333',
                '#F3C',
                '#3FC'
            ],
            formatter: function(x) {
                return x + "" }
        });
    }
})(jQuery);

(function($) {
    var tpl_gv_html = '<article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">' + '<div class="jarviswidget jarviswidget-color-darken" id="wid-id-0" data-widget-editbutton="false" style="margin-bottom: 0px;">' + '<header>'
        /*+'<span class="widget-icon">'
        	+'<i class="fa fa-table"></i>'
        +'</span>'*/
        + '<h2>' + '<span data-i18n={{title}}></span>&nbsp;' + '</h2>' + '</header>' + '<div>' + '<div class="widget-body no-padding">' + '{{#if noToolBar}}' + '{{else}}' + '<div class="widget-body-toolbar">' + '</div>' + '{{/if}}' + '<table id="datatable_tabletools_{{count}}" class="table table-striped table-bordered table-hover">' + '</table>' + '</div>' + '</div>' + '</div>' + '</article>';
    var B = Backbone;
    var H = Handlebars;
    var tpl_table = H.compile(tpl_gv_html);
    var table_id = '#datatable_tabletools';

    // Plug-in
    jQuery.fn.dataTableExt.oApi.fnProcessingIndicator = function(oSettings, onoff) {
        if (onoff === undefined) {
            onoff = true;
        }
        this.oApi._fnProcessingDisplay(oSettings, onoff);
    };
    //

    var count = 0;
    B.GridView = B.View.extend({
        select: [],
        initialize: function(options) {
            var t = this;

            t.options = options;

            t.columns = options.columns;
            t.title = options.title || "清單列表";
            t.isAdd = options.isAdd;
            //t.isUpload = options.isUpload;
            t.sort = options.sort; // default sort
            t.order = options.order;
            t.filter = options.filter;
            t.buttons = options.buttons || [];
            //t.paginated = options.paginated || true;
            // Holisun
            // console.log("[Search] t.filter="+t.filter);
            // console.log("[Search] t.filter.search.operatingSystemName="+t.filter.search.operatingSystemName);
            // if (! _.isUndefined(t.filter)){
            //     t.filter.search = {};
            // } 

            if (_.isUndefined(options.paginated)) {
                t.paginated = true;
            } else {
                t.paginated = options.paginated;
            }
            t.page = options.page || 1;
            t.per_page = options.per_page || $.config.ItemsPerPage;

            //---Alvin modify
            if (t.isAdd == true || t.buttons.length > 0)
                t.$el.html(tpl_table({ title: t.title, count: count }));
            else
                t.$el.html(tpl_table({ title: t.title, count: count, noToolBar: true }));
            //---

            t.table_id = table_id + '_' + count;
            count++;
            t.render_datatable();
            // where_in
            // filter_in
            // control datatables

            /*
			i18n.init(function(t){
                $('[data-i18n]').i18n();
            });
			*/
        },
        getRequest: function() {
            var t = this;
            return function(sSource, aoData, fnCallback, oSettings) {
                // console.log("call _request");
                var page = t.page;
                var per_page = t.per_page;

                var map = {}

                for (var i = 0; i < aoData.length; i++) {
                    map[aoData[i].name] = aoData[i].value;
                };

                //console.log(map);

                if (!_.isUndefined(map["sEcho"]))
                    var sEcho = map["sEcho"];

                // Fix 不分頁時 傳值為-1
                if (!_.isUndefined(map["iDisplayLength"]))
                    per_page = map["iDisplayLength"];

                if (!_.isUndefined(map["iDisplayStart"]))
                    page = map["iDisplayStart"] / per_page + 1;

                if (!_.isUndefined(map["iSortCol_0"]) && !_.isUndefined(map["sSortDir_0"])) {
                    var sortField = map["iSortCol_0"];
                    var sortDir = map["sSortDir_0"];
                }

                if (!_.isUndefined(map["sSearch"]) && map["sSearch"] !== "")
                    var sSearchWord = map["sSearch"];

                var query_para = {};
                //console.log("[Search] 2 _.isUndefined(t.filter)=" + _.isUndefined(t.filter));
                //console.log("[Search] 2 t.filter.search.operatingSystemName=" + t.filter.search.operatingSystemName);
                if (_.isUndefined(t.filter) === false && _.isUndefined(t.filter.search) === false && _.isObject(t.filter.search)) {
                    var sSearchString = "";
                    var bHasSearchCondition = false;
                    $.each(t.filter.search, function(index, value) {
                        sSearchString += index + ":" + value + ",";
                        bHasSearchCondition = true;
                    });

                    if (bHasSearchCondition) {
                        query_para.search = sSearchString.substring(0, sSearchString.lastIndexOf(","));
                    }
                    console.log("[Search]] query_para.search=" + query_para.search);
                }
                //query_para.search = "operatingSystemName:Android"+",label:5";


                //if (t.options.AD.api === 'cctech') {
                //    query_para["page"] = page;
                //    query_para["per_page"] = per_page;
                //} else {
                if (SpringPaggingMode || TempDemoMode) {
                    // Spring Pagging
                    query_para["pageNumber"] = page;
                    query_para["pageSize"] = per_page;
                } else {
                    query_para["page"] = page;
                    query_para["size"] = per_page;
                }
                //}

                // console.log(t.sort);
                if (_.isUndefined(t.sort)) {
                    if (sortField !== "" && sortDir !== "" && !_.isUndefined(t.columns[sortField].property)) {
                        if (t.columns[sortField].sortable) {
                            if (SpringPaggingMode || TempDemoMode) {
                                // Spring Pagging
                                query_para["pageSort"] = t.columns[sortField].property + "," + sortDir;
                            } else {
                                // Original
                                query_para["sortDir"] = sortDir;
                                query_para["sortField"] = t.columns[sortField].property;
                                //// query_para["order"] = t.columns[sortField].property + ' ' + sortDir;
                                //// query_para["sortDir"] = sortDir;
                                //// query_para["sortField"] = t.columns[sortField].property;
                            }
                        } else {
                            if (SpringPaggingMode || TempDemoMode) {
                                // Spring Pagging
                                query_para["pageSort"] = "id";
                            } else {
                                // Original
                                query_para["sortDir"] = "asc";
                                query_para["sortField"] = "id";
                            }
                        }
                        /*
                        } else if (!_.isUndefined(t.sort)) {
                        	query_para["sortDir"] = t.sort.sortDir;
                        	query_para["sortField"] = t.sort.column;
                        	// query_para["order"] = t.sort.column + ' ' + t.sort.sortDir;
                        	// query_para["sortDir"] = t.sort.sortDir;
                        	// query_para["sortField"] = t.sort.column;
                        */
                    } else {
                        if (SpringPaggingMode || TempDemoMode) {
                            // Spring Pagging
                            query_para["pageSort"] = "id";
                        } else {
                            // Original
                            query_para["sortDir"] = "asc";
                            query_para["sortField"] = "id";
                        }
                    }
                } else {
                    // Get default sort
                    if (SpringPaggingMode || TempDemoMode) {
                        // Spring Pagging
                        query_para["pageSort"] = t.sort.column + "," + t.sort.sortDir;
                    } else {
                        // Original
                        query_para["sortDir"] = t.sort.sortDir;
                        query_para["sortField"] = t.sort.column;
                    }
                }

                if (!_.isUndefined(sSearchWord)) {
                    // query_para["or_where"] = "";
                    var or_like = {};
                    for (var i = 0; i < t.columns.length; i++) {
                        if (t.columns[i].filterable === true) {
                            or_like[t.columns[i].property] = sSearchWord;
                        }
                    }
                    query_para["or_like"] = or_like;
                }

                //=====================================================================================================
                // Prepare object (data/success/error) for Collection.fetch()
                //=====================================================================================================
                var options = {
                    data: query_para,
                    timeout: 5 * 1000
                };

                options.success = function(c, response, options) {
                    // Kenny modify at 2015/01/27 for Spring Pagging
                    var amount;
                    var json;
                    var ilen;
                    var jlen;
                    if (SpringPaggingMode) {
                        if (typeof response.pageMetaData === "undefined") {
                            amount = 15;
                        } else {
                            var pageMetaData = response.pageMetaData;
                            amount = pageMetaData.resourceAmount;
                        }
                        json = response.content;
                        ilen = json.length;
                        jlen = t.columns.length;
                    } else {
                        var info = c.at(c.length - 1);
                        c.remove(info);
                        info = info.toJSON();
                        json = c.toJSON();
                        ilen = json.length;
                        jlen = t.columns.length;
                        //if (t.options.AD.api === 'cctech') {
                        //	amount = info.count;
                        //} else {
                        amount = info.amount;
                        //}
                    }

                    var data = {
                        iTotalRecords: amount,
                        iTotalDisplayRecords: amount,
                        sEcho: sEcho,
                        aaData: []
                    };

                    /*
                    data.aaData = json;
                    */

                    /*
                    var arr = [];

                    for(var i = 0; i< ilen ;i++) {
                    	var item = [];
                    	item.push(json[i]);
                    	arr.push(item);
                    }

                    data.aaData = arr;
                    */


                    var arr = [];

                    for (var i = 0; i < ilen; i++) {
                        var item = [];
                        for (var j = 0; j < jlen; j++) {
                            var coldef = t.columns[j];
                            if (!_.isUndefined(coldef.property)) {
                                item.push(json[i][coldef.property]);
                            } else
                                item.push(null);

                        }
                        item.push(json[i]);
                        arr.push(item);
                    }

                    data.aaData = arr;

                    // console.log(data);
                    fnCallback(data);

                    /*$('body,html').animate({
                        scrollTop: 0
                    }, 500);*/
                };

                options.error = function(collection, response, options) {
                    $.smallBox({
                        title: i18n.t("ns:Message.ErrorOccurred"),
                        content: "<i>" + i18n.t("ns:Message.ErrorTryAgain") + "</i>",
                        // title : "An error occurred on the server",
                        // content : "<i>Please try again in a minute.</i>",
                        color: "#B36464",
                        iconSmall: "fa fa-times bounce animated",
                        timeout: 6000
                    });
                    $(t.table_id).dataTable().fnProcessingIndicator(false);
                };
                //async function
                t.collection.fetch(options);
            };
        },
        render_datatable: function(argument) {
            var t = this;
            // console.log(t);
            var aoColumnDefs = [];

            /*
			i18n.init(function(t){
                $('[data-i18n]').i18n();
            });
			*/

            for (var i = 0; i < t.columns.length; i++) {
                if (_.isUndefined(t.columns[i].callback)) {
                    aoColumnDefs.push({
                        "sName": t.columns[i].property,
                        "sTitle": i18n.translate(t.columns[i].title),
                        "sWidth": t.columns[i].width,
                        "bVisible": !_.isUndefined(t.columns[i].visible) ? t.columns[i].visible : true,
                        "bSortable": t.columns[i].sortable,
                        "bSearchable": t.columns[i].filterable,
                        //"asSorting": ["desc"],
                        "aTargets": [i],
                        "mRender": function(argument) {
                            var index = argument;
                            return function(data, type, full) {
                                // console.log("[data]" + data);
                                if (t.columns[index].property) {
                                    //return full[full.length - 1][t.columns[index].property];
                                    // Prevent XSS Attack
                                    return _.escape(full[full.length - 1][t.columns[index].property]);
                                } else {
                                    return '';
                                }
                            };
                        }(i)
                    });
                } else {
                    aoColumnDefs.push({
                        "sName": t.columns[i].property,
                        "sTitle": i18n.translate(t.columns[i].title),
                        "sWidth": t.columns[i].width,
                        "bVisible": !_.isUndefined(t.columns[i].visible) ? t.columns[i].visible : true,
                        "bSortable": t.columns[i].sortable,
                        //"asSorting": ["desc"],
                        "aTargets": [i],
                        "mRender": function(argument) {
                            var index = argument;
                            return function(data, type, full) {
                                // console.log("callback data["+index+"]="+data);
                                // console.log("callback full["+index+"]="+full);
                                return t.columns[index].callback(full[full.length - 1]);
                            };
                        }(i)

                    });
                }
            };

            /*
            {
            // `data` refers to the data for the cell (defined by `mData`, which
            // defaults to the column being worked with, in this case is the first
            // Using `row[0]` is equivalent.
                "mRender": function ( data, type, row ) {
                    return data +' '+ row[3];
                },
                "aTargets": [ 0 ]
            },*/
            // "_fnApplyColumnDefs": _fnApplyColumnDefs,

            var oTableTools = {
                aButtons: []
            };

            if (t.isAdd === true) {
                oTableTools.aButtons.push({
                    "sExtends": "text",
                    "sButtonText": "<i class='fa fa-plus'></i>&nbsp; " + i18n.translate("ns:Menu.Add"),
                    "sButtonClass": "add-btn btn-default txt-color-white bg-color-blue"
                });
            }

            // if (t.isUpload === true) {
            //     oTableTools.aButtons.push({
            //         "sExtends" : "text",
            //         "sButtonText" : "<i class='fa fa-cloud-upload'></i>&nbsp; Upload",
            //         "sButtonClass" : "upload-btn s2-btn btn-default txt-color-white bg-color-blue"
            //     });
            // }

            for (var i = 0; i < t.buttons.length; i++) {
                oTableTools.aButtons.push(t.buttons[i]);
            };

            var sFilterPlaceholder = "";
            var tmp = [];
            for (var i = 0; i < t.columns.length; i++) {
                if (t.columns[i].filterable) {
                    tmp.push(' ' + t.columns[i].title + ' ');
                }
            };

            sFilterPlaceholder += tmp.join("or");

            // alert(t.table_id + "=" + $(t.table_id).length);            
            t.dt = $(t.table_id).dataTable({
                "bProcessing": true,
                "bServerSide": true,
                "fnServerData": t.getRequest(),
                "bPaginate": t.paginated,
                "sPaginationType": "bootstrap_full",
                "order": t.order,
                "bFilter": _.isObject(t.filter), // true: to display filter on table
                "bStateSave": false,
                "aoColumnDefs": aoColumnDefs,
                //"sDom": "<'dt-top-row'Tlf>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>",
                //"sDom": "<'dt-top-row'Tl>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-6'i><'col-sm-6 text-right'p>>", // 2015/01/28 Works & Final
                "sDom": "<'dt-top-row'Tl>r<'dt-wrapper't><'dt-row dt-bottom-row'<'row'<'col-sm-3'i><'col-sm-6 text-right'p><'col-sm-3'<'#jumpPageContent'>>>",
                "oTableTools": oTableTools,
                "iDisplayLength": t.per_page,
                "bLengthChange": false, // hide datatable page menu
                "oLanguage": {
                    "oPaginate": {
                        "sFirst": i18n.t("ns:DataTables.First"),
                        "sPrevious": i18n.t("ns:DataTables.Previous"),
                        "sNext": i18n.t("ns:DataTables.Next"),
                        "sLast": i18n.t("ns:DataTables.Last")
                    },
                    "sEmptyTable": i18n.t("ns:DataTables.EmptyTable"),
                    "sInfo": i18n.t("ns:DataTables.Showing") + "_START_ " + i18n.t("ns:DataTables.to") + " _END_ " + i18n.t("ns:DataTables.of") + " _TOTAL_ " + i18n.t("ns:DataTables.Entry"),
                    "sInfoEmpty": i18n.t("ns:DataTables.ShowNoEntry"),
                    "sLoadingRecords": i18n.t("ns:DataTables.Loading"),
                    "sProcessing": i18n.t("ns:DataTables.Process"),
                    "sSearch": i18n.t("ns:DataTables.Search"),
                    "sFilterPlaceholder": sFilterPlaceholder
                }
            });
            $("#jumpPageContent").css({ 'line-height': '25px', 'margin-top': '4px' });
            $("#jumpPageContent").append('<font data-i18n="ns:System.Page">Page</font>:&nbsp;<input type="number" min="1" id="jumpPage" size="7" /><div id="jumpPageBtn" class="btn btn-primary btn-sm" style="margin-top: -2px;border-radius: 0px 2px 2px 0px;"><i class="fa fa-mail-forward"></i></div>');
            $("#jumpPageBtn").on("click", function() {
                var page = $("#jumpPage").val();
                if (page > 0) {
                    page--;
                }
                t.dt.fnPageChange(page);
                $("#jumpPage").val("");
            });

            $(t.table_id + " thead th").click(function() {
                console.log("[Search] t.table_id=" + t.table_id);
                console.log("[Search] t=" + t);
                // Sort
                t.sort = undefined; // Clear default sort
                //t.dt.fnFilter(this.value, t.dt.oApi._fnVisibleToColumnIndex(t.dt.fnSettings(), $("thead th").index(this)));
            });
            // alert(t.dt.find("#datatable_tabletools_filter input").length);
            // t.dt.find("#datatable_tabletools_filter input").prop("placeholder", "Email");

            //=========================================================================================================
            // Create search input fields
            //=========================================================================================================
            console.log("[Search] t.filter=" + t.filter);
            console.log("[Search] _.isObject(t.filter)=" + _.isObject(t.filter));
            if (_.isObject(t.filter)) {
                var sHtmlSearch = "";
                $(t.table_id + " thead th").each(function() {
                    var title = $(t.table_id + " thead th").eq($(this).index()).text();
                    //console.log("[Search] title=" + title);
                    //$(this).html('<input type="text" placeholder="Search '+title+'" />');
                    var col = _.find(t.columns, function(column) {
                        if (i18n.t(column.title) === title) {
                            //console.log("[Search] column=" + column);
                            return column;
                        }
                    });
                    //sHtmlSearch += '<td style="text-align:center;">';
                    sHtmlSearch += '';
                    if (!_.isUndefined(col)) {
                        if (col.filterable) {
                            var searchName = (_.isUndefined(col.searchName)) ? "" : col.searchName;
                            //console.log("[Search] searchName=" + searchName);
                            var searchDom = '<span class="searchbox_1">' + '<b>' + title + ' </b>';
                            searchDom += '<input name="' + searchName + '" type="text" placeholder="" class="search_1" /> ';
                            searchDom += '<i style="color:red" class="fa fa-remove"></i></span>';
                            //searchDom += '<button type="submit" class="submit_1" value="search">&nbsp;</button>';
                            if (!_.isUndefined(col.searchDom)) {
                                if (_.isFunction(col.searchDom)) {
                                    searchDom = col.searchDom();
                                }
                            }
                            sHtmlSearch +=
                                //'<div class="input-group">'
                                //+'<input name="'+searchName+'" type="text" placeholder="'+i18n.t("ns:DataTables.Search")+' '+title+'" class="form-control" />'
                                searchDom
                                //+ '<span class="input-group-addon">'
                                //+ '<i class="fa fa-search"></i>'
                                //+ '</span>'
                                //+ '<span class="input-group-addon">'
                                //+ '<i class="fa fa-remove"></i>'
                                //+ '</span>'
                                //+ '</div>';
                        }
                    }
                    //sHtmlSearch += '</td>'
                });
                // console.log("sHtmlSearch="+sHtmlSearch);
                // $(t.table_id + " thead").append("<tr>" + sHtmlSearch + "</tr>");	
                $(".widget-body-toolbar").prepend(sHtmlSearch);
                $(".widget-body-toolbar").css({
                    height: '70px'
                });
                //=====================================================================================================
                // Events Binding
                //=====================================================================================================				
                $(".widget-body-toolbar span i").hover(function(oEvent) {
                    $(this).css('cursor', 'pointer');
                });

                $(".widget-body-toolbar span i").on('click', function(oEvent) {
                    if ($(this.parentNode).children("input").length > 0) {
                        var $oDomInput = $(this.parentNode).children("input").eq(0);
                        var sDomInputValue = $oDomInput.val();
                        if (_.isEmpty(sDomInputValue) === false) {
                            $oDomInput.val("");
                            $oDomInput.trigger('change');
                        }
                    } else if ($(this.parentNode).children("select").length > 0) {
                        var $oDomSelect = $(this.parentNode).children("select").eq(0);
                        var sDomSelectValue = $oDomSelect.find("option:selected").text();
                        if (_.isEmpty(sDomSelectValue) === false) {
                            $oDomSelect.find("option[value='']").prop("selected", true);
                            $oDomSelect.trigger('change');
                        }
                    }
                });

                $(".widget-body-toolbar span input").on('change', function(oEvent) {
                    console.log("[Search] Input change");
                    if (_.isNull(this.value) || _.isEmpty(this.value)) {
                        var objStr = 't.filter.search.' + this.name;
                        eval('delete ' + objStr);
                    } else {
                        eval('t.filter.search.' + this.name + '= this.value');
                    }
                    t.dt.fnDraw();
                });

                $(".widget-body-toolbar span select").on('change', function() {
                    console.log("[Search] select change=" + this.options[this.selectedIndex].value);
                    if (_.isNull(this.value) || _.isEmpty(this.value)) {
                        var objStr = 't.filter.search.' + this.name;
                        eval('delete ' + objStr);
                    } else {
                        eval('t.filter.search.' + this.name + '= this.value');
                    }
                    t.dt.fnDraw();
                });
                $(t.table_id + " thead tr input").on('change', function() {
                    console.log("[Search] Input change");
                    //t.filter = {};
                    //t.filter.name = this.value;
                    if (_.isNull(this.value) || _.isEmpty(this.value)) {
                        var objStr = 't.filter.search.' + this.name;
                        eval('delete ' + objStr);
                    } else {
                        eval('t.filter.search.' + this.name + '= this.value');
                    }
                    t.dt.fnDraw();
                });

                $(t.table_id + " thead tr select").on('change', function() {
                    console.log("[Search] select change=" + this.options[this.selectedIndex].value);
                    if (_.isNull(this.value) || _.isEmpty(this.value)) {
                        var objStr = 't.filter.search.' + this.name;
                        eval('delete ' + objStr);
                    } else {
                        eval('t.filter.search.' + this.name + '= this.value');
                    }
                    t.dt.fnDraw();
                });

                $(t.table_id + " thead tr .fa-search").parent().on('click', function() {
                    console.log("[Search] search button CLICK");
                    console.log("[Search] 3 t.filter.search.operatingSystemName=" + t.filter.search.operatingSystemName);
                    // t.filter.search={};
                    console.log("[Search] t.filter.search=" + t.filter.search);
                    var searchName = $(this.parentNode.children).attr("name");
                    console.log("[Search] searchName=" + searchName);
                    var searchVal = $(this.parentNode.children).val();
                    console.log("[Search] searchVal=" + searchVal);
                    if (_.isNull(searchVal) || _.isEmpty(searchVal)) {
                        var objStr = 't.filter.search.' + searchName;
                        eval('delete ' + objStr);
                    } else {
                        eval('t.filter.search.' + searchName + '= searchVal');
                        console.log("[Search] t.filter.search=" + t.filter.search.label);
                    }
                    t.dt.fnDraw();
                });

                $(t.table_id + " thead tr .fa-remove").parent().on('click', function() {
                    console.log("[Search] remove");
                    var searchName = $(this.parentNode.children).attr("name");
                    var searchVal = $(this.parentNode.children).val("");
                    var objStr = 't.filter.search.' + searchName;
                    eval('delete ' + objStr);
                    t.dt.fnDraw();
                });
            }
            /*
            if(t.filter){
            	var tplFilterHtml = "<tr class=\"second\">"
            						+"{{#each columns}}"
            							+"<td>"
            								+"{{#if this.filterable}}"
            									+"<label>"
            										+"<input type=\"text\" name=\"search_{{this.property}}\" class=\"search_init\">"
            									+"</label>"
            								+"{{/if}}"
            							+"</td>"
            						+"{{/each}}"
            					+"</tr>";
            	var filterColumns = {
            		"columns" : t.columns
            	};
            	debugger;
            	// 预编译模板
            	var tplFilter = Handlebars.compile(tplFilterHtml);
            	// 模版與資料互相匹配
            	var inputHtml = tplFilter(filterColumns);
            	$(t.table_id+" thead tr").after(inputHtml);
            	*/
            /*
				var tplFilterHtml = "<tfoot><tr class=\"second\">"
									+"{{#each columns}}"
										+"<th>"
											+"{{#if this.bVisible}}"
												+"{{this.sName}}Filter"
											+"{{/if}}"
										+"</th>"
									+"{{/each}}"
								+"</tr></tfoot>";
				var filterColumns = {
					"columns" : t.columns
				};
				debugger;
				var tplFilter = Handlebars.compile(tplFilterHtml);
				var inputHtml = tplFilter(filterColumns);
				$(t.table_id+" thead").after(inputHtml);
				
				$(t.table_id+" thead input").keyup(function() {
					t.dt.fnFilter(this.value, t.dt.oApi._fnVisibleToColumnIndex(t.dt.fnSettings(), $("thead input").index(this)));
				});
				$(t.table_id+" thead input").each(function() {
					this.initVal = this.value;
				});
				$(t.table_id+" thead input").focus(function() {
					if (this.className == "search_init") {
						this.className = "";
						this.value = "";
					}
				});
				$(t.table_id+" thead input").blur(function() {
					if (this.value == "") {
						this.className = "search_init";
						this.value = this.initVal;
					}
				});

			}
			*/

        },
        per_page: function(target) {

        },
        removeOne: function(target) {
            var id = $(target).data("id");
            var model = this.collection.get(id);
            model.destroy({
                async: false
            });
            this.datagrid.refresh();
            this.select = [];
            this.trigger("change:select", this.select);
        },
        remove: function() {
            // alert(this.select);
            var that = this;
            // alert(this.select);
            $.each(this.select, function(i, v) {
                // alert(v);
                var model = that.collection.get(v);
                model.destroy({
                    async: false
                });
            });
            this.select = [];
            this.datagrid.refresh();
            this.trigger("change:select", this.select);
        },
        /*
        _clearSelect: function() {
            this.select = [];
            this.trigger("change:select", this.select);
        },

        _select: function(event, target) {
            $t = $(target);
            if ($t.val() === "all") {
                if (typeof $t.attr('checked') === 'undefined') {
                    this.$el.find("input[type='checkbox']").removeAttr('checked');
                } else {
                    this.$el.find("input[type='checkbox']").attr('checked', $t.attr('checked'));
                }
            }

            var array = [];
            this.$el.find("input[type='checkbox'][checked][value!='all']").each(function() {
                array.push($(this).val());
            });

            this.select = array;
            this.trigger("change:select", this.select);
            // alert(that.select);
        },
        */
        refresh: function() {
            var t = this;
            // Holisun add false
            t.dt.fnDraw(false);
            //console.log("KKKKKKKKKKKKKK");
        }
    });

})(jQuery);

(function($) {
    var B = Backbone;
    var BV = B.View;
    var H = Handlebars;

    //針對Backbone.Datagrid的修改！
    var abstract_form_cell = {
        default_value: "",
        useTemplate: true,
        fedcls: "input",
        'class': 'col col-6',
        view: function(o) {
            if (this.disabled == 'disabled')
                return $('<input type="text" name="' + this.property + '"disabled="' + this.disabled + '" placeholder="' + this.title + '" class="text" >');
            else
                return $('<input type="text" name="' + this.property + '" placeholder="' + this.title + '" class="text" >');
        },
        value: function() { //provide value
            return $('input[name="' + this.property + '"]').val();
        },
        setValue: function(m) { //set the value to this element
            var v = m.get(this.property) || this.default_value;
            $('input[name="' + this.property + '"]').val(v);
        },
        update: function(m) {

        }
    };

    //只負責畫面產生
    B.FormView = BV.extend({
        template: Handlebars.compile($('#tpl_edit').html()),
        cell_template: Handlebars.compile('<section><label id="title" class="label" for="{{property}}">{{title}}</label><label class="input"></label><div class="note"></div></section>'),

        initialize: function(options) {
            this.options = options;

            this.forms = this.options.forms;
            this.button = this.options.button || [];
            if (!this.button) {
                this.button = _.defaults(this.button, [{
                    "type": "submit",
                    "cls": "btn btn-primary",
                    "text": "Save"
                }, {
                    "type": "button",
                    "cls": "btn btn-default",
                    "fn": "window.history.back()",
                    "text": "Back"
                }]);
            }
            var flen = this.forms.length;
            for (var i = 0; i < flen; i++) {
                spec = this.forms[i];
                _.defaults(spec, abstract_form_cell);
            };

            /*
			i18n.init(function(t){
                $('[data-i18n]').i18n();
            });
			*/

            this._prepare();
        },
        _prepare: function() {
            this.render();
        },
        render: function() {
            this._renderForm(this.model);
            return this;
        },
        _renderForm: function(model) {
            //先取得template
            this.$el = $(this.el).append(this.template({
                Name: this.options.Name
            }));

            //產生form
            var $form = $('<form></form>');
            $form.addClass('smart-form'); //硬幹的把className加進去

            //把form加進template中
            this.$el.find(".widget-body").append($form);

            $fieldset = $('<fieldset></fieldset>');
            $form.append($fieldset);

            $row = $('<div class="row"></div>');
            $fieldset.append($row);
            //針對每一個 column 來進行 iteration
            var len = this.forms.length;
            for (var i = 0; i < len; i++) {
                var spec = this.forms[i];

                if (spec === "fieldset") {
                    $fieldset = $('<fieldset></fieldset>');
                    $form.append($fieldset);
                    //use cell_template
                } else if (spec === "row") {
                    $row = $('<div class="row"></div>');
                    $fieldset.append($row);
                    //use cell_template
                } else if (spec.useTemplate === true) {
                    var data = {
                        property: spec.property,
                        title: spec.title
                    };
                    $cell = $(this.cell_template(data));
                    $cell.addClass(spec['class']);
                    $el = spec.view(model); //call external function
                    $input = $cell.find(".input");
                    $input.append($el).removeClass("input").addClass(spec.fedcls);

                    if (_.isString(spec.note)) {
                        $note = $cell.find(".note");
                        $note.append(spec.note);
                    }

                    $el = $cell;
                    spec.$el = $el;
                    $row.append($el);

                } else { //不用cell_template
                    $el = spec.view(model); //call external function
                    spec.$el = $el;
                    $row.append($el);
                }

            }

            for (var i = 0; i < len; i++) {
                var spec = this.forms[i];
                if (_.isObject(spec) && _.isFunction(spec.ready)) {
                    spec.ready();
                }
            }

            // $form.append("<hr>");
            // $form.append('<a href="#" class="btn btn-primary submit">Save</a>');
            // $form.append('<a href="#grid" class="btn btn-warning">Cancel</a>');

            var buttonsHtml = '<footer>';

            for (var i in this.button) {
                //$form.append('<footer><button type="submit" class="btn btn-primary">' + this.lang.save + '</button><button type="button" class="btn btn-default" onclick="window.history.back();">' + this.lang.back + '</button></footer>');
                //<button type="submit" class="btn btn-primary">' + this.lang.save + '</button>
                //<button type="button" class="btn btn-default" onclick="window.history.back();">' + this.lang.back + '</button>
                var buttonType = this.button[i].type || "button";
                var buttonClass = this.button[i].cls || "btn btn-default";
                var buttonOnClickHandler = this.button[i].fn || false;
                var buttonText = this.button[i].text || "";
                buttonsHtml += '<button '
                buttonsHtml += 'type="' + buttonType + '" class="' + buttonClass + '"';
                if (buttonOnClickHandler) {
                    buttonsHtml += 'onclick="' + buttonOnClickHandler + '"';
                }
                buttonsHtml += '>' + buttonText + '</button>';
            }
            buttonsHtml += '</footer>';
            $form.append(buttonsHtml);

            //set up default for all property
            for (var i = 0; i < len; i++) {
                var spec = this.forms[i];
                if (_.isObject(spec))
                    spec.setValue(model);
            }
        }
    });

})(jQuery);

(function($) {
    //system default value
    var gvs = ".cctech-gv";
    var fvs = ".cctech-fv";

    /* Router starts */
    var B = Backbone;
    var BM = B.Model;
    var BC = B.Collection;
    var BR = B.Router;

    // object property

    var op = {

        //frist run the initialize
        initialize: function(opt) {
            var t = this;

            t.options = opt;

            opt.gvs = opt.gvs || gvs;
            // alert(opt.gvs);
            opt.fvs = opt.fvs || fvs;
            // alert(opt.fvs);
            t.AD = opt.AD;
            // t.addurl = opt.ADaddurl || "add";

            t.$gv = $(opt.gvs).first();
            t.$fv = $(opt.fvs).first();

        },



        show_add: function() {

            var t = this;

            t.$fv.empty();
            t.$gv.hide();

            var m = new t.AD.Model();

            t.fv = new B.FormView({
                el: t.$fv,
                Name: "Add",
                model: new B.Model(),
                forms: t.AD.forms,
                lang: {
                    save: 'Save',
                    back: 'Back'
                },
                button: [{
                    "type": "submit",
                    "cls": "btn btn-primary",
                    "text": "Save"
                }, {
                    "type": "button",
                    "cls": "btn btn-default",
                    "fn": "window.history.back()",
                    "text": "Back"
                }]
            });

            t.fv.$el.unbind();
            t.fv.$el.on("click", '[type="submit"]', function(event) {
                t.submit(event, m);
            });
            t.$fv.show();

            pageSetUp();

        },



        show_edit: function(_id) {
            var t = this;
            t.$fv.empty();
            t.$gv.hide();

            var model = new t.AD.Model();

            var attrs = {};
            var idAttr = model.idAttribute;
            attrs[idAttr] = _id;
            model.set(attrs);

            model.fetch({
                async: false,
                success: function(model, response, options) {
                    t.fv = new B.FormView({
                        el: t.$fv,
                        Name: "Edit",
                        model: model,
                        forms: t.AD.forms,
                        lang: {
                            save: 'Save',
                            back: 'Back'
                        },
                        button: [{
                            "type": "submit",
                            "cls": "btn btn-primary",
                            "text": "Save"
                        }, {
                            "type": "button",
                            "cls": "btn btn-default",
                            "fn": "window.history.back()",
                            "text": "Back"
                        }]
                    });
                    // this.fv.render();

                    t.fv.$el.unbind();
                    t.fv.$el.on("click", '[type="submit"]', function(event) {
                        // var $target = $(event.target);
                        event.preventDefault();
                        // alert("hello!");
                        t.submit(event, model, t.AD.forms);
                    });
                    t.$fv.show();

                    pageSetUp();
                },
                error: function(model, response, options) {
                    window.location.hash = "#ajax/error404.html";
                }
            });



        },


        show_grid: function() {
            var t = this;

            /*
            $('body,html').animate({
                scrollTop: 0
            }, 500);*/

            // init GridView
            if (!t.gv) {
                t.gv = new B.GridView({
                    el: t.$gv,
                    collection: new t.AD.Collection(),
                    columns: t.AD.columns,
                    title: t.AD.title,
                    isAdd: !_.isUndefined(t.AD.isAdd) ? t.AD.isAdd : true,
                    //isUpload: !_.isUndefined(t.AD.isUpload) ? t.AD.isUpload : true,
                    sort: t.AD.sort,
                    order: t.AD.order,
                    filter: t.AD.filter,
                    buttons: t.AD.buttons,
                    paginated: t.AD.paginated,
                    AD: t.AD
                });

                t.$gv.on("click", ".add-btn", function(event) {
                    event.preventDefault();

                    var hash = window.location.hash;
                    var url = location.hash.replace(/^#/, '');

                    var action = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$3');
                    url = url.replace(/^([^\/]+)\/([^\/]+)\/([^.]+)$/, '$1/$2');

                    // alert(t.AD.addurl);
                    // var addurl = t.AD.addurl || "add";
                    t.navigate("#" + t.AD.page + "/" + "add", {
                        trigger: true
                    });
                });
            } else
                t.gv.refresh();

            t.$fv.hide();
            t.$gv.show();

            pageSetUp();

            // setTimeout(runDataTables, 500);
        },

        render: function() {
            var t = this;
            t.navigate("#" + t.AD.page + "/grid", {
                trigger: true
            });
        },

        delete_single: function(_id) {
            var t = this;

            var model = new this.AD.Model();

            var attrs = {};
            var idAttr = model.idAttribute;
            attrs[idAttr] = _id;
            model.set(attrs);

            model.destroy({
                async: false,
                success: function(model, response, options) {
                    t.navigate("#" + t.AD.page + "/grid", {
                        trigger: true
                    });
                },
                error: function(model, xhr, options) {
                    // alert("Error:" + xhr.reponseText);
                }
            });

        },


        submit: function(event, model, forms) {
            event.preventDefault();

            var t = this;

            if (_.isUndefined(forms)) {
                forms = t.AD.forms;
            }



            var $fv = t.fv.$el;

            var clen = forms.length;

            for (var i = 0; i < clen; i++) {
                var c = forms[i];
                if (_.isObject(c)) {
                    var v = c.value();
                    if (_.isObject(v))
                        model.set(v);
                    else
                        model.set(c.property, v);
                }
            }

            if (t.AD.validate && !t.AD.validate(model))
                return;

            model.save({}, {
                async: false,
                crossDomain: true,
                error: t.AD.error || function(model, xhr, options) {
                    // alert("error:" + xhr.responseText);
                },
                success: function(model, response, options) {
                    for (var i = 0; i < clen; i++) {
                        var c = forms[i];
                        if (_.isObject(c)) {
                            var v = c.update(model);
                        }
                    }
                    t.navigate("#" + t.AD.page + "/grid", {
                        trigger: true
                    });
                }
            });
        }
    };

    // class property
    var cp = {
        extend: function(a, b) {
            var target = _.clone(a);
            _.defaults(target.routes, op.routes);

            var routes = target.routes;
            _.defaults(target, op);
            target.routes = routes;
            return Backbone.Router.extend(target, b);
        }
    };

    B.DG1 = BR.extend(op, cp);

    B.getCollectionByUrl = function(url, idAttribute) {
        var M = BM.extend({
            urlRoot: url,
            idAttribute: idAttribute
        });

        var C = BC.extend({
            url: url,
            model: M
        });
        return new C();
    };
})(jQuery);


(function($) {
    var B = Backbone;
    var BU = B.UIToolBox = {};
})(jQuery);

(function($) {
    var tag_template = '<div class="col-sm-12"><div class="form-group"><div class="bootstrap-tagsinput" ></div></div><div class="well"><div class="input-group"><input class="form-control" type="text" placeholder=""><div class="input-group-btn"><button class="btn btn-default btn-primary" type="button" style="height: 100%; width: 81px;">Add</button></div></div></div></div>';
    var onetag_template;
    /* title: '單選選項',
            property: 'single_choice_data',
            'class': 'col col-6 single_choice_data',
            note: '可按下新增新增單選項項目', */
    Backbone.UIToolBox.tag = {
        view: function(o) {
            var html = tag_template;

            if (!onetag_template)
                onetag_template = Handlebars.compile('<span class="tag label label-info">{{ value }}<span data-role="remove"></span></span>');

            return $(html);
        },
        value: function() {
            var t = this;
            var ret = {};
            ret[t.property] = JSON.stringify(t.values);


            return ret;
        },
        setValue: function(m) {
            var t = this;
            var vs = m.get(t.property);
            var v = (vs && $.parseJSON(vs)) || [];
            t.values = v;
            for (var i = 0; i < v.length; i++) {
                t.$el.find('.bootstrap-tagsinput').append(onetag_template({
                    value: v[i]
                }));
            }
        },
        update: function(m) {

        },
        ready: function() {
            var t = this;
            t.$el.on("click", "button", function(event) {
                var v = t.$el.find('input').val();
                var index = t.values.indexOf(v);
                if (v != '' && index < 0) {
                    t.$el.find('.bootstrap-tagsinput').append('<span class="tag label label-info">' + v + '<span data-role="remove"></span></span>');
                    t.$el.find('input').val('');
                    t.values.push(v);
                } else if (v != '' && index > -1) {
                    // alert('選項已存在');
                } else {
                    // alert('你需要填寫文字');
                }
            });

            t.$el.on("click", '[data-role="remove"]', function(event) {
                // var t = this;
                $target = $(event.target).parent()
                var text = $target.text();
                // alert(text);
                var index = t.values.indexOf(text);
                if (index > -1) {
                    t.values.splice(index, 1);
                    $target.replaceWith('');
                }

            });
        }
    }
})(jQuery);

(function($) {

    var B = Backbone;
    var BU = B.UIToolBox;

    BU.password = {
        view: function(o) {
            return '<input type="password" name="' + this.property + '" placeholder="Password" class="password">';
        },
        default_value: "",
        value: function() { //provide value
            var v = $('input[name="' + this.property + '"]').val();
            return v ? SHA1(v) : undefined;
        },
        setValue: function(o) { //set the value to this element
            $('input[name="' + this.property + '"]').val();
        }
    };
})(jQuery);

(function($) {

    var B = Backbone;
    var BU = B.UIToolBox;
    /*
        _.defaults({
            title: '狀態',
            property: 'Verified',
            label_value: [ {label:"未驗證",value:"0"},{label:"已驗證",value:"1"}],
        },Backbone.UIToolBox.selector)
    */
    BU.selector = {
        "fedcls": "select",
        view: function(o) {
            this.default_value = this.default_value || this.label_value[0].value;

            var lv = this.label_value;
            var lvlen = lv.length;
            var _html = '<select class="' + this.property + '">';
            if (this.tips !== undefined) {
                _html += '<option value="" disabled selected>' + this.tips + '</option>';
            }
            for (var i = 0; i < lvlen; i++)
                _html += '<option value="' + lv[i].value + '">' + lv[i].label + '</option>';
            _html += '</select>';
            return _html;
        },
        value: function() {
            return $("select." + this.property).val();
        },
        setValue: function(o) {
            var v = o.get(this.property) || this.default_value;
            $("select." + this.property).val(v);
        }
    }

})(jQuery);

(function($) {
    Backbone.UIToolBox.RadioButton = {
        "fedcls": "radio",
        view: function(o) {
            var options = this.options;
            var _html = '<div class="inline-group" id="' + this.property + '">';
            for (var i = 0; i < options.length; i++) {
                _html += '<label class="radio">';
                _html += '<input type="radio" name="radio-inline" ' + ((options[i].isChecked) ? 'checked="checked"' : '') + '>';
                _html += '<i></i>' + options[i].name + '</label>';
            }
            _html += '</div>';
            return _html;
        },
        value: function() {
            return $("#" + this.property + " input:checked").parent().text();
        },
        setValue: function(m) {
            var v = m.get(this.property);
            $("#" + this.property + " label:contains('" + v + "') input").attr("checked", "checked");
        }
    };
})(jQuery);

(function($) {
    Backbone.UIToolBox.Button = {
        "fedcls": "button",
        view: function(o) {}
    };
})(jQuery);

(function($) {
    /* 能用 */
    Backbone.UIToolBox.textarea = {
        "rows": 6,
        "fedcls": "textarea",
        view: function(o) {
            return '<textarea name="' + this.property + '" rows="' + this['rows'] + '"></textarea>';
        },
        default_value: "",
        value: function() { //provide value
            return $('textarea[name="' + this.property + '"]').val();
        },
        setValue: function(m) { //set the value to this element
            var v = m.get(this.property);
            $('textarea[name="' + this.property + '"]').text(v);
        }
    }
})(jQuery);

(function($) {

    var B = Backbone;
    var BU = B.UIToolBox;
    /*
        _.defaults({
            title: '狀態',
            property: 'Verified',
            label_value: [ {label:"未驗證",value:"0"},{label:"已驗證",value:"1"}],
        },Backbone.UIToolBox.selector)
    */
    BU.selector_backend = {
        "fedcls": "select",
        idAttribute: "id",
        data: {},
        fields: ["id", "name"],
        view: function(o) {
            var t = this;
            var v = t.fields[0];
            var l = t.fields[1];
            var lv = t.fetch();
            t.default_value = t.default_value || lv[0][v];

            // = t.label_value;
            var lvlen = lv.length;
            var _html = '<select class="' + t.property + '">';
            for (var i = 0; i < lvlen; i++)
                _html += '<option value="' + lv[i][v] + '">' + lv[i][l] + '</option>';
            _html += '</select>';
            return _html;
        },
        value: function() {
            return $("select." + this.property).val();
        },
        setValue: function(o) {
            var t = this;
            var v = o.get(t.property) || t.default_value;
            $("select." + t.property).val(v);
        },
        fetch: function() {
            var t = this;
            if (!t.coll) {
                var coll = B.getCollectionByUrl(t.url, t.idAttribute);
                t.data.fields = t.fields;
                coll.fetch({
                    async: false,
                    data: t.data
                });
                var info = coll.at(coll.length - 1);
                coll.remove(info);

                t.coll = coll;
            }
            return t.coll.toJSON();
        }
    }

})(jQuery);

/*
 
 */

(function($) {
    /*
    _.defaults({
      title:'權重'
          property:"Weight",
      default_value:50     //optional
        },Backbone.UIToolBox.slider)
     */
    Backbone.UIToolBox.slider = {
        default_value: 50,
        view: function(o) {
            var that = this;
            var v = o.get(this.property) || this.default_value;
            var html = '<input type="text" class="slider slider-primary" id="g1" value="" data-slider-max="100" data-slider-value="' + v + '" data-slider-selection = "before" data-slider-handle="round">';
            // var html = '<div style="width:260px"><div id="' + this.property + '-amount">' + v + '</div><div class="slider-yellow slider"></div></div>';
            var $html = $(html);
            // $html.ready(function() {
            //     $html.find(".slider").slider({
            //         value: v,
            //         orientation: "horizontal",
            //         range: "min",
            //         animate: true,
            //         slide: function(event, ui) {
            //             $("#" + that.property + "-amount").html(ui.value);
            //         }
            //     });
            // });
            return $html;
        },
        value: function(o) {
            var that = this;
            return $("#" + that.property + "-amount").html();
        },
        setValue: function(o) {

        }
    }
})(jQuery);

(function($) {
    var datetimepicker_template;
    var datetimepicker_range_template;

    var BU = Backbone.UIToolBox;
    var df = new DateFormatter({
        format: "yyyy-MM-dd hh:mm:ss"
    });

    BU.datetime = {};
    BU.datetime_range = {};

    var pk = {
        view: function(m) {
            if (!datetimepicker_template)
                datetimepicker_template = Handlebars.compile($('#tpl_datetimepicker').html());
            var html = datetimepicker_template(this);
            var $html = $(html);
            $html = $html.datetimepicker(this);
            this.$dtp = $html.data("datetimepicker");
            return $html;
        },
        setValue: function(m) {
            var v = m.get(this.property) || new Date();

            if (typeof v === "string") {
                v = df.parseDate(v);
            }

            v.setHours(v.getHours() + 8);
            //console.log("m="+m.get(this.property)+" v="+v);
            this.$dtp.setValue(v);
        }
    };

    var pkr = {
        placeholder: ["開始時間", "結束時間"],
        view: function(m) {
            var t = this;
            // alert($('#tpl_datetimepicker_range').html());
            if (!datetimepicker_range_template)
                datetimepicker_range_template = Handlebars.compile($('#tpl_datetimepicker_range').html());
            var html = datetimepicker_range_template(t);
            var $html = $(html);

            $dtp1 = $html.find('.' + t.property[0] + '_dt').datetimepicker(t).on("show", function() {
                $(".datepicker .ui-datepicker-inline").hide();
            });
            t.$dtp1 = $dtp1.data("datetimepicker");

            $dtp2 = $html.find('.' + t.property[1] + '_dt').datetimepicker(t).on("show", function() {
                $(".datepicker .ui-datepicker-inline").hide();
            });;
            t.$dtp2 = $dtp2.data("datetimepicker");
            return $html;
        },
        setValue: function(m) {
            var t = this;
            var v = m.get(this.property[0]) || new Date();

            if (typeof v === "string") {
                v = df.parseDate(v);
            }

            v.setHours(v.getHours() + 8);
            //console.log("m="+m.get(this.property)+" v="+v);
            this.$dtp1.setValue(v);

            var v = m.get(this.property[1]) || new Date();

            if (typeof v === "string") {
                v = df.parseDate(v);
            }

            v.setHours(v.getHours() + 8);
            //console.log("m="+m.get(this.property)+" v="+v);
            this.$dtp2.setValue(v);
        },
        value: function() {
            var ret = {};
            ret[this.property[0]] = $('input[name="' + this.property[0] + '"]').val();
            ret[this.property[1]] = $('input[name="' + this.property[1] + '"]').val();
            return ret;
        }
    };

    BU.datetime_range.datetimepicker = _.defaults({
        format: "yyyy-MM-dd hh:mm:ss",
        pickDate: true,
        pickTime: true,
        pickSeconds: false,
        language: "zh_TW"
    }, pkr);

    BU.datetime.yearpicker = _.defaults({
        format: "yyyy",
        pickDate: true,
        pickTime: false,
        pickSeconds: false,
        viewMode: "years",
        minViewMode: "years",
        language: "zh_TW"
    }, pk);


    BU.datetime.yearmonthpicker = _.defaults({
        format: "yyyy-MM",
        pickDate: true,
        pickTime: false,
        pickSeconds: false,
        viewMode: "months",
        minViewMode: "months",
        language: "zh_TW"
    }, pk);

    BU.datetime.datepicker = _.defaults({
        format: "yyyy-MM-dd",
        pickDate: true,
        pickTime: false,
        pickSeconds: false,
        viewMode: "days",
        minViewMode: "days",
        language: "zh_TW"
    }, pk);


    BU.datetime.datetimepicker = _.defaults({
        format: "yyyy-MM-dd hh:mm:ss",
        pickDate: true,
        pickTime: true,
        pickSeconds: false,
        language: "zh_TW"
    }, pk);

    BU.datetime.timepicker = _.defaults({
        format: "hh:mm:ss",
        pickDate: false,
        pickTime: true,
        pickSeconds: false,
        language: "zh_TW"
    }, pk);
})(jQuery);

(function($) { //OK
    var tpl_editor = '<div class="col-sm-{{ col }}"><div class="form-group"><div class="summernote {{property}}"></div></div></div>';
    var tpl_editor_func;
    Backbone.UIToolBox.summernote = {
        view: function(o) {
            var t = this;
            if (!tpl_editor_func)
                tpl_editor_func = Handlebars.compile(tpl_editor);

            var html = tpl_editor_func(t);

            // alert(html);
            // alert(html);
            var $html = $(html);
            t.$html = $html;
            return $html;
        },
        ready: function() {
            var t = this;

            function startSummernote() {
                t.$editor = t.$html.find('.summernote.' + t.property);
                // alert(t.$editor.html());
                t.$editor.summernote({
                    height: 180,
                    focus: false,
                    tabsize: 2
                });
                if (t.tmp_code) {
                    t.$editor.code(t.tmp_code);
                }
            };

            loadScript("js/plugin/summernote/summernote.js", startSummernote);


        },
        value: function() {
            var t = this;
            return t.$editor.code();
        },
        setValue: function(m) {
            var t = this;
            // alert(m.get("detail1_data"));
            // var d=;
            if (t.$editor) {
                t.$editor.code(m.get(t.property));
            } else {
                t.tmp_code = m.get(t.property);
            }
        }
    };
})(jQuery);

(function($) {
    var jqfu_template;
    /*
         _.defaults({
          property:"Product.ProductPhoto",
          idAttribute:"ProductID"
        },Backbone.UIToolBox.multiphoto)
     */
    Backbone.UIToolBox.multiphoto = {
        useTemplate: false,
        view: function(o) {
            if (!jqfu_template)
                jqfu_template = Handlebars.compile($('#tpl_jqfu').html());
            var tid = o.get(this.idAttribute);
            var data = {}
            if (typeof tid !== 'undefined') {
                data.tid = tid;
                data.name = this.property;
                data.ref = window.location.hash;
            }
            var html = jqfu_template();
            var $html = $(html);
            $html.ready(function() {
                $html.fileupload({
                    url: '../jqfu/'
                });
                $html.addClass('fileupload-processing');
                $html.fileupload('option', {
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    maxFileSize: 5000000,
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
                });
                $.ajax({
                    // Uncomment the following to send cross-domain cookies:
                    //xhrFields: {withCredentials: true},
                    url: $html.fileupload('option', 'url'),
                    dataType: 'json',
                    context: $html[0],
                    data: data
                }).always(function() {
                    $(this).removeClass('fileupload-processing');
                }).done(function(result) {
                    $(this).fileupload('option', 'done')
                        .call(this, null, {
                            result: result
                        });
                });
            });
            return $html;
        },
        update: function(o) {
            tmp_photos = $("#fileupload").find(".template-download .name a");

            if (tmp_photos.length > 0) {
                var arr = [];
                for (var i = 0; i < tmp_photos.length; i++) {
                    var id = $(tmp_photos[i]).attr("download").replace(/^(\d+)\.[.]+$/, "$1");
                    arr.push(id.replace(/\..+$/g, ""));
                };

                var data = {
                    pids: arr,
                    name: this.property,
                    id: o.get("id")
                };
                $.ajax({
                    url: '../bn/index.php/photo/addto',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    async: true,
                    complete: function(xhr, textStatus) {
                        //called when complete
                        // alert("complete");
                    },
                    success: function(data, textStatus, xhr) {
                        //called when successful
                        // alert("success");
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        //called when there is an error
                        // alert("error");
                    }
                });

            }
        }
    };

    Backbone.UIToolBox.fileupload = {
        useTemplate: false,
        view: function(o) {
            var that = this;
            if (_.isUndefined(jqfu_template))
                jqfu_template = Handlebars.compile($('#tpl_jqfu').html());
            var tid = o.get(that.idAttribute);
            var data = {}
            if (typeof tid !== 'undefined') {
                data.tid = tid;
                data.name = this.property;
            }
            var html = jqfu_template();
            var $html = $(html);
            $html.ready(function() {
                $html.fileupload({
                    url: '../fileupload/'
                });
                $html.addClass('fileupload-processing');
                $html.fileupload('option', {
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    maxFileSize: 5000000,
                    //acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
                });

                that.data = {
                    url: $html.fileupload('option', 'url'),
                    dataType: 'json',
                    context: $html[0],
                    data: data
                };

                $.ajax(that.data).always(function() {
                    $(this).removeClass('fileupload-processing');
                }).done(function(result) {
                    $(this).fileupload('option', 'done').call(this, null, {
                        result: result
                    });
                });
            });
            return $html;
        },
        update: function(o) {
            tmp_photos = $("#fileupload").find(".template-download .name a");

            if (tmp_photos.length > 0) {
                var arr = [];
                for (var i = 0; i < tmp_photos.length; i++) {
                    var id = $(tmp_photos[i]).attr("href").replace(/^\.\.\/bn\/index\.php\/file\/download\/([\d]+)+$/, "$1");
                    arr.push(id);
                };

                var data = {
                    pids: arr,
                    name: this.property,
                    id: o.get("id"),
                };

                $.ajax({
                    url: '../bn/index.php/file/addto',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    async: false,
                    complete: function(xhr, textStatus) {
                        //called when complete
                        // alert("complete");
                    },
                    success: function(data, textStatus, xhr) {
                        //called when successful
                        // alert("success");
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        //called when there is an error
                        // alert("error");
                    }
                });

            }
        }
    };

    /*
         _.defaults({
          property:"Product.ProductPhoto",
          idAttribute:"ProductID"
        },Backbone.UIToolBox.photo)
     */
    Backbone.UIToolBox.photo = {
        useTemplate: false,
        view: function(o) {
            if (!jqfu_template)
                jqfu_template = Handlebars.compile($('#tpl_jqfu').html());
            var tid = o.get(this.idAttribute);
            var data = {

            };
            if (typeof tid !== 'undefined') {
                data.tid = tid;
                data.name = this.property;
            }
            var html = jqfu_template();
            var $html = $(html);
            $html.ready(function() {
                $html.fileupload({
                    url: '../jqfu/server/php/'
                });
                $html.addClass('fileupload-processing');
                $html.fileupload('option', {
                    disableImageResize: /Android(?!.*Chrome)|Opera/
                        .test(window.navigator.userAgent),
                    maxFileSize: 5000000,
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i
                });
                $.ajax({
                    // Uncomment the following to send cross-domain cookies:
                    //xhrFields: {withCredentials: true},
                    url: $html.fileupload('option', 'url'),
                    dataType: 'json',
                    context: $html[0],
                    data: data
                }).always(function() {
                    $(this).removeClass('fileupload-processing');
                }).done(function(result) {
                    $(this).fileupload('option', 'done')
                        .call(this, null, {
                            result: result
                        });
                });
            });
            return $html;
        },
        update: function(o) {
            tmp_photos = $("#fileupload").find(".template-download .name a");

            if (tmp_photos.length > 0) {
                var arr = [];
                for (var i = 0; i < tmp_photos.length; i++) {
                    var id = $(tmp_photos[i]).attr("download").replace(/^(\d+)\.[.]+$/, "$1");
                    arr.push(id.replace(/\..+$/g, ""));
                };

                var data = {
                    pids: arr,
                    name: "product.productphoto",
                    id: o.get("id")
                };
                $.ajax({
                    url: '../bn/index.php/photo/addto',
                    type: 'POST',
                    dataType: 'json',
                    data: data,
                    async: false,
                    complete: function(xhr, textStatus) {
                        //called when complete
                        // alert("complete");
                    },
                    success: function(data, textStatus, xhr) {
                        //called when successful
                        // alert("success");
                    },
                    error: function(xhr, textStatus, errorThrown) {
                        //called when there is an error
                        // alert("error:" + textStatus);
                    }
                });

            }
        }
    }
})(jQuery);
