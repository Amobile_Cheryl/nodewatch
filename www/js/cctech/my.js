(function($) {
	my = {
		isCid: function(id) {
			tab = "ABCDEFGHJKLMNPQRSTUVXYWZIO"
			A1 = new Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3);
			A2 = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 0, 1, 2, 3, 4, 5);
			Mx = new Array(9, 8, 7, 6, 5, 4, 3, 2, 1, 1);

			if (id.length != 10) return false;

			i = tab.indexOf(id.charAt(0));

			if (i == -1) return false;
			sum = A1[i] + A2[i] * 9;

			for (i = 1; i < 10; i++) {
				v = parseInt(id.charAt(i));
				if (isNaN(v)) return false;
				sum = sum + v * Mx[i];
			}

			if (sum % 10 != 0) return false;
			return true;
		},
		isEmail: function(email) {
			var regex = /^\w+((\-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|\-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
			return typeof email === 'string' && email.match(regex);
		},
		isPhone: function(phone) {
			var regex = /^[0-9][0-9\-]+[0-9]$/;
			return typeof phone === 'string' && phone.match(regex);
		},
		isBankAccount: function(bankaccount) {
			var regex = /^[0-9][0-9\-]+[0-9]$/;
			return typeof bankaccount === 'string' && bankaccount.match(regex);
		},
		isDigitString: function(digit) {
			var regex = /^\d+$/;
			return typeof digit === 'string' && digit.match(regex);
		},
		isNotEmpty: function(str) {
			return str;
		},
		isEmpty: function(str) {
			return !str;
		}
	};
})(jQuery);