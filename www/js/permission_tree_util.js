(function($) {
    $.permission_tree_util = {};

    $.permission_tree_util.permissionTreeCheck = function(roleNameSet){
        // SuperUserRole
        if ($.common.getPermissionClass(roleNameSet) === $.common.PermissionClass.SuperUserRole){
            $('input').prop('checked',true);
        }            
        // Other permission class
        else{
            $.each(roleNameSet, function(index, value) {
                $('input[name="' + value + '"]').prop('checked', true);
                if ($('input[name="' + value + '"]').length != 0)
                    $.permission_tree_util.recursiveParentCheck($('input[name="' + value + '"]'));
            });

            // If all the input except 'AllPermission' is checked, we have to check 'AllPermission'.
            var isAllPermissionsChecked = true;
            $.each($('#permissionTree :input'), function(index, value) {
                if (value.name != 'AllPermission') {
                    if (!$(value).is(':checked')) {
                        isAllPermissionsChecked = false;
                    }
                }
            });
            $('input[name="AllPermission"]').prop('checked', isAllPermissionsChecked);
        }
    }

    $.permission_tree_util.recursiveParentCheck = function(node) {
        if (node.length > 0) {
            if (node.get(0).name == 'AllPermission') return;
            var parent = $(node).parents('li').find(' > span > input');
            console.log('parent is ');
            $.each(parent, function(index, value) {
                console.log('value.name is'+value.name);
                if (value.name != 'AllPermission') {
                    console.log(value.name + ' is not SuperUserRole');
                    $(value).prop('checked', true);
                }
            })            
        }
    };

    // If this node's sibling are all unchecked, then we uncheck its parent..and go on...
    $.permission_tree_util.recursiveParentUncheck = function(node) {
        if (node.length > 0) {
            if (node.get(0).name == 'AllPermission') return;
            var parent = $(node).parent().parent().parent().parent().find(' > span > input');
            var sibling = $(node).parent().parent().parent().find(' > li > span > input');
            var isAllSiblingUnchecked = true;
            $.each(sibling, function(index, value) {
                // console.log(value.name);
                if ($(value).is(':checked')) {
                    isAllSiblingUnchecked = false;
                }
            })
            if (isAllSiblingUnchecked == true) {
                $(parent).prop('checked', false);
                $.permission_tree_util.recursiveParentUncheck(parent);
            }
        }
    }

    $.permission_tree_util.recursiveCheck = function(node, isChecked) {
        var leafs = node.parent().parent('li.parent_li').find(' > ul > li > span > label > input');
        leafs.prop('checked', isChecked);
        var nonleafs = node.parent().parent('li.parent_li').find(' > ul > li > span > input');
        nonleafs.prop('checked', isChecked);
        if (nonleafs.length > 0)
            $.permission_tree_util.recursiveCheck(nonleafs, isChecked);
    }
})(jQuery);
