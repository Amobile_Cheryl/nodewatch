(function($) {
    var AD = {};
    var referer;
    var id;
    var validateRule;
    AD.page = "ajax/user_profile.html";

    var uploadImage = function() {
            // Image
            var result = $('#result');
            var currentFile;
            var replaceResults = function(img) {
                var content;
                if (!(img.src || img instanceof HTMLCanvasElement)) {
                    content = $('<span>Loading image file failed</span>');
                } else {
                    content = $('<a target="_blank">').append(img)
                        .attr('download', currentFile.name)
                        .attr('href', img.src || img.toDataURL());
                }
                result.children().replaceWith(content);
                $("#appearance").val(content[0].href);
            };
            var displayImage = function(file, options) {
                currentFile = file;
                if (!loadImage(
                    file,
                    replaceResults,
                    options
                )) {
                    result.children().replaceWith(
                        $('<span>Your browser does not support the URL or FileReader API.</span>')
                    );
                }
            };
            var dropChangeHandler = function(e) {
                e.preventDefault();
                e = e.originalEvent;
                var target = e.dataTransfer || e.target,
                    file = target && target.files && target.files[0],
                    options = {
                        maxWidth: result.width(),
                        canvas: true
                    };
                if (!file) {
                    return;
                }
                loadImage.parseMetaData(file, function(data) {
                    displayImage(file, options);
                });
            };
            $("#appearanceInput").on("change", dropChangeHandler);
        }
        // console.log($("#title").children('font'));

    var reloadJs = function() {
        uploadImage();

        $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
        // Handle collapse & expand

        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').find(' > i').on('click', function(e) {
            var children = $(this).parent().parent('li.parent_li').find(' > ul > li');
            if (children.is(':visible')) {
                console.log('Expand Children');
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').removeClass().addClass('fa fa-lg fa-plus-circle');
            } else {
                console.log('Collapse Children');
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').removeClass().addClass('fa fa-lg fa-minus-circle');
            }
            e.stopPropagation();
        });        
    }

    AD.bootstrap = function() {
		$("body").removeClass("loading");
        // Reload JavaScript

        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });

        // Translate Input of the placeholder
        translatePlaceholder();

		$("#appearanceInput").change(function(event){
			$("#customFileInput span").text(this.files[0].name);
		});
		
        reloadJs();
        $('#btn_save').on('click',function(){
            var data = {
                //email: $('input[name="email"]').val(),
                remark: $('input[name="description"]').val(),
                portrait: $('input[name="appearance"]').val().replace("data:image/png;base64,", "")
            };

            // url = $.config.server_rest_url + '/users/' + $.login.user.get('userId');
            url = $.config.server_rest_url + '/changeMyData';
            $.ajax({
                type: "POST",
                async: false,
                url: url,
                contentType: 'application/json; charset=utf-8',
                headers: {
                    "X-HTTP-Method-Override": "PUT"
                },
                data: JSON.stringify(data),
                success: function() {
                    $.smallBox({
                        title: i18n.t("ns:Message.User.UpdateProfileSuccess"),
                        content: "<i class='fa fa-pencil-square-o'></i>"+ i18n.t("ns:Message.User.ProfileUpdateSuccess"),
                        // title: "Update profile success!",
                        // content: "<i class='fa fa-pencil-square-o'></i>Your profile is updated successfully.",
                        color: "#648BB2",
                        iconSmall: "fa fa-pencil-square-o fa-2x fadeInRight animated",
                        timeout: 5000
                    }); 
                    // Modify login user portrait
                    $.login.user.portrait = data.portrait;
                    localStorage.setItem("portrait",data.portrait);
                    $('#loginUserPortrait').attr('src','data:image/png;base64,'+data.portrait);  
                    window.location.href = "index.html";
                },
                error: function(data) {
                    var errorMsg = data.responseJSON.error;
                    if(errorMsg.match("email not a well-formed email address,")){
                        errorMsg = i18n.t("ns:Message.User.EmailErrorMsg");
                    }
                    $.smallBox({
                        title: i18n.t("ns:Message.User.UpdateProfileFailed"), 
                        // title: "Update profile failed!",
                        content: "<i class='fa fa-pencil-square-o'></i>"+ errorMsg,
                        color: "#B36464",
                        iconSmall: "fa fa-exclamation-circle fa-2x fadeInRight animated",
                        timeout: 5000
                    });    
                },
                dataType: "json"
            });            
        });

        // var url = $.config.server_rest_url + '/users';

        $.ajax({
            type: "GET",
            // url: url + '/' + $.login.user.get('userId'),
            url: $.config.server_rest_url + '/mySessionData',
            dataType: 'json',
            success: function(data) {
                console.log(data);
                // for tab1 to display user informaiton
                // $("#username").attr('disabled', true);
                // $("#permissionclass").attr('disabled', true);
                // $("#domainname").attr('disabled', true);
                $('input[type="checkbox"]').attr('disabled','disabled');

                //$('input[name="username"]').val(data.userName);
                $('input[name="description"]').val(data.remark);
                //$('input[name="email"]').val(data.email);
                $('#username').text(data.userName);
                $('#domainname').text(data.domainName);
                $('#email').text(data.email);
                
                var permission = $.common.getPermissionClass(data.roleNameList);

                $("#permissionColor").attr('style','color:'+permission.color);
                $('#permissionclass').text(permission.display);
                $('input[name="domainname"]').val(data.domainName);

                var portrait = data.portrait || $.common.defaultPortrait;
                $('input[name="appearance"]').attr('value', portrait);
                $('#result').children().replaceWith('<img src="' + portrait + '" style="box-shadow: 1px 2px 3px rgba(0,0,0,0.1);background-color: #fff;border: 1px solid #e3e3e3;padding: 4px;position: relative;z-index: 1;"/>');

                // for tab2 to display permission
                console.log('Start to check relative permission.');
                $.permission_tree_util.permissionTreeCheck(data.roleNameList);
            },
            async: false
        });

    
        // var $validator = $("#wizard-1").validate({

        //     rules: validateRule,

        //     messages: {
        //         // rolename: "User name MUST be filled",
        //         // description: "Please specify something to remind you",
        //         // email: "Please use the available email address"
        //     },

        //     highlight: function(element) {
        //         $(element).closest('.form-group').removeClass('has-success').addClass('has-error');
        //     },
        //     unhighlight: function(element) {
        //         $(element).closest('.form-group').removeClass('has-error').addClass('has-success');
        //     },
        //     errorElement: 'span',
        //     errorClass: 'help-block',
        //     errorPlacement: function(error, element) {
        //         if (element.parent('.input-group').length) {
        //             error.insertAfter(element.parent());
        //         } else {
        //             error.insertAfter(element);
        //         }
        //     }
        // });


    };
    /* define bootstrap ends */
    $[AD.page] = AD;

    function translatePlaceholder () {
        $("#email").attr("placeholder", i18n.t("ns:User.Message.Email"));
        $("#appearanceInput").attr("placeholder", i18n.t("ns:User.Message.DeviceTypeAppearance"));
    }

})(jQuery);