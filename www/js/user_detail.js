(function($) {

    var AD = {};
    // var url = "http://220.130.176.238:8080/acs-core/RESTful/roles";
    // var idAttribute = AD.idAttribute = "id";
    AD.title = "Roles";
    AD.page = "ajax/user_detail.html";

    function s1(id) {
        // Don't allow user to modify it
        $('input[type="checkbox"]').attr('disabled','disabled');

        var url = $.config.server_rest_url + '/users/'+id;
        $.ajax({
            type: "GET",
            async: false,
            url: url,
            success: function(data) {
                $("#userName font").text(data.name);
                console.log('Get Success');
                // console.log('data is');
                // console.log(data);
                console.log(data.roleNameSet);
                var roleNameList = {};
                console.log('Start to check relative permission.');
                $.permission_tree_util.permissionTreeCheck(data.roleNameSet);
            },
            error: function(data){
                console.log('Get Error');
                // alert('Get Failed!!');
                return;
            },
        });
        
    };

    AD.bootstrap = function() {
        // Multi-Language
        i18n.init(function(t){
            $('[data-i18n]').i18n();
        });
        
        var hash = window.location.hash;
        var id = hash.replace(/^#ajax\/user_detail.html\/(\w+)$/, "$1");
        // console.log(id);
		$("body").removeClass("loading");
        // Initial page
        s1(id);


        // PAGE RELATED SCRIPTS

        $('.tree > ul').attr('role', 'tree').find('ul').attr('role', 'group');
        // Handle collapse & expand

        $('.tree').find('li:has(ul)').addClass('parent_li').attr('role', 'treeitem').find(' > span').attr('title', 'Collapse this branch').find(' > i').on('click', function(e) {
            var children = $(this).parent().parent('li.parent_li').find(' > ul > li');
            if (children.is(':visible')) {
                console.log('Expand Children');
                children.hide('fast');
                $(this).attr('title', 'Expand this branch').removeClass().addClass('fa fa-lg fa-plus-circle');
            } else {
                console.log('Collapse Children');
                children.show('fast');
                $(this).attr('title', 'Collapse this branch').removeClass().addClass('fa fa-lg fa-minus-circle');
            }
            e.stopPropagation();
        });
        
    };

    $[AD.page] = AD;
})(jQuery);