(function($) {
    /* define namespace starts */

    var AD = {};
    AD.collections = {};
    // var url = AD.url = $.config.server_url + '/system_property';
    var url = 'http://220.130.176.238:8080/acs-core/RESTful/domains';
    var idAttribute = AD.idAttribute = "id";
    AD.title = "Reseller";
    AD.page = "ajax/system_property_list.html";

    AD.validate = function() {
        return true;
    };


    AD.forms = [
        //form
        {
            title: 'System Name',
            property: 'name',
            default_value: ""
        },
        //form
        {
            title: 'Send Error Message',
            property: "isSendErrorMessage",
            // useTemplate: false,
            view: function() {
                var html = '<section>';
                html += '<label class="checkbox">';
                html += '<input type="checkbox" name="' + this.property + '" value="Y">';
                html += '<i></i>' + this.title;
                html += '</label>';
                html += '</section>';
                return $(html);
            },
            value: function() {
                var t = this;
                if (t.$el.find('input[type="checkbox"]:checked').length > 0)
                    return true;
                else
                    return false;
            },
            setValue: function(m) {
                var t = this;
                var v = m.get(t.property) || t.default_value || 'N';
                console.log(m.get(t.property));
                if (v === true || v === "true")
                    t.$el.find('input[type="checkbox"]').prop('checked', true);
                else
                    t.$el.find('input[type="checkbox"]').prop('checked', false);


            }
        },
        //form
        _.defaults({
            title: 'Description',
            property: 'remark',
            default_value: ""
        }, Backbone.UIToolBox.textarea),
        'row',
        //form
        _.defaults({
            property: "Test.Photo",
            idAttribute: "id"
        }, Backbone.UIToolBox.multiphoto)
    ];

    /* model starts */

    AD.Model = Backbone.Model.extend({
        urlRoot: url,
        idAttribute: idAttribute
    });

    /* model ends */

    var routes = {};

    routes[AD.page + '/grid'] = 'show_grid';
    routes[AD.page + '/edit/:_id'] = 'show_edit';
    // routes[AD.page+'/add/'] = 'show_add';
    routes[AD.page] = 'render';
    var MyRouter = Backbone.DG1.extend({
        routes: routes,
        render: function() {
            var t = this;
            t.navigate("#" + t.AD.page + "/edit/1", {
                trigger: true
            });
        },
        show_add: function() {
            Backbone.DG1.prototype.show_add.call(this);
        },
        // show_edit: function(_id) {
        //     Backbone.DG1.prototype.show_edit.call(this, _id);
        //     // $('.smart-form button[type="button"]').hide();
        //     // $('.cctech-fv header h2').html('編輯-個資使用同意資料');
        // },
        show_grid: function() {
            var t = this;
            // alert("更新成功");
            t.navigate("#" + t.AD.page + "/edit/1", {
                trigger: true
            });
        }
    });

    /* define bootstrap starts */
    AD.bootstrap = function() {
        // alert('bootstrap');
        if (!_.isUndefined($[AD.page].app)) {
            $[AD.page].app.navigate("#" + AD.page + "/edit/1", {
                trigger: true
            });
        }


        AD.app = new MyRouter({
            "AD": AD
        });

        if (!Backbone.History.started) {
            Backbone.emulateHTTP = true;
            Backbone.history.start();
        }

    };
    /* define bootstrap ends */

    $[AD.page] = AD;
    $[AD.page].bootstrap();

})(jQuery);